cmake_minimum_required(VERSION 3.20)

print_location()

find_package(Qt6 6.2 COMPONENTS Test Svg QuickTest Qml REQUIRED)

# Enable testing is not necessary because include(CTest) already enable testing but i keep it for information
enable_testing()

add_subdirectory(testData)
add_subdirectory(Logger)
add_subdirectory(Utils)
add_subdirectory(Painter)
add_subdirectory(DocumentDataDefinitions)
add_subdirectory(DocumentTemplate)
add_subdirectory(DataTemplateAssociation)
add_subdirectory(SosQml)
add_subdirectory(Document)
add_subdirectory(DataDocumentAssociation)
