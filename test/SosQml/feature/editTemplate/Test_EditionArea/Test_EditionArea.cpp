#include <QPainter>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQmlExtensionPlugin>
#include <QtQuickTest>
#include <QtWidgets/QApplication>

#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/PageSize.hpp"
#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "Painter/controllers/InterfaceQmlPaintableItem.hpp"
#include "SosQml/QmlPluginInclude.hpp"
#include "Utils/FileUtils.hpp"
#include "testData/TestFonts.hpp"

constexpr quint32 RGBA2ARGB(quint32 x)
{
    // RGBA8888 is ABGR32 on little endian.
    quint32 ag = x & 0xff00ff00;
    quint32 rg = x & 0x00ff00ff;
    return ag | (rg << 16) | (rg >> 16);
}

class Test_Painter : public QObject
{
    Q_OBJECT
  public:
    Test_Painter(QObject* parent = nullptr) : QObject(parent) {}

    Q_INVOKABLE QImage paint(documenttemplate::controller::DocumentTemplate* documentTemplate, QSize targetSize)
    {
        QSize documentSize(documentTemplate->pageSize()->sizePixel().width(),
                           documentTemplate->pageSize()->sizePixel().height());
        QImage image(targetSize, QImage::Format_RGBA8888_Premultiplied);
        image.fill("white");
        image.setDotsPerMeterX(documentTemplate->pageSize()->resolution() * 39.37);
        image.setDotsPerMeterY(documentTemplate->pageSize()->resolution() * 39.37);

        QPainter painter(&image);
        painter.scale(double(targetSize.width()) / double(documentSize.width()),
                      double(targetSize.height()) / double(documentSize.height()));

        for (int rowIndex = 0; rowIndex < documentTemplate->paintableItemList()->rowCount(); ++rowIndex)
        {
            const painter::controller::InterfaceQmlPaintableItem* const paintableItem =
              documentTemplate->paintableItemList()->at(rowIndex);
            paintableItem->paint(&painter);
        }

        return image;
    }

    Q_INVOKABLE bool exactImagesComparison(const QImage& qmlGrabedImage, const QImage& paintedImage)
    {
        if (qmlGrabedImage.size() != paintedImage.size())
        {
            qWarning() << "Images size are different : " << qmlGrabedImage.size() << "," << paintedImage.size();
        }

        if (qmlGrabedImage.format() != paintedImage.format())
        {
            qWarning() << "Images format are different : " << qmlGrabedImage.format() << ","
                       << paintedImage.format();
        }

        return qmlGrabedImage == paintedImage;
    }

    Q_INVOKABLE bool approximativeImagesComparison(const QImage& qmlGrabedImage,
                                                   const QImage& paintedImage,
                                                   unsigned int pixelApproximation,
                                                   const QList<QColor>& filteredColors)
    {
        QImage convertedQmlGrabedImage = qmlGrabedImage.convertToFormat(QImage::Format_ARGB32);
        QImage convertedPaintedImage   = paintedImage.convertToFormat(QImage::Format_ARGB32);
        if (convertedQmlGrabedImage.format() != QImage::Format_ARGB32
            || convertedPaintedImage.format() != QImage::Format_ARGB32)
        {
            qCritical() << "One of the image is not in the expected format. convertedQmlGrabedImage ("
                        << convertedQmlGrabedImage.format() << "), convertedPaintedImage("
                        << convertedPaintedImage.format() << ")";
            return false;
        }

        QSet<QRgb> filteredColorsRgb;

        for (const QColor& filteredColor : filteredColors) { filteredColorsRgb.insert(filteredColor.rgba()); }

        if (convertedQmlGrabedImage.size() != convertedPaintedImage.size())
        {
            qCritical() << "Images size are different : " << convertedQmlGrabedImage.size() << ","
                        << convertedPaintedImage.size();
            return false;
        }

        if (convertedQmlGrabedImage.format() != convertedPaintedImage.format())
        {
            qCritical() << "Images format are different : " << convertedQmlGrabedImage.format() << ","
                        << convertedPaintedImage.format();
            return false;
        }

        if (pixelApproximation == 0) { return convertedQmlGrabedImage == convertedPaintedImage; }

        ImageLines convertedQmlGrabedImageLines =
          initializeImageLines(convertedQmlGrabedImage, pixelApproximation);

        ImageLines convertedPaintedImageLines = initializeImageLines(convertedPaintedImage, pixelApproximation);

        for (int y = 0; y < convertedQmlGrabedImage.height(); ++y)
        {
            for (int x = 0; x < convertedQmlGrabedImage.width(); ++x)
            {
                if (convertedQmlGrabedImageLines.currentLine && convertedPaintedImageLines.currentLine
                    && filteredColorsRgb.contains(
                      convertedPaintedImageLines.pixelColor(convertedPaintedImageLines.currentLine, x)))
                {
                    if (convertedQmlGrabedImageLines.pixelColor(convertedQmlGrabedImageLines.currentLine, x)
                        != convertedPaintedImageLines.pixelColor(convertedPaintedImageLines.currentLine, x))
                    {
                        if (!searchPixelInOtherImage(
                              convertedPaintedImageLines.pixelColor(convertedPaintedImageLines.currentLine, x),
                              convertedQmlGrabedImageLines,
                              x))
                        {
                            qDebug() << "convertedQmlGrabedImageLines";
                            printImageArea(convertedQmlGrabedImageLines, x);
                            qDebug() << "convertedPaintedImageLines";
                            printImageArea(convertedPaintedImageLines, x);
                            qWarning() << "Image are different" << x << ";" << y << ";"
                                       << QColor(convertedQmlGrabedImageLines.pixelColor(
                                                   convertedQmlGrabedImageLines.currentLine, x))
                                            .name()
                                       << ";"
                                       << QColor(convertedPaintedImageLines.pixelColor(
                                                   convertedPaintedImageLines.currentLine, x))
                                            .name();
                            return false;
                        }
                    }
                }
            }

            // verifyImageLines(convertedQmlGrabedImageLines);
            advanceImageLines(convertedQmlGrabedImageLines);
            // verifyImageLines(convertedQmlGrabedImageLines);
            advanceImageLines(convertedPaintedImageLines);
        }

        return true;
    }

    Q_INVOKABLE void saveQImage(const QImage& image, const QString& fileName) { image.save(fileName); }

  private:
    Q_DISABLE_COPY_MOVE(Test_Painter)

    struct ImageLines
    {
        const QImage* image { nullptr };
        int lineNumber { 0 };
        unsigned int pixelApproximation { 0 };
        QList<const uchar*> previousLines;
        const uchar* currentLine;
        QList<const uchar*> nextLines;

        QRgb pixelColor(const uchar* line, int x) const
        {
            return RGBA2ARGB(reinterpret_cast<const quint32*>(line)[x]);
        }
    };

    ImageLines initializeImageLines(const QImage& image, unsigned int pixelApproximation)
    {
        ImageLines imageLines;
        imageLines.image              = &image;
        imageLines.lineNumber         = 0;
        imageLines.pixelApproximation = pixelApproximation;

        imageLines.currentLine = image.constScanLine(0);

        for (unsigned int i = 1; i <= pixelApproximation && i < static_cast<unsigned int>(image.height()); ++i)
        {
            imageLines.nextLines.append(image.constScanLine(i));
        }

        return imageLines;
    }

    void advanceImageLines(ImageLines& imageLines)
    {
        ++imageLines.lineNumber;

        if (imageLines.previousLines.size() == 0) { imageLines.previousLines.append(imageLines.currentLine); }
        else
        {
            if (imageLines.previousLines.size() < imageLines.pixelApproximation)
            {
                imageLines.previousLines.prepend(*std::begin(imageLines.previousLines));
            }

            auto begin = std::begin(imageLines.previousLines);
            auto end   = std::prev(std::end(imageLines.previousLines));

            for (auto iterator = begin; iterator != end; ++iterator) { *iterator = *std::next(iterator); }

            *std::rbegin(imageLines.previousLines) = imageLines.currentLine;
        }

        if (imageLines.nextLines.size() > 0)
        {
            imageLines.currentLine = *std::begin(imageLines.nextLines);

            auto begin = std::begin(imageLines.nextLines);
            auto end   = std::prev(std::end(imageLines.nextLines));

            for (auto iterator = begin; iterator != end; ++iterator) { *iterator = *std::next(iterator); }

            if (imageLines.lineNumber + imageLines.nextLines.size() < imageLines.image->height())
            {
                *std::rbegin(imageLines.nextLines) =
                  imageLines.image->constScanLine(imageLines.lineNumber + imageLines.nextLines.size());
            }
            else { imageLines.nextLines.removeLast(); }
        }
    }

    bool searchPixelInOtherImage(const QRgb& pixel, const ImageLines& imageLines, unsigned int x)
    {
        for (unsigned int dxImage = 1; dxImage <= imageLines.pixelApproximation && x >= dxImage; ++dxImage)
        {
            auto checkLineContainsPixel = [&pixel, &imageLines, &x, &dxImage](const uchar* previousLine) -> bool
            { return imageLines.pixelColor(previousLine, x - dxImage) == pixel; };

            if (std::any_of(std::cbegin(imageLines.previousLines),
                            std::cend(imageLines.previousLines),
                            checkLineContainsPixel))
            {
                return true;
            }

            if (imageLines.pixelColor(imageLines.currentLine, x - dxImage) == pixel) { return true; }

            if (std::any_of(
                  std::cbegin(imageLines.nextLines), std::cend(imageLines.nextLines), checkLineContainsPixel))
            {
                return true;
            }
        }

        for (unsigned int dxImage = 0; dxImage <= imageLines.pixelApproximation
                                       && (x + dxImage) < static_cast<unsigned int>(imageLines.image->width());
             ++dxImage)
        {
            auto checkLineContainsPixel = [&pixel, &imageLines, &x, &dxImage](const uchar* previousLine) -> bool
            { return imageLines.pixelColor(previousLine, x + dxImage) == pixel; };

            if (std::any_of(std::cbegin(imageLines.previousLines),
                            std::cend(imageLines.previousLines),
                            checkLineContainsPixel))
            {
                return true;
            }

            if (imageLines.pixelColor(imageLines.currentLine, x + dxImage) == pixel) { return true; }

            if (std::any_of(
                  std::cbegin(imageLines.nextLines), std::cend(imageLines.nextLines), checkLineContainsPixel))
            {
                return true;
            }
        }

        return false;
    }

    void printImageArea(const ImageLines& imageLines, unsigned int x)
    {
        for (const uchar* previousLine : imageLines.previousLines)
        {
            QString imageLineString;
            for (unsigned int dxImage = 1; dxImage <= imageLines.pixelApproximation && x >= dxImage; ++dxImage)
            {
                imageLineString.prepend(QColor(imageLines.pixelColor(previousLine, x - dxImage)).name());
                imageLineString.prepend(" ");
            }
            for (unsigned int dxImage = 0; dxImage <= imageLines.pixelApproximation
                                           && (x + dxImage) < static_cast<unsigned int>(imageLines.image->width());
                 ++dxImage)
            {
                imageLineString.append(QColor(imageLines.pixelColor(previousLine, x + dxImage)).name());
                imageLineString.append(" ");
            }
            qDebug() << imageLineString;
        }

        {
            QString imageLineString;
            for (unsigned int dxImage = 1; dxImage <= imageLines.pixelApproximation && x >= dxImage; ++dxImage)
            {
                imageLineString.prepend(QColor(imageLines.pixelColor(imageLines.currentLine, x - dxImage)).name());
                imageLineString.prepend(" ");
            }
            for (unsigned int dxImage = 0; dxImage <= imageLines.pixelApproximation
                                           && (x + dxImage) < static_cast<unsigned int>(imageLines.image->width());
                 ++dxImage)
            {
                imageLineString.append(QColor(imageLines.pixelColor(imageLines.currentLine, x + dxImage)).name());
                imageLineString.append(" ");
            }
            qDebug() << imageLineString;
        }

        for (const uchar* nextLine : imageLines.nextLines)
        {
            QString imageLineString;
            for (unsigned int dxImage = 1; dxImage <= imageLines.pixelApproximation && x >= dxImage; ++dxImage)
            {
                imageLineString.prepend(QColor(imageLines.pixelColor(nextLine, x - dxImage)).name());
                imageLineString.prepend(" ");
            }
            for (unsigned int dxImage = 0; dxImage <= imageLines.pixelApproximation
                                           && (x + dxImage) < static_cast<unsigned int>(imageLines.image->width());
                 ++dxImage)
            {
                imageLineString.append(QColor(imageLines.pixelColor(nextLine, x + dxImage)).name());
                imageLineString.append(" ");
            }
            qDebug() << imageLineString;
        }
    }

    void verifyImageLines(const ImageLines& imageLines)
    {
        if (imageLines.lineNumber >= imageLines.image->height()) { return; }
        if (imageLines.currentLine != imageLines.image->constScanLine(imageLines.lineNumber))
        {
            qFatal("Current line is wrong");
        }

        for (int x = 0; x < imageLines.image->width(); ++x)
        {
            if (QColor(imageLines.pixelColor(imageLines.currentLine, x))
                != imageLines.image->pixelColor(x, imageLines.lineNumber))
            {
                qFatal("Pixel color is badly read : %s, %s",
                       qUtf8Printable(QColor(imageLines.pixelColor(imageLines.currentLine, x)).name()),
                       qUtf8Printable(imageLines.image->pixelColor(x, imageLines.lineNumber).name()));
            }
        }

        for (int i = 0; i < imageLines.nextLines.size(); ++i)
        {
            if (imageLines.nextLines.at(i) != imageLines.image->constScanLine(imageLines.lineNumber + i + 1))
            {
                qFatal("Next line %d is wrong", i);
            }
        }

        for (int i = 0; i < imageLines.previousLines.size(); ++i)
        {
            if (imageLines.previousLines.at(imageLines.previousLines.size() - i - 1)
                != imageLines.image->constScanLine(imageLines.lineNumber - i - 1))
            {
                qFatal("Previous line %d is wrong", i);
            }
        }
    }
};

class Test_EditionAreaSetup : public QObject
{
    Q_OBJECT

  public:
  public slots:
    void applicationAvailable()
    {
        utils::FileUtils::loadFonts(testFontFolderPath);

        QFont font("Arial");
        font.setPixelSize(150);
        QGuiApplication::setFont(font);
    }

    void qmlEngineAvailable(QQmlEngine* engine)
    {
        engine->addImportPath(SosQmlPluginPath);
        engine->addImportPath(RootQmlPluginPath);
        engine->addPluginPath(SosQmlPluginPath);

        engine->rootContext()->setContextProperty("testPainter", &_testPainter);
    }

  private:
    Test_Painter _testPainter;
};

QUICK_TEST_MAIN_WITH_SETUP(Test_EditionArea, Test_EditionAreaSetup)

#include "Test_EditionArea.moc"
