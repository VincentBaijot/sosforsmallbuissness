import QtQuick
import QtQuick.Controls
import QtTest

import SosQml.Features.EditTemplate
import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosQml.Data
import SosQml.Controls

Item {
    id: root

    width: 1000
    height: 1000

    property size outputImageSize: Qt.size(3000, 3000)

    DocumentTemplate {
        id: testDocumentTemplate
    }

    ListModel {
        id: paintableItemListModel

        Component.onCompleted: {
            paintableItemListModel.append({
                                              "display": testDocumentTemplate.paintableItemList
                                          })
        }
    }

    EditionArea {
        id: editionArea

        anchors.fill: root

        selectedTool: Data.SelectionTool
        pageSize: testDocumentTemplate.pageSize
        model: paintableItemListModel
        selectedPaintableItem: undefined
    }

    TestCase {

        name: "InteractionsTest"
        when: windowShown

        function test_drawingRectangle() {
            editionArea.selectedTool = Data.RectangleTool
            editionArea.viewScale = 1

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel

            compare(testDocumentTemplate.paintableItemList.rowCount(), 0)

            var drawingSelectionRectangle = findChild(
                        editionArea, "drawingSelectionRectangle")
            verify(drawingSelectionRectangle !== null,
                   "drawingSelectionRectangle is null")

            compare(drawingSelectionRectangle.visible, false)

            var contentContainer = findChild(editionArea, "contentContainer")
            verify(contentContainer !== null, "contentContainer is null")

            //------------------------------------------
            // Draw a first rectangle
            //------------------------------------------
            editionArea.contentX = 0
            editionArea.contentY = 0

            mousePress(contentContainer, 0, 0)

            tryCompare(drawingSelectionRectangle, "visible", true)
            compare(drawingSelectionRectangle.x, 0)
            compare(drawingSelectionRectangle.y, 0)
            compare(drawingSelectionRectangle.width, 0)
            compare(drawingSelectionRectangle.height, 0)

            mouseMove(contentContainer, 2000, 2000)

            compare(drawingSelectionRectangle.x, 0)
            compare(drawingSelectionRectangle.y, 0)
            compare(drawingSelectionRectangle.width, 2000)
            compare(drawingSelectionRectangle.height, 2000)

            mouseRelease(contentContainer, 2000, 2000)

            tryCompare(drawingSelectionRectangle, "visible", false)

            //---------------------------------------------------------------------
            // Check that the rectangle is well added to the list of items
            //---------------------------------------------------------------------
            compare(testDocumentTemplate.paintableItemList.rowCount(), 1)

            var paintableItemRepeater = findChild(editionArea,
                                                  "paintableItemRepeater")
            verify(paintableItemRepeater !== null,
                   "paintableItemRepeater is null")

            var firstRectangle = paintableItemRepeater.itemAt(0)
            verify(firstRectangle !== null, "firstRectangle is null")

            compare(firstRectangle.objectName, "delegateContainer")

            var firstRectangleDisplay = findChild(firstRectangle,
                                                  "paintableRectangleDisplay")
            verify(firstRectangleDisplay instanceof PaintableRectangleDisplay,
                   "firstRectangleDisplay is not instanceof PaintableRectangleDisplay")

            compare(firstRectangleDisplay.x, 0)
            compare(firstRectangleDisplay.y, 0)
            compare(firstRectangleDisplay.width, 2000)
            compare(firstRectangleDisplay.height, 2000)

            //----------------------------------------------------------------------------
            // Draw a second rectangle,
            // check that the drawing rectangle is well displayed when the mouse move
            //----------------------------------------------------------------------------
            editionArea.contentX = 5000
            editionArea.contentY = 5000

            waitForRendering(editionArea)

            mousePress(contentContainer, 5000, 5000)

            tryCompare(drawingSelectionRectangle, "visible", true)
            compare(drawingSelectionRectangle.x, 5000)
            compare(drawingSelectionRectangle.y, 5000)
            compare(drawingSelectionRectangle.width, 0)
            compare(drawingSelectionRectangle.height, 0)

            mouseMove(contentContainer, 3000, 3000)

            compare(drawingSelectionRectangle.x, 3000)
            compare(drawingSelectionRectangle.y, 3000)
            compare(drawingSelectionRectangle.width, 2000)
            compare(drawingSelectionRectangle.height, 2000)

            mouseMove(contentContainer, 3500, 6000)

            compare(drawingSelectionRectangle.x, 3500)
            compare(drawingSelectionRectangle.y, 5000)
            compare(drawingSelectionRectangle.width, 1500)
            compare(drawingSelectionRectangle.height, 1000)

            mouseMove(contentContainer, 6000, 3500)

            compare(drawingSelectionRectangle.x, 5000)
            compare(drawingSelectionRectangle.y, 3500)
            compare(drawingSelectionRectangle.width, 1000)
            compare(drawingSelectionRectangle.height, 1500)

            mouseRelease(contentContainer, 6000, 3500)

            tryCompare(drawingSelectionRectangle, "visible", false)

            //---------------------------------------------------------------------
            // Check that the rectangle is well added to the list of items
            //---------------------------------------------------------------------
            compare(testDocumentTemplate.paintableItemList.rowCount(), 2)

            var secondRectangle = paintableItemRepeater.itemAt(1)
            verify(secondRectangle !== null, "secondRectangle is null")

            compare(secondRectangle.objectName, "delegateContainer")

            var secondRectangleDisplay = findChild(secondRectangle,
                                                   "paintableRectangleDisplay")
            verify(secondRectangleDisplay instanceof PaintableRectangleDisplay,
                   "secondRectangleDisplay is not instanceof PaintableRectangleDisplay")

            compare(secondRectangleDisplay.x, 5000)
            compare(secondRectangleDisplay.y, 3500)
            compare(secondRectangleDisplay.width, 1000)
            compare(secondRectangleDisplay.height, 1500)

            //----------------------------------------------------------------------------
            // Draw a third rectangle
            //----------------------------------------------------------------------------
            editionArea.contentX = contentContainer.width
                    + editionArea.flickableMargin - editionArea.width
            editionArea.contentY = contentContainer.height
                    + editionArea.flickableMargin - editionArea.height

            waitForRendering(editionArea)

            mousePress(contentContainer, contentContainer.width - 20,
                       contentContainer.height - 20)
            mouseMove(contentContainer, contentContainer.width - 220,
                      contentContainer.height - 209)
            mouseRelease(contentContainer, contentContainer.width - 220,
                         contentContainer.height - 209)

            compare(testDocumentTemplate.paintableItemList.rowCount(), 3)

            var thirdRectangle = paintableItemRepeater.itemAt(2)
            verify(thirdRectangle !== null, "thirdRectangle is null")

            compare(thirdRectangle.objectName, "delegateContainer")

            var thirdRectangleDisplay = findChild(thirdRectangle,
                                                  "paintableRectangleDisplay")
            verify(thirdRectangleDisplay instanceof PaintableRectangleDisplay,
                   "thirdRectangleDisplay is not instanceof PaintableRectangleDisplay")

            compare(thirdRectangleDisplay.x, contentContainer.width - 220)
            compare(thirdRectangleDisplay.y, contentContainer.height - 209)
            compare(thirdRectangleDisplay.width, 200)
            compare(thirdRectangleDisplay.height, 189)
        }

        function test_drawingText() {
            editionArea.selectedTool = Data.TextTool
            editionArea.viewScale = 1

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel

            compare(testDocumentTemplate.paintableItemList.rowCount(), 0)

            var drawingSelectionRectangle = findChild(
                        editionArea, "drawingSelectionRectangle")
            verify(drawingSelectionRectangle !== null,
                   "drawingSelectionRectangle is null")

            compare(drawingSelectionRectangle.visible, false)

            var contentContainer = findChild(editionArea, "contentContainer")
            verify(contentContainer !== null, "contentContainer is null")

            //------------------------------------------
            // Draw a first text
            //------------------------------------------
            editionArea.contentX = 0
            editionArea.contentY = 0

            mousePress(contentContainer, 0, 0)

            tryCompare(drawingSelectionRectangle, "visible", true)
            compare(drawingSelectionRectangle.x, 0)
            compare(drawingSelectionRectangle.y, 0)
            compare(drawingSelectionRectangle.width, 0)
            compare(drawingSelectionRectangle.height, 0)

            mouseMove(contentContainer, 2000, 2000)

            compare(drawingSelectionRectangle.x, 0)
            compare(drawingSelectionRectangle.y, 0)
            compare(drawingSelectionRectangle.width, 2000)
            compare(drawingSelectionRectangle.height, 2000)

            mouseRelease(contentContainer, 2000, 2000)

            tryCompare(drawingSelectionRectangle, "visible", false)

            //---------------------------------------------------------------------
            // Check that the text is well added to the list of items
            //---------------------------------------------------------------------
            compare(testDocumentTemplate.paintableItemList.rowCount(), 1)

            var paintableItemRepeater = findChild(editionArea,
                                                  "paintableItemRepeater")
            verify(paintableItemRepeater !== null,
                   "paintableItemRepeater is null")

            var firstText = paintableItemRepeater.itemAt(0)
            verify(firstText !== null, "firstText is null")

            compare(firstText.objectName, "delegateContainer")

            var firstTextDisplay = findChild(firstText, "paintableTextDisplay")
            verify(firstTextDisplay instanceof PaintableTextDisplay,
                   "firstTextDisplay is not instanceof PaintableTextDisplay")

            compare(firstTextDisplay.x, 0)
            compare(firstTextDisplay.y, 0)
            compare(firstTextDisplay.width, 2000)
            compare(firstTextDisplay.height, 2000)

            //----------------------------------------------------------------------------
            // Draw a second text,
            // check that the drawing rectangle is well displayed when the mouse move
            //----------------------------------------------------------------------------
            editionArea.contentX = 5000
            editionArea.contentY = 5000

            waitForRendering(editionArea)

            mousePress(contentContainer, 5000, 5000)

            tryCompare(drawingSelectionRectangle, "visible", true)
            compare(drawingSelectionRectangle.x, 5000)
            compare(drawingSelectionRectangle.y, 5000)
            compare(drawingSelectionRectangle.width, 0)
            compare(drawingSelectionRectangle.height, 0)

            mouseMove(contentContainer, 3000, 3000)

            compare(drawingSelectionRectangle.x, 3000)
            compare(drawingSelectionRectangle.y, 3000)
            compare(drawingSelectionRectangle.width, 2000)
            compare(drawingSelectionRectangle.height, 2000)

            mouseMove(contentContainer, 3500, 6000)

            compare(drawingSelectionRectangle.x, 3500)
            compare(drawingSelectionRectangle.y, 5000)
            compare(drawingSelectionRectangle.width, 1500)
            compare(drawingSelectionRectangle.height, 1000)

            mouseMove(contentContainer, 8000, 3500)

            compare(drawingSelectionRectangle.x, 5000)
            compare(drawingSelectionRectangle.y, 3500)
            compare(drawingSelectionRectangle.width, 3000)
            compare(drawingSelectionRectangle.height, 1500)

            mouseRelease(contentContainer, 6000, 3500)

            tryCompare(drawingSelectionRectangle, "visible", false)

            //---------------------------------------------------------------------
            // Check that the text is well added to the list of items
            //---------------------------------------------------------------------
            compare(testDocumentTemplate.paintableItemList.rowCount(), 2)

            var secondText = paintableItemRepeater.itemAt(1)
            verify(secondText !== null, "secondText is null")

            compare(secondText.objectName, "delegateContainer")

            var secondTextDisplay = findChild(secondText,
                                              "paintableTextDisplay")
            verify(secondTextDisplay instanceof PaintableTextDisplay,
                   "secondTextDisplay is not instanceof PaintableTextDisplay")

            compare(secondTextDisplay.x, 5000)
            compare(secondTextDisplay.y, 3500)
            compare(secondTextDisplay.width, 3000)
            compare(secondTextDisplay.height, 1500)

            //----------------------------------------------------------------------------
            // Draw a third text
            //----------------------------------------------------------------------------
            editionArea.contentX = contentContainer.width
                    + editionArea.flickableMargin - editionArea.width
            editionArea.contentY = contentContainer.height
                    + editionArea.flickableMargin - editionArea.height

            waitForRendering(editionArea)

            mousePress(contentContainer, contentContainer.width - 20,
                       contentContainer.height - 20)
            mouseMove(contentContainer, contentContainer.width - 2020,
                      contentContainer.height - 2009)
            mouseRelease(contentContainer, contentContainer.width - 2020,
                         contentContainer.height - 2009)

            compare(testDocumentTemplate.paintableItemList.rowCount(), 3)

            var thirdText = paintableItemRepeater.itemAt(2)
            verify(thirdText !== null, "thirdText is null")

            compare(thirdText.objectName, "delegateContainer")

            var thirdTextDisplay = findChild(thirdText, "paintableTextDisplay")
            verify(thirdTextDisplay instanceof PaintableTextDisplay,
                   "thirdTextDisplay is not instanceof PaintableTextDisplay")

            compare(thirdTextDisplay.x, contentContainer.width - 2020)
            compare(thirdTextDisplay.y, contentContainer.height - 2009)
            compare(thirdTextDisplay.width, 2000)
            compare(thirdTextDisplay.height, 1989)
        }
    }

    TestCase {

        name: "DisplayOfRectangle"
        when: windowShown

        function test_displayOfRectangle() {
            editionArea.viewScale = 1

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel

            var contentContainer = findChild(editionArea, "contentContainer")
            verify(contentContainer !== null, "contentContainer is null")

            var rectanglePosition = Qt.rect(20, 20, 1000, 1000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(1050, 20, 2000, 1000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(20, 1050, 1000, 2000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(1050, 1050, 500, 500)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(1600, 1600, 500, 500)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(20, contentContainer.height - 520,
                                        1000, 500)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(contentContainer.width - 2020, 20,
                                        1000, 2000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            rectanglePosition = Qt.rect(contentContainer.width - 2020,
                                        contentContainer.height - 520,
                                        2000, 500)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        rectanglePosition)

            compare(testDocumentTemplate.paintableItemList.rowCount(), 8)

            //-------------------------------
            // Edit the rectangles
            //-------------------------------
            var rectangle1 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(0, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle1.color = "blue"

            var rectangle2 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(1, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle2.color = "red"
            rectangle2.mode = PaintableRectangle.PaintableRectangleAbsoluteSize
            rectangle2.radius = 50

            var rectangle3 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(2, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle3.color = "yellow"
            rectangle3.mode = PaintableRectangle.PaintableRectangleAbsoluteSize
            rectangle3.radius = 50
            rectangle3.opacity = 50

            var rectangle4 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(3, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle4.color = "pink"
            rectangle4.radius = 50
            rectangle3.opacity = 70
            rectangle4.mode = PaintableRectangle.PaintableRectangleRelativeSize

            var rectangle5 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(4, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle5.color = ""

            var rectangle6 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(5, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle6.color = "black"
            rectangle6.border.color = "red"
            rectangle6.border.borderWidth = 100
            rectangle6.opacity = 80

            var rectangle7 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(6, 0),
                        RootPaintableItemList.PaintableItemRole)

            rectangle7.color = "black"
            rectangle7.border.color = "red"
            rectangle7.border.borderWidth = 100
            rectangle7.opacity = 80

            var rectangle8 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(7, 0),
                        RootPaintableItemList.PaintableItemRole)

            waitForRendering(editionArea)

            var targetSize = Qt.size(
                        contentContainer.width * root.outputImageSize.width
                        / contentContainer.height, root.outputImageSize.height)
            var qmlGrabedImage

            contentContainer.grabToImage(function (result) {
                qmlGrabedImage = result.image
            }, targetSize)

            tryVerify(function () {
                return qmlGrabedImage !== undefined
            })

            var paintedImage = testPainter.paint(testDocumentTemplate,
                                                 targetSize)

            if (!testPainter.approximativeImagesComparison(
                        qmlGrabedImage, paintedImage, 2,
                        ["black", "white", "red", "blue", "yellow", "pink", // red with opacity // black with opacity // pink with opacity // blue with opacity
                         "#ff3333", "#333333", "#ffc0cb", "#3333ff"])) {
                testPainter.saveQImage(qmlGrabedImage, "qmlGrabedImage.png")
                testPainter.saveQImage(paintedImage, "paintedImage.png")
                fail("qmlGrabedImage and paintedImage are different")
            }

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel
        }
    }

    TestCase {

        name: "DisplayOfText"
        when: windowShown

        function test_displayOfText() {
            editionArea.viewScale = 1

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel

            var contentContainer = findChild(editionArea, "contentContainer")
            verify(contentContainer !== null, "contentContainer is null")

            var textPosition = Qt.rect(20, 20, 2000, 2000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            textPosition = Qt.rect(2050, 20, 3000, 2000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            textPosition = Qt.rect(20, 2050, 2000, 3000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            textPosition = Qt.rect(2050, 2050, 1000, 1000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            textPosition = Qt.rect(20, contentContainer.height - 1020,
                                   2000, 1000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.RectangleItemType,
                        textPosition)

            textPosition = Qt.rect(contentContainer.width - 3020, 20,
                                   2000, 3000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            textPosition = Qt.rect(contentContainer.width - 3020,
                                   contentContainer.height - 1020, 3000, 1000)
            testDocumentTemplate.paintableItemList.addPaintableItem(
                        InterfaceQmlPaintableItem.TextItemType, textPosition)

            compare(testDocumentTemplate.paintableItemList.rowCount(), 7)

            //-------------------------------
            // Edit the Texts
            //-------------------------------
            var text1 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(0, 0),
                        RootPaintableItemList.PaintableItemRole)

            text1.color = "blue"
            text1.text = "Patate"
            text1.flag = PaintableText.PaintableTextAlignHCenter

            var text2 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(1, 0),
                        RootPaintableItemList.PaintableItemRole)

            text2.color = "red"
            text2.text = "A very long text with the word wrap, so we see the wrapping"
            text2.pointSize = 16
            text2.flags = PaintableText.PaintableTextWordWrap
                    | PaintableText.PaintableTextAlignVCenter

            var text3 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(2, 0),
                        RootPaintableItemList.PaintableItemRole)

            text3.color = "yellow"
            text3.text = "Centered text"
            text3.fontFamily = "Calibri"
            text3.flags = PaintableText.PaintableTextAlignCenter
            text3.background.color = "black"

            var text4 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(3, 0),
                        RootPaintableItemList.PaintableItemRole)

            text4.color = "pink"
            text4.text = "Something written"
            text4.background.color = "green"
            text4.background.radius = 20
            text4.border.borderWidth = 20
            text4.border.color = "blue"

            var text5 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(4, 0),
                        RootPaintableItemList.PaintableItemRole)

            text5.color = "black"
            text5.text = "A black text"
            text5.opacity = 80
            text5.border.borderWidth = 100
            text5.border.color = "red"
            text5.flags = PaintableText.PaintableTextAlignRight

            var text6 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(5, 0),
                        RootPaintableItemList.PaintableItemRole)

            text6.color = "red"
            text6.text = "A red text\n with 2 lines"
            text6.opacity = 20
            text6.background.color = "blue"
            text6.background.radius = 50
            text6.mode = PaintableRectangle.PaintableRectangleRelativeSize
            text6.flags = PaintableText.PaintableTextAlignRight
                    | PaintableText.PaintableTextAlignBottom

            var text7 = testDocumentTemplate.paintableItemList.data(
                        testDocumentTemplate.paintableItemList.index(6, 0),
                        RootPaintableItemList.PaintableItemRole)

            text7.text = "Last text"

            waitForRendering(editionArea)

            var targetSize = Qt.size(
                        contentContainer.width * root.outputImageSize.width
                        / contentContainer.height, root.outputImageSize.height)
            var qmlGrabedImage

            contentContainer.grabToImage(function (result) {
                qmlGrabedImage = result.image
            }, targetSize)

            tryVerify(function () {
                return qmlGrabedImage !== undefined
            })

            var paintedImage = testPainter.paint(testDocumentTemplate,
                                                 targetSize)

            if (!testPainter.approximativeImagesComparison(
                        qmlGrabedImage, paintedImage, 5,
                        ["black", "white", "red", "blue", "yellow", "pink"])) {
                testPainter.saveQImage(qmlGrabedImage, "qmlGrabedImage.png")
                testPainter.saveQImage(paintedImage, "paintedImage.png")
                fail("qmlGrabedImage and paintedImage are different")
            }

            editionArea.model = undefined
            testDocumentTemplate.paintableItemList.clearPaintableItemList()
            editionArea.model = paintableItemListModel
        }
    }
}
