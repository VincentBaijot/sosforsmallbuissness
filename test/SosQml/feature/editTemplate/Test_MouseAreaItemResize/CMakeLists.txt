cmake_minimum_required(VERSION 3.20)

print_location()

enable_testing()

add_definitions(-DQUICK_TEST_SOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}")

set(TEST_NAME Test_MouseCornerItemResize)

qt_add_executable(${TEST_NAME} Test_MouseAreaItemResize.cpp tst_MouseAreaItemResize.qml)

add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})

target_link_libraries(${TEST_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Qml Qt${QT_VERSION_MAJOR}::Quick Qt${QT_VERSION_MAJOR}::QuickControls2 Qt${QT_VERSION_MAJOR}::QuickTest)
target_link_libraries(${TEST_NAME} PRIVATE
    Utils Logger Painter DocumentTemplate
    SosQmlData SosQmlControls SosQmlFeatures SosQmlFeaturesEditTemplate)

