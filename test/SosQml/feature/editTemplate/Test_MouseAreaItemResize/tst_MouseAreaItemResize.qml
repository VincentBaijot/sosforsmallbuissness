import QtQuick
import QtTest

import SosQml.Features.EditTemplate
import SosForSmallBuissness.Painter

Item {
    id: root

    width: 500
    height: 500

    PaintableRectangle{
        id: paintableRectangle

        x:200
        y:200
        width: 100
        height: 100
    }

    Item {
        id: itemRectangle

        x: paintableRectangle.x
        y: paintableRectangle.y
        width: paintableRectangle.width
        height: paintableRectangle.height

        MouseAreaItemResize {
            id: topLeftCornerMouseArea

            anchors{
                top: itemRectangle.top
                left: itemRectangle.left
            }

            width: 10
            height: 10

            verticalResize: MouseAreaItemResize.TopResize
            horizontalResize: MouseAreaItemResize.LeftResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: topRightCornerMouseArea

            anchors{
                top: itemRectangle.top
                right: itemRectangle.right
            }

            width: 10
            height: 10

            verticalResize: MouseAreaItemResize.TopResize
            horizontalResize: MouseAreaItemResize.RightResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: bottomLeftCornerMouseArea

            anchors{
                bottom: itemRectangle.bottom
                left: itemRectangle.left
            }

            width: 10
            height: 10

            verticalResize: MouseAreaItemResize.BottomResize
            horizontalResize: MouseAreaItemResize.LeftResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: bottomRightCornerMouseArea

            anchors{
                bottom: itemRectangle.bottom
                right: itemRectangle.right
            }

            width: 10
            height: 10

            verticalResize: MouseAreaItemResize.BottomResize
            horizontalResize: MouseAreaItemResize.RightResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: topBorderMouseArea

            anchors{
                top: itemRectangle.top
                left: topLeftCornerMouseArea.right
                right : topRightCornerMouseArea.left
                bottom: topLeftCornerMouseArea.bottom
            }

            verticalResize: MouseAreaItemResize.TopResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: leftBorderMouseArea

            anchors{
                top: topLeftCornerMouseArea.bottom
                left: itemRectangle.left
                right : topLeftCornerMouseArea.right
                bottom: bottomLeftCornerMouseArea.top
            }

            horizontalResize: MouseAreaItemResize.LeftResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: bottomBorderMouseArea

            anchors{
                top: bottomLeftCornerMouseArea.top
                left: bottomLeftCornerMouseArea.right
                right : bottomRightCornerMouseArea.left
                bottom: itemRectangle.bottom
            }

            verticalResize: MouseAreaItemResize.BottomResize

            referenceItem: root
            positionableItem: paintableRectangle
        }

        MouseAreaItemResize {
            id: rightBorderMouseArea

            anchors{
                top: topRightCornerMouseArea.bottom
                left: topRightCornerMouseArea.left
                right : itemRectangle.right
                bottom: bottomRightCornerMouseArea.top
            }

            horizontalResize: MouseAreaItemResize.RightResize

            referenceItem: root
            positionableItem: paintableRectangle
        }
    }

    TestCase {

        name: "MouseAreaItemResize"
        when: windowShown

        function test_topLeftCorner() {
            resetPaintableRectangle()

            compare(topLeftCornerMouseArea.cursorShape, Qt.SizeFDiagCursor)

            var topLeftCornerMouseAreaCenter = root.mapFromItem(topLeftCornerMouseArea, topLeftCornerMouseArea.width/2,topLeftCornerMouseArea.height/2)
            mousePress(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // *          -----------
            // *          |         |
            // *          |         |
            // *          -----------
            // **********************
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x - 50, topLeftCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |          **********|
            // |          *         |
            // |          *         |
            // |          **********|
            // ----------------------
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x + 50, topLeftCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       ************
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x + 150, topLeftCornerMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than 0,0

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // *          -----------
            // *          |         |
            // *          |         |
            // *          -----------
            // **********************
            //

            mouseMove(root, -20, -50)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than width, height

            //
            // Goes from ---- to ****
            //
            // ------------
            // |          |
            // |          |
            // |          |
            // ------------********************
            //             *                  *
            //             *                  *
            //             *                  *
            //             *                  *
            //             *                  *
            //             ********************
            //

            mouseMove(root, 520, 530)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)

            topLeftCornerMouseAreaCenter = root.mapFromItem(topLeftCornerMouseArea, topLeftCornerMouseArea.width/2-5,topLeftCornerMouseArea.height/2-5)
            mousePress(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // *          -----------
            // *          |         |
            // *          |         |
            // *          -----------
            // **********************
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x - 50, topLeftCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |          **********|
            // |          *         |
            // |          *         |
            // |          **********|
            // ----------------------
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x + 50, topLeftCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       ************
            //

            mouseMove(root, topLeftCornerMouseAreaCenter.x + 150, topLeftCornerMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than 0,0

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // *          -----------
            // *          |         |
            // *          |         |
            // *          -----------
            // **********************
            //

            mouseMove(root, -20, -50)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than width, height

            //
            // Goes from ---- to ****
            //
            // ------------
            // |          |
            // |          |
            // |          |
            // ------------********************
            //             *                  *
            //             *                  *
            //             *                  *
            //             *                  *
            //             *                  *
            //             ********************
            //

            mouseMove(root, 520, 530)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topLeftCornerMouseAreaCenter.x, topLeftCornerMouseAreaCenter.y)
        }

        function test_topRightCorner() {
            resetPaintableRectangle()

            compare(topRightCornerMouseArea.cursorShape, Qt.SizeBDiagCursor)

            var topRightCornerMouseAreaCenter = root.mapFromItem(topRightCornerMouseArea, topRightCornerMouseArea.width/2,topRightCornerMouseArea.height/2)
            mousePress(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // -----------          *
            // |         |          *
            // |         |          *
            // -----------          *
            // **********************
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x + 50, topRightCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // ***********          |
            // *         *          |
            // *         *          |
            // ***********          |
            // ----------------------
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x - 50, topRightCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //             ----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            // ************----------------------
            // *          *
            // *          *
            // *          *
            // ************
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x - 150, topRightCornerMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than width,0
            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // -----------          *
            // |         |          *
            // |         |          *
            // -----------          *
            // **********************
            //

            mouseMove(root, 560, -30)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than 0,height
            //
            // Goes from ---- to ****
            //
            //             ----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            // ************----------------------
            // *          *
            // *          *
            // *          *
            // ************
            //

            mouseMove(root, -15, 580)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            mouseMove(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)

            topRightCornerMouseAreaCenter = root.mapFromItem(topRightCornerMouseArea, topRightCornerMouseArea.width/2-5,topRightCornerMouseArea.height/2-5)
            mousePress(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // -----------          *
            // |         |          *
            // |         |          *
            // -----------          *
            // **********************
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x + 50, topRightCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // ***********          |
            // *         *          |
            // *         *          |
            // ***********          |
            // ----------------------
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x - 50, topRightCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //             ----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            // ************----------------------
            // *          *
            // *          *
            // *          *
            // ************
            //

            mouseMove(root, topRightCornerMouseAreaCenter.x - 150, topRightCornerMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than width,0
            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // -----------          *
            // |         |          *
            // |         |          *
            // -----------          *
            // **********************
            //

            mouseMove(root, 560, -30)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than 0,height
            //
            // Goes from ---- to ****
            //
            //             ----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            // ************----------------------
            // *          *
            // *          *
            // *          *
            // ************
            //

            mouseMove(root, -15, 580)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            mouseMove(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topRightCornerMouseAreaCenter.x, topRightCornerMouseAreaCenter.y)
        }

        function test_bottomLeftCorner() {
            resetPaintableRectangle()

            compare(bottomLeftCornerMouseArea.cursorShape, Qt.SizeBDiagCursor)

            var bottomLeftCornerMouseAreaCenter = root.mapFromItem(bottomLeftCornerMouseArea, bottomLeftCornerMouseArea.width/2,bottomLeftCornerMouseArea.height/2)
            mousePress(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ***********------------
            // *          |         |
            // *          |         |
            // *          -----------
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x - 50, bottomLeftCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |            *       |
            // |            *       |
            // |            *       |
            // |            ********|
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x + 50, bottomLeftCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //                       ************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       *          *
            // ----------------------************
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x + 150, bottomLeftCornerMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than 0,height

            //
            // Goes from ---- to ****
            //
            // ***********------------
            // *          |         |
            // *          |         |
            // *          -----------
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, -60, 600)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than width, 0

            //
            // Goes from ---- to ****
            //
            //                       ************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       *          *
            // ----------------------************
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, 550, -60)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)

            bottomLeftCornerMouseAreaCenter = root.mapFromItem(bottomLeftCornerMouseArea, bottomLeftCornerMouseArea.width/2-5,bottomLeftCornerMouseArea.height/2-5)
            mousePress(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ***********------------
            // *          |         |
            // *          |         |
            // *          -----------
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x - 50, bottomLeftCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |            *       |
            // |            *       |
            // |            *       |
            // |            ********|
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x + 50, bottomLeftCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //                       ************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       *          *
            // ----------------------************
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x + 150, bottomLeftCornerMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Move to a position further than 0,height

            //
            // Goes from ---- to ****
            //
            // ***********------------
            // *          |         |
            // *          |         |
            // *          -----------
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, -60, 600)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Move to a position further than width, 0

            //
            // Goes from ---- to ****
            //
            //                       ************
            //                       *          *
            //                       *          *
            //                       *          *
            //                       *          *
            // ----------------------************
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, 550, -60)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomLeftCornerMouseAreaCenter.x, bottomLeftCornerMouseAreaCenter.y)
        }

        function test_bottomRightCorner() {
            resetPaintableRectangle()

            compare(topLeftCornerMouseArea.cursorShape, Qt.SizeFDiagCursor)

            var bottomRightCornerMouseAreaCenter = root.mapFromItem(bottomRightCornerMouseArea, bottomRightCornerMouseArea.width/2,bottomRightCornerMouseArea.height/2)
            mousePress(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ------------**********
            // |         |          *
            // |         |          *
            // -----------          *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x + 50, bottomRightCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |       *            |
            // |       *            |
            // |       *            |
            // |********            |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x - 50, bottomRightCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            // ************
            // *          *
            // *          *
            // *          *
            // *          *
            // ************----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             ----------------------
            //
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x - 150, bottomRightCornerMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Go further than width, height
            //
            // Goes from ---- to ****
            //
            // ------------**********
            // |         |          *
            // |         |          *
            // -----------          *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, 900, 540)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Go further than 0, 0

            //
            // Goes from ---- to ****
            //
            // ************
            // *          *
            // *          *
            // *          *
            // *          *
            // ************----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             ----------------------
            //
            //

            mouseMove(root, -142, -42)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)

            bottomRightCornerMouseAreaCenter = root.mapFromItem(bottomRightCornerMouseArea, bottomRightCornerMouseArea.width/2-5,bottomRightCornerMouseArea.height/2-5)
            mousePress(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ------------**********
            // |         |          *
            // |         |          *
            // -----------          *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x + 50, bottomRightCornerMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |       *            |
            // |       *            |
            // |       *            |
            // |********            |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x - 50, bottomRightCornerMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            // ************
            // *          *
            // *          *
            // *          *
            // *          *
            // ************----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             ----------------------
            //
            //

            mouseMove(root, bottomRightCornerMouseAreaCenter.x - 150, bottomRightCornerMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 50)

            // Go further than width, height
            //
            // Goes from ---- to ****
            //
            // ------------**********
            // |         |          *
            // |         |          *
            // -----------          *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, 900, 540)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 300)

            // Go further than 0, 0

            //
            // Goes from ---- to ****
            //
            // ************
            // *          *
            // *          *
            // *          *
            // *          *
            // ************----------------------
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             |                    |
            //             ----------------------
            //
            //

            mouseMove(root, -142, -42)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomRightCornerMouseAreaCenter.x, bottomRightCornerMouseAreaCenter.y)
        }

        function test_topBorder() {
            resetPaintableRectangle()

            compare(topBorderMouseArea.cursorShape, Qt.SizeVerCursor)

            var topBorderMouseAreaCenter = root.mapFromItem(topBorderMouseArea, topBorderMouseArea.width/2,topBorderMouseArea.height/2)
            mousePress(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            //

            mouseMove(root, topBorderMouseAreaCenter.x - 50, topBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |********************|
            // |*                  *|
            // |*                  *|
            // |********************|
            // ----------------------
            //

            mouseMove(root, topBorderMouseAreaCenter.x + 50, topBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //
            //

            mouseMove(root, topBorderMouseAreaCenter.x + 150, topBorderMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            // Go further than 0 on y

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            //

            mouseMove(root, -50, -100)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 300)

            // Go further than height on y

            //
            // Goes from ---- to ****
            //
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //
            //

            mouseMove(root, -100, 600)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)

            topBorderMouseAreaCenter = root.mapFromItem(topBorderMouseArea, topBorderMouseArea.width/2-5,topBorderMouseArea.height/2-5)
            mousePress(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            //

            mouseMove(root, topBorderMouseAreaCenter.x - 50, topBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |********************|
            // |*                  *|
            // |*                  *|
            // |********************|
            // ----------------------
            //

            mouseMove(root, topBorderMouseAreaCenter.x + 50, topBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 250)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //
            //

            mouseMove(root, topBorderMouseAreaCenter.x + 150, topBorderMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            // Go further than 0 on y

            //
            // Goes from ---- to ****
            //
            // **********************
            // *                    *
            // *                    *
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            //

            mouseMove(root, -50, -100)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 300)

            // Go further than height on y

            //
            // Goes from ---- to ****
            //
            //
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            //
            //

            mouseMove(root, -100, 600)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 300)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, topBorderMouseAreaCenter.x, topBorderMouseAreaCenter.y)
        }

        function test_leftBorder() {
            resetPaintableRectangle()

            compare(leftBorderMouseArea.cursorShape, Qt.SizeHorCursor)

            var leftBorderMouseAreaCenter = root.mapFromItem(leftBorderMouseArea, leftBorderMouseArea.width/2,leftBorderMouseArea.height/2)
            mousePress(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // ***********-----------
            //

            mouseMove(root, leftBorderMouseAreaCenter.x - 50, leftBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // -----------***********
            //

            mouseMove(root, leftBorderMouseAreaCenter.x + 50, leftBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ----------------------************
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // ----------------------************
            //
            //

            mouseMove(root, leftBorderMouseAreaCenter.x + 150, leftBorderMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            // Go further than 0 on x

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // ***********-----------
            //

            mouseMove(root, -78, -90)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 100)

            // Go furhter than width on x

            //
            // Goes from ---- to ****
            //
            // ----------------------************
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // ----------------------************
            //
            //

            mouseMove(root, 888, 666)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 100)

            // Come back to initial position

            mouseMove(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)

            leftBorderMouseAreaCenter = root.mapFromItem(leftBorderMouseArea, leftBorderMouseArea.width/2-5,leftBorderMouseArea.height/2-5)
            mousePress(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // ***********-----------
            //

            mouseMove(root, leftBorderMouseAreaCenter.x - 50, leftBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // |          *         *
            // -----------***********
            //

            mouseMove(root, leftBorderMouseAreaCenter.x + 50, leftBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 250)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ----------------------************
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // ----------------------************
            //
            //

            mouseMove(root, leftBorderMouseAreaCenter.x + 150, leftBorderMouseAreaCenter.y + 150)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            // Go further than 0 on x

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // *          |         |
            // ***********-----------
            //

            mouseMove(root, -78, -90)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 100)

            // Go further than width on x

            //
            // Goes from ---- to ****
            //
            // ----------------------************
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // |                    |           *
            // ----------------------************
            //
            //

            mouseMove(root, 888, 666)

            compare(paintableRectangle.x, 300)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 100)

            // Come back to initial position

            mouseMove(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, leftBorderMouseAreaCenter.x, leftBorderMouseAreaCenter.y)
        }

        function test_bottomBorder() {
            resetPaintableRectangle()

            compare(bottomBorderMouseArea.cursorShape, Qt.SizeVerCursor)

            var bottomBorderMouseAreaCenter = root.mapFromItem(bottomBorderMouseArea, bottomBorderMouseArea.width/2,bottomBorderMouseArea.height/2)
            mousePress(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x + 50, bottomBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |********************|
            // |*                  *|
            // |*                  *|
            // |********************|
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x - 50, bottomBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x - 150, bottomBorderMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            // Go further than height on y

            //
            // Goes from ---- to ****
            //
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, 0, 777)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 300)

            // Go further than 0 on y

            // Goes from ---- to ****
            //
            //
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //
            //

            mouseMove(root, -65, -80)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)

            bottomBorderMouseAreaCenter = root.mapFromItem(bottomBorderMouseArea, bottomBorderMouseArea.width/2-5,bottomBorderMouseArea.height/2-5)
            mousePress(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x + 50, bottomBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 150)

            //
            // Goes from ---- to ****
            //
            // ----------------------
            // |********************|
            // |*                  *|
            // |*                  *|
            // |********************|
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x - 50, bottomBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x - 150, bottomBorderMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            //
            // Goes from ---- to ****
            //
            //
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //
            //

            mouseMove(root, bottomBorderMouseAreaCenter.x - 150, bottomBorderMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 150)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 50)

            // Go further than height on y

            //
            // Goes from ---- to ****
            //
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // ----------------------
            // *                    *
            // *                    *
            // **********************
            //

            mouseMove(root, 0, 777)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 300)

            // Go further than 0 on y

            // Goes from ---- to ****
            //
            //
            // **********************
            // *                    *
            // *                    *
            // *                    *
            // *                    *
            // **********************
            // ----------------------
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // |                    |
            // ----------------------
            //
            //

            mouseMove(root, -65, -80)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 0)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 200)

            // Come back to initial position

            mouseMove(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, bottomBorderMouseAreaCenter.x, bottomBorderMouseAreaCenter.y)
        }

        function test_rightBorder() {
            resetPaintableRectangle()

            compare(rightBorderMouseArea.cursorShape, Qt.SizeHorCursor)

            var rightBorderMouseAreaCenter = root.mapFromItem(rightBorderMouseArea, rightBorderMouseArea.width/2,rightBorderMouseArea.height/2)
            mousePress(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // -----------***********
            //

            mouseMove(root, rightBorderMouseAreaCenter.x + 50, rightBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // ***********-----------
            //

            mouseMove(root, rightBorderMouseAreaCenter.x - 50, rightBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ************----------------------
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // ************----------------------
            //
            //

            mouseMove(root, rightBorderMouseAreaCenter.x - 150, rightBorderMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            // Go futher than width on x
            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // -----------***********
            //

            mouseMove(root, 3333, -500)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 100)

            // Go futher than 0 on x

            //
            // Goes from ---- to ****
            //
            // ************----------------------
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // ************----------------------
            //
            //

            mouseMove(root, -450, 578)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 100)

            // Come back to initial position

            mouseMove(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)

            rightBorderMouseAreaCenter = root.mapFromItem(rightBorderMouseArea, rightBorderMouseArea.width/2-5,rightBorderMouseArea.height/2-5)
            mousePress(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)

            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // -----------***********
            //

            mouseMove(root, rightBorderMouseAreaCenter.x + 50, rightBorderMouseAreaCenter.y + 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 150)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ***********-----------
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // *         *          |
            // ***********-----------
            //

            mouseMove(root, rightBorderMouseAreaCenter.x - 50, rightBorderMouseAreaCenter.y - 50)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            //
            // Goes from ---- to ****
            //
            // ************----------------------
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // ************----------------------
            //
            //

            mouseMove(root, rightBorderMouseAreaCenter.x - 150, rightBorderMouseAreaCenter.y - 150)

            compare(paintableRectangle.x, 150)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 50)
            compare(paintableRectangle.height, 100)

            // Go futher than width on x
            //
            // Goes from ---- to ****
            //
            // -----------***********
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // |         |          *
            // -----------***********
            //

            mouseMove(root, 3333, -500)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 300)
            compare(paintableRectangle.height, 100)

            // Go futher than 0 on x

            //
            // Goes from ---- to ****
            //
            // ************----------------------
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // *           |                    |
            // ************----------------------
            //
            //

            mouseMove(root, -450, 578)

            compare(paintableRectangle.x, 0)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 200)
            compare(paintableRectangle.height, 100)

            // Come back to initial position

            mouseMove(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)

            compare(paintableRectangle.x, 200)
            compare(paintableRectangle.y, 200)
            compare(paintableRectangle.width, 100)
            compare(paintableRectangle.height, 100)

            mouseRelease(root, rightBorderMouseAreaCenter.x, rightBorderMouseAreaCenter.y)
        }

        function resetPaintableRectangle() {
            paintableRectangle.x = 200
            paintableRectangle.y = 200
            paintableRectangle.width = 100
            paintableRectangle.height = 100
        }
    }
}
