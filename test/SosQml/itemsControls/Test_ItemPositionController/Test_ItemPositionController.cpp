#include <QQmlEngine>
#include <QtQuickTest>

#include "SosQml/QmlPluginInclude.hpp"

class Test_ItemPositionControllerSetup : public QObject
{
    Q_OBJECT

  public:
  public slots:
    void qmlEngineAvailable(QQmlEngine* engine)
    {
        engine->addImportPath(SosQmlPluginPath);
        engine->addImportPath(RootQmlPluginPath);
        engine->addPluginPath(SosQmlPluginPath);
    }
};

QUICK_TEST_MAIN_WITH_SETUP(Test_ItemPositionController, Test_ItemPositionControllerSetup)

#include "Test_ItemPositionController.moc"
