import QtQuick
import QtQuick.Controls
import QtTest

import SosQml.ItemsControls
import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate

TestCase {

        name: "ItemPositionController"

        ItemPositionController{
            id: itemPositionController

            property PaintableRectangle paintableRectangle : PaintableRectangle {
                id: paintableRectangle

                x: 42
                y: 95
                width:  200
                height: 150
            }

            itemX: paintableRectangle.x
            itemY: paintableRectangle.y
            itemWidth: paintableRectangle.width
            itemHeight: paintableRectangle.height

            onItemXUpdated: (newItemX) => {
                paintableRectangle.x = newItemX
            }

            onItemYUpdated: (newItemY) => {
                paintableRectangle.y = newItemY
            }

            onItemWidthUpdated: (newItemWidth) => {
                paintableRectangle.width = newItemWidth
            }

            onItemHeightUpdated: (newItemHeight) => {
                paintableRectangle.height = newItemHeight
            }

            pageSize: PageSize {
                id: pageSize
            }
        }

        function test_xSpinBox() {
            var xSpinBox = findChild(itemPositionController, "xSpinBox")
            verify(xSpinBox)

            compare(xSpinBox.fromPixel, 0)
            compare(xSpinBox.toPixel, pageSize.sizePixel.width - paintableRectangle.width)
            compare(xSpinBox.pixelValue, paintableRectangle.x)

            xSpinBox.pixelValueModified(123)

            compare(paintableRectangle.x , 123)
        }

        function test_ySpinBox() {
            var ySpinBox = findChild(itemPositionController, "ySpinBox")
            verify(ySpinBox)

            compare(ySpinBox.fromPixel, 0)
            compare(ySpinBox.toPixel, pageSize.sizePixel.height - paintableRectangle.height)
            compare(ySpinBox.pixelValue, paintableRectangle.y)

            ySpinBox.pixelValueModified(45)

            compare(paintableRectangle.y , 45)
        }

        function test_widthSpinBox() {
            var widthSpinBox = findChild(itemPositionController, "widthSpinBox")
            verify(widthSpinBox)

            compare(widthSpinBox.fromPixel, 0)
            compare(widthSpinBox.toPixel, pageSize.sizePixel.width - paintableRectangle.x)
            compare(widthSpinBox.pixelValue, paintableRectangle.width)

            widthSpinBox.pixelValueModified(94)

            compare(paintableRectangle.width , 94)
        }

        function test_heightSpinBox() {
            var heightSpinBox = findChild(itemPositionController, "heightSpinBox")
            verify(heightSpinBox)

            compare(heightSpinBox.fromPixel, 0)
            compare(heightSpinBox.toPixel, pageSize.sizePixel.height - paintableRectangle.y)
            compare(heightSpinBox.pixelValue, paintableRectangle.height)

            heightSpinBox.pixelValueModified(67)

            compare(paintableRectangle.height , 67)
        }
}
