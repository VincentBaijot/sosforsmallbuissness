#include <QSignalSpy>
#include <QTest>

#include "SosQml/ItemsControls/PageSizeControlProxy.hpp"

class Test_PageSizeControlProxy : public QObject
{
    Q_OBJECT

  public:
    Test_PageSizeControlProxy() = default;

  private slots:
    void noPageSize();
    void initialValue();
    void setUnit();
    void setUnitName();
    void setPageSizeId();
    void setPageSizeName();
    void setSize();
    void setResolution();

  private:
    void _initializeSpies(const itemscontrols::PageSizeControlProxy* templatePageSizeControlProxy);
    bool _compareProxyToPageSize(const itemscontrols::PageSizeControlProxy& pageSizeControlProxy,
                                 const documenttemplate::controller::PageSize& pageSize);

    QSharedPointer<QSignalSpy> _pageSizeIdChangedSpy;
    QSharedPointer<QSignalSpy> _resolutionChangedSpy;
    QSharedPointer<QSignalSpy> _sizeChangedSpy;
    QSharedPointer<QSignalSpy> _sizePixelChangedSpy;
    QSharedPointer<QSignalSpy> _unitChangedSpy;
    QSharedPointer<QSignalSpy> _unitPerPixelChangedSpy;
};

constexpr double unitPerPixelPrecision = 1000.0;

void Test_PageSizeControlProxy::noPageSize()
{
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;

    _initializeSpies(&pageSizeControlProxy);

    QCOMPARE(pageSizeControlProxy.pageSize(), nullptr);
    QCOMPARE(pageSizeControlProxy.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::Custom);
    QCOMPARE(pageSizeControlProxy.pageSizeName(), "");
    QCOMPARE(pageSizeControlProxy.resolution(), 0);
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSizeControlProxy.unitName(), QString());
    QCOMPARE(pageSizeControlProxy.size(), QSizeF());
    QCOMPARE(pageSizeControlProxy.sizePixel(), QSize());

    pageSizeControlProxy.setPageSize(nullptr);
    pageSizeControlProxy.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A4);
    pageSizeControlProxy.setPageSizeName("A5");
    pageSizeControlProxy.setResolution(1600);
    pageSizeControlProxy.setSize(QSizeF(40, 50));
    pageSizeControlProxy.setUnit(documenttemplate::data::PageSizeData::Unit::Didot);
    pageSizeControlProxy.setUnitName("Cicerot");

    QCOMPARE(pageSizeControlProxy.pageSize(), nullptr);
    QCOMPARE(pageSizeControlProxy.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::Custom);
    QCOMPARE(pageSizeControlProxy.pageSizeName(), "");
    QCOMPARE(pageSizeControlProxy.resolution(), 0);
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSizeControlProxy.unitName(), QString());
    QCOMPARE(pageSizeControlProxy.size(), QSizeF());
    QCOMPARE(pageSizeControlProxy.sizePixel(), QSize());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSizeControlProxy::initialValue()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    QCOMPARE(pageSizeControlProxy.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());
    QCOMPARE(pageSizeControlProxy.resolution(), 1200);
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(static_cast<int>(documenttemplate::data::PageSizeData::Unit::Millimeter),
             static_cast<int>(QPageSize::Millimeter));
    QCOMPARE(pageSizeControlProxy.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSizeControlProxy.sizePixel(), qPageSize.sizePixels(pageSizeControlProxy.resolution()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSizeControlProxy::setUnit()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setUnit(documenttemplate::data::PageSizeData::Unit::Millimeter);

    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());
    QCOMPARE(pageSizeControlProxy.unitName(), "Millimeter");

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    pageSizeControlProxy.setUnit(documenttemplate::data::PageSizeData::Unit::Point);

    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Point);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());
    QCOMPARE(pageSizeControlProxy.unitName(), "Point");

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 1);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    pageSizeControlProxy.setUnit(documenttemplate::data::PageSizeData::Unit::Millimeter);

    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());
    QCOMPARE(pageSizeControlProxy.unitName(), "Millimeter");

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 2);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 2);
}

void Test_PageSizeControlProxy::setUnitName()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setUnitName("Millimeter");

    QCOMPARE(pageSizeControlProxy.unitName(), "Millimeter");
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    pageSizeControlProxy.setUnitName("Point");

    QCOMPARE(pageSizeControlProxy.unitName(), "Point");
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Point);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Point);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 1);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    pageSizeControlProxy.setUnitName("whatever");

    QCOMPARE(pageSizeControlProxy.unitName(), "Millimeter");
    QCOMPARE(pageSizeControlProxy.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 2);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 2);
}

void Test_PageSizeControlProxy::setPageSizeId()
{
    QPageSize qPageSize(QPageSize::A2);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A2);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::A8);
    pageSizeControlProxy.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A8);

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 2);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSizeControlProxy::setPageSizeName()
{
    QPageSize qPageSize(QPageSize::A2);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setPageSizeName(qPageSize.name());

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));
    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::A8);
    pageSizeControlProxy.setPageSizeName(qPageSize.name());

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));
    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 2);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::Custom);
    pageSizeControlProxy.setPageSizeName("whatever");

    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));
    QCOMPARE(pageSizeControlProxy.pageSizeName(), qPageSize.name());

    QCOMPARE(_pageSizeIdChangedSpy->count(), 3);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 3);
    QCOMPARE(_sizePixelChangedSpy->count(), 3);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSizeControlProxy::setSize()
{
    QPageSize qPageSize(QPageSize::A2);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setPageSizeName(qPageSize.name());
    pageSizeControlProxy.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSizeControlProxy.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::A8);
    pageSizeControlProxy.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSizeControlProxy.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 2);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QSizeF(130, 32), QPageSize::Millimeter);
    pageSizeControlProxy.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSizeControlProxy.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 3);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 3);
    QCOMPARE(_sizePixelChangedSpy->count(), 3);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSizeControlProxy::setResolution()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;
    itemscontrols::PageSizeControlProxy pageSizeControlProxy;
    pageSizeControlProxy.setPageSize(&pageSize);

    _initializeSpies(&pageSizeControlProxy);

    pageSizeControlProxy.setPageSizeName(qPageSize.name());

    pageSizeControlProxy.setResolution(500);

    QCOMPARE(pageSizeControlProxy.resolution(), 500);
    QCOMPARE(pageSize.resolution(), 500);
    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 1);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    pageSizeControlProxy.setResolution(1300);

    QCOMPARE(pageSizeControlProxy.resolution(), 1300);
    QCOMPARE(pageSize.resolution(), 1300);
    QVERIFY(_compareProxyToPageSize(pageSizeControlProxy, pageSize));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 2);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 2);
}

void Test_PageSizeControlProxy::_initializeSpies(
  const itemscontrols::PageSizeControlProxy* templatePageSizeControlProxy)
{
    _pageSizeIdChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::pageSizeIdChanged));
    _resolutionChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::resolutionChanged));
    _sizeChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::sizeChanged));
    _sizePixelChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::sizePixelChanged));
    _unitChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::unitChanged));
    _unitPerPixelChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSizeControlProxy, &itemscontrols::PageSizeControlProxy::unitPerPixelChanged));
}

void compareProxyToPageSize(const itemscontrols::PageSizeControlProxy& pageSizeControlProxy,
                            const documenttemplate::controller::PageSize& pageSize,
                            bool& result)
{
    result = false;

    QCOMPARE(pageSizeControlProxy.pageSizeId(), pageSize.pageSizeId());
    QCOMPARE(pageSizeControlProxy.resolution(), pageSize.resolution());
    QCOMPARE(pageSizeControlProxy.unit(), pageSize.unit());
    QCOMPARE(pageSizeControlProxy.size(), pageSize.size());
    QCOMPARE(pageSizeControlProxy.sizePixel(), pageSize.sizePixel());
    QCOMPARE(pageSizeControlProxy.unitPerPixel(), pageSize.unitPerPixel());

    result = true;
}

bool Test_PageSizeControlProxy::_compareProxyToPageSize(
  const itemscontrols::PageSizeControlProxy& pageSizeControlProxy,
  const documenttemplate::controller::PageSize& pageSize)
{
    bool result = false;

    compareProxyToPageSize(pageSizeControlProxy, pageSize, result);

    return result;
}

QTEST_APPLESS_MAIN(Test_PageSizeControlProxy)

#include "Test_PageSizeControlProxy.moc"
