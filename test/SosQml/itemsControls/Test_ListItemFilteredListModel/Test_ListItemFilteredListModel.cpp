#include <QRectF>
#include <QSignalSpy>
#include <QTest>

#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "Painter/controllers/PaintableList.hpp"
#include "SosQml/ItemsControls/ListAssociationControllerProxyModel.hpp"

class Test_ListItemFilteredListModel : public QObject
{
    Q_OBJECT

  public:
    Test_ListItemFilteredListModel() = default;

  private slots:
    void synchronizationOfModel_setSourceListModel();
    void synchronizationOfModel_addItemsToSourceListModel();
    void synchronizationOfModel_removeItemsFromSourceListModel();
    void synchronizationOfModel_resetSourceListModel();

  private:
};

void Test_ListItemFilteredListModel::synchronizationOfModel_setSourceListModel()
{
    itemscontrols::ListAssociationControllerProxyModel listItemFilteredListModel;

    QSignalSpy listModelAboutToBeReset(&listItemFilteredListModel,
                                       &itemscontrols::ListAssociationControllerProxyModel::modelAboutToBeReset);
    QSignalSpy listModelReset(&listItemFilteredListModel,
                              &itemscontrols::ListAssociationControllerProxyModel::modelReset);
    QSignalSpy listModelRowsAboutToBeInserted(
      &listItemFilteredListModel, &itemscontrols::ListAssociationControllerProxyModel::rowsAboutToBeInserted);
    QSignalSpy listModelRowsInserted(&listItemFilteredListModel,
                                     &itemscontrols::ListAssociationControllerProxyModel::rowsInserted);

    // case not list
    {
        QCOMPARE(listItemFilteredListModel.sourceListModel(), nullptr);
        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(1)), QVariant());

        QCOMPARE(listModelAboutToBeReset.count(), 0);
        QCOMPARE(listModelReset.count(), 0);
        QCOMPARE(listModelRowsAboutToBeInserted.count(), 0);
        QCOMPARE(listModelRowsInserted.count(), 0);
    }
    // case set nullptr
    {
        listItemFilteredListModel.setSourceListModel(nullptr);
        QCOMPARE(listItemFilteredListModel.sourceListModel(), nullptr);
        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(1)), QVariant());

        QCOMPARE(listModelAboutToBeReset.count(), 0);
        QCOMPARE(listModelReset.count(), 0);
        QCOMPARE(listModelRowsAboutToBeInserted.count(), 0);
        QCOMPARE(listModelRowsInserted.count(), 0);
    }

    // Empty list
    {
        documenttemplate::controller::RootPaintableItemList rootPaintableItemList;
        listItemFilteredListModel.setSourceListModel(&rootPaintableItemList);
        QCOMPARE(listItemFilteredListModel.sourceListModel(), &rootPaintableItemList);
        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(1)), QVariant());

        QCOMPARE(listModelAboutToBeReset.count(), 1);
        QCOMPARE(listModelReset.count(), 1);
        QCOMPARE(listModelRowsAboutToBeInserted.count(), 0);
        QCOMPARE(listModelRowsInserted.count(), 0);

        listItemFilteredListModel.setSourceListModel(nullptr);
    }

    listModelAboutToBeReset.clear();
    listModelReset.clear();
    listModelRowsAboutToBeInserted.clear();
    listModelRowsInserted.clear();

    // List with elements
    {
        documenttemplate::controller::RootPaintableItemList rootPaintableItemList;

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(0, 0, 10, 10));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(50, 50, 20, 20));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType,
                                               QRectF(0, 0, 30, 30));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(100, 100, 100, 100));

        listItemFilteredListModel.setSourceListModel(&rootPaintableItemList);
        QCOMPARE(listItemFilteredListModel.sourceListModel(), &rootPaintableItemList);
        QCOMPARE(listItemFilteredListModel.rowCount(), 3);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(4));
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(2))),
                 rootPaintableItemList.at(6));

        QCOMPARE(listModelAboutToBeReset.count(), 1);
        QCOMPARE(listModelReset.count(), 1);
        QCOMPARE(listModelRowsAboutToBeInserted.count(), 1);
        QCOMPARE(listModelRowsInserted.count(), 1);

        QList<QVariant> rowsAboutToBeInsertedArguments = listModelRowsAboutToBeInserted.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsAboutToBeInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsAboutToBeInsertedArguments.at(1), 0);
        QCOMPARE(rowsAboutToBeInsertedArguments.at(2), 1);

        QList<QVariant> rowsInsertedArguments = listModelRowsInserted.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsInsertedArguments.at(1), 0);
        QCOMPARE(rowsInsertedArguments.at(2), 1);

        listItemFilteredListModel.setSourceListModel(nullptr);
    }
}

void Test_ListItemFilteredListModel::synchronizationOfModel_addItemsToSourceListModel()
{
    itemscontrols::ListAssociationControllerProxyModel listItemFilteredListModel;

    QSignalSpy listModelRowsAboutToBeInserted(
      &listItemFilteredListModel, &itemscontrols::ListAssociationControllerProxyModel::rowsAboutToBeInserted);
    QSignalSpy listModelRowsInserted(&listItemFilteredListModel,
                                     &itemscontrols::ListAssociationControllerProxyModel::rowsInserted);

    documenttemplate::controller::RootPaintableItemList rootPaintableItemList;
    listItemFilteredListModel.setSourceListModel(&rootPaintableItemList);

    // Empty list
    {
        QCOMPARE(listItemFilteredListModel.sourceListModel(), &rootPaintableItemList);
        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(1)), QVariant());

        QCOMPARE(listModelRowsAboutToBeInserted.count(), 0);
        QCOMPARE(listModelRowsInserted.count(), 0);
    }

    // Add element to the list
    {
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(0, 0, 10, 10));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(50, 50, 20, 20));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType,
                                               QRectF(0, 0, 30, 30));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        // Before insertion of list

        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());

        QCOMPARE(listModelRowsAboutToBeInserted.count(), 0);
        QCOMPARE(listModelRowsInserted.count(), 0);

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(60, 60, 60, 60));

        // After insertion of list

        QCOMPARE(listItemFilteredListModel.rowCount(), 2);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(4));

        QCOMPARE(listModelRowsAboutToBeInserted.count(), 1);
        QCOMPARE(listModelRowsInserted.count(), 1);

        QList<QVariant> rowsAboutToBeInsertedArguments = listModelRowsAboutToBeInserted.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsAboutToBeInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsAboutToBeInsertedArguments.at(1), 1);
        QCOMPARE(rowsAboutToBeInsertedArguments.at(2), 1);

        QList<QVariant> rowsInsertedArguments = listModelRowsInserted.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsInsertedArguments.at(1), 1);
        QCOMPARE(rowsInsertedArguments.at(2), 1);

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        // Insertion of second list

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(100, 100, 100, 100));

        QCOMPARE(listItemFilteredListModel.rowCount(), 3);
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(2))),
                 rootPaintableItemList.at(6));

        QCOMPARE(listModelRowsAboutToBeInserted.count(), 2);
        QCOMPARE(listModelRowsInserted.count(), 2);

        rowsAboutToBeInsertedArguments = listModelRowsAboutToBeInserted.at(1);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsAboutToBeInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsAboutToBeInsertedArguments.at(1), 2);
        QCOMPARE(rowsAboutToBeInsertedArguments.at(2), 2);

        rowsInsertedArguments = listModelRowsInserted.at(1);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsInsertedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsInsertedArguments.at(1), 2);
        QCOMPARE(rowsInsertedArguments.at(2), 2);
    }
}

void Test_ListItemFilteredListModel::synchronizationOfModel_removeItemsFromSourceListModel()
{
    itemscontrols::ListAssociationControllerProxyModel listItemFilteredListModel;

    QSignalSpy listModelRowsAboutToBeRemoved(
      &listItemFilteredListModel, &itemscontrols::ListAssociationControllerProxyModel::rowsAboutToBeRemoved);
    QSignalSpy listModelRowsRemoved(&listItemFilteredListModel,
                                    &itemscontrols::ListAssociationControllerProxyModel::rowsRemoved);

    documenttemplate::controller::RootPaintableItemList rootPaintableItemList;
    listItemFilteredListModel.setSourceListModel(&rootPaintableItemList);

    {
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(0, 0, 10, 10));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(50, 50, 20, 20));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType,
                                               QRectF(0, 0, 30, 30));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(100, 100, 100, 100));

        QCOMPARE(listItemFilteredListModel.rowCount(), 3);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(4));
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(2))),
                 rootPaintableItemList.at(6));
        QCOMPARE(listModelRowsAboutToBeRemoved.size(), 0);
        QCOMPARE(listModelRowsRemoved.size(), 0);

        rootPaintableItemList.removePaintableItemAt(0);

        QCOMPARE(listItemFilteredListModel.rowCount(), 3);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(3));
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(2))),
                 rootPaintableItemList.at(5));
        QCOMPARE(listModelRowsAboutToBeRemoved.size(), 0);
        QCOMPARE(listModelRowsRemoved.size(), 0);

        rootPaintableItemList.removePaintableItemAt(3);

        QCOMPARE(listItemFilteredListModel.rowCount(), 2);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(4));
        QCOMPARE(listModelRowsAboutToBeRemoved.size(), 1);
        QCOMPARE(listModelRowsRemoved.size(), 1);

        QList<QVariant> rowsAboutToBeRemovedArguments = listModelRowsAboutToBeRemoved.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsAboutToBeRemovedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsAboutToBeRemovedArguments.at(1), 1);
        QCOMPARE(rowsAboutToBeRemovedArguments.at(2), 1);

        QList<QVariant> rowsRemovedArguments = listModelRowsRemoved.at(0);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsRemovedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsRemovedArguments.at(1), 1);
        QCOMPARE(rowsRemovedArguments.at(2), 1);

        rootPaintableItemList.removePaintableItemAt(3);

        QCOMPARE(listItemFilteredListModel.rowCount(), 2);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(3));
        QCOMPARE(listModelRowsAboutToBeRemoved.size(), 1);
        QCOMPARE(listModelRowsRemoved.size(), 1);

        rootPaintableItemList.removePaintableItemAt(3);

        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(listModelRowsAboutToBeRemoved.size(), 2);
        QCOMPARE(listModelRowsRemoved.size(), 2);

        rowsAboutToBeRemovedArguments = listModelRowsAboutToBeRemoved.at(1);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsAboutToBeRemovedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsAboutToBeRemovedArguments.at(1), 1);
        QCOMPARE(rowsAboutToBeRemovedArguments.at(2), 1);

        rowsRemovedArguments = listModelRowsRemoved.at(1);
        QCOMPARE(qvariant_cast<QModelIndex>(rowsRemovedArguments.at(0)), QModelIndex());
        QCOMPARE(rowsRemovedArguments.at(1), 1);
        QCOMPARE(rowsRemovedArguments.at(2), 1);
    }
}

void Test_ListItemFilteredListModel::synchronizationOfModel_resetSourceListModel()
{
    itemscontrols::ListAssociationControllerProxyModel listItemFilteredListModel;

    documenttemplate::controller::RootPaintableItemList rootPaintableItemList;
    listItemFilteredListModel.setSourceListModel(&rootPaintableItemList);

    QSignalSpy listModelAboutToBeReset(&listItemFilteredListModel,
                                       &itemscontrols::ListAssociationControllerProxyModel::modelAboutToBeReset);
    QSignalSpy listModelReset(&listItemFilteredListModel,
                              &itemscontrols::ListAssociationControllerProxyModel::modelReset);

    {
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(0, 0, 10, 10));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(50, 50, 20, 20));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType,
                                               QRectF(0, 0, 30, 30));
        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType,
                                               QRectF(60, 60, 60, 60));

        rootPaintableItemList.addPaintableItem(painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType,
                                               QRectF(100, 100, 100, 100));

        QCOMPARE(listItemFilteredListModel.rowCount(), 3);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(1))),
                 rootPaintableItemList.at(4));
        QCOMPARE(qvariant_cast<painter::controller::PaintableList*>(
                   listItemFilteredListModel.data(listItemFilteredListModel.index(2))),
                 rootPaintableItemList.at(6));
        QCOMPARE(listModelAboutToBeReset.size(), 0);
        QCOMPARE(listModelReset.size(), 0);

        rootPaintableItemList.clearPaintableItemList();

        QCOMPARE(listModelAboutToBeReset.size(), 1);
        QCOMPARE(listModelReset.size(), 1);

        QCOMPARE(listItemFilteredListModel.rowCount(), 1);
        QCOMPARE(listItemFilteredListModel.data(listItemFilteredListModel.index(0)), QVariant());
    }
}

QTEST_APPLESS_MAIN(Test_ListItemFilteredListModel)

#include "Test_ListItemFilteredListModel.moc"
