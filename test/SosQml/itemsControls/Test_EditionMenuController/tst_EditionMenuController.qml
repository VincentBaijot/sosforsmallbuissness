import QtQuick
import QtQuick.Controls
import QtTest

import SosQml.ItemsControls
import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation

TestCase {
    id: testCaseItem
    name: "EditionMenuController"

    property InterfaceQmlPaintableItem selectedPaintableItem

    DocumentTemplate {
        id: testDocumentTemplate
    }

    DataTemplateAssociation {
        id: dataDocumentAssociation
    }

    EditionMenuController {
        id: testEditionMenuController

        selectedPaintableItem: testCaseItem.selectedPaintableItem
        pageSize: testDocumentTemplate.pageSize
        rootPaintableItemList: testDocumentTemplate.paintableItemList
        dataDocumentAssociation: dataDocumentAssociation

        width: 500
        height: 500

        onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                            testCaseItem.selectedPaintableItem
                                            = newSelectedPaintableItem
                                        }
    }

    function initialiseDocumentTemplate() {
        testDocumentTemplate.paintableItemList.clearPaintableItemList()

        var rectanglePosition = Qt.rect(0, 0, 50, 50)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.RectangleItemType,
                    rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.TextItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.ListItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(2, 0),
                    RootPaintableItemList.PaintableItemRole).itemName = "a"

        rectanglePosition = Qt.rect(0, 0, 100, 50)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.RectangleItemType,
                    rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.TextItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.ListItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(5, 0),
                    RootPaintableItemList.PaintableItemRole).itemName = "b"

        rectanglePosition = Qt.rect(100, 100, 50, 50)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.RectangleItemType,
                    rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.TextItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.addPaintableItem(
                    InterfaceQmlPaintableItem.ListItemType, rectanglePosition)

        testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(8, 0),
                    RootPaintableItemList.PaintableItemRole).itemName = "c"
    }

    function test_loadingOfController() {
        initialiseDocumentTemplate()

        tryCompare(testCaseItem, "selectedPaintableItem", null)

        var editionMenuLoader = findChild(testEditionMenuController,
                                          "editionMenuControllerLoader")
        verify(editionMenuLoader !== null)

        verify(editionMenuLoader.item instanceof PageSizeController)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(0, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(editionMenuLoader.item instanceof RectangleController)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(1, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(editionMenuLoader.item instanceof TextController)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(2, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(editionMenuLoader.item instanceof ListController)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(editionMenuLoader.item instanceof RectangleController)

        testCaseItem.selectedPaintableItem = null

        verify(editionMenuLoader.item instanceof PageSizeController)
    }

    function test_deleteOfItem() {
        initialiseDocumentTemplate()

        tryCompare(testCaseItem , "selectedPaintableItem", null)
        compare(testDocumentTemplate.paintableItemList.rowCount(), 9)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(0, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableRectangle)

        var deleteButton = findChild(testEditionMenuController,
                                     "removeButtonRectangleMenu")
        verify(deleteButton)
        deleteButton.clicked()

        compare(testDocumentTemplate.paintableItemList.rowCount(), 8)
        compare(testCaseItem.selectedPaintableItem, null)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableText)

        deleteButton = findChild(testEditionMenuController,
                                 "removeButtonTextMenu")
        verify(deleteButton)
        deleteButton.clicked()

        compare(testDocumentTemplate.paintableItemList.rowCount(), 7)
        compare(testCaseItem.selectedPaintableItem, null)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableList)

        deleteButton = findChild(testEditionMenuController,
                                 "removeButtonListMenu")
        verify(deleteButton)
        deleteButton.clicked()

        compare(testDocumentTemplate.paintableItemList.rowCount(), 6)
        compare(testCaseItem.selectedPaintableItem, null)

        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableRectangle)
    }

    function test_associatedListUpdated() {
        initialiseDocumentTemplate()

        compare(testCaseItem.selectedPaintableItem, null)
        compare(testDocumentTemplate.paintableItemList.rowCount(), 9)

        var editionMenuLoader = findChild(testEditionMenuController,
                                          "editionMenuControllerLoader")
        verify(editionMenuLoader !== null)

        //-------------------------------------------------------------------------
        // Test the association of rectangle
        //-------------------------------------------------------------------------
        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(0, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableRectangle)

        // Check the combo box state
        var listAssociationController = findChild(editionMenuLoader.item,
                                                  "ListAssociationController")
        verify(listAssociationController)
        var listAssociationComboBox = findChild(
                    listAssociationController,
                    "ListAssociationControllerComboBox")
        verify(listAssociationComboBox)

        compare(listAssociationComboBox.model.rowCount(), 4)
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(1, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "a")
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(2, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "b")
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(3, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "c")

        compare(listAssociationComboBox.currentIndex, 0)

        // Change the list listAssociationComboBox to the first list (index 0)
        compare(testCaseItem.selectedPaintableItem.parentList,
                testDocumentTemplate.paintableItemList)

        var newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(1, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList !== null)
        listAssociationController.associatedListUpdated(
                    newList.listableItemListModel)

        var newListFromRootDocument = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(1, 0),
                    RootPaintableItemList.PaintableItemRole).listableItemListModel

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 1)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 8)
        compare(newListFromRootDocument.rowCount(), 1)

        // Reset the no list
        newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(0, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList === undefined)
        listAssociationController.associatedListUpdated(newList)

        newListFromRootDocument = testDocumentTemplate.paintableItemList

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 0)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 9)

        // Change the list listAssociationComboBox to the first list (index 1)
        compare(testCaseItem.selectedPaintableItem.parentList,
                testDocumentTemplate.paintableItemList)

        newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(2, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList !== null)
        listAssociationController.associatedListUpdated(
                    newList.listableItemListModel)

        newListFromRootDocument = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(4, 0),
                    RootPaintableItemList.PaintableItemRole).listableItemListModel

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 2)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 8)
        compare(newListFromRootDocument.rowCount(), 1)

        //-------------------------------------------------------------------------
        // Test the association of text
        //-------------------------------------------------------------------------
        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(0, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableText)

        // Check the combo box state
        listAssociationController = findChild(editionMenuLoader.item,
                                              "ListAssociationController")
        verify(listAssociationController !== null)

        listAssociationComboBox = findChild(listAssociationController,
                                            "ListAssociationControllerComboBox")
        verify(listAssociationComboBox)

        compare(listAssociationComboBox.model.rowCount(), 4)
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(1, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "a")
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(2, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "b")
        compare(listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(3, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.textRole)), "c")

        compare(listAssociationComboBox.currentIndex, 0)

        // Change the list listAssociationComboBox to the first list (index 0)
        compare(testCaseItem.selectedPaintableItem.parentList,
                testDocumentTemplate.paintableItemList)

        newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(1, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList !== null)
        listAssociationController.associatedListUpdated(
                    newList.listableItemListModel)

        newListFromRootDocument = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(0, 0),
                    RootPaintableItemList.PaintableItemRole).listableItemListModel

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 1)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 7)
        compare(newListFromRootDocument.rowCount(), 1)

        // Reset the no list
        newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(0, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList === undefined)
        listAssociationController.associatedListUpdated(newList)

        newListFromRootDocument = testDocumentTemplate.paintableItemList

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 0)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 8)

        // Change the list listAssociationComboBox to the first list (index 1)
        compare(testCaseItem.selectedPaintableItem.parentList,
                testDocumentTemplate.paintableItemList)

        newList = listAssociationComboBox.model.data(
                    listAssociationComboBox.model.index(2, 0),
                    listAssociationComboBox.model.roleValue(
                        listAssociationComboBox.valueRole))

        verify(newList !== null)
        listAssociationController.associatedListUpdated(
                    newList.listableItemListModel)

        newListFromRootDocument = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole).listableItemListModel

        compare(testCaseItem.selectedPaintableItem.parentList,
                newListFromRootDocument)

        compare(listAssociationController.currentAssociatedList,
                newListFromRootDocument)

        compare(listAssociationComboBox.currentIndex, 2)

        compare(testDocumentTemplate.paintableItemList.rowCount(), 7)
        compare(newListFromRootDocument.rowCount(), 2)

        //-------------------------------------------------------------------------
        // Test the association of text
        //-------------------------------------------------------------------------
        testCaseItem.selectedPaintableItem = testDocumentTemplate.paintableItemList.data(
                    testDocumentTemplate.paintableItemList.index(3, 0),
                    RootPaintableItemList.PaintableItemRole)

        verify(testCaseItem.selectedPaintableItem instanceof PaintableList)

        // Check the combo box state
        listAssociationController = findChild(
                    editionMenuLoader.item, "ListAssociationControllerComboBox")
        verify(listAssociationController === null)
    }
}
