#include <QQmlEngine>
#include <QtQuickTest>

#include "SosQml/QmlPluginInclude.hpp"

class Test_EditionMenuControllerSetup : public QObject
{
    Q_OBJECT

  public:
  public slots:
    void qmlEngineAvailable(QQmlEngine* engine)
    {
        engine->addImportPath(SosQmlPluginPath);
        engine->addImportPath(RootQmlPluginPath);
    }
};

QUICK_TEST_MAIN_WITH_SETUP(Test_EditionMenuController, Test_EditionMenuControllerSetup)

#include "Test_EditionMenuController.moc"
