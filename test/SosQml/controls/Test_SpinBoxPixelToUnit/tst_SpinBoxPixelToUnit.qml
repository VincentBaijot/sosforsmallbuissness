import QtQuick
import QtTest

import SosQml.Controls

Item {
    id: root

    TestCase {
        id: test_spinBox_DisplayedValue
        name: "test_spinBox_DisplayedValue"

        property int pixelValue: 0
        property double unitPerPixel: 0

        SpinBoxPixelToUnit {
            id: spinBoxPixelToUnit_displayedValue

            fromPixel: 10
            toPixel: 10000
            pixelValue: test_spinBox_DisplayedValue.pixelValue
            unitPerPixel: test_spinBox_DisplayedValue.unitPerPixel
        }

        function test_spinBox_DisplayedValue() {

            //------------------------------------------------------------------------
            // Round displayed value bewlow 1000
            // Validate also the calulation of the displayed value from pixel value
            //------------------------------------------------------------------------
            test_spinBox_DisplayedValue.unitPerPixel = 0.25
            test_spinBox_DisplayedValue.pixelValue = 1250

            compare(spinBoxPixelToUnit_displayedValue.unitPerPixel, 0.25)
            compare(spinBoxPixelToUnit_displayedValue.pixelValue, 1250)

            compare(spinBoxPixelToUnit_displayedValue.numberOfSignificativeDigits,
                    0)
            compare(spinBoxPixelToUnit_displayedValue.unitSpinBoxStep, 1)

            compare(spinBoxPixelToUnit_displayedValue.value, 313)
            compare(spinBoxPixelToUnit_displayedValue.displayText, "313")

            compare(spinBoxPixelToUnit_displayedValue.from, 3)
            compare(spinBoxPixelToUnit_displayedValue.to, 2500)

            //------------------------------------------------------------------------
            // Floating point number
            //------------------------------------------------------------------------
            test_spinBox_DisplayedValue.unitPerPixel = 0.001

            compare(spinBoxPixelToUnit_displayedValue.unitPerPixel, 0.001)
            compare(spinBoxPixelToUnit_displayedValue.pixelValue, 1250)

            compare(spinBoxPixelToUnit_displayedValue.numberOfSignificativeDigits,
                    -3)
            compare(spinBoxPixelToUnit_displayedValue.unitSpinBoxStep, 0.001)

            compare(spinBoxPixelToUnit_displayedValue.value, 1250)
            compare(spinBoxPixelToUnit_displayedValue.displayText, "1.250")

            compare(spinBoxPixelToUnit_displayedValue.from, 10)
            compare(spinBoxPixelToUnit_displayedValue.to, 10000)

            //------------------------------------------------------------------------
            // Number above 1000
            //------------------------------------------------------------------------
            test_spinBox_DisplayedValue.unitPerPixel = 1
            test_spinBox_DisplayedValue.pixelValue = 1000

            compare(spinBoxPixelToUnit_displayedValue.unitPerPixel, 1)
            compare(spinBoxPixelToUnit_displayedValue.pixelValue, 1000)

            compare(spinBoxPixelToUnit_displayedValue.numberOfSignificativeDigits,
                    0)
            compare(spinBoxPixelToUnit_displayedValue.unitSpinBoxStep, 1)

            compare(spinBoxPixelToUnit_displayedValue.value, 1000)
            compare(spinBoxPixelToUnit_displayedValue.displayText, "1,000")

            compare(spinBoxPixelToUnit_displayedValue.from, 10)
            compare(spinBoxPixelToUnit_displayedValue.to, 10000)

            //------------------------------------------------------------------------
            // 0 unit per pixel
            //------------------------------------------------------------------------
            test_spinBox_DisplayedValue.unitPerPixel = 0
            test_spinBox_DisplayedValue.pixelValue = 2000

            compare(spinBoxPixelToUnit_displayedValue.unitPerPixel, 0)
            compare(spinBoxPixelToUnit_displayedValue.pixelValue, 2000)

            compare(spinBoxPixelToUnit_displayedValue.numberOfSignificativeDigits,
                    0)
            compare(spinBoxPixelToUnit_displayedValue.unitSpinBoxStep, 1)

            compare(spinBoxPixelToUnit_displayedValue.value, 0)
            compare(spinBoxPixelToUnit_displayedValue.displayText, "0")

            compare(spinBoxPixelToUnit_displayedValue.from, 0)
            compare(spinBoxPixelToUnit_displayedValue.to, 0)

            //------------------------------------------------------------------------
            // 0 pixel value
            //------------------------------------------------------------------------
            spinBoxPixelToUnit_displayedValue.fromPixel = 0
            test_spinBox_DisplayedValue.unitPerPixel = 1
            test_spinBox_DisplayedValue.pixelValue = 0

            compare(spinBoxPixelToUnit_displayedValue.unitPerPixel, 1)
            compare(spinBoxPixelToUnit_displayedValue.pixelValue, 0)

            compare(spinBoxPixelToUnit_displayedValue.numberOfSignificativeDigits,
                    0)
            compare(spinBoxPixelToUnit_displayedValue.unitSpinBoxStep, 1)

            compare(spinBoxPixelToUnit_displayedValue.value, 0)
            compare(spinBoxPixelToUnit_displayedValue.displayText, "0")

            compare(spinBoxPixelToUnit_displayedValue.from, 0)
            compare(spinBoxPixelToUnit_displayedValue.to, 10000)
        }
    }
    TestCase {
        id: test_spinBox_setValue
        name: "test_spinBox_setValue"

        property int newPixelValue: 0

        SpinBoxPixelToUnit {
            id: spinBoxPixelToUnit_setValue

            fromPixel: 0
            toPixel: 10000
            pixelValue: 0
            unitPerPixel: 1

            onPixelValueModified: newPixelValue => {
                                      test_spinBox_setValue.newPixelValue = newPixelValue
                                  }
        }

        function test_spinBox_setValue() {
            compare(spinBoxPixelToUnit_setValue.numberOfSignificativeDigits, 0)
            compare(spinBoxPixelToUnit_setValue.unitSpinBoxStep, 1)

            compare(spinBoxPixelToUnit_setValue.value, 0)
            compare(spinBoxPixelToUnit_setValue.displayText, "0")

            spinBoxPixelToUnit_setValue.value = 313
            // Signal called when user change the spin box value
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 313)
            compare(spinBoxPixelToUnit_setValue.displayText, "313")
            compare(test_spinBox_setValue.newPixelValue, 313)

            spinBoxPixelToUnit_setValue.increase()
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 314)
            compare(spinBoxPixelToUnit_setValue.displayText, "314")
            compare(test_spinBox_setValue.newPixelValue, 314)

            spinBoxPixelToUnit_setValue.decrease()
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 313)
            compare(spinBoxPixelToUnit_setValue.displayText, "313")
            compare(test_spinBox_setValue.newPixelValue, 313)

            spinBoxPixelToUnit_setValue.unitPerPixel = 0.042

            spinBoxPixelToUnit_setValue.value = 426
            // Signal called when user change the spin box value
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 426)
            compare(spinBoxPixelToUnit_setValue.displayText, "42.6")
            // To display float the value is in unit divided by unitSpinBoxStep, so the final value in pixel is value * unitSpinBoxStep / unitPrePixel
            compare(test_spinBox_setValue.newPixelValue, 1014)

            spinBoxPixelToUnit_setValue.increase()
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 427)
            compare(spinBoxPixelToUnit_setValue.displayText, "42.7")
            compare(test_spinBox_setValue.newPixelValue, 1017)

            spinBoxPixelToUnit_setValue.decrease()
            spinBoxPixelToUnit_setValue.valueModified()

            compare(spinBoxPixelToUnit_setValue.value, 426)
            compare(spinBoxPixelToUnit_setValue.displayText, "42.6")
            compare(test_spinBox_setValue.newPixelValue, 1014)
        }
    }

    TestCase {
        id: test_spinBox_valueFromText
        name: "test_spinBox_valueFromText"

        // To test the valueFromText function use a property instead of the actual Spin box to simplify the test
        property string testValueText: "0"
        readonly property int testValueFromText: spinBoxPixelToUnit_valueFromText.valueFromText(
                                                     test_spinBox_valueFromText.testValueText,
                                                     Qt.locale())

        SpinBoxPixelToUnit {
            id: spinBoxPixelToUnit_valueFromText

            pixelValue: 0
            unitPerPixel: 0.02
        }

        function test_spinBox_valueFromText() {

            test_spinBox_valueFromText.testValueText = "127.1"
            compare(test_spinBox_valueFromText.testValueFromText, 1271)

            test_spinBox_valueFromText.testValueText = "0"
            compare(test_spinBox_valueFromText.testValueFromText, 0)
        }
    }
}
