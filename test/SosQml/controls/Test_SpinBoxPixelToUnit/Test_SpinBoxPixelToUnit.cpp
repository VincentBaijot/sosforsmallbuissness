#include <QQmlEngine>
#include <QtQuickTest>

#include "SosQml/QmlPluginInclude.hpp"

class Test_SpinBoxPixelToUnitSetup : public QObject
{
    Q_OBJECT

  public:
  public slots:
    void qmlEngineAvailable(QQmlEngine* engine)
    {
        QLocale::setDefault(QLocale::English);

        engine->addImportPath(SosQmlPluginPath);
        engine->addPluginPath(SosQmlPluginPath);
    }
};

QUICK_TEST_MAIN_WITH_SETUP(Test_SpinBoxPixelToUnit, Test_SpinBoxPixelToUnitSetup)

#include "Test_SpinBoxPixelToUnit.moc"
