cmake_minimum_required(VERSION 3.20)

print_location()

set(RootQmlPluginPath ${CMAKE_BINARY_DIR})
set(SosQmlPluginPath ${CMAKE_BINARY_DIR}/SosForSmallBuissness)

configure_file(QmlPluginInclude.hpp.in ${CMAKE_CURRENT_SOURCE_DIR}/QmlPluginInclude.hpp)

add_subdirectory(feature)
add_subdirectory(controls)
add_subdirectory(itemsControls)

set(LIBRARY_NAME SosQmlTest)

add_library(${LIBRARY_NAME} INTERFACE QmlPluginInclude.hpp QmlPluginInclude.hpp.in)

target_link_libraries(${LIBRARY_NAME} INTERFACE Qt${QT_VERSION_MAJOR}::Core)
