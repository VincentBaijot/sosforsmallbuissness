#include <QSignalSpy>
#include <QTest>

#include "DataTemplateAssociation/controllers/DataTemplateAssociation.hpp"
#include "DataTemplateAssociation/controllers/DataTemplateAssociationMap.hpp"
#include "DataTemplateAssociation/controllers/PaintableListDataDefinitionListBinding.hpp"
#include "DataTemplateAssociation/controllers/PaintableListDataTableModelBinding.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "DocumentTemplate/controllers/TemplatesHandler.hpp"
#include "Painter/controllers/PaintableList.hpp"

class Test_DataTemplateAssociationMap : public QObject
{
    Q_OBJECT

  public:
    Test_DataTemplateAssociationMap() = default;

  private slots:
    void data();
    void setData();

  private:
    void _initializeBaseData();

    documenttemplate::controller::TemplatesHandler* _templatesHandler { nullptr };
    documentdatadefinitions::controller::DataHandler* _dataHandler { nullptr };
};

void Test_DataTemplateAssociationMap::_initializeBaseData()
{
    if (_templatesHandler) { delete _templatesHandler; }

    documenttemplate::data::DocumentTemplateData documentTemplateData {
        documenttemplate::data::PageSizeData {},
        documenttemplate::data::RootPaintableItemListData {
          { painter::data::PaintableRectangleData {},
            painter::data::PaintableTextData {},
            painter::data::PaintableListData {},
            painter::data::PaintableRectangleData {},
            painter::data::PaintableTextData {},
            painter::data::PaintableListData {},
            painter::data::PaintableRectangleData {},
            painter::data::PaintableTextData {},
            painter::data::PaintableListData {} } }
    };

    documenttemplate::data::TemplatesHandlerData templatesHandlerData {
        { documentTemplateData, documentTemplateData, documentTemplateData, documentTemplateData }
    };

    _templatesHandler = new documenttemplate::controller::TemplatesHandler(templatesHandlerData, this);

    if (_dataHandler) { delete _dataHandler; }

    _dataHandler = new documentdatadefinitions::controller::DataHandler(this);
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
}

void Test_DataTemplateAssociationMap::data()
{
    _initializeBaseData();

    datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData0 {
        datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
          { { 2, 0 }, { 5, 3 }, { 8, 4 } } },
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
          { { 2, 0 }, { 5, 3 }, { 8, 4 } } }
    };

    datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData1 {
        datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
          { { 2, 1 }, { 5, 2 }, { 8, 3 } } },
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
          { { 2, 1 }, { 5, 2 }, { 8, 3 } } }
    };

    datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData2 {
        datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
          { { 2, 2 }, { 5, 3 }, { 8, 4 } } },
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
          { { 2, 2 }, { 5, 3 }, { 8, 4 } } }
    };

    datatemplateassociation::controller::DataTemplateAssociationMap dataDocumentAssociationMap;
    dataDocumentAssociationMap.setDataHandler(_dataHandler);
    dataDocumentAssociationMap.setTemplatesHandler(_templatesHandler);

    auto toto = _templatesHandler->at(0);
    dataDocumentAssociationMap.association(_templatesHandler->at(0))->setObjectData(dataDocumentAssociationData0);
    dataDocumentAssociationMap.association(_templatesHandler->at(1))->setObjectData(dataDocumentAssociationData1);
    // Skip 2
    dataDocumentAssociationMap.association(_templatesHandler->at(3))->setObjectData(dataDocumentAssociationData2);

    datatemplateassociation::data::DataTemplateAssociationMapData dataDocumentAssociationMapData =
      dataDocumentAssociationMap.objectData();

    QCOMPARE(dataDocumentAssociationMapData.dataDocumentAssociationMap.size(), 3);

    QVERIFY(dataDocumentAssociationMapData.dataDocumentAssociationMap.contains(0));
    QCOMPARE(dataDocumentAssociationMapData.dataDocumentAssociationMap.value(0), dataDocumentAssociationData0);

    QVERIFY(dataDocumentAssociationMapData.dataDocumentAssociationMap.contains(1));
    QCOMPARE(dataDocumentAssociationMapData.dataDocumentAssociationMap.value(1), dataDocumentAssociationData1);

    QVERIFY(!dataDocumentAssociationMapData.dataDocumentAssociationMap.contains(2));

    QVERIFY(dataDocumentAssociationMapData.dataDocumentAssociationMap.contains(3));
    QCOMPARE(dataDocumentAssociationMapData.dataDocumentAssociationMap.value(3), dataDocumentAssociationData2);
}

void Test_DataTemplateAssociationMap::setData()
{
    _initializeBaseData();

    QList<QPointer<datatemplateassociation::controller::DataTemplateAssociation>> associationPointerList;

    //----------------------------------------------------------
    // Test constructor with data
    //----------------------------------------------------------
    {
        datatemplateassociation::data::DataTemplateAssociationMapData dataDocumentAssociationMapData {
            { { 0,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 0 }, { 5, 3 }, { 8, 4 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 0 }, { 5, 3 }, { 8, 4 } } } } },
              { 1,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 1 }, { 5, 2 }, { 8, 3 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 1 }, { 5, 2 }, { 8, 4 } } } } },
              { 3,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 2 }, { 5, 3 }, { 8, 3 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 2 }, { 5, 3 }, { 8, 3 } } } } } }
        };

        datatemplateassociation::controller::DataTemplateAssociationMap dataDocumentAssociationMap(
          _dataHandler, _templatesHandler, dataDocumentAssociationMapData);

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(0);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(0));
        }

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(1);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(1));
        }

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(3);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(3));
        }
    }

    for (const QPointer<datatemplateassociation::controller::DataTemplateAssociation>& dataDocumentAssociation :
         associationPointerList)
    {
        QVERIFY(dataDocumentAssociation.isNull());
    }

    associationPointerList.clear();

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------
    {
        datatemplateassociation::data::DataTemplateAssociationMapData dataDocumentAssociationMapData {
            { { 0,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 5 }, { 5, 4 }, { 8, 3 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 5 }, { 5, 4 }, { 8, 3 } } } } },
              { 1,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 1 }, { 5, 2 }, { 8, 3 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 1 }, { 5, 2 }, { 8, 3 } } } } },
              { 3,
                datatemplateassociation::data::DataTemplateAssociationData {
                  datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                    { { 2, 2 }, { 5, 2 }, { 8, 3 } } },
                  datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                    { { 2, 2 }, { 5, 2 }, { 8, 3 } } } } } }
        };

        datatemplateassociation::controller::DataTemplateAssociationMap dataDocumentAssociationMap;
        dataDocumentAssociationMap.setDataHandler(_dataHandler);
        dataDocumentAssociationMap.setTemplatesHandler(_templatesHandler);
        dataDocumentAssociationMap.setObjectData(dataDocumentAssociationMapData);

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(0);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(0));
        }

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(1);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(1));
        }

        {
            documenttemplate::controller::DocumentTemplate* documentTemplate = _templatesHandler->at(3);
            QPointer<datatemplateassociation::controller::DataTemplateAssociation> dataDocumentAssociation =
              dataDocumentAssociationMap.association(documentTemplate);
            QVERIFY(!dataDocumentAssociation.isNull());
            associationPointerList.append(dataDocumentAssociation);

            QCOMPARE(dataDocumentAssociation->objectData(),
                     dataDocumentAssociationMapData.dataDocumentAssociationMap.value(3));
        }
    }

    for (const QPointer<datatemplateassociation::controller::DataTemplateAssociation>& dataDocumentAssociation :
         associationPointerList)
    {
        QVERIFY(dataDocumentAssociation.isNull());
    }
}

QTEST_APPLESS_MAIN(Test_DataTemplateAssociationMap)

#include "Test_DataTemplateAssociationMap.moc"
