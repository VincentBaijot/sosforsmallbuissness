#include <QSignalSpy>
#include <QTest>

#include "DataTemplateAssociation/controllers/DataTemplateAssociation.hpp"
#include "DataTemplateAssociation/controllers/PaintableListDataDefinitionListBinding.hpp"
#include "DataTemplateAssociation/controllers/PaintableListDataTableModelBinding.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "Painter/controllers/PaintableList.hpp"

class Test_DataTemplateAssociation : public QObject
{
    Q_OBJECT

  public:
    Test_DataTemplateAssociation() = default;

  private slots:
    void getAndUpdateAssociation();
    void insertAndUpdateAssociation();
    void data();
    void setData();

  private:
    void _initializeBaseData();

    documenttemplate::controller::DocumentTemplate* _documentTemplate { nullptr };
    documentdatadefinitions::controller::DataHandler* _dataHandler { nullptr };
};

void Test_DataTemplateAssociation::_initializeBaseData()
{
    if (_documentTemplate) { delete _documentTemplate; }

    _documentTemplate = new documenttemplate::controller::DocumentTemplate(this);

    QRectF rect(0, 0, 1, 1);

    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::RectangleItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::TextItemType, rect);
    _documentTemplate->paintableItemList()->addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType::ListItemType, rect);

    if (_dataHandler) { delete _dataHandler; }

    _dataHandler = new documentdatadefinitions::controller::DataHandler(this);
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
    _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->addListDataDefinition();
}

void Test_DataTemplateAssociation::getAndUpdateAssociation()
{
    _initializeBaseData();

    datatemplateassociation::controller::DataTemplateAssociation dataDocumentAssociation;
    dataDocumentAssociation.setDocumentTemplate(_documentTemplate);
    dataDocumentAssociation.setDataHandler(_dataHandler);

    for (int rowIndex = 0; rowIndex < _documentTemplate->paintableItemList()->rowCount(); ++rowIndex)
    {
        painter::controller::InterfaceQmlPaintableItem* item = _documentTemplate->paintableItemList()->at(rowIndex);
        if (painter::controller::PaintableList* paintableList = qobject_cast<painter::controller::PaintableList*>(item))
        {
            QVERIFY(dataDocumentAssociation.associatedDataDefinition(paintableList));
            QVERIFY(dataDocumentAssociation.associatedDataTable(paintableList));
        }
    }

    for (int paintableListIndex : { 2, 5, 8 })
    {
        painter::controller::PaintableList* paintableList =
          qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(paintableListIndex));

        documentdatadefinitions::controller::ListDataDefinition* listDataDefinition =
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(0);
        documentdatadefinitions::controller::DataTablesModel* dataTableModel =
          _dataHandler->userDataHandler()->dataTableModelListModel()->at(0);

        datatemplateassociation::controller::PaintableListDataDefinitionListBinding* definitionBinding0 =
          dataDocumentAssociation.associatedDataDefinition(paintableList);
        datatemplateassociation::controller::PaintableListDataTableModelBinding* dataBinding0 =
          dataDocumentAssociation.associatedDataTable(paintableList);

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), nullptr);

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), nullptr);

        QSignalSpy spyDefinitionBinding0(
          definitionBinding0,
          &datatemplateassociation::controller::PaintableListDataDefinitionListBinding::dataDefinitionListChanged);
        QSignalSpy spyDataBinding0(
          dataBinding0, &datatemplateassociation::controller::PaintableListDataTableModelBinding::dataTableModelChanged);

        QCOMPARE(spyDefinitionBinding0.size(), 0);
        QCOMPARE(spyDataBinding0.size(), 0);

        dataDocumentAssociation.insertAssociation(paintableList, listDataDefinition->dataDefinitionListModel());

        QCOMPARE(definitionBinding0, dataDocumentAssociation.associatedDataDefinition(paintableList));
        QCOMPARE(dataBinding0, dataDocumentAssociation.associatedDataTable(paintableList));

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), listDataDefinition->dataDefinitionListModel());

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), dataTableModel);

        QCOMPARE(spyDefinitionBinding0.size(), 1);
        QCOMPARE(spyDataBinding0.size(), 1);

        listDataDefinition = _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(2);
        dataTableModel     = _dataHandler->userDataHandler()->dataTableModelListModel()->at(2);

        dataDocumentAssociation.insertAssociation(paintableList, listDataDefinition->dataDefinitionListModel());

        QCOMPARE(definitionBinding0, dataDocumentAssociation.associatedDataDefinition(paintableList));
        QCOMPARE(dataBinding0, dataDocumentAssociation.associatedDataTable(paintableList));

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), listDataDefinition->dataDefinitionListModel());

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), dataTableModel);

        QCOMPARE(spyDefinitionBinding0.size(), 2);
        QCOMPARE(spyDataBinding0.size(), 2);
    }
}

void Test_DataTemplateAssociation::insertAndUpdateAssociation()
{
    _initializeBaseData();

    datatemplateassociation::controller::DataTemplateAssociation dataDocumentAssociation;
    dataDocumentAssociation.setDocumentTemplate(_documentTemplate);
    dataDocumentAssociation.setDataHandler(_dataHandler);

    for (int rowIndex = 0; rowIndex < _documentTemplate->paintableItemList()->rowCount(); ++rowIndex)
    {
        painter::controller::InterfaceQmlPaintableItem* item = _documentTemplate->paintableItemList()->at(rowIndex);
        if (painter::controller::PaintableList* paintableList = qobject_cast<painter::controller::PaintableList*>(item))
        {
            QVERIFY(dataDocumentAssociation.associatedDataDefinition(paintableList));
            QVERIFY(dataDocumentAssociation.associatedDataTable(paintableList));
        }
    }

    for (int paintableListIndex : { 2, 5, 8 })
    {
        painter::controller::PaintableList* paintableList =
          qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(paintableListIndex));

        documentdatadefinitions::controller::ListDataDefinition* listDataDefinition =
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(0);
        documentdatadefinitions::controller::DataTablesModel* dataTableModel =
          _dataHandler->userDataHandler()->dataTableModelListModel()->at(0);

        dataDocumentAssociation.insertAssociation(paintableList, listDataDefinition->dataDefinitionListModel());

        datatemplateassociation::controller::PaintableListDataDefinitionListBinding* definitionBinding0 =
          dataDocumentAssociation.associatedDataDefinition(paintableList);
        datatemplateassociation::controller::PaintableListDataTableModelBinding* dataBinding0 =
          dataDocumentAssociation.associatedDataTable(paintableList);

        // CHeck that getting multiple time the same value gives the same binding object
        QCOMPARE(definitionBinding0, dataDocumentAssociation.associatedDataDefinition(paintableList));
        QCOMPARE(dataBinding0, dataDocumentAssociation.associatedDataTable(paintableList));

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), listDataDefinition->dataDefinitionListModel());

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), dataTableModel);

        QSignalSpy spyDefinitionBinding0(
          definitionBinding0,
          &datatemplateassociation::controller::PaintableListDataDefinitionListBinding::dataDefinitionListChanged);
        QSignalSpy spyDataBinding0(
          dataBinding0, &datatemplateassociation::controller::PaintableListDataTableModelBinding::dataTableModelChanged);

        QCOMPARE(spyDefinitionBinding0.size(), 0);
        QCOMPARE(spyDataBinding0.size(), 0);

        listDataDefinition = _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(2);
        dataTableModel     = _dataHandler->userDataHandler()->dataTableModelListModel()->at(2);

        dataDocumentAssociation.insertAssociation(paintableList, listDataDefinition->dataDefinitionListModel());

        QCOMPARE(definitionBinding0, dataDocumentAssociation.associatedDataDefinition(paintableList));
        QCOMPARE(dataBinding0, dataDocumentAssociation.associatedDataTable(paintableList));

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), listDataDefinition->dataDefinitionListModel());

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), dataTableModel);

        QCOMPARE(spyDefinitionBinding0.size(), 1);
        QCOMPARE(spyDataBinding0.size(), 1);

        listDataDefinition = _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(5);
        dataTableModel     = _dataHandler->userDataHandler()->dataTableModelListModel()->at(5);

        dataDocumentAssociation.insertAssociation(paintableList, listDataDefinition->dataDefinitionListModel());

        QCOMPARE(definitionBinding0, dataDocumentAssociation.associatedDataDefinition(paintableList));
        QCOMPARE(dataBinding0, dataDocumentAssociation.associatedDataTable(paintableList));

        QVERIFY(definitionBinding0 != nullptr);
        QCOMPARE(definitionBinding0->paintableList(), paintableList);
        QCOMPARE(definitionBinding0->dataDefinitionList(), listDataDefinition->dataDefinitionListModel());

        QVERIFY(dataBinding0 != nullptr);
        QCOMPARE(dataBinding0->paintableList(), paintableList);
        QCOMPARE(dataBinding0->dataTableModel(), dataTableModel);

        QCOMPARE(spyDefinitionBinding0.size(), 2);
        QCOMPARE(spyDataBinding0.size(), 2);
    }
}

void Test_DataTemplateAssociation::data()
{
    _initializeBaseData();

    datatemplateassociation::controller::DataTemplateAssociation dataDocumentAssociation;
    dataDocumentAssociation.setDocumentTemplate(_documentTemplate);
    dataDocumentAssociation.setDataHandler(_dataHandler);

    QCOMPARE(_documentTemplate->paintableItemList()->rowCount(), 9);
    QCOMPARE(_dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->rowCount(), 7);

    dataDocumentAssociation.insertAssociation(
      qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(2)),
      _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(0)->dataDefinitionListModel());

    dataDocumentAssociation.insertAssociation(
      qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(5)),
      _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(3)->dataDefinitionListModel());

    dataDocumentAssociation.insertAssociation(
      qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(8)),
      _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(5)->dataDefinitionListModel());

    datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData =
      dataDocumentAssociation.objectData();

    QCOMPARE(dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.size(), 3);

    {
        auto found =
          dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.constFind(2);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.cend());
        QCOMPARE(found.value(), 0);

        found =
          dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.constFind(5);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.cend());
        QCOMPARE(found.value(), 3);

        found =
          dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.constFind(8);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.cend());
        QCOMPARE(found.value(), 5);
    }

    // Check user data

    QCOMPARE(
      dataDocumentAssociation
        .associatedDataTable(qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(2)))
        ->dataTableModel(),
      _dataHandler->userDataHandler()->dataTableModelListModel()->at(0));

    QCOMPARE(
      dataDocumentAssociation
        .associatedDataTable(qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(5)))
        ->dataTableModel(),
      _dataHandler->userDataHandler()->dataTableModelListModel()->at(3));

    QCOMPARE(
      dataDocumentAssociation
        .associatedDataTable(qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(8)))
        ->dataTableModel(),
      _dataHandler->userDataHandler()->dataTableModelListModel()->at(5));

    QCOMPARE(dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.size(), 3);

    {
        auto found =
          dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.constFind(2);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.cend());
        QCOMPARE(found.value(), 0);

        found = dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.constFind(5);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.cend());
        QCOMPARE(found.value(), 3);

        found = dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.constFind(8);
        QVERIFY(found
                != dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.cend());
        QCOMPARE(found.value(), 5);
    }
}

void Test_DataTemplateAssociation::setData()
{
    _initializeBaseData();

    //----------------------------------------------------------
    // Test constructor with data
    //----------------------------------------------------------
    {
        datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData;

        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(2, 0);
        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(5, 3);
        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(8, 42);
        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(42, 42);

        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(2, 0);
        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(5, 3);
        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(8, 42);
        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(42, 42);

        datatemplateassociation::controller::DataTemplateAssociation dataDocumentAssociation { _documentTemplate,
                                                                                   _dataHandler,
                                                                                   dataDocumentAssociationData };

        painter::controller::PaintableList* paintableList =
          qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(2));

        QCOMPARE(
          dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(),
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(0)->dataDefinitionListModel());
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(),
                 _dataHandler->userDataHandler()->dataTableModelListModel()->at(0));

        paintableList = qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(5));

        QCOMPARE(
          dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(),
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(3)->dataDefinitionListModel());
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(),
                 _dataHandler->userDataHandler()->dataTableModelListModel()->at(3));

        paintableList = qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(8));

        QCOMPARE(dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(), nullptr);
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(), nullptr);

        QCOMPARE(dataDocumentAssociation.associatedDataDefinition(nullptr), nullptr);
        QCOMPARE(dataDocumentAssociation.associatedDataTable(nullptr), nullptr);
    }

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------
    {
        datatemplateassociation::data::DataTemplateAssociationData dataDocumentAssociationData;

        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(2, 1);
        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(5, 2);
        dataDocumentAssociationData.paintableListDataDefinitionListAssociationData.association.insert(8, 3);

        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(2, 1);
        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(5, 2);
        dataDocumentAssociationData.paintableListDataTableModelAssociationData.association.insert(8, 3);

        datatemplateassociation::controller::DataTemplateAssociation dataDocumentAssociation;
        dataDocumentAssociation.setDocumentTemplate(_documentTemplate);
        dataDocumentAssociation.setDataHandler(_dataHandler);
        dataDocumentAssociation.setObjectData(dataDocumentAssociationData);

        painter::controller::PaintableList* paintableList =
          qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(2));

        QCOMPARE(
          dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(),
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(1)->dataDefinitionListModel());
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(),
                 _dataHandler->userDataHandler()->dataTableModelListModel()->at(1));

        paintableList = qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(5));

        QCOMPARE(
          dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(),
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(2)->dataDefinitionListModel());
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(),
                 _dataHandler->userDataHandler()->dataTableModelListModel()->at(2));

        paintableList = qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(8));

        QCOMPARE(
          dataDocumentAssociation.associatedDataDefinition(paintableList)->dataDefinitionList(),
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(3)->dataDefinitionListModel());
        QCOMPARE(dataDocumentAssociation.associatedDataTable(paintableList)->dataTableModel(),
                 _dataHandler->userDataHandler()->dataTableModelListModel()->at(3));
    }
}

QTEST_APPLESS_MAIN(Test_DataTemplateAssociation)

#include "Test_DataTemplateAssociation.moc"
