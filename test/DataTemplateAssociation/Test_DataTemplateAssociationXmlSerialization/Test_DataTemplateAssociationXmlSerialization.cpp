#include <QtTest/QTest>

#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataDefinitionListAssociationData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataTableModelAssociationData.hpp"
#include "DataTemplateAssociation/services/DataTemplateAssociationXmlConstants.hpp"
#include "DataTemplateAssociation/services/DataTemplateAssociationXmlSerialization.hpp"

class Test_DataTemplateAssociationXmlSerialization : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void test_readPaintableListDataDefinitionListAssociationData_data();
    void test_readPaintableListDataDefinitionListAssociationData();

    void test_writePaintableListDataDefinitionListAssociationData();

    void test_readPaintableListDataTableModelAssociationData_data();
    void test_readPaintableListDataTableModelAssociationData();

    void test_writePaintableListDataTableModelAssociationData();

    void test_readDataTemplateAssociationData_data();
    void test_readDataTemplateAssociationData();

    void test_writeDataTemplateAssociationData();

    void test_readDataTemplateAssociationMapData_data();
    void test_readDataTemplateAssociationMapData();

    void test_writeDataTemplateAssociationMapData();

  private:
    static QByteArray writePaintableListDataDefinitionListAssociationDataXml(
      const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
        paintableListDataDefinitionListAssociationData);
    static QByteArray writePaintableListDataTableModelAssociationDataXml(
      const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
        paintableListDataTableModelAssociationData);
    static QByteArray writeDataTemplateAssociationDataXml(
      const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData);
};

void Test_DataTemplateAssociationXmlSerialization::test_readPaintableListDataDefinitionListAssociationData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<
      std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>>(
      "expectedPaintableListDataDefinitionListAssociationData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>5</PaintableListIndex>\n"
                                                 "        <DataDefinitionIndex>3</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>8</PaintableListIndex>\n"
                                                 "        <DataDefinitionIndex>4</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>2</PaintableListIndex>\n"
                                                 "        <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataDefinitionListAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } }
            };

        QTest::newRow("Same_order_than_write")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Other order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataDefinitionIndex>3</DataDefinitionIndex>\n"
                                                 "        <PaintableListIndex>5</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>8</PaintableListIndex>\n"
                                                 "        <DataDefinitionIndex>4</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                                 "        <PaintableListIndex>2</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataDefinitionListAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } }
            };

        QTest::newRow("Same_order_than_write")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Some missing values
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>5</PaintableListIndex>\n"
                                                 "        <DataDefinitionIndex>3</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>8</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataDefinitionListAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataDefinitionIndex> { { 5, 3 } }
            };

        QTest::newRow("Same_order_than_write")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n"
                                                 "    <AssociationEntry/>\n"
                                                 "</PaintableListDataDefinitionListAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {};

        QTest::newRow("Self_closing_parts") << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Empty PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n"
                                                 "</PaintableListDataDefinitionListAssociation>\n");
        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {};
        QTest::newRow("Empty_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Empty self closing PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation/>\n");
        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData =
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {};
        QTest::newRow("Empty_self_closing_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Not PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData = std::nullopt;
        QTest::newRow("Not_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
          expectedPaintableListDataDefinitionListAssociationData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedPaintableListDataDefinitionListAssociationData;
    }
}

void Test_DataTemplateAssociationXmlSerialization::test_readPaintableListDataDefinitionListAssociationData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>,
           expectedPaintableListDataDefinitionListAssociationData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
      actualPaintableListDataDefinitionListAssociationData =
        datatemplateassociation::service::readPaintableListDataDefinitionListAssociationData(streamReader);

    if (expectedPaintableListDataDefinitionListAssociationData.has_value())
    {
        QCOMPARE(actualPaintableListDataDefinitionListAssociationData.value(),
                 expectedPaintableListDataDefinitionListAssociationData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(),
                 datatemplateassociation::service::paintableListDataDefinitionListAssociationElement);
    }
    else { QVERIFY(!actualPaintableListDataDefinitionListAssociationData.has_value()); }
}

void Test_DataTemplateAssociationXmlSerialization::test_writePaintableListDataDefinitionListAssociationData()
{
    datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
      paintableListDataDefinitionListAssociationData {
          QHash<datatemplateassociation::data::PaintableListIndex,
                datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } }
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    datatemplateassociation::service::writePaintableListDataDefinitionListAssociationData(
      streamWriter, paintableListDataDefinitionListAssociationData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataDefinitionListAssociation>\n");
    QVERIFY(arrayData.startsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>5</PaintableListIndex>\n"
                                      "        <DataDefinitionIndex>3</DataDefinitionIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>8</PaintableListIndex>\n"
                                      "        <DataDefinitionIndex>4</DataDefinitionIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>2</PaintableListIndex>\n"
                                      "        <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("</PaintableListDataDefinitionListAssociation>\n");

    QVERIFY(arrayData.endsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    QCOMPARE(arrayData, "");
}

void Test_DataTemplateAssociationXmlSerialization::test_readPaintableListDataTableModelAssociationData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<
      std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>>(
      "expectedPaintableListDataTableModelAssociationData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>5</PaintableListIndex>\n"
                                                 "        <DataTablesModelIndex>2</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>8</PaintableListIndex>\n"
                                                 "        <DataTablesModelIndex>3</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>2</PaintableListIndex>\n"
                                                 "        <DataTablesModelIndex>1</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataTableModelAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                { { 2, 1 }, { 5, 2 }, { 8, 3 } }
            };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Other order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataTablesModelIndex>2</DataTablesModelIndex>\n"
                                                 "        <PaintableListIndex>5</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <PaintableListIndex>8</PaintableListIndex>\n"
                                                 "        <DataTablesModelIndex>3</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataTablesModelIndex>1</DataTablesModelIndex>\n"
                                                 "        <PaintableListIndex>2</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataTableModelAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                { { 2, 1 }, { 5, 2 }, { 8, 3 } }
            };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Some missing values
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataTablesModelIndex>2</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataTablesModelIndex>3</DataTablesModelIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "    <AssociationEntry>\n"
                                                 "        <DataTablesModelIndex>1</DataTablesModelIndex>\n"
                                                 "        <PaintableListIndex>2</PaintableListIndex>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataTableModelAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 2, 1 } } };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n"
                                                 "    <AssociationEntry>\n"
                                                 "    </AssociationEntry>\n"
                                                 "</PaintableListDataTableModelAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData {

            };

        QTest::newRow("Self_closing_parts") << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Empty PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n"
                                                 "</PaintableListDataTableModelAssociation>\n");

        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData {

            };
        QTest::newRow("Empty_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Empty self closing PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation/>\n");
        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData =
            datatemplateassociation::data::PaintableListDataTableModelAssociationData {

            };
        QTest::newRow("Empty_self_closing_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Not PaintableListDataDefinitionListAssociation
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData = std::nullopt;
        QTest::newRow("Not_PaintableListDataDefinitionListAssociation")
          << arrayData << expectedPaintableListDataTableModelAssociationData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
          expectedPaintableListDataTableModelAssociationData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedPaintableListDataTableModelAssociationData;
    }
}

void Test_DataTemplateAssociationXmlSerialization::test_readPaintableListDataTableModelAssociationData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>,
           expectedPaintableListDataTableModelAssociationData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
      actualPaintableListDataTableModelAssociationData =
        datatemplateassociation::service::readPaintableListDataTableModelAssociationData(streamReader);

    if (expectedPaintableListDataTableModelAssociationData.has_value())
    {
        QCOMPARE(actualPaintableListDataTableModelAssociationData.value(),
                 expectedPaintableListDataTableModelAssociationData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(),
                 datatemplateassociation::service::paintableListDataTableModelAssociationElement);
    }
    else { QVERIFY(!actualPaintableListDataTableModelAssociationData.has_value()); }
}

void Test_DataTemplateAssociationXmlSerialization::test_writePaintableListDataTableModelAssociationData()
{
    datatemplateassociation::data::PaintableListDataTableModelAssociationData
      paintableListDataTableModelAssociationData =
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
            { { 2, 1 }, { 5, 2 }, { 8, 3 } }
        };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    datatemplateassociation::service::writePaintableListDataTableModelAssociationData(
      streamWriter, paintableListDataTableModelAssociationData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableListDataTableModelAssociation>\n");
    QVERIFY(arrayData.startsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>5</PaintableListIndex>\n"
                                      "        <DataTablesModelIndex>2</DataTablesModelIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>8</PaintableListIndex>\n"
                                      "        <DataTablesModelIndex>3</DataTablesModelIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    <AssociationEntry>\n"
                                      "        <PaintableListIndex>2</PaintableListIndex>\n"
                                      "        <DataTablesModelIndex>1</DataTablesModelIndex>\n"
                                      "    </AssociationEntry>\n");

    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("</PaintableListDataTableModelAssociation>\n");

    QVERIFY(arrayData.endsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    QCOMPARE(arrayData, "");
}

void Test_DataTemplateAssociationXmlSerialization::test_readDataTemplateAssociationData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<datatemplateassociation::data::DataTemplateAssociationData>>(
      "expectedDataTemplateAssociationData");

    // Same order than write
    {
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = datatemplateassociation::data::DataTemplateAssociationData {
              datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataDefinitionIndex> {
                  { 2, 0 }, { 5, 3 }, { 8, 4 } } },
              datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataTablesModelIndex> {
                  { 2, 1 }, { 5, 2 }, { 8, 3 } } }
          };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociation>");
        arrayData.append(writePaintableListDataDefinitionListAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataDefinitionListAssociationData));
        arrayData.append(writePaintableListDataTableModelAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataTableModelAssociationData));
        arrayData.append(QByteArrayLiteral("</DataTemplateAssociation>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << dataTemplateAssociationData;
    }

    // Other order than write
    {
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = datatemplateassociation::data::DataTemplateAssociationData {
              datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataDefinitionIndex> {
                  { 2, 0 }, { 5, 3 }, { 8, 4 } } },
              datatemplateassociation::data::PaintableListDataTableModelAssociationData {
                QHash<datatemplateassociation::data::PaintableListIndex,
                      datatemplateassociation::data::DataTablesModelIndex> {
                  { 2, 1 }, { 5, 2 }, { 8, 3 } } }
          };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociation>");
        arrayData.append(writePaintableListDataTableModelAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataTableModelAssociationData));
        arrayData.append(writePaintableListDataDefinitionListAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataDefinitionListAssociationData));
        arrayData.append(QByteArrayLiteral("</DataTemplateAssociation>\n"));

        QTest::newRow("Other_order_than_write") << arrayData << dataTemplateAssociationData;
    }

    // Self closing parts
    {
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = datatemplateassociation::data::DataTemplateAssociationData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociation>");
        arrayData.append(writePaintableListDataDefinitionListAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataDefinitionListAssociationData));
        arrayData.append(writePaintableListDataTableModelAssociationDataXml(
          dataTemplateAssociationData.value().paintableListDataTableModelAssociationData));
        arrayData.append(QByteArrayLiteral("</DataTemplateAssociation>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << dataTemplateAssociationData;
    }

    // Empty DataTemplateAssociationData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTemplateAssociation>\n"
                                                 "</DataTemplateAssociation>\n");

        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = datatemplateassociation::data::DataTemplateAssociationData {};

        QTest::newRow("Empty_DataTemplateAssociationData") << arrayData << dataTemplateAssociationData;
    }

    // Empty self closing DataTemplateAssociationData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTemplateAssociation/>\n");
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = datatemplateassociation::data::DataTemplateAssociationData {};

        QTest::newRow("Empty_self_closing_DataTemplateAssociation") << arrayData << dataTemplateAssociationData;
    }

    // Not DataTemplateAssociationData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = std::nullopt;

        QTest::newRow("Not_DataTemplateAssociationData") << arrayData << dataTemplateAssociationData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<datatemplateassociation::data::DataTemplateAssociationData>
          dataTemplateAssociationData = std::nullopt;

        QTest::newRow("Not_Xml") << arrayData << dataTemplateAssociationData;
    }
}

void Test_DataTemplateAssociationXmlSerialization::test_readDataTemplateAssociationData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<datatemplateassociation::data::DataTemplateAssociationData>,
           expectedDataTemplateAssociationData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<datatemplateassociation::data::DataTemplateAssociationData>
      actualDataTemplateAssociationData =
        datatemplateassociation::service::readDataTemplateAssociationData(streamReader);

    if (expectedDataTemplateAssociationData.has_value())
    {
        QVERIFY(actualDataTemplateAssociationData.has_value());
        QCOMPARE(actualDataTemplateAssociationData.value(), expectedDataTemplateAssociationData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), datatemplateassociation::service::dataTemplateAssociationElement);
    }
    else { QVERIFY(!actualDataTemplateAssociationData.has_value()); }
}

void Test_DataTemplateAssociationXmlSerialization::test_writeDataTemplateAssociationData()
{
    datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData =
      datatemplateassociation::data::DataTemplateAssociationData {
          datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
            QHash<datatemplateassociation::data::PaintableListIndex,
                  datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } } },
          datatemplateassociation::data::PaintableListDataTableModelAssociationData {
            QHash<datatemplateassociation::data::PaintableListIndex,
                  datatemplateassociation::data::DataTablesModelIndex> { { 2, 1 }, { 5, 2 }, { 8, 3 } } }
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    datatemplateassociation::service::writeDataTemplateAssociationData(streamWriter, dataTemplateAssociationData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociation>");

    expectedArray.append(writePaintableListDataDefinitionListAssociationDataXml(
      dataTemplateAssociationData.paintableListDataDefinitionListAssociationData));
    expectedArray.append(writePaintableListDataTableModelAssociationDataXml(
      dataTemplateAssociationData.paintableListDataTableModelAssociationData));

    expectedArray.append(QByteArrayLiteral("</DataTemplateAssociation>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DataTemplateAssociationXmlSerialization::test_readDataTemplateAssociationMapData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>>(
      "expectedDataTemplateAssociationMapData");

    // Complete data
    {
        std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
          dataTemplateAssociationMapData;

        {
            using namespace datatemplateassociation::data;
            dataTemplateAssociationMapData =
              DataTemplateAssociationMapData { QHash<DocumentTemplateIndex, DataTemplateAssociationData> {
                { 0,
                  DataTemplateAssociationData {
                    PaintableListDataDefinitionListAssociationData {
                      QHash<PaintableListIndex, DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } } },
                    PaintableListDataTableModelAssociationData {
                      QHash<PaintableListIndex, DataTablesModelIndex> { { 2, 1 }, { 5, 2 }, { 8, 3 } } } } },
                { 1,
                  DataTemplateAssociationData {
                    PaintableListDataDefinitionListAssociationData {
                      QHash<PaintableListIndex, DataDefinitionIndex> { { 2, 1 }, { 5, 4 }, { 8, 5 } } },
                    PaintableListDataTableModelAssociationData {
                      QHash<PaintableListIndex, DataTablesModelIndex> { { 2, 2 }, { 5, 3 }, { 8, 4 } } } } },
                { 2, DataTemplateAssociationData {} } } };
        }

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociationMap>");

        for (auto it = dataTemplateAssociationMapData.value().dataDocumentAssociationMap.cbegin();
             it != dataTemplateAssociationMapData.value().dataDocumentAssociationMap.cend();
             ++it)

        {
            arrayData.append(QByteArrayLiteral("<AssociationEntry>"
                                               "<DocumentTemplateIndex>"));
            arrayData.append(QByteArray::number(it.key()));
            arrayData.append(QByteArrayLiteral("</DocumentTemplateIndex>"));
            arrayData.append(writeDataTemplateAssociationDataXml(it.value()));
            arrayData.append(QByteArrayLiteral("</AssociationEntry>"));
        }

        arrayData.append(QByteArrayLiteral("</DataTemplateAssociationMap>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << dataTemplateAssociationMapData;
    }

    // Empty DataTemplateAssociationMap
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTemplateAssociationMap>\n"
                                                 "</DataTemplateAssociationMap>\n");

        std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
          dataTemplateAssociationMapData = datatemplateassociation::data::DataTemplateAssociationMapData {};

        QTest::newRow("Empty_DataTemplateAssociationMap") << arrayData << dataTemplateAssociationMapData;
    }

    // Empty self closing DataTemplateAssociationMap
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTemplateAssociationMap/>\n");
        std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
          dataTemplateAssociationMap = datatemplateassociation::data::DataTemplateAssociationMapData {};

        QTest::newRow("Empty_self_closing_DataTemplateAssociation") << arrayData << dataTemplateAssociationMap;
    }

    // Not DataTemplateAssociationMap
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
          dataTemplateAssociationMap = std::nullopt;

        QTest::newRow("Not_DataTemplateAssociationData") << arrayData << dataTemplateAssociationMap;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
          dataTemplateAssociationMap = std::nullopt;

        QTest::newRow("Not_Xml") << arrayData << dataTemplateAssociationMap;
    }
}

void Test_DataTemplateAssociationXmlSerialization::test_readDataTemplateAssociationMapData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>,
           expectedDataTemplateAssociationMapData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
      actualDataTemplateAssociationMapData =
        datatemplateassociation::service::readDataTemplateAssociationMapData(streamReader);

    if (expectedDataTemplateAssociationMapData.has_value())
    {
        QCOMPARE(actualDataTemplateAssociationMapData.value(), expectedDataTemplateAssociationMapData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), datatemplateassociation::service::dataTemplateAssociationMapElement);
    }
    else { QVERIFY(!actualDataTemplateAssociationMapData.has_value()); }
}

void Test_DataTemplateAssociationXmlSerialization::test_writeDataTemplateAssociationMapData()
{
    datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData;

    {
        using namespace datatemplateassociation::data;
        dataTemplateAssociationMapData =
          DataTemplateAssociationMapData { QHash<DocumentTemplateIndex, DataTemplateAssociationData> {
            { 0,
              DataTemplateAssociationData {
                PaintableListDataDefinitionListAssociationData {
                  QHash<PaintableListIndex, DataDefinitionIndex> { { 2, 0 }, { 5, 3 }, { 8, 4 } } },
                PaintableListDataTableModelAssociationData {
                  QHash<PaintableListIndex, DataTablesModelIndex> { { 2, 1 }, { 5, 2 }, { 8, 3 } } } } },
            { 1,
              DataTemplateAssociationData {
                PaintableListDataDefinitionListAssociationData {
                  QHash<PaintableListIndex, DataDefinitionIndex> { { 2, 1 }, { 5, 4 }, { 8, 5 } } },
                PaintableListDataTableModelAssociationData {
                  QHash<PaintableListIndex, DataTablesModelIndex> { { 2, 2 }, { 5, 3 }, { 8, 4 } } } } },
            { 2, DataTemplateAssociationData {} } } };
    }

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    datatemplateassociation::service::writeDataTemplateAssociationMapData(streamWriter,
                                                                          dataTemplateAssociationMapData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataTemplateAssociationMap>");

    QVERIFY(arrayData.startsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    for (auto it = dataTemplateAssociationMapData.dataDocumentAssociationMap.cbegin();
         it != dataTemplateAssociationMapData.dataDocumentAssociationMap.cend();
         ++it)
    {
        expectedArray = QByteArrayLiteral("<AssociationEntry>"
                                          "<DocumentTemplateIndex>");
        expectedArray.append(QByteArray::number(it.key()));
        expectedArray.append(QByteArrayLiteral("</DocumentTemplateIndex>"));
        expectedArray.append(writeDataTemplateAssociationDataXml(it.value()));
        expectedArray.append(QByteArrayLiteral("</AssociationEntry>"));

        QVERIFY(arrayData.contains(expectedArray));
        arrayData.replace(expectedArray, "");
    }

    expectedArray = QByteArrayLiteral("</DataTemplateAssociationMap>\n");

    QVERIFY(arrayData.endsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    QCOMPARE(arrayData, "");
}

QByteArray Test_DataTemplateAssociationXmlSerialization::writePaintableListDataDefinitionListAssociationDataXml(
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData)
{
    QByteArray paintableListDataDefinitionListAssociationXml;

    QXmlStreamWriter streamWriter(&paintableListDataDefinitionListAssociationXml);
    streamWriter.setAutoFormatting(false);

    datatemplateassociation::service::writePaintableListDataDefinitionListAssociationData(
      streamWriter, paintableListDataDefinitionListAssociationData);

    return paintableListDataDefinitionListAssociationXml;
}

QByteArray Test_DataTemplateAssociationXmlSerialization::writePaintableListDataTableModelAssociationDataXml(
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData)
{
    QByteArray paintableListDataTableModelAssociationXml;

    QXmlStreamWriter streamWriter(&paintableListDataTableModelAssociationXml);
    streamWriter.setAutoFormatting(false);

    datatemplateassociation::service::writePaintableListDataTableModelAssociationData(
      streamWriter, paintableListDataTableModelAssociationData);

    return paintableListDataTableModelAssociationXml;
}

QByteArray Test_DataTemplateAssociationXmlSerialization::writeDataTemplateAssociationDataXml(
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData)
{
    QByteArray dataTemplateAssociationDataXml;

    QXmlStreamWriter streamWriter(&dataTemplateAssociationDataXml);
    streamWriter.setAutoFormatting(false);

    datatemplateassociation::service::writeDataTemplateAssociationData(streamWriter, dataTemplateAssociationData);

    return dataTemplateAssociationDataXml;
}

QTEST_APPLESS_MAIN(Test_DataTemplateAssociationXmlSerialization)

#include "Test_DataTemplateAssociationXmlSerialization.moc"
