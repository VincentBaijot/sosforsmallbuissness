#include <QTest>
#include <QtGui/QFontDatabase>

#include "Utils/FileUtils.hpp"

class Test_LoadFont : public QObject
{
    Q_OBJECT

  private slots:
    void loadFonts();
};

void Test_LoadFont::loadFonts()
{
    const QList<int>& fontIds = utils::FileUtils::loadFonts(":/data");

    QStringList loadedFontFamilies;

    for (int fontId : fontIds) loadedFontFamilies.append(QFontDatabase::applicationFontFamilies(fontId));

    QVERIFY(loadedFontFamilies.contains("Handlee"));
    QVERIFY(loadedFontFamilies.contains("Roboto"));
    QVERIFY(loadedFontFamilies.contains("Libre Baskerville"));
}

QTEST_MAIN(Test_LoadFont)

#include "Test_LoadFont.moc"
