cmake_minimum_required(VERSION 3.20)

print_location()

set(SOURCES
   PainterTestCommon.cpp
)
set(HEADERS
   PainterTestCommon.hpp
)

set(LIBRARY_NAME PainterTestCommon)

add_library(${LIBRARY_NAME} STATIC ${SOURCES} ${HEADERS})

target_link_libraries(${LIBRARY_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Test Qt${QT_VERSION_MAJOR}::Quick Qt${QT_VERSION_MAJOR}::Svg)
