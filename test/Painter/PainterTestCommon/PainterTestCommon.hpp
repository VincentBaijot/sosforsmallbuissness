#pragma once

#include <QObject>

class QPaintDevice;

class PainterTestCommon : public QObject
{
    Q_OBJECT
  public:
    PainterTestCommon() = default;

    QSharedPointer<QPaintDevice> createSvgPaintDevice(const QString& generatedFilePath);
    QSharedPointer<QPaintDevice> createPdfPaintDevice(const QString& generatedFilePath);
    // Compare ignoring windows/unix endling
    void compareFiles(const QString& actualFilePath, const QString& expectedFilePath);

    int resolution { 1200 };
};
