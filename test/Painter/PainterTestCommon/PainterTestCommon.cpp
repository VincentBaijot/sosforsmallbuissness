#include "PainterTestCommon.hpp"

#include <QFile>
#include <QPdfWriter>
#include <QTest>
#include <QtGui/QPageSize>
#include <QtSvg/QSvgGenerator>

QSharedPointer<QPaintDevice> PainterTestCommon::createSvgPaintDevice(const QString& generatedFilePath)
{
    QPageSize pageSize(QPageSize::A4);
    QSize sizePixel = pageSize.sizePixels(resolution);

    QSharedPointer<QSvgGenerator> svgGenerator = QSharedPointer<QSvgGenerator>(new QSvgGenerator());
    svgGenerator->setFileName(generatedFilePath);
    svgGenerator->setResolution(resolution);
    svgGenerator->setSize(sizePixel);
    svgGenerator->setViewBox(QRect(0, 0, sizePixel.width(), sizePixel.height()));

    return svgGenerator;
}

QSharedPointer<QPaintDevice> PainterTestCommon::createPdfPaintDevice(const QString& generatedFilePath)
{
    QPageSize pageSize(QPageSize::A4);

    QSharedPointer<QPdfWriter> pdfWriter = QSharedPointer<QPdfWriter>(new QPdfWriter(generatedFilePath));
    pdfWriter->setResolution(resolution);
    pdfWriter->setPageSize(QPageSize::A4);

    return pdfWriter;
}

QByteArray readFileLine(QFile& file, qint64 maxSize)
{
    return file.readLine(maxSize).replace("\r\n", "\n");
}

void PainterTestCommon::compareFiles(const QString& actualFilePath, const QString& expectedFilePath)
{
    QFile expectedFile(expectedFilePath);
    QVERIFY(expectedFile.open(QIODevice::ReadOnly));

    QFile actualFile(actualFilePath);
    QVERIFY(actualFile.open(QIODevice::ReadOnly));

    QVERIFY(expectedFile.isOpen());
    QVERIFY(actualFile.isOpen());

    qint64 readedSize       = 1024;
    QByteArray expectedData = readFileLine(expectedFile, readedSize);
    QByteArray actualData   = readFileLine(actualFile, readedSize);

    while (!expectedData.isEmpty() && !actualData.isEmpty()) {
        QCOMPARE(actualData, expectedData);
        expectedData = readFileLine(expectedFile, readedSize);
        actualData   = readFileLine(actualFile, readedSize);
    }
}
