#include <QPainter>
#include <QSignalSpy>
#include <QTest>

#include "Painter/controllers/ListableItemListModel.hpp"
#include "Painter/controllers/PaintableBorder.hpp"
#include "Painter/controllers/PaintableList.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"
#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"

class Test_PaintableList : public QObject
{
    Q_OBJECT

  public:
    Test_PaintableList() = default;

  private slots:
    void listData();
    void listSetData();
};

void Test_PaintableList::listData()
{
    QPointer<painter::controller::PaintableRectangle> rectangle1 =
      new painter::controller::PaintableRectangle(this);

    painter::data::PositionData position { 10, 20, 30, 40 };

    rectangle1->setPositionData(position);
    rectangle1->setColor("pink");
    rectangle1->setOpacity(20);
    rectangle1->setRadius(0.60);
    rectangle1->setMode(Qt::SizeMode::AbsoluteSize);
    rectangle1->border()->setColor("yellow");
    rectangle1->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableRectangle> rectangle2 =
      new painter::controller::PaintableRectangle(this);

    position = painter::data::PositionData(50, 60, 70, 80);

    rectangle2->setPositionData(position);
    rectangle2->setColor("blue");
    rectangle2->setOpacity(40);
    rectangle2->setRadius(0.80);
    rectangle2->setMode(Qt::SizeMode::AbsoluteSize);

    rectangle2->border()->setColor("purple");
    rectangle2->border()->setBorderWidth(5);

    QPointer<painter::controller::PaintableText> text1 = new painter::controller::PaintableText(this);

    position = painter::data::PositionData(200, 400, 600, 800);

    text1->setPositionData(position);
    text1->setColor("pink");
    text1->setOpacity(85);
    text1->setText("Patate");
    text1->setFlags(42);
    text1->setPointSize(12);
    text1->setFontFamily("Roboto");

    text1->background()->setColor("red");
    text1->background()->setRadius(0.60);
    text1->background()->setMode(Qt::SizeMode::AbsoluteSize);

    text1->border()->setColor("yellow");
    text1->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableText> text2 = new painter::controller::PaintableText(this);

    position = painter::data::PositionData(20, 40, 60, 80);

    text2->setPositionData(position);
    text2->setColor("green");
    text2->setOpacity(45);
    text2->setText("Frites");
    text2->setFlags(32);
    text2->setPointSize(8);
    text2->setFontFamily("Arial");

    text2->background()->setColor("red");
    text2->background()->setRadius(0.60);
    text2->background()->setMode(Qt::SizeMode::AbsoluteSize);

    text2->border()->setColor("yellow");
    text2->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableRectangle> rectangle3 =
      new painter::controller::PaintableRectangle(this);

    {
        painter::controller::PaintableList paintableList;

        painter::data::PositionData paintableListPosition { 35, 45, 55, 65 };

        paintableList.setPositionData(paintableListPosition);
        paintableList.setPrimaryRepetitionDirection(
          painter::controller::PaintableList::RepetitionDirection::BottomRepetition);
        paintableList.setSecondaryRepetitionDirection(
          painter::controller::PaintableList::RepetitionDirection::LeftRepetition);

        paintableList.listableItemListModel()->addPaintableItem(rectangle1);
        paintableList.listableItemListModel()->addPaintableItem(rectangle2);
        paintableList.listableItemListModel()->addPaintableItem(text1);
        paintableList.listableItemListModel()->addPaintableItem(text2);
        paintableList.listableItemListModel()->addPaintableItem(rectangle3);

        painter::data::PaintableListData paintableListData = paintableList.objectData();

        QCOMPARE(paintableListData.position, paintableListPosition);
        QCOMPARE(paintableListData.primaryRepetitionDirection,
                 painter::data::PaintableListData::RepetitionDirection::BottomRepetition);
        QCOMPARE(paintableListData.secondaryRepetitionDirection,
                 painter::data::PaintableListData::RepetitionDirection::LeftRepetition);

        QCOMPARE(paintableListData.paintableItemListModel.paintableItemList.size(), 5);
        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          paintableListData.paintableItemListModel.paintableItemList.at(0)));
        QCOMPARE(std::get<painter::data::PaintableRectangleData>(
                   paintableListData.paintableItemListModel.paintableItemList.at(0)),
                 rectangle1->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          paintableListData.paintableItemListModel.paintableItemList.at(1)));
        QCOMPARE(std::get<painter::data::PaintableRectangleData>(
                   paintableListData.paintableItemListModel.paintableItemList.at(1)),
                 rectangle2->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableTextData>(
          paintableListData.paintableItemListModel.paintableItemList.at(2)));
        QCOMPARE(std::get<painter::data::PaintableTextData>(
                   paintableListData.paintableItemListModel.paintableItemList.at(2)),
                 text1->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableTextData>(
          paintableListData.paintableItemListModel.paintableItemList.at(3)));
        QCOMPARE(std::get<painter::data::PaintableTextData>(
                   paintableListData.paintableItemListModel.paintableItemList.at(3)),
                 text2->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          paintableListData.paintableItemListModel.paintableItemList.at(4)));
        QCOMPARE(std::get<painter::data::PaintableRectangleData>(
                   paintableListData.paintableItemListModel.paintableItemList.at(4)),
                 rectangle3->objectData());

        QCOMPARE(rectangle1->parent(), paintableList.listableItemListModel());
        QCOMPARE(rectangle2->parent(), paintableList.listableItemListModel());
        QCOMPARE(text1->parent(), paintableList.listableItemListModel());
        QCOMPARE(text2->parent(), paintableList.listableItemListModel());
        QCOMPARE(rectangle3->parent(), paintableList.listableItemListModel());

        QCOMPARE(rectangle1->parentList(), paintableList.listableItemListModel());
        QCOMPARE(rectangle2->parentList(), paintableList.listableItemListModel());
        QCOMPARE(text1->parentList(), paintableList.listableItemListModel());
        QCOMPARE(text2->parentList(), paintableList.listableItemListModel());
        QCOMPARE(rectangle3->parentList(), paintableList.listableItemListModel());
    }

    QVERIFY(rectangle1.isNull());
    QVERIFY(rectangle2.isNull());
    QVERIFY(text1.isNull());
    QVERIFY(text2.isNull());
    QVERIFY(rectangle3.isNull());
}

void Test_PaintableList::listSetData()
{
    painter::data::PaintableRectangleData paintableRectangleData1;

    painter::data::PositionData position { 100, 80, 90, 70 };

    paintableRectangleData1.position.position = position;
    paintableRectangleData1.position.mode     = Qt::SizeMode::RelativeSize;
    paintableRectangleData1.position.radius   = 0.42;

    QColor color("orange");
    color.setAlpha(40 * 2.55);

    paintableRectangleData1.color.color   = color;
    paintableRectangleData1.color.opacity = 40;

    QColor borderColor("green");
    borderColor.setAlpha(40 * 2.55);

    paintableRectangleData1.border.color.color   = borderColor;
    paintableRectangleData1.border.color.opacity = 40;
    paintableRectangleData1.border.borderWidth   = 84;

    painter::data::PaintableRectangleData paintableRectangleData2;

    position = painter::data::PositionData(600, 400, 200, 100);

    paintableRectangleData2.position.position = position;
    paintableRectangleData2.position.mode     = Qt::SizeMode::RelativeSize;
    paintableRectangleData2.position.radius   = 0.33;

    color = QColor("purple");
    color.setAlpha(80 * 2.55);

    paintableRectangleData2.color.color   = color;
    paintableRectangleData2.color.opacity = 80;

    borderColor = QColor("red");
    borderColor.setAlpha(80 * 2.55);

    paintableRectangleData2.border.color.color   = borderColor;
    paintableRectangleData2.border.color.opacity = 80;
    paintableRectangleData2.border.borderWidth   = 6;

    painter::data::PaintableTextData paintableTextData1;

    position = painter::data::PositionData(200, 150, 100, 50);

    paintableTextData1.background.position.position = position;
    paintableTextData1.background.position.radius   = 0.8;
    paintableTextData1.background.position.mode     = Qt::SizeMode::RelativeSize;

    QColor textColor("orange");
    textColor.setAlpha(40 * 2.55);

    paintableTextData1.color.color   = textColor;
    paintableTextData1.color.opacity = 40;
    paintableTextData1.text          = "Chips";
    paintableTextData1.flags         = 42;
    paintableTextData1.pointSize     = 12;
    paintableTextData1.fontFamily    = "Arial";

    QColor backgroundColor("green");
    backgroundColor.setAlpha(40 * 2.55);

    paintableTextData1.background.color.color   = backgroundColor;
    paintableTextData1.background.color.opacity = 40;

    borderColor = QColor("blue");
    borderColor.setAlpha(40 * 2.55);

    paintableTextData1.background.border.color.color   = borderColor;
    paintableTextData1.background.border.color.opacity = 40;
    paintableTextData1.background.border.borderWidth   = 3;

    painter::data::PaintableTextData paintableTextData2;

    position = painter::data::PositionData(500, 500, 500, 500);

    paintableTextData2.background.position.position = position;
    paintableTextData2.background.position.radius   = 0.6;
    paintableTextData2.background.position.mode     = Qt::SizeMode::RelativeSize;

    textColor = QColor("orange");
    textColor.setAlpha(90 * 2.55);

    paintableTextData2.color.color   = textColor;
    paintableTextData2.color.opacity = 90;
    paintableTextData2.text          = "Frite";
    paintableTextData2.flags         = 6;
    paintableTextData2.pointSize     = 14;
    paintableTextData2.fontFamily    = "Roboto";

    backgroundColor = QColor("blue");
    backgroundColor.setAlpha(80 * 2.55);

    paintableTextData2.background.color.color   = backgroundColor;
    paintableTextData2.background.color.opacity = 80;

    borderColor = QColor("cyan");
    borderColor.setAlpha(80 * 2.55);

    paintableTextData2.background.border.color.color   = borderColor;
    paintableTextData2.background.border.color.opacity = 80;
    paintableTextData2.background.border.borderWidth   = 3;

    painter::data::PaintableRectangleData paintableRectangleData3;

    //-------------------------------------------------------
    // Test constructor with data
    //-------------------------------------------------------

    painter::data::PaintableListData paintableListData;

    position = painter::data::PositionData(1000, 1000, 1000, 1000);
    paintableListData.primaryRepetitionDirection =
      painter::data::PaintableListData::RepetitionDirection::RightRepetition;

    paintableListData.position = position;
    paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData2);
    paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData1);
    paintableListData.paintableItemListModel.paintableItemList.append(paintableTextData1);
    paintableListData.paintableItemListModel.paintableItemList.append(paintableTextData2);
    paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData3);

    QList<QPointer<painter::controller::InterfaceQmlPaintableItem>> listOfpaintableItems;

    {
        painter::controller::PaintableList paintableList(paintableListData);

        QCOMPARE(paintableList.positionData(), position);
        QCOMPARE(paintableList.primaryRepetitionDirection(),
                 painter::controller::PaintableList::RepetitionDirection::RightRepetition);
        QCOMPARE(paintableList.secondaryRepetitionDirection(),
                 painter::controller::PaintableList::RepetitionDirection::NoRepetition);

        QCOMPARE(paintableList.listableItemListModel()->rowCount(), 5);

        painter::controller::InterfaceQmlPaintableItem* paintableItem =
          paintableList.listableItemListModel()->at(0);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData2);

        paintableItem = paintableList.listableItemListModel()->at(1);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData1);

        paintableItem = paintableList.listableItemListModel()->at(2);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(),
                 paintableTextData1);

        paintableItem = paintableList.listableItemListModel()->at(3);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(),
                 paintableTextData2);

        paintableItem = paintableList.listableItemListModel()->at(4);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData3);

        QCOMPARE(paintableList.objectData(), paintableListData);

        for (int i = 0; i < 5; ++i)
        {
            QPointer<painter::controller::InterfaceQmlPaintableItem> item =
              paintableList.listableItemListModel()->at(i);
            listOfpaintableItems.append(item);
            QCOMPARE(item->parent(), paintableList.listableItemListModel());

            if (painter::controller::AbstractListableItem* listableItem =
                  qobject_cast<painter::controller::AbstractListableItem*>(item.get()))
            {
                QCOMPARE(listableItem->parentList(), paintableList.listableItemListModel());
            }
        }

        for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems)
        {
            QVERIFY(item);
        }

        //-------------------------------------------------------
        // Test set data
        //-------------------------------------------------------

        paintableListData.paintableItemListModel.paintableItemList.clear();

        position = painter::data::PositionData(666, 666, 666, 666);

        paintableListData.position = position;
        paintableListData.primaryRepetitionDirection =
          painter::data::PaintableListData::RepetitionDirection::TopRepetition;
        paintableListData.secondaryRepetitionDirection =
          painter::data::PaintableListData::RepetitionDirection::BottomRepetition;

        paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData1);
        paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData2);
        paintableListData.paintableItemListModel.paintableItemList.append(paintableTextData1);
        paintableListData.paintableItemListModel.paintableItemList.append(paintableTextData2);
        paintableListData.paintableItemListModel.paintableItemList.append(paintableRectangleData3);

        QSignalSpy xChangedSpy(&paintableList, &painter::controller::PaintableList::xChanged);
        QSignalSpy yChangedSpy(&paintableList, &painter::controller::PaintableList::yChanged);
        QSignalSpy widthChangedSpy(&paintableList, &painter::controller::PaintableList::widthChanged);
        QSignalSpy heightChangedSpy(&paintableList, &painter::controller::PaintableList::heightChanged);
        QSignalSpy primaryRepetitionDirectionChangedSpy(
          &paintableList, &painter::controller::PaintableList::primaryRepetitionDirectionChanged);
        QSignalSpy secondaryRepetitionDirectionChangedSpy(
          &paintableList, &painter::controller::PaintableList::secondaryRepetitionDirectionChanged);
        QSignalSpy modelResetSpy(paintableList.listableItemListModel(), &QAbstractListModel::modelReset);

        paintableList.setObjectData(paintableListData);

        // After the set all previous items have been deleted
        QCOMPARE(listOfpaintableItems.size(), 5);
        for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems)
        {
            QTRY_VERIFY(item.isNull());
        }
        listOfpaintableItems.clear();

        QCOMPARE(xChangedSpy.size(), 1);
        QCOMPARE(yChangedSpy.size(), 1);
        QCOMPARE(widthChangedSpy.size(), 1);
        QCOMPARE(heightChangedSpy.size(), 1);
        QCOMPARE(primaryRepetitionDirectionChangedSpy.size(), 1);
        QCOMPARE(secondaryRepetitionDirectionChangedSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);

        QCOMPARE(paintableList.positionData(), position);
        QCOMPARE(paintableList.primaryRepetitionDirection(),
                 painter::controller::PaintableList::RepetitionDirection::TopRepetition);
        QCOMPARE(paintableList.secondaryRepetitionDirection(),
                 painter::controller::PaintableList::RepetitionDirection::BottomRepetition);

        QCOMPARE(paintableList.listableItemListModel()->rowCount(), 5);

        paintableItem = paintableList.listableItemListModel()->at(0);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData1);

        paintableItem = paintableList.listableItemListModel()->at(1);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData2);

        paintableItem = paintableList.listableItemListModel()->at(2);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(),
                 paintableTextData1);

        paintableItem = paintableList.listableItemListModel()->at(3);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(),
                 paintableTextData2);

        paintableItem = paintableList.listableItemListModel()->at(4);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(),
                 paintableRectangleData3);

        QCOMPARE(paintableList.objectData(), paintableListData);

        for (int i = 0; i < 5; ++i)
        {
            QPointer<painter::controller::InterfaceQmlPaintableItem> item =
              paintableList.listableItemListModel()->at(i);
            listOfpaintableItems.append(item);
            QCOMPARE(item->parent(), paintableList.listableItemListModel());

            if (painter::controller::AbstractListableItem* listableItem =
                  qobject_cast<painter::controller::AbstractListableItem*>(item.get()))
            {
                QCOMPARE(listableItem->parentList(), paintableList.listableItemListModel());
            }
        }
    }

    QCOMPARE(listOfpaintableItems.size(), 5);
    for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems)
    {
        QVERIFY(item.isNull());
    }
}

QTEST_MAIN(Test_PaintableList)

#include "Test_PaintableList.moc"
