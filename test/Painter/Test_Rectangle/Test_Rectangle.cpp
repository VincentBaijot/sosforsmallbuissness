#include <QApplication>
#include <QPainter>
#include <QSignalSpy>
#include <QtTest/QTest>

#include "Painter/PainterTestCommon/PainterTestCommon.hpp"
#include "Painter/controllers/PaintableBorder.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/data/PaintableRectangleData.hpp"

class Test_Rectangle : public PainterTestCommon
{
    Q_OBJECT

  public:
    Test_Rectangle() = default;

  private slots:
    void initTestCase();
    void rectanglePosition();
    void rectangleSize();
    void rectangleColor();
    void rectangleOpacity();
    void rectangleRadius();
    void rectangleBorderWidth();
    void rectangleBorderColor();
    void rectangleBorderRadius();
    void rectangleData();
    void rectangleSetData();

  private:
    QTemporaryDir _tempDir;
};

void Test_Rectangle::initTestCase()
{
    QVERIFY(_tempDir.isValid());
    QFont font("Segoe UI");
    font.setPixelSize(150);
    QApplication::setFont(font);
}

void Test_Rectangle::rectanglePosition()
{
    QString generatedFilePath(_tempDir.path() + "/rectanglePosition.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectanglePosition.svg");
}

void Test_Rectangle::rectangleSize()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleSize.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 2000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 2000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 500, 500 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1500), 0, 1500, 1500 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 500), 500, 500 });
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 2000),
                                               static_cast<qreal>(painter.device()->height() - 2000),
                                               2000,
                                               2000 });
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleSize.svg");
}

void Test_Rectangle::rectangleColor()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleColor.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        rectangle.setColor("red");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        rectangle.setColor("yellow");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        rectangle.setColor("green");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        rectangle.setColor("blue");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        rectangle.setColor("purple");
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        rectangle.setColor("grey");
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleColor.svg");
}

void Test_Rectangle::rectangleOpacity()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleOpacity.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;

        rectangle.border()->setColor("red");
        rectangle.border()->setBorderWidth(10);

        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.setOpacity(20);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        rectangle.setOpacity(50);
        rectangle.setColor("yellow");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        rectangle.setOpacity(70);
        rectangle.setColor("green");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        rectangle.setColor("blue");
        rectangle.setOpacity(100);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        rectangle.setColor("purple");
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        rectangle.setColor("black");
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleOpacity.svg");
}

void Test_Rectangle::rectangleRadius()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleRadius.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.setMode(Qt::SizeMode::AbsoluteSize);
        rectangle.setRadius(50);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        rectangle.setRadius(0);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        rectangle.setRadius(100);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        rectangle.setRadius(200);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        rectangle.setMode(Qt::SizeMode::RelativeSize);
        rectangle.setRadius(50);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        rectangle.setRadius(100);
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        rectangle.setRadius(0);
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleRadius.svg");
}

void Test_Rectangle::rectangleBorderWidth()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleBorderWidth.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.border()->setColor("red");
        rectangle.border()->setBorderWidth(10);

        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 2000, 1000 });
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 2000 });
        rectangle.border()->setBorderWidth(0);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 500, 500 });
        rectangle.border()->setBorderWidth(50);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1500), 0, 1500, 1500 });
        rectangle.border()->setBorderWidth(100);
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 2000),
                                               static_cast<qreal>(painter.device()->height() - 2000),
                                               2000,
                                               2000 });
        rectangle.border()->setBorderWidth(3000);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 500), 500, 500 });
        rectangle.border()->setBorderWidth(70);
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleBorderWidth.svg");
}

void Test_Rectangle::rectangleBorderColor()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleBorderColor.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.border()->setColor("red");
        rectangle.border()->setBorderWidth(50);

        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 2000, 1000 });
        rectangle.border()->setColor("blue");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 2000 });
        rectangle.border()->setColor("green");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 500, 500 });
        rectangle.border()->setColor("orange");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1500), 0, 1500, 1500 });
        rectangle.setColor("grey");
        rectangle.border()->setColor("black");
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 2000),
                                               static_cast<qreal>(painter.device()->height() - 2000),
                                               2000,
                                               2000 });
        rectangle.setColor("black");
        rectangle.border()->setColor("yellow");
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 500), 500, 500 });
        rectangle.border()->setColor("purple");
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleBorderColor.svg");
}

void Test_Rectangle::rectangleBorderRadius()
{
    QString generatedFilePath(_tempDir.path() + "/rectangleBorderRadius.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableRectangle rectangle;
        rectangle.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        rectangle.setColor("black");
        rectangle.border()->setColor("red");
        rectangle.border()->setBorderWidth(10);

        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 0, 2000, 1000 });
        rectangle.setRadius(100);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 0, 1050, 1000, 2000 });
        rectangle.setRadius(40);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData { 1050, 1050, 500, 500 });
        rectangle.setRadius(500);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1500), 0, 1500, 1500 });
        rectangle.setRadius(0);
        rectangle.paint(&painter);

        rectangle.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 2000),
                                               static_cast<qreal>(painter.device()->height() - 2000),
                                               2000,
                                               2000 });
        rectangle.setMode(Qt::SizeMode::RelativeSize);
        rectangle.setRadius(40);
        rectangle.paint(&painter);

        rectangle.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 500), 500, 500 });
        rectangle.border()->setBorderWidth(70);
        rectangle.setMode(Qt::SizeMode::RelativeSize);
        rectangle.setRadius(100);
        rectangle.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/rectangleBorderRadius.svg");
}

void Test_Rectangle::rectangleData()
{
    painter::controller::PaintableRectangle rectangle;

    painter::data::PositionData position { 10, 20, 30, 40 };

    rectangle.setPositionData(position);
    rectangle.setColor("pink");
    rectangle.setOpacity(20);
    rectangle.setRadius(0.60);
    rectangle.setMode(Qt::SizeMode::AbsoluteSize);

    rectangle.border()->setColor("yellow");
    rectangle.border()->setBorderWidth(20);

    painter::data::PaintableRectangleData paintableRectangleData = rectangle.objectData();

    QCOMPARE(paintableRectangleData.position.position, position);
    QCOMPARE(paintableRectangleData.position.mode, Qt::SizeMode::AbsoluteSize);
    QCOMPARE(paintableRectangleData.position.radius, 0.60);

    QColor color("pink");
    color.setAlpha(20 * 2.55);

    QCOMPARE(paintableRectangleData.color.color, color);
    QCOMPARE(paintableRectangleData.color.opacity, 20);

    QColor borderColor("yellow");
    borderColor.setAlpha(20 * 2.55);

    QCOMPARE(paintableRectangleData.border.color.color, borderColor);
    QCOMPARE(paintableRectangleData.border.color.opacity, 20);
    QCOMPARE(paintableRectangleData.border.borderWidth, 20);
}

void Test_Rectangle::rectangleSetData()
{
    painter::data::PaintableRectangleData paintableRectangleData;

    //-------------------------------------------------------
    // Test constructor with data
    //-------------------------------------------------------

    painter::data::PositionData position { 600, 700, 800, 900 };

    paintableRectangleData.position.position = position;
    paintableRectangleData.position.mode     = Qt::SizeMode::AbsoluteSize;
    paintableRectangleData.position.radius   = 0.32;

    QColor color("purple");
    color.setAlpha(60 * 2.55);

    paintableRectangleData.color.color   = color;
    paintableRectangleData.color.opacity = 60;

    QColor borderColor("red");
    borderColor.setAlpha(60 * 2.55);

    paintableRectangleData.border.color.color   = borderColor;
    paintableRectangleData.border.color.opacity = 60;
    paintableRectangleData.border.borderWidth   = 3;

    painter::controller::PaintableRectangle rectangle { paintableRectangleData };

    QCOMPARE(rectangle.positionData(), position);
    QCOMPARE(rectangle.color(), color);
    QCOMPARE(rectangle.opacity(), 60);
    QCOMPARE(rectangle.radius(), 0.32);
    QCOMPARE(rectangle.mode(), Qt::SizeMode::AbsoluteSize);

    QCOMPARE(rectangle.border()->color(), borderColor);
    QCOMPARE(rectangle.border()->borderWidth(), 3);

    QCOMPARE(rectangle.objectData(), paintableRectangleData);

    //-------------------------------------------------------
    // Test set data
    //-------------------------------------------------------

    position = painter::data::PositionData { 100, 80, 90, 70 };

    paintableRectangleData.position.position = position;
    paintableRectangleData.position.mode     = Qt::SizeMode::RelativeSize;
    paintableRectangleData.position.radius   = 0.42;

    color = QColor("orange");
    color.setAlpha(40 * 2.55);

    paintableRectangleData.color.color   = color;
    paintableRectangleData.color.opacity = 40;

    borderColor = QColor("green");
    borderColor.setAlpha(40 * 2.55);

    paintableRectangleData.border.color.color   = borderColor;
    paintableRectangleData.border.color.opacity = 40;
    paintableRectangleData.border.borderWidth   = 84;

    QSignalSpy xChangedSpy(&rectangle, &painter::controller::PaintableRectangle::xChanged);
    QSignalSpy yChangedSpy(&rectangle, &painter::controller::PaintableRectangle::yChanged);
    QSignalSpy widthChangedSpy(&rectangle, &painter::controller::PaintableRectangle::widthChanged);
    QSignalSpy heightChangedSpy(&rectangle, &painter::controller::PaintableRectangle::heightChanged);
    QSignalSpy colorChangedSpy(&rectangle, &painter::controller::PaintableRectangle::colorChanged);
    QSignalSpy opacityChangedSpy(&rectangle, &painter::controller::PaintableRectangle::opacityChanged);
    QSignalSpy radiusChangedSpy(&rectangle, &painter::controller::PaintableRectangle::radiusChanged);
    QSignalSpy modeChangedSpy(&rectangle, &painter::controller::PaintableRectangle::modeChanged);
    QSignalSpy calculatedRadiusChangedSpy(&rectangle, &painter::controller::PaintableRectangle::calculatedRadiusChanged);

    rectangle.setObjectData(paintableRectangleData);

    QCOMPARE(xChangedSpy.size(), 1);
    QCOMPARE(yChangedSpy.size(), 1);
    QCOMPARE(widthChangedSpy.size(), 1);
    QCOMPARE(heightChangedSpy.size(), 1);
    QCOMPARE(colorChangedSpy.size(), 2);
    QCOMPARE(opacityChangedSpy.size(), 1);
    QCOMPARE(radiusChangedSpy.size(), 1);
    QCOMPARE(modeChangedSpy.size(), 1);
    QCOMPARE(calculatedRadiusChangedSpy.size(), 4);

    QCOMPARE(rectangle.positionData(), position);
    QCOMPARE(rectangle.color(), color);
    QCOMPARE(rectangle.opacity(), 40);
    QCOMPARE(rectangle.radius(), 0.42);
    QCOMPARE(rectangle.mode(), Qt::SizeMode::RelativeSize);

    QCOMPARE(rectangle.border()->color(), borderColor);
    QCOMPARE(rectangle.border()->borderWidth(), 84);

    QCOMPARE(rectangle.objectData(), paintableRectangleData);
}

QTEST_MAIN(Test_Rectangle)

#include "Test_Rectangle.moc"
