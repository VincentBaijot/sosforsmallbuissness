#include <QtTest/QTest>

#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"
#include "Painter/services/PainterXmlConstants.hpp"
#include "Painter/services/PainterXmlSerialization.hpp"

class Test_PainterXmlSerialization : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void test_readPaintableRectangleData_data();
    void test_readPaintableRectangleData();

    void test_writePaintableRectangleData();

    void test_readPaintableTextData_data();
    void test_readPaintableTextData();

    void test_writePaintableTextData();

    void test_readPaintableListData_data();
    void test_readPaintableListData();

    void test_writePaintableListData();

  private:
    static QByteArray writePaintableRectangleXml(
      const painter::data::PaintableRectangleData& paintableRectangleData);
    static QByteArray writePaintableTextXml(const painter::data::PaintableTextData& paintableTextData);
    static QByteArray writeVariantListableItemData(
      const painter::data::VariantListableItemData& variantListableItem);
};

void Test_PainterXmlSerialization::test_readPaintableRectangleData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<painter::data::PaintableRectangleData>>("expectedPaintableRectangleData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableRectangle>\n"
                                                 "    <RectanglePosition>\n"
                                                 "        <PositionData>\n"
                                                 "            <x>20.546</x>\n"
                                                 "            <y>50.32</y>\n"
                                                 "            <width>100.21</width>\n"
                                                 "            <height>200.42</height>\n"
                                                 "        </PositionData>\n"
                                                 "        <Radius>42</Radius>\n"
                                                 "        <SizeMode>0</SizeMode>\n"
                                                 "    </RectanglePosition>\n"
                                                 "    <ColorData>\n"
                                                 "        <Opacity>100</Opacity>\n"
                                                 "        <Color>\n"
                                                 "            <r>255</r>\n"
                                                 "            <g>191</g>\n"
                                                 "            <b>128</b>\n"
                                                 "            <a>64</a>\n"
                                                 "        </Color>\n"
                                                 "    </ColorData>\n"
                                                 "    <PaintableBorder>\n"
                                                 "        <BorderWidth>2.365</BorderWidth>\n"
                                                 "        <ColorData>\n"
                                                 "            <Opacity>100</Opacity>\n"
                                                 "            <Color>\n"
                                                 "                <r>128</r>\n"
                                                 "                <g>0</g>\n"
                                                 "                <b>128</b>\n"
                                                 "                <a>255</a>\n"
                                                 "            </Color>\n"
                                                 "        </ColorData>\n"
                                                 "    </PaintableBorder>\n"
                                                 "</PaintableRectangle>\n");

        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData =
          painter::data::PaintableRectangleData {
              painter::data::RectanglePositionData {
                painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
              painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
              painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } }
          };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableRectangleData;
    }

    // Other order
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableRectangle>\n"
                                                 "    <ColorData>\n"
                                                 "        <Color>\n"
                                                 "            <a>64</a>\n"
                                                 "            <g>191</g>\n"
                                                 "            <r>255</r>\n"
                                                 "            <b>128</b>\n"
                                                 "        </Color>\n"
                                                 "        <Opacity>100</Opacity>\n"
                                                 "    </ColorData>\n"
                                                 "    <PaintableBorder>\n"
                                                 "        <ColorData>\n"
                                                 "            <Opacity>100</Opacity>\n"
                                                 "            <Color>\n"
                                                 "                <r>128</r>\n"
                                                 "                <g>0</g>\n"
                                                 "                <b>128</b>\n"
                                                 "                <a>255</a>\n"
                                                 "            </Color>\n"
                                                 "        </ColorData>\n"
                                                 "        <BorderWidth>2.365</BorderWidth>\n"
                                                 "    </PaintableBorder>\n"
                                                 "    <RectanglePosition>\n"
                                                 "        <SizeMode>0</SizeMode>\n"
                                                 "        <PositionData>\n"
                                                 "            <y>50.32</y>\n"
                                                 "            <width>100.21</width>\n"
                                                 "            <x>20.546</x>\n"
                                                 "            <height>200.42</height>\n"
                                                 "        </PositionData>\n"
                                                 "        <Radius>42</Radius>\n"
                                                 "    </RectanglePosition>\n"
                                                 "</PaintableRectangle>\n");

        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData =
          painter::data::PaintableRectangleData {
              painter::data::RectanglePositionData {
                painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
              painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
              painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } }
          };

        QTest::newRow("Other_order") << arrayData << expectedPaintableRectangleData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<PaintableRectangle>"
                                                 "<ColorData/>"
                                                 "<PaintableBorder/>"
                                                 "<RectanglePosition/>"
                                                 "</PaintableRectangle>");

        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData =
          painter::data::PaintableRectangleData { painter::data::RectanglePositionData {},
                                                  painter::data::ColorData {},
                                                  painter::data::PaintableBorderData {} };

        QTest::newRow("Self_Closing_parts") << arrayData << expectedPaintableRectangleData;
    }

    // Empty paintable rectangle
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableRectangle>\n"
                                                 "</PaintableRectangle>\n");
        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData =
          painter::data::PaintableRectangleData {};
        QTest::newRow("Empty_paintable_rectangle") << arrayData << expectedPaintableRectangleData;
    }

    // Empty self closing
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<PaintableRectangle/>");
        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData =
          painter::data::PaintableRectangleData {};
        QTest::newRow("Empty_self_closing_paintable_rectangle") << arrayData << expectedPaintableRectangleData;
    }

    // Not paintable rectangle
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData = std::nullopt;
        QTest::newRow("Not_paintable_rectangle") << arrayData << expectedPaintableRectangleData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Some random text");
        std::optional<painter::data::PaintableRectangleData> expectedPaintableRectangleData = std::nullopt;
        QTest::newRow("Not_xml") << arrayData << expectedPaintableRectangleData;
    }
}

void Test_PainterXmlSerialization::test_readPaintableRectangleData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<painter::data::PaintableRectangleData>, expectedPaintableRectangleData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<painter::data::PaintableRectangleData> actualPaintableRectangleData =
      painter::service::readPaintableRectangleData(streamReader);

    if (expectedPaintableRectangleData.has_value())
    {
        QCOMPARE(actualPaintableRectangleData.value(), expectedPaintableRectangleData.value());
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), painter::service::paintableRectangleElement);
    }
    else { QVERIFY(!actualPaintableRectangleData.has_value()); }
}

void Test_PainterXmlSerialization::test_writePaintableRectangleData()
{
    painter::data::PaintableRectangleData paintableRectangleData = painter::data::PaintableRectangleData {
        painter::data::RectanglePositionData {
          painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
        painter::data::ColorData { 64, QColor(255, 191, 128, 64) },
        painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } }
    };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    painter::service::writePaintableRectangleData(streamWriter, paintableRectangleData);

    streamWriter.writeEndDocument();

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
             "<PaintableRectangle>\n"
             "    <RectanglePosition>\n"
             "        <PositionData>\n"
             "            <x>20.546</x>\n"
             "            <y>50.32</y>\n"
             "            <width>100.21</width>\n"
             "            <height>200.42</height>\n"
             "        </PositionData>\n"
             "        <Radius>42</Radius>\n"
             "        <SizeMode>0</SizeMode>\n"
             "    </RectanglePosition>\n"
             "    <ColorData>\n"
             "        <Opacity>64</Opacity>\n"
             "        <Color>\n"
             "            <r>255</r>\n"
             "            <g>191</g>\n"
             "            <b>128</b>\n"
             "            <a>64</a>\n"
             "        </Color>\n"
             "    </ColorData>\n"
             "    <PaintableBorder>\n"
             "        <BorderWidth>2.365</BorderWidth>\n"
             "        <ColorData>\n"
             "            <Opacity>100</Opacity>\n"
             "            <Color>\n"
             "                <r>128</r>\n"
             "                <g>0</g>\n"
             "                <b>128</b>\n"
             "                <a>255</a>\n"
             "            </Color>\n"
             "        </ColorData>\n"
             "    </PaintableBorder>\n"
             "</PaintableRectangle>\n");
}

void Test_PainterXmlSerialization::test_readPaintableTextData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<painter::data::PaintableTextData>>("expectedPaintableTextData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableText>\n"
                                                 "    <ColorData>\n"
                                                 "        <Opacity>34</Opacity>\n"
                                                 "        <Color>\n"
                                                 "            <r>255</r>\n"
                                                 "            <g>191</g>\n"
                                                 "            <b>128</b>\n"
                                                 "            <a>34</a>\n"
                                                 "        </Color>\n"
                                                 "    </ColorData>\n"
                                                 "    <PaintableRectangle>\n"
                                                 "        <RectanglePosition>\n"
                                                 "            <PositionData>\n"
                                                 "                <x>12.24</x>\n"
                                                 "                <y>32.56</y>\n"
                                                 "                <width>45.42</width>\n"
                                                 "                <height>19.58</height>\n"
                                                 "            </PositionData>\n"
                                                 "            <Radius>0</Radius>\n"
                                                 "            <SizeMode>1</SizeMode>\n"
                                                 "        </RectanglePosition>\n"
                                                 "        <ColorData>\n"
                                                 "            <Opacity>85</Opacity>\n"
                                                 "            <Color>\n"
                                                 "                <r>255</r>\n"
                                                 "                <g>191</g>\n"
                                                 "                <b>128</b>\n"
                                                 "                <a>85</a>\n"
                                                 "            </Color>\n"
                                                 "        </ColorData>\n"
                                                 "        <PaintableBorder>\n"
                                                 "            <BorderWidth>25</BorderWidth>\n"
                                                 "            <ColorData>\n"
                                                 "                <Opacity>55</Opacity>\n"
                                                 "                <Color>\n"
                                                 "                    <r>161</r>\n"
                                                 "                    <g>162</g>\n"
                                                 "                    <b>163</b>\n"
                                                 "                    <a>164</a>\n"
                                                 "                </Color>\n"
                                                 "            </ColorData>\n"
                                                 "        </PaintableBorder>\n"
                                                 "    </PaintableRectangle>\n"
                                                 "    <Text>Some text</Text>\n"
                                                 "    <FontFamily>Arial</FontFamily>\n"
                                                 "    <Flags>12</Flags>\n"
                                                 "    <PointSize>25</PointSize>\n"
                                                 "</PaintableText>\n");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData =
          painter::data::PaintableTextData {
              painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
              painter::data::PaintableRectangleData {
                painter::data::RectanglePositionData {
                  painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                painter::data::PaintableBorderData {
                  25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
              "Some text",
              "Arial",
              12,
              25
          };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableTextData;
    }

    // Other order
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableText>\n"
                                                 "    <Flags>12</Flags>\n"
                                                 "    <PaintableRectangle>\n"
                                                 "        <RectanglePosition>\n"
                                                 "            <PositionData>\n"
                                                 "                <x>12.24</x>\n"
                                                 "                <y>32.56</y>\n"
                                                 "                <width>45.42</width>\n"
                                                 "                <height>19.58</height>\n"
                                                 "            </PositionData>\n"
                                                 "            <Radius>0</Radius>\n"
                                                 "            <SizeMode>1</SizeMode>\n"
                                                 "        </RectanglePosition>\n"
                                                 "        <ColorData>\n"
                                                 "            <Opacity>85</Opacity>\n"
                                                 "            <Color>\n"
                                                 "                <r>255</r>\n"
                                                 "                <g>191</g>\n"
                                                 "                <b>128</b>\n"
                                                 "                <a>85</a>\n"
                                                 "            </Color>\n"
                                                 "        </ColorData>\n"
                                                 "        <PaintableBorder>\n"
                                                 "            <BorderWidth>25</BorderWidth>\n"
                                                 "            <ColorData>\n"
                                                 "                <Opacity>55</Opacity>\n"
                                                 "                <Color>\n"
                                                 "                    <r>161</r>\n"
                                                 "                    <g>162</g>\n"
                                                 "                    <b>163</b>\n"
                                                 "                    <a>164</a>\n"
                                                 "                </Color>\n"
                                                 "            </ColorData>\n"
                                                 "        </PaintableBorder>\n"
                                                 "    </PaintableRectangle>\n"
                                                 "    <Text>Some text</Text>\n"
                                                 "    <FontFamily>Arial</FontFamily>\n"
                                                 "    <ColorData>\n"
                                                 "        <Opacity>34</Opacity>\n"
                                                 "        <Color>\n"
                                                 "            <r>255</r>\n"
                                                 "            <g>191</g>\n"
                                                 "            <b>128</b>\n"
                                                 "            <a>34</a>\n"
                                                 "        </Color>\n"
                                                 "    </ColorData>\n"
                                                 "    <PointSize>25</PointSize>\n"
                                                 "</PaintableText>\n");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData =
          painter::data::PaintableTextData {
              painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
              painter::data::PaintableRectangleData {
                painter::data::RectanglePositionData {
                  painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                painter::data::PaintableBorderData {
                  25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
              "Some text",
              "Arial",
              12,
              25
          };

        QTest::newRow("Other_order") << arrayData << expectedPaintableTextData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<PaintableText>"
                                                 "<ColorData/>"
                                                 "<PaintableRectangle/>"
                                                 "<Text/>"
                                                 "<FontFamily/>"
                                                 "<Flags/>"
                                                 "<PointSize/>"
                                                 "</PaintableText>");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData =
          painter::data::PaintableTextData {};

        QTest::newRow("Self_closing_parts") << arrayData << expectedPaintableTextData;
    }

    // Empty PaintableText
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableText>\n"
                                                 "</PaintableText>\n");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData =
          painter::data::PaintableTextData {};

        QTest::newRow("Empty_PaintableText") << arrayData << expectedPaintableTextData;
    }

    // Empty self closing PaintableText
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableText/>");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData =
          painter::data::PaintableTextData {};

        QTest::newRow("Empty_Self_Closing_PaintableText") << arrayData << expectedPaintableTextData;
    }

    // Not PaintableText
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData = std::nullopt;

        QTest::newRow("Not_PaintableText") << arrayData << expectedPaintableTextData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Some random text");

        std::optional<painter::data::PaintableTextData> expectedPaintableTextData = std::nullopt;

        QTest::newRow("Not_xml") << arrayData << expectedPaintableTextData;
    }
}

void Test_PainterXmlSerialization::test_readPaintableTextData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<painter::data::PaintableTextData>, expectedPaintableTextData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<painter::data::PaintableTextData> actualPaintableTextData =
      painter::service::readPaintableTextData(streamReader);

    QCOMPARE(actualPaintableTextData, expectedPaintableTextData);

    if (expectedPaintableTextData.has_value())
    {
        QCOMPARE(actualPaintableTextData.value(), expectedPaintableTextData.value());
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), painter::service::paintableTextElement);
    }
    else { QVERIFY(!actualPaintableTextData.has_value()); }
}

void Test_PainterXmlSerialization::test_writePaintableTextData()
{
    painter::data::PaintableTextData paintableTextData = painter::data::PaintableTextData {
        painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
        painter::data::PaintableRectangleData {
          painter::data::RectanglePositionData {
            painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
          painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
          painter::data::PaintableBorderData { 25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
        "Some text",
        "Arial",
        12,
        25
    };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    painter::service::writePaintableTextData(streamWriter, paintableTextData);

    streamWriter.writeEndDocument();

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
             "<PaintableText>\n"
             "    <ColorData>\n"
             "        <Opacity>34</Opacity>\n"
             "        <Color>\n"
             "            <r>255</r>\n"
             "            <g>191</g>\n"
             "            <b>128</b>\n"
             "            <a>34</a>\n"
             "        </Color>\n"
             "    </ColorData>\n"
             "    <PaintableRectangle>\n"
             "        <RectanglePosition>\n"
             "            <PositionData>\n"
             "                <x>12.24</x>\n"
             "                <y>32.56</y>\n"
             "                <width>45.42</width>\n"
             "                <height>19.58</height>\n"
             "            </PositionData>\n"
             "            <Radius>0</Radius>\n"
             "            <SizeMode>1</SizeMode>\n"
             "        </RectanglePosition>\n"
             "        <ColorData>\n"
             "            <Opacity>85</Opacity>\n"
             "            <Color>\n"
             "                <r>255</r>\n"
             "                <g>191</g>\n"
             "                <b>128</b>\n"
             "                <a>85</a>\n"
             "            </Color>\n"
             "        </ColorData>\n"
             "        <PaintableBorder>\n"
             "            <BorderWidth>25</BorderWidth>\n"
             "            <ColorData>\n"
             "                <Opacity>55</Opacity>\n"
             "                <Color>\n"
             "                    <r>161</r>\n"
             "                    <g>162</g>\n"
             "                    <b>163</b>\n"
             "                    <a>164</a>\n"
             "                </Color>\n"
             "            </ColorData>\n"
             "        </PaintableBorder>\n"
             "    </PaintableRectangle>\n"
             "    <Text>Some text</Text>\n"
             "    <FontFamily>Arial</FontFamily>\n"
             "    <Flags>12</Flags>\n"
             "    <PointSize>25</PointSize>\n"
             "</PaintableText>\n");
}

void Test_PainterXmlSerialization::test_readPaintableListData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<painter::data::PaintableListData>>("expectedPaintableListData");

    // Same order than write
    {
        std::optional<painter::data::PaintableListData> expectedPaintableListData =
          painter::data::PaintableListData {
              painter::data::PositionData { 42.42, 53.53, 64.64, 75.75 },
              painter::data::PaintableListData::RepetitionDirection::LeftRepetition,
              painter::data::PaintableListData::RepetitionDirection::TopRepetition,
              painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
                painter::data::PaintableTextData {
                  painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
                  painter::data::PaintableRectangleData {
                    painter::data::RectanglePositionData {
                      painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                    painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                    painter::data::PaintableBorderData {
                      25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
                  "Some text",
                  "Arial",
                  12,
                  25 },

                painter::data::PaintableRectangleData {
                  painter::data::RectanglePositionData {
                    painter::data::PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                  painter::data::ColorData { 100, QColor(0, 0, 0) },
                  painter::data::PaintableBorderData { 0, painter::data::ColorData { 100, QColor(0, 0, 0) } } },

                painter::data::PaintableRectangleData {
                  painter::data::RectanglePositionData {
                    painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
                  painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
                  painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } } },

                painter::data::PaintableTextData {
                  painter::data::ColorData { 100, QColor(0, 0, 0) },
                  painter::data::PaintableRectangleData {
                    painter::data::RectanglePositionData {
                      painter::data::PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    painter::data::ColorData { 100, QColor(0, 0, 0) },
                    painter::data::PaintableBorderData { 0, painter::data::ColorData { 100, QColor(0, 0, 0) } } },
                  "",
                  "",
                  0,
                  0 },
                painter::data::PaintableRectangleData {},
                painter::data::PaintableTextData {} } }
          };

        QByteArray arrayData =
          QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<PaintableList>\n"
                            "    <PositionData>\n"
                            "        <x>42.42</x>\n"
                            "        <y>53.53</y>\n"
                            "        <width>64.64</width>\n"
                            "        <height>75.75</height>\n"
                            "    </PositionData>\n"
                            "    <PrimaryRepetitionDirection>4</PrimaryRepetitionDirection>\n"
                            "    <SecondaryRepetitionDirection>3</SecondaryRepetitionDirection>\n");

        arrayData.append(QByteArrayLiteral("    <PaintableItemList>\n"));

        for (const painter::data::VariantListableItemData& variantListableItem :
             expectedPaintableListData.value().paintableItemListModel.paintableItemList)
        {
            arrayData.append(writeVariantListableItemData(variantListableItem));
        }

        arrayData.append(QByteArrayLiteral("    </PaintableItemList>\n"
                                           "</PaintableList>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedPaintableListData;
    }

    // Other order
    {
        std::optional<painter::data::PaintableListData> expectedPaintableListData =
          painter::data::PaintableListData {
              painter::data::PositionData { 42.42, 53.53, 64.64, 75.75 },
              painter::data::PaintableListData::RepetitionDirection::LeftRepetition,
              painter::data::PaintableListData::RepetitionDirection::TopRepetition,
              painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
                painter::data::PaintableTextData {
                  painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
                  painter::data::PaintableRectangleData {
                    painter::data::RectanglePositionData {
                      painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                    painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                    painter::data::PaintableBorderData {
                      25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
                  "Some text",
                  "Arial",
                  12,
                  25 },

                painter::data::PaintableRectangleData {
                  painter::data::RectanglePositionData {
                    painter::data::PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                  painter::data::ColorData { 100, QColor(0, 0, 0) },
                  painter::data::PaintableBorderData { 0, painter::data::ColorData { 100, QColor(0, 0, 0) } } },

                painter::data::PaintableRectangleData {
                  painter::data::RectanglePositionData {
                    painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
                  painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
                  painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } } },

                painter::data::PaintableTextData {
                  painter::data::ColorData { 100, QColor(0, 0, 0) },
                  painter::data::PaintableRectangleData {
                    painter::data::RectanglePositionData {
                      painter::data::PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    painter::data::ColorData { 100, QColor(0, 0, 0) },
                    painter::data::PaintableBorderData { 0, painter::data::ColorData { 100, QColor(0, 0, 0) } } },
                  "",
                  "",
                  0,
                  0 },
                painter::data::PaintableRectangleData {},
                painter::data::PaintableTextData {} } }
          };

        QByteArray arrayData =
          QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<PaintableList>\n"
                            "    <SecondaryRepetitionDirection>3</SecondaryRepetitionDirection>\n");

        arrayData.append(QByteArrayLiteral("    <PaintableItemList>\n"));

        for (const painter::data::VariantListableItemData& variantListableItem :
             expectedPaintableListData.value().paintableItemListModel.paintableItemList)
        {
            arrayData.append(writeVariantListableItemData(variantListableItem));
        }

        arrayData.append(QByteArrayLiteral("    </PaintableItemList>\n"));

        arrayData.append(QByteArrayLiteral("    <PrimaryRepetitionDirection>4</PrimaryRepetitionDirection>\n"));
        arrayData.append(QByteArrayLiteral("    <PositionData>\n"
                                           "        <x>42.42</x>\n"
                                           "        <y>53.53</y>\n"
                                           "        <width>64.64</width>\n"
                                           "        <height>75.75</height>\n"
                                           "    </PositionData>\n"));

        arrayData.append(QByteArrayLiteral("</PaintableList>\n"));

        QTest::newRow("Other_order") << arrayData << expectedPaintableListData;
    }

    // Empty PaintableList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableList>\n"
                                                 "</PaintableList>\n");
        std::optional<painter::data::PaintableListData> expectedPaintableListData =
          painter::data::PaintableListData {};

        QTest::newRow("Empty_PaintableList") << arrayData << expectedPaintableListData;
    }

    // Empty Self Closing
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PaintableList/>");
        std::optional<painter::data::PaintableListData> expectedPaintableListData =
          painter::data::PaintableListData {};

        QTest::newRow("Empty_Self_Closing_PaintableList") << arrayData << expectedPaintableListData;
    }

    // Not PaintableList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");

        std::optional<painter::data::PaintableListData> expectedPaintableListData = std::nullopt;

        QTest::newRow("Not_PaintableList") << arrayData << expectedPaintableListData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Some random text");
        std::optional<painter::data::PaintableListData> expectedPaintableListData = std::nullopt;

        QTest::newRow("Not_Xml") << arrayData << expectedPaintableListData;
    }
}

void Test_PainterXmlSerialization::test_readPaintableListData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<painter::data::PaintableListData>, expectedPaintableListData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<painter::data::PaintableListData> actualPaintableListData =
      painter::service::readPaintableListData(streamReader);

    if (expectedPaintableListData.has_value())
    {
        {
            const painter::data::PaintableListData& actualPaintableList   = actualPaintableListData.value();
            const painter::data::PaintableListData& expectedPaintableList = expectedPaintableListData.value();

            QCOMPARE(actualPaintableList.position, expectedPaintableList.position);
            QCOMPARE(actualPaintableList.primaryRepetitionDirection,
                     expectedPaintableList.primaryRepetitionDirection);
            QCOMPARE(actualPaintableList.secondaryRepetitionDirection,
                     expectedPaintableList.secondaryRepetitionDirection);

            QCOMPARE(actualPaintableList.paintableItemListModel.paintableItemList.size(),
                     expectedPaintableList.paintableItemListModel.paintableItemList.size());

            for (int index = 0; index < expectedPaintableList.paintableItemListModel.paintableItemList.size();
                 ++index)
            {
                const painter::data::VariantListableItemData& actualVariant =
                  actualPaintableList.paintableItemListModel.paintableItemList.at(index);
                const painter::data::VariantListableItemData& expectedVariant =
                  expectedPaintableList.paintableItemListModel.paintableItemList.at(index);

                QCOMPARE(actualVariant, expectedVariant);
            }

            QCOMPARE(actualPaintableList, expectedPaintableList);
        }
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), painter::service::paintableListElement);
    }
    else { QVERIFY(!actualPaintableListData.has_value()); }
}

void Test_PainterXmlSerialization::test_writePaintableListData()
{
    painter::data::PaintableListData paintableListData = painter::data::PaintableListData {
        painter::data::PositionData { 42.42, 53.53, 64.64, 75.75 },
        painter::data::PaintableListData::RepetitionDirection::LeftRepetition,
        painter::data::PaintableListData::RepetitionDirection::TopRepetition,
        painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
          painter::data::PaintableTextData {
            painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
            painter::data::PaintableRectangleData {
              painter::data::RectanglePositionData {
                painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
              painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
              painter::data::PaintableBorderData { 25,
                                                   painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
            "Some text",
            "Arial",
            12,
            25 },
          painter::data::PaintableRectangleData {},
          painter::data::PaintableRectangleData {
            painter::data::RectanglePositionData {
              painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
            painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
            painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } } },
          painter::data::PaintableTextData {} } }
    };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    painter::service::writePaintableListData(streamWriter, paintableListData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
             "<PaintableList>\n"
             "    <PositionData>\n"
             "        <x>42.42</x>\n"
             "        <y>53.53</y>\n"
             "        <width>64.64</width>\n"
             "        <height>75.75</height>\n"
             "    </PositionData>\n"
             "    <PrimaryRepetitionDirection>4</PrimaryRepetitionDirection>\n"
             "    <SecondaryRepetitionDirection>3</SecondaryRepetitionDirection>\n"
             "    <PaintableItemList>\n"
             "        <PaintableText>\n"
             "            <ColorData>\n"
             "                <Opacity>34</Opacity>\n"
             "                <Color>\n"
             "                    <r>255</r>\n"
             "                    <g>191</g>\n"
             "                    <b>128</b>\n"
             "                    <a>34</a>\n"
             "                </Color>\n"
             "            </ColorData>\n"
             "            <PaintableRectangle>\n"
             "                <RectanglePosition>\n"
             "                    <PositionData>\n"
             "                        <x>12.24</x>\n"
             "                        <y>32.56</y>\n"
             "                        <width>45.42</width>\n"
             "                        <height>19.58</height>\n"
             "                    </PositionData>\n"
             "                    <Radius>0</Radius>\n"
             "                    <SizeMode>1</SizeMode>\n"
             "                </RectanglePosition>\n"
             "                <ColorData>\n"
             "                    <Opacity>85</Opacity>\n"
             "                    <Color>\n"
             "                        <r>255</r>\n"
             "                        <g>191</g>\n"
             "                        <b>128</b>\n"
             "                        <a>85</a>\n"
             "                    </Color>\n"
             "                </ColorData>\n"
             "                <PaintableBorder>\n"
             "                    <BorderWidth>25</BorderWidth>\n"
             "                    <ColorData>\n"
             "                        <Opacity>55</Opacity>\n"
             "                        <Color>\n"
             "                            <r>161</r>\n"
             "                            <g>162</g>\n"
             "                            <b>163</b>\n"
             "                            <a>164</a>\n"
             "                        </Color>\n"
             "                    </ColorData>\n"
             "                </PaintableBorder>\n"
             "            </PaintableRectangle>\n"
             "            <Text>Some text</Text>\n"
             "            <FontFamily>Arial</FontFamily>\n"
             "            <Flags>12</Flags>\n"
             "            <PointSize>25</PointSize>\n"
             "        </PaintableText>\n"
             "        <PaintableRectangle>\n"
             "            <RectanglePosition>\n"
             "                <PositionData>\n"
             "                    <x>0</x>\n"
             "                    <y>0</y>\n"
             "                    <width>0</width>\n"
             "                    <height>0</height>\n"
             "                </PositionData>\n"
             "                <Radius>0</Radius>\n"
             "                <SizeMode>0</SizeMode>\n"
             "            </RectanglePosition>\n"
             "            <ColorData/>\n"
             "            <PaintableBorder>\n"
             "                <BorderWidth>0</BorderWidth>\n"
             "                <ColorData/>\n"
             "            </PaintableBorder>\n"
             "        </PaintableRectangle>\n"
             "        <PaintableRectangle>\n"
             "            <RectanglePosition>\n"
             "                <PositionData>\n"
             "                    <x>20.546</x>\n"
             "                    <y>50.32</y>\n"
             "                    <width>100.21</width>\n"
             "                    <height>200.42</height>\n"
             "                </PositionData>\n"
             "                <Radius>42</Radius>\n"
             "                <SizeMode>0</SizeMode>\n"
             "            </RectanglePosition>\n"
             "            <ColorData>\n"
             "                <Opacity>100</Opacity>\n"
             "                <Color>\n"
             "                    <r>255</r>\n"
             "                    <g>191</g>\n"
             "                    <b>128</b>\n"
             "                    <a>64</a>\n"
             "                </Color>\n"
             "            </ColorData>\n"
             "            <PaintableBorder>\n"
             "                <BorderWidth>2.365</BorderWidth>\n"
             "                <ColorData>\n"
             "                    <Opacity>100</Opacity>\n"
             "                    <Color>\n"
             "                        <r>128</r>\n"
             "                        <g>0</g>\n"
             "                        <b>128</b>\n"
             "                        <a>255</a>\n"
             "                    </Color>\n"
             "                </ColorData>\n"
             "            </PaintableBorder>\n"
             "        </PaintableRectangle>\n"
             "        <PaintableText>\n"
             "            <ColorData/>\n"
             "            <PaintableRectangle>\n"
             "                <RectanglePosition>\n"
             "                    <PositionData>\n"
             "                        <x>0</x>\n"
             "                        <y>0</y>\n"
             "                        <width>0</width>\n"
             "                        <height>0</height>\n"
             "                    </PositionData>\n"
             "                    <Radius>0</Radius>\n"
             "                    <SizeMode>0</SizeMode>\n"
             "                </RectanglePosition>\n"
             "                <ColorData/>\n"
             "                <PaintableBorder>\n"
             "                    <BorderWidth>0</BorderWidth>\n"
             "                    <ColorData/>\n"
             "                </PaintableBorder>\n"
             "            </PaintableRectangle>\n"
             "            <Text></Text>\n"
             "            <FontFamily></FontFamily>\n"
             "            <Flags>0</Flags>\n"
             "            <PointSize>0</PointSize>\n"
             "        </PaintableText>\n"
             "    </PaintableItemList>\n"
             "</PaintableList>\n");
}

QByteArray Test_PainterXmlSerialization::writePaintableRectangleXml(
  const painter::data::PaintableRectangleData& paintableRectangleData)
{
    QByteArray paintableRectangleXml;

    QXmlStreamWriter streamWriter(&paintableRectangleXml);
    streamWriter.setAutoFormatting(false);

    painter::service::writePaintableRectangleData(streamWriter, paintableRectangleData);

    return paintableRectangleXml;
}

QByteArray Test_PainterXmlSerialization::writePaintableTextXml(
  const painter::data::PaintableTextData& paintableTextData)
{
    QByteArray paintableTextXml;

    QXmlStreamWriter streamWriter(&paintableTextXml);
    streamWriter.setAutoFormatting(false);

    painter::service::writePaintableTextData(streamWriter, paintableTextData);

    return paintableTextXml;
}

QByteArray Test_PainterXmlSerialization::writeVariantListableItemData(
  const painter::data::VariantListableItemData& variantListableItem)
{
    if (const painter::data::PaintableRectangleData* const rectangleData =
          std::get_if<painter::data::PaintableRectangleData>(&variantListableItem))
    {
        return writePaintableRectangleXml(*rectangleData);
    }
    else if (const painter::data::PaintableTextData* const textData =
               std::get_if<painter::data::PaintableTextData>(&variantListableItem))
    {
        return writePaintableTextXml(*textData);
    }
    else
    {
        qWarning() << "Cannot write data for unknown VariantListableItemData";
        return QByteArray();
    }
}

QTEST_APPLESS_MAIN(Test_PainterXmlSerialization)

#include "Test_PainterXmlSerialization.moc"
