#include <QApplication>
#include <QPainter>
#include <QSignalSpy>
#include <QTest>

#include "Painter/PainterTestCommon/PainterTestCommon.hpp"
#include "Painter/controllers/PaintableBorder.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"
#include "Painter/data/PaintableTextData.hpp"
#include "Utils/FileUtils.hpp"
#include "testData/TestFonts.hpp"

class Test_Text : public PainterTestCommon
{
    Q_OBJECT

  public:
    Test_Text() = default;

  private slots:
    void initTestCase();
    void textPosition();
    void textSize();
    void textColor();
    void textOpacity();
    void textFontSize();
    void textFontFamilly();
    void textFlags();
    void textBackground();
    void textBorder();
    void textData();
    void textSetData();

  private:
    void _paint(QPainter* painter);

    QTemporaryDir _tempDir;
};

void Test_Text::initTestCase()
{
    QVERIFY(_tempDir.isValid());

    utils::FileUtils::loadFonts(testFontFolderPath);
    QFont font("Segoe UI");
    font.setPixelSize(150);
    QApplication::setFont(font);
}

void Test_Text::textPosition()
{
    QString generatedFilePath(_tempDir.path() + "/textPosition.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setText("Top Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setText("Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(static_cast<qreal>(painter.device()->width() - 1000)), 0, 1000, 1000 });
        text.setText("Right");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setText("Bottom");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(static_cast<qreal>(painter.device()->width() - 1000)),
          static_cast<qreal>(painter.device()->height() - 1000),
          1000,
          1000 });
        text.setText("Bottom right");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textPosition.svg");
}

void Test_Text::textSize()
{
    QString generatedFilePath(_tempDir.path() + "/textSize.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 2000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 2000 });
        text.setText("Top Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 500, 500 });
        text.setText("Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1550), 0, 1500, 1500 });
        text.setText("Right");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 500), 500, 500 });
        text.setText("Bottom");
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 2000),
                                               static_cast<qreal>(painter.device()->height() - 2000),
                                               2000,
                                               2000 });
        text.setText("Bottom right");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textSize.svg");
}

void Test_Text::textColor()
{
    QString generatedFilePath(_tempDir.path() + "/textColor.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setColor("white");
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setColor("red");
        text.setText("Top Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setColor("yellow");
        text.setText("Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(static_cast<qreal>(painter.device()->width() - 1000)), 0, 1000, 1000 });
        text.setColor("purple");
        text.setText("Right");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setColor("cyan");
        text.setText("Bottom");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(static_cast<qreal>(painter.device()->width() - 1000)),
          static_cast<qreal>(painter.device()->height() - 1000),
          1000,
          1000 });
        text.setColor("orange");
        text.setText("Bottom right");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textColor.svg");
}

void Test_Text::textOpacity()
{
    QString generatedFilePath(_tempDir.path() + "/textOpacity.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setOpacity(80);
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setColor("white");
        text.setOpacity(50);
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setColor("red");
        text.setOpacity(60);
        text.setText("Top Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setColor("yellow");
        text.setOpacity(100);
        text.setText("Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setColor("purple");
        text.setOpacity(10);
        text.setText("Right");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setColor("cyan");
        text.setOpacity(20);
        text.setText("Bottom");
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        text.setColor("orange");
        text.setOpacity(30);
        text.setText("Bottom right");
        text.border()->setColor("red");
        text.border()->setBorderWidth(10);
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textOpacity.svg");
}

void Test_Text::textFontSize()
{
    QString generatedFilePath(_tempDir.path() + "/textFontSize.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setPointSize(5);
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setPointSize(20);
        text.setText("Top Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setPointSize(11);
        text.setText("Middle");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setPointSize(14);
        text.setText("Right");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setPointSize(13);
        text.setText("Bottom");
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        text.setPointSize(16);
        text.setText("Bottom right");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textFontSize.svg");
}

void Test_Text::textFontFamilly()
{
    QString generatedFilePath(_tempDir.path() + "/textFontFamilly.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.background()->setColor("blue");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.setFontFamily("Arial");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.setFontFamily("Montserrat");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setText("Top Middle");
        text.setFontFamily("Anta");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setText("Middle");
        text.setFontFamily("Cambria");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setText("Right");
        text.setFontFamily("Wingdings");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setText("Bottom");
        text.setFontFamily("Comic Sans MS");
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        text.setText("Bottom right");
        text.setFontFamily("Segoe UI");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textFontFamilly.svg");
}

void Test_Text::textFlags()
{
    QString generatedFilePath(_tempDir.path() + "/textFlags.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.background()->setColor("blue");
        text.setColor("black");
        text.setPointSize(15);
        text.setFlags(Qt::AlignLeft | Qt::AlignVCenter);
        text.setText("Top left");

        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.setFontFamily("Montserrat");
        text.setFlags(Qt::AlignCenter | Qt::TextWordWrap);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setText("Top Middle");
        text.setFlags(Qt::AlignTop | Qt::AlignRight);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setText("Middle");
        text.setFlags(Qt::AlignJustify);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setText("Right");
        text.setFlags(Qt::AlignBottom | Qt::AlignLeft);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setText("Bottom");
        text.setFlags(0);
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        text.setText("Bottom right");
        text.setFlags(Qt::AlignVCenter | Qt::AlignHCenter);
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textFlags.svg");
}

void Test_Text::textBackground()
{
    QString generatedFilePath(_tempDir.path() + "/textBackground.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.background()->setColor("blue");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.setFontFamily("Montserrat");
        text.background()->setColor("yellow");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setText("Top Middle");
        text.background()->setColor("red");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setText("Middle");
        text.background()->setColor("white");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setText("Right");
        text.background()->setColor("green");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setText("Bottom");
        text.background()->setColor("cyan");
        text.background()->setRadius(100);
        text.paint(&painter);

        text.setPositionData(
          painter::data::PositionData { static_cast<qreal>(painter.device()->width() - 1000),
                                               static_cast<qreal>(painter.device()->height() - 1000),
                                               1000,
                                               1000 });
        text.setText("Bottom right");
        text.background()->setColor("purple");
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textBackground.svg");
}

void Test_Text::textBorder()
{
    QString generatedFilePath(_tempDir.path() + "/textBorder.svg");
    // Create a scope to ensure the painter complete painting before the following of the test
    {
        QSharedPointer<QPaintDevice> paintDevice(createSvgPaintDevice(generatedFilePath));
        QPainter painter(paintDevice.get());

        painter::controller::PaintableText text;
        text.setPositionData(painter::data::PositionData { 0, 0, 1000, 1000 });
        text.setFontFamily("Arial");
        text.border()->setColor("red");
        text.border()->setBorderWidth(10);
        text.setColor("black");
        text.setPointSize(15);
        text.setText("Top left");
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 0, 1000, 1000 });
        text.setText(
          "The text at the right of the top left, it is at the right of the one at the bottom right of the top "
          "left. Clear not ?");
        text.setFontFamily("Montserrat");
        text.border()->setColor("green");
        text.border()->setBorderWidth(20);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 0, 1050, 1000, 1000 });
        text.setText("Top Middle");
        text.background()->setColor("blue");
        text.border()->setColor("yellow");
        text.border()->setBorderWidth(50);
        text.background()->setRadius(100);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData { 1050, 1050, 1000, 1000 });
        text.setText("Middle");
        text.border()->setColor("purple");
        text.border()->setBorderWidth(50);
        text.background()->setRadius(0);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(painter.device()->width() - 1000), 0, 1000, 1000 });
        text.setText("Right");
        text.border()->setBorderWidth(0);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          0, static_cast<qreal>(painter.device()->height() - 1000), 1000, 1000 });
        text.setText("Bottom");
        text.border()->setColor("black");
        text.border()->setBorderWidth(50);
        text.paint(&painter);

        text.setPositionData(painter::data::PositionData {
          static_cast<qreal>(static_cast<qreal>(painter.device()->width() - 1000)),
          static_cast<qreal>(painter.device()->height() - 1000),
          1000,
          1000 });
        text.setText("Bottom right");
        text.border()->setColor("cyan");
        text.border()->setBorderWidth(50);
        text.paint(&painter);
    }

    compareFiles(generatedFilePath, ":/data/textBorder.svg");
}

void Test_Text::textData()
{
    painter::controller::PaintableText text;

    painter::data::PositionData position(200, 400, 600, 800);

    text.setPositionData(position);
    text.setColor("pink");
    text.setOpacity(85);
    text.setText("Patate");
    text.setFlags(42);
    text.setPointSize(12);
    text.setFontFamily("Montserrat");

    text.background()->setColor("red");
    text.background()->setRadius(0.60);
    text.background()->setMode(Qt::SizeMode::AbsoluteSize);

    text.border()->setColor("yellow");
    text.border()->setBorderWidth(20);

    painter::data::PaintableTextData paintableTextData = text.objectData();

    QColor textColor("pink");
    textColor.setAlpha(std::round(85 * 2.55));

    QCOMPARE(paintableTextData.background.position.position, position);
    QCOMPARE(paintableTextData.color.color, textColor);
    QCOMPARE(paintableTextData.color.opacity, 85);
    QCOMPARE(paintableTextData.text, "Patate");
    QCOMPARE(paintableTextData.flags, 42);
    QCOMPARE(paintableTextData.pointSize, 12);
    QCOMPARE(paintableTextData.fontFamily, "Montserrat");

    QColor backgroundColor("red");
    backgroundColor.setAlpha(std::round(85 * 2.55));

    QCOMPARE(paintableTextData.background.color.color, backgroundColor);
    QCOMPARE(paintableTextData.background.color.opacity, 85);
    QCOMPARE(paintableTextData.background.position.radius, 0.6);
    QCOMPARE(paintableTextData.background.position.mode, Qt::SizeMode::AbsoluteSize);

    QColor borderColor("yellow");
    borderColor.setAlpha(std::round(85 * 2.55));

    QCOMPARE(paintableTextData.background.border.color.color, borderColor);
    QCOMPARE(paintableTextData.background.border.color.opacity, 85);
    QCOMPARE(paintableTextData.background.border.borderWidth, 20);
}

void Test_Text::textSetData()
{
    painter::data::PaintableTextData paintableTextData;

    //-------------------------------------------------------
    // Test constructor with data
    //-------------------------------------------------------
    painter::data::PositionData position(600, 680, 670, 660);

    paintableTextData.background.position.position = position;
    paintableTextData.background.position.radius   = 0.66;
    paintableTextData.background.position.mode     = Qt::SizeMode::AbsoluteSize;

    QColor textColor("red");
    textColor.setAlpha(80 * 2.55);

    paintableTextData.color.color   = textColor;
    paintableTextData.color.opacity = 80;
    paintableTextData.text          = "Pop";
    paintableTextData.flags         = 36;
    paintableTextData.pointSize     = 16;
    paintableTextData.fontFamily    = "Montserrat";

    QColor backgroundColor("blue");
    backgroundColor.setAlpha(80 * 2.55);

    paintableTextData.background.color.color   = backgroundColor;
    paintableTextData.background.color.opacity = 80;

    QColor borderColor("green");
    borderColor.setAlpha(80 * 2.55);

    paintableTextData.background.border.color.color   = borderColor;
    paintableTextData.background.border.color.opacity = 80;
    paintableTextData.background.border.borderWidth   = 6;

    painter::controller::PaintableText paintableText(paintableTextData);

    QCOMPARE(paintableText.positionData(), position);
    QCOMPARE(paintableText.color(), textColor);
    QCOMPARE(paintableText.opacity(), 80);
    QCOMPARE(paintableText.text(), "Pop");
    QCOMPARE(paintableText.flags(), 36);
    QCOMPARE(paintableText.pointSize(), 16);
    QCOMPARE(paintableText.fontFamily(), "Montserrat");

    QCOMPARE(paintableText.background()->color(), backgroundColor);
    QCOMPARE(paintableText.background()->opacity(), 80);
    QCOMPARE(paintableText.background()->radius(), 0.66);
    QCOMPARE(paintableText.background()->mode(), Qt::SizeMode::AbsoluteSize);

    QCOMPARE(paintableText.background()->border()->color(), borderColor);
    QCOMPARE(paintableText.background()->border()->opacity(), 80);
    QCOMPARE(paintableText.background()->border()->borderWidth(), 6);

    QCOMPARE(paintableText.objectData(), paintableTextData);

    //-------------------------------------------------------
    // Test set data
    //-------------------------------------------------------
    position = painter::data::PositionData { 100, 80, 90, 70 };

    paintableTextData.background.position.position = position;
    paintableTextData.background.position.radius   = 0.8;
    paintableTextData.background.position.mode     = Qt::SizeMode::RelativeSize;

    textColor = QColor("orange");
    textColor.setAlpha(40 * 2.55);

    paintableTextData.color.color   = textColor;
    paintableTextData.color.opacity = 40;
    paintableTextData.text          = "Chips";
    paintableTextData.flags         = 42;
    paintableTextData.pointSize     = 12;
    paintableTextData.fontFamily    = "GrandPadano";

    backgroundColor = QColor("green");
    backgroundColor.setAlpha(40 * 2.55);

    paintableTextData.background.color.color   = backgroundColor;
    paintableTextData.background.color.opacity = 40;

    borderColor = QColor("blue");
    borderColor.setAlpha(40 * 2.55);

    paintableTextData.background.border.color.color   = borderColor;
    paintableTextData.background.border.color.opacity = 40;
    paintableTextData.background.border.borderWidth   = 3;

    QSignalSpy xChangedSpy(&paintableText, &painter::controller::PaintableText::xChanged);
    QSignalSpy yChangedSpy(&paintableText, &painter::controller::PaintableText::yChanged);
    QSignalSpy widthChangedSpy(&paintableText, &painter::controller::PaintableText::widthChanged);
    QSignalSpy heightChangedSpy(&paintableText, &painter::controller::PaintableText::heightChanged);
    QSignalSpy colorChangedSpy(&paintableText, &painter::controller::PaintableText::colorChanged);
    QSignalSpy opacityChangedSpy(&paintableText, &painter::controller::PaintableText::opacityChanged);
    QSignalSpy textChangedSpy(&paintableText, &painter::controller::PaintableText::textChanged);
    QSignalSpy flagsChangedSpy(&paintableText, &painter::controller::PaintableText::flagsChanged);
    QSignalSpy pointSizeChangedSpy(&paintableText, &painter::controller::PaintableText::pointSizeChanged);
    QSignalSpy fontFamilyChangedSpy(&paintableText, &painter::controller::PaintableText::fontFamilyChanged);

    paintableText.setObjectData(paintableTextData);

    QCOMPARE(xChangedSpy.size(), 1);
    QCOMPARE(yChangedSpy.size(), 1);
    QCOMPARE(widthChangedSpy.size(), 1);
    QCOMPARE(heightChangedSpy.size(), 1);
    QCOMPARE(colorChangedSpy.size(), 2);
    QCOMPARE(opacityChangedSpy.size(), 1);
    QCOMPARE(textChangedSpy.size(), 1);
    QCOMPARE(flagsChangedSpy.size(), 1);
    QCOMPARE(pointSizeChangedSpy.size(), 1);
    QCOMPARE(fontFamilyChangedSpy.size(), 1);

    QCOMPARE(paintableText.positionData(), position);
    QCOMPARE(paintableText.color(), textColor);
    QCOMPARE(paintableText.opacity(), 40);
    QCOMPARE(paintableText.text(), "Chips");
    QCOMPARE(paintableText.flags(), 42);
    QCOMPARE(paintableText.pointSize(), 12);
    QCOMPARE(paintableText.fontFamily(), "GrandPadano");

    QCOMPARE(paintableText.background()->color(), backgroundColor);
    QCOMPARE(paintableText.background()->opacity(), 40);
    QCOMPARE(paintableText.background()->radius(), 0.8);
    QCOMPARE(paintableText.background()->mode(), Qt::SizeMode::RelativeSize);

    QCOMPARE(paintableText.background()->border()->color(), borderColor);
    QCOMPARE(paintableText.background()->border()->opacity(), 40);
    QCOMPARE(paintableText.background()->border()->borderWidth(), 3);

    QCOMPARE(paintableText.objectData(), paintableTextData);
}

QTEST_MAIN(Test_Text)

#include "Test_Text.moc"
