#include <QBuffer>
#include <QtTest/QTest>

#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "DocumentTemplate/data/PageSizeData.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "DocumentTemplate/services/DocumentTemplateXmlConstants.hpp"
#include "DocumentTemplate/services/DocumentTemplateXmlSerialization.hpp"
#include "Painter/services/PainterXmlSerializationDetails.hpp"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

class Test_DocumentTemplateXmlSerialization : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void test_readPageSizeData_data();
    void test_readPageSizeData();

    void test_writePageSizeData();

    void test_readDocumentTemplateData_data();
    void test_readDocumentTemplateData();

    void test_writeDocumentTemplateData();

    void test_readTemplatesHandlerData_data();
    void test_readTemplatesHandlerData();

    void test_writeTemplatesHandlerData();

  private:
    static QByteArray writePageSizeXml(const documenttemplate::data::PageSizeData& pageSizeData);
    static QByteArray writeListVariantItemXml(
      const QList<documenttemplate::data::VariantInterfaceQmlPaintableItem>& listVariantItem);
    static QByteArray writeDocumentTemplateXml(
      const documenttemplate::data::DocumentTemplateData& documentTemplateData);
};

void Test_DocumentTemplateXmlSerialization::test_readPageSizeData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documenttemplate::data::PageSizeData>>("expectedPageSizeData");

    // Same order than the write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<PageSize>\n"
                                                 "    <QPageSize>\n"
                                                 "        <QSizeF>\n"
                                                 "            <width>297</width>\n"
                                                 "            <height>125</height>\n"
                                                 "        </QSizeF>\n"
                                                 "        <definitionUnit>2</definitionUnit>\n"
                                                 "    </QPageSize>\n"
                                                 "    <resolution>1650</resolution>\n"
                                                 "    <unit>2</unit>\n"
                                                 "</PageSize>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData =
          documenttemplate::data::PageSizeData {
              QPageSize { QSizeF { 297, 125 }, QPageSize::Unit::Inch, QString(), QPageSize::ExactMatch },
              1650,
              documenttemplate::data::PageSizeData::Unit::Inch
          };

        QTest::newRow("Same_order_than_write") << arrayData << expectedPageSizeData;
    }

    // Other order
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PageSize>\n"
                                                 "    <unit>2</unit>\n"
                                                 "    <resolution>1650</resolution>\n"
                                                 "    <QPageSize>\n"
                                                 "        <QSizeF>\n"
                                                 "            <width>297</width>\n"
                                                 "            <height>125</height>\n"
                                                 "        </QSizeF>\n"
                                                 "        <definitionUnit>2</definitionUnit>\n"
                                                 "    </QPageSize>\n"
                                                 "</PageSize>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData =
          documenttemplate::data::PageSizeData {
              QPageSize { QSizeF { 297, 125 }, QPageSize::Unit::Inch, QString(), QPageSize::ExactMatch },
              1650,
              documenttemplate::data::PageSizeData::Unit::Inch
          };

        QTest::newRow("Other_order") << arrayData << expectedPageSizeData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PageSize>\n"
                                                 "    <unit/>\n"
                                                 "    <resolution/>\n"
                                                 "    <QPageSize/>\n"
                                                 "</PageSize>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData =
          documenttemplate::data::PageSizeData {};

        QTest::newRow("Self_Closing_parts") << arrayData << expectedPageSizeData;
    }

    // Empty page size
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PageSize>\n"
                                                 "</PageSize>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData =
          documenttemplate::data::PageSizeData {};

        QTest::newRow("Empty_page_size") << arrayData << expectedPageSizeData;
    }

    // Empty self closing page size
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<PageSize/>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData =
          documenttemplate::data::PageSizeData {};

        QTest::newRow("Empty_self_closing_page_size") << arrayData << expectedPageSizeData;
    }

    // Not page size
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData = std::nullopt;

        QTest::newRow("Not_page_size") << arrayData << expectedPageSizeData;
    }

    // Non xml
    {
        QByteArray arrayData = QByteArrayLiteral("Some random text");

        std::optional<documenttemplate::data::PageSizeData> expectedPageSizeData = std::nullopt;

        QTest::newRow("Non_xml") << arrayData << expectedPageSizeData;
    }
}

void Test_DocumentTemplateXmlSerialization::test_readPageSizeData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documenttemplate::data::PageSizeData>, expectedPageSizeData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documenttemplate::data::PageSizeData> actualPageSizeData =
      documenttemplate::service::readPageSizeData(streamReader);

    if (expectedPageSizeData.has_value())
    {
        QCOMPARE(actualPageSizeData.value(), expectedPageSizeData.value());
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documenttemplate::service::pageSizeElement);
    }
    else { QVERIFY(!actualPageSizeData.has_value()); }
}

void Test_DocumentTemplateXmlSerialization::test_writePageSizeData()
{
    documenttemplate::data::PageSizeData pageSizeData {
        QPageSize { QSizeF { 297, 125 }, QPageSize::Unit::Cicero, "Custom", QPageSize::ExactMatch },
        1650,
        documenttemplate::data::PageSizeData::Unit::Inch
    };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    documenttemplate::service::writePageSizeData(streamWriter, pageSizeData);

    streamWriter.writeEndDocument();

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<PageSize>\n"
             "    <QPageSize>\n"
             "        <QSizeF>\n"
             "            <width>297</width>\n"
             "            <height>125</height>\n"
             "        </QSizeF>\n"
             "        <definitionUnit>5</definitionUnit>\n"
             "    </QPageSize>\n"
             "    <resolution>1650</resolution>\n"
             "    <unit>2</unit>\n</PageSize>\n");
}

void Test_DocumentTemplateXmlSerialization::test_readDocumentTemplateData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documenttemplate::data::DocumentTemplateData>>("expectedDocumentTemplateData");

    // Same order than the write
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData;
        {
            using namespace documenttemplate::data;
            using namespace painter::data;
            expectedDocumentTemplateData = DocumentTemplateData {
                PageSizeData { QPageSize { QSizeF { 420, 594 }, QPageSize::Unit(0), "A2", QPageSize::ExactMatch },
                               1200,
                               PageSizeData::Unit(0) },
                RootPaintableItemListData { QList<VariantInterfaceQmlPaintableItem> {
                  PaintableTextData { ColorData { 100, QColor(0, 0, 0, 255) },
                                      PaintableRectangleData {
                                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                        ColorData { 100, QColor(0, 0, 0, 255) },
                                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                                      "",
                                      "",
                                      0,
                                      0 },
                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    ColorData { 100, QColor(0, 0, 0, 255) },
                    PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                  PaintableListData {
                    PositionData { 20, 30, 40, 50 },
                    PaintableListData::RepetitionDirection(2),
                    PaintableListData::RepetitionDirection(1),
                    PaintableItemListModelData { QList<VariantListableItemData> {
                      PaintableRectangleData {
                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                      PaintableTextData {
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableRectangleData {
                          RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                          ColorData { 100, QColor(0, 0, 0, 255) },
                          PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                        "",
                        "",
                        0,
                        0 } } } } } }
            };
        }

        QByteArray arrayData;
        {
            arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                          "<DocumentTemplate>");

            arrayData.append(writePageSizeXml(expectedDocumentTemplateData.value().pageSizeData));

            arrayData.append(QByteArrayLiteral("<RootPaintableItemList>"));

            arrayData.append(writeListVariantItemXml(
              expectedDocumentTemplateData.value().rootPaintableItemListData.paintableList));

            arrayData.append(QByteArrayLiteral("</RootPaintableItemList></DocumentTemplate>"));
        }

        QTest::newRow("Same_order_than_write") << arrayData << expectedDocumentTemplateData;
    }

    // Other order
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData;
        {
            using namespace documenttemplate::data;
            using namespace painter::data;
            expectedDocumentTemplateData = DocumentTemplateData {
                PageSizeData { QPageSize { QSizeF { 420, 594 }, QPageSize::Unit(0), "A2", QPageSize::ExactMatch },
                               1200,
                               PageSizeData::Unit(0) },
                RootPaintableItemListData { QList<VariantInterfaceQmlPaintableItem> {
                  PaintableTextData { ColorData { 100, QColor(0, 0, 0, 255) },
                                      PaintableRectangleData {
                                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                        ColorData { 100, QColor(0, 0, 0, 255) },
                                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                                      "",
                                      "",
                                      0,
                                      0 },
                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    ColorData { 100, QColor(0, 0, 0, 255) },
                    PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                  PaintableListData {
                    PositionData { 20, 30, 40, 50 },
                    PaintableListData::RepetitionDirection(2),
                    PaintableListData::RepetitionDirection(1),
                    PaintableItemListModelData { QList<VariantListableItemData> {
                      PaintableRectangleData {
                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                      PaintableTextData {
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableRectangleData {
                          RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                          ColorData { 100, QColor(0, 0, 0, 255) },
                          PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                        "",
                        "",
                        0,
                        0 } } } } } }
            };
        }

        QByteArray arrayData;
        {
            arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                          "<DocumentTemplate>");

            arrayData.append(QByteArrayLiteral("<RootPaintableItemList>"));

            arrayData.append(writeListVariantItemXml(
              expectedDocumentTemplateData.value().rootPaintableItemListData.paintableList));

            arrayData.append(QByteArrayLiteral("</RootPaintableItemList>"));

            arrayData.append(writePageSizeXml(expectedDocumentTemplateData.value().pageSizeData));

            arrayData.append(QByteArrayLiteral("</DocumentTemplate>"));
        }

        QTest::newRow("Other_order") << arrayData << expectedDocumentTemplateData;
    }

    // Self closing parts
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData =
          documenttemplate::data::DocumentTemplateData {};

        QByteArray arrayData;
        {
            arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                          "<DocumentTemplate>");

            arrayData.append(QByteArrayLiteral("<RootPaintableItemList>"));

            arrayData.append(writeListVariantItemXml(
              expectedDocumentTemplateData.value().rootPaintableItemListData.paintableList));

            arrayData.append(QByteArrayLiteral("</RootPaintableItemList>"));

            arrayData.append(writePageSizeXml(expectedDocumentTemplateData.value().pageSizeData));

            arrayData.append(QByteArrayLiteral("</DocumentTemplate>"));
        }

        QTest::newRow("Self_closing_parts") << arrayData << expectedDocumentTemplateData;
    }

    // Empty DocumentTemplate
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData =
          documenttemplate::data::DocumentTemplateData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DocumentTemplate>\n"
                                                 "</DocumentTemplate>\n");

        QTest::newRow("Empty_DocumentTemplate") << arrayData << expectedDocumentTemplateData;
    }

    // Empty self closing DocumentTemplate
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData =
          documenttemplate::data::DocumentTemplateData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DocumentTemplate/>\n");

        QTest::newRow("Empty_self_closing_DocumentTemplate") << arrayData << expectedDocumentTemplateData;
    }

    // Not DocumentTemplate
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData = std::nullopt;

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");

        QTest::newRow("Not_DocumentTemplate") << arrayData << expectedDocumentTemplateData;
    }

    // Not xml
    {
        std::optional<documenttemplate::data::DocumentTemplateData> expectedDocumentTemplateData = std::nullopt;

        QByteArray arrayData = QByteArrayLiteral("Some random text");

        QTest::newRow("Not_Xml") << arrayData << expectedDocumentTemplateData;
    }
}

void Test_DocumentTemplateXmlSerialization::test_readDocumentTemplateData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documenttemplate::data::DocumentTemplateData>, expectedDocumentTemplateData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documenttemplate::data::DocumentTemplateData> actualDocumentTemplateData =
      documenttemplate::service::readDocumentTemplateData(streamReader);

    QCOMPARE(actualDocumentTemplateData, expectedDocumentTemplateData);

    if (expectedDocumentTemplateData.has_value())
    {
        QCOMPARE(actualDocumentTemplateData.value(), expectedDocumentTemplateData.value());
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documenttemplate::service::documentTemplateElement);
    }
    else { QVERIFY(!actualDocumentTemplateData.has_value()); }
}

void Test_DocumentTemplateXmlSerialization::test_writeDocumentTemplateData()
{
    documenttemplate::data::DocumentTemplateData documentTemplate;

    {
        using namespace documenttemplate::data;
        using namespace painter::data;
        documentTemplate = DocumentTemplateData {
            PageSizeData { QPageSize { QSizeF { 420, 594 }, QPageSize::Unit(0), "A2", QPageSize::ExactMatch },
                           1200,
                           PageSizeData::Unit(0) },
            RootPaintableItemListData { QList<VariantInterfaceQmlPaintableItem> {
              PaintableTextData { ColorData { 100, QColor(0, 0, 0, 255) },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                    ColorData { 100, QColor(0, 0, 0, 255) },
                                    PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                                  "",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                       ColorData { 100, QColor(0, 0, 0, 255) },
                                       PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
              PaintableListData {
                PositionData { 20, 30, 40, 50 },
                PaintableListData::RepetitionDirection(2),
                PaintableListData::RepetitionDirection(1),
                PaintableItemListModelData { QList<VariantListableItemData> {
                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    ColorData { 100, QColor(0, 0, 0, 255) },
                    PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                  PaintableTextData { ColorData { 100, QColor(0, 0, 0, 255) },
                                      PaintableRectangleData {
                                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                        ColorData { 100, QColor(0, 0, 0, 255) },
                                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                                      "",
                                      "",
                                      0,
                                      0 } } } } } }
        };
    }

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documenttemplate::service::writeDocumentTemplateData(streamWriter, documentTemplate);

    streamWriter.writeEndDocument();

    QByteArray expectedArrayData;
    {
        expectedArrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                              "<DocumentTemplate>");

        expectedArrayData.append(writePageSizeXml(documentTemplate.pageSizeData));

        expectedArrayData.append(QByteArrayLiteral("<RootPaintableItemList>"));

        expectedArrayData.append(
          writeListVariantItemXml(documentTemplate.rootPaintableItemListData.paintableList));

        expectedArrayData.append(QByteArrayLiteral("</RootPaintableItemList></DocumentTemplate>\n"));
    }

    QCOMPARE(arrayData, expectedArrayData);
}

void Test_DocumentTemplateXmlSerialization::test_readTemplatesHandlerData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documenttemplate::data::TemplatesHandlerData>>("expectedTemplatesHandler");

    // Complete
    {
        std::optional<documenttemplate::data::TemplatesHandlerData> expectedTemplatesHandler;

        {
            using namespace documenttemplate::data;
            using namespace painter::data;
            DocumentTemplateData documentTemplate = DocumentTemplateData {
                PageSizeData { QPageSize { QSizeF { 420, 594 }, QPageSize::Unit(0), "A2", QPageSize::ExactMatch },
                               1200,
                               PageSizeData::Unit(0) },
                RootPaintableItemListData { QList<VariantInterfaceQmlPaintableItem> {
                  PaintableTextData { ColorData { 100, QColor(0, 0, 0, 255) },
                                      PaintableRectangleData {
                                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                        ColorData { 100, QColor(0, 0, 0, 255) },
                                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                                      "",
                                      "",
                                      0,
                                      0 },
                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    ColorData { 100, QColor(0, 0, 0, 255) },
                    PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                  PaintableListData {
                    PositionData { 20, 30, 40, 50 },
                    PaintableListData::RepetitionDirection(2),
                    PaintableListData::RepetitionDirection(1),
                    PaintableItemListModelData { QList<VariantListableItemData> {
                      PaintableRectangleData {
                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                      PaintableTextData {
                        ColorData { 100, QColor(0, 0, 0, 255) },
                        PaintableRectangleData {
                          RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                          ColorData { 100, QColor(0, 0, 0, 255) },
                          PaintableBorderData { 0, ColorData { 100, QColor(0, 0, 0, 255) } } },
                        "",
                        "",
                        0,
                        0 } } } } } }
            };

            expectedTemplatesHandler = TemplatesHandlerData { QList<DocumentTemplateData> {
              documentTemplate, documentTemplate, documentTemplate } };
        }

        QByteArray arrayData;

        {
            arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                          "<TemplatesHandler>");

            for (const documenttemplate::data::DocumentTemplateData& documentTemplate :
                 expectedTemplatesHandler.value().documentTemplates)
            {
                arrayData.append(writeDocumentTemplateXml(documentTemplate));
            }

            arrayData.append(QByteArrayLiteral("</TemplatesHandler>\n"));
        }

        QTest::newRow("Same_order_than_write") << arrayData << expectedTemplatesHandler;
    }

    // Empty TemplatesHandler
    {
        std::optional<documenttemplate::data::TemplatesHandlerData> expectedTemplatesHandler =
          documenttemplate::data::TemplatesHandlerData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<TemplatesHandler></TemplatesHandler>\n");

        QTest::newRow("Empty_TemplatesHandler") << arrayData << expectedTemplatesHandler;
    }

    // Empty self closing TemplatesHandler
    {
        std::optional<documenttemplate::data::TemplatesHandlerData> expectedTemplatesHandler =
          documenttemplate::data::TemplatesHandlerData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<TemplatesHandler/>\n");

        QTest::newRow("Empty_self_closing_TemplatesHandler") << arrayData << expectedTemplatesHandler;
    }

    // Not templatesHandler
    {
        std::optional<documenttemplate::data::TemplatesHandlerData> expectedTemplatesHandler = std::nullopt;

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");

        QTest::newRow("Not_templatesHandler") << arrayData << expectedTemplatesHandler;
    }

    // Not xml
    {
        std::optional<documenttemplate::data::TemplatesHandlerData> expectedTemplatesHandler = std::nullopt;

        QByteArray arrayData = QByteArrayLiteral("Some random text");

        QTest::newRow("Not_Xml") << arrayData << expectedTemplatesHandler;
    }
}

void Test_DocumentTemplateXmlSerialization::test_readTemplatesHandlerData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documenttemplate::data::TemplatesHandlerData>, expectedTemplatesHandler);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documenttemplate::data::TemplatesHandlerData> actualTemplatesHandler =
      documenttemplate::service::readTemplatesHandlerData(streamReader);

    QCOMPARE(actualTemplatesHandler, expectedTemplatesHandler);

    if (expectedTemplatesHandler.has_value())
    {
        QCOMPARE(actualTemplatesHandler.value(), expectedTemplatesHandler.value());
        QCOMPARE(streamReader.errorString(), QString());
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documenttemplate::service::templatesHandlerElement);
    }
    else { QVERIFY(!actualTemplatesHandler.has_value()); }
}

void Test_DocumentTemplateXmlSerialization::test_writeTemplatesHandlerData()
{
    documenttemplate::data::TemplatesHandlerData templatesHandler;

    {
        using namespace documenttemplate::data;
        using namespace painter::data;
        DocumentTemplateData documentTemplate = DocumentTemplateData {
            PageSizeData { QPageSize { QSizeF { 420, 594 }, QPageSize::Unit(0), "A2", QPageSize::ExactMatch },
                           1200,
                           PageSizeData::Unit(0) },
            RootPaintableItemListData { QList<VariantInterfaceQmlPaintableItem> {
              PaintableTextData { ColorData { 100, "" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                    ColorData { 100, "" },
                                    PaintableBorderData { 0, ColorData { 100, "" } } },
                                  "",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                       ColorData { 100, "" },
                                       PaintableBorderData { 0, ColorData { 100, "" } } },
              PaintableListData {
                PositionData { 20, 30, 40, 50 },
                PaintableListData::RepetitionDirection(2),
                PaintableListData::RepetitionDirection(1),
                PaintableItemListModelData { QList<VariantListableItemData> {
                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                    ColorData { 100, "" },
                    PaintableBorderData { 0, ColorData { 100, "" } } },
                  PaintableTextData { ColorData { 100, "" },
                                      PaintableRectangleData {
                                        RectanglePositionData { PositionData { 0, 0, 0, 0 }, 0, Qt::AbsoluteSize },
                                        ColorData { 100, "" },
                                        PaintableBorderData { 0, ColorData { 100, "" } } },
                                      "",
                                      "",
                                      0,
                                      0 } } } } } }
        };

        templatesHandler.documentTemplates.append(documentTemplate);
        templatesHandler.documentTemplates.append(documentTemplate);
        templatesHandler.documentTemplates.append(documentTemplate);
    }

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documenttemplate::service::writeTemplatesHandlerData(streamWriter, templatesHandler);

    streamWriter.writeEndDocument();

    QByteArray expectedArrayData;
    {
        expectedArrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                              "<TemplatesHandler>");

        for (const documenttemplate::data::DocumentTemplateData& documentTemplate :
             templatesHandler.documentTemplates)
        {
            expectedArrayData.append(writeDocumentTemplateXml(documentTemplate));
        }

        expectedArrayData.append(QByteArrayLiteral("</TemplatesHandler>\n"));
    }

    QCOMPARE(arrayData, expectedArrayData);
}

QByteArray Test_DocumentTemplateXmlSerialization::writePageSizeXml(
  const documenttemplate::data::PageSizeData& pageSizeData)
{
    QByteArray pageSizeXml;

    QXmlStreamWriter streamWriter(&pageSizeXml);
    streamWriter.setAutoFormatting(false);

    documenttemplate::service::writePageSizeData(streamWriter, pageSizeData);

    return pageSizeXml;
}

QByteArray Test_DocumentTemplateXmlSerialization::writeListVariantItemXml(
  const QList<documenttemplate::data::VariantInterfaceQmlPaintableItem>& listVariantItem)
{
    QBuffer buffer;
    QByteArray rootPaintableItemListXml;
    buffer.setBuffer(&rootPaintableItemListXml);
    buffer.open(QIODevice::WriteOnly);

    QXmlStreamWriter streamWriter;
    streamWriter.setDevice(&buffer);
    streamWriter.setAutoFormatting(false);

    for (const documenttemplate::data::VariantInterfaceQmlPaintableItem& variantInterfaceQmlPaintableItem :
         listVariantItem)
    {
        std::visit(
          overloaded { [&streamWriter](const painter::data::PaintableRectangleData& paintableRectangleData) {
                          painter::service::details::writePaintableRectangleData(streamWriter,
                                                                                 paintableRectangleData);
                      },
                       [&streamWriter](const painter::data::PaintableTextData& paintableTextData)
                       { painter::service::details::writePaintableTextData(streamWriter, paintableTextData); },
                       [&streamWriter](const painter::data::PaintableListData& paintableListData)
                       { painter::service::details::writePaintableListData(streamWriter, paintableListData); } },
          variantInterfaceQmlPaintableItem);
    }

    return rootPaintableItemListXml;
}

QByteArray Test_DocumentTemplateXmlSerialization::writeDocumentTemplateXml(
  const documenttemplate::data::DocumentTemplateData& documentTemplateData)
{
    QByteArray documentTemplateXml;

    QXmlStreamWriter streamWriter(&documentTemplateXml);
    streamWriter.setAutoFormatting(false);

    documenttemplate::service::writeDocumentTemplateData(streamWriter, documentTemplateData);

    return documentTemplateXml;
}

QTEST_APPLESS_MAIN(Test_DocumentTemplateXmlSerialization)

#include "Test_DocumentTemplateXmlSerialization.moc"
