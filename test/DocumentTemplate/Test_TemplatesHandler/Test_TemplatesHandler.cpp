#include <QSignalSpy>
#include <QTest>

#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/TemplatesHandler.hpp"

class Test_TemplatesHandler : public QObject
{
    Q_OBJECT

  public:
    Test_TemplatesHandler() = default;

  private slots:
    void data();
    void setData();
};

void Test_TemplatesHandler::data()
{
    documenttemplate::data::DocumentTemplateData documentTemplateData1;

    documentTemplateData1.pageSizeData.setPageSize(QPageSize::A4);

    documentTemplateData1.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());
    documentTemplateData1.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());

    documenttemplate::data::DocumentTemplateData documentTemplateData2;

    documentTemplateData2.pageSizeData.setPageSize(QPageSize::A3);

    documentTemplateData2.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());
    documentTemplateData2.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());

    documenttemplate::data::DocumentTemplateData documentTemplateData3;

    documentTemplateData3.pageSizeData.setPageSize(QPageSize::A5);

    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());
    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());
    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());

    documenttemplate::controller::TemplatesHandler templatesHandler;

    templatesHandler.addDocumentTemplate();
    templatesHandler.addDocumentTemplate();
    templatesHandler.addDocumentTemplate();

    templatesHandler.at(0)->setObjectData(documentTemplateData1);
    templatesHandler.at(1)->setObjectData(documentTemplateData2);
    templatesHandler.at(2)->setObjectData(documentTemplateData3);

    documenttemplate::data::TemplatesHandlerData templatesHandlerData = templatesHandler.objectData();

    QCOMPARE(templatesHandlerData.documentTemplates.size(), 3);
    QCOMPARE(templatesHandlerData.documentTemplates.at(0), documentTemplateData1);
    QCOMPARE(templatesHandlerData.documentTemplates.at(1), documentTemplateData2);
    QCOMPARE(templatesHandlerData.documentTemplates.at(2), documentTemplateData3);
}

void Test_TemplatesHandler::setData()
{
    documenttemplate::data::TemplatesHandlerData templatesHandlerData;

    documenttemplate::data::DocumentTemplateData documentTemplateData1;

    documentTemplateData1.pageSizeData.setPageSize(QPageSize::A4);

    documentTemplateData1.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());
    documentTemplateData1.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());

    documenttemplate::data::DocumentTemplateData documentTemplateData2;

    documentTemplateData2.pageSizeData.setPageSize(QPageSize::A3);

    documentTemplateData2.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());
    documentTemplateData2.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());

    documenttemplate::data::DocumentTemplateData documentTemplateData3;

    documentTemplateData3.pageSizeData.setPageSize(QPageSize::A5);

    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());
    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableTextData());
    documentTemplateData3.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());

    templatesHandlerData.documentTemplates.append(documentTemplateData1);
    templatesHandlerData.documentTemplates.append(documentTemplateData2);
    templatesHandlerData.documentTemplates.append(documentTemplateData3);

    QList<QPointer<documenttemplate::controller::DocumentTemplate>> documentTemplatePointers;

    {
        documenttemplate::controller::TemplatesHandler templatesHandler(templatesHandlerData);

        QSignalSpy rowsAboutToBeInsertedSpy(
          &templatesHandler, &documenttemplate::controller::TemplatesHandler::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&templatesHandler,
                                   &documenttemplate::controller::TemplatesHandler::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&templatesHandler,
                                           &documenttemplate::controller::TemplatesHandler::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&templatesHandler, &documenttemplate::controller::TemplatesHandler::rowsRemoved);
        QSignalSpy modelAboutToBeResetSpy(&templatesHandler,
                                          &documenttemplate::controller::TemplatesHandler::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&templatesHandler, &documenttemplate::controller::TemplatesHandler::modelReset);

        QCOMPARE(templatesHandler.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            QPointer<documenttemplate::controller::DocumentTemplate> documentTemplate = templatesHandler.at(i);
            QVERIFY(!documentTemplate.isNull());
            documentTemplatePointers.append(documentTemplate);
            QCOMPARE(documentTemplate->objectData(), templatesHandlerData.documentTemplates.at(i));
        }

        QCOMPARE(templatesHandler.objectData(), templatesHandlerData);

        templatesHandlerData.documentTemplates = { documentTemplateData3,
                                                   documentTemplateData2,
                                                   documentTemplateData1 };

        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);
        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);

        templatesHandler.setObjectData(templatesHandlerData);

        for (const QPointer<documenttemplate::controller::DocumentTemplate>& documentTemplate :
             documentTemplatePointers)
        {
            QTRY_VERIFY(documentTemplate.isNull());
        }

        documentTemplatePointers.clear();

        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);
        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);

        QCOMPARE(templatesHandler.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            QPointer<documenttemplate::controller::DocumentTemplate> documentTemplate = templatesHandler.at(i);
            QTRY_VERIFY(!documentTemplate.isNull());
            documentTemplatePointers.append(documentTemplate);
            QCOMPARE(documentTemplate->objectData(), templatesHandlerData.documentTemplates.at(i));
        }

        QCOMPARE(templatesHandler.objectData(), templatesHandlerData);
    }

    for (const QPointer<documenttemplate::controller::DocumentTemplate>& documentTemplate :
         documentTemplatePointers)
    {
        QTRY_VERIFY(documentTemplate.isNull());
    }
}

QTEST_GUILESS_MAIN(Test_TemplatesHandler)

#include "Test_TemplatesHandler.moc"
