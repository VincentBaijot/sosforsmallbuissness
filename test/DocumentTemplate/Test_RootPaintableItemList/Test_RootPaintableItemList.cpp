
#include <QSignalSpy>
#include <QTest>

#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "Painter/controllers/ListableItemListModel.hpp"
#include "Painter/controllers/PaintableBorder.hpp"
#include "Painter/controllers/PaintableList.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"
#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"

class Test_RootPaintableItemList : public QObject
{
    Q_OBJECT

  public:
    Test_RootPaintableItemList() = default;

  private slots:
    void data();
    void setData();
};

void Test_RootPaintableItemList::data()
{
    QPointer<painter::controller::PaintableRectangle> rectangle1 = new painter::controller::PaintableRectangle(this);

    painter::data::PositionData position { 10, 20, 30, 40 };

    rectangle1->setPositionData(position);
    rectangle1->setColor("pink");
    rectangle1->setOpacity(20);
    rectangle1->setRadius(0.60);
    rectangle1->setMode(Qt::SizeMode::AbsoluteSize);
    rectangle1->border()->setColor("yellow");
    rectangle1->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableRectangle> rectangle2 = new painter::controller::PaintableRectangle(this);

    position = painter::data::PositionData { 50, 60, 70, 80 };

    rectangle2->setPositionData(position);
    rectangle2->setColor("blue");
    rectangle2->setOpacity(40);
    rectangle2->setRadius(0.80);
    rectangle2->setMode(Qt::SizeMode::AbsoluteSize);

    rectangle2->border()->setColor("purple");
    rectangle2->border()->setBorderWidth(5);

    QPointer<painter::controller::PaintableText> text1 = new painter::controller::PaintableText(this);

    position = painter::data::PositionData(200, 400, 600, 800);

    text1->setPositionData(position);
    text1->setColor("pink");
    text1->setOpacity(85);
    text1->setText("Patate");
    text1->setFlags(42);
    text1->setPointSize(12);
    text1->setFontFamily("Roboto");

    text1->background()->setColor("red");
    text1->background()->setRadius(0.60);
    text1->background()->setMode(Qt::SizeMode::AbsoluteSize);

    text1->border()->setColor("yellow");
    text1->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableText> text2 = new painter::controller::PaintableText(this);

    position = painter::data::PositionData { 20, 40, 60, 80 };

    text2->setPositionData(position);
    text2->setColor("green");
    text2->setOpacity(45);
    text2->setText("Frites");
    text2->setFlags(32);
    text2->setPointSize(8);
    text2->setFontFamily("Arial");

    text2->background()->setColor("red");
    text2->background()->setRadius(0.60);
    text2->background()->setMode(Qt::SizeMode::AbsoluteSize);

    text2->border()->setColor("yellow");
    text2->border()->setBorderWidth(20);

    QPointer<painter::controller::PaintableRectangle> rectangle3 = new painter::controller::PaintableRectangle(this);

    QPointer<painter::controller::PaintableList> paintableList = new painter::controller::PaintableList(this);

    QPointer<painter::controller::PaintableRectangle> subRectangle1 =
      new painter::controller::PaintableRectangle(rectangle1->objectData(), this);
    QPointer<painter::controller::PaintableText> subText1 = new painter::controller::PaintableText(text1->objectData(), this);
    QPointer<painter::controller::PaintableRectangle> subRectangle2 =
      new painter::controller::PaintableRectangle(rectangle2->objectData(), this);

    paintableList->listableItemListModel()->addPaintableItem(subRectangle1);
    paintableList->listableItemListModel()->addPaintableItem(subText1);
    paintableList->listableItemListModel()->addPaintableItem(subRectangle2);

    QCOMPARE(subRectangle1->parent(), paintableList->listableItemListModel());
    QCOMPARE(subRectangle2->parent(), paintableList->listableItemListModel());
    QCOMPARE(subText1->parent(), paintableList->listableItemListModel());

    {
        documenttemplate::controller::RootPaintableItemList rootPaintableItemList;
        rootPaintableItemList.addPaintableItem(rectangle1);
        rootPaintableItemList.addPaintableItem(rectangle2);
        rootPaintableItemList.addPaintableItem(text1);
        rootPaintableItemList.addPaintableItem(rectangle3);
        rootPaintableItemList.addPaintableItem(text2);
        rootPaintableItemList.addPaintableItem(paintableList);

        QCOMPARE(rectangle1->parent(), &rootPaintableItemList);
        QCOMPARE(rectangle2->parent(), &rootPaintableItemList);
        QCOMPARE(text1->parent(), &rootPaintableItemList);
        QCOMPARE(rectangle3->parent(), &rootPaintableItemList);
        QCOMPARE(text2->parent(), &rootPaintableItemList);
        QCOMPARE(paintableList->parent(), &rootPaintableItemList);

        documenttemplate::data::RootPaintableItemListData rootPaintableItemListData =
          rootPaintableItemList.objectData();

        QCOMPARE(rootPaintableItemListData.paintableList.size(), 6);

        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          rootPaintableItemListData.paintableList.at(0)));
        QCOMPARE(
          std::get<painter::data::PaintableRectangleData>(rootPaintableItemListData.paintableList.at(0)),
          rectangle1->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          rootPaintableItemListData.paintableList.at(1)));
        QCOMPARE(
          std::get<painter::data::PaintableRectangleData>(rootPaintableItemListData.paintableList.at(1)),
          rectangle2->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableTextData>(
          rootPaintableItemListData.paintableList.at(2)));
        QCOMPARE(std::get<painter::data::PaintableTextData>(rootPaintableItemListData.paintableList.at(2)),
                 text1->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableRectangleData>(
          rootPaintableItemListData.paintableList.at(3)));
        QCOMPARE(
          std::get<painter::data::PaintableRectangleData>(rootPaintableItemListData.paintableList.at(3)),
          rectangle3->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableTextData>(
          rootPaintableItemListData.paintableList.at(4)));
        QCOMPARE(std::get<painter::data::PaintableTextData>(rootPaintableItemListData.paintableList.at(4)),
                 text2->objectData());
        QVERIFY(std::holds_alternative<painter::data::PaintableListData>(
          rootPaintableItemListData.paintableList.at(5)));
        QCOMPARE(std::get<painter::data::PaintableListData>(rootPaintableItemListData.paintableList.at(5)),
                 paintableList->objectData());
    }

    QVERIFY(rectangle1.isNull());
    QVERIFY(rectangle2.isNull());
    QVERIFY(text1.isNull());
    QVERIFY(text2.isNull());
    QVERIFY(rectangle3.isNull());
    QVERIFY(paintableList.isNull());
    QVERIFY(subRectangle1.isNull());
    QVERIFY(subRectangle2.isNull());
    QVERIFY(subText1.isNull());
}

void Test_RootPaintableItemList::setData()
{
    painter::data::PaintableRectangleData rectangleData1;

    painter::data::PositionData position { 10, 20, 30, 40 };

    QColor backgroundColor("Pink");
    backgroundColor.setAlpha(20 * 2.55);

    rectangleData1.position.position = position;
    rectangleData1.color.color       = backgroundColor;
    rectangleData1.color.opacity     = 20;
    rectangleData1.position.radius   = 0.60;
    rectangleData1.position.mode     = Qt::SizeMode::AbsoluteSize;

    QColor borderColor("Yellow");
    borderColor.setAlpha(20 * 2.55);

    rectangleData1.border.color.color   = borderColor;
    rectangleData1.border.color.opacity = 20;
    rectangleData1.border.borderWidth   = 20;

    painter::data::PaintableRectangleData rectangleData2;

    position = painter::data::PositionData { 50, 60, 70, 80 };

    backgroundColor = QColor("blue");
    backgroundColor.setAlpha(40 * 2.55);

    rectangleData2.position.position = position;
    rectangleData2.color.color       = backgroundColor;
    rectangleData2.color.opacity     = 40;
    rectangleData2.position.radius   = 0.80;
    rectangleData2.position.mode     = Qt::SizeMode::AbsoluteSize;

    borderColor = QColor("Purple");
    borderColor.setAlpha(40 * 2.55);

    rectangleData2.border.color.color   = borderColor;
    rectangleData2.border.color.opacity = 40;
    rectangleData2.border.borderWidth   = 5;

    painter::data::PaintableTextData textData1;

    position = painter::data::PositionData { 200, 400, 600, 800 };

    textData1.background.position.position = position;
    textData1.background.position.radius   = 0.8;
    textData1.background.position.mode     = Qt::SizeMode::RelativeSize;

    QColor textColor("orange");
    textColor.setAlpha(40 * 2.55);

    textData1.color.color   = textColor;
    textData1.color.opacity = 40;
    textData1.text          = "Chips";
    textData1.flags         = 42;
    textData1.pointSize     = 12;
    textData1.fontFamily    = "GrandPadano";

    backgroundColor = QColor("green");
    backgroundColor.setAlpha(40 * 2.55);

    textData1.background.color.color   = backgroundColor;
    textData1.background.color.opacity = 40;

    borderColor = QColor("blue");
    borderColor.setAlpha(40 * 2.55);

    textData1.background.border.color.color   = borderColor;
    textData1.background.border.color.opacity = 40;
    textData1.background.border.borderWidth   = 3;

    painter::data::PaintableTextData textData2;

    position = painter::data::PositionData { 20, 40, 60, 80 };

    textData2.background.position.position = position;
    textData2.background.position.radius   = 0.8;
    textData2.background.position.mode     = Qt::SizeMode::RelativeSize;

    textColor = QColor("orange");
    textColor.setAlpha(40 * 2.55);

    textData2.color.color   = textColor;
    textData2.color.opacity = 40;
    textData2.text          = "Chips";
    textData2.flags         = 42;
    textData2.pointSize     = 12;
    textData2.fontFamily    = "GrandPadano";

    backgroundColor = QColor("green");
    backgroundColor.setAlpha(40 * 2.55);

    textData2.background.color.color   = backgroundColor;
    textData2.background.color.opacity = 40;

    borderColor = QColor("blue");
    borderColor.setAlpha(40 * 2.55);

    textData2.background.border.color.color   = borderColor;
    textData2.background.border.color.opacity = 40;
    textData2.background.border.borderWidth   = 3;

    painter::data::PaintableRectangleData rectangleData3;

    painter::data::PaintableListData listData;

    position = painter::data::PositionData { 33, 44, 55, 66 };

    listData.position = position;
    listData.paintableItemListModel.paintableItemList.append(rectangleData1);
    listData.paintableItemListModel.paintableItemList.append(rectangleData2);
    listData.paintableItemListModel.paintableItemList.append(textData1);

    documenttemplate::data::RootPaintableItemListData rootPaintableListData;

    rootPaintableListData.paintableList.append(rectangleData1);
    rootPaintableListData.paintableList.append(rectangleData2);
    rootPaintableListData.paintableList.append(textData1);
    rootPaintableListData.paintableList.append(rectangleData3);
    rootPaintableListData.paintableList.append(textData2);
    rootPaintableListData.paintableList.append(listData);

    QList<QPointer<painter::controller::InterfaceQmlPaintableItem>> listOfpaintableItems;

    {
        documenttemplate::controller::RootPaintableItemList rootPaintableItemList(rootPaintableListData);

        QSignalSpy modelResetSpy(&rootPaintableItemList, &QAbstractListModel::modelReset);

        QCOMPARE(rootPaintableItemList.rowCount(), 6);

        painter::controller::InterfaceQmlPaintableItem* paintableItem = rootPaintableItemList.at(0);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData1);

        paintableItem = rootPaintableItemList.at(1);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData2);

        paintableItem = rootPaintableItemList.at(2);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(), textData1);

        paintableItem = rootPaintableItemList.at(3);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData3);

        paintableItem = rootPaintableItemList.at(4);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(), textData2);

        paintableItem = rootPaintableItemList.at(5);
        QVERIFY(qobject_cast<painter::controller::PaintableList*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableList*>(paintableItem)->objectData(), listData);

        QCOMPARE(rootPaintableItemList.objectData(), rootPaintableListData);

        for (int i = 0; i < 6; ++i)
        {
            QPointer<painter::controller::InterfaceQmlPaintableItem> item = rootPaintableItemList.at(i);
            listOfpaintableItems.append(item);
            QCOMPARE(item->parent(), &rootPaintableItemList);
        }

        for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems) { QVERIFY(item); }
        //-------------------------------------------------------
        // Test set data
        //-------------------------------------------------------

        rootPaintableListData.paintableList.clear();

        rootPaintableListData.paintableList.append(rectangleData1);
        rootPaintableListData.paintableList.append(textData1);
        rootPaintableListData.paintableList.append(rectangleData2);
        rootPaintableListData.paintableList.append(rectangleData3);
        rootPaintableListData.paintableList.append(textData2);
        rootPaintableListData.paintableList.append(listData);

        rootPaintableItemList.setObjectData(rootPaintableListData);

        QCOMPARE(listOfpaintableItems.size(), 6);
        for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems)
        {
            QTRY_VERIFY(item.isNull());
        }
        listOfpaintableItems.clear();

        QCOMPARE(modelResetSpy.size(), 1);

        QCOMPARE(rootPaintableItemList.rowCount(), 6);

        paintableItem = rootPaintableItemList.at(0);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData1);

        paintableItem = rootPaintableItemList.at(1);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(), textData1);

        paintableItem = rootPaintableItemList.at(2);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData2);

        paintableItem = rootPaintableItemList.at(3);
        QVERIFY(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableRectangle*>(paintableItem)->objectData(), rectangleData3);

        paintableItem = rootPaintableItemList.at(4);
        QVERIFY(qobject_cast<painter::controller::PaintableText*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableText*>(paintableItem)->objectData(), textData2);

        paintableItem = rootPaintableItemList.at(5);
        QVERIFY(qobject_cast<painter::controller::PaintableList*>(paintableItem));
        QCOMPARE(qobject_cast<painter::controller::PaintableList*>(paintableItem)->objectData(), listData);

        QCOMPARE(rootPaintableItemList.objectData(), rootPaintableListData);

        for (int i = 0; i < 6; ++i)
        {
            QPointer<painter::controller::InterfaceQmlPaintableItem> item = rootPaintableItemList.at(i);
            listOfpaintableItems.append(item);
            QCOMPARE(item->parent(), &rootPaintableItemList);
        }

        for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems) { QVERIFY(item); }
    }

    QCOMPARE(listOfpaintableItems.size(), 6);
    for (const QPointer<painter::controller::InterfaceQmlPaintableItem>& item : listOfpaintableItems)
    {
        QVERIFY(item.isNull());
    }
}

QTEST_GUILESS_MAIN(Test_RootPaintableItemList)

#include "Test_RootPaintableItemList.moc"
