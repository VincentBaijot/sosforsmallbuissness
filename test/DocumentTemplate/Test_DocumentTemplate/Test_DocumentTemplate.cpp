#include <QSignalSpy>
#include <QTest>

#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/PageSize.hpp"
#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"

class Test_DocumentTemplate : public QObject
{
    Q_OBJECT

  public:
    Test_DocumentTemplate() = default;

  private slots:
    void data();
    void setData();
};

void Test_DocumentTemplate::data()
{
    documenttemplate::data::RootPaintableItemListData rootPaintableItemListData;

    {
        using namespace painter::data;
        rootPaintableItemListData.paintableList.append(PaintableRectangleData());
        rootPaintableItemListData.paintableList.append(PaintableTextData());
        rootPaintableItemListData.paintableList.append(
          PaintableListData { PositionData { 20, 30, 40, 50 },
                              PaintableListData::RepetitionDirection::RightRepetition,
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableItemListModelData { QList<VariantListableItemData> {
                                PaintableRectangleData {}, PaintableTextData {} } } });
    }

    documenttemplate::data::PageSizeData pageSizeData;
    pageSizeData.setPageSize(QPageSize::A4);

    documenttemplate::controller::DocumentTemplate documenttemplate;
    documenttemplate.pageSize()->setObjectData(pageSizeData);
    documenttemplate.paintableItemList()->setObjectData(rootPaintableItemListData);

    documenttemplate::data::DocumentTemplateData documentTemplateData(documenttemplate.objectData());
    QCOMPARE(documentTemplateData.pageSizeData, pageSizeData);
    QCOMPARE(documentTemplateData.rootPaintableItemListData, rootPaintableItemListData);
}

void Test_DocumentTemplate::setData()
{
    documenttemplate::data::DocumentTemplateData documentTemplateData;

    {
        using namespace painter::data;
        documentTemplateData.rootPaintableItemListData.paintableList.append(
          painter::data::PaintableTextData());
        documentTemplateData.rootPaintableItemListData.paintableList.append(
          painter::data::PaintableRectangleData());
        documentTemplateData.rootPaintableItemListData.paintableList.append(
          PaintableListData { PositionData { 20, 30, 40, 50 },
                              PaintableListData::RepetitionDirection::RightRepetition,
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableItemListModelData { QList<VariantListableItemData> {
                                PaintableRectangleData {}, PaintableTextData {} } } });
    }

    documentTemplateData.pageSizeData.setPageSize(QPageSize::A2);

    documenttemplate::controller::DocumentTemplate documenttemplate(documentTemplateData);

    QCOMPARE(documenttemplate.pageSize()->objectData(), documentTemplateData.pageSizeData);
    QCOMPARE(documenttemplate.paintableItemList()->objectData(), documentTemplateData.rootPaintableItemListData);

    QCOMPARE(documenttemplate.objectData(), documentTemplateData);

    documentTemplateData.pageSizeData.setPageSize(QPageSize::A5);

    documentTemplateData.rootPaintableItemListData.paintableList.clear();

    documentTemplateData.rootPaintableItemListData.paintableList.append(
      painter::data::PaintableRectangleData());
    documentTemplateData.rootPaintableItemListData.paintableList.append(painter::data::PaintableTextData());

    documenttemplate.setObjectData(documentTemplateData);

    QCOMPARE(documenttemplate.pageSize()->objectData(), documentTemplateData.pageSizeData);
    QCOMPARE(documenttemplate.paintableItemList()->objectData(), documentTemplateData.rootPaintableItemListData);

    QCOMPARE(documenttemplate.objectData(), documentTemplateData);
}

QTEST_APPLESS_MAIN(Test_DocumentTemplate)

#include "Test_DocumentTemplate.moc"
