#include <QSignalSpy>
#include <QTest>

#include "DocumentTemplate/controllers/PageSize.hpp"

class Test_PageSize : public QObject
{
    Q_OBJECT

  public:
    Test_PageSize() = default;

  private slots:
    void initialValue();
    void setUnit();
    void setPageSizeId();
    void setSize();
    void setResolution();
    void data();
    void setData();

  private:
    void _initializeSpies(const documenttemplate::controller::PageSize* templatePageSize);

    QSharedPointer<QSignalSpy> _pageSizeIdChangedSpy;
    QSharedPointer<QSignalSpy> _resolutionChangedSpy;
    QSharedPointer<QSignalSpy> _sizeChangedSpy;
    QSharedPointer<QSignalSpy> _sizePixelChangedSpy;
    QSharedPointer<QSignalSpy> _unitChangedSpy;
    QSharedPointer<QSignalSpy> _unitPerPixelChangedSpy;
};

constexpr double unitPerPixelPrecision = 1000.0;

void Test_PageSize::initialValue()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;

    _initializeSpies(&pageSize);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(static_cast<int>(documenttemplate::data::PageSizeData::Unit::Millimeter),
             static_cast<int>(QPageSize::Millimeter));
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSize::setUnit()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;

    _initializeSpies(&pageSize);

    pageSize.setUnit(documenttemplate::data::PageSizeData::Unit::Millimeter);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / qreal(QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                                 QPageSize::Millimeter,
                                 QString(),
                                 QPageSize::ExactMatch)
                         .sizePixels(pageSize.resolution())
                         .width()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    pageSize.setUnit(documenttemplate::data::PageSizeData::Unit::Point);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Point);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Point));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Point,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Point,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 1);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    pageSize.setUnit(documenttemplate::data::PageSizeData::Unit::Millimeter);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 0);
    QCOMPARE(_unitChangedSpy->count(), 2);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 2);
}

void Test_PageSize::setPageSizeId()
{
    QPageSize qPageSize(QPageSize::A2);
    documenttemplate::controller::PageSize pageSize;
    ;

    _initializeSpies(&pageSize);

    pageSize.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A2);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A2);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A2),
             static_cast<int>(QPageSize::A2));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::A8);
    pageSize.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A8);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A8);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A8),
             static_cast<int>(QPageSize::A8));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 2);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSize::setSize()
{
    QPageSize qPageSize(QPageSize::A2);
    documenttemplate::controller::PageSize pageSize;

    _initializeSpies(&pageSize);

    pageSize.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A2);

    pageSize.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A2);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A2),
             static_cast<int>(QPageSize::A2));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QPageSize::A8);
    pageSize.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A8);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A8),
             static_cast<int>(QPageSize::A8));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 2);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 2);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);

    qPageSize = QPageSize(QSizeF(130, 32), QPageSize::Millimeter);
    pageSize.setSize(qPageSize.size(QPageSize::Millimeter));

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::Custom);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::Custom),
             static_cast<int>(QPageSize::Custom));
    QCOMPARE(pageSize.resolution(), 1200);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 3);
    QCOMPARE(_resolutionChangedSpy->count(), 0);
    QCOMPARE(_sizeChangedSpy->count(), 3);
    QCOMPARE(_sizePixelChangedSpy->count(), 3);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 0);
}

void Test_PageSize::setResolution()
{
    QPageSize qPageSize(QPageSize::A4);
    documenttemplate::controller::PageSize pageSize;

    _initializeSpies(&pageSize);

    pageSize.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A4);

    pageSize.setResolution(500);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 500);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 1);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    pageSize.setResolution(1300);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A4);
    QCOMPARE(static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::A4),
             static_cast<int>(QPageSize::A4));
    QCOMPARE(pageSize.resolution(), 1300);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Millimeter);
    QCOMPARE(pageSize.size(), qPageSize.size(QPageSize::Millimeter));
    QCOMPARE(pageSize.sizePixel(), qPageSize.sizePixels(pageSize.resolution()));
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .width());
    QCOMPARE(pageSize.unitPerPixel(),
             unitPerPixelPrecision
               / QPageSize(QSize(unitPerPixelPrecision, unitPerPixelPrecision),
                           QPageSize::Millimeter,
                           QString(),
                           QPageSize::ExactMatch)
                   .sizePixels(pageSize.resolution())
                   .height());

    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().width()), qRound(pageSize.size().width()));
    QCOMPARE(qRound(pageSize.unitPerPixel() * pageSize.sizePixel().height()), qRound(pageSize.size().height()));

    QCOMPARE(_pageSizeIdChangedSpy->count(), 0);
    QCOMPARE(_resolutionChangedSpy->count(), 2);
    QCOMPARE(_sizeChangedSpy->count(), 0);
    QCOMPARE(_sizePixelChangedSpy->count(), 2);
    QCOMPARE(_unitChangedSpy->count(), 0);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 2);
}

void Test_PageSize::data()
{
    documenttemplate::controller::PageSize pageSize;
    pageSize.setUnit(documenttemplate::data::PageSizeData::Unit::Point);
    pageSize.setResolution(600);
    pageSize.setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A5);

    documenttemplate::data::PageSizeData pageSizeData = pageSize.objectData();

    QCOMPARE(pageSizeData.pageSize(), QPageSize(QPageSize::A5));
    QCOMPARE(pageSizeData.unit(), documenttemplate::data::PageSizeData::Unit::Point);
    QCOMPARE(pageSizeData.resolution(), 600);
}

void Test_PageSize::setData()
{
    documenttemplate::data::PageSizeData pageSizeData;

    pageSizeData.setPageSize(QPageSize(QPageSize::A6));
    pageSizeData.setResolution(250);
    pageSizeData.setUnit(documenttemplate::data::PageSizeData::Unit::Inch);

    documenttemplate::controller::PageSize pageSize(pageSizeData);

    _initializeSpies(&pageSize);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A6);
    QCOMPARE(pageSize.resolution(), 250);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Inch);

    pageSizeData.setPageSize(QPageSize(QPageSize::A2));
    pageSizeData.setResolution(3000);
    pageSizeData.setUnit(documenttemplate::data::PageSizeData::Unit::Cicero);

    pageSize.setObjectData(pageSizeData);

    QCOMPARE(_pageSizeIdChangedSpy->count(), 1);
    QCOMPARE(_resolutionChangedSpy->count(), 1);
    QCOMPARE(_sizeChangedSpy->count(), 1);
    QCOMPARE(_sizePixelChangedSpy->count(), 1);
    QCOMPARE(_unitChangedSpy->count(), 1);
    QCOMPARE(_unitPerPixelChangedSpy->count(), 1);

    QCOMPARE(pageSize.pageSizeId(), documenttemplate::controller::PageSize::PageSizeId::A2);
    QCOMPARE(pageSize.resolution(), 3000);
    QCOMPARE(pageSize.unit(), documenttemplate::data::PageSizeData::Unit::Cicero);

    QCOMPARE(pageSize.objectData(), pageSizeData);
}

void Test_PageSize::_initializeSpies(const documenttemplate::controller::PageSize* templatePageSize)
{
    _pageSizeIdChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::pageSizeIdChanged));
    _resolutionChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::resolutionChanged));
    _sizeChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::sizeChanged));
    _sizePixelChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::sizePixelChanged));
    _unitChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::unitChanged));
    _unitPerPixelChangedSpy = QSharedPointer<QSignalSpy>(
      new QSignalSpy(templatePageSize, &documenttemplate::controller::PageSize::unitPerPixelChanged));
}

QTEST_APPLESS_MAIN(Test_PageSize)

#include "Test_PageSize.moc"
