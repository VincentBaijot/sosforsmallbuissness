#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"

class Test_ListDataDefinitionListModel : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void data();
    void setData();
};

using DataType = documentdatadefinitions::data::DataDefinitionData::DataType;

void Test_ListDataDefinitionListModel::data()
{
    documentdatadefinitions::data::ListDataDefinitionListData testListDataDefinitionListData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList;

    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel;

    // Already have one list data definition
    listDataDefinitionListModel.removeListDataDefinition(0);

    for (qsizetype index = 0; index < testListDataDefinitionListData.listOfDataDefinition.size(); ++index)
    {
        listDataDefinitionListModel.addListDataDefinition();

        documentdatadefinitions::controller::ListDataDefinition* listDataDefinition =
          qvariant_cast<documentdatadefinitions::controller::ListDataDefinition*>(
            listDataDefinitionListModel.data(listDataDefinitionListModel.index(index)));

        listDataDefinition->setObjectData(testListDataDefinitionListData.listOfDataDefinition.at(index));
    }

    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData(
      listDataDefinitionListModel.objectData());

    QCOMPARE(listDataDefinitionListData.listOfDataDefinition.size(),
             testListDataDefinitionListData.listOfDataDefinition.size());

    for (qsizetype index = 0; index < listDataDefinitionListData.listOfDataDefinition.size(); ++index)
    {
        QCOMPARE(listDataDefinitionListData.listOfDataDefinition.at(index),
                 testListDataDefinitionListData.listOfDataDefinition.at(index));
    }

    QCOMPARE(listDataDefinitionListData, testListDataDefinitionListData);
}

void Test_ListDataDefinitionListModel::setData()
{
    QList<QPointer<documentdatadefinitions::controller::ListDataDefinition>> listDataDefinitions;
    {
        //-------------------------------------------------------
        // Test constructor with data
        //-------------------------------------------------------

        documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData =
          datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList;

        documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel(
          listDataDefinitionListData);

        QSignalSpy modelAboutToBeResetSpy(&listDataDefinitionListModel, &QAbstractListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&listDataDefinitionListModel, &QAbstractListModel::modelReset);
        QSignalSpy dataChangedSpy(&listDataDefinitionListModel, &QAbstractListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(&listDataDefinitionListModel,
                                            &QAbstractListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&listDataDefinitionListModel, &QAbstractListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&listDataDefinitionListModel,
                                           &QAbstractListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&listDataDefinitionListModel, &QAbstractListModel::rowsRemoved);

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        QCOMPARE(listDataDefinitionListModel.rowCount(), listDataDefinitionListData.listOfDataDefinition.size());

        for (int index = 0; index < listDataDefinitionListModel.rowCount(); ++index)
        {
            QPointer<documentdatadefinitions::controller::ListDataDefinition> listDataDefinitionPointer =
              qvariant_cast<documentdatadefinitions::controller::ListDataDefinition*>(
                listDataDefinitionListModel.data(listDataDefinitionListModel.index(index)));

            listDataDefinitions.append(listDataDefinitionPointer);

            QVERIFY(!listDataDefinitionPointer.isNull());

            QCOMPARE(listDataDefinitionPointer->objectData(),
                     listDataDefinitionListData.listOfDataDefinition.at(index));
        }

        //-------------------------------------------------------
        // Test set data function
        //-------------------------------------------------------

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        {
            documentdatadefinitions::data::ListDataDefinitionListData testListDataDefinitionListData =
              listDataDefinitionListData;
            // Reverse the list
            listDataDefinitionListData.listOfDataDefinition = {
                testListDataDefinitionListData.listOfDataDefinition.rbegin(),
                testListDataDefinitionListData.listOfDataDefinition.rend()
            };
        }

        listDataDefinitionListModel.setObjectData(listDataDefinitionListData);

        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        for (const QPointer<documentdatadefinitions::controller::ListDataDefinition>& listDataDefinitionPointer :
             listDataDefinitions)
        {
            QTRY_VERIFY(listDataDefinitionPointer.isNull());
        }

        listDataDefinitions.clear();

        QCOMPARE(listDataDefinitionListModel.rowCount(), listDataDefinitionListData.listOfDataDefinition.size());

        for (int index = 0; index < listDataDefinitionListModel.rowCount(); ++index)
        {
            QPointer<documentdatadefinitions::controller::ListDataDefinition> listDataDefinitionPointer =
              qvariant_cast<documentdatadefinitions::controller::ListDataDefinition*>(
                listDataDefinitionListModel.data(listDataDefinitionListModel.index(index)));

            listDataDefinitions.append(listDataDefinitionPointer);

            QVERIFY(!listDataDefinitionPointer.isNull());

            QCOMPARE(listDataDefinitionPointer->objectData(),
                     listDataDefinitionListData.listOfDataDefinition.at(index));
        }

        listDataDefinitionListModel.setObjectData(listDataDefinitionListData);

        for (const QPointer<documentdatadefinitions::controller::ListDataDefinition>& listDataDefinitionPointer :
             listDataDefinitions)
        {
            QVERIFY(!listDataDefinitionPointer.isNull());
        }

        QCOMPARE(listDataDefinitionListModel.objectData(), listDataDefinitionListData);
    }

    for (const QPointer<documentdatadefinitions::controller::ListDataDefinition>& listDataDefinitionPointer :
         listDataDefinitions)
    {
        QVERIFY(listDataDefinitionPointer.isNull());
    }
}

QTEST_GUILESS_MAIN(Test_ListDataDefinitionListModel)

#include "Test_ListDataDefinitionListModel.moc"
