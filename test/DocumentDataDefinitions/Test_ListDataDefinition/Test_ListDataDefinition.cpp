#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"

class Test_ListDataDefinition : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
};

void Test_ListDataDefinition::data()
{
    documentdatadefinitions::data::ListDataDefinitionData testListDataDefinitionData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList.listOfDataDefinition.at(0);

    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData(
      testListDataDefinitionData.dataDefinitionListData);
    documentdatadefinitions::data::DataTablesData demonstrationData(testListDataDefinitionData.demonstrationTable);

    documentdatadefinitions::controller::ListDataDefinition listDataDefinition;
    listDataDefinition.dataDefinitionListModel()->setObjectData(dataDefinitionListData);
    listDataDefinition.demonstrationDataTable()->setObjectData(demonstrationData);

    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData = listDataDefinition.objectData();

    QCOMPARE(listDataDefinitionData.dataDefinitionListData, dataDefinitionListData);
    QCOMPARE(listDataDefinitionData.demonstrationTable, demonstrationData);
}

void Test_ListDataDefinition::setData()
{
    //-------------------------------------------------------
    // Test constructor with data
    //-------------------------------------------------------
    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList.listOfDataDefinition.at(0);

    documentdatadefinitions::controller::ListDataDefinition listDataDefinition(listDataDefinitionData);

    QCOMPARE(listDataDefinition.dataDefinitionListModel()->objectData(),
             listDataDefinitionData.dataDefinitionListData);
    QCOMPARE(listDataDefinition.demonstrationDataTable()->objectData(), listDataDefinitionData.demonstrationTable);

    QCOMPARE(listDataDefinition.objectData(), listDataDefinitionData);

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------

    listDataDefinitionData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList.listOfDataDefinition.at(1);

    listDataDefinition.setObjectData(listDataDefinitionData);

    QCOMPARE(listDataDefinition.dataDefinitionListModel()->objectData(),
             listDataDefinitionData.dataDefinitionListData);
    QCOMPARE(listDataDefinition.demonstrationDataTable()->objectData(), listDataDefinitionData.demonstrationTable);

    QCOMPARE(listDataDefinition.objectData(), listDataDefinitionData);
}

QTEST_APPLESS_MAIN(Test_ListDataDefinition)

#include "Test_ListDataDefinition.moc"
