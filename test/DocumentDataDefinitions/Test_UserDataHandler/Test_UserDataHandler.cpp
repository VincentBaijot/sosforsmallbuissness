#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"

class Test_UserDataHandler : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
    void setDataDefinitionsHandler();
};

void Test_UserDataHandler::data()
{
    documentdatadefinitions::controller::DataDefinitionsHandler dataDefinitionsHandler {
        datatemplatetestdata::dataDefinitionHandlerTestData()
    };
    documentdatadefinitions::controller::UserDataHandler userDataHandler { &dataDefinitionsHandler };

    documentdatadefinitions::data::UserDataHandlerData testUserDataHandlerData =
      datatemplatetestdata::userDataHandlerData();

    //----------------------------------------------------------------------------
    // Prepare the data of the list data definition list
    //----------------------------------------------------------------------------

    userDataHandler.generalVariableDataList()->setObjectData(testUserDataHandlerData.generalVariableDataList);
    userDataHandler.dataTableModelListModel()->setObjectData(testUserDataHandlerData.dataTableModelListModel);

    //----------------------------------------------------------------------------
    // Check the data
    //----------------------------------------------------------------------------

    documentdatadefinitions::data::UserDataHandlerData userDataHandlerData = userDataHandler.objectData();

    QCOMPARE(userDataHandlerData.generalVariableDataList, testUserDataHandlerData.generalVariableDataList);
    QCOMPARE(userDataHandlerData.dataTableModelListModel, testUserDataHandlerData.dataTableModelListModel);

    QCOMPARE(userDataHandlerData, testUserDataHandlerData);
}

void Test_UserDataHandler::setData()
{
    documentdatadefinitions::controller::DataDefinitionsHandler dataDefinitionsHandler {
        datatemplatetestdata::dataDefinitionHandlerTestData()
    };

    QPointer<documentdatadefinitions::controller::DataTablesModel> generalVariableList;
    QPointer<documentdatadefinitions::controller::DataTableModelListModel> dataTableModelListModel;

    {
        //----------------------------------------------------------
        // Test constructor with data
        //----------------------------------------------------------

        documentdatadefinitions::data::UserDataHandlerData testUserDataHandlerData =
          datatemplatetestdata::userDataHandlerData();

        documentdatadefinitions::controller::UserDataHandler userDataHandler(&dataDefinitionsHandler, testUserDataHandlerData);

        generalVariableList     = userDataHandler.generalVariableDataList();
        dataTableModelListModel = userDataHandler.dataTableModelListModel();

        QCOMPARE(userDataHandler.generalVariableDataList()->objectData(),
                 testUserDataHandlerData.generalVariableDataList);
        QCOMPARE(userDataHandler.dataTableModelListModel()->objectData(),
                 testUserDataHandlerData.dataTableModelListModel);

        QCOMPARE(userDataHandler.objectData(), testUserDataHandlerData);

        //----------------------------------------------------------
        // Test set data function
        //----------------------------------------------------------

        testUserDataHandlerData.generalVariableDataList.dataTables[0][0] = " Another Product 1 Key 1";
        testUserDataHandlerData.generalVariableDataList.dataTables[1][0] = " Another Product 2 Key 1";
        testUserDataHandlerData.generalVariableDataList.dataTables[2][0] = " Another Product 3 Key 1";

        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[0].dataTables[0][1] =
          "Another Product a";
        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[0].dataTables[1][1] =
          "Another Product b";
        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[0].dataTables[2][1] =
          "Another Product c";

        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[1].dataTables[0][1] =
          "Another Product 1";
        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[1].dataTables[1][1] =
          "Another Product 2";
        testUserDataHandlerData.dataTableModelListModel.dataTableModelList[1].dataTables[2][1] =
          "Another Product 3";

        userDataHandler.setObjectData(testUserDataHandlerData);

        QCOMPARE(userDataHandler.generalVariableDataList()->objectData(),
                 testUserDataHandlerData.generalVariableDataList);
        QCOMPARE(userDataHandler.dataTableModelListModel()->objectData(),
                 testUserDataHandlerData.dataTableModelListModel);

        QCOMPARE(userDataHandler.objectData(), testUserDataHandlerData);

        QVERIFY(!generalVariableList.isNull());
        QVERIFY(!dataTableModelListModel.isNull());
    }

    QVERIFY(generalVariableList.isNull());
    QVERIFY(dataTableModelListModel.isNull());
}

void Test_UserDataHandler::setDataDefinitionsHandler()
{
    // Default constructor and set data
    {
        documentdatadefinitions::controller::UserDataHandler userDataHandler;

        {
            documentdatadefinitions::controller::DataDefinitionsHandler dataDefinitionsHandler {
                datatemplatetestdata::dataDefinitionHandlerTestData()
            };
            userDataHandler.setDataDefinitionsHandler(&dataDefinitionsHandler);

            QCOMPARE(userDataHandler.dataDefinitionsHandler(), &dataDefinitionsHandler);
            QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(),
                     dataDefinitionsHandler.generalVariableList());
            QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(),
                     dataDefinitionsHandler.listDataDefinitionListModel());
        }

        QCOMPARE(userDataHandler.dataDefinitionsHandler(), nullptr);
        QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(), nullptr);
        QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(), nullptr);
    }

    // Constructor setting the value of data definitions
    {
        documentdatadefinitions::controller::DataDefinitionsHandler* dataDefinitionsHandler =
          new documentdatadefinitions::controller::DataDefinitionsHandler { datatemplatetestdata::dataDefinitionHandlerTestData() };

        documentdatadefinitions::controller::UserDataHandler userDataHandler(dataDefinitionsHandler);

        QCOMPARE(userDataHandler.dataDefinitionsHandler(), dataDefinitionsHandler);
        QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(),
                 dataDefinitionsHandler->generalVariableList());
        QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(),
                 dataDefinitionsHandler->listDataDefinitionListModel());

        delete dataDefinitionsHandler;

        QCOMPARE(userDataHandler.dataDefinitionsHandler(), nullptr);
        QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(), nullptr);
        QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(), nullptr);
    }

    // Constructor setting the value of data definitions and the user data
    {
        documentdatadefinitions::controller::DataDefinitionsHandler* dataDefinitionsHandler =
          new documentdatadefinitions::controller::DataDefinitionsHandler { datatemplatetestdata::dataDefinitionHandlerTestData() };

        documentdatadefinitions::controller::UserDataHandler userDataHandler(dataDefinitionsHandler,
                                                      datatemplatetestdata::userDataHandlerData());

        QCOMPARE(userDataHandler.dataDefinitionsHandler(), dataDefinitionsHandler);
        QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(),
                 dataDefinitionsHandler->generalVariableList());
        QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(),
                 dataDefinitionsHandler->listDataDefinitionListModel());

        delete dataDefinitionsHandler;

        QCOMPARE(userDataHandler.dataDefinitionsHandler(), nullptr);
        QCOMPARE(userDataHandler.generalVariableDataList()->dataDefinitionModel(), nullptr);
        QCOMPARE(userDataHandler.dataTableModelListModel()->listDataDefinitionListModel(), nullptr);
    }
}

QTEST_APPLESS_MAIN(Test_UserDataHandler)

#include "Test_UserDataHandler.moc"
