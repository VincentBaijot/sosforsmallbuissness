#include <QTest>

#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"

class Test_DataHandler : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
};

void Test_DataHandler::data()
{
    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData =
      datatemplatetestdata::dataDefinitionHandlerTestData();
    documentdatadefinitions::data::UserDataHandlerData userDataHandlerData =
      datatemplatetestdata::userDataHandlerData();

    documentdatadefinitions::controller::DataHandler dataHandler;

    //----------------------------------------------------------------------------
    // Prepare the data of the list data definition list
    //----------------------------------------------------------------------------

    dataHandler.dataDefinitionsHandler()->setObjetData(dataDefinitionsHandlerData);
    dataHandler.userDataHandler()->setObjectData(userDataHandlerData);

    //----------------------------------------------------------------------------
    // Check the data
    //----------------------------------------------------------------------------

    documentdatadefinitions::data::DataHandlerData dataHandlerData = dataHandler.objectData();

    QCOMPARE(dataHandlerData.dataDefinitionsHandler, dataDefinitionsHandlerData);
    QCOMPARE(dataHandlerData.userDataHandler, userDataHandlerData);

    documentdatadefinitions::data::DataHandlerData testDataHandlerData { dataDefinitionsHandlerData,
                                                                     userDataHandlerData };

    QCOMPARE(dataHandlerData, testDataHandlerData);
}

void Test_DataHandler::setData()
{
    QPointer<documentdatadefinitions::controller::DataDefinitionsHandler> dataDefinitionsHandler;
    QPointer<documentdatadefinitions::controller::UserDataHandler> userDataHandler;

    {
        //----------------------------------------------------------
        // Test constructor with data
        //----------------------------------------------------------

        documentdatadefinitions::data::DataHandlerData testDataHandlerData {
            datatemplatetestdata::dataDefinitionHandlerTestData(), datatemplatetestdata::userDataHandlerData()
        };

        documentdatadefinitions::controller::DataHandler dataHandler(testDataHandlerData);

        dataDefinitionsHandler = dataHandler.dataDefinitionsHandler();
        userDataHandler        = dataHandler.userDataHandler();

        QCOMPARE(dataHandler.dataDefinitionsHandler()->objectData(), testDataHandlerData.dataDefinitionsHandler);
        QCOMPARE(dataHandler.userDataHandler()->objectData(), testDataHandlerData.userDataHandler);

        QCOMPARE(dataHandler.objectData(), testDataHandlerData);

        //----------------------------------------------------------
        // Test set data function
        //----------------------------------------------------------

        // Edit data definition data

        testDataHandlerData.dataDefinitionsHandler.generalVariableList.listName = "The list name 2";

        for (qsizetype index = 0;
             index < testDataHandlerData.dataDefinitionsHandler.generalVariableList.dataDefinitions.size();
             ++index)
        {
            testDataHandlerData.dataDefinitionsHandler.generalVariableList.dataDefinitions[index].description =
              QString("Another description ") + QString::number(index);
        }

        for (qsizetype listIndex = 0;
             listIndex
             < testDataHandlerData.dataDefinitionsHandler.listDataDefinitionList.listOfDataDefinition.size();
             ++listIndex)
        {
            documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData =
              testDataHandlerData.dataDefinitionsHandler.listDataDefinitionList.listOfDataDefinition[listIndex]
                .dataDefinitionListData;

            dataDefinitionListData.listName = QString("AnotherName ") + QString::number(listIndex);

            for (qsizetype dataIndex = 0; dataIndex < dataDefinitionListData.dataDefinitions.size(); ++dataIndex)
            {
                dataDefinitionListData.dataDefinitions[dataIndex].name =
                  QString("Another name") + QString::number(dataIndex);
            }
        }

        // Edit user data

        testDataHandlerData.userDataHandler.generalVariableDataList.dataTables[0][0] = " Another Product 1 Key 1";
        testDataHandlerData.userDataHandler.generalVariableDataList.dataTables[1][0] = " Another Product 2 Key 1";
        testDataHandlerData.userDataHandler.generalVariableDataList.dataTables[2][0] = " Another Product 3 Key 1";

        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[0].dataTables[0][1] =
          "Another Product a";
        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[0].dataTables[1][1] =
          "Another Product b";
        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[0].dataTables[2][1] =
          "Another Product c";

        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[1].dataTables[0][1] =
          "Another Product 1";
        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[1].dataTables[1][1] =
          "Another Product 2";
        testDataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList[1].dataTables[2][1] =
          "Another Product 3";

        dataHandler.setObjectData(testDataHandlerData);

        QCOMPARE(dataHandler.dataDefinitionsHandler()->objectData(), testDataHandlerData.dataDefinitionsHandler);
        QCOMPARE(dataHandler.userDataHandler()->objectData(), testDataHandlerData.userDataHandler);

        QCOMPARE(dataHandler.objectData(), testDataHandlerData);

        QVERIFY(!dataDefinitionsHandler.isNull());
        QVERIFY(!userDataHandler.isNull());
    }

    QVERIFY(dataDefinitionsHandler.isNull());
    QVERIFY(userDataHandler.isNull());
}

QTEST_APPLESS_MAIN(Test_DataHandler)

#include "Test_DataHandler.moc"
