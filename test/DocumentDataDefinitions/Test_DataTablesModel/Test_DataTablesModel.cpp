
#include <QPointer>
#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/controllers/DataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"

class Test_DataTablesModel : public QObject
{
    Q_OBJECT

  public:
    explicit Test_DataTablesModel(QObject* parent = nullptr);

  private slots:
    void data();
    void setData();
    void setAndDeleteDataDefinitionsModel();
    void onDataDefinitionRemoved();
    void onDataDefinitionsModelReset();

  private:
    const documentdatadefinitions::controller::DataDefinitionListModel* const _dataDefinitionListModel;
};

Test_DataTablesModel::Test_DataTablesModel(QObject* parent)
    : _dataDefinitionListModel { new documentdatadefinitions::controller::DataDefinitionListModel(
        datatemplatetestdata::dataDefinitionHandlerTestData()
          .listDataDefinitionList.listOfDataDefinition.front()
          .dataDefinitionListData,
        this) }
{
}

void Test_DataTablesModel::data()
{
    documentdatadefinitions::data::DataTablesData testDataTablesData =
      datatemplatetestdata::userDataHandlerData().dataTableModelListModel.dataTableModelList.at(0);

    documentdatadefinitions::controller::DataTablesModel dataTablesModel;

    // By default a row is added in the model
    dataTablesModel.removeDataRow(0);

    dataTablesModel.setDataDefinitionModel(_dataDefinitionListModel);

    for (int index = 0; index < testDataTablesData.dataTables.size(); ++index)
    {
        dataTablesModel.addDataRow();

        const documentdatadefinitions::data::DataTableData& dataTable =
          testDataTablesData.dataTables.at(index);

        for (documentdatadefinitions::data::DataTableData::const_iterator iterator = dataTable.cbegin();
             iterator != dataTable.cend();
             ++iterator)
        {
            dataTablesModel.setData(dataTablesModel.index(index, iterator.key()), iterator.value());
        }
    }

    documentdatadefinitions::data::DataTablesData dataTablesData(dataTablesModel.objectData());

    QCOMPARE(dataTablesData.dataTables.size(), testDataTablesData.dataTables.size());

    for (int index = 0; index < dataTablesData.dataTables.size(); ++index)
    {
        QCOMPARE(dataTablesData.dataTables.at(0), testDataTablesData.dataTables.at(0));
    }

    QCOMPARE(dataTablesData, testDataTablesData);
}

void Test_DataTablesModel::setData()
{
    //-------------------------------------------------------
    // Test constructor with data
    //-------------------------------------------------------
    documentdatadefinitions::data::DataTablesData dataTablesData =
      datatemplatetestdata::userDataHandlerData().generalVariableDataList;

    documentdatadefinitions::controller::DataTablesModel dataTablesModel(_dataDefinitionListModel, dataTablesData);

    QSignalSpy listNameChangedSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::listNameChanged);
    QSignalSpy rowsAboutToBeInsertedSpy(&dataTablesModel,
                                        &documentdatadefinitions::controller::DataTablesModel::rowsAboutToBeInserted);
    QSignalSpy rowsInsertedSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::rowsInserted);
    QSignalSpy modelAboutToBeResetSpy(&dataTablesModel,
                                      &documentdatadefinitions::controller::DataTablesModel::modelAboutToBeReset);
    QSignalSpy modelResetSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::modelReset);

    QList<QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>> dataTables =
      dataTablesModel.dataTables();

    QCOMPARE(dataTables.size(), dataTablesData.dataTables.size());

    for (qsizetype indexTable = 0; indexTable < dataTables.size(); ++indexTable)
    {
        const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>& dataTable =
          dataTables.at(indexTable);
        const documentdatadefinitions::data::DataTableData& testDataTable =
          dataTablesData.dataTables.at(indexTable);

        for (QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>::const_iterator iterator =
               dataTable.cbegin();
             iterator != dataTable.cend();
             ++iterator)
        {
            documentdatadefinitions::data::DataTableData::const_iterator found =
              testDataTable.constFind(_dataDefinitionListModel->indexOf(iterator.key()));
            QVERIFY(found != testDataTable.cend());
            QCOMPARE(iterator.value(), found.value());
        }
    }

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------

    dataTablesData = datatemplatetestdata::userDataHandlerData().dataTableModelListModel.dataTableModelList.at(0);

    dataTablesModel.setObjectData(dataTablesData);

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 1);
    QCOMPARE(modelResetSpy.size(), 1);

    dataTables = dataTablesModel.dataTables();

    QCOMPARE(dataTables.size(), dataTablesData.dataTables.size());

    for (qsizetype indexTable = 0; indexTable < dataTables.size(); ++indexTable)
    {
        const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>& dataTable =
          dataTables.at(indexTable);
        const documentdatadefinitions::data::DataTableData& testDataTable =
          dataTablesData.dataTables.at(indexTable);

        QCOMPARE(dataTable.size(), testDataTable.size());

        for (QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>::const_iterator iterator =
               dataTable.cbegin();
             iterator != dataTable.cend();
             ++iterator)
        {
            documentdatadefinitions::data::DataTableData::const_iterator found =
              testDataTable.constFind(_dataDefinitionListModel->indexOf(iterator.key()));
            QVERIFY(found != testDataTable.cend());
            QCOMPARE(iterator.value(), found.value());
        }
    }

    dataTablesModel.setObjectData(dataTablesData);

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 1);
    QCOMPARE(modelResetSpy.size(), 1);

    dataTablesModel.setObjectData(dataTablesData);

    QCOMPARE(dataTables, dataTablesModel.dataTables());

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 1);
    QCOMPARE(modelResetSpy.size(), 1);

    QCOMPARE(dataTablesData, dataTablesModel.objectData());

    //-------------------------------------------------------
    // Test to set a value for a non existing definition
    //-------------------------------------------------------

    // No data definition with index 42
    dataTablesData.dataTables[0].insert(42, "Patate");

    dataTablesModel.setObjectData(dataTablesData);

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 2);
    QCOMPARE(modelResetSpy.size(), 2);

    dataTables = dataTablesModel.dataTables();

    QCOMPARE(dataTables.size(), dataTablesData.dataTables.size());

    {
        const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>& dataTable = dataTables.at(0);
        const documentdatadefinitions::data::DataTableData& testDataTable = dataTablesData.dataTables.at(0);

        // The data table do not contains the 42 element
        QCOMPARE(dataTable.size(), testDataTable.size() - 1);

        for (QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>::const_iterator iterator =
               dataTable.cbegin();
             iterator != dataTable.cend();
             ++iterator)
        {
            documentdatadefinitions::data::DataTableData::const_iterator found =
              testDataTable.constFind(_dataDefinitionListModel->indexOf(iterator.key()));
            QVERIFY(found != testDataTable.cend());
            QCOMPARE(iterator.value(), found.value());
        }
    }
}

void Test_DataTablesModel::setAndDeleteDataDefinitionsModel()
{
    // Default constructor and set data
    {
        documentdatadefinitions::controller::DataTablesModel dataTablesModel;

        {
            documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel {
                datatemplatetestdata::dataDefinitionHandlerTestData()
                  .listDataDefinitionList.listOfDataDefinition.front()
                  .dataDefinitionListData
            };

            dataTablesModel.setDataDefinitionModel(&dataDefinitionListModel);

            dataTablesModel.setObjectData(datatemplatetestdata::userDataHandlerData().generalVariableDataList);

            QCOMPARE(dataTablesModel.dataDefinitionModel(), &dataDefinitionListModel);
            QVERIFY(dataTablesModel.dataTables().size() != 0);

            for (const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString>& dataRow :
                 dataTablesModel.dataTables())
            {
                QVERIFY(dataRow.size() != 0);

                for (auto iterator = dataRow.cbegin(); iterator != dataRow.cend(); ++iterator)
                {
                    QVERIFY(iterator.key() != nullptr);
                }
            }
        }

        QCOMPARE(dataTablesModel.dataDefinitionModel(), nullptr);
        QCOMPARE(dataTablesModel.dataTables().size(), 0);
    }

    // Constructor setting the value of data definitions
    {
        documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
          new documentdatadefinitions::controller::DataDefinitionListModel {
              datatemplatetestdata::dataDefinitionHandlerTestData()
                .listDataDefinitionList.listOfDataDefinition.front()
                .dataDefinitionListData,
              this
          };

        documentdatadefinitions::controller::DataTablesModel dataTablesModel { dataDefinitionListModel };
        dataTablesModel.setObjectData(datatemplatetestdata::userDataHandlerData().generalVariableDataList);

        QCOMPARE(dataTablesModel.dataDefinitionModel(), dataDefinitionListModel);
        QVERIFY(dataTablesModel.dataTables().size() != 0);

        for (const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString> dataRow :
             dataTablesModel.dataTables())
        {
            QVERIFY(dataRow.size() != 0);

            for (auto iterator = dataRow.cbegin(); iterator != dataRow.cend(); ++iterator)
            {
                QVERIFY(iterator.key() != nullptr);
            }
        }

        delete dataDefinitionListModel;

        QCOMPARE(dataTablesModel.dataDefinitionModel(), nullptr);
        QCOMPARE(dataTablesModel.dataTables().size(), 0);
    }

    // Constructor setting the value of data definitions and the table data
    {
        documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
          new documentdatadefinitions::controller::DataDefinitionListModel {
              datatemplatetestdata::dataDefinitionHandlerTestData()
                .listDataDefinitionList.listOfDataDefinition.front()
                .dataDefinitionListData
          };

        documentdatadefinitions::controller::DataTablesModel dataTablesModel {
            dataDefinitionListModel, datatemplatetestdata::userDataHandlerData().generalVariableDataList
        };

        QCOMPARE(dataTablesModel.dataDefinitionModel(), dataDefinitionListModel);
        QVERIFY(dataTablesModel.dataTables().size() != 0);

        for (const QHash<const documentdatadefinitions::controller::DataDefinition* const, QString> dataRow :
             dataTablesModel.dataTables())
        {
            QVERIFY(dataRow.size() != 0);

            for (auto iterator = dataRow.cbegin(); iterator != dataRow.cend(); ++iterator)
            {
                QVERIFY(iterator.key() != nullptr);
            }
        }

        delete dataDefinitionListModel;

        QCOMPARE(dataTablesModel.dataDefinitionModel(), nullptr);
        QCOMPARE(dataTablesModel.dataTables().size(), 0);
    }
}

void Test_DataTablesModel::onDataDefinitionRemoved()
{
    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel {
        datatemplatetestdata::dataDefinitionHandlerTestData()
          .listDataDefinitionList.listOfDataDefinition.front()
          .dataDefinitionListData
    };

    documentdatadefinitions::controller::DataTablesModel dataTablesModel {
        &dataDefinitionListModel, datatemplatetestdata::userDataHandlerData().generalVariableDataList
    };

    documentdatadefinitions::controller::DataDefinition* dataDefinition                 = dataDefinitionListModel.at(0);
    QPointer<documentdatadefinitions::controller::DataDefinition> dataDefinitionPointer = dataDefinition;

    QVERIFY(!dataDefinitionPointer.isNull());

    QCOMPARE(dataTablesModel.dataTables().size(), 3);
    QVERIFY(dataTablesModel.dataTables().at(0).contains(dataDefinition));
    QVERIFY(dataTablesModel.dataTables().at(1).contains(dataDefinition));
    QVERIFY(dataTablesModel.dataTables().at(2).contains(dataDefinition));

    dataDefinitionListModel.removeDataDefinition(0);

    QTRY_VERIFY(dataDefinitionPointer.isNull());

    QCOMPARE(dataTablesModel.dataTables().size(), 3);
    QVERIFY(!dataTablesModel.dataTables().at(0).contains(dataDefinition));
    QVERIFY(!dataTablesModel.dataTables().at(1).contains(dataDefinition));
    QVERIFY(!dataTablesModel.dataTables().at(2).contains(dataDefinition));
}

void Test_DataTablesModel::onDataDefinitionsModelReset()
{
    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel {
        datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList
    };

    documentdatadefinitions::controller::DataTablesModel dataTablesModel {
        &dataDefinitionListModel, datatemplatetestdata::userDataHandlerData().generalVariableDataList
    };

    QSignalSpy listNameChangedSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::listNameChanged);
    QSignalSpy rowsAboutToBeInsertedSpy(&dataTablesModel,
                                        &documentdatadefinitions::controller::DataTablesModel::rowsAboutToBeInserted);
    QSignalSpy rowsInsertedSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::rowsInserted);
    QSignalSpy modelAboutToBeResetSpy(&dataTablesModel,
                                      &documentdatadefinitions::controller::DataTablesModel::modelAboutToBeReset);
    QSignalSpy modelResetSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::modelReset);
    QSignalSpy rowsAboutToBeRemovedSpy(&dataTablesModel,
                                       &documentdatadefinitions::controller::DataTablesModel::rowsAboutToBeRemoved);
    QSignalSpy rowsRemovedSpy(&dataTablesModel, &documentdatadefinitions::controller::DataTablesModel::rowsRemoved);

    QCOMPARE(listNameChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    QCOMPARE(dataTablesModel.rowCount(), 3);

    dataDefinitionListModel.setObjectData(datatemplatetestdata::dataDefinitionHandlerTestData()
                                            .listDataDefinitionList.listOfDataDefinition.front()
                                            .dataDefinitionListData);

    QCOMPARE(dataTablesModel.rowCount(), 0);

    QCOMPARE(listNameChangedSpy.size(), 1);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(modelAboutToBeResetSpy.size(), 1);
    QCOMPARE(modelResetSpy.size(), 1);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);
}

QTEST_GUILESS_MAIN(Test_DataTablesModel)

#include "Test_DataTablesModel.moc"
