#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"

class Test_DataTableModelListModel : public QObject
{
    Q_OBJECT

  private slots:
    void dataDefinitionListModelAdded();
    void dataDefinitionListModelRemoved();
    void setListDataDefinitionListModel();
    void setAndDeleteListDataDefinitionListModel();
    void onListDataDefinitionModelReset();
    void data();
    void setData();
};

void Test_DataTableModelListModel::dataDefinitionListModelAdded()
{
    QList<QPointer<documentdatadefinitions::controller::DataTablesModel>> listOfDataTables;
    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel;

    QCOMPARE(listDataDefinitionListModel.rowCount(), 1);

    listDataDefinitionListModel.removeListDataDefinition(0);

    // Test from an empty List of data definition
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        // First addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 1);
        QCOMPARE(dataTableModelListModel.rowCount(), 1);

        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
          listDataDefinitionListModel.at(0)->dataDefinitionListModel();

        QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(0), Qt::DisplayRole);
        documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
          qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
          dataTablesModel->dataDefinitionModel();
        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Second addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 2);
        QCOMPARE(dataTableModelListModel.rowCount(), 2);

        dataDefinitionListModel = listDataDefinitionListModel.at(1)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(1), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();
        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Third addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);
        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        dataDefinitionListModel = listDataDefinitionListModel.at(2)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(2), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();
        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
        {
            QVERIFY(!dataTableModelPointer.isNull());
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();

    // Test from a non empty List of data definition
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // First addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 4);
        QCOMPARE(dataTableModelListModel.rowCount(), 4);

        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
          listDataDefinitionListModel.at(3)->dataDefinitionListModel();

        QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(3), Qt::DisplayRole);
        documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
          qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
          dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Second addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 5);
        QCOMPARE(dataTableModelListModel.rowCount(), 5);

        dataDefinitionListModel = listDataDefinitionListModel.at(4)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(4), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Third addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 6);
        QCOMPARE(dataTableModelListModel.rowCount(), 6);

        dataDefinitionListModel = listDataDefinitionListModel.at(5)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(5), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
        {
            QVERIFY(!dataTableModelPointer.isNull());
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();

    // Remove then add
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 6);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        QCOMPARE(dataTableModelListModel.rowCount(), 6);

        for (int i = 0; i < 6; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        listDataDefinitionListModel.removeListDataDefinition(5);
        listDataDefinitionListModel.removeListDataDefinition(4);
        listDataDefinitionListModel.removeListDataDefinition(3);

        QTRY_VERIFY(listOfDataTables.at(5).isNull());
        QTRY_VERIFY(listOfDataTables.at(4).isNull());
        QTRY_VERIFY(listOfDataTables.at(3).isNull());

        listOfDataTables.remove(3, 3);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);
        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // First addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 4);
        QCOMPARE(dataTableModelListModel.rowCount(), 4);

        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
          listDataDefinitionListModel.at(3)->dataDefinitionListModel();

        QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(3), Qt::DisplayRole);
        documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
          qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
          dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Second addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 5);
        QCOMPARE(dataTableModelListModel.rowCount(), 5);

        dataDefinitionListModel = listDataDefinitionListModel.at(4)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(4), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        // Third addition of data

        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 6);
        QCOMPARE(dataTableModelListModel.rowCount(), 6);

        dataDefinitionListModel = listDataDefinitionListModel.at(5)->dataDefinitionListModel();

        dataVariant     = dataTableModelListModel.data(dataTableModelListModel.index(5), Qt::DisplayRole);
        dataTablesModel = qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
        dataDefinitionListModelFromDataTable = dataTablesModel->dataDefinitionModel();

        listOfDataTables.append(dataTablesModel);

        QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

        for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
        {
            QVERIFY(!dataTableModelPointer.isNull());
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();
}

void Test_DataTableModelListModel::dataDefinitionListModelRemoved()
{
    QList<QPointer<documentdatadefinitions::controller::DataTablesModel>> listOfDataTables;
    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel;

    QCOMPARE(listDataDefinitionListModel.rowCount(), 1);

    listDataDefinitionListModel.removeListDataDefinition(0);

    // Always remove from the end
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        // Add three data

        listDataDefinitionListModel.addListDataDefinition();
        listDataDefinitionListModel.addListDataDefinition();
        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);
        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();
            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // First removale of data

        listDataDefinitionListModel.removeListDataDefinition(2);

        QTRY_VERIFY(listOfDataTables.at(2).isNull());

        listOfDataTables.remove(2);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 2);
        QCOMPARE(dataTableModelListModel.rowCount(), 2);

        for (int i = 0; i < 2; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Second removale of data

        listDataDefinitionListModel.removeListDataDefinition(1);

        QTRY_VERIFY(listOfDataTables.at(1).isNull());

        listOfDataTables.remove(1);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 1);
        QCOMPARE(dataTableModelListModel.rowCount(), 1);

        for (int i = 0; i < 1; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Third removale of data

        listDataDefinitionListModel.removeListDataDefinition(0);

        QTRY_VERIFY(listOfDataTables.at(0).isNull());

        listOfDataTables.remove(0);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
        {
            QVERIFY(!dataTableModelPointer.isNull());
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();

    // Remove from the beginning
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        // Add three data

        listDataDefinitionListModel.addListDataDefinition();
        listDataDefinitionListModel.addListDataDefinition();
        listDataDefinitionListModel.addListDataDefinition();

        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);
        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // First removale of data

        listDataDefinitionListModel.removeListDataDefinition(0);

        QTRY_VERIFY(listOfDataTables.at(0).isNull());

        listOfDataTables.remove(0);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 2);
        QCOMPARE(dataTableModelListModel.rowCount(), 2);

        for (int i = 0; i < 2; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Second removale of data

        listDataDefinitionListModel.removeListDataDefinition(0);

        QTRY_VERIFY(listOfDataTables.at(0).isNull());
        listOfDataTables.remove(0);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 1);
        QCOMPARE(dataTableModelListModel.rowCount(), 1);

        for (int i = 0; i < 1; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant)->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Third removale of data

        listDataDefinitionListModel.removeListDataDefinition(0);

        QTRY_VERIFY(listOfDataTables.at(0).isNull());

        listOfDataTables.remove(0);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);
        QCOMPARE(dataTableModelListModel.rowCount(), 0);
    }

    // Remove randomly add
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        // Add six data

        for (int i = 0; i < 6; ++i) { listDataDefinitionListModel.addListDataDefinition(); }

        QCOMPARE(listDataDefinitionListModel.rowCount(), 6);
        QCOMPARE(dataTableModelListModel.rowCount(), 6);

        for (int i = 0; i < 6; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // First removale of data

        listDataDefinitionListModel.removeListDataDefinition(4);

        QTRY_VERIFY(listOfDataTables.at(4).isNull());
        listOfDataTables.remove(4);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 5);
        QCOMPARE(dataTableModelListModel.rowCount(), 5);

        for (int i = 0; i < 5; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant)->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Second removale of data

        listDataDefinitionListModel.removeListDataDefinition(2);

        QTRY_VERIFY(listOfDataTables.at(2).isNull());
        listOfDataTables.remove(2);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 4);
        QCOMPARE(dataTableModelListModel.rowCount(), 4);

        for (int i = 0; i < 4; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant)->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        // Third removale of data

        listDataDefinitionListModel.removeListDataDefinition(1);

        QTRY_VERIFY(listOfDataTables.at(1).isNull());
        listOfDataTables.remove(1);

        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);
        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant)->dataDefinitionModel();

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);
        }

        for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
        {
            QTRY_VERIFY(!dataTableModelPointer.isNull());
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }
}

void Test_DataTableModelListModel::setListDataDefinitionListModel()
{
    QList<QPointer<documentdatadefinitions::controller::DataTablesModel>> listOfDataTables;

    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel;

    QCOMPARE(listDataDefinitionListModel.rowCount(), 1);

    listDataDefinitionListModel.removeListDataDefinition(0);

    // Empty list
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 0);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        QCOMPARE(dataTableModelListModel.rowCount(), 0);
    }

    listDataDefinitionListModel.addListDataDefinition();
    listDataDefinitionListModel.addListDataDefinition();
    listDataDefinitionListModel.addListDataDefinition();

    // List of 3 elements
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 3);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        QCOMPARE(dataTableModelListModel.rowCount(), 3);

        for (int i = 0; i < 3; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

            for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer :
                 listOfDataTables)
            {
                QVERIFY(!dataTableModelPointer.isNull());
            }
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();

    listDataDefinitionListModel.addListDataDefinition();
    listDataDefinitionListModel.addListDataDefinition();
    listDataDefinitionListModel.removeListDataDefinition(3);
    listDataDefinitionListModel.addListDataDefinition();
    listDataDefinitionListModel.addListDataDefinition();

    // List of 6 elements
    {
        QCOMPARE(listDataDefinitionListModel.rowCount(), 6);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
        QCOMPARE(dataTableModelListModel.rowCount(), 0);

        dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

        QCOMPARE(dataTableModelListModel.rowCount(), 6);

        for (int i = 0; i < 6; ++i)
        {
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel =
              listDataDefinitionListModel.at(i)->dataDefinitionListModel();

            QVariant dataVariant = dataTableModelListModel.data(dataTableModelListModel.index(i), Qt::DisplayRole);
            documentdatadefinitions::controller::DataTablesModel* dataTablesModel =
              qvariant_cast<documentdatadefinitions::controller::DataTablesModel*>(dataVariant);
            const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModelFromDataTable =
              dataTablesModel->dataDefinitionModel();

            listOfDataTables.append(dataTablesModel);

            QCOMPARE(dataDefinitionListModel, dataDefinitionListModelFromDataTable);

            for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer :
                 listOfDataTables)
            {
                QVERIFY(!dataTableModelPointer.isNull());
            }
        }
    }

    for (const QPointer<documentdatadefinitions::controller::DataTablesModel>& dataTableModelPointer : listOfDataTables)
    {
        QVERIFY(dataTableModelPointer.isNull());
    }

    listOfDataTables.clear();
}

void Test_DataTableModelListModel::data()
{
    documentdatadefinitions::data::DataTableModelListData testDataTableModelListData =
      datatemplatetestdata::userDataHandlerData().dataTableModelListModel;

    documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;

    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel(
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList);

    dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

    int actuaListSize =
      qMin(dataTableModelListModel.rowCount(), testDataTableModelListData.dataTableModelList.size());

    for (int index = 0; index < actuaListSize; ++index)
    {
        documentdatadefinitions::controller::DataTablesModel* dataTable = dataTableModelListModel.at(index);
        dataTable->setObjectData(testDataTableModelListData.dataTableModelList.at(index));
    }

    documentdatadefinitions::data::DataTableModelListData dataTableModelListData =
      dataTableModelListModel.objectData();

    QCOMPARE(dataTableModelListData.dataTableModelList.size(), actuaListSize);

    for (int index = 0; index < actuaListSize; ++index)
    {
        QCOMPARE(dataTableModelListData.dataTableModelList.at(index),
                 testDataTableModelListData.dataTableModelList.at(index));
    }

    QCOMPARE(dataTableModelListData, testDataTableModelListData);
}

void Test_DataTableModelListModel::setData()
{
    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel(
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList);

    documentdatadefinitions::data::DataTableModelListData dataTableModelListData =
      datatemplatetestdata::userDataHandlerData().dataTableModelListModel;

    //----------------------------------------------------------
    // Test constructor with data
    //----------------------------------------------------------
    documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel(&listDataDefinitionListModel,
                                                                             dataTableModelListData);

    QSignalSpy modelAboutToBeResetSpy(&dataTableModelListModel,
                                      &documentdatadefinitions::controller::DataTableModelListModel::modelAboutToBeReset);
    QSignalSpy modelResetSpy(&dataTableModelListModel,
                             &documentdatadefinitions::controller::DataTableModelListModel::modelReset);
    QSignalSpy dataChangedSpy(&dataTableModelListModel,
                              &documentdatadefinitions::controller::DataTableModelListModel::dataChanged);
    QSignalSpy rowsAboutToBeInsertedSpy(&dataTableModelListModel,
                                        &documentdatadefinitions::controller::DataTableModelListModel::rowsAboutToBeInserted);
    QSignalSpy rowsInsertedSpy(&dataTableModelListModel,
                               &documentdatadefinitions::controller::DataTableModelListModel::rowsInserted);
    QSignalSpy rowsAboutToBeRemovedSpy(&dataTableModelListModel,
                                       &documentdatadefinitions::controller::DataTableModelListModel::rowsAboutToBeRemoved);
    QSignalSpy rowsRemovedSpy(&dataTableModelListModel,
                              &documentdatadefinitions::controller::DataTableModelListModel::rowsRemoved);

    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(dataChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    int actuaListSize =
      qMin(listDataDefinitionListModel.rowCount(), dataTableModelListData.dataTableModelList.size());

    QCOMPARE(dataTableModelListModel.rowCount(), actuaListSize);

    for (int index = 0; index < actuaListSize; ++index)
    {
        documentdatadefinitions::data::DataTablesData dataTablesData =
          dataTableModelListModel.at(index)->objectData();
        QCOMPARE(dataTablesData, dataTableModelListData.dataTableModelList.at(index));
    }

    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(dataChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------

    for (documentdatadefinitions::data::DataTablesData& dataTableData :
         dataTableModelListData.dataTableModelList)
    {
        QList<documentdatadefinitions::data::DataTableData> tmpDataTable(dataTableData.dataTables);
        dataTableData.dataTables = { tmpDataTable.rbegin(), tmpDataTable.rend() };
    }

    dataTableModelListModel.setObjectData(dataTableModelListData);

    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(dataChangedSpy.size(), 1);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    QVariantList dataChangedSpyArguments = dataChangedSpy.at(0);
    QCOMPARE(dataChangedSpyArguments.size(), 3);
    QCOMPARE(qvariant_cast<QModelIndex>(dataChangedSpyArguments.at(0)), dataTableModelListModel.index(0));
    QCOMPARE(qvariant_cast<QModelIndex>(dataChangedSpyArguments.at(1)),
             dataTableModelListModel.index(dataTableModelListModel.rowCount()));
    QCOMPARE(qvariant_cast<QList<int>>(dataChangedSpyArguments.at(2)), QList<int> { Qt::DisplayRole });

    actuaListSize = qMin(listDataDefinitionListModel.rowCount(), dataTableModelListData.dataTableModelList.size());

    QCOMPARE(dataTableModelListModel.rowCount(), actuaListSize);

    for (int index = 0; index < actuaListSize; ++index)
    {
        documentdatadefinitions::data::DataTablesData dataTablesData =
          dataTableModelListModel.at(index)->objectData();
        QCOMPARE(dataTablesData, dataTableModelListData.dataTableModelList.at(index));
    }

    QCOMPARE(dataTableModelListModel.objectData(), dataTableModelListData);

    dataTableModelListModel.setObjectData(dataTableModelListData);

    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(dataChangedSpy.size(), 1);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    QCOMPARE(dataTableModelListModel.objectData(), dataTableModelListData);
}

void Test_DataTableModelListModel::setAndDeleteListDataDefinitionListModel()
{
    // Default constructor and set data
    {
        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;

        {
            documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel(
              datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList);

            dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

            dataTableModelListModel.setObjectData(
              datatemplatetestdata::userDataHandlerData().dataTableModelListModel);

            QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), &listDataDefinitionListModel);

            QVERIFY(dataTableModelListModel.rowCount() != 0);
        }

        QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), nullptr);

        QCOMPARE(dataTableModelListModel.rowCount(), 0);
    }

    // Constructor setting the value of data definitions
    {
        documentdatadefinitions::controller::ListDataDefinitionListModel* listDataDefinitionListModel =
          new documentdatadefinitions::controller::ListDataDefinitionListModel(
            datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList, this);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel { listDataDefinitionListModel };

        dataTableModelListModel.setObjectData(datatemplatetestdata::userDataHandlerData().dataTableModelListModel);

        QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), listDataDefinitionListModel);
        QVERIFY(dataTableModelListModel.rowCount() != 0);

        delete listDataDefinitionListModel;

        QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), nullptr);

        QCOMPARE(dataTableModelListModel.rowCount(), 0);
    }

    // Constructor setting the value of data definitions and the table data
    {
        documentdatadefinitions::controller::ListDataDefinitionListModel* listDataDefinitionListModel =
          new documentdatadefinitions::controller::ListDataDefinitionListModel(
            datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList, this);

        documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel {
            listDataDefinitionListModel, datatemplatetestdata::userDataHandlerData().dataTableModelListModel
        };

        QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), listDataDefinitionListModel);
        QVERIFY(dataTableModelListModel.rowCount() != 0);

        delete listDataDefinitionListModel;

        QCOMPARE(dataTableModelListModel.listDataDefinitionListModel(), nullptr);

        QCOMPARE(dataTableModelListModel.rowCount(), 0);
    }
}

void Test_DataTableModelListModel::onListDataDefinitionModelReset()
{
    documentdatadefinitions::controller::ListDataDefinitionListModel listDataDefinitionListModel;

    documentdatadefinitions::controller::DataTableModelListModel dataTableModelListModel;
    dataTableModelListModel.setListDataDefinitionListModel(&listDataDefinitionListModel);

    QSignalSpy modelAboutToBeResetSpy(&dataTableModelListModel,
                                      &documentdatadefinitions::controller::DataTableModelListModel::modelAboutToBeReset);
    QSignalSpy modelResetSpy(&dataTableModelListModel,
                             &documentdatadefinitions::controller::DataTableModelListModel::modelReset);
    QSignalSpy dataChangedSpy(&dataTableModelListModel,
                              &documentdatadefinitions::controller::DataTableModelListModel::dataChanged);
    QSignalSpy rowsAboutToBeInsertedSpy(&dataTableModelListModel,
                                        &documentdatadefinitions::controller::DataTableModelListModel::rowsAboutToBeInserted);
    QSignalSpy rowsInsertedSpy(&dataTableModelListModel,
                               &documentdatadefinitions::controller::DataTableModelListModel::rowsInserted);
    QSignalSpy rowsAboutToBeRemovedSpy(&dataTableModelListModel,
                                       &documentdatadefinitions::controller::DataTableModelListModel::rowsAboutToBeRemoved);
    QSignalSpy rowsRemovedSpy(&dataTableModelListModel,
                              &documentdatadefinitions::controller::DataTableModelListModel::rowsRemoved);

    QCOMPARE(dataTableModelListModel.rowCount(), 1);

    QCOMPARE(modelAboutToBeResetSpy.size(), 0);
    QCOMPARE(modelResetSpy.size(), 0);
    QCOMPARE(dataChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList;
    listDataDefinitionListModel.setObjectData(listDataDefinitionListData);

    QCOMPARE(dataTableModelListModel.rowCount(), listDataDefinitionListData.listOfDataDefinition.size());

    QCOMPARE(modelAboutToBeResetSpy.size(), 1);
    QCOMPARE(modelResetSpy.size(), 1);
    QCOMPARE(dataChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);

    listDataDefinitionListData.listOfDataDefinition.remove(0);

    listDataDefinitionListModel.setObjectData(listDataDefinitionListData);

    QCOMPARE(dataTableModelListModel.rowCount(), listDataDefinitionListData.listOfDataDefinition.size());

    QCOMPARE(modelAboutToBeResetSpy.size(), 2);
    QCOMPARE(modelResetSpy.size(), 2);
    QCOMPARE(dataChangedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
    QCOMPARE(rowsInsertedSpy.size(), 0);
    QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
    QCOMPARE(rowsRemovedSpy.size(), 0);
}

QTEST_GUILESS_MAIN(Test_DataTableModelListModel)

#include "Test_DataTableModelListModel.moc"
