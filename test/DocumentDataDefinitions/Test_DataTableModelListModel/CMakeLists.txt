cmake_minimum_required(VERSION 3.20)

print_location()

enable_testing()

set(TEST_NAME Test_DataTableModelListModel)

add_executable(${TEST_NAME} Test_DataTableModelListModel.cpp)

add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME})

target_link_libraries(${TEST_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Test Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Qml)
target_link_libraries(${TEST_NAME} PUBLIC DocumentDataDefinitions)
