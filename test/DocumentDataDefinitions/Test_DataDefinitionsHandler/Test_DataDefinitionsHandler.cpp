#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"

class Test_DataDefinitionsHandler : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
};

void Test_DataDefinitionsHandler::data()
{
    documentdatadefinitions::controller::DataDefinitionsHandler dataDefinitionHandler;

    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerDataTest =
      datatemplatetestdata::dataDefinitionHandlerTestData();

    //----------------------------------------------------------------------------
    // Prepare the data of the list data definition list
    //----------------------------------------------------------------------------

    dataDefinitionHandler.generalVariableList()->setObjectData(dataDefinitionsHandlerDataTest.generalVariableList);
    dataDefinitionHandler.listDataDefinitionListModel()->setObjectData(
      dataDefinitionsHandlerDataTest.listDataDefinitionList);

    //----------------------------------------------------------------------------
    // Check the data
    //----------------------------------------------------------------------------
    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData(
      dataDefinitionHandler.objectData());

    QCOMPARE(dataDefinitionsHandlerData.generalVariableList, dataDefinitionsHandlerDataTest.generalVariableList);
    QCOMPARE(dataDefinitionsHandlerData.listDataDefinitionList,
             dataDefinitionsHandlerDataTest.listDataDefinitionList);
}

void Test_DataDefinitionsHandler::setData()
{
    QPointer<documentdatadefinitions::controller::DataDefinitionListModel> generalVariableList;
    QPointer<documentdatadefinitions::controller::ListDataDefinitionListModel> listDataDefinitionListModel;

    {
        documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerDataTest =
          datatemplatetestdata::dataDefinitionHandlerTestData();

        //----------------------------------------------------------
        // Test constructor with data
        //----------------------------------------------------------
        documentdatadefinitions::controller::DataDefinitionsHandler dataDefinitionsHandler(dataDefinitionsHandlerDataTest);

        generalVariableList         = dataDefinitionsHandler.generalVariableList();
        listDataDefinitionListModel = dataDefinitionsHandler.listDataDefinitionListModel();

        QCOMPARE(dataDefinitionsHandler.generalVariableList()->objectData(),
                 dataDefinitionsHandlerDataTest.generalVariableList);
        QCOMPARE(dataDefinitionsHandler.listDataDefinitionListModel()->objectData(),
                 dataDefinitionsHandlerDataTest.listDataDefinitionList);

        //----------------------------------------------------------
        // Test set data function
        //----------------------------------------------------------

        dataDefinitionsHandlerDataTest.generalVariableList.listName = "The list name 2";

        for (qsizetype index = 0;
             index < dataDefinitionsHandlerDataTest.generalVariableList.dataDefinitions.size();
             ++index)
        {
            dataDefinitionsHandlerDataTest.generalVariableList.dataDefinitions[index].description =
              QString("Another description ") + QString::number(index);
        }

        for (qsizetype listIndex = 0;
             listIndex < dataDefinitionsHandlerDataTest.listDataDefinitionList.listOfDataDefinition.size();
             ++listIndex)
        {
            documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData =
              dataDefinitionsHandlerDataTest.listDataDefinitionList.listOfDataDefinition[listIndex]
                .dataDefinitionListData;

            dataDefinitionListData.listName = QString("AnotherName ") + QString::number(listIndex);

            for (qsizetype dataIndex = 0; dataIndex < dataDefinitionListData.dataDefinitions.size(); ++dataIndex)
            {
                dataDefinitionListData.dataDefinitions[dataIndex].name =
                  QString("Another name") + QString::number(dataIndex);
            }
        }

        dataDefinitionsHandler.setObjetData(dataDefinitionsHandlerDataTest);

        QCOMPARE(dataDefinitionsHandler.generalVariableList()->objectData(),
                 dataDefinitionsHandlerDataTest.generalVariableList);
        QCOMPARE(dataDefinitionsHandler.listDataDefinitionListModel()->objectData(),
                 dataDefinitionsHandlerDataTest.listDataDefinitionList);

        QVERIFY(!generalVariableList.isNull());
        QVERIFY(!listDataDefinitionListModel.isNull());
    }

    QVERIFY(generalVariableList.isNull());
    QVERIFY(listDataDefinitionListModel.isNull());
}

QTEST_APPLESS_MAIN(Test_DataDefinitionsHandler)

#include "Test_DataDefinitionsHandler.moc"
