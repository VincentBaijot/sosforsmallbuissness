#include <QtTest/QTest>

#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/data/DataDefinitionData.hpp"
#include "DocumentDataDefinitions/data/DataDefinitionListData.hpp"
#include "DocumentDataDefinitions/data/DataDefinitionsHandlerData.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentDataDefinitions/data/DataTablesData.hpp"
#include "DocumentDataDefinitions/data/ListDataDefinitionData.hpp"
#include "DocumentDataDefinitions/data/ListDataDefinitionListData.hpp"
#include "DocumentDataDefinitions/data/UserDataHandlerData.hpp"
#include "DocumentDataDefinitions/services/DocumentDataDefinitionsXmlConstants.hpp"
#include "DocumentDataDefinitions/services/DocumentDataDefinitionsXmlSerialization.hpp"

class Test_DocumentDataDefinitionsXmlSerialization : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void test_readDataDefinitionData_data();
    void test_readDataDefinitionData();

    void test_writeDataDefinitionData();

    void test_readDataDefinitionListData_data();
    void test_readDataDefinitionListData();

    void test_writeDataDefinitionListData();

    void test_readDataTablesData_data();
    void test_readDataTablesData();

    void test_writeDataTablesData();

    void test_readListDataDefinitionData_data();
    void test_readListDataDefinitionData();

    void test_writeListDataDefinitionData();

    void test_readListDataDefinitionListData_data();
    void test_readListDataDefinitionListData();

    void test_writeListDataDefinitionListData();

    void test_readDataDefinitionsHandlerData_data();
    void test_readDataDefinitionsHandlerData();

    void test_writeDataDefinitionsHandlerData();

    void test_readUserDataHandlerData_data();
    void test_readUserDataHandlerData();

    void test_writeUserDataHandlerData();

    void test_readDataHandlerData_data();
    void test_readDataHandlerData();

    void test_writeDataHandlerData();

  private:
    static QByteArray writeDataDefinitionListDataXml(
      const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData);
    static QByteArray writeDataTablesDataXml(
      const documentdatadefinitions::data::DataTablesData& dataTablesData);
    static QByteArray writeListDataDefinitionDataXml(
      const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData);
    static QByteArray writeListDataDefinitionListDataXml(
      const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData);
    static QByteArray writeDataDefinitionsHandlerDataXml(
      const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData);
    static QByteArray writeUserDataHandlerDataXml(
      const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData);
};

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::DataDefinitionData>>(
      "expectedDataDefinitionData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinition>\n"
                                                 "    <Key>key</Key>\n"
                                                 "    <Name>name</Name>\n"
                                                 "    <Description>description</Description>\n"
                                                 "    <DataType>1</DataType>\n"
                                                 "</DataDefinition>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          documentdatadefinitions::data::DataDefinitionData {
              "key",
              "name",
              "description",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType
          };
        QTest::newRow("Same_order_than_write") << arrayData << expectedDataDefinitionData;
    }

    // Other other than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinition>\n"
                                                 "    <DataType>1</DataType>\n"
                                                 "    <Name>name</Name>\n"
                                                 "    <Key>key</Key>\n"
                                                 "    <Description>description</Description>\n"
                                                 "</DataDefinition>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          documentdatadefinitions::data::DataDefinitionData {
              "key",
              "name",
              "description",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType
          };
        QTest::newRow("Other_other_than_write") << arrayData << expectedDataDefinitionData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinition>\n"
                                                 "    <Key/>\n"
                                                 "    <Name/>\n"
                                                 "    <Description/>\n"
                                                 "    <DataType/>\n"
                                                 "</DataDefinition>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          documentdatadefinitions::data::DataDefinitionData {};
        QTest::newRow("Self_closing_parts") << arrayData << expectedDataDefinitionData;
    }

    // Empty DataDefinition
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinition>\n"
                                                 "</DataDefinition>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          documentdatadefinitions::data::DataDefinitionData {};
        QTest::newRow("Empty_DataDefinition") << arrayData << expectedDataDefinitionData;
    }

    // Empty self closing DataDefinition
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinition/>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          documentdatadefinitions::data::DataDefinitionData {};
        QTest::newRow("Empty_self_closing_DataDefinition") << arrayData << expectedDataDefinitionData;
    }

    // Not DataDefinition
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          std::nullopt;
        QTest::newRow("Not_DataDefinition") << arrayData << expectedDataDefinitionData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml");
        std::optional<documentdatadefinitions::data::DataDefinitionData> expectedDataDefinitionData =
          std::nullopt;
        QTest::newRow("Not_xml") << arrayData << expectedDataDefinitionData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::DataDefinitionData>, expectedDataDefinitionData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::DataDefinitionData> actualDataDefinitionData =
      documentdatadefinitions::service::readDataDefinitionData(streamReader);

    QCOMPARE(actualDataDefinitionData, expectedDataDefinitionData);

    if (expectedDataDefinitionData.has_value())
    {
        QCOMPARE(actualDataDefinitionData.value(), expectedDataDefinitionData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::dataDefinitionElement);
    }
    else { QVERIFY(!actualDataDefinitionData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeDataDefinitionData()
{
    documentdatadefinitions::data::DataDefinitionData dataDefinitionData =
      documentdatadefinitions::data::DataDefinitionData {
          "key",
          "name",
          "description",
          documentdatadefinitions::data::DataDefinitionData::DataType::NumberType
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeDataDefinitionData(streamWriter, dataDefinitionData);

    streamWriter.writeEndDocument();

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
             "<DataDefinition>\n"
             "    <Key>key</Key>\n"
             "    <Name>name</Name>\n"
             "    <Description>description</Description>\n"
             "    <DataType>1</DataType>\n"
             "</DataDefinition>\n");
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionListData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::DataDefinitionListData>>(
      "expectedDataDefinitionListData");

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinitionList>\n"
                                                 "    <Name>ListName</Name>\n"
                                                 "    <DataDefinition>\n"
                                                 "        <Key>key</Key>\n"
                                                 "        <Name>name</Name>\n"
                                                 "        <Description>description</Description>\n"
                                                 "        <DataType>1</DataType>\n"
                                                 "    </DataDefinition>\n"
                                                 "    <DataDefinition>\n"
                                                 "        <Key>key2</Key>\n"
                                                 "        <Name>name2</Name>\n"
                                                 "        <Description>description2</Description>\n"
                                                 "        <DataType>1</DataType>\n"
                                                 "    </DataDefinition>\n"
                                                 "    <DataDefinition/>\n"
                                                 "</DataDefinitionList>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = documentdatadefinitions::data::DataDefinitionListData {
              "ListName",
              QList<documentdatadefinitions::data::DataDefinitionData> {
                documentdatadefinitions::data::DataDefinitionData {
                  "key",
                  "name",
                  "description",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
                documentdatadefinitions::data::DataDefinitionData {
                  "key2",
                  "name2",
                  "description2",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
                documentdatadefinitions::data::DataDefinitionData {} }

          };
        QTest::newRow("Same_order_than_write") << arrayData << expectedDataDefinitionListData;
    }

    // Other order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinitionList>\n"
                                                 "    <DataDefinition>\n"
                                                 "        <Key>key</Key>\n"
                                                 "        <Name>name</Name>\n"
                                                 "        <Description>description</Description>\n"
                                                 "        <DataType>1</DataType>\n"
                                                 "    </DataDefinition>\n"
                                                 "    <DataDefinition>\n"
                                                 "        <Key>key2</Key>\n"
                                                 "        <Name>name2</Name>\n"
                                                 "        <Description>description2</Description>\n"
                                                 "        <DataType>1</DataType>\n"
                                                 "    </DataDefinition>\n"
                                                 "    <DataDefinition/>\n"
                                                 "    <Name>ListName</Name>\n"
                                                 "</DataDefinitionList>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = documentdatadefinitions::data::DataDefinitionListData {
              "ListName",
              QList<documentdatadefinitions::data::DataDefinitionData> {
                documentdatadefinitions::data::DataDefinitionData {
                  "key",
                  "name",
                  "description",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
                documentdatadefinitions::data::DataDefinitionData {
                  "key2",
                  "name2",
                  "description2",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
                documentdatadefinitions::data::DataDefinitionData {} }

          };
        QTest::newRow("Other_order_than_write") << arrayData << expectedDataDefinitionListData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinitionList>\n"
                                                 "    <Name/>\n"
                                                 "</DataDefinitionList>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = documentdatadefinitions::data::DataDefinitionListData {};
        QTest::newRow("Self_closing_parts") << arrayData << expectedDataDefinitionListData;
    }

    // Empty DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinitionList>\n"
                                                 "</DataDefinitionList>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = documentdatadefinitions::data::DataDefinitionListData {};
        QTest::newRow("Empty_DataDefinitionList") << arrayData << expectedDataDefinitionListData;
    }

    // Empty self closing DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataDefinitionList/>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = documentdatadefinitions::data::DataDefinitionListData {};
        QTest::newRow("Empty_self_closing_DataDefinitionList") << arrayData << expectedDataDefinitionListData;
    }

    // Not DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = std::nullopt;
        QTest::newRow("Not_DataDefinitionList") << arrayData << expectedDataDefinitionListData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::DataDefinitionListData>
          expectedDataDefinitionListData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDataDefinitionListData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionListData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::DataDefinitionListData>,
           expectedDataDefinitionListData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::DataDefinitionListData> actualDataDefinitionListData =
      documentdatadefinitions::service::readDataDefinitionListData(streamReader);

    if (expectedDataDefinitionListData.has_value())
    {
        QCOMPARE(actualDataDefinitionListData.value(), expectedDataDefinitionListData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::dataDefinitionListElement);
    }
    else { QVERIFY(!actualDataDefinitionListData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeDataDefinitionListData()
{
    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData =
      documentdatadefinitions::data::DataDefinitionListData {
          "ListName",
          QList<documentdatadefinitions::data::DataDefinitionData> {
            documentdatadefinitions::data::DataDefinitionData {
              "key",
              "name",
              "description",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
            documentdatadefinitions::data::DataDefinitionData {
              "key2",
              "name2",
              "description2",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } }

      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeDataDefinitionListData(streamWriter, dataDefinitionListData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QCOMPARE(arrayData,
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
             "<DataDefinitionList>\n"
             "    <Name>ListName</Name>\n"
             "    <DataDefinition>\n"
             "        <Key>key</Key>\n"
             "        <Name>name</Name>\n"
             "        <Description>description</Description>\n"
             "        <DataType>1</DataType>\n"
             "    </DataDefinition>\n"
             "    <DataDefinition>\n"
             "        <Key>key2</Key>\n"
             "        <Name>name2</Name>\n"
             "        <Description>description2</Description>\n"
             "        <DataType>1</DataType>\n"
             "    </DataDefinition>\n"
             "</DataDefinitionList>\n");
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataTablesData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::DataTablesData>>(
      "expectedDataTablesData");

    // Same order than write
    {
        QByteArray arrayData =
          QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<DataTablesData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Value2</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Patate</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Value</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue2</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue1</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n "
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Third</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData/>\n"
                            "</DataTablesData>\n");

        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {
              QList<documentdatadefinitions::data::DataTableData> {
                documentdatadefinitions::data::DataTableData {
                  { 0, "Patate" },
                  { 1, "Value" },
                  { 20, "Value2" },
                },
                documentdatadefinitions::data::DataTableData {
                  { 0, "AnotherValue" },
                  { 1, "AnotherValue1" },
                  { 20, "AnotherValue2" },
                },
                documentdatadefinitions::data::DataTableData { { 0, "Third" } },
                documentdatadefinitions::data::DataTableData {} }
          };
        QTest::newRow("Same_order_than_write") << arrayData << expectedDataTablesData;
    }

    // Other order than write
    {
        QByteArray arrayData =
          QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<DataTablesData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionValue>Value2</DataDefinitionValue>\n"
                            "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionValue>Patate</DataDefinitionValue>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Value</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue2</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionValue>AnotherValue1</DataDefinitionValue>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n "
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Third</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData/>\n"
                            "</DataTablesData>\n");

        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {
              QList<documentdatadefinitions::data::DataTableData> {
                documentdatadefinitions::data::DataTableData {
                  { 0, "Patate" },
                  { 1, "Value" },
                  { 20, "Value2" },
                },
                documentdatadefinitions::data::DataTableData {
                  { 0, "AnotherValue" },
                  { 1, "AnotherValue1" },
                  { 20, "AnotherValue2" },
                },
                documentdatadefinitions::data::DataTableData { { 0, "Third" } },
                documentdatadefinitions::data::DataTableData {} }
          };
        QTest::newRow("Other_order_than_write") << arrayData << expectedDataTablesData;
    }

    // Some missing values
    {
        QByteArray arrayData =
          QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                            "<DataTablesData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Value2</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Patate</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionValue>AnotherValue2</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "        <DataTableEntry>\n"
                            "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>AnotherValue1</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData>\n"
                            "        <DataTableEntry>\n "
                            "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                            "            <DataDefinitionValue>Third</DataDefinitionValue>\n"
                            "        </DataTableEntry>\n"
                            "    </DataTableData>\n"
                            "    <DataTableData/>\n"
                            "</DataTablesData>\n");

        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {
              QList<documentdatadefinitions::data::DataTableData> {
                documentdatadefinitions::data::DataTableData {
                  { 0, "Patate" },
                  { 20, "Value2" },
                },
                documentdatadefinitions::data::DataTableData { { 0, "AnotherValue" },
                                                                      { 1, "AnotherValue1" } },
                documentdatadefinitions::data::DataTableData { { 0, "Third" } },
                documentdatadefinitions::data::DataTableData {} }
          };
        QTest::newRow("Some_missing_values") << arrayData << expectedDataTablesData;
    }

    // Self closing parts
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTablesData>\n"
                                                 "    <DataTableData/>\n"
                                                 "    <DataTableData/>\n"
                                                 "</DataTablesData>\n");

        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {
              QList<documentdatadefinitions::data::DataTableData> {
                documentdatadefinitions::data::DataTableData {},
                documentdatadefinitions::data::DataTableData {} }
          };
        QTest::newRow("Self_closing_parts") << arrayData << expectedDataTablesData;
    }

    // Empty DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTablesData>\n"
                                                 "</DataTablesData>\n");
        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {};
        QTest::newRow("Empty_DataDefinitionList") << arrayData << expectedDataTablesData;
    }

    // Empty self closing DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTablesData/>\n");
        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData =
          documentdatadefinitions::data::DataTablesData {};
        QTest::newRow("Empty_self_closing_DataDefinitionList") << arrayData << expectedDataTablesData;
    }

    // Not DataDefinitionList
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData = std::nullopt;
        QTest::newRow("Not_DataDefinitionList") << arrayData << expectedDataTablesData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::DataTablesData> expectedDataTablesData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDataTablesData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataTablesData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::DataTablesData>, expectedDataTablesData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::DataTablesData> actualDataTablesData =
      documentdatadefinitions::service::readDataTablesData(streamReader);

    if (expectedDataTablesData.has_value())
    {
        QCOMPARE(actualDataTablesData.value(), expectedDataTablesData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::dataTablesElement);
    }
    else { QVERIFY(!actualDataTablesData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeDataTablesData()
{
    documentdatadefinitions::data::DataTablesData dataTablesData =
      documentdatadefinitions::data::DataTablesData {
          QList<documentdatadefinitions::data::DataTableData> {
            documentdatadefinitions::data::DataTableData {
              { 0, "Patate" },
              { 1, "Value" },
              { 20, "Value2" },
            },
            documentdatadefinitions::data::DataTableData {
              { 0, "AnotherValue" },
              { 1, "AnotherValue1" },
              { 20, "AnotherValue2" },
            },
            documentdatadefinitions::data::DataTableData { { 0, "Third" } },
            documentdatadefinitions::data::DataTableData {} }
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeDataTablesData(streamWriter, dataTablesData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    // Order of elements in the hash is random so we need to check without consideration of the exact order of
    // element in <DataTableData>

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<DataTablesData>\n"
                                                 "    <DataTableData>\n");
    QVERIFY(arrayData.startsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>Value2</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>Patate</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>Value</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    qDebug() << arrayData;
    expectedArray = QByteArrayLiteral("    </DataTableData>\n"
                                      "    <DataTableData>\n");
    QVERIFY(arrayData.startsWith(expectedArray));
    arrayData.remove(0, expectedArray.size());

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>20</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>AnotherValue2</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>AnotherValue</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>1</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>AnotherValue1</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n");
    QVERIFY(arrayData.contains(expectedArray));
    arrayData.replace(expectedArray, "");

    expectedArray = QByteArrayLiteral("    </DataTableData>\n"
                                      "    <DataTableData>\n"
                                      "        <DataTableEntry>\n"
                                      "            <DataDefinitionIndex>0</DataDefinitionIndex>\n"
                                      "            <DataDefinitionValue>Third</DataDefinitionValue>\n"
                                      "        </DataTableEntry>\n"
                                      "    </DataTableData>\n"
                                      "    <DataTableData/>\n"
                                      "</DataTablesData>\n");
    QVERIFY(arrayData.endsWith(expectedArray));
    arrayData.replace(expectedArray, "");

    QCOMPARE(arrayData, "");
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readListDataDefinitionData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::ListDataDefinitionData>>(
      "expectedListDataDefinitionData");

    std::optional<documentdatadefinitions::data::ListDataDefinitionData>
      expectedCompleteListDataDefinitionData;

    {
        expectedCompleteListDataDefinitionData = documentdatadefinitions::data::ListDataDefinitionData {
            documentdatadefinitions::data::DataDefinitionListData {
              "ListName",
              QList<documentdatadefinitions::data::DataDefinitionData> {
                documentdatadefinitions::data::DataDefinitionData {
                  "key",
                  "name",
                  "description",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
                documentdatadefinitions::data::DataDefinitionData {
                  "key2",
                  "name2",
                  "description2",
                  documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } }

            },
            documentdatadefinitions::data::DataTablesData {
              QList<documentdatadefinitions::data::DataTableData> {
                documentdatadefinitions::data::DataTableData {
                  { 0, "Patate" },
                  { 1, "Value" },
                  { 20, "Value2" },
                },
                documentdatadefinitions::data::DataTableData {
                  { 0, "AnotherValue" },
                  { 1, "AnotherValue1" },
                  { 20, "AnotherValue2" },
                },
                documentdatadefinitions::data::DataTableData { { 0, "Third" } },
                documentdatadefinitions::data::DataTableData {} } }
        };
    }

    // Same order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition>");

        arrayData.append(
          writeDataDefinitionListDataXml(expectedCompleteListDataDefinitionData.value().dataDefinitionListData));
        arrayData.append(
          writeDataTablesDataXml(expectedCompleteListDataDefinitionData.value().demonstrationTable));

        arrayData.append(QByteArrayLiteral("</ListDataDefinition>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedCompleteListDataDefinitionData;
    }

    // Other order than write
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition>");

        arrayData.append(
          writeDataTablesDataXml(expectedCompleteListDataDefinitionData.value().demonstrationTable));
        arrayData.append(
          writeDataDefinitionListDataXml(expectedCompleteListDataDefinitionData.value().dataDefinitionListData));

        arrayData.append(QByteArrayLiteral("</ListDataDefinition>\n"));

        QTest::newRow("Other_order_than_write") << arrayData << expectedCompleteListDataDefinitionData;
    }

    // Self closing parts
    {
        std::optional<documentdatadefinitions::data::ListDataDefinitionData>
          expectedCompleteListDataDefinitionData = documentdatadefinitions::data::ListDataDefinitionData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition>");

        arrayData.append(
          writeDataDefinitionListDataXml(expectedCompleteListDataDefinitionData.value().dataDefinitionListData));
        arrayData.append(
          writeDataTablesDataXml(expectedCompleteListDataDefinitionData.value().demonstrationTable));

        arrayData.append(QByteArrayLiteral("</ListDataDefinition>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedCompleteListDataDefinitionData;
    }

    // Empty ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition>"
                                                 "</ListDataDefinition>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionData>
          expectedListDataDefinitionData = documentdatadefinitions::data::ListDataDefinitionData {};
        QTest::newRow("Empty_ListDataDefinitionData") << arrayData << expectedListDataDefinitionData;
    }

    // Empty self closing ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition/>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionData>
          expectedListDataDefinitionData = documentdatadefinitions::data::ListDataDefinitionData {};
        QTest::newRow("Empty_self_closing_ListDataDefinitionData") << arrayData << expectedListDataDefinitionData;
    }

    // Not ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionData>
          expectedListDataDefinitionData = std::nullopt;
        QTest::newRow("Not_ListDataDefinitionData") << arrayData << expectedListDataDefinitionData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::ListDataDefinitionData>
          expectedListDataDefinitionData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedListDataDefinitionData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readListDataDefinitionData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::ListDataDefinitionData>,
           expectedListDataDefinitionData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::ListDataDefinitionData> actualListDataDefinitionData =
      documentdatadefinitions::service::readListDataDefinitionData(streamReader);

    if (expectedListDataDefinitionData.has_value())
    {
        QCOMPARE(actualListDataDefinitionData.value(), expectedListDataDefinitionData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::listDataDefinitionElement);
    }
    else { QVERIFY(!actualListDataDefinitionData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeListDataDefinitionData()
{
    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData =
      documentdatadefinitions::data::ListDataDefinitionData {
          documentdatadefinitions::data::DataDefinitionListData {
            "ListName",
            QList<documentdatadefinitions::data::DataDefinitionData> {
              documentdatadefinitions::data::DataDefinitionData {
                "key",
                "name",
                "description",
                documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
              documentdatadefinitions::data::DataDefinitionData {
                "key2",
                "name2",
                "description2",
                documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } }

          },
          documentdatadefinitions::data::DataTablesData {
            QList<documentdatadefinitions::data::DataTableData> {
              documentdatadefinitions::data::DataTableData {
                { 0, "Patate" },
                { 1, "Value" },
                { 20, "Value2" },
              },
              documentdatadefinitions::data::DataTableData {
                { 0, "AnotherValue" },
                { 1, "AnotherValue1" },
                { 20, "AnotherValue2" },
              },
              documentdatadefinitions::data::DataTableData { { 0, "Third" } },
              documentdatadefinitions::data::DataTableData {} } }
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeListDataDefinitionData(streamWriter, listDataDefinitionData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinition>");

    expectedArray.append(writeDataDefinitionListDataXml(listDataDefinitionData.dataDefinitionListData));
    expectedArray.append(writeDataTablesDataXml(listDataDefinitionData.demonstrationTable));

    expectedArray.append(QByteArrayLiteral("</ListDataDefinition>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readListDataDefinitionListData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::ListDataDefinitionListData>>(
      "expectedListDataDefinitionListData");

    // Complete data
    {
        std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
          expectedListDataDefinitionListData =
            datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList;

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinitionList>");

        for (const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData :
             expectedListDataDefinitionListData.value().listOfDataDefinition)
        {
            arrayData.append(writeListDataDefinitionDataXml(listDataDefinitionData));
        }

        arrayData.append(QByteArrayLiteral("</ListDataDefinitionList>\n"));

        qDebug() << arrayData;

        QTest::newRow("Complete_data") << arrayData << expectedListDataDefinitionListData;
    }

    // Empty ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinitionList>"
                                                 "</ListDataDefinitionList>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
          expectedListDataDefinitionListData = documentdatadefinitions::data::ListDataDefinitionListData {};
        QTest::newRow("Empty_ListDataDefinitionData") << arrayData << expectedListDataDefinitionListData;
    }

    // Empty self closing ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinitionList/>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
          expectedListDataDefinitionListData = documentdatadefinitions::data::ListDataDefinitionListData {};
        QTest::newRow("Empty_self_closing_ListDataDefinitionData")
          << arrayData << expectedListDataDefinitionListData;
    }

    // Not ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
          expectedListDataDefinitionListData = std::nullopt;
        QTest::newRow("Not_ListDataDefinitionData") << arrayData << expectedListDataDefinitionListData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
          expectedListDataDefinitionListData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedListDataDefinitionListData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readListDataDefinitionListData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::ListDataDefinitionListData>,
           expectedListDataDefinitionListData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
      actualListDataDefinitionListData =
        documentdatadefinitions::service::readListDataDefinitionListData(streamReader);

    if (expectedListDataDefinitionListData.has_value())
    {
        QCOMPARE(actualListDataDefinitionListData.value(), expectedListDataDefinitionListData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::listDataDefinitionListElement);
    }
    else { QVERIFY(!actualListDataDefinitionListData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeListDataDefinitionListData()
{
    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData =
      datatemplatetestdata::dataDefinitionHandlerTestData().listDataDefinitionList;

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeListDataDefinitionListData(streamWriter, listDataDefinitionListData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<ListDataDefinitionList>");

    for (const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData :
         listDataDefinitionListData.listOfDataDefinition)
    {
        expectedArray.append(writeListDataDefinitionDataXml(listDataDefinitionData));
    }

    expectedArray.append(QByteArrayLiteral("</ListDataDefinitionList>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionsHandlerData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>>(
      "expectedDataDefinitionsHandlerData");

    // Same order than write
    {
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = datatemplatetestdata::dataDefinitionHandlerTestData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler>");

        arrayData.append(
          writeDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().generalVariableList));
        arrayData.append(
          writeListDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().listDataDefinitionList));

        arrayData.append(QByteArrayLiteral("</DataDefinitionsHandler>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedDataDefinitionsHandler;
    }

    // Other order than write
    {
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = datatemplatetestdata::dataDefinitionHandlerTestData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler>");

        arrayData.append(
          writeListDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().listDataDefinitionList));
        arrayData.append(
          writeDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().generalVariableList));

        arrayData.append(QByteArrayLiteral("</DataDefinitionsHandler>\n"));

        qDebug() << arrayData;

        QTest::newRow("Other_order_than_write") << arrayData << expectedDataDefinitionsHandler;
    }

    // self closing parts
    {
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = documentdatadefinitions::data::DataDefinitionsHandlerData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler>");

        arrayData.append(
          writeDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().generalVariableList));
        arrayData.append(
          writeListDataDefinitionListDataXml(expectedDataDefinitionsHandler.value().listDataDefinitionList));

        arrayData.append(QByteArrayLiteral("</DataDefinitionsHandler>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedDataDefinitionsHandler;
    }

    // Empty DataDefinitionsHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler>"
                                                 "</DataDefinitionsHandler>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = documentdatadefinitions::data::DataDefinitionsHandlerData {};
        QTest::newRow("Empty_ListDataDefinitionData") << arrayData << expectedDataDefinitionsHandler;
    }

    // Empty self closing DataDefinitionsHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler/>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = documentdatadefinitions::data::DataDefinitionsHandlerData {};
        QTest::newRow("Empty_self_closing_ListDataDefinitionData") << arrayData << expectedDataDefinitionsHandler;
    }

    // Not ListDataDefinitionData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = std::nullopt;
        QTest::newRow("Not_DataDefinitionsHandler") << arrayData << expectedDataDefinitionsHandler;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
          expectedDataDefinitionsHandler = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDataDefinitionsHandler;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataDefinitionsHandlerData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>,
           expectedDataDefinitionsHandlerData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
      actualDataDefinitionsHandlerData =
        documentdatadefinitions::service::readDataDefinitionsHandlerData(streamReader);

    if (expectedDataDefinitionsHandlerData.has_value())
    {
        QCOMPARE(actualDataDefinitionsHandlerData.value(), expectedDataDefinitionsHandlerData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::dataDefinitionsHandlerElement);
    }
    else { QVERIFY(!actualDataDefinitionsHandlerData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeDataDefinitionsHandlerData()
{
    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData =
      datatemplatetestdata::dataDefinitionHandlerTestData();

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeDataDefinitionsHandlerData(streamWriter, dataDefinitionsHandlerData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataDefinitionsHandler>");

    expectedArray.append(writeDataDefinitionListDataXml(dataDefinitionsHandlerData.generalVariableList));
    expectedArray.append(writeListDataDefinitionListDataXml(dataDefinitionsHandlerData.listDataDefinitionList));

    expectedArray.append(QByteArrayLiteral("</DataDefinitionsHandler>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readUserDataHandlerData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::UserDataHandlerData>>(
      "expectedUserDataHandlerData");

    // Same order than write
    {
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          datatemplatetestdata::userDataHandlerData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler>");

        arrayData.append(writeDataTablesDataXml(expectedUserDataHandlerData.value().generalVariableDataList));

        arrayData.append(QByteArrayLiteral("<DataTableModelList>"));

        for (const documentdatadefinitions::data::DataTablesData& dataTablesData :
             expectedUserDataHandlerData.value().dataTableModelListModel.dataTableModelList)
        {
            arrayData.append(writeDataTablesDataXml(dataTablesData));
        }

        arrayData.append(QByteArrayLiteral("</DataTableModelList>"));

        arrayData.append(QByteArrayLiteral("</UserDataHandler>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedUserDataHandlerData;
    }

    // Other order than write
    {
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          datatemplatetestdata::userDataHandlerData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler>");

        arrayData.append(QByteArrayLiteral("<DataTableModelList>"));

        for (const documentdatadefinitions::data::DataTablesData& dataTablesData :
             expectedUserDataHandlerData.value().dataTableModelListModel.dataTableModelList)
        {
            arrayData.append(writeDataTablesDataXml(dataTablesData));
        }

        arrayData.append(QByteArrayLiteral("</DataTableModelList>"));

        arrayData.append(writeDataTablesDataXml(expectedUserDataHandlerData.value().generalVariableDataList));

        arrayData.append(QByteArrayLiteral("</UserDataHandler>\n"));

        QTest::newRow("Other_order_than_write") << arrayData << expectedUserDataHandlerData;
    }

    // self closing parts
    {
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          documentdatadefinitions::data::UserDataHandlerData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler>");

        arrayData.append(writeDataTablesDataXml(expectedUserDataHandlerData.value().generalVariableDataList));

        arrayData.append(QByteArrayLiteral("<DataTableModelList/>"));

        arrayData.append(QByteArrayLiteral("</UserDataHandler>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedUserDataHandlerData;
    }

    // Empty UserDataHandlerData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler>"
                                                 "</UserDataHandler>\n");
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          documentdatadefinitions::data::UserDataHandlerData {};
        QTest::newRow("Empty_ListDataDefinitionData") << arrayData << expectedUserDataHandlerData;
    }

    // Empty self closing UserDataHandlerData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler/>\n");
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          documentdatadefinitions::data::UserDataHandlerData {};
        QTest::newRow("Empty_self_closing_UserDataHandlerData") << arrayData << expectedUserDataHandlerData;
    }

    // Not UserDataHandlerData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          std::nullopt;
        QTest::newRow("Not_UserDataHandlerData") << arrayData << expectedUserDataHandlerData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::UserDataHandlerData> expectedUserDataHandlerData =
          std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedUserDataHandlerData;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readUserDataHandlerData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::UserDataHandlerData>, expectedUserDataHandlerData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::UserDataHandlerData> actualUserDataHandlerData =
      documentdatadefinitions::service::readUserDataHandlerData(streamReader);

    if (expectedUserDataHandlerData.has_value())
    {
        QCOMPARE(actualUserDataHandlerData.value(), expectedUserDataHandlerData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::userDataHandlerElement);
    }
    else { QVERIFY(!actualUserDataHandlerData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeUserDataHandlerData()
{
    documentdatadefinitions::data::UserDataHandlerData userDataHandlerData =
      datatemplatetestdata::userDataHandlerData();

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeUserDataHandlerData(streamWriter, userDataHandlerData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<UserDataHandler>");

    expectedArray.append(writeDataTablesDataXml(userDataHandlerData.generalVariableDataList));

    expectedArray.append(QByteArrayLiteral("<DataTableModelList>"));

    for (const documentdatadefinitions::data::DataTablesData& dataTablesData :
         userDataHandlerData.dataTableModelListModel.dataTableModelList)
    {
        expectedArray.append(writeDataTablesDataXml(dataTablesData));
    }

    expectedArray.append(QByteArrayLiteral("</DataTableModelList>"));
    expectedArray.append(QByteArrayLiteral("</UserDataHandler>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataHandlerData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<documentdatadefinitions::data::DataHandlerData>>(
      "expectedDataHandlerData");

    // Same order than write
    {
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandlerData =
          documentdatadefinitions::data::DataHandlerData {
              datatemplatetestdata::dataDefinitionHandlerTestData(), datatemplatetestdata::userDataHandlerData()
          };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler>");

        arrayData.append(
          writeDataDefinitionsHandlerDataXml(expectedDataHandlerData.value().dataDefinitionsHandler));
        arrayData.append(writeUserDataHandlerDataXml(expectedDataHandlerData.value().userDataHandler));

        arrayData.append(QByteArrayLiteral("</DataHandler>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedDataHandlerData;
    }

    // Other order than write
    {
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandlerData =
          documentdatadefinitions::data::DataHandlerData {
              datatemplatetestdata::dataDefinitionHandlerTestData(), datatemplatetestdata::userDataHandlerData()
          };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler>");

        arrayData.append(writeUserDataHandlerDataXml(expectedDataHandlerData.value().userDataHandler));
        arrayData.append(
          writeDataDefinitionsHandlerDataXml(expectedDataHandlerData.value().dataDefinitionsHandler));

        arrayData.append(QByteArrayLiteral("</DataHandler>\n"));

        QTest::newRow("Other_order_than_write") << arrayData << expectedDataHandlerData;
    }

    // self closing parts
    {
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandlerData =
          documentdatadefinitions::data::DataHandlerData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler>");

        arrayData.append(
          writeDataDefinitionsHandlerDataXml(expectedDataHandlerData.value().dataDefinitionsHandler));
        arrayData.append(writeUserDataHandlerDataXml(expectedDataHandlerData.value().userDataHandler));

        arrayData.append(QByteArrayLiteral("</DataHandler>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedDataHandlerData;
    }

    // Empty UserDataHandlerData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler>"
                                                 "</DataHandler>\n");
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandlerData =
          documentdatadefinitions::data::DataHandlerData {};
        QTest::newRow("Empty_ListDataDefinitionData") << arrayData << expectedDataHandlerData;
    }

    // Empty self closing DataHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler/>\n");
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandlerData =
          documentdatadefinitions::data::DataHandlerData {};
        QTest::newRow("Empty_self_closing_DataHandler") << arrayData << expectedDataHandlerData;
    }

    // Not DataHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandler = std::nullopt;
        QTest::newRow("Not_UserDataHandlerData") << arrayData << expectedDataHandler;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<documentdatadefinitions::data::DataHandlerData> expectedDataHandler = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDataHandler;
    }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_readDataHandlerData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<documentdatadefinitions::data::DataHandlerData>, expectedDataHandlerData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<documentdatadefinitions::data::DataHandlerData> actualDataHandlerData =
      documentdatadefinitions::service::readDataHandlerData(streamReader);

    if (expectedDataHandlerData.has_value())
    {
        QCOMPARE(actualDataHandlerData.value(), expectedDataHandlerData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), documentdatadefinitions::service::dataHandlerElement);
    }
    else { QVERIFY(!actualDataHandlerData.has_value()); }
}

void Test_DocumentDataDefinitionsXmlSerialization::test_writeDataHandlerData()
{
    documentdatadefinitions::data::DataHandlerData dataHandlerData =
      documentdatadefinitions::data::DataHandlerData {
          datatemplatetestdata::dataDefinitionHandlerTestData(), datatemplatetestdata::userDataHandlerData()
      };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    documentdatadefinitions::service::writeDataHandlerData(streamWriter, dataHandlerData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DataHandler>");

    expectedArray.append(writeDataDefinitionsHandlerDataXml(dataHandlerData.dataDefinitionsHandler));
    expectedArray.append(writeUserDataHandlerDataXml(dataHandlerData.userDataHandler));

    expectedArray.append(QByteArrayLiteral("</DataHandler>\n"));

    QCOMPARE(arrayData, expectedArray);
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeDataDefinitionListDataXml(
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData)
{
    QByteArray dataDefinitionListDataXml;

    QXmlStreamWriter streamWriter(&dataDefinitionListDataXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeDataDefinitionListData(streamWriter, dataDefinitionListData);

    return dataDefinitionListDataXml;
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeDataTablesDataXml(
  const documentdatadefinitions::data::DataTablesData& dataTablesData)
{
    QByteArray dataTablesDataXml;

    QXmlStreamWriter streamWriter(&dataTablesDataXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeDataTablesData(streamWriter, dataTablesData);

    return dataTablesDataXml;
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeListDataDefinitionDataXml(
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData)
{
    QByteArray listDataDefinitionDataXml;

    QXmlStreamWriter streamWriter(&listDataDefinitionDataXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeListDataDefinitionData(streamWriter, listDataDefinitionData);

    return listDataDefinitionDataXml;
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeListDataDefinitionListDataXml(
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData)
{
    QByteArray listDataDefinitionDataListXml;

    QXmlStreamWriter streamWriter(&listDataDefinitionDataListXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeListDataDefinitionListData(streamWriter, listDataDefinitionListData);

    return listDataDefinitionDataListXml;
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeDataDefinitionsHandlerDataXml(
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData)
{
    QByteArray dataDefinitionsHandlerDataXml;

    QXmlStreamWriter streamWriter(&dataDefinitionsHandlerDataXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeDataDefinitionsHandlerData(streamWriter, dataDefinitionsHandlerData);

    return dataDefinitionsHandlerDataXml;
}

QByteArray Test_DocumentDataDefinitionsXmlSerialization::writeUserDataHandlerDataXml(
  const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData)
{
    QByteArray userDataHandlerDataXml;

    QXmlStreamWriter streamWriter(&userDataHandlerDataXml);
    streamWriter.setAutoFormatting(false);

    documentdatadefinitions::service::writeUserDataHandlerData(streamWriter, userDataHandlerData);

    return userDataHandlerDataXml;
}

QTEST_APPLESS_MAIN(Test_DocumentDataDefinitionsXmlSerialization)

#include "Test_DocumentDataDefinitionsXmlSerialization.moc"
