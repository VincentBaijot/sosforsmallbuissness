#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"

class Test_DataDefinitionListModel : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
};

void Test_DataDefinitionListModel::data()
{
    documentdatadefinitions::data::DataDefinitionListData testDataDefinitionListData(
      datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList);

    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel;

    dataDefinitionListModel.setListName(testDataDefinitionListData.listName);

    // For now the list already contains one data definition
    // dataDefinitionListModel.addDataDefinition();

    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(0),
      QVariant::fromValue(testDataDefinitionListData.dataDefinitions.at(0).dataType),
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::DataTypeDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(0),
      testDataDefinitionListData.dataDefinitions.at(0).key,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::KeyDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(0),
      testDataDefinitionListData.dataDefinitions.at(0).name,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::NameDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(0),
      testDataDefinitionListData.dataDefinitions.at(0).description,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::DescriptionDefinition));

    dataDefinitionListModel.addDataDefinition();

    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(1),
      QVariant::fromValue(testDataDefinitionListData.dataDefinitions.at(1).dataType),
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::DataTypeDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(1),
      testDataDefinitionListData.dataDefinitions.at(1).key,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::KeyDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(1),
      testDataDefinitionListData.dataDefinitions.at(1).name,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::NameDefinition));
    dataDefinitionListModel.setData(
      dataDefinitionListModel.index(1),
      testDataDefinitionListData.dataDefinitions.at(1).description,
      static_cast<int>(
        documentdatadefinitions::controller::DataDefinitionListModel::DataDefinitionRole::DescriptionDefinition));

    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData(
      dataDefinitionListModel.objectData());

    QCOMPARE(dataDefinitionListData.listName, testDataDefinitionListData.listName);

    QCOMPARE(dataDefinitionListData.dataDefinitions.size(), testDataDefinitionListData.dataDefinitions.size());

    for (int index = 0; index < dataDefinitionListData.dataDefinitions.size(); ++index)
    {
        documentdatadefinitions::data::DataDefinitionData dataDefinitionData =
          dataDefinitionListData.dataDefinitions.at(index);

        QCOMPARE(dataDefinitionData, testDataDefinitionListData.dataDefinitions.at(index));
    }
}

void Test_DataDefinitionListModel::setData()
{
    QList<QPointer<documentdatadefinitions::controller::DataDefinition>> dataDefinitions;

    {
        //----------------------------------------------------------
        // Test constructor with data
        //----------------------------------------------------------
        documentdatadefinitions::data::DataDefinitionListData testDataDefinitionListData(
          datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList);

        documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel(
          testDataDefinitionListData);

        QSignalSpy listNameChanged(&dataDefinitionListModel,
                                   &documentdatadefinitions::controller::DataDefinitionListModel::listNameChanged);
        QSignalSpy modelAboutToBeResetSpy(
          &dataDefinitionListModel,
          &documentdatadefinitions::controller::DataDefinitionListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&dataDefinitionListModel,
                                 &documentdatadefinitions::controller::DataDefinitionListModel::modelReset);
        QSignalSpy dataChangedSpy(&dataDefinitionListModel,
                                  &documentdatadefinitions::controller::DataDefinitionListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(
          &dataDefinitionListModel,
          &documentdatadefinitions::controller::DataDefinitionListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&dataDefinitionListModel,
                                   &documentdatadefinitions::controller::DataDefinitionListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(
          &dataDefinitionListModel,
          &documentdatadefinitions::controller::DataDefinitionListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&dataDefinitionListModel,
                                  &documentdatadefinitions::controller::DataDefinitionListModel::rowsRemoved);

        QCOMPARE(listNameChanged.size(), 0);
        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        QCOMPARE(dataDefinitionListModel.listName(), testDataDefinitionListData.listName);

        QCOMPARE(dataDefinitionListModel.rowCount(), testDataDefinitionListData.dataDefinitions.size());
        for (int index = 0; index < dataDefinitionListModel.rowCount(); ++index)
        {
            QPointer<documentdatadefinitions::controller::DataDefinition> dataDefinitionPointer =
              dataDefinitionListModel.at(index);
            dataDefinitions.append(dataDefinitionPointer);

            documentdatadefinitions::data::DataDefinitionData dataDefinition =
              testDataDefinitionListData.dataDefinitions.at(index);

            QCOMPARE(dataDefinitionPointer->dataType(), dataDefinition.dataType);
            QCOMPARE(dataDefinitionPointer->key(), dataDefinition.key);
            QCOMPARE(dataDefinitionPointer->name(), dataDefinition.name);
            QCOMPARE(dataDefinitionPointer->description(), dataDefinition.description);
        }

        QCOMPARE(dataDefinitions.size(), testDataDefinitionListData.dataDefinitions.size());
        for (const QPointer<documentdatadefinitions::controller::DataDefinition>& dataDefinitionPointer :
             dataDefinitions)
        {
            QVERIFY(!dataDefinitionPointer.isNull());
        }

        QCOMPARE(dataDefinitionListModel.objectData(), testDataDefinitionListData);

        //-------------------------------------------------------
        // Test set data function
        //-------------------------------------------------------

        documentdatadefinitions::data::DataDefinitionListData testDataDefinitionListData2 =
          datatemplatetestdata::dataDefinitionHandlerTestData()
            .listDataDefinitionList.listOfDataDefinition.front()
            .dataDefinitionListData;

        dataDefinitionListModel.setObjectData(testDataDefinitionListData2);

        QCOMPARE(dataDefinitions.size(), testDataDefinitionListData.dataDefinitions.size());
        for (const QPointer<documentdatadefinitions::controller::DataDefinition>& dataDefinitionPointer :
             dataDefinitions)
        {
            QTRY_VERIFY(dataDefinitionPointer.isNull());
        }
        dataDefinitions.clear();

        QCOMPARE(listNameChanged.size(), 1);
        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        QCOMPARE(dataDefinitionListModel.listName(), testDataDefinitionListData2.listName);

        QCOMPARE(dataDefinitionListModel.rowCount(), testDataDefinitionListData2.dataDefinitions.size());
        for (int index = 0; index < dataDefinitionListModel.rowCount(); ++index)
        {
            QPointer<documentdatadefinitions::controller::DataDefinition> dataDefinitionPointer =
              dataDefinitionListModel.at(index);
            dataDefinitions.append(dataDefinitionPointer);

            documentdatadefinitions::data::DataDefinitionData dataDefinition =
              testDataDefinitionListData2.dataDefinitions.at(index);

            QCOMPARE(dataDefinitionPointer->dataType(), dataDefinition.dataType);
            QCOMPARE(dataDefinitionPointer->key(), dataDefinition.key);
            QCOMPARE(dataDefinitionPointer->name(), dataDefinition.name);
            QCOMPARE(dataDefinitionPointer->description(), dataDefinition.description);
        }

        QCOMPARE(dataDefinitions.size(), testDataDefinitionListData2.dataDefinitions.size());
        for (const QPointer<documentdatadefinitions::controller::DataDefinition>& dataDefinitionPointer :
             dataDefinitions)
        {
            QVERIFY(!dataDefinitionPointer.isNull());
        }

        dataDefinitionListModel.setObjectData(testDataDefinitionListData2);

        QCOMPARE(dataDefinitions.size(), testDataDefinitionListData2.dataDefinitions.size());
        for (const QPointer<documentdatadefinitions::controller::DataDefinition>& dataDefinitionPointer :
             dataDefinitions)
        {
            QVERIFY(!dataDefinitionPointer.isNull());
        }

        QCOMPARE(listNameChanged.size(), 1);
        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        QCOMPARE(dataDefinitionListModel.objectData(), testDataDefinitionListData2);
    }

    QCOMPARE(dataDefinitions.size(), 3);
    for (const QPointer<documentdatadefinitions::controller::DataDefinition>& dataDefinitionPointer :
         dataDefinitions)
    {
        QVERIFY(dataDefinitionPointer.isNull());
    }
}

QTEST_GUILESS_MAIN(Test_DataDefinitionListModel)

#include "Test_DataDefinitionListModel.moc"
