#pragma once

#include "DocumentDataDefinitions/data/DataDefinitionsHandlerData.hpp"
#include "DocumentDataDefinitions/data/UserDataHandlerData.hpp"

namespace datatemplatetestdata
{
documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionHandlerTestData()
{
    documentdatadefinitions::data::DataDefinitionListData generalVariableListData {
        "The list name",
        { documentdatadefinitions::data::DataDefinitionData {
            "The key",
            "The name",
            "The description",
            documentdatadefinitions::data::DataDefinitionData::DataType::NumberType },
          documentdatadefinitions::data::DataDefinitionData {
            "The key 2",
            "The name 2",
            "The description 2",
            documentdatadefinitions::data::DataDefinitionData::DataType::StringType } }
    };

    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData1 {
        documentdatadefinitions::data::DataDefinitionListData {
          "Name of the list",
          { documentdatadefinitions::data::DataDefinitionData {
              "Date",
              "The date of the day",
              "Its today the day of the day",
              documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
            documentdatadefinitions::data::DataDefinitionData {
              "Product",
              "The product",
              "Something sell to a client",
              documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
            documentdatadefinitions::data::DataDefinitionData {
              "Count",
              "The number of something",
              "1,2,3 and more",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } } },
        documentdatadefinitions::data::DataTablesData {
          { documentdatadefinitions::data::DataTableData { { 0, "02/11/22" }, { 1, "Demo product a" }, { 2, "42" } },
            documentdatadefinitions::data::DataTableData { { 0, "03/11/22" }, { 1, "Demo product b" }, { 2, "23" } },
            documentdatadefinitions::data::DataTableData {
              { 0, "04/11/22" }, { 1, "Demo product c" }, { 2, "12" } } } }
    };

    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData2 {
        documentdatadefinitions::data::DataDefinitionListData {
          "Name of the list 2",
          { documentdatadefinitions::data::DataDefinitionData {
              "Date 2",
              "The date of the day 2",
              "Its today the day of the day 2",
              documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
            documentdatadefinitions::data::DataDefinitionData {
              "Product 2",
              "The product 2",
              "Something sell to a client 2",
              documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
            documentdatadefinitions::data::DataDefinitionData {
              "Count 2",
              "The number of something 2",
              "1,2,3 and more 2",
              documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } } },
        documentdatadefinitions::data::DataTablesData {
          { documentdatadefinitions::data::DataTableData { { 0, "02/12/22" }, { 1, "Demo product 1" }, { 2, "56" } },
            documentdatadefinitions::data::DataTableData { { 0, "03/12/22" }, { 1, "Demo product 2" }, { 2, "45" } },
            documentdatadefinitions::data::DataTableData {
              { 0, "04/12/22" }, { 1, "Demo product 3" }, { 2, "34" } } } }
    };

    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData {
        { listDataDefinitionData1, listDataDefinitionData2 }
    };

    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData {
        generalVariableListData, listDataDefinitionListData
    };

    return dataDefinitionsHandlerData;
}

documentdatadefinitions::data::UserDataHandlerData userDataHandlerData()
{
    documentdatadefinitions::data::UserDataHandlerData userDataHandlerData {
        documentdatadefinitions::data::DataTablesData {
          { documentdatadefinitions::data::DataTableData { { 0, "Product 1 Key 1" }, { 1, "Product 1 Key 2" } },
            documentdatadefinitions::data::DataTableData { { 0, "Product 2 Key 1" }, { 1, "Product 2 Key 2" } },
            documentdatadefinitions::data::DataTableData { { 0, "Product 3 Key 1" }, { 1, "Product 3 Key 2" } } } },
        documentdatadefinitions::data::DataTableModelListData {
          { documentdatadefinitions::data::DataTablesData {
              { documentdatadefinitions::data::DataTableData { { 0, "02/11/22" }, { 1, "Product a" }, { 2, "42" } },
                documentdatadefinitions::data::DataTableData { { 0, "03/11/22" }, { 1, "Product b" }, { 2, "23" } },
                documentdatadefinitions::data::DataTableData {
                  { 0, "04/11/22" }, { 1, "Product c" }, { 2, "12" } } } },
            documentdatadefinitions::data::DataTablesData {
              { documentdatadefinitions::data::DataTableData { { 0, "02/12/22" }, { 1, "Product 1" }, { 2, "56" } },
                documentdatadefinitions::data::DataTableData { { 0, "03/12/22" }, { 1, "Product 2" }, { 2, "45" } },
                documentdatadefinitions::data::DataTableData {
                  { 0, "04/12/22" }, { 1, "Product 3" }, { 2, "34" } } } } } }
    };

    return userDataHandlerData;
}

}    // namespace datatemplatetestdata
