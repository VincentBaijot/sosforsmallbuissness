#include <QSignalSpy>
#include <QTest>

#include "DocumentDataDefinitions/DataTemplateTestData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinition.hpp"

class Test_DataDefinition : public QObject
{
    Q_OBJECT

  private slots:
    void data();
    void setData();
};

void Test_DataDefinition::data()
{
    documentdatadefinitions::data::DataDefinitionData testdataDefinitionData(
      datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList.dataDefinitions.front());

    documentdatadefinitions::controller::DataDefinition dataDefinition;
    dataDefinition.setDataType(testdataDefinitionData.dataType);
    dataDefinition.setKey(testdataDefinitionData.key);
    dataDefinition.setName(testdataDefinitionData.name);
    dataDefinition.setDescription(testdataDefinitionData.description);

    documentdatadefinitions::data::DataDefinitionData dataDefinitionData(dataDefinition.objectData());
    QCOMPARE(dataDefinitionData.dataType, testdataDefinitionData.dataType);
    QCOMPARE(dataDefinitionData.key, testdataDefinitionData.key);
    QCOMPARE(dataDefinitionData.name, testdataDefinitionData.name);
    QCOMPARE(dataDefinitionData.description, testdataDefinitionData.description);
}

void Test_DataDefinition::setData()
{
    documentdatadefinitions::data::DataDefinitionData dataDefinitionData(
      datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList.dataDefinitions.front());

    //----------------------------------------------------------
    // Test constructor with data
    //----------------------------------------------------------
    documentdatadefinitions::controller::DataDefinition dataDefinition(dataDefinitionData);

    QSignalSpy keyChangedSpy(&dataDefinition, &documentdatadefinitions::controller::DataDefinition::keyChanged);
    QSignalSpy nameChangedSpy(&dataDefinition, &documentdatadefinitions::controller::DataDefinition::nameChanged);
    QSignalSpy descriptionChangedSpy(&dataDefinition, &documentdatadefinitions::controller::DataDefinition::descriptionChanged);
    QSignalSpy dataTypeChangedSpy(&dataDefinition, &documentdatadefinitions::controller::DataDefinition::dataTypeChanged);

    QCOMPARE(dataDefinition.key(), dataDefinitionData.key);
    QCOMPARE(dataDefinition.name(), dataDefinitionData.name);
    QCOMPARE(dataDefinition.description(), dataDefinitionData.description);
    QCOMPARE(dataDefinition.dataType(), dataDefinitionData.dataType);

    dataDefinitionData =
      datatemplatetestdata::dataDefinitionHandlerTestData().generalVariableList.dataDefinitions.back();

    //-------------------------------------------------------
    // Test set data function
    //-------------------------------------------------------
    dataDefinition.setObjectData(dataDefinitionData);

    QCOMPARE(keyChangedSpy.size(), 1);
    QCOMPARE(nameChangedSpy.size(), 1);
    QCOMPARE(descriptionChangedSpy.size(), 1);
    QCOMPARE(dataTypeChangedSpy.size(), 1);

    QCOMPARE(dataDefinition.key(), dataDefinitionData.key);
    QCOMPARE(dataDefinition.name(), dataDefinitionData.name);
    QCOMPARE(dataDefinition.description(), dataDefinitionData.description);
    QCOMPARE(dataDefinition.dataType(), dataDefinitionData.dataType);

    QCOMPARE(dataDefinition.objectData(), dataDefinitionData);

    dataDefinition.setObjectData(dataDefinitionData);

    QCOMPARE(keyChangedSpy.size(), 1);
    QCOMPARE(nameChangedSpy.size(), 1);
    QCOMPARE(descriptionChangedSpy.size(), 1);
    QCOMPARE(dataTypeChangedSpy.size(), 1);

    QCOMPARE(dataDefinition.key(), dataDefinitionData.key);
    QCOMPARE(dataDefinition.name(), dataDefinitionData.name);
    QCOMPARE(dataDefinition.description(), dataDefinitionData.description);
    QCOMPARE(dataDefinition.dataType(), dataDefinitionData.dataType);

    QCOMPARE(dataDefinition.objectData(), dataDefinitionData);
}

QTEST_APPLESS_MAIN(Test_DataDefinition)

#include "Test_DataDefinition.moc"
