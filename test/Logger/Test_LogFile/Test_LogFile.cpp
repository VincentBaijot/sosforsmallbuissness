#include <QTest>

#include "Logger/LogFile.hpp"

class Test_LogFile : public QObject
{
    Q_OBJECT

  private slots:
    void readLogFile();
    void writLogFile();
};

void Test_LogFile::readLogFile()
{
    QCoreApplication::setApplicationName("ExampleApplication");
    logger::LogFile logFile(":/" + QCoreApplication::applicationName() + "_20190520_082136452.log");

    QVERIFY2(logFile.exists(), qUtf8Printable("Cannot find the file : " + QFileInfo(logFile).absolutePath()));

    QCOMPARE(QDateTime::fromString("20190520_082136452", logger::LOG_DATE_FORMAT).toString(),
             logFile.getLogDate().toString());
}

void Test_LogFile::writLogFile()
{
    QCoreApplication::setApplicationName("ExampleApplication");
    QTemporaryDir output;
    QVERIFY(output.isValid());

    logger::LogFile logFile(QDir(output.path()));

    if (logFile.open(QIODevice::WriteOnly))
    {
        logFile.write("Whatever");
        logFile.close();
    }

    QVERIFY(QFile::exists(logFile.fileName()));
    QVERIFY(logFile.getLogDate().msecsTo(QDateTime::currentDateTime()) < 100);

    QCOMPARE(QFileInfo(logFile).fileName(),
             qUtf8Printable(QCoreApplication::applicationName() + "_"
                            + logFile.getLogDate().toString(logger::LOG_DATE_FORMAT) + ".log"));
}

QTEST_APPLESS_MAIN(Test_LogFile)

#include "Test_LogFile.moc"
