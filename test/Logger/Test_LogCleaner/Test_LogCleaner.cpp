#include <QDebug>
#include <QTest>

#include "Logger/LogFile.hpp"
#include "Logger/Logger.hpp"

class Test_LogCleaner : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void cleanLog();

  private:
    void createFile(QFile& file);
};

void Test_LogCleaner::cleanLog()
{
    QCoreApplication::setApplicationName("ExampleApplication");
    QTemporaryDir output;
    QVERIFY(output.isValid());

    QFile logFile1(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().toString(logger::LOG_DATE_FORMAT) + ".log");
    QFile logFile2(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().addDays(-5).toString(logger::LOG_DATE_FORMAT) + ".log");
    QFile logFile3(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().addDays(-10).toString(logger::LOG_DATE_FORMAT) + ".log");
    QFile logFile4(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().addDays(-15).toString(logger::LOG_DATE_FORMAT) + ".log");
    QFile logFile5(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().addDays(-20).toString(logger::LOG_DATE_FORMAT) + ".log");
    QFile logFile6(output.path() + "/" + QCoreApplication::applicationName() + "_"
                   + QDateTime::currentDateTime().addDays(-25).toString(logger::LOG_DATE_FORMAT) + ".log");

    createFile(logFile1);
    createFile(logFile2);
    createFile(logFile3);
    createFile(logFile4);
    createFile(logFile5);

    logger::cleanLogger(output.path());

    QVERIFY(logFile1.exists());
    QVERIFY(logFile2.exists());
    QVERIFY(logFile3.exists());
    QVERIFY(logFile4.exists());
    QVERIFY(!logFile5.exists());
    QVERIFY(!logFile6.exists());

    logFile1.remove();
    logFile2.remove();
    logFile3.remove();
    logFile4.remove();
}

void Test_LogCleaner::createFile(QFile& file)
{
    if (!file.exists() && file.open(QIODevice::WriteOnly))
    {
        file.write(" ");
        file.close();
    }
}

QTEST_APPLESS_MAIN(Test_LogCleaner)

#include "Test_LogCleaner.moc"
