#include <QDebug>
#include <QLoggingCategory>
#include <QTest>

#include "Logger/Logger.hpp"

Q_DECLARE_LOGGING_CATEGORY(testCategory)

Q_LOGGING_CATEGORY(testCategory, "test.category")

class Test_Logger_DEBUG : public QObject
{
    Q_OBJECT

  public:
    Test_Logger_DEBUG() = default;

  private slots:
    void writeLog();
};

void Test_Logger_DEBUG::writeLog()
{
    QTemporaryDir output;
    QVERIFY(output.isValid());
    QDir outputDir(output.path());

    logger::setLogFolder(outputDir);

    qInstallMessageHandler(logger::messageHandler);

    qDebug("Hello");
    qInfo("Some info");
    qWarning("is there in the log");
    qCritical("you need to read it");

    qCDebug(testCategory) << "A module";
    qCInfo(testCategory) << "have";
    qCWarning(testCategory) << "something";
    qCCritical(testCategory) << "to say";

    QFile logFile(output.path() + "/" + outputDir.entryList().at(2));
    QVERIFY2(logFile.exists(), qUtf8Printable("Cannot find the log file : " + QFileInfo(logFile).fileName()));

    if (logFile.open(QIODevice::ReadOnly))
    {
        QString log1(logFile.readLine());
        QString log2(logFile.readLine());
        QString log3(logFile.readLine());
        QString log4(logFile.readLine());

        QVERIFY2(log1.startsWith("[Debug]") && log1.contains("(default)") && log1.contains(" : \"Hello\"")
                   && (log1.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:32")
                       || log1.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:32")),
                 qUtf8Printable(log1));
        QVERIFY2(log2.startsWith("[Info]") && log2.contains("(default)") && log2.contains(" : \"Some info\"")
                   && (log2.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:33")
                       || log2.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:33")),
                 qUtf8Printable(log2));
        QVERIFY2(log3.startsWith("[Warning]") && log3.contains("(default)")
                   && log3.contains(" : \"is there in the log\"")
                   && (log3.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:34")
                       || log3.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:34")),
                 qUtf8Printable(log3));
        QVERIFY2(log4.startsWith("[Critical]") && log4.contains("(default)")
                   && log4.contains(" : \"you need to read it\"")
                   && (log4.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:35")
                       || log4.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:35")),
                 qUtf8Printable(log4));

        QString log5(logFile.readLine());
        QString log6(logFile.readLine());
        QString log7(logFile.readLine());
        QString log8(logFile.readLine());

        QVERIFY2(log5.startsWith("[Debug]") && log5.contains("(test.category)") && log5.contains(" : \"A module\"")
                   && (log5.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:37")
                       || log5.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:37")),
                 qUtf8Printable(log5));
        QVERIFY2(log6.startsWith("[Info]") && log6.contains("(test.category)") && log6.contains(" : \"have\"")
                   && (log6.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:38")
                       || log6.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:38")),
                 qUtf8Printable(log6));
        QVERIFY2(log7.startsWith("[Warning]") && log7.contains("(test.category)")
                   && log7.contains(" : \"something\"")
                   && (log7.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:39")
                       || log7.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:39")),
                 qUtf8Printable(log7));
        QVERIFY2(log8.startsWith("[Critical]") && log8.contains("(test.category)")
                   && log8.contains(" : \"to say\"")
                   && (log8.contains("Logger\\Test_Logger_DEBUG\\Test_Logger_DEBUG.cpp:40")
                       || log8.contains("Logger/Test_Logger_DEBUG/Test_Logger_DEBUG.cpp:40")),
                 qUtf8Printable(log8));

        QVERIFY(logFile.readLine().isEmpty());
    }
    else { QFAIL("Cannot open the log file to read"); }
}

QTEST_APPLESS_MAIN(Test_Logger_DEBUG)

#include "Test_Logger_DEBUG.moc"
