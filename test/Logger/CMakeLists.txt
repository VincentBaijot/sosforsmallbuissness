cmake_minimum_required(VERSION 3.20)

print_location()

add_subdirectory(Test_LogFile)
add_subdirectory(Test_LogCleaner)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
add_subdirectory(Test_Logger_DEBUG)
else ()
add_subdirectory(Test_Logger_RELEASE)
endif ()
