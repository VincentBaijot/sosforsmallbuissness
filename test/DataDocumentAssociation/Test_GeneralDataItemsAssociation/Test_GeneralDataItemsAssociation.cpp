#include <QTest>

#include "DataDocumentAssociation/controllers/GeneralDataItemsAssociation.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataDefinitionListAssociationData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataTableModelAssociationData.hpp"
#include "Document/controllers/Document.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentDataDefinitions/data/DataDefinitionListData.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentDataDefinitions/data/DataTablesData.hpp"
#include "Document/data/DocumentData.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"

class Test_GeneralDataItemsAssociation : public QObject
{
    Q_OBJECT

  public:
    Test_GeneralDataItemsAssociation()           = default;
    ~Test_GeneralDataItemsAssociation() override = default;

  private slots:
    void test_objectData();
    void test_setObjectData();
    void test_onTableDataChanged();

  private:
    Q_DISABLE_COPY_MOVE(Test_GeneralDataItemsAssociation)

    documenttemplate::data::DocumentTemplateData _documentTemplateData() const;
    document::data::DocumentData _documentData() const;
    documentdatadefinitions::data::DataHandlerData _dataHandlerData() const;
    datatemplateassociation::data::DataTemplateAssociationData _dataTemplateAssociationData() const;
    datadocumentassociation::data::GeneralListItemsAssociationData _generalListItemsAssociationData();
};

void Test_GeneralDataItemsAssociation::test_objectData()
{
    document::controller::Document document(_documentData());
    documentdatadefinitions::controller::DataHandler dataHandler(_dataHandlerData());
    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();
    const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociation =
      _dataTemplateAssociationData();

    datadocumentassociation::controller::GeneralDataItemsAssociation generalListItemsAssociation;
    generalListItemsAssociation.setDocument(&document);
    generalListItemsAssociation.setGeneralDataTablesModel(
      dataHandler.userDataHandler()->generalVariableDataList());
    generalListItemsAssociation.setDocumentTemplateData(documentTemplateData);

    const datadocumentassociation::data::GeneralListItemsAssociationData& generalListItemsAssociationData =
      _generalListItemsAssociationData();

    for (const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData :
         generalListItemsAssociationData.generalListItemsAssociationData)
    {
        generalListItemsAssociation.insertTemplatedItems(pageItemsTemplateAssociationData);
    }

    QCOMPARE(generalListItemsAssociation.objectData(), generalListItemsAssociationData);
}

void Test_GeneralDataItemsAssociation::test_setObjectData()
{
    document::controller::Document document(_documentData());
    documentdatadefinitions::controller::DataHandler dataHandler(_dataHandlerData());
    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();
    const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociation =
      _dataTemplateAssociationData();

    const datadocumentassociation::data::GeneralListItemsAssociationData& generalListItemsAssociationData =
      _generalListItemsAssociationData();

    {
        datadocumentassociation::controller::GeneralDataItemsAssociation generalListItemsAssociation;
        generalListItemsAssociation.setDocument(&document);
        generalListItemsAssociation.setGeneralDataTablesModel(
          dataHandler.userDataHandler()->generalVariableDataList());
        generalListItemsAssociation.setDocumentTemplateData(documentTemplateData);

        generalListItemsAssociation.setObjectData(generalListItemsAssociationData);

        QCOMPARE(generalListItemsAssociation.objectData(), generalListItemsAssociationData);
    }

    {
        datadocumentassociation::controller::GeneralDataItemsAssociation generalListItemsAssociation(
          &document,
          dataHandler.userDataHandler()->generalVariableDataList(),
          documentTemplateData,
          generalListItemsAssociationData);

        QCOMPARE(generalListItemsAssociation.objectData(), generalListItemsAssociationData);
    }
}

void Test_GeneralDataItemsAssociation::test_onTableDataChanged()
{
    document::controller::Document document(_documentData());
    documentdatadefinitions::controller::DataHandler dataHandler(_dataHandlerData());
    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();
    const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociation =
      _dataTemplateAssociationData();

    const datadocumentassociation::data::GeneralListItemsAssociationData& generalListItemsAssociationData =
      _generalListItemsAssociationData();

    datadocumentassociation::controller::GeneralDataItemsAssociation generalListItemsAssociation;
    generalListItemsAssociation.setDocument(&document);
    generalListItemsAssociation.setGeneralDataTablesModel(
      dataHandler.userDataHandler()->generalVariableDataList());
    generalListItemsAssociation.setDocumentTemplateData(documentTemplateData);

    generalListItemsAssociation.setObjectData(generalListItemsAssociationData);

    document::data::DocumentData expectedDocumentData = document.objectData();

    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[0].paintableItemList[1])
      .text = "A general variable : 24 main street 658 NorthCity DarkCountry, 24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[0].paintableItemList[38])
      .text = "24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[1].paintableItemList[1])
      .text = "A general variable : 24 main street 658 NorthCity DarkCountry, 24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[1].paintableItemList[38])
      .text = "24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[2].paintableItemList[1])
      .text = "A general variable : 24 main street 658 NorthCity DarkCountry, 24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[2].paintableItemList[30])
      .text = "24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[3].paintableItemList[1])
      .text = "A general variable : 24 main street 658 NorthCity DarkCountry, 24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[3].paintableItemList[22])
      .text = "24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[4].paintableItemList[1])
      .text = "A general variable : 24 main street 658 NorthCity DarkCountry, 24/01/2024";
    std::get<painter::data::PaintableTextData>(expectedDocumentData.pageItemsList[4].paintableItemList[12])
      .text = "24/01/2024";

    dataHandler.userDataHandler()->generalVariableDataList()->setData(
      dataHandler.userDataHandler()->generalVariableDataList()->index(0, 1), "24/01/2024");

    document::data::DocumentData actualDocumentData = document.objectData();

    QCOMPARE(actualDocumentData.pageItemsList.size(), expectedDocumentData.pageItemsList.size());

    for (int pageIndex = 0; pageIndex < expectedDocumentData.pageItemsList.size(); ++pageIndex)
    {
        const auto& actualPageData   = actualDocumentData.pageItemsList.at(pageIndex);
        const auto& expectedPageData = expectedDocumentData.pageItemsList.at(pageIndex);

        QCOMPARE(actualPageData.paintableItemList.size(), expectedPageData.paintableItemList.size());

        for (int itemIndex = 0; itemIndex < expectedPageData.paintableItemList.size(); ++itemIndex)
        {
            QCOMPARE(actualPageData.paintableItemList.at(itemIndex),
                     expectedPageData.paintableItemList.at(itemIndex));
        }
    }

    QCOMPARE(document.objectData(), expectedDocumentData);
}

documenttemplate::data::DocumentTemplateData Test_GeneralDataItemsAssociation::_documentTemplateData() const
{
    using namespace painter::data;
    documenttemplate::data::DocumentTemplateData documentTemplateData;
    documentTemplateData.pageSizeData.setPageSize(QPageSize::A4);

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(PaintableTextData {
      ColorData {},
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
      QStringLiteral("A general variable : ${address}, ${date}") });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 17372, 5000, 8000 },
                          PaintableListData::RepetitionDirection::BottomRepetition,
                          PaintableListData::RepetitionDirection::RightRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 2280, 3843 } } },
                                                  QStringLiteral("Another data : ${DataOfList1}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 25000, 5000, 8000 },
                          PaintableListData::RepetitionDirection::BottomRepetition,
                          PaintableListData::RepetitionDirection::NoRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 5000, 1000 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 5000, 1000 } } },
                                                  QStringLiteral("Another data : ${DataOfList2}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 34000, 5000, 8000 },
                          PaintableListData::RepetitionDirection::RightRepetition,
                          PaintableListData::RepetitionDirection::NoRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 1000, 500 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 1000, 500 } } },
                                                  QStringLiteral("Another data : ${DataOfList3}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(PaintableTextData {
      ColorData {},
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } },
      QStringLiteral("${date}") });

    return documentTemplateData;
}

document::data::DocumentData Test_GeneralDataItemsAssociation::_documentData() const
{
    document::service::documentgenerator::GeneratedDocument generatedDocument =
      document::service::documentgenerator::generateDocument(
        _documentTemplateData(), _dataHandlerData(), _dataTemplateAssociationData());

    return generatedDocument.documentData;
}

documentdatadefinitions::data::DataHandlerData Test_GeneralDataItemsAssociation::_dataHandlerData() const
{
    using namespace documentdatadefinitions::data;

    DataHandlerData dataHandlerData;

    dataHandlerData.dataDefinitionsHandler.generalVariableList = DataDefinitionListData {
        "Geneal list data",
        { DataDefinitionData {
            "address", "The address", "Its somewhere", DataDefinitionData::DataType::StringType },
          DataDefinitionData {
            "date", "The date of today", "01/01/2023", DataDefinitionData::DataType::StringType } }
    };

    dataHandlerData.userDataHandler.generalVariableDataList = DataTablesData { { DataTableData {
      { 0, "24 main street 658 NorthCity DarkCountry" },
      { 1, "17/12/2022" },
    } } };

    for (int dataListIndex = 0; dataListIndex < 3; ++dataListIndex)
    {
        dataHandlerData.dataDefinitionsHandler.listDataDefinitionList.listOfDataDefinition.push_back(
          ListDataDefinitionData {
            DataDefinitionListData { QStringLiteral("DataList%1").arg(dataListIndex),
                                     {
                                       DataDefinitionData { QStringLiteral("DataOfList%1").arg(dataListIndex),
                                                            "Some data",
                                                            "Some data",
                                                            DataDefinitionData::DataType::StringType },
                                     } },
            DataTablesData { { DataTableData { { 0, "" } } } } });

        DataTablesData listData;

        for (int listRowIndex = 0; listRowIndex < 20; ++listRowIndex)
        {
            listData.dataTables.push_back(
              DataTableData { { 0, QStringLiteral("RandomData %1").arg(listRowIndex) } });
        }

        dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.push_back(listData);
    }

    return dataHandlerData;
}

datatemplateassociation::data::DataTemplateAssociationData
Test_GeneralDataItemsAssociation::_dataTemplateAssociationData() const
{
    return datatemplateassociation::data::DataTemplateAssociationData {
        datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
          QHash<datatemplateassociation::data::PaintableListIndex,
                datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 3, 1 }, { 5, 2 } } },
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
          QHash<datatemplateassociation::data::PaintableListIndex,
                datatemplateassociation::data::DataTablesModelIndex> { { 2, 0 }, { 3, 1 }, { 5, 2 } } }
    };
}

datadocumentassociation::data::GeneralListItemsAssociationData
Test_GeneralDataItemsAssociation::_generalListItemsAssociationData()
{
    document::service::documentgenerator::GeneratedDocument generatedDocument =
      document::service::documentgenerator::generateDocument(
        _documentTemplateData(), _dataHandlerData(), _dataTemplateAssociationData());

    datadocumentassociation::data::GeneralListItemsAssociationData generalListItemsAssociationData;

    for (int pageIndex = 0; pageIndex < generatedDocument.dataItemsAssociationData.pageItemsAssociationData.size();
         ++pageIndex)
    {
        datadocumentassociation::data::PageItemsTemplateAssociationData pageItemsTemplateAssociationData;
        pageItemsTemplateAssociationData.pageIndex = pageIndex;

        const document::data::PageItemsAssociationData& pageItemsAssociation =
          generatedDocument.dataItemsAssociationData.pageItemsAssociationData.at(pageIndex);
        for (int itemIndex = 0; itemIndex < pageItemsAssociation.itemsAssociationData.size(); ++itemIndex)
        {
            const document::data::ItemAssociationData& itemAssociationData =
              pageItemsAssociation.itemsAssociationData[itemIndex];

            if (itemAssociationData.templateListIndex == -1)
            {
                datadocumentassociation::data::TemplatedItemData templatedItemdata;

                templatedItemdata.itemIndex         = itemIndex;
                templatedItemdata.itemTemplateIndex = itemAssociationData.templateItemIndex;

                pageItemsTemplateAssociationData.templatedItems.push_back(templatedItemdata);
            }
        }

        generalListItemsAssociationData.generalListItemsAssociationData.push_back(
          pageItemsTemplateAssociationData);
    }

    return generalListItemsAssociationData;
}

QTEST_MAIN(Test_GeneralDataItemsAssociation)

#include "Test_GeneralDataItemsAssociation.moc"
