#include <QTest>

#include "DataDocumentAssociation/services/DataDocumentAssociationService.hpp"
#include "Document/services/DocumentGenerator.hpp"

class Test_DataDocumentAssociation : public QObject
{
    Q_OBJECT

  private slots:
    void test_generateDataDocumentAssociationData();

  private:
    document::service::documentgenerator::GeneratedDocument _generateDocument_4pages_6items_bottomRepetition();
};

void Test_DataDocumentAssociation::test_generateDataDocumentAssociationData()
{
    document::service::documentgenerator::GeneratedDocument generatedDocument =
      _generateDocument_4pages_6items_bottomRepetition();

    datadocumentassociation::data::DataDocumentAssociationData actualDataDocumentAssociationData =
      datadocumentassociation::service::generateDataDocumentAssociationData(generatedDocument);

    datadocumentassociation::data::DataDocumentAssociationData expectedDataDocumentAssociationData;

    {
        using namespace datadocumentassociation::data;

        expectedDataDocumentAssociationData = DataDocumentAssociationData {
            GeneralListItemsAssociationData {
              { PageItemsTemplateAssociationData { 0,
                                                   { TemplatedItemData { 0, 0 },
                                                     TemplatedItemData { 1, 1 },
                                                     TemplatedItemData { 2, 2 },
                                                     TemplatedItemData { 3, 3 },
                                                     TemplatedItemData { 5, 58 },
                                                     TemplatedItemData { 6, 59 } } },
                PageItemsTemplateAssociationData { 1,
                                                   { TemplatedItemData { 0, 0 },
                                                     TemplatedItemData { 1, 1 },
                                                     TemplatedItemData { 2, 2 },
                                                     TemplatedItemData { 3, 3 },
                                                     TemplatedItemData { 5, 58 },
                                                     TemplatedItemData { 6, 59 } } },
                PageItemsTemplateAssociationData { 2,
                                                   { TemplatedItemData { 0, 0 },
                                                     TemplatedItemData { 1, 1 },
                                                     TemplatedItemData { 2, 2 },
                                                     TemplatedItemData { 3, 3 },
                                                     TemplatedItemData { 5, 58 },
                                                     TemplatedItemData { 6, 59 } } },
                PageItemsTemplateAssociationData { 3,
                                                   { TemplatedItemData { 0, 0 },
                                                     TemplatedItemData { 1, 1 },
                                                     TemplatedItemData { 2, 2 },
                                                     TemplatedItemData { 3, 3 },
                                                     TemplatedItemData { 5, 34 },
                                                     TemplatedItemData { 6, 35 } } } } },
            { { 4,
                DataItemsAssociationData {
                  { PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 4 },
                                                         TemplatedItemData { 1, 5 },
                                                         TemplatedItemData { 2, 6 },
                                                         TemplatedItemData { 3, 7 },
                                                         TemplatedItemData { 4, 8 },
                                                         TemplatedItemData { 5, 9 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 10 },
                                                         TemplatedItemData { 1, 11 },
                                                         TemplatedItemData { 2, 12 },
                                                         TemplatedItemData { 3, 13 },
                                                         TemplatedItemData { 4, 14 },
                                                         TemplatedItemData { 5, 15 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 16 },
                                                         TemplatedItemData { 1, 17 },
                                                         TemplatedItemData { 2, 18 },
                                                         TemplatedItemData { 3, 19 },
                                                         TemplatedItemData { 4, 20 },
                                                         TemplatedItemData { 5, 21 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 22 },
                                                         TemplatedItemData { 1, 23 },
                                                         TemplatedItemData { 2, 24 },
                                                         TemplatedItemData { 3, 25 },
                                                         TemplatedItemData { 4, 26 },
                                                         TemplatedItemData { 5, 27 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 28 },
                                                         TemplatedItemData { 1, 29 },
                                                         TemplatedItemData { 2, 30 },
                                                         TemplatedItemData { 3, 31 },
                                                         TemplatedItemData { 4, 32 },
                                                         TemplatedItemData { 5, 33 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 34 },
                                                         TemplatedItemData { 1, 35 },
                                                         TemplatedItemData { 2, 36 },
                                                         TemplatedItemData { 3, 37 },
                                                         TemplatedItemData { 4, 38 },
                                                         TemplatedItemData { 5, 39 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 40 },
                                                         TemplatedItemData { 1, 41 },
                                                         TemplatedItemData { 2, 42 },
                                                         TemplatedItemData { 3, 43 },
                                                         TemplatedItemData { 4, 44 },
                                                         TemplatedItemData { 5, 45 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 46 },
                                                         TemplatedItemData { 1, 47 },
                                                         TemplatedItemData { 2, 48 },
                                                         TemplatedItemData { 3, 49 },
                                                         TemplatedItemData { 4, 50 },
                                                         TemplatedItemData { 5, 51 } } },
                    PageItemsTemplateAssociationData { 0,
                                                       { TemplatedItemData { 0, 52 },
                                                         TemplatedItemData { 1, 53 },
                                                         TemplatedItemData { 2, 54 },
                                                         TemplatedItemData { 3, 55 },
                                                         TemplatedItemData { 4, 56 },
                                                         TemplatedItemData { 5, 57 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 4 },
                                                         TemplatedItemData { 1, 5 },
                                                         TemplatedItemData { 2, 6 },
                                                         TemplatedItemData { 3, 7 },
                                                         TemplatedItemData { 4, 8 },
                                                         TemplatedItemData { 5, 9 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 10 },
                                                         TemplatedItemData { 1, 11 },
                                                         TemplatedItemData { 2, 12 },
                                                         TemplatedItemData { 3, 13 },
                                                         TemplatedItemData { 4, 14 },
                                                         TemplatedItemData { 5, 15 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 16 },
                                                         TemplatedItemData { 1, 17 },
                                                         TemplatedItemData { 2, 18 },
                                                         TemplatedItemData { 3, 19 },
                                                         TemplatedItemData { 4, 20 },
                                                         TemplatedItemData { 5, 21 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 22 },
                                                         TemplatedItemData { 1, 23 },
                                                         TemplatedItemData { 2, 24 },
                                                         TemplatedItemData { 3, 25 },
                                                         TemplatedItemData { 4, 26 },
                                                         TemplatedItemData { 5, 27 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 28 },
                                                         TemplatedItemData { 1, 29 },
                                                         TemplatedItemData { 2, 30 },
                                                         TemplatedItemData { 3, 31 },
                                                         TemplatedItemData { 4, 32 },
                                                         TemplatedItemData { 5, 33 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 34 },
                                                         TemplatedItemData { 1, 35 },
                                                         TemplatedItemData { 2, 36 },
                                                         TemplatedItemData { 3, 37 },
                                                         TemplatedItemData { 4, 38 },
                                                         TemplatedItemData { 5, 39 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 40 },
                                                         TemplatedItemData { 1, 41 },
                                                         TemplatedItemData { 2, 42 },
                                                         TemplatedItemData { 3, 43 },
                                                         TemplatedItemData { 4, 44 },
                                                         TemplatedItemData { 5, 45 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 46 },
                                                         TemplatedItemData { 1, 47 },
                                                         TemplatedItemData { 2, 48 },
                                                         TemplatedItemData { 3, 49 },
                                                         TemplatedItemData { 4, 50 },
                                                         TemplatedItemData { 5, 51 } } },
                    PageItemsTemplateAssociationData { 1,
                                                       { TemplatedItemData { 0, 52 },
                                                         TemplatedItemData { 1, 53 },
                                                         TemplatedItemData { 2, 54 },
                                                         TemplatedItemData { 3, 55 },
                                                         TemplatedItemData { 4, 56 },
                                                         TemplatedItemData { 5, 57 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 4 },
                                                         TemplatedItemData { 1, 5 },
                                                         TemplatedItemData { 2, 6 },
                                                         TemplatedItemData { 3, 7 },
                                                         TemplatedItemData { 4, 8 },
                                                         TemplatedItemData { 5, 9 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 10 },
                                                         TemplatedItemData { 1, 11 },
                                                         TemplatedItemData { 2, 12 },
                                                         TemplatedItemData { 3, 13 },
                                                         TemplatedItemData { 4, 14 },
                                                         TemplatedItemData { 5, 15 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 16 },
                                                         TemplatedItemData { 1, 17 },
                                                         TemplatedItemData { 2, 18 },
                                                         TemplatedItemData { 3, 19 },
                                                         TemplatedItemData { 4, 20 },
                                                         TemplatedItemData { 5, 21 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 22 },
                                                         TemplatedItemData { 1, 23 },
                                                         TemplatedItemData { 2, 24 },
                                                         TemplatedItemData { 3, 25 },
                                                         TemplatedItemData { 4, 26 },
                                                         TemplatedItemData { 5, 27 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 28 },
                                                         TemplatedItemData { 1, 29 },
                                                         TemplatedItemData { 2, 30 },
                                                         TemplatedItemData { 3, 31 },
                                                         TemplatedItemData { 4, 32 },
                                                         TemplatedItemData { 5, 33 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 34 },
                                                         TemplatedItemData { 1, 35 },
                                                         TemplatedItemData { 2, 36 },
                                                         TemplatedItemData { 3, 37 },
                                                         TemplatedItemData { 4, 38 },
                                                         TemplatedItemData { 5, 39 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 40 },
                                                         TemplatedItemData { 1, 41 },
                                                         TemplatedItemData { 2, 42 },
                                                         TemplatedItemData { 3, 43 },
                                                         TemplatedItemData { 4, 44 },
                                                         TemplatedItemData { 5, 45 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 46 },
                                                         TemplatedItemData { 1, 47 },
                                                         TemplatedItemData { 2, 48 },
                                                         TemplatedItemData { 3, 49 },
                                                         TemplatedItemData { 4, 50 },
                                                         TemplatedItemData { 5, 51 } } },
                    PageItemsTemplateAssociationData { 2,
                                                       { TemplatedItemData { 0, 52 },
                                                         TemplatedItemData { 1, 53 },
                                                         TemplatedItemData { 2, 54 },
                                                         TemplatedItemData { 3, 55 },
                                                         TemplatedItemData { 4, 56 },
                                                         TemplatedItemData { 5, 57 } } },
                    PageItemsTemplateAssociationData { 3,
                                                       { TemplatedItemData { 0, 4 },
                                                         TemplatedItemData { 1, 5 },
                                                         TemplatedItemData { 2, 6 },
                                                         TemplatedItemData { 3, 7 },
                                                         TemplatedItemData { 4, 8 },
                                                         TemplatedItemData { 5, 9 } } },
                    PageItemsTemplateAssociationData { 3,
                                                       { TemplatedItemData { 0, 10 },
                                                         TemplatedItemData { 1, 11 },
                                                         TemplatedItemData { 2, 12 },
                                                         TemplatedItemData { 3, 13 },
                                                         TemplatedItemData { 4, 14 },
                                                         TemplatedItemData { 5, 15 } } },
                    PageItemsTemplateAssociationData { 3,
                                                       { TemplatedItemData { 0, 16 },
                                                         TemplatedItemData { 1, 17 },
                                                         TemplatedItemData { 2, 18 },
                                                         TemplatedItemData { 3, 19 },
                                                         TemplatedItemData { 4, 20 },
                                                         TemplatedItemData { 5, 21 } } },
                    PageItemsTemplateAssociationData { 3,
                                                       { TemplatedItemData { 0, 22 },
                                                         TemplatedItemData { 1, 23 },
                                                         TemplatedItemData { 2, 24 },
                                                         TemplatedItemData { 3, 25 },
                                                         TemplatedItemData { 4, 26 },
                                                         TemplatedItemData { 5, 27 } } },
                    PageItemsTemplateAssociationData { 3,
                                                       { TemplatedItemData { 0, 28 },
                                                         TemplatedItemData { 1, 29 },
                                                         TemplatedItemData { 2, 30 },
                                                         TemplatedItemData { 3, 31 },
                                                         TemplatedItemData { 4, 32 },
                                                         TemplatedItemData { 5, 33 } } } } } } }
        };
    }

    QCOMPARE(actualDataDocumentAssociationData.generalListItemsAssociationData,
             expectedDataDocumentAssociationData.generalListItemsAssociationData);

    qDebug() << actualDataDocumentAssociationData;

    QCOMPARE(actualDataDocumentAssociationData, expectedDataDocumentAssociationData);
}

document::service::documentgenerator::GeneratedDocument
Test_DataDocumentAssociation::_generateDocument_4pages_6items_bottomRepetition()
{
    using namespace document::data;
    using namespace documenttemplate::data;
    using namespace painter::data;

    return document::service::documentgenerator::GeneratedDocument {
        DocumentData {
          QList<painter::data::PaintableItemListModelData> {
            PaintableItemListModelData { QList<VariantListableItemData> {
              PaintableRectangleData {
                RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData { ColorData { 100, "#ff000000" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                    ColorData { 90, "#ff0000ff" },
                                    PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                                  "The company address",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "The client address",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 110, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 120, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 160, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 180, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Bell peppers 1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 130, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 150, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 220, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 230, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 270, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 290, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Eggs 2",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 240, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 260, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "0.34",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 330, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 340, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 380, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 400, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 350, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 370, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 440, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 450, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 490, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 510, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 4",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 460, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 480, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 550, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 560, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 600, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 620, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 570, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 590, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 660, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 670, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 710, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 730, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 6",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 680, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 700, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 770, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 780, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 820, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 840, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 7",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 790, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 810, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 880, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 890, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 930, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 950, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 8",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 900, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 920, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 990, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 1000, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 1040, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 1060, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 9",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 1010, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 1030, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
                ColorData { 100, "#ff808080" },
                PaintableBorderData { 0, ColorData { 100, "#ffffff00" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "Total",
                "",
                0,
                0 } } },
            PaintableItemListModelData { QList<VariantListableItemData> {
              PaintableRectangleData {
                RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData { ColorData { 100, "#ff000000" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                    ColorData { 90, "#ff0000ff" },
                                    PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                                  "The company address",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "The client address",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 110, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 120, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 160, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 180, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Bell peppers 10",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 130, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 150, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 220, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 230, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 270, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 290, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Eggs 11",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 240, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 260, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "0.34",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 330, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 340, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 380, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 400, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 350, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 370, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 440, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 450, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 490, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 510, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 13",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 460, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 480, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 550, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 560, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 600, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 620, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 14",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 570, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 590, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 660, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 670, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 710, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 730, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 15",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 680, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 700, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 770, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 780, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 820, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 840, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 16",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 790, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 810, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 880, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 890, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 930, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 950, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 17",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 900, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 920, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 990, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 1000, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 1040, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 1060, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 18",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 1010, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 1030, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
                ColorData { 100, "#ff808080" },
                PaintableBorderData { 0, ColorData { 100, "#ffffff00" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "Total",
                "",
                0,
                0 } } },
            PaintableItemListModelData { QList<VariantListableItemData> {
              PaintableRectangleData {
                RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData { ColorData { 100, "#ff000000" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                    ColorData { 90, "#ff0000ff" },
                                    PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                                  "The company address",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "The client address",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 110, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 120, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 160, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 180, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Bell peppers 19",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 130, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 150, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 220, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 230, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 270, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 290, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Eggs 20",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 240, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 260, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "0.34",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 330, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 340, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 380, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 400, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 350, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 370, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 440, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 450, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 490, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 510, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 21",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 460, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 480, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 550, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 560, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 600, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 620, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 22",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 570, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 590, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 660, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 670, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 710, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 730, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 23",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 680, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 700, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 770, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 780, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 820, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 840, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 24",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 790, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 810, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 880, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 890, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 930, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 950, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 25",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 900, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 920, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 990, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 1000, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 1040, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 1060, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 26",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 1010, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 1030, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
                ColorData { 100, "#ff808080" },
                PaintableBorderData { 0, ColorData { 100, "#ffffff00" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "Total",
                "",
                0,
                0 } } },
            PaintableItemListModelData { QList<VariantListableItemData> {
              PaintableRectangleData {
                RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData { ColorData { 100, "#ff000000" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                    ColorData { 90, "#ff0000ff" },
                                    PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                                  "The company address",
                                  "",
                                  0,
                                  0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "The client address",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 110, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 120, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 160, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 180, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Brocoli 27",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 130, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 150, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 220, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 230, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "3",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 270, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 290, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Potatoes 28",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 240, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 260, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2.5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 330, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 340, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 380, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 400, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Greens 29",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 350, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 370, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.75",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 440, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 450, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "2",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 490, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 510, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Bell peppers 30",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 460, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 480, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "1.99",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 33, 550, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 53, 560, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "5",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 253, 600, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 273, 620, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "Eggs 31",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 673, 570, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "#ffffc0cb" },
                PaintableBorderData { 0, ColorData { 100, "#ff000000" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 693, 590, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "#ff000000" },
                  PaintableBorderData { 1, ColorData { 100, "#ff000000" } } },
                "0.34",
                "",
                0,
                0 },
              PaintableRectangleData {
                RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
                ColorData { 100, "#ff808080" },
                PaintableBorderData { 0, ColorData { 100, "#ffffff00" } } },
              PaintableTextData {
                ColorData { 100, "#ff000000" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "#ff0000ff" },
                  PaintableBorderData { 0, ColorData { 0, "#ff000000" } } },
                "Total",
                "",
                0,
                0 } } } },
          PageSizeData { QPageSize { QSizeF { 210, 297 }, QPageSize::Unit(0), "A4", QPageSize::ExactMatch },
                         1200,
                         PageSizeData::Unit(0) } },
        DataItemsAssociationData { QList<PageItemsAssociationData> {
          PageItemsAssociationData { QList<ItemAssociationData> {
            ItemAssociationData { -1, 0, -1, 0 }, ItemAssociationData { -1, 1, -1, 0 },
            ItemAssociationData { -1, 2, -1, 0 }, ItemAssociationData { -1, 3, -1, 0 },
            ItemAssociationData { 4, 0, 0, 0 },   ItemAssociationData { 4, 1, 0, 0 },
            ItemAssociationData { 4, 2, 0, 0 },   ItemAssociationData { 4, 3, 0, 0 },
            ItemAssociationData { 4, 4, 0, 0 },   ItemAssociationData { 4, 5, 0, 0 },
            ItemAssociationData { 4, 0, 0, 1 },   ItemAssociationData { 4, 1, 0, 1 },
            ItemAssociationData { 4, 2, 0, 1 },   ItemAssociationData { 4, 3, 0, 1 },
            ItemAssociationData { 4, 4, 0, 1 },   ItemAssociationData { 4, 5, 0, 1 },
            ItemAssociationData { 4, 0, 0, 2 },   ItemAssociationData { 4, 1, 0, 2 },
            ItemAssociationData { 4, 2, 0, 2 },   ItemAssociationData { 4, 3, 0, 2 },
            ItemAssociationData { 4, 4, 0, 2 },   ItemAssociationData { 4, 5, 0, 2 },
            ItemAssociationData { 4, 0, 0, 3 },   ItemAssociationData { 4, 1, 0, 3 },
            ItemAssociationData { 4, 2, 0, 3 },   ItemAssociationData { 4, 3, 0, 3 },
            ItemAssociationData { 4, 4, 0, 3 },   ItemAssociationData { 4, 5, 0, 3 },
            ItemAssociationData { 4, 0, 0, 4 },   ItemAssociationData { 4, 1, 0, 4 },
            ItemAssociationData { 4, 2, 0, 4 },   ItemAssociationData { 4, 3, 0, 4 },
            ItemAssociationData { 4, 4, 0, 4 },   ItemAssociationData { 4, 5, 0, 4 },
            ItemAssociationData { 4, 0, 0, 5 },   ItemAssociationData { 4, 1, 0, 5 },
            ItemAssociationData { 4, 2, 0, 5 },   ItemAssociationData { 4, 3, 0, 5 },
            ItemAssociationData { 4, 4, 0, 5 },   ItemAssociationData { 4, 5, 0, 5 },
            ItemAssociationData { 4, 0, 0, 6 },   ItemAssociationData { 4, 1, 0, 6 },
            ItemAssociationData { 4, 2, 0, 6 },   ItemAssociationData { 4, 3, 0, 6 },
            ItemAssociationData { 4, 4, 0, 6 },   ItemAssociationData { 4, 5, 0, 6 },
            ItemAssociationData { 4, 0, 0, 7 },   ItemAssociationData { 4, 1, 0, 7 },
            ItemAssociationData { 4, 2, 0, 7 },   ItemAssociationData { 4, 3, 0, 7 },
            ItemAssociationData { 4, 4, 0, 7 },   ItemAssociationData { 4, 5, 0, 7 },
            ItemAssociationData { 4, 0, 0, 8 },   ItemAssociationData { 4, 1, 0, 8 },
            ItemAssociationData { 4, 2, 0, 8 },   ItemAssociationData { 4, 3, 0, 8 },
            ItemAssociationData { 4, 4, 0, 8 },   ItemAssociationData { 4, 5, 0, 8 },
            ItemAssociationData { -1, 5, -1, 0 }, ItemAssociationData { -1, 6, -1, 0 } } },
          PageItemsAssociationData { QList<ItemAssociationData> {
            ItemAssociationData { -1, 0, -1, 0 }, ItemAssociationData { -1, 1, -1, 0 },
            ItemAssociationData { -1, 2, -1, 0 }, ItemAssociationData { -1, 3, -1, 0 },
            ItemAssociationData { 4, 0, 0, 9 },   ItemAssociationData { 4, 1, 0, 9 },
            ItemAssociationData { 4, 2, 0, 9 },   ItemAssociationData { 4, 3, 0, 9 },
            ItemAssociationData { 4, 4, 0, 9 },   ItemAssociationData { 4, 5, 0, 9 },
            ItemAssociationData { 4, 0, 0, 10 },  ItemAssociationData { 4, 1, 0, 10 },
            ItemAssociationData { 4, 2, 0, 10 },  ItemAssociationData { 4, 3, 0, 10 },
            ItemAssociationData { 4, 4, 0, 10 },  ItemAssociationData { 4, 5, 0, 10 },
            ItemAssociationData { 4, 0, 0, 11 },  ItemAssociationData { 4, 1, 0, 11 },
            ItemAssociationData { 4, 2, 0, 11 },  ItemAssociationData { 4, 3, 0, 11 },
            ItemAssociationData { 4, 4, 0, 11 },  ItemAssociationData { 4, 5, 0, 11 },
            ItemAssociationData { 4, 0, 0, 12 },  ItemAssociationData { 4, 1, 0, 12 },
            ItemAssociationData { 4, 2, 0, 12 },  ItemAssociationData { 4, 3, 0, 12 },
            ItemAssociationData { 4, 4, 0, 12 },  ItemAssociationData { 4, 5, 0, 12 },
            ItemAssociationData { 4, 0, 0, 13 },  ItemAssociationData { 4, 1, 0, 13 },
            ItemAssociationData { 4, 2, 0, 13 },  ItemAssociationData { 4, 3, 0, 13 },
            ItemAssociationData { 4, 4, 0, 13 },  ItemAssociationData { 4, 5, 0, 13 },
            ItemAssociationData { 4, 0, 0, 14 },  ItemAssociationData { 4, 1, 0, 14 },
            ItemAssociationData { 4, 2, 0, 14 },  ItemAssociationData { 4, 3, 0, 14 },
            ItemAssociationData { 4, 4, 0, 14 },  ItemAssociationData { 4, 5, 0, 14 },
            ItemAssociationData { 4, 0, 0, 15 },  ItemAssociationData { 4, 1, 0, 15 },
            ItemAssociationData { 4, 2, 0, 15 },  ItemAssociationData { 4, 3, 0, 15 },
            ItemAssociationData { 4, 4, 0, 15 },  ItemAssociationData { 4, 5, 0, 15 },
            ItemAssociationData { 4, 0, 0, 16 },  ItemAssociationData { 4, 1, 0, 16 },
            ItemAssociationData { 4, 2, 0, 16 },  ItemAssociationData { 4, 3, 0, 16 },
            ItemAssociationData { 4, 4, 0, 16 },  ItemAssociationData { 4, 5, 0, 16 },
            ItemAssociationData { 4, 0, 0, 17 },  ItemAssociationData { 4, 1, 0, 17 },
            ItemAssociationData { 4, 2, 0, 17 },  ItemAssociationData { 4, 3, 0, 17 },
            ItemAssociationData { 4, 4, 0, 17 },  ItemAssociationData { 4, 5, 0, 17 },
            ItemAssociationData { -1, 5, -1, 0 }, ItemAssociationData { -1, 6, -1, 0 } } },
          PageItemsAssociationData { QList<ItemAssociationData> {
            ItemAssociationData { -1, 0, -1, 0 }, ItemAssociationData { -1, 1, -1, 0 },
            ItemAssociationData { -1, 2, -1, 0 }, ItemAssociationData { -1, 3, -1, 0 },
            ItemAssociationData { 4, 0, 0, 18 },  ItemAssociationData { 4, 1, 0, 18 },
            ItemAssociationData { 4, 2, 0, 18 },  ItemAssociationData { 4, 3, 0, 18 },
            ItemAssociationData { 4, 4, 0, 18 },  ItemAssociationData { 4, 5, 0, 18 },
            ItemAssociationData { 4, 0, 0, 19 },  ItemAssociationData { 4, 1, 0, 19 },
            ItemAssociationData { 4, 2, 0, 19 },  ItemAssociationData { 4, 3, 0, 19 },
            ItemAssociationData { 4, 4, 0, 19 },  ItemAssociationData { 4, 5, 0, 19 },
            ItemAssociationData { 4, 0, 0, 20 },  ItemAssociationData { 4, 1, 0, 20 },
            ItemAssociationData { 4, 2, 0, 20 },  ItemAssociationData { 4, 3, 0, 20 },
            ItemAssociationData { 4, 4, 0, 20 },  ItemAssociationData { 4, 5, 0, 20 },
            ItemAssociationData { 4, 0, 0, 21 },  ItemAssociationData { 4, 1, 0, 21 },
            ItemAssociationData { 4, 2, 0, 21 },  ItemAssociationData { 4, 3, 0, 21 },
            ItemAssociationData { 4, 4, 0, 21 },  ItemAssociationData { 4, 5, 0, 21 },
            ItemAssociationData { 4, 0, 0, 22 },  ItemAssociationData { 4, 1, 0, 22 },
            ItemAssociationData { 4, 2, 0, 22 },  ItemAssociationData { 4, 3, 0, 22 },
            ItemAssociationData { 4, 4, 0, 22 },  ItemAssociationData { 4, 5, 0, 22 },
            ItemAssociationData { 4, 0, 0, 23 },  ItemAssociationData { 4, 1, 0, 23 },
            ItemAssociationData { 4, 2, 0, 23 },  ItemAssociationData { 4, 3, 0, 23 },
            ItemAssociationData { 4, 4, 0, 23 },  ItemAssociationData { 4, 5, 0, 23 },
            ItemAssociationData { 4, 0, 0, 24 },  ItemAssociationData { 4, 1, 0, 24 },
            ItemAssociationData { 4, 2, 0, 24 },  ItemAssociationData { 4, 3, 0, 24 },
            ItemAssociationData { 4, 4, 0, 24 },  ItemAssociationData { 4, 5, 0, 24 },
            ItemAssociationData { 4, 0, 0, 25 },  ItemAssociationData { 4, 1, 0, 25 },
            ItemAssociationData { 4, 2, 0, 25 },  ItemAssociationData { 4, 3, 0, 25 },
            ItemAssociationData { 4, 4, 0, 25 },  ItemAssociationData { 4, 5, 0, 25 },
            ItemAssociationData { 4, 0, 0, 26 },  ItemAssociationData { 4, 1, 0, 26 },
            ItemAssociationData { 4, 2, 0, 26 },  ItemAssociationData { 4, 3, 0, 26 },
            ItemAssociationData { 4, 4, 0, 26 },  ItemAssociationData { 4, 5, 0, 26 },
            ItemAssociationData { -1, 5, -1, 0 }, ItemAssociationData { -1, 6, -1, 0 } } },
          PageItemsAssociationData { QList<ItemAssociationData> {
            ItemAssociationData { -1, 0, -1, 0 }, ItemAssociationData { -1, 1, -1, 0 },
            ItemAssociationData { -1, 2, -1, 0 }, ItemAssociationData { -1, 3, -1, 0 },
            ItemAssociationData { 4, 0, 0, 27 },  ItemAssociationData { 4, 1, 0, 27 },
            ItemAssociationData { 4, 2, 0, 27 },  ItemAssociationData { 4, 3, 0, 27 },
            ItemAssociationData { 4, 4, 0, 27 },  ItemAssociationData { 4, 5, 0, 27 },
            ItemAssociationData { 4, 0, 0, 28 },  ItemAssociationData { 4, 1, 0, 28 },
            ItemAssociationData { 4, 2, 0, 28 },  ItemAssociationData { 4, 3, 0, 28 },
            ItemAssociationData { 4, 4, 0, 28 },  ItemAssociationData { 4, 5, 0, 28 },
            ItemAssociationData { 4, 0, 0, 29 },  ItemAssociationData { 4, 1, 0, 29 },
            ItemAssociationData { 4, 2, 0, 29 },  ItemAssociationData { 4, 3, 0, 29 },
            ItemAssociationData { 4, 4, 0, 29 },  ItemAssociationData { 4, 5, 0, 29 },
            ItemAssociationData { 4, 0, 0, 30 },  ItemAssociationData { 4, 1, 0, 30 },
            ItemAssociationData { 4, 2, 0, 30 },  ItemAssociationData { 4, 3, 0, 30 },
            ItemAssociationData { 4, 4, 0, 30 },  ItemAssociationData { 4, 5, 0, 30 },
            ItemAssociationData { 4, 0, 0, 31 },  ItemAssociationData { 4, 1, 0, 31 },
            ItemAssociationData { 4, 2, 0, 31 },  ItemAssociationData { 4, 3, 0, 31 },
            ItemAssociationData { 4, 4, 0, 31 },  ItemAssociationData { 4, 5, 0, 31 },
            ItemAssociationData { -1, 5, -1, 0 }, ItemAssociationData { -1, 6, -1, 0 } } } } }
    };
}

QTEST_MAIN(Test_DataDocumentAssociation)

#include "Test_DataDocumentAssociationService.moc"
