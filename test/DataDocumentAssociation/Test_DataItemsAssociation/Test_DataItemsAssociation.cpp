#include <QSignalSpy>
#include <QTest>

#include "DataDocumentAssociation/controllers/DataItemsAssociation.hpp"
#include "DataDocumentAssociation/data/DataItemsAssociationData.hpp"
#include "Document/controllers/Document.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/data/DataTablesData.hpp"
#include "Document/data/DocumentData.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "Painter/controllers/PaintableText.hpp"
#include "Painter/data/PaintableListData.hpp"

constexpr int PAGE_NUMBER = 3;
constexpr int ROW_NUMBER  = 4;

class Test_DataItemsAssociation : public QObject
{
    Q_OBJECT

  public:
    Test_DataItemsAssociation()           = default;
    ~Test_DataItemsAssociation() override = default;

  private slots:
    void test_objectData();
    void test_setObjectData();
    void test_onTableDataChanged_data();
    void test_onTableDataChanged();
    void test_onTableRowsInserted_data();
    void test_onTableRowsInserted();
    void test_onTableRowsRemoved_data();
    void test_onTableRowsRemoved();

  private:
    Q_DISABLE_COPY_MOVE(Test_DataItemsAssociation)

    documentdatadefinitions::data::DataDefinitionListData _listDataDefinitionListData() const;
    documentdatadefinitions::data::DataTablesData _listDataTablesData() const;
    documentdatadefinitions::data::DataDefinitionListData _generalDataDefinitionListData() const;
    documentdatadefinitions::data::DataTablesData _generalDataTablesData() const;
    documenttemplate::data::DocumentTemplateData _documentTemplateData() const;
    document::data::DocumentData _documentData() const;
    painter::data::PaintableListData _paintableListTemplate() const;
    datadocumentassociation::data::DataItemsAssociationData _dataItemsAssociationData() const;
};

struct DataIndex
{
    int row;
    int column;

    bool operator==(const DataIndex&) const = default;
};

struct ItemIndex
{
    int page;
    int itemIndex;

    bool operator==(const ItemIndex&) const = default;
};

struct DataTableData
{
    DataIndex itemIndex;
    QString data;
};

inline size_t qHash(const ItemIndex& key, size_t seed)
{
    return qHashMulti(seed, key.page, key.itemIndex);
}

void Test_DataItemsAssociation::test_objectData()
{
    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData =
      _listDataDefinitionListData();

    documentdatadefinitions::data::DataTablesData dataTablesData = _listDataTablesData();

    document::data::DocumentData documentData = _documentData();

    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData = _dataItemsAssociationData();

    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel(dataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel dataTablesModel(&dataDefinitionListModel, dataTablesData, this);
    document::controller::Document document(documentData);

    datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation;
    dataItemsAssociation.setListDataTablesModel(&dataTablesModel);
    dataItemsAssociation.setDocument(&document);
    dataItemsAssociation.setDocumentTemplateData(_documentTemplateData());
    dataItemsAssociation.setPaintableListTemplateIndex(3);

    for (int rowIndex = 0; rowIndex < dataItemsAssociationData.rows.size(); ++rowIndex)
    {
        const datadocumentassociation::data::PageItemsTemplateAssociationData& dataRowItemsAssociationData =
          dataItemsAssociationData.rows.at(rowIndex);
        dataItemsAssociation.insertDataRowItemsAssociationData(rowIndex, dataRowItemsAssociationData);
    }

    QCOMPARE(dataItemsAssociation.objectData(), dataItemsAssociationData);
}

void Test_DataItemsAssociation::test_setObjectData()
{
    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData =
      _listDataDefinitionListData();

    documentdatadefinitions::data::DataTablesData dataTablesData = _listDataTablesData();

    document::data::DocumentData documentData = _documentData();

    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData = _dataItemsAssociationData();

    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel(dataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel listDataTablesModel(&dataDefinitionListModel, dataTablesData, this);
    document::controller::Document document(documentData);

    {
        datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation(
          &document, nullptr, &listDataTablesModel, _documentTemplateData(), 3, dataItemsAssociationData);

        for (int rowIndex = 0; rowIndex < dataItemsAssociationData.rows.size(); ++rowIndex)
        {
            QCOMPARE(dataItemsAssociation.dataRowItemsAssociationData(rowIndex),
                     dataItemsAssociationData.rows.at(rowIndex));
        }
    }

    {
        datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation;
        dataItemsAssociation.setListDataTablesModel(&listDataTablesModel);
        dataItemsAssociation.setDocument(&document);
        dataItemsAssociation.setDocumentTemplateData(_documentTemplateData());
        dataItemsAssociation.setPaintableListTemplateIndex(3);
        dataItemsAssociation.setObjectData(dataItemsAssociationData);

        for (int rowIndex = 0; rowIndex < dataItemsAssociationData.rows.size(); ++rowIndex)
        {
            QCOMPARE(dataItemsAssociation.dataRowItemsAssociationData(rowIndex),
                     dataItemsAssociationData.rows.at(rowIndex));
        }
    }
}

void Test_DataItemsAssociation::test_onTableDataChanged_data()
{
    QTest::addColumn<DataIndex>("changedDataIndex");
    QTest::addColumn<QString>("newData");
    QTest::addColumn<QHash<ItemIndex, QString>>("updatedTexts");

    {
        int pageIndex = 0;
        {
            int rowIndex  = 0;
            int dataIndex = 0;
            QTest::addRow("data_page0_row0_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page0_row0_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page0_row0_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }

        {
            int rowIndex  = 2;
            int dataIndex = 2;
            QTest::addRow("data_page0_row2_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page0_row2_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page0_row2_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }
    }

    {
        int pageIndex = 1;

        {
            int rowIndex  = 1;
            int dataIndex = 5;
            QTest::addRow("data_page1_row1_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page1_row1_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page1_row1_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }

        {
            int rowIndex  = 3;
            int dataIndex = 7;
            QTest::addRow("data_page1_row3_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page1_row3_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page1_row3_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }
    }

    {
        int pageIndex = 2;
        {
            int rowIndex  = 1;
            int dataIndex = 9;
            QTest::addRow("data_page2_row1_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row1_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row1_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }

        {
            int rowIndex  = 2;
            int dataIndex = 10;
            QTest::addRow("data_page2_row2_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row2_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row2_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }

        {
            int rowIndex  = 3;
            int dataIndex = 11;
            QTest::addRow("data_page2_row3_0")
              << DataIndex { dataIndex, 0 } << "New date"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 2 + 8 * rowIndex }, "A date : New date" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (New date,Old product %1,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row3_1")
              << DataIndex { dataIndex, 1 } << "New product"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 4 + 8 * rowIndex }, "A product : New product" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,New product,Old count %1)").arg(dataIndex) }
                 };

            QTest::addRow("data_page2_row3_2")
              << DataIndex { dataIndex, 2 } << "New count"
              << QHash<ItemIndex, QString> {
                     { ItemIndex { pageIndex, 6 + 8 * rowIndex }, "Count : New count" },
                     { ItemIndex { pageIndex, 8 + 8 * rowIndex },
                       QStringLiteral("Combined (Old date %1,Old product %1,New count)").arg(dataIndex) }
                 };
        }
    }
}

void Test_DataItemsAssociation::test_onTableDataChanged()
{
    using ItemHash = QHash<ItemIndex, QString>;

    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData =
      _listDataDefinitionListData();

    documentdatadefinitions::data::DataTablesData dataTablesData = _listDataTablesData();

    document::data::DocumentData documentData = _documentData();

    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData = _dataItemsAssociationData();

    QFETCH(DataIndex, changedDataIndex);
    QFETCH(QString, newData);
    QFETCH(ItemHash, updatedTexts);

    documentdatadefinitions::controller::DataDefinitionListModel dataDefinitionListModel(dataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel dataTablesModel(&dataDefinitionListModel, dataTablesData, this);
    document::controller::Document document(documentData);

    datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation;
    dataItemsAssociation.setListDataTablesModel(&dataTablesModel);
    dataItemsAssociation.setDocument(&document);
    dataItemsAssociation.setDocumentTemplateData(_documentTemplateData());
    dataItemsAssociation.setPaintableListTemplateIndex(3);
    dataItemsAssociation.setObjectData(dataItemsAssociationData);

    std::vector<std::unique_ptr<QSignalSpy>> oldTextChangedSpys;
    std::vector<std::unique_ptr<QSignalSpy>> newTextChangedSpys;

    for (int pageIndex = 0; pageIndex < document.rowCount(); ++pageIndex)
    {
        const painter::controller::ListableItemListModel* const listableItemList = document.at(pageIndex);

        for (int itemIndex = 0; itemIndex < listableItemList->rowCount(); ++itemIndex)
        {
            if (const auto* const textItem =
                  qobject_cast<const painter::controller::PaintableText* const>(listableItemList->at(itemIndex)))
            {
                ItemIndex itemIndexValue { pageIndex, itemIndex };
                if (!updatedTexts.contains(itemIndexValue))
                {
                    oldTextChangedSpys.emplace_back(
                      std::make_unique<QSignalSpy>(textItem, &painter::controller::PaintableText::textChanged));
                }
                else
                {
                    newTextChangedSpys.emplace_back(
                      std::make_unique<QSignalSpy>(textItem, &painter::controller::PaintableText::textChanged));
                }
            }
        }
    }

    dataTablesModel.setData(dataTablesModel.index(changedDataIndex.row, changedDataIndex.column), newData);

    for (const auto& spy : oldTextChangedSpys) { QCOMPARE(spy->count(), 0); }
    for (const auto& spy : newTextChangedSpys) { QCOMPARE(spy->count(), 1); }

    for (int pageIndex = 0; pageIndex < document.rowCount(); ++pageIndex)
    {
        const painter::controller::ListableItemListModel* const listableItemList = document.at(pageIndex);

        for (int itemIndex = 0; itemIndex < listableItemList->rowCount(); ++itemIndex)
        {
            if (const auto* const textItem =
                  qobject_cast<const painter::controller::PaintableText* const>(listableItemList->at(itemIndex)))
            {
                if (!updatedTexts.contains(ItemIndex { pageIndex, itemIndex }))
                {
                    auto sourceTextData = std::get<painter::data::PaintableTextData>(
                      documentData.pageItemsList.at(pageIndex).paintableItemList.at(itemIndex));
                    QCOMPARE(textItem->text(), sourceTextData.text);
                }
                else { QCOMPARE(textItem->text(), updatedTexts.value(ItemIndex { pageIndex, itemIndex })); }
            }
        }
    }
}

void Test_DataItemsAssociation::test_onTableRowsInserted_data()
{
    QTest::addColumn<documentdatadefinitions::data::DataTablesData>("listDataTablesData");
    QTest::addColumn<document::data::DocumentData>("documentData");
    QTest::addColumn<datadocumentassociation::data::DataItemsAssociationData>("dataItemsAssociationData");
    QTest::addColumn<QList<DataTableData>>("dataTablesData");
    QTest::addColumn<document::data::DocumentData>("expectedDocumentData");

    {
        documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                = _documentData();
        datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();

        using namespace painter::data;
        expectedDocumentData.pageItemsList.push_back(painter::data::PaintableItemListModelData {
          { PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } },

            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
              QStringLiteral("A general variable : Somewhere over the rainbow, 10/10/2022") },

            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },

            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } } },
              QStringLiteral("A date : New date") },
            PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } } },
              QStringLiteral("A product : New product") },
            PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } } },
              QStringLiteral("Count : New count") },
            PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } } },
              QStringLiteral("Combined (New date,New product,New count)") },

            PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } } } });

        QTest::addRow("newPage") << listDataTablesData << documentData << dataItemsAssociationData
                                 << QList<DataTableData> { DataTableData { DataIndex { 12, 0 }, "New date" },
                                                           DataTableData { DataIndex { 12, 1 }, "New product" },
                                                           DataTableData { DataIndex { 12, 2 }, "New count" } }
                                 << expectedDocumentData;
    }

    {
        documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                = _documentData();
        datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();

        listDataTablesData.dataTables.push_back(documentdatadefinitions::data::DataTableData {
          { 0, QStringLiteral("Old date 12") },
          { 1, QStringLiteral("Old product 12") },
          { 2, QStringLiteral("Old count 12") },
        });

        listDataTablesData.dataTables.push_back(documentdatadefinitions::data::DataTableData {
          { 0, QStringLiteral("Old date 13") },
          { 1, QStringLiteral("Old product 13") },
          { 2, QStringLiteral("Old count 13") },
        });

        using namespace painter::data;

        documentData.pageItemsList.push_back(painter::data::PaintableItemListModelData { {
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } },

          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
            QStringLiteral("A general variable : Somewhere over the rainbow, 10/10/2022") },

          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },

          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } } },
            QStringLiteral("A date : Old date 12") },
          PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } } },
            QStringLiteral("A product : Old product 12") },
          PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } } },
            QStringLiteral("Count : Old count 12") },
          PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } } },
            QStringLiteral("Combined (Old date 12,Old product 12,Old count 12)") },

          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000 + 3843, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000 + 3843, 2280, 3843 } } },
            QStringLiteral("A date : Old date 13") },
          PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000 + 3843, 2280, 3843 } } },
            QStringLiteral("A product : Old product 13") },
          PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000 + 3843, 2280, 3843 } } },
            QStringLiteral("Count : Old count 13") },
          PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000 + 3843, 2280, 3843 } } },
            QStringLiteral("Combined (Old date 13,Old product 13,Old count 13)") },

        } });

        expectedDocumentData = documentData;

        expectedDocumentData.pageItemsList.last().paintableItemList.append({
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000 + 2 * 3843, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },

          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000 + 2 * 3843, 2280, 3843 } } },
            QStringLiteral("A date : New date") },
          PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000 + 2 * 3843, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 2605, 1000 + 2 * 3843, 2280, 3843 } } },
                              QStringLiteral("A product : New product") },
          PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000 + 2 * 3843, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 4885, 1000 + 2 * 3843, 2280, 3843 } } },
                              QStringLiteral("Count : New count") },
          PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000 + 2 * 3843, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 7165, 1000 + 2 * 3843, 2280, 3843 } } },
                              QStringLiteral("Combined (New date,New product,New count)") },
        });

        documentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        expectedDocumentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        dataItemsAssociationData.rows.append(datadocumentassociation::data::PageItemsTemplateAssociationData {
          3,
          { datadocumentassociation::data::TemplatedItemData { 0, 2 },
            datadocumentassociation::data::TemplatedItemData { 1, 3 },
            datadocumentassociation::data::TemplatedItemData { 2, 4 },
            datadocumentassociation::data::TemplatedItemData { 3, 5 },
            datadocumentassociation::data::TemplatedItemData { 4, 6 },
            datadocumentassociation::data::TemplatedItemData { 5, 7 },
            datadocumentassociation::data::TemplatedItemData { 6, 8 },
            datadocumentassociation::data::TemplatedItemData { 7, 9 } } });

        dataItemsAssociationData.rows.append(datadocumentassociation::data::PageItemsTemplateAssociationData {
          3,
          { datadocumentassociation::data::TemplatedItemData { 0, 10 },
            datadocumentassociation::data::TemplatedItemData { 1, 11 },
            datadocumentassociation::data::TemplatedItemData { 2, 12 },
            datadocumentassociation::data::TemplatedItemData { 3, 13 },
            datadocumentassociation::data::TemplatedItemData { 4, 14 },
            datadocumentassociation::data::TemplatedItemData { 5, 15 },
            datadocumentassociation::data::TemplatedItemData { 6, 16 },
            datadocumentassociation::data::TemplatedItemData { 7, 17 } } });

        QTest::addRow("pageContainingRow")
          << listDataTablesData << documentData << dataItemsAssociationData
          << QList<DataTableData> { DataTableData { DataIndex { 14, 0 }, "New date" },
                                    DataTableData { DataIndex { 14, 1 }, "New product" },
                                    DataTableData { DataIndex { 14, 2 }, "New count" } }
          << expectedDocumentData;
    }

    {
        documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                = _documentData();
        datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();

        using namespace painter::data;

        documentData.pageItemsList.push_back(painter::data::PaintableItemListModelData {
          { PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } },

            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
              QStringLiteral("A general variable : Somewhere over the rainbow, 10/10/2022") },

            PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372, 2280, 3843 } } },
              QStringLiteral("Another data : whatever") },

            PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372 + 3843, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372 + 3843, 2280, 3843 } } },
              QStringLiteral("Another data : whatever2") } } });

        expectedDocumentData = documentData;

        expectedDocumentData.pageItemsList.last().paintableItemList.append({
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },

          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } } },
            QStringLiteral("A date : New date") },
          PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } } },
            QStringLiteral("A product : New product") },
          PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } } },
            QStringLiteral("Count : New count") },
          PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } } },
            QStringLiteral("Combined (New date,New product,New count)") },
        });

        documentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        expectedDocumentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        QTest::addRow("pageContainingRow_FromAnotherListOnly")
          << listDataTablesData << documentData << dataItemsAssociationData
          << QList<DataTableData> { DataTableData { DataIndex { 12, 0 }, "New date" },
                                    DataTableData { DataIndex { 12, 1 }, "New product" },
                                    DataTableData { DataIndex { 12, 2 }, "New count" } }
          << expectedDocumentData;
    }

    {
        documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                = _documentData();
        datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();

        using namespace painter::data;

        documentData.pageItemsList.push_back(painter::data::PaintableItemListModelData {
          { PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } },

            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
              QStringLiteral("A general variable : Somewhere over the rainbow, 10/10/2022") },

            PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372, 2280, 3843 } } },
              QStringLiteral("Another data : whatever") },

            PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372 + 3843, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372 + 3843, 2280, 3843 } } },
              QStringLiteral("Another data : whatever2") },

            PaintableRectangleData { RectanglePositionData { PositionData { 5164 + 2280, 17372, 2280, 3843 } },
                                     ColorData {},
                                     PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData {
              ColorData {},
              PaintableRectangleData { RectanglePositionData { PositionData { 5164 + 2280, 17372, 2280, 3843 } } },
              QStringLiteral("Another data : whatever") },

            PaintableRectangleData {
              RectanglePositionData { PositionData { 5164 + 2280, 17372 + 3843, 2280, 3843 } },
              ColorData {},
              PaintableBorderData { 1, ColorData { 1, "black" } } },
            PaintableTextData { ColorData {},
                                PaintableRectangleData { RectanglePositionData {
                                  PositionData { 5164 + 2280, 17372 + 3843, 2280, 3843 } } },
                                QStringLiteral("Another data : whatever2") } } });

        expectedDocumentData = documentData;

        expectedDocumentData.pageItemsList.last().paintableItemList.append({
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },

          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 2280, 3843 } } },
            QStringLiteral("A date : New date") },
          PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 2605, 1000, 2280, 3843 } } },
            QStringLiteral("A product : New product") },
          PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 4885, 1000, 2280, 3843 } } },
            QStringLiteral("Count : New count") },
          PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } },
          PaintableTextData {
            ColorData {},
            PaintableRectangleData { RectanglePositionData { PositionData { 7165, 1000, 2280, 3843 } } },
            QStringLiteral("Combined (New date,New product,New count)") },
        });

        documentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        expectedDocumentData.pageItemsList.last().paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

        QTest::addRow("pageContainingRow_fromAnotherListOnly_secondaryRepetition")
          << listDataTablesData << documentData << dataItemsAssociationData
          << QList<DataTableData> { DataTableData { DataIndex { 12, 0 }, "New date" },
                                    DataTableData { DataIndex { 12, 1 }, "New product" },
                                    DataTableData { DataIndex { 12, 2 }, "New count" } }
          << expectedDocumentData;
    }
}

void Test_DataItemsAssociation::test_onTableRowsInserted()
{
    QFETCH(const documentdatadefinitions::data::DataTablesData, listDataTablesData);
    QFETCH(const document::data::DocumentData, documentData);
    QFETCH(const datadocumentassociation::data::DataItemsAssociationData, dataItemsAssociationData);
    QFETCH(const document::data::DocumentData, expectedDocumentData);
    QFETCH(const QList<DataTableData>, dataTablesData);

    const documentdatadefinitions::data::DataDefinitionListData listDataDefinitionListData =
      _listDataDefinitionListData();

    const documentdatadefinitions::data::DataDefinitionListData generalDataDefinitionListData =
      _generalDataDefinitionListData();
    const documentdatadefinitions::data::DataTablesData generalDataTablesData = _generalDataTablesData();

    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();

    documentdatadefinitions::controller::DataDefinitionListModel listDataDefinitionListModel(listDataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel listDataTablesModel(
      &listDataDefinitionListModel, listDataTablesData, this);

    documentdatadefinitions::controller::DataDefinitionListModel generalDataDefinitionListModel(generalDataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel generalDataTablesModel(
      &generalDataDefinitionListModel, generalDataTablesData, this);

    document::controller::Document document(documentData);

    datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation;
    dataItemsAssociation.setListDataTablesModel(&listDataTablesModel);
    dataItemsAssociation.setDocument(&document);
    dataItemsAssociation.setDocumentTemplateData(documentTemplateData);
    dataItemsAssociation.setPaintableListTemplateIndex(3);
    dataItemsAssociation.setGeneralDataTablesModel(&generalDataTablesModel);
    dataItemsAssociation.setObjectData(dataItemsAssociationData);

    listDataTablesModel.addDataRow();

    for (const DataTableData& dataTableData : dataTablesData)
    {
        listDataTablesModel.setData(
          listDataTablesModel.index(dataTableData.itemIndex.row, dataTableData.itemIndex.column),
          dataTableData.data);
    }

    QCOMPARE(document.rowCount(), 4);

    document::data::DocumentData actualDocumentData = document.objectData();

    QCOMPARE(actualDocumentData.pageItemsList.size(), expectedDocumentData.pageItemsList.size());

    for (int pageIndex = 0; pageIndex < expectedDocumentData.pageItemsList.size(); ++pageIndex)
    {
        painter::data::PaintableItemListModelData actualPageData =
          actualDocumentData.pageItemsList.at(pageIndex);
        painter::data::PaintableItemListModelData expectedPageData =
          expectedDocumentData.pageItemsList.at(pageIndex);

        qDebug() << "Page index : " << pageIndex;

        QCOMPARE(actualPageData.paintableItemList.size(), expectedPageData.paintableItemList.size());

        for (int itemIndex = 0; itemIndex < expectedPageData.paintableItemList.size(); ++itemIndex)
        {
            qDebug() << "ItemIndex : " << itemIndex;
            QCOMPARE(actualPageData.paintableItemList.at(itemIndex),
                     expectedPageData.paintableItemList.at(itemIndex));
        }
    }
}

void Test_DataItemsAssociation::test_onTableRowsRemoved_data()
{
    QTest::addColumn<documentdatadefinitions::data::DataTablesData>("listDataTablesData");
    QTest::addColumn<document::data::DocumentData>("documentData");
    QTest::addColumn<datadocumentassociation::data::DataItemsAssociationData>("dataItemsAssociationData");
    QTest::addColumn<int>("rowIndex");
    QTest::addColumn<document::data::DocumentData>("expectedDocumentData");
    QTest::addColumn<datadocumentassociation::data::DataItemsAssociationData>("expectedDataItemsAssociationData");

    {
        const documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        const document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                      = _documentData();
        const datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();
        datadocumentassociation::data::DataItemsAssociationData expectedDataItemsAssociationData =
          _dataItemsAssociationData();

        expectedDocumentData.pageItemsList[2].paintableItemList.remove(25, 8);

        expectedDataItemsAssociationData.rows.remove(11);

        QTest::addRow("removeLastRow") << listDataTablesData << documentData << dataItemsAssociationData << 11
                                       << expectedDocumentData << expectedDataItemsAssociationData;
    }

    {
        documentdatadefinitions::data::DataTablesData listDataTablesData = _listDataTablesData();
        document::data::DocumentData documentData                        = _documentData();
        document::data::DocumentData expectedDocumentData                = _documentData();
        datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          _dataItemsAssociationData();
        datadocumentassociation::data::DataItemsAssociationData expectedDataItemsAssociationData =
          _dataItemsAssociationData();

        // Replace the text of the items n with the one of n+1
        for (int rowIndex = 6; rowIndex < dataItemsAssociationData.rows.size() - 1; ++rowIndex)
        {
            datadocumentassociation::data::PageItemsTemplateAssociationData dataRowAssociation =
              dataItemsAssociationData.rows.at(rowIndex);

            datadocumentassociation::data::PageItemsTemplateAssociationData expectedDataRowAssociation =
              expectedDataItemsAssociationData.rows.at(rowIndex + 1);

            for (int templatedItemIndex = 0; templatedItemIndex < dataRowAssociation.templatedItems.size();
                 ++templatedItemIndex)
            {
                const datadocumentassociation::data::TemplatedItemData& templatedItemData =
                  dataRowAssociation.templatedItems.at(templatedItemIndex);

                const datadocumentassociation::data::TemplatedItemData& expectedTemplatedItemData =
                  expectedDataRowAssociation.templatedItems.at(templatedItemIndex);

                const painter::data::VariantListableItemData& item =
                  documentData.pageItemsList.at(expectedDataRowAssociation.pageIndex)
                    .paintableItemList.at(expectedTemplatedItemData.itemIndex);

                if (const auto* const paintableText = std::get_if<painter::data::PaintableTextData>(&item))
                {
                    painter::data::VariantListableItemData& expectedItem =
                      expectedDocumentData.pageItemsList[dataRowAssociation.pageIndex]
                        .paintableItemList[templatedItemData.itemIndex];

                    std::get<painter::data::PaintableTextData>(expectedItem).text = paintableText->text;
                }
            }
        }

        // Remove the last items
        expectedDocumentData.pageItemsList[2].paintableItemList.remove(25, 8);

        expectedDataItemsAssociationData.rows.remove(11);

        QTest::addRow("removeRow_7") << listDataTablesData << documentData << dataItemsAssociationData << 6
                                     << expectedDocumentData << expectedDataItemsAssociationData;
    }
}

void Test_DataItemsAssociation::test_onTableRowsRemoved()
{
    QFETCH(const documentdatadefinitions::data::DataTablesData, listDataTablesData);
    QFETCH(const document::data::DocumentData, documentData);
    QFETCH(const datadocumentassociation::data::DataItemsAssociationData, dataItemsAssociationData);
    QFETCH(const document::data::DocumentData, expectedDocumentData);
    QFETCH(const datadocumentassociation::data::DataItemsAssociationData, expectedDataItemsAssociationData);
    QFETCH(int, rowIndex);

    const documentdatadefinitions::data::DataDefinitionListData listDataDefinitionListData =
      _listDataDefinitionListData();

    const documentdatadefinitions::data::DataDefinitionListData generalDataDefinitionListData =
      _generalDataDefinitionListData();
    const documentdatadefinitions::data::DataTablesData generalDataTablesData = _generalDataTablesData();

    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();

    documentdatadefinitions::controller::DataDefinitionListModel listDataDefinitionListModel(listDataDefinitionListData);
    documentdatadefinitions::controller::DataTablesModel listDataTablesModel(
      &listDataDefinitionListModel, listDataTablesData, this);

    document::controller::Document document(documentData);

    datadocumentassociation::controller::DataItemsAssociation dataItemsAssociation;
    dataItemsAssociation.setListDataTablesModel(&listDataTablesModel);
    dataItemsAssociation.setDocumentTemplateData(documentTemplateData);
    dataItemsAssociation.setPaintableListTemplateIndex(3);
    dataItemsAssociation.setDocument(&document);
    dataItemsAssociation.setObjectData(dataItemsAssociationData);

    listDataTablesModel.removeDataRow(rowIndex);

    document::data::DocumentData actualDocumentData = document.objectData();

    QCOMPARE(actualDocumentData.pageItemsList.size(), expectedDocumentData.pageItemsList.size());

    for (int pageIndex = 0; pageIndex < expectedDocumentData.pageItemsList.size(); ++pageIndex)
    {
        qDebug() << "Page : " << pageIndex;

        // qDebug() << documentData.pageItemsList.at(pageIndex);
        // qDebug() << actualDocumentData.pageItemsList.at(pageIndex);

        painter::data::PaintableItemListModelData actualPageData =
          actualDocumentData.pageItemsList.at(pageIndex);
        painter::data::PaintableItemListModelData expectedPageData =
          expectedDocumentData.pageItemsList.at(pageIndex);

        qDebug() << actualPageData.paintableItemList.size();
        QCOMPARE(actualPageData.paintableItemList.size(), expectedPageData.paintableItemList.size());

        for (int itemIndex = 0; itemIndex < expectedPageData.paintableItemList.size(); ++itemIndex)
        {
            QCOMPARE(actualPageData.paintableItemList.at(itemIndex),
                     expectedPageData.paintableItemList.at(itemIndex));
        }
    }

    datadocumentassociation::data::DataItemsAssociationData actualDataItemsAssociationData =
      dataItemsAssociation.objectData();

    QCOMPARE(actualDataItemsAssociationData.rows.size(), expectedDataItemsAssociationData.rows.size());

    for (int dataItemsAssociationIndex = 0;
         dataItemsAssociationIndex < expectedDataItemsAssociationData.rows.size();
         ++dataItemsAssociationIndex)
    {
        QCOMPARE(actualDataItemsAssociationData.rows.at(dataItemsAssociationIndex),
                 expectedDataItemsAssociationData.rows.at(dataItemsAssociationIndex));
    }
}

documentdatadefinitions::data::DataDefinitionListData
Test_DataItemsAssociation::_listDataDefinitionListData() const
{
    return documentdatadefinitions::data::DataDefinitionListData {
        "Name of the list",
        { documentdatadefinitions::data::DataDefinitionData {
            "Date",
            "The date of the day",
            "Its today the day of the day",
            documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
          documentdatadefinitions::data::DataDefinitionData {
            "Product",
            "The product",
            "Something sell to a client",
            documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
          documentdatadefinitions::data::DataDefinitionData {
            "Count",
            "The number of something",
            "1,2,3 and more",
            documentdatadefinitions::data::DataDefinitionData::DataType::NumberType } }
    };
}

documentdatadefinitions::data::DataTablesData Test_DataItemsAssociation::_listDataTablesData() const
{
    documentdatadefinitions::data::DataTablesData dataTablesData;

    for (int pageIndex = 0; pageIndex < PAGE_NUMBER; ++pageIndex)
    {
        for (int rowIndex = 0; rowIndex < ROW_NUMBER; ++rowIndex)
        {
            documentdatadefinitions::data::DataTableData dataTableData {
                { 0, QStringLiteral("Old date %1").arg(rowIndex + 4 * pageIndex) },
                { 1, QStringLiteral("Old product %1").arg(rowIndex + 4 * pageIndex) },
                { 2, QStringLiteral("Old count %1").arg(rowIndex + 4 * pageIndex) }
            };

            dataTablesData.dataTables.push_back(dataTableData);
        }
    }

    return dataTablesData;
}

documentdatadefinitions::data::DataDefinitionListData
Test_DataItemsAssociation::_generalDataDefinitionListData() const
{
    return documentdatadefinitions::data::DataDefinitionListData {
        "Geneal list data",
        { documentdatadefinitions::data::DataDefinitionData {
            "address",
            "The address",
            "Its somewhere",
            documentdatadefinitions::data::DataDefinitionData::DataType::StringType },
          documentdatadefinitions::data::DataDefinitionData {
            "date",
            "The date of today",
            "01/01/2023",
            documentdatadefinitions::data::DataDefinitionData::DataType::StringType } }
    };
}

documentdatadefinitions::data::DataTablesData Test_DataItemsAssociation::_generalDataTablesData() const
{
    return documentdatadefinitions::data::DataTablesData {
        { documentdatadefinitions::data::DataTableData {
          { 0, QStringLiteral("Somewhere over the rainbow") }, { 1, QStringLiteral("10/10/2022") } } }
    };
}

documenttemplate::data::DocumentTemplateData Test_DataItemsAssociation::_documentTemplateData() const
{
    using namespace painter::data;
    documenttemplate::data::DocumentTemplateData documentTemplateData;
    documentTemplateData.pageSizeData.setPageSize(QPageSize::A4);

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(PaintableTextData {
      ColorData {},
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
      QStringLiteral("A general variable : ${address}, ${date}") });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5164, 17372, 5000, 8000 },
                          PaintableListData::RepetitionDirection::BottomRepetition,
                          PaintableListData::RepetitionDirection::RightRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 2280, 3843 } } },
                                                  QStringLiteral("Another data : ${AnotherData}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(_paintableListTemplate());

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

    return documentTemplateData;
}

document::data::DocumentData Test_DataItemsAssociation::_documentData() const
{
    documentdatadefinitions::data::DataTablesData dataTablesData = _listDataTablesData();

    document::data::DocumentData documentData;
    documentData.pageSize.setPageSize(QPageSize::A4);

    for (int pageIndex = 0; pageIndex < PAGE_NUMBER; ++pageIndex)
    {
        using namespace painter::data;
        PaintableItemListModelData paintableItemListModelData;
        paintableItemListModelData.paintableItemList.push_back(
          PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

        for (int rowIndex = 0; rowIndex < ROW_NUMBER; ++rowIndex)
        {
            auto dataTableData = dataTablesData.dataTables.at(rowIndex + 4 * pageIndex);
            // Complete the document

            paintableItemListModelData.paintableItemList.push_back(PaintableRectangleData {
              RectanglePositionData { PositionData { 325, 1000 + rowIndex * 3843.0, 2280, 3843 } },
              ColorData {},
              PaintableBorderData { 1, ColorData { 1, "black" } } });
            paintableItemListModelData.paintableItemList.push_back(
              PaintableTextData { ColorData {},
                                  PaintableRectangleData { RectanglePositionData {
                                    PositionData { 325, 1000 + rowIndex * 3843.0, 2280, 3843 } } },
                                  QStringLiteral("A date : %1").arg(dataTableData.value(0)) });

            paintableItemListModelData.paintableItemList.push_back(PaintableRectangleData {
              RectanglePositionData { PositionData { 2605, 1000 + rowIndex * 3843.0, 2280, 3843 } },
              ColorData {},
              PaintableBorderData { 1, ColorData { 1, "black" } } });
            paintableItemListModelData.paintableItemList.push_back(
              PaintableTextData { ColorData {},
                                  PaintableRectangleData { RectanglePositionData {
                                    PositionData { 2605, 1000 + rowIndex * 3843.0, 2280, 3843 } } },
                                  QStringLiteral("A product : %1").arg(dataTableData.value(1)) });

            paintableItemListModelData.paintableItemList.push_back(PaintableRectangleData {
              RectanglePositionData { PositionData { 4885, 1000 + rowIndex * 3843.0, 2280, 3843 } },
              ColorData {},
              PaintableBorderData { 1, ColorData { 1, "black" } } });
            paintableItemListModelData.paintableItemList.push_back(
              PaintableTextData { ColorData {},
                                  PaintableRectangleData { RectanglePositionData {
                                    PositionData { 4885, 1000 + rowIndex * 3843.0, 2280, 3843 } } },
                                  QStringLiteral("Count : %1").arg(dataTableData.value(2)) });

            paintableItemListModelData.paintableItemList.push_back(PaintableRectangleData {
              RectanglePositionData { PositionData { 7166, 1000 + rowIndex * 3843.0, 2280, 3843 } },
              ColorData {},
              PaintableBorderData { 1, ColorData { 1, "black" } } });
            paintableItemListModelData.paintableItemList.push_back(PaintableTextData {
              ColorData {},
              PaintableRectangleData {
                RectanglePositionData { PositionData { 7166, 1000 + rowIndex * 3843.0, 2280, 3843 } } },
              QStringLiteral("Combined (%1,%2,%3)")
                .arg(dataTableData.value(0), dataTableData.value(1), dataTableData.value(2)) });
        }

        paintableItemListModelData.paintableItemList.push_back(PaintableRectangleData {
          PaintableRectangleData { RectanglePositionData { PositionData { 5164, 17372, 2280, 3843 } },
                                   ColorData {},
                                   PaintableBorderData { 1, ColorData { 1, "black" } } } });

        documentData.pageItemsList.push_back(paintableItemListModelData);
    }

    return documentData;
}

painter::data::PaintableListData Test_DataItemsAssociation::_paintableListTemplate() const
{
    painter::data::PaintableListData paintableListTemplate;

    {
        using namespace painter::data;
        paintableListTemplate = PaintableListData {
            PositionData { 325, 1000, 9120, 15372 },
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData {
              { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } },
                                         ColorData {},
                                         PaintableBorderData { 1, ColorData { 1, "black" } } },
                PaintableTextData {
                  ColorData {},
                  PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } } },
                  QStringLiteral("A date : ${Date}") },
                PaintableRectangleData { RectanglePositionData { PositionData { 2280, 0, 2280, 3843 } },
                                         ColorData {},
                                         PaintableBorderData { 1, ColorData { 1, "black" } } },
                PaintableTextData {
                  ColorData {},
                  PaintableRectangleData { RectanglePositionData { PositionData { 2280, 0, 2280, 3843 } } },
                  QStringLiteral("A product : ${Product}") },
                PaintableRectangleData { RectanglePositionData { PositionData { 4560, 0, 2280, 3843 } },
                                         ColorData {},
                                         PaintableBorderData { 1, ColorData { 1, "black" } } },
                PaintableTextData {
                  ColorData {},
                  PaintableRectangleData { RectanglePositionData { PositionData { 4560, 0, 2280, 3843 } } },
                  QStringLiteral("Count : ${Count}") },
                PaintableRectangleData { RectanglePositionData { PositionData { 6840, 0, 2280, 3843 } },
                                         ColorData {},
                                         PaintableBorderData { 1, ColorData { 1, "black" } } },
                PaintableTextData {
                  ColorData {},
                  PaintableRectangleData { RectanglePositionData { PositionData { 6840, 0, 2280, 3843 } } },
                  QStringLiteral("Combined (${Date},${Product},${Count})") } } }
        };
    }
    return paintableListTemplate;
}

datadocumentassociation::data::DataItemsAssociationData Test_DataItemsAssociation::_dataItemsAssociationData()
  const
{
    painter::data::PaintableListData paintableListTemplate = _paintableListTemplate();

    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData;

    {
        for (int pageIndex = 0; pageIndex < PAGE_NUMBER; ++pageIndex)
        {
            for (int rowIndex = 0; rowIndex < ROW_NUMBER; ++rowIndex)
            {
                datadocumentassociation::data::PageItemsTemplateAssociationData dataRowItemsAssociationData;
                dataRowItemsAssociationData.pageIndex = pageIndex;

                for (int itemIndex = 0;
                     itemIndex < paintableListTemplate.paintableItemListModel.paintableItemList.size();
                     ++itemIndex)
                {
                    dataRowItemsAssociationData.templatedItems.push_back(
                      datadocumentassociation::data::TemplatedItemData { itemIndex,
                                                                         1 + 8 * rowIndex + itemIndex });
                }

                dataItemsAssociationData.rows.push_back(dataRowItemsAssociationData);
            }
        }
    }

    return dataItemsAssociationData;
}

QTEST_MAIN(Test_DataItemsAssociation)

#include "Test_DataItemsAssociation.moc"
