#include <QTest>

#include "DataDocumentAssociation/controllers/DataDocumentAssociation.hpp"
#include "DataDocumentAssociation/controllers/DataItemsAssociation.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "Document/controllers/Document.hpp"
#include "Document/data/DocumentData.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "Painter/data/PaintableListData.hpp"

class Test_DataDocumentAssociation : public QObject
{
    Q_OBJECT

  public:
    Test_DataDocumentAssociation()           = default;
    ~Test_DataDocumentAssociation() override = default;

  private slots:
    void test_objectData();
    void test_setObjectData();

  private:
    Q_DISABLE_COPY_MOVE(Test_DataDocumentAssociation)

    documenttemplate::data::DocumentTemplateData _documentTemplateData() const;
    document::data::DocumentData _documentData() const;
    documentdatadefinitions::data::DataHandlerData _dataHandlerData() const;
    datatemplateassociation::data::DataTemplateAssociationData _dataTemplateAssociationData() const;
    datadocumentassociation::data::DataDocumentAssociationData _dataDocumentAssociationData() const;
};

void Test_DataDocumentAssociation::test_objectData()
{
    document::controller::Document document(_documentData());
    documentdatadefinitions::controller::DataHandler dataHandler(_dataHandlerData());
    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();
    const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociation =
      _dataTemplateAssociationData();
    const datadocumentassociation::data::DataDocumentAssociationData expectedDataDocumentAssociationData =
      _dataDocumentAssociationData();

    datadocumentassociation::controller::DataDocumentAssociation dataDocumentAssociation;
    dataDocumentAssociation.setDocument(&document);
    dataDocumentAssociation.setUserDataHandler(dataHandler.userDataHandler());
    dataDocumentAssociation.setDocumentTemplateData(_documentTemplateData());
    dataDocumentAssociation.setDataTemplateAssociationData(_dataTemplateAssociationData());

    dataDocumentAssociation.setObjectData(expectedDataDocumentAssociationData);

    datadocumentassociation::data::DataDocumentAssociationData actualDataDocumentAsssociationData =
      dataDocumentAssociation.objectData();

    QCOMPARE(actualDataDocumentAsssociationData.generalListItemsAssociationData,
             expectedDataDocumentAssociationData.generalListItemsAssociationData);

    QCOMPARE(actualDataDocumentAsssociationData.dataItemsAssociationData.keys(),
             expectedDataDocumentAssociationData.dataItemsAssociationData.keys());

    for (auto iterator = std::cbegin(expectedDataDocumentAssociationData.dataItemsAssociationData);
         iterator != std::cend(expectedDataDocumentAssociationData.dataItemsAssociationData);
         ++iterator)
    {
        QCOMPARE(iterator.value(),
                 expectedDataDocumentAssociationData.dataItemsAssociationData.value(iterator.key()));
    }

    QCOMPARE(actualDataDocumentAsssociationData, expectedDataDocumentAssociationData);
}

void Test_DataDocumentAssociation::test_setObjectData()
{
    document::controller::Document document(_documentData());
    documentdatadefinitions::controller::DataHandler dataHandler(_dataHandlerData());
    const documenttemplate::data::DocumentTemplateData documentTemplateData = _documentTemplateData();
    const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociation =
      _dataTemplateAssociationData();
    const datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData =
      _dataDocumentAssociationData();

    datadocumentassociation::controller::DataDocumentAssociation dataDocumentAssociation;
    dataDocumentAssociation.setDocument(&document);
    dataDocumentAssociation.setUserDataHandler(dataHandler.userDataHandler());
    dataDocumentAssociation.setDocumentTemplateData(_documentTemplateData());
    dataDocumentAssociation.setDataTemplateAssociationData(_dataTemplateAssociationData());

    dataDocumentAssociation.setObjectData(dataDocumentAssociationData);

    for (int paintableListIndex : { 2, 3, 5 })
    {
        const datadocumentassociation::controller::DataItemsAssociation* const dataItemsAssociation =
          dataDocumentAssociation.dataItemsAssociation(paintableListIndex);

        QCOMPARE(dataItemsAssociation->document(), &document);
        QCOMPARE(dataItemsAssociation->generalDataTablesModel(),
                 dataHandler.userDataHandler()->generalVariableDataList());

        datatemplateassociation::data::DataTablesModelIndex dataTableModelIndex =
          dataTemplateAssociation.paintableListDataTableModelAssociationData.association.value(paintableListIndex);

        QCOMPARE(dataItemsAssociation->listDataTablesModel(),
                 dataHandler.userDataHandler()->dataTableModelListModel()->at(dataTableModelIndex));
        QCOMPARE(dataItemsAssociation->documentTemplateData(), documentTemplateData);
        QCOMPARE(dataItemsAssociation->paintableListTemplate(),
                 std::get<painter::data::PaintableListData>(
                   documentTemplateData.rootPaintableItemListData.paintableList.at(paintableListIndex)));
        QCOMPARE(dataItemsAssociation->objectData(),
                 dataDocumentAssociationData.dataItemsAssociationData.value(paintableListIndex));
    }
}

documenttemplate::data::DocumentTemplateData Test_DataDocumentAssociation::_documentTemplateData() const
{
    using namespace painter::data;
    documenttemplate::data::DocumentTemplateData documentTemplateData;
    documentTemplateData.pageSizeData.setPageSize(QPageSize::A4);

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(PaintableTextData {
      ColorData {},
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 500, 500 } } },
      QStringLiteral("A general variable : ${address}, ${date}") });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 17372, 5000, 8000 },
                          PaintableListData::RepetitionDirection::BottomRepetition,
                          PaintableListData::RepetitionDirection::RightRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 2280, 3843 } } },
                                                  QStringLiteral("Another data : ${DataOfList1}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 25000, 5000, 8000 },
                          PaintableListData::RepetitionDirection::BottomRepetition,
                          PaintableListData::RepetitionDirection::NoRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 5000, 1000 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 5000, 1000 } } },
                                                  QStringLiteral("Another data : ${DataOfList2}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 1000, 9120, 11530 } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableListData { PositionData { 5000, 34000, 5000, 8000 },
                          PaintableListData::RepetitionDirection::RightRepetition,
                          PaintableListData::RepetitionDirection::NoRepetition,
                          PaintableItemListModelData {
                            { PaintableRectangleData { RectanglePositionData { PositionData { 0, 0, 2280, 3843 } },
                                                       ColorData {},
                                                       PaintableBorderData { 1, ColorData { 1, "black" } } },
                              PaintableTextData { ColorData {},
                                                  PaintableRectangleData {
                                                    RectanglePositionData { PositionData { 0, 0, 2280, 3843 } } },
                                                  QStringLiteral("Another data : ${DataOfList3}") } } } });

    documentTemplateData.rootPaintableItemListData.paintableList.push_back(
      PaintableRectangleData { RectanglePositionData { PositionData { 325, 20372, 2500, 2500 } } });

    return documentTemplateData;
}

document::data::DocumentData Test_DataDocumentAssociation::_documentData() const
{
    document::service::documentgenerator::GeneratedDocument generatedDocument =
      document::service::documentgenerator::generateDocument(
        _documentTemplateData(), _dataHandlerData(), _dataTemplateAssociationData());

    return generatedDocument.documentData;
}

documentdatadefinitions::data::DataHandlerData Test_DataDocumentAssociation::_dataHandlerData() const
{
    using namespace documentdatadefinitions::data;

    DataHandlerData dataHandlerData;

    dataHandlerData.dataDefinitionsHandler.generalVariableList = DataDefinitionListData {
        "Geneal list data",
        { DataDefinitionData {
            "address", "The address", "Its somewhere", DataDefinitionData::DataType::StringType },
          DataDefinitionData {
            "date", "The date of today", "01/01/2023", DataDefinitionData::DataType::StringType } }
    };

    dataHandlerData.userDataHandler.generalVariableDataList = DataTablesData { { DataTableData {
      { 0, "24 main street 658 NorthCity DarkCountry" },
      { 1, "17/12/2022" },
    } } };

    for (int dataListIndex = 0; dataListIndex < 3; ++dataListIndex)
    {
        dataHandlerData.dataDefinitionsHandler.listDataDefinitionList.listOfDataDefinition.push_back(
          ListDataDefinitionData {
            DataDefinitionListData { QStringLiteral("DataList%1").arg(dataListIndex),
                                     {
                                       DataDefinitionData { QStringLiteral("DataOfList%1").arg(dataListIndex),
                                                            "Some data",
                                                            "Some data",
                                                            DataDefinitionData::DataType::StringType },
                                     } },
            DataTablesData { { DataTableData { { 0, "" } } } } });

        DataTablesData listData;

        for (int listRowIndex = 0; listRowIndex < 20; ++listRowIndex)
        {
            listData.dataTables.push_back(
              DataTableData { { 0, QStringLiteral("RandomData %1").arg(listRowIndex) } });
        }

        dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.push_back(listData);
    }

    return dataHandlerData;
}

datatemplateassociation::data::DataTemplateAssociationData
Test_DataDocumentAssociation::_dataTemplateAssociationData() const
{
    return datatemplateassociation::data::DataTemplateAssociationData {
        datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
          QHash<datatemplateassociation::data::PaintableListIndex,
                datatemplateassociation::data::DataDefinitionIndex> { { 2, 0 }, { 3, 1 }, { 5, 2 } } },
        datatemplateassociation::data::PaintableListDataTableModelAssociationData {
          QHash<datatemplateassociation::data::PaintableListIndex,
                datatemplateassociation::data::DataTablesModelIndex> { { 2, 0 }, { 3, 1 }, { 5, 2 } } }
    };
}

datadocumentassociation::data::DataDocumentAssociationData
Test_DataDocumentAssociation::_dataDocumentAssociationData() const
{
    document::service::documentgenerator::GeneratedDocument generatedDocument =
      document::service::documentgenerator::generateDocument(
        _documentTemplateData(), _dataHandlerData(), _dataTemplateAssociationData());

    datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData;

    for (int pageIndex = 0; pageIndex < generatedDocument.dataItemsAssociationData.pageItemsAssociationData.size();
         ++pageIndex)
    {
        const document::data::PageItemsAssociationData& pageItemsAssociation =
          generatedDocument.dataItemsAssociationData.pageItemsAssociationData.at(pageIndex);

        datadocumentassociation::data::PageItemsTemplateAssociationData pageItemsTemplateAssociationData;
        pageItemsTemplateAssociationData.pageIndex = pageIndex;

        for (int itemIndex = 0; itemIndex < pageItemsAssociation.itemsAssociationData.size(); ++itemIndex)
        {
            const document::data::ItemAssociationData& itemAssociationData =
              pageItemsAssociation.itemsAssociationData[itemIndex];

            if (itemAssociationData.templateListIndex == -1)
            {
                datadocumentassociation::data::TemplatedItemData templatedItemdata;

                templatedItemdata.itemIndex         = itemIndex;
                templatedItemdata.itemTemplateIndex = itemAssociationData.templateItemIndex;

                pageItemsTemplateAssociationData.templatedItems.push_back(templatedItemdata);
            }
            else
            {
                datadocumentassociation::data::DataItemsAssociationData& dataItemsAssociationData =
                  dataDocumentAssociationData.dataItemsAssociationData[itemAssociationData.templateListIndex];

                while (dataItemsAssociationData.rows.size() <= itemAssociationData.rowIndex)
                {
                    dataItemsAssociationData.rows.push_back(
                      datadocumentassociation::data::PageItemsTemplateAssociationData {});
                }

                dataItemsAssociationData.rows[itemAssociationData.rowIndex].pageIndex = pageIndex;
                dataItemsAssociationData.rows[itemAssociationData.rowIndex].templatedItems.push_back(
                  datadocumentassociation::data::TemplatedItemData { itemAssociationData.templateItemIndex,
                                                                     itemIndex });
            }
        }

        dataDocumentAssociationData.generalListItemsAssociationData.generalListItemsAssociationData.push_back(
          pageItemsTemplateAssociationData);
    }

    return dataDocumentAssociationData;
}

QTEST_MAIN(Test_DataDocumentAssociation)

#include "Test_DataDocumentAssociation.moc"
