#include <QSignalSpy>
#include <QTest>

#include "Document/controllers/Document.hpp"
#include "Document/controllers/DocumentHandler.hpp"
#include "Document/data/DocumentHandlerData.hpp"
#include "DocumentTemplate/controllers/PageSize.hpp"
#include "Painter/controllers/ListableItemListModel.hpp"

class Test_DocumentHandler : public QObject
{
    Q_OBJECT

  public:
    Test_DocumentHandler() = default;

  private slots:
    void data();
    void setData();
};

void Test_DocumentHandler::data()
{
    document::data::DocumentData testDocumentData1;
    testDocumentData1.pageSize.setPageSize(QPageSize::A4);
    testDocumentData1.pageItemsList = {
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } }
    };

    document::data::DocumentData testDocumentData2;
    testDocumentData2.pageSize.setPageSize(QPageSize::A3);
    testDocumentData2.pageItemsList = {
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } }
    };

    document::data::DocumentData testDocumentData3;
    testDocumentData3.pageSize.setPageSize(QPageSize::A5);
    testDocumentData3.pageItemsList = {
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },

        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } }
    };

    document::data::DocumentHandlerData testDocumentHandlerData {
        { document::data::DocumentData {},
          document::data::DocumentData {},
          document::data::DocumentData {} }
    };

    document::controller::DocumentHandler documentHandler;
    documentHandler.setObjectData(testDocumentHandlerData);

    documentHandler.at(0)->setObjectData(testDocumentData1);
    documentHandler.at(1)->setObjectData(testDocumentData2);
    documentHandler.at(2)->setObjectData(testDocumentData3);

    document::data::DocumentHandlerData documentHandlerData = documentHandler.objectData();

    QCOMPARE(documentHandlerData.documents.size(), 3);
    QCOMPARE(documentHandlerData.documents[0], testDocumentData1);
    QCOMPARE(documentHandlerData.documents[1], testDocumentData2);
    QCOMPARE(documentHandlerData.documents[2], testDocumentData3);
}

void Test_DocumentHandler::setData()
{
    document::data::DocumentData testDocumentData1;
    testDocumentData1.pageSize.setPageSize(QPageSize::A4);
    testDocumentData1.pageItemsList = {
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } }
    };

    document::data::DocumentData testDocumentData2;
    testDocumentData2.pageSize.setPageSize(QPageSize::A3);
    testDocumentData2.pageItemsList = {
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } }
    };

    document::data::DocumentData testDocumentData3;
    testDocumentData3.pageSize.setPageSize(QPageSize::A5);
    testDocumentData3.pageItemsList = {
        painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                             painter::data::PaintableRectangleData(),
                                                             painter::data::PaintableTextData() } },
        painter::data::PaintableItemListModelData {
          { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },

        painter::data::PaintableItemListModelData {
          { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } }
    };

    QList<QPointer<document::controller::Document>> documentListPointer;
    {
        document::data::DocumentHandlerData testDocumentHandlerData {
            { testDocumentData1, testDocumentData2, testDocumentData3 }
        };

        document::controller::DocumentHandler documentHandler(testDocumentHandlerData);

        QSignalSpy modelAboutToBeResetSpy(&documentHandler, &QAbstractListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&documentHandler, &QAbstractListModel::modelReset);
        QSignalSpy dataChangedSpy(&documentHandler, &QAbstractListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(&documentHandler, &QAbstractListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&documentHandler, &QAbstractListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&documentHandler, &QAbstractListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&documentHandler, &QAbstractListModel::rowsRemoved);

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        for (int i = 0; i < documentHandler.rowCount(); ++i)
        {
            document::controller::Document* document =
              qvariant_cast<document::controller::Document*>(documentHandler.data(documentHandler.index(i)));
            documentListPointer.append(document);
            QVERIFY(document);
            QCOMPARE(document->objectData(), testDocumentHandlerData.documents[i]);
        }
    }

    for (const QPointer<document::controller::Document>& document : documentListPointer) { QVERIFY(document.isNull()); }

    documentListPointer.clear();

    {
        document::data::DocumentHandlerData testDocumentHandlerData {
            { testDocumentData3, testDocumentData2, testDocumentData1 }
        };

        document::controller::DocumentHandler documentHandler;

        QSignalSpy modelAboutToBeResetSpy(&documentHandler, &QAbstractListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&documentHandler, &QAbstractListModel::modelReset);
        QSignalSpy dataChangedSpy(&documentHandler, &QAbstractListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(&documentHandler, &QAbstractListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&documentHandler, &QAbstractListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&documentHandler, &QAbstractListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&documentHandler, &QAbstractListModel::rowsRemoved);

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        documentHandler.setObjectData(testDocumentHandlerData);

        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        for (int i = 0; i < documentHandler.rowCount(); ++i)
        {
            document::controller::Document* document =
              qvariant_cast<document::controller::Document*>(documentHandler.data(documentHandler.index(i)));
            documentListPointer.append(document);
            QVERIFY(document);
            QCOMPARE(document->objectData(), testDocumentHandlerData.documents[i]);
        }

        testDocumentHandlerData = { document::data::DocumentHandlerData {
          { testDocumentData2, testDocumentData3, testDocumentData1 } } };

        documentHandler.setObjectData(testDocumentHandlerData);

        QCOMPARE(modelAboutToBeResetSpy.size(), 2);
        QCOMPARE(modelResetSpy.size(), 2);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        for (const QPointer<document::controller::Document>& document : documentListPointer)
        {
            QTRY_VERIFY(document.isNull());
        }

        documentListPointer.clear();

        for (int i = 0; i < documentHandler.rowCount(); ++i)
        {
            document::controller::Document* document =
              qvariant_cast<document::controller::Document*>(documentHandler.data(documentHandler.index(i)));
            documentListPointer.append(document);
            QVERIFY(document);
            QCOMPARE(document->objectData(), testDocumentHandlerData.documents[i]);
        }
    }

    for (const QPointer<document::controller::Document>& document : documentListPointer) { QVERIFY(document.isNull()); }
}

QTEST_GUILESS_MAIN(Test_DocumentHandler)

#include "Test_DocumentHandler.moc"
