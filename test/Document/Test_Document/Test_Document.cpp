#include <QSignalSpy>
#include <QTest>

#include "Document/controllers/Document.hpp"
#include "Document/data/DocumentData.hpp"
#include "DocumentTemplate/controllers/PageSize.hpp"
#include "Painter/controllers/ListableItemListModel.hpp"

class Test_Document : public QObject
{
    Q_OBJECT

  public:
    Test_Document() = default;

  private slots:
    void data();
    void setData();
};

void Test_Document::data()
{
    document::data::DocumentData testDocumentData;
    testDocumentData.pageItemsList = { painter::data::PaintableItemListModelData {},
                                       painter::data::PaintableItemListModelData {},
                                       painter::data::PaintableItemListModelData {} };

    document::controller::Document document;
    document.setObjectData(testDocumentData);

    testDocumentData.pageItemsList[0] =
      painter::data::PaintableItemListModelData { { painter::data::PaintableRectangleData(),
                                                           painter::data::PaintableTextData() } };
    testDocumentData.pageItemsList[1] =
      painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                           painter::data::PaintableRectangleData() } };
    testDocumentData.pageItemsList[2] =
      painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                           painter::data::PaintableRectangleData(),
                                                           painter::data::PaintableTextData() } };
    testDocumentData.pageSize.setPageSize(QPageSize::A5);

    document.pageSize()->setPageSizeId(documenttemplate::controller::PageSize::PageSizeId::A5);

    painter::controller::ListableItemListModel* listableModel =
      qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(0)));
    listableModel->setObjectData(testDocumentData.pageItemsList[0]);

    listableModel = qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(1)));
    listableModel->setObjectData(testDocumentData.pageItemsList[1]);

    listableModel = qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(2)));
    listableModel->setObjectData(testDocumentData.pageItemsList[2]);

    document::data::DocumentData documentData = document.objectData();
    QCOMPARE(documentData.pageSize, testDocumentData.pageSize);
    QCOMPARE(documentData.pageItemsList[0], testDocumentData.pageItemsList[0]);
    QCOMPARE(documentData.pageItemsList[1], testDocumentData.pageItemsList[1]);
    QCOMPARE(documentData.pageItemsList[2], testDocumentData.pageItemsList[2]);
    QCOMPARE(documentData, testDocumentData);
}

void Test_Document::setData()
{
    QPointer<documenttemplate::controller::PageSize> pageSizePointer;
    QList<QPointer<painter::controller::ListableItemListModel>> listableItemListPointer;
    {
        document::data::DocumentData testDocumentData;
        testDocumentData.pageSize.setPageSize(QPageSize::A3);
        testDocumentData.pageItemsList = {
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } },
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
            painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                                 painter::data::PaintableRectangleData(),
                                                                 painter::data::PaintableTextData() } }
        };

        document::controller::Document document(testDocumentData);

        QSignalSpy modelAboutToBeResetSpy(&document, &QAbstractListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&document, &QAbstractListModel::modelReset);
        QSignalSpy dataChangedSpy(&document, &QAbstractListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(&document, &QAbstractListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&document, &QAbstractListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&document, &QAbstractListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&document, &QAbstractListModel::rowsRemoved);

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        pageSizePointer = document.pageSize();
        QVERIFY(pageSizePointer);
        QCOMPARE(pageSizePointer->objectData(), testDocumentData.pageSize);

        for (int i = 0; i < document.rowCount(); ++i)
        {
            painter::controller::ListableItemListModel* listableModel =
              qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(i)));
            listableItemListPointer.append(listableModel);
            QVERIFY(listableModel);
            QCOMPARE(listableModel->objectData(), testDocumentData.pageItemsList[i]);
        }
    }

    QVERIFY(pageSizePointer.isNull());
    for (const QPointer<painter::controller::ListableItemListModel>& listableItemPointer : listableItemListPointer)
    {
        QVERIFY(listableItemPointer.isNull());
    }

    listableItemListPointer.clear();

    {
        document::data::DocumentData testDocumentData;
        testDocumentData.pageSize.setPageSize(QPageSize::A6);
        testDocumentData.pageItemsList = {
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } },

            painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                                 painter::data::PaintableRectangleData(),
                                                                 painter::data::PaintableTextData() } }
        };

        document::controller::Document document;

        QSignalSpy modelAboutToBeResetSpy(&document, &QAbstractListModel::modelAboutToBeReset);
        QSignalSpy modelResetSpy(&document, &QAbstractListModel::modelReset);
        QSignalSpy dataChangedSpy(&document, &QAbstractListModel::dataChanged);
        QSignalSpy rowsAboutToBeInsertedSpy(&document, &QAbstractListModel::rowsAboutToBeInserted);
        QSignalSpy rowsInsertedSpy(&document, &QAbstractListModel::rowsInserted);
        QSignalSpy rowsAboutToBeRemovedSpy(&document, &QAbstractListModel::rowsAboutToBeRemoved);
        QSignalSpy rowsRemovedSpy(&document, &QAbstractListModel::rowsRemoved);

        QCOMPARE(modelAboutToBeResetSpy.size(), 0);
        QCOMPARE(modelResetSpy.size(), 0);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        document.setObjectData(testDocumentData);

        QCOMPARE(modelAboutToBeResetSpy.size(), 1);
        QCOMPARE(modelResetSpy.size(), 1);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        pageSizePointer = document.pageSize();
        QVERIFY(pageSizePointer);
        QCOMPARE(pageSizePointer->objectData(), testDocumentData.pageSize);

        for (int i = 0; i < document.rowCount(); ++i)
        {
            painter::controller::ListableItemListModel* listableModel =
              qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(i)));
            listableItemListPointer.append(listableModel);
            QVERIFY(listableModel);
            QCOMPARE(listableModel->objectData(), testDocumentData.pageItemsList[i]);
        }

        testDocumentData.pageItemsList = {
            painter::data::PaintableItemListModelData { { painter::data::PaintableTextData(),
                                                                 painter::data::PaintableRectangleData(),
                                                                 painter::data::PaintableTextData() } },
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableTextData(), painter::data::PaintableRectangleData() } },
            painter::data::PaintableItemListModelData {
              { painter::data::PaintableRectangleData(), painter::data::PaintableTextData() } }

        };

        document.setObjectData(testDocumentData);

        QCOMPARE(modelAboutToBeResetSpy.size(), 2);
        QCOMPARE(modelResetSpy.size(), 2);
        QCOMPARE(dataChangedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeInsertedSpy.size(), 0);
        QCOMPARE(rowsInsertedSpy.size(), 0);
        QCOMPARE(rowsAboutToBeRemovedSpy.size(), 0);
        QCOMPARE(rowsRemovedSpy.size(), 0);

        for (const QPointer<painter::controller::ListableItemListModel>& listableItemPointer : listableItemListPointer)
        {
            QTRY_VERIFY(listableItemPointer.isNull());
        }

        listableItemListPointer.clear();

        pageSizePointer = document.pageSize();
        QVERIFY(pageSizePointer);
        QCOMPARE(pageSizePointer->objectData(), testDocumentData.pageSize);

        for (int i = 0; i < document.rowCount(); ++i)
        {
            painter::controller::ListableItemListModel* listableModel =
              qvariant_cast<painter::controller::ListableItemListModel*>(document.data(document.index(i)));
            listableItemListPointer.append(listableModel);
            QVERIFY(listableModel);
            QCOMPARE(listableModel->objectData(), testDocumentData.pageItemsList[i]);
        }
    }

    QVERIFY(pageSizePointer.isNull());
    for (const QPointer<painter::controller::ListableItemListModel>& listableItemPointer : listableItemListPointer)
    {
        QVERIFY(listableItemPointer.isNull());
    }
}

QTEST_GUILESS_MAIN(Test_Document)

#include "Test_Document.moc"
