#include <QtTest/QTest>

#include "Document/data/DocumentData.hpp"
#include "Document/data/DocumentHandlerData.hpp"
#include "Document/services/DocumentXmlConstants.hpp"
#include "Document/services/DocumentXmlSerialization.hpp"
#include "DocumentTemplate/services/DocumentTemplateXmlSerializationDetails.hpp"
#include "Painter/services/PainterXmlSerializationDetails.hpp"

class Test_DocumentXmlSerialization : public QObject
{
    Q_OBJECT

  public:
  private slots:
    void test_readDocumentData_data();
    void test_readDocumentData();

    void test_writeDocumentData();

    void test_readDocumentHandlerData_data();
    void test_readDocumentHandlerData();

    void test_writeDocumentHandlerData();

  private:
    document::data::DocumentData testDocumentData() const;

    static QByteArray writePaintableItemListModelDataXml(
      const painter::data::PaintableItemListModelData& paintableItemListModelData);
    static QByteArray writePageSizeDataXml(const documenttemplate::data::PageSizeData& pageSizeData);
    static QByteArray writeDocumentDataXml(const document::data::DocumentData& document);
};

void Test_DocumentXmlSerialization::test_readDocumentData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<document::data::DocumentData>>("expectedDocumentData");

    // Same order than write
    {
        std::optional<document::data::DocumentData> expectedDocumentData = testDocumentData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document>");

        arrayData.append(QByteArrayLiteral("<PageItems>"));

        for (const painter::data::PaintableItemListModelData& paintableItemListModelData :
             expectedDocumentData.value().pageItemsList)
        {
            arrayData.append(writePaintableItemListModelDataXml(paintableItemListModelData));
        }

        arrayData.append(QByteArrayLiteral("</PageItems>"));

        arrayData.append(writePageSizeDataXml(expectedDocumentData.value().pageSize));

        arrayData.append(QByteArrayLiteral("</Document>\n"));

        QTest::newRow("Same_order_than_write") << arrayData << expectedDocumentData;
    }

    // Other order than write
    {
        std::optional<document::data::DocumentData> expectedDocumentData = testDocumentData();

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document>");

        arrayData.append(writePageSizeDataXml(expectedDocumentData.value().pageSize));

        arrayData.append(QByteArrayLiteral("<PageItems>"));

        for (const painter::data::PaintableItemListModelData& paintableItemListModelData :
             expectedDocumentData.value().pageItemsList)
        {
            arrayData.append(writePaintableItemListModelDataXml(paintableItemListModelData));
        }

        arrayData.append(QByteArrayLiteral("</PageItems>"));

        arrayData.append(QByteArrayLiteral("</Document>\n"));

        QTest::newRow("Other_order_than_write") << arrayData << expectedDocumentData;
    }

    // self closing parts
    {
        std::optional<document::data::DocumentData> expectedDocumentData = document::data::DocumentData {};

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document>");

        arrayData.append(writePageSizeDataXml(expectedDocumentData.value().pageSize));

        arrayData.append(QByteArrayLiteral("<PageItems/>"));

        arrayData.append(QByteArrayLiteral("</Document>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedDocumentData;
    }

    // Empty DocumentData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document>"
                                                 "</Document>\n");
        std::optional<document::data::DocumentData> expectedDocumentData = document::data::DocumentData {};

        QTest::newRow("Empty_DocumentData") << arrayData << expectedDocumentData;
    }

    // Empty self closing DocumentData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document/>\n");
        std::optional<document::data::DocumentData> expectedDocumentData = document::data::DocumentData {};
        QTest::newRow("Empty_self_closing_DataHandler") << arrayData << expectedDocumentData;
    }

    // Not DataHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<document::data::DocumentData> expectedDocumentData = std::nullopt;
        QTest::newRow("Not_UserDataHandlerData") << arrayData << expectedDocumentData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<document::data::DocumentData> expectedDocumentData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDocumentData;
    }
}

void Test_DocumentXmlSerialization::test_readDocumentData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<document::data::DocumentData>, expectedDocumentData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<document::data::DocumentData> actualDocumentData =
      document::service::readDocumentData(streamReader);

    if (expectedDocumentData.has_value())
    {
        QCOMPARE(actualDocumentData.value(), expectedDocumentData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), document::service::documentElement);
    }
    else { QVERIFY(!actualDocumentData.has_value()); }
}

void Test_DocumentXmlSerialization::test_writeDocumentData()
{
    document::data::DocumentData documentData = testDocumentData();

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    document::service::writeDocumentData(streamWriter, documentData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<Document>");

    expectedArray.append(QByteArrayLiteral("<PageItems>"));

    for (const painter::data::PaintableItemListModelData& paintableItemListModelData : documentData.pageItemsList)
    {
        expectedArray.append(writePaintableItemListModelDataXml(paintableItemListModelData));
    }

    expectedArray.append(QByteArrayLiteral("</PageItems>"));

    expectedArray.append(writePageSizeDataXml(documentData.pageSize));

    expectedArray.append(QByteArrayLiteral("</Document>\n"));

    QCOMPARE(arrayData, expectedArray);
}

void Test_DocumentXmlSerialization::test_readDocumentHandlerData_data()
{
    QTest::addColumn<QByteArray>("arrayData");
    QTest::addColumn<std::optional<document::data::DocumentHandlerData>>("expectedDocumentHandlerData");

    // Complete data
    {
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData =
          document::data::DocumentHandlerData { QList<document::data::DocumentData> {
            testDocumentData(), testDocumentData(), document::data::DocumentData {} } };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DocumentHandler>");

        for (const document::data::DocumentData& document : expectedDocumentHandlerData.value().documents)
        {
            arrayData.append(writeDocumentDataXml(document));
        }

        arrayData.append(QByteArrayLiteral("</DocumentHandler>\n"));

        QTest::newRow("Complete_data") << arrayData << expectedDocumentHandlerData;
    }

    // self closing parts
    {
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData =
          document::data::DocumentHandlerData { QList<document::data::DocumentData> {
            document::data::DocumentData {}, document::data::DocumentData {} } };

        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DocumentHandler>");

        for (const document::data::DocumentData& document : expectedDocumentHandlerData.value().documents)
        {
            arrayData.append(writeDocumentDataXml(document));
        }

        arrayData.append(QByteArrayLiteral("</DocumentHandler>\n"));

        QTest::newRow("Self_closing_parts") << arrayData << expectedDocumentHandlerData;
    }

    // Empty DocumentData
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DocumentHandler>"
                                                 "</DocumentHandler>\n");
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData =
          document::data::DocumentHandlerData {};

        QTest::newRow("Empty_DocumentData") << arrayData << expectedDocumentHandlerData;
    }

    // Empty self closing DocumentHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DocumentHandler/>\n");
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData =
          document::data::DocumentHandlerData {};
        QTest::newRow("Empty_self_closing_DataHandler") << arrayData << expectedDocumentHandlerData;
    }

    // Not DataHandler
    {
        QByteArray arrayData = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                                 "<Patate>\n"
                                                 "</Patate>\n");
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData = std::nullopt;
        QTest::newRow("Not_UserDataHandlerData") << arrayData << expectedDocumentHandlerData;
    }

    // Not xml
    {
        QByteArray arrayData = QByteArrayLiteral("Anything but not xml content");
        std::optional<document::data::DocumentHandlerData> expectedDocumentHandlerData = std::nullopt;
        QTest::newRow("Not_Xml") << arrayData << expectedDocumentHandlerData;
    }
}

void Test_DocumentXmlSerialization::test_readDocumentHandlerData()
{
    QFETCH(QByteArray, arrayData);
    QFETCH(std::optional<document::data::DocumentHandlerData>, expectedDocumentHandlerData);

    QXmlStreamReader streamReader(arrayData);

    streamReader.readNextStartElement();

    std::optional<document::data::DocumentHandlerData> actualDocumentHandlerData =
      document::service::readDocumentHandlerData(streamReader);

    if (expectedDocumentHandlerData.has_value())
    {
        QCOMPARE(actualDocumentHandlerData.value(), expectedDocumentHandlerData.value());
        QCOMPARE(streamReader.errorString(), "");
        QVERIFY(streamReader.isEndElement());
        QCOMPARE(streamReader.name(), document::service::documentHandlerElement);
    }
    else { QVERIFY(!actualDocumentHandlerData.has_value()); }
}

void Test_DocumentXmlSerialization::test_writeDocumentHandlerData()
{
    document::data::DocumentHandlerData documentHandlerData =
      document::data::DocumentHandlerData { QList<document::data::DocumentData> {
        testDocumentData(), testDocumentData(), document::data::DocumentData {} } };

    QByteArray arrayData;
    QXmlStreamWriter streamWriter(&arrayData);

    streamWriter.setAutoFormatting(false);
    streamWriter.writeStartDocument();

    document::service::writeDocumentHandlerData(streamWriter, documentHandlerData);

    streamWriter.writeEndDocument();

    qDebug() << arrayData;

    QByteArray expectedArray = QByteArrayLiteral("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                                                 "<DocumentHandler>");

    for (const document::data::DocumentData& document : documentHandlerData.documents)
    {
        expectedArray.append(writeDocumentDataXml(document));
    }

    expectedArray.append(QByteArrayLiteral("</DocumentHandler>\n"));

    QCOMPARE(arrayData, expectedArray);
}

document::data::DocumentData Test_DocumentXmlSerialization::testDocumentData() const
{
    return document::data::DocumentData {
        QList<painter::data::PaintableItemListModelData> {
          painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
            painter::data::PaintableTextData {
              painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
              painter::data::PaintableRectangleData {
                painter::data::RectanglePositionData {
                  painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                painter::data::PaintableBorderData {
                  25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
              "Some text",
              "Arial",
              12,
              25 },
            painter::data::PaintableRectangleData {},
            painter::data::PaintableRectangleData {
              painter::data::RectanglePositionData {
                painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
              painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
              painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } } },
            painter::data::PaintableTextData {} } },
          painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
            painter::data::PaintableTextData {
              painter::data::ColorData { 34, QColor(255, 191, 128, 34) },
              painter::data::PaintableRectangleData {
                painter::data::RectanglePositionData {
                  painter::data::PositionData { 12.24, 32.56, 45.42, 19.58 }, 0, Qt::RelativeSize },
                painter::data::ColorData { 85, QColor(255, 191, 128, 85) },
                painter::data::PaintableBorderData {
                  25, painter::data::ColorData { 55, QColor(161, 162, 163, 164) } } },
              "Some text",
              "Arial",
              12,
              25 },
            painter::data::PaintableRectangleData {} } },
          painter::data::PaintableItemListModelData { QList<painter::data::VariantListableItemData> {
            painter::data::PaintableRectangleData {
              painter::data::RectanglePositionData {
                painter::data::PositionData { 20.546, 50.32, 100.21, 200.42 }, 42, Qt::AbsoluteSize },
              painter::data::ColorData { 100, QColor(255, 191, 128, 64) },
              painter::data::PaintableBorderData { 2.365, painter::data::ColorData { 100, "purple" } } },
            painter::data::PaintableTextData {} } },
          painter::data::PaintableItemListModelData {} },
        documenttemplate::data::PageSizeData {
          QPageSize { QSizeF { 297, 125 }, QPageSize::Unit::Inch, QString(), QPageSize::ExactMatch },
          1650,
          documenttemplate::data::PageSizeData::Unit::Inch }
    };
}

QByteArray Test_DocumentXmlSerialization::writePaintableItemListModelDataXml(
  const painter::data::PaintableItemListModelData& paintableItemListModelData)
{
    QByteArray paintableItemListModelDataXml;

    QXmlStreamWriter streamWriter(&paintableItemListModelDataXml);
    streamWriter.setAutoFormatting(false);

    painter::service::details::writePaintableItemListModelData(streamWriter, paintableItemListModelData);

    return paintableItemListModelDataXml;
}

QByteArray Test_DocumentXmlSerialization::writePageSizeDataXml(
  const documenttemplate::data::PageSizeData& pageSizeData)
{
    QByteArray pageSizeDataXml;

    QXmlStreamWriter streamWriter(&pageSizeDataXml);
    streamWriter.setAutoFormatting(false);

    documenttemplate::service::details::writePageSizeData(streamWriter, pageSizeData);

    return pageSizeDataXml;
}

QByteArray Test_DocumentXmlSerialization::writeDocumentDataXml(const document::data::DocumentData& document)
{
    QByteArray DocumentXml;

    QXmlStreamWriter streamWriter(&DocumentXml);
    streamWriter.setAutoFormatting(false);

    document::service::writeDocumentData(streamWriter, document);

    return DocumentXml;
}

QTEST_APPLESS_MAIN(Test_DocumentXmlSerialization)

#include "Test_DocumentXmlSerialization.moc"
