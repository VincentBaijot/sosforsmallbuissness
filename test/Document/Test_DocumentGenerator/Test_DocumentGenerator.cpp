#include <QTest>

#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "Document/services/DocumentGeneratorPosition.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "DocumentTemplate/data/VariantInterfaceQmlPaintableItem.hpp"
#include "Painter/data/VariantListableItemData.hpp"

using namespace document::data;
using namespace documenttemplate::data;
using namespace documentdatadefinitions::data;
using namespace painter::data;
using GeneratedDocument = document::service::documentgenerator::GeneratedDocument;

class Test_DocumentGenerator : public QObject
{
    Q_OBJECT

  public:
    Test_DocumentGenerator() = default;

  private slots:
    void generateDocument_data();
    void generateDocument();

    void itemsPerPage_data();
    void itemsPerPage();
    void itemPositionFromIndex_data();
    void itemPositionFromIndex();

    void listChildrenRect();

  private:
    DataHandlerData _generateDocument_dataHandler_data() const;
    DocumentTemplateData _generateDocument_documentTemplate_data() const;

    GeneratedDocument _generateDocument_withoutVariables(const DocumentTemplateData& documentTemplateData) const;

    GeneratedDocument _generateDocument_1page_1item_bottomRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_1page_1item_rightRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_1page_1item_topRepetition(const DocumentTemplateData& documentTemplateData,
                                                                  const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_1page_1item_leftRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;

    GeneratedDocument _generateDocument_4pages_6items_bottomRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_4pages_6items_topRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_4pages_6items_rightRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_4pages_6items_leftRepetition(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;

    GeneratedDocument _generateDocument_secondaryRepetition_primarybottom_secondaryright(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primaryright_secondarybottom(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primarybottom_secondaryleft(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primaryleft_secondarybottom(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primarytop_secondaryright(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primaryright_secondarytop(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primarytop_secondaryleft(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_secondaryRepetition_primaryleft_secondarytop(
      const DocumentTemplateData& documentTemplateData,
      const DataHandlerData& dataHandlerData) const;
    GeneratedDocument _generateDocument_2lists(const DocumentTemplateData& documentTemplateData,
                                               const DataHandlerData& dataHandlerData) const;
};

// Utils equality comparison
inline bool operator==(const VariantInterfaceQmlPaintableItem& variantItem,
                       const VariantListableItemData& variantListable)
{
    const PaintableRectangleData* const rectangleItemData = std::get_if<PaintableRectangleData>(&variantItem);
    const PaintableRectangleData* const rectangleListableData =
      std::get_if<PaintableRectangleData>(&variantListable);

    if (rectangleItemData && rectangleListableData && *rectangleItemData == *rectangleListableData)
    {
        return true;
    }

    const PaintableTextData* const textItemData     = std::get_if<PaintableTextData>(&variantItem);
    const PaintableTextData* const textListableData = std::get_if<PaintableTextData>(&variantListable);

    if (textItemData && textListableData && *textItemData == *textListableData) { return true; }

    return false;
}

void Test_DocumentGenerator::generateDocument_data()
{
    QTest::addColumn<DocumentTemplateData>("documentTemplateData");
    QTest::addColumn<DataHandlerData>("dataHandlerData");
    QTest::addColumn<datatemplateassociation::data::DataTemplateAssociationData>("dataTemplateAssociationData");
    QTest::addColumn<GeneratedDocument>("expectedGeneratedDocument");

    {
        DocumentTemplateData documentTemplateData;
        DataHandlerData dataHandlerData;
        datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData;
        GeneratedDocument expectedGeneratedDocument;

        documentTemplateData.pageSizeData.setPageSize(QPageSize::A5);

        expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;
        expectedGeneratedDocument.documentData.pageItemsList.push_back(PaintableItemListModelData {});
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          PageItemsAssociationData {});

        QTest::newRow("emptyTemplate") << documentTemplateData << dataHandlerData << dataTemplateAssociationData
                                       << expectedGeneratedDocument;
    }

    const DataHandlerData dataHandlerData           = _generateDocument_dataHandler_data();
    const DocumentTemplateData documentTemplateData = _generateDocument_documentTemplate_data();

    {
        GeneratedDocument expectedGeneratedDocument = _generateDocument_withoutVariables(documentTemplateData);

        QTest::newRow("withoutVariables")
          << documentTemplateData << dataHandlerData
          << datatemplateassociation::data::DataTemplateAssociationData {} << expectedGeneratedDocument;
    }

    {
        DocumentTemplateData generalVariableTemplate = documentTemplateData;

        std::get<PaintableTextData>(generalVariableTemplate.rootPaintableItemListData.paintableList[1]).text =
          "${companyAddress}";

        std::get<PaintableTextData>(generalVariableTemplate.rootPaintableItemListData.paintableList[3]).text =
          "The client address : ${clientAddress}";

        std::get<PaintableTextData>(generalVariableTemplate.rootPaintableItemListData.paintableList[6]).text =
          "Date : ${date}, total : ${total}";

        GeneratedDocument expectedGeneratedDocument;

        expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;
        expectedGeneratedDocument.documentData.pageItemsList.push_back(PaintableItemListModelData {
          { std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)),
            std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)),
            std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)),
            std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)),
            std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)),
            std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)) } });

        std::get<PaintableTextData>(expectedGeneratedDocument.documentData.pageItemsList[0].paintableItemList[1])
          .text = "24 main street 658 NorthCity DarkCountry";

        std::get<PaintableTextData>(expectedGeneratedDocument.documentData.pageItemsList[0].paintableItemList[3])
          .text = "The client address : 42 liberty street 348 LargeCity BrightCountry";

        std::get<PaintableTextData>(expectedGeneratedDocument.documentData.pageItemsList[0].paintableItemList[5])
          .text = "Date : 17/12/2022, total : 1650";

        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          PageItemsAssociationData { {
            ItemAssociationData { -1, 0 },
            ItemAssociationData { -1, 1 },
            ItemAssociationData { -1, 2 },
            ItemAssociationData { -1, 3 },
            ItemAssociationData { -1, 5 },
            ItemAssociationData { -1, 6 },
          } });

        QTest::newRow("withGeneralVariables")
          << generalVariableTemplate << dataHandlerData
          << datatemplateassociation::data::DataTemplateAssociationData {} << expectedGeneratedDocument;
    }

    //----------------------------------
    // 1page_1item
    //----------------------------------
    {
        datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData {
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData { { { 4, 1 } } },
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 4, 1 } } }
        };

        //----------------------------------
        // Test with bottom repetition
        //----------------------------------
        {
            DocumentTemplateData currentDocumentTemplateData = documentTemplateData;

            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] =
              PaintableListData { PositionData { 13, 100, 3000, 3000 },
                                  PaintableListData::RepetitionDirection::BottomRepetition,
                                  PaintableListData::RepetitionDirection::NoRepetition,
                                  PaintableItemListModelData { {

                                    PaintableTextData { ColorData { 100, "black" },
                                                        PaintableRectangleData {
                                                          RectanglePositionData {
                                                            PositionData { 0, 0, 178, 160 }, 0, Qt::AbsoluteSize },
                                                          ColorData { 0, "black" },
                                                          PaintableBorderData { 1, ColorData { 100, "black" } } },
                                                        "${signature}",
                                                        "",
                                                        0,
                                                        0 } }

                                  } };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_1page_1item_bottomRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("1page_1item_bottomRepetition")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with right repetition
        //----------------------------------
        {
            DocumentTemplateData currentDocumentTemplateData = documentTemplateData;

            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 3000, 3000 },
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 10, 50, 200, 100 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${signature}",
                    "",
                    0,
                    0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_1page_1item_rightRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("1page_1item_rightRepetition")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with top repetition
        //----------------------------------
        {
            DocumentTemplateData currentDocumentTemplateData = documentTemplateData;

            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 3000, 3000 },
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 3000 - 200,
                                                         3000 - 36 - 10,    // 1000 - 36 to have the position align
                                                                            // with bottom border and -10 of margin
                                                         100,
                                                         36 },
                                          0,
                                          Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${signature}",
                                      "",
                                      0,
                                      0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_1page_1item_topRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("1page_1item_topRepetition") << currentDocumentTemplateData << dataHandlerData
                                                       << dataTemplateAssociationData << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with left repetition
        //----------------------------------
        {
            DocumentTemplateData currentDocumentTemplateData = documentTemplateData;

            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 3000, 3000 },
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 3000 - 100 - 5,
                                                         3000 - 36 - 10,    // 1000 - 36 to have the position align
                                                                            // with bottom border and -10 of margin
                                                         100,
                                                         36 },
                                          0,
                                          Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${signature}",
                                      "",
                                      0,
                                      0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_1page_1item_leftRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("1page_1item_leftRepetition")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }
    }

    {
        DocumentTemplateData currentDocumentTemplateData = documentTemplateData;
        currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] =
          PaintableListData { PositionData { 13, 100, 178, 160 },
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableListData::RepetitionDirection::NoRepetition,
                              PaintableItemListModelData {} };

        datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData {
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData { { { 4, 0 } } },
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 4, 0 } } }
        };

        GeneratedDocument expectedGeneratedDocument;

        expectedGeneratedDocument.documentData.pageSize = currentDocumentTemplateData.pageSizeData;
        expectedGeneratedDocument.documentData.pageItemsList.push_back(PaintableItemListModelData {
          { std::get<PaintableRectangleData>(
              currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(0)),
            std::get<PaintableTextData>(currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(1)),
            std::get<PaintableRectangleData>(
              currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(2)),
            std::get<PaintableTextData>(currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(3)),
            std::get<PaintableRectangleData>(
              currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(5)),
            std::get<PaintableTextData>(
              currentDocumentTemplateData.rootPaintableItemListData.paintableList.at(6)) } });

        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          PageItemsAssociationData { { ItemAssociationData { -1, 0 },
                                       ItemAssociationData { -1, 1 },
                                       ItemAssociationData { -1, 2 },
                                       ItemAssociationData { -1, 3 },
                                       ItemAssociationData { -1, 5 },
                                       ItemAssociationData { -1, 6 } } });

        QTest::newRow("withoutItems") << currentDocumentTemplateData << dataHandlerData
                                      << dataTemplateAssociationData << expectedGeneratedDocument;
    }

    {
        DocumentTemplateData currentDocumentTemplateData;
        currentDocumentTemplateData.rootPaintableItemListData = RootPaintableItemListData { {

          PaintableRectangleData { RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                                   ColorData { 0, "pink" },
                                   PaintableBorderData { 0, ColorData { 100, "black" } } },

          PaintableTextData {
            ColorData { 100, "black" },
            PaintableRectangleData { RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                     ColorData { 90, "blue" },
                                     PaintableBorderData { 0, ColorData { 0, "black" } } },
            "The company address",
            "",
            0,
            0 },

          PaintableRectangleData { RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                                   ColorData { 0, "pink" },
                                   PaintableBorderData { 0, ColorData { 100, "black" } } },

          PaintableTextData { ColorData { 100, "black" },
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                                ColorData { 90, "blue" },
                                PaintableBorderData { 0, ColorData { 0, "black" } } },
                              "The client address",
                              "",
                              0,
                              0 },

          PaintableListData {
            PositionData { 13, 100, 3000, 1000 },
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 20, 160, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${number}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 240, 60, 400, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 260, 80, 360, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${description}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 660, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 680, 50, 110, 30 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${price}",
                "",
                0,
                0 } }

            } },

          PaintableRectangleData {
            RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
            ColorData { 100, "grey" },
            PaintableBorderData { 0, ColorData { 100, "yellow" } } },

          PaintableTextData { ColorData { 100, "black" },
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                                ColorData { 90, "blue" },
                                PaintableBorderData { 0, ColorData { 0, "black" } } },
                              "Total",
                              "",
                              0,
                              0 }

        } };

        DataHandlerData currentDataHandlerData = dataHandlerData;

        datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData {
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData { { { 4, 0 } } },
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 4, 0 } } }
        };

        //------------------------------
        // Test bottom repetition
        //------------------------------
        {
            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_4pages_6items_bottomRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("4pages_6items_bottomrepetition")
              << currentDocumentTemplateData << currentDataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with right repetition
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 100, 13, 1000, 3000 },
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 10, 20, 50, 200 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 20, 40, 30, 160 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${number}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 60, 240, 50, 400 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 80, 260, 30, 360 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 30, 660, 50, 150 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 50, 680, 30, 110 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_4pages_6items_rightRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("4pages_6items_rightrepetition")
              << currentDocumentTemplateData << currentDataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with top repetition
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 3000, 1000 },
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 1000 - 50 - 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 40, 1000 - 30 - 20, 160, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${number}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 240, 1000 - 50 - 60, 400, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 260, 1000 - 30 - 80, 360, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 660, 1000 - 50 - 30, 150, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 680, 1000 - 30 - 50, 110, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_4pages_6items_topRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("4pages_6items_toprepetition")
              << currentDocumentTemplateData << currentDataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with left repetition
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 100, 13, 1000, 3000 },
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 1000 - 50 - 10, 20, 50, 200 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 1000 - 30 - 20, 40, 30, 160 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${number}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 1000 - 50 - 60, 240, 50, 400 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 1000 - 30 - 80, 260, 30, 360 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 1000 - 50 - 30, 660, 50, 150 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 1000 - 30 - 50, 680, 30, 110 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } }

                }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_4pages_6items_leftRepetition(currentDocumentTemplateData, dataHandlerData);

            QTest::newRow("4pages_6items_leftrepetition")
              << currentDocumentTemplateData << currentDataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }
    }

    //----------------------------------
    // Secondary repetition
    //----------------------------------
    {
        const datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData = {
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData { { { 4, 0 } } },
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 4, 0 } } }
        };

        DocumentTemplateData currentDocumentTemplateData = documentTemplateData;

        //----------------------------------
        // Test with bottom primary and right secondary
        //----------------------------------

        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::BottomRepetition,
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 240, 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 460, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primarybottom_secondaryright(currentDocumentTemplateData,
                                                                                 dataHandlerData);

            QTest::newRow("secondaryRepetition_primarybottom_secondaryright")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with right primary and bottom secondary
        //----------------------------------

        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableListData::RepetitionDirection::BottomRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 240, 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 460, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primaryright_secondarybottom(currentDocumentTemplateData,
                                                                                 dataHandlerData);

            QTest::newRow("secondaryRepetition_primaryright_secondarybottom")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with bottom primary and left secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::BottomRepetition,
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 2000 - 200 - 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 2000 - 200 - 240, 60, 200, 50 }, 0, Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${description}",
                                      "",
                                      0,
                                      0 },

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 2000 - 150 - 460, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${price}",
                                      "",
                                      0,
                                      0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primarybottom_secondaryleft(currentDocumentTemplateData,
                                                                                dataHandlerData);

            QTest::newRow("secondaryRepetition_primarybottom_secondaryleft")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with left primary and bottom secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableListData::RepetitionDirection::BottomRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 2000 - 200 - 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 2000 - 200 - 240, 60, 200, 50 }, 0, Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${description}",
                                      "",
                                      0,
                                      0 },

                  PaintableTextData { ColorData { 100, "black" },
                                      PaintableRectangleData {
                                        RectanglePositionData {
                                          PositionData { 2000 - 150 - 460, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                                        ColorData { 0, "black" },
                                        PaintableBorderData { 1, ColorData { 100, "black" } } },
                                      "${price}",
                                      "",
                                      0,
                                      0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primaryleft_secondarybottom(currentDocumentTemplateData,
                                                                                dataHandlerData);

            QTest::newRow("secondaryRepetition_primaryleft_secondarybottom")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with top primary and right secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 600 - 50 - 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 240, 600 - 50 - 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 460, 600 - 50 - 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primarytop_secondaryright(currentDocumentTemplateData,
                                                                              dataHandlerData);

            QTest::newRow("secondaryRepetition_primarytop_secondaryright")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with right primary and top secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 600 - 50 - 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 240, 600 - 50 - 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 460, 600 - 50 - 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primaryright_secondarytop(currentDocumentTemplateData,
                                                                              dataHandlerData);

            QTest::newRow("secondaryRepetition_primaryright_secondarytop")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with top primary and left secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData {
                      PositionData { 2000 - 200 - 20, 600 - 50 - 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData {
                        PositionData { 2000 - 200 - 240, 600 - 50 - 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData {
                        PositionData { 2000 - 150 - 460, 600 - 50 - 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primarytop_secondaryleft(currentDocumentTemplateData,
                                                                             dataHandlerData);

            QTest::newRow("secondaryRepetition_primarytop_secondaryleft")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }

        //----------------------------------
        // Test with left primary and top secondary
        //----------------------------------
        {
            currentDocumentTemplateData.rootPaintableItemListData.paintableList[4] = PaintableListData {
                PositionData { 13, 100, 2000, 600 },
                PaintableListData::RepetitionDirection::LeftRepetition,
                PaintableListData::RepetitionDirection::TopRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData {
                      PositionData { 2000 - 200 - 20, 600 - 50 - 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData {
                        PositionData { 2000 - 200 - 240, 600 - 50 - 60, 200, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData {
                        PositionData { 2000 - 150 - 460, 600 - 50 - 30, 150, 50 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } } }
            };

            GeneratedDocument expectedGeneratedDocument =
              _generateDocument_secondaryRepetition_primaryleft_secondarytop(currentDocumentTemplateData,
                                                                             dataHandlerData);

            QTest::newRow("secondaryRepetition_primaryleft_secondarytop")
              << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
              << expectedGeneratedDocument;
        }
    }

    //-------------------------------------------
    // 2 lists
    //-------------------------------------------
    {
        DocumentTemplateData currentDocumentTemplateData;

        {
            currentDocumentTemplateData.rootPaintableItemListData = RootPaintableItemListData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData { ColorData { 100, "black" },
                                  PaintableRectangleData {
                                    RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                    ColorData { 90, "blue" },
                                    PaintableBorderData { 0, ColorData { 0, "black" } } },
                                  "The company address",
                                  "",
                                  0,
                                  0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "blue" },
                  PaintableBorderData { 0, ColorData { 0, "black" } } },
                "The client address",
                "",
                0,
                0 },

              PaintableListData {
                PositionData { 13, 100, 3000, 1000 },
                PaintableListData::RepetitionDirection::BottomRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 40, 20, 160, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${number}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 240, 60, 400, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 260, 80, 360, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${description}",
                    "",
                    0,
                    0 },

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 660, 30, 150, 50 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 680, 50, 110, 30 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${price}",
                    "",
                    0,
                    0 } }

                } },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
                ColorData { 100, "grey" },
                PaintableBorderData { 0, ColorData { 100, "yellow" } } },

              PaintableListData {
                PositionData { 13, 1200, 3000, 1000 },
                PaintableListData::RepetitionDirection::RightRepetition,
                PaintableListData::RepetitionDirection::NoRepetition,
                PaintableItemListModelData { {

                  PaintableRectangleData {
                    RectanglePositionData { PositionData { 20, 10, 500, 100 }, 0, Qt::AbsoluteSize },
                    ColorData { 0, "pink" },
                    PaintableBorderData { 0, ColorData { 100, "black" } } },

                  PaintableTextData {
                    ColorData { 100, "black" },
                    PaintableRectangleData {
                      RectanglePositionData { PositionData { 40, 20, 460, 80 }, 0, Qt::AbsoluteSize },
                      ColorData { 0, "black" },
                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                    "${signature}",
                    "",
                    0,
                    0 } } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                  ColorData { 90, "blue" },
                  PaintableBorderData { 0, ColorData { 0, "black" } } },
                "Total",
                "",
                0,
                0 }

            } };
        }

        datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData {
            datatemplateassociation::data::PaintableListDataDefinitionListAssociationData {
              { { 4, 0 }, { 6, 1 } } },
            datatemplateassociation::data::PaintableListDataTableModelAssociationData { { { 4, 0 }, { 6, 1 } } }
        };

        GeneratedDocument expectedGeneratedDocument =
          _generateDocument_2lists(currentDocumentTemplateData, dataHandlerData);

        QTest::newRow("2lists") << currentDocumentTemplateData << dataHandlerData << dataTemplateAssociationData
                                << expectedGeneratedDocument;
    }
}

void Test_DocumentGenerator::generateDocument()
{
    QFETCH(DocumentTemplateData, documentTemplateData);
    QFETCH(DataHandlerData, dataHandlerData);
    QFETCH(datatemplateassociation::data::DataTemplateAssociationData, dataTemplateAssociationData);
    QFETCH(document::service::documentgenerator::GeneratedDocument, expectedGeneratedDocument);

    document::service::documentgenerator::GeneratedDocument generatedDocument =
      document::service::documentgenerator::generateDocument(
        documentTemplateData, dataHandlerData, dataTemplateAssociationData);

    QCOMPARE(generatedDocument.documentData.pageItemsList.size(),
             expectedGeneratedDocument.documentData.pageItemsList.size());
    QCOMPARE(generatedDocument.documentData.pageSize, documentTemplateData.pageSizeData);
    QCOMPARE(generatedDocument.dataItemsAssociationData.pageItemsAssociationData.size(),
             expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.size());

    // We must have the same number of page in the document and the association
    QCOMPARE(generatedDocument.dataItemsAssociationData.pageItemsAssociationData.size(),
             generatedDocument.documentData.pageItemsList.size());

    for (int pageNumber = 0; pageNumber < expectedGeneratedDocument.documentData.pageItemsList.size();
         ++pageNumber)
    {
        const PaintableItemListModelData& page = generatedDocument.documentData.pageItemsList.at(pageNumber);
        const PaintableItemListModelData& expectedPage =
          expectedGeneratedDocument.documentData.pageItemsList.at(pageNumber);

        QCOMPARE(page.paintableItemList.size(), expectedPage.paintableItemList.size());

        for (int index = 0; index < page.paintableItemList.size(); ++index)
        {
            QCOMPARE(document::service::documentgenerator::position(page.paintableItemList.at(index)),
                     document::service::documentgenerator::position(expectedPage.paintableItemList.at(index)));

            if (const PaintableTextData* textData =
                  std::get_if<PaintableTextData>(&page.paintableItemList.at(index)))
            {
                const PaintableTextData* expectedTextData =
                  std::get_if<PaintableTextData>(&expectedPage.paintableItemList.at(index));
                QCOMPARE(textData->text, expectedTextData->text);
            }

            QCOMPARE(page.paintableItemList.at(index), expectedPage.paintableItemList.at(index));
        }

        const PageItemsAssociationData& pageAssociation =
          generatedDocument.dataItemsAssociationData.pageItemsAssociationData.at(pageNumber);
        const PageItemsAssociationData& expectedPageAssociation =
          expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.at(pageNumber);

        QCOMPARE(pageAssociation.itemsAssociationData.size(), expectedPageAssociation.itemsAssociationData.size());

        // We must have the same number of items in the document and the association
        QCOMPARE(pageAssociation.itemsAssociationData.size(), page.paintableItemList.size());

        for (int itemIndex = 0; itemIndex < pageAssociation.itemsAssociationData.size(); ++itemIndex)
        {
            qDebug() << pageNumber;
            qDebug() << itemIndex;
            const ItemAssociationData& itemAssociation = pageAssociation.itemsAssociationData.at(itemIndex);
            const ItemAssociationData& expectedItemAssociation =
              expectedPageAssociation.itemsAssociationData.at(itemIndex);

            QCOMPARE(itemAssociation.templateListIndex, expectedItemAssociation.templateListIndex);
            QCOMPARE(itemAssociation.templateItemIndex, expectedItemAssociation.templateItemIndex);
            QCOMPARE(itemAssociation.modelIndex, expectedItemAssociation.modelIndex);
            QCOMPARE(itemAssociation.rowIndex, expectedItemAssociation.rowIndex);
        }
    }

    QCOMPARE(generatedDocument.documentData, expectedGeneratedDocument.documentData);
}

void Test_DocumentGenerator::itemsPerPage_data()
{
    QTest::addColumn<PaintableListData>("paintableListData");
    QTest::addColumn<document::service::documentgenerator::ItemsPerPage>("expectedItemsPerPage");

    {
        QTest::newRow("emptyList") << PaintableListData {}
                                   << document::service::documentgenerator::ItemsPerPage {};
    }

    {
        PaintableListData paintableListData =
          PaintableListData { PositionData { 13, 100, 3000, 3000 },
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableListData::RepetitionDirection::NoRepetition,
                              PaintableItemListModelData { {

                                PaintableTextData { ColorData { 100, "black" },
                                                    PaintableRectangleData {
                                                      RectanglePositionData {
                                                        PositionData { 20, 20, 160, 180 }, 0, Qt::AbsoluteSize },
                                                      ColorData { 0, "black" },
                                                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                                                    "${signature}",
                                                    "",
                                                    0,
                                                    0 } }

                              } };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 15, 0 };

        QTest::newRow("1items_exactfill_bottom") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData =
          PaintableListData { PositionData { 13, 100, 3000, 3000 },
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableListData::RepetitionDirection::NoRepetition,
                              PaintableItemListModelData { {

                                PaintableTextData { ColorData { 100, "black" },
                                                    PaintableRectangleData {
                                                      RectanglePositionData {
                                                        PositionData { 20, 20, 160, 240 }, 0, Qt::AbsoluteSize },
                                                      ColorData { 0, "black" },
                                                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                                                    "${signature}",
                                                    "",
                                                    0,
                                                    0 } }

                              } };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 0 };

        QTest::newRow("1items_nonexactfill_bottom") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 0 };

        QTest::newRow("3items_bottom") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 160 - 20, 3000 - 180 - 20, 160, 180 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 15, 0 };

        QTest::newRow("1items_exactfill_top") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 160 - 20, 3000 - 240 - 20, 160, 240 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 0 };

        QTest::newRow("1items_nonexactfill_top") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 240 - 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 200 - 40, 3000 - 120 - 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 200 - 20, 3000 - 120 - 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 0 };

        QTest::newRow("3items_top") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData =
          PaintableListData { PositionData { 100, 13, 3000, 3000 },
                              PaintableListData::RepetitionDirection::RightRepetition,
                              PaintableListData::RepetitionDirection::NoRepetition,
                              PaintableItemListModelData { {

                                PaintableTextData { ColorData { 100, "black" },
                                                    PaintableRectangleData {
                                                      RectanglePositionData {
                                                        PositionData { 20, 20, 180, 160 }, 0, Qt::AbsoluteSize },
                                                      ColorData { 0, "black" },
                                                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                                                    "${signature}",
                                                    "",
                                                    0,
                                                    0 } }

                              } };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 15, 0 };

        QTest::newRow("1items_exactfill_right") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData =
          PaintableListData { PositionData { 13, 100, 3000, 3000 },
                              PaintableListData::RepetitionDirection::RightRepetition,
                              PaintableListData::RepetitionDirection::NoRepetition,
                              PaintableItemListModelData { {

                                PaintableTextData { ColorData { 100, "black" },
                                                    PaintableRectangleData {
                                                      RectanglePositionData {
                                                        PositionData { 20, 20, 240, 160 }, 0, Qt::AbsoluteSize },
                                                      ColorData { 0, "black" },
                                                      PaintableBorderData { 1, ColorData { 100, "black" } } },
                                                    "${signature}",
                                                    "",
                                                    0,
                                                    0 } }

                              } };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 0 };

        QTest::newRow("1items_nonexactfill_right") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 100, 13, 3000, 3000 },
            PaintableListData::RepetitionDirection::RightRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 20, 160, 240 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 40, 120, 200 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 200, 20, 120, 200 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 0 };

        QTest::newRow("3items_right") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 100, 13, 3000, 3000 },
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 180 - 20, 3000 - 160 - 20, 180, 160 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 15, 0 };

        QTest::newRow("1items_exactfill_left") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 240 - 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 0 };

        QTest::newRow("1items_nonexactfill_left") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 100, 13, 3000, 3000 },
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableListData::RepetitionDirection::NoRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 160 - 20, 3000 - 240 - 20, 160, 240 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 120 - 40, 3000 - 200 - 40, 120, 200 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 120 - 200, 3000 - 200 - 20, 120, 200 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 0 };

        QTest::newRow("3items_left") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableListData::RepetitionDirection::RightRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 11 };

        QTest::newRow("3items_bottom_right") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 100, 13, 3000, 3000 },
            PaintableListData::RepetitionDirection::RightRepetition,
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 20, 160, 240 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 40, 120, 200 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 200, 20, 120, 200 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 11 };

        QTest::newRow("3items_right_bottom") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 3000 - 240 - 20, 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 3000 - 200 - 40, 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 3000 - 200 - 20, 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 11 };

        QTest::newRow("3items_bottom_left") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableListData::RepetitionDirection::BottomRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 3000 - 240 - 20, 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 3000 - 200 - 40, 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 3000 - 200 - 20, 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 9 };

        QTest::newRow("3items_left_bottom") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableListData::RepetitionDirection::RightRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 3000 - 120 - 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 3000 - 120 - 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 11 };

        QTest::newRow("3items_top_right") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::RightRepetition,
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData { PositionData { 40, 3000 - 120 - 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData { PositionData { 20, 3000 - 120 - 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 9 };

        QTest::newRow("3items_right_top") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 240 - 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 200 - 40, 3000 - 120 - 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 200 - 20, 3000 - 120 - 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 9, 11 };

        QTest::newRow("3items_top_left") << paintableListData << itemsPerPage;
    }

    {
        PaintableListData paintableListData = PaintableListData {
            PositionData { 13, 100, 3000, 3000 },
            PaintableListData::RepetitionDirection::LeftRepetition,
            PaintableListData::RepetitionDirection::TopRepetition,
            PaintableItemListModelData { {

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 240 - 20, 3000 - 160 - 20, 240, 160 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } },

              PaintableTextData {
                ColorData { 100, "black" },
                PaintableRectangleData {
                  RectanglePositionData {
                    PositionData { 3000 - 200 - 40, 3000 - 120 - 40, 200, 120 }, 0, Qt::AbsoluteSize },
                  ColorData { 0, "black" },
                  PaintableBorderData { 1, ColorData { 100, "black" } } },
                "${signature}",
                "",
                0,
                0 },

              PaintableRectangleData {
                RectanglePositionData {
                  PositionData { 3000 - 200 - 20, 3000 - 120 - 200, 200, 120 }, 0, Qt::AbsoluteSize },
                ColorData { 0, "pink" },
                PaintableBorderData { 0, ColorData { 100, "black" } } } }

            }
        };

        document::service::documentgenerator::ItemsPerPage itemsPerPage { 11, 9 };

        QTest::newRow("3items_top_left") << paintableListData << itemsPerPage;
    }
}

void Test_DocumentGenerator::itemsPerPage()
{
    QFETCH(PaintableListData, paintableListData);
    QFETCH(document::service::documentgenerator::ItemsPerPage, expectedItemsPerPage);

    document::service::documentgenerator::ItemsPerPage itemsPerPage =
      document::service::documentgenerator::calculateItemsPerPage(paintableListData);

    QCOMPARE(itemsPerPage.primaryRepetition, expectedItemsPerPage.primaryRepetition);
    QCOMPARE(itemsPerPage.secondaryRepetition, expectedItemsPerPage.secondaryRepetition);
}

void Test_DocumentGenerator::itemPositionFromIndex_data()
{
    QTest::addColumn<document::service::documentgenerator::ItemsPerPage>("itemsPerPage");
    QTest::addColumn<int>("itemIndex");
    QTest::addColumn<document::service::documentgenerator::ItemPosition>("expectedItemPosition");

    QTest::newRow("empty") << document::service::documentgenerator::ItemsPerPage {} << 42
                           << document::service::documentgenerator::ItemPosition {};
    QTest::newRow("primaryrepetition_index2") << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 2
                                              << document::service::documentgenerator::ItemPosition { 0, 2, 0 };
    QTest::newRow("primaryrepetition_index10")
      << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 10
      << document::service::documentgenerator::ItemPosition { 1, 0, 0 };
    QTest::newRow("primaryrepetition_index12")
      << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 12
      << document::service::documentgenerator::ItemPosition { 1, 2, 0 };
    QTest::newRow("primaryrepetition_index15")
      << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 15
      << document::service::documentgenerator::ItemPosition { 1, 5, 0 };
    QTest::newRow("primaryrepetition_index20")
      << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 20
      << document::service::documentgenerator::ItemPosition { 2, 0, 0 };
    QTest::newRow("primaryrepetition_index42")
      << document::service::documentgenerator::ItemsPerPage { 10, 0 } << 42
      << document::service::documentgenerator::ItemPosition { 4, 2, 0 };

    QTest::newRow("secondaryrepetition_index2")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 2
      << document::service::documentgenerator::ItemPosition { 0, 2, 0 };
    QTest::newRow("secondaryrepetition_index10")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 10
      << document::service::documentgenerator::ItemPosition { 0, 0, 1 };
    QTest::newRow("secondaryrepetition_index12")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 12
      << document::service::documentgenerator::ItemPosition { 0, 2, 1 };
    QTest::newRow("secondaryrepetition_index25")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 25
      << document::service::documentgenerator::ItemPosition { 0, 5, 2 };
    QTest::newRow("secondaryrepetition_index49")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 49
      << document::service::documentgenerator::ItemPosition { 0, 9, 4 };
    QTest::newRow("secondaryrepetition_index50")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 50
      << document::service::documentgenerator::ItemPosition { 1, 0, 0 };
    QTest::newRow("secondaryrepetition_index69")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 69
      << document::service::documentgenerator::ItemPosition { 1, 9, 1 };
    QTest::newRow("secondaryrepetition_index143")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 143
      << document::service::documentgenerator::ItemPosition { 2, 3, 4 };
    QTest::newRow("secondaryrepetition_index666")
      << document::service::documentgenerator::ItemsPerPage { 10, 5 } << 666
      << document::service::documentgenerator::ItemPosition { 13, 6, 1 };
}

void Test_DocumentGenerator::itemPositionFromIndex()
{
    QFETCH(document::service::documentgenerator::ItemsPerPage, itemsPerPage);
    QFETCH(int, itemIndex);
    QFETCH(document::service::documentgenerator::ItemPosition, expectedItemPosition);

    document::service::documentgenerator::ItemPosition itemPosition =
      document::service::documentgenerator::calculateItemPositionFromIndex(itemsPerPage, itemIndex);

    QCOMPARE(itemPosition.page, expectedItemPosition.page);
    QCOMPARE(itemPosition.primaryRepetitionIndex, expectedItemPosition.primaryRepetitionIndex);
    QCOMPARE(itemPosition.secondaryRepetitionIndex, expectedItemPosition.secondaryRepetitionIndex);
}

void Test_DocumentGenerator::listChildrenRect()
{
    // No list
    QCOMPARE(document::service::documentgenerator::childrenRectList(PaintableItemListModelData {}),
             PositionData());

    //----------------------------------------------
    // 1 item
    //----------------------------------------------
    PaintableItemListModelData paintableListData = PaintableItemListModelData { {

      PaintableTextData {
        ColorData {},
        PaintableRectangleData { RectanglePositionData { PositionData { 10, 20, 30, 40 }, 0, Qt::AbsoluteSize },
                                 ColorData {},
                                 PaintableBorderData {} },
        "",
        "",
        0,
        0 } }

    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(10, 20, 30, 40));

    paintableListData = PaintableItemListModelData { { PaintableRectangleData {
      RectanglePositionData { PositionData { 10, 20, 150, 200 }, 0, Qt::AbsoluteSize },
      ColorData {},
      PaintableBorderData {} } }

    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(10, 20, 150, 200));

    //----------------------------------------------
    // 2 items one in the other
    //----------------------------------------------
    paintableListData = PaintableItemListModelData {
        { PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 10, 20, 50, 100 }, 0, Qt::AbsoluteSize },
                                ColorData {},
                                PaintableBorderData {} },
                              "",
                              "",
                              0,
                              0 },
          PaintableRectangleData { RectanglePositionData { PositionData { 15, 25, 25, 50 }, 0, Qt::AbsoluteSize },
                                   ColorData {},
                                   PaintableBorderData {} } }

    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(10, 20, 50, 100));

    paintableListData = PaintableItemListModelData {
        { PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 30, 40, 50, 60 }, 0, Qt::AbsoluteSize },
                                ColorData {},
                                PaintableBorderData {} },
                              "",
                              "",
                              0,
                              0 },
          PaintableRectangleData { RectanglePositionData { PositionData { 5, 10, 100, 500 }, 0, Qt::AbsoluteSize },
                                   ColorData {},
                                   PaintableBorderData {} } }

    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(5, 10, 100, 500));

    //----------------------------------------------
    // multiple items
    //----------------------------------------------
    paintableListData = PaintableItemListModelData {
        { PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 10, 20, 100, 200 }, 0, Qt::AbsoluteSize },
                                ColorData {},
                                PaintableBorderData {} },
                              "",
                              "",
                              0,
                              0 },
          PaintableRectangleData {
            RectanglePositionData { PositionData { 100, 200, 100, 200 }, 0, Qt::AbsoluteSize },
            ColorData {},
            PaintableBorderData {} } }

    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(10, 20, 190, 380));

    paintableListData = PaintableItemListModelData { {

      PaintableRectangleData { RectanglePositionData { PositionData { 150, 10, 100, 150 }, 0, Qt::AbsoluteSize },
                               ColorData {},
                               PaintableBorderData {} },

      PaintableRectangleData { RectanglePositionData { PositionData { 300, 10, 100, 100 }, 0, Qt::AbsoluteSize },
                               ColorData {},
                               PaintableBorderData {} },

      PaintableTextData {
        ColorData {},
        PaintableRectangleData { RectanglePositionData { PositionData { 10, 10, 100, 200 }, 0, Qt::AbsoluteSize },
                                 ColorData {},
                                 PaintableBorderData {} },
        "",
        "",
        0,
        0 },

      PaintableRectangleData { RectanglePositionData { PositionData { 500, 10, 50, 50 }, 0, Qt::AbsoluteSize },
                               ColorData {},
                               PaintableBorderData {} }

    } };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(10, 10, 540, 200));

    paintableListData = PaintableItemListModelData {
        { PaintableRectangleData {
            RectanglePositionData { PositionData { 20, 940, 200, 50 }, 0, Qt::AbsoluteSize },
            ColorData {},
            PaintableBorderData {} },

          PaintableRectangleData {
            RectanglePositionData { PositionData { 240, 890, 400, 50 }, 0, Qt::AbsoluteSize },
            ColorData {},
            PaintableBorderData {} },

          PaintableTextData { ColorData {},
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 660, 920, 150, 50 }, 0, Qt::AbsoluteSize },
                                ColorData {},
                                PaintableBorderData {} },
                              "",
                              "",
                              0,
                              0 }

        }
    };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(20, 890, 790, 100));

    paintableListData = PaintableItemListModelData { {

      PaintableRectangleData { RectanglePositionData { PositionData { 940, 20, 50, 200 }, 0, Qt::AbsoluteSize },
                               ColorData {},
                               PaintableBorderData {} },

      PaintableRectangleData { RectanglePositionData { PositionData { 890, 240, 50, 400 }, 0, Qt::AbsoluteSize },
                               ColorData {},
                               PaintableBorderData {} },

      PaintableTextData {
        ColorData {},
        PaintableRectangleData { RectanglePositionData { PositionData { 920, 660, 50, 150 }, 0, Qt::AbsoluteSize },
                                 ColorData {},
                                 PaintableBorderData {} },
        "",
        "",
        0,
        0 }

    } };

    QCOMPARE(document::service::documentgenerator::childrenRectList(paintableListData),
             PositionData(890, 20, 100, 790));
}

DataHandlerData Test_DocumentGenerator::_generateDocument_dataHandler_data() const
{
    return DataHandlerData {
        DataDefinitionsHandlerData {
          DataDefinitionListData {
            "General",
            { DataDefinitionData {
                "date", "The date", "The date of the billing", DataDefinitionData::DataType::StringType },
              DataDefinitionData { "total",
                                   "The total price",
                                   "The sum of the price of all the products",
                                   DataDefinitionData::DataType::NumberType },
              DataDefinitionData { "companyAddress",
                                   "The address of the company",
                                   "The address of the company emitting the billing",
                                   DataDefinitionData::DataType::NumberType },
              DataDefinitionData { "clientAddress",
                                   "The address of the client",
                                   "The address of the client for who the billing is emitted",
                                   DataDefinitionData::DataType::NumberType } } },
          ListDataDefinitionListData {
            { ListDataDefinitionData {
                DataDefinitionListData { "Products",
                                         { DataDefinitionData { "number",
                                                                "The number of products",
                                                                "The number of units of the product",
                                                                DataDefinitionData::DataType::NumberType },
                                           DataDefinitionData { "description",
                                                                "The description of product",
                                                                "The detailled description of the product",
                                                                DataDefinitionData::DataType::StringType },
                                           DataDefinitionData { "price",
                                                                "The price of product",
                                                                "The price of one unit of the product",
                                                                DataDefinitionData::DataType::StringType } } },

                DataTablesData { { DataTableData { { 0, "" } } } } },
              ListDataDefinitionData {
                DataDefinitionListData {
                  "Signature",
                  { DataDefinitionData {
                    "signature", "The signature", "", DataDefinitionData::DataType::StringType } } },

                DataTablesData { { DataTableData { { 0, "" } } } } } } } },

        UserDataHandlerData {
          DataTablesData { { DataTableData { { 0, "17/12/2022" },
                                             { 1, "1650" },
                                             { 2, "24 main street 658 NorthCity DarkCountry" },
                                             { 3, "42 liberty street 348 LargeCity BrightCountry" } } } },
          DataTableModelListData {
            { DataTablesData { { DataTableData { { 0, "2" }, { 1, "Bell peppers 1" }, { 2, "1.99" } },
                                 DataTableData { { 0, "5" }, { 1, "Eggs 2" }, { 2, "0.34" } },
                                 DataTableData {},
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 4" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 5" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 6" }, { 2, "1.75" } },
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 7" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 8" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 9" }, { 2, "1.75" } },
                                 DataTableData { { 0, "2" }, { 1, "Bell peppers 10" }, { 2, "1.99" } },
                                 DataTableData { { 0, "5" }, { 1, "Eggs 11" }, { 2, "0.34" } },
                                 DataTableData {},
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 13" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 14" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 15" }, { 2, "1.75" } },
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 16" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 17" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 18" }, { 2, "1.75" } },
                                 DataTableData { { 0, "2" }, { 1, "Bell peppers 19" }, { 2, "1.99" } },
                                 DataTableData { { 0, "5" }, { 1, "Eggs 20" }, { 2, "0.34" } },
                                 DataTableData {},
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 21" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 22" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 23" }, { 2, "1.75" } },
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 24" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 25" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 26" }, { 2, "1.75" } },
                                 DataTableData { { 0, "1" }, { 1, "Brocoli 27" }, { 2, "3.99" } },
                                 DataTableData { { 0, "3" }, { 1, "Potatoes 28" }, { 2, "2.5" } },
                                 DataTableData { { 0, "5" }, { 1, "Greens 29" }, { 2, "1.75" } },
                                 DataTableData { { 0, "2" }, { 1, "Bell peppers 30" }, { 2, "1.99" } },
                                 DataTableData { { 0, "5" }, { 1, "Eggs 31" }, { 2, "0.34" } } } },
              DataTablesData { { DataTableData { { 0, "Bob" } },
                                 DataTableData { { 0, "John" } },
                                 DataTableData {},
                                 DataTableData { { 0, "Dylan" } },
                                 DataTableData { { 0, "Lola" } },
                                 DataTableData { { 0, "Nina" } },
                                 DataTableData { { 0, "Lou" } },
                                 DataTableData { { 0, "Marylin" } } } } } } }

    };
}

DocumentTemplateData Test_DocumentGenerator::_generateDocument_documentTemplate_data() const
{
    return {
        PageSizeData {},
        RootPaintableItemListData { {

          PaintableRectangleData { RectanglePositionData { PositionData { 5, 5, 85, 37 }, 0, Qt::AbsoluteSize },
                                   ColorData { 0, "pink" },
                                   PaintableBorderData { 0, ColorData { 100, "black" } } },

          PaintableTextData {
            ColorData { 100, "black" },
            PaintableRectangleData { RectanglePositionData { PositionData { 11, 9, 74, 29 }, 0, Qt::AbsoluteSize },
                                     ColorData { 90, "blue" },
                                     PaintableBorderData { 0, ColorData { 0, "black" } } },
            "The company address",
            "",
            0,
            0 },

          PaintableRectangleData { RectanglePositionData { PositionData { 110, 52, 85, 37 }, 0, Qt::AbsoluteSize },
                                   ColorData { 0, "pink" },
                                   PaintableBorderData { 0, ColorData { 100, "black" } } },

          PaintableTextData { ColorData { 100, "black" },
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 115, 56, 74, 29 }, 0, Qt::AbsoluteSize },
                                ColorData { 90, "blue" },
                                PaintableBorderData { 0, ColorData { 0, "black" } } },
                              "The client address",
                              "",
                              0,
                              0 },

          PaintableListData { PositionData { 13, 100, 2000, 600 },
                              PaintableListData::RepetitionDirection::BottomRepetition,
                              PaintableListData::RepetitionDirection::RightRepetition,
                              PaintableItemListModelData { {

                                PaintableRectangleData {
                                  RectanglePositionData { PositionData { 20, 10, 200, 50 }, 0, Qt::AbsoluteSize },
                                  ColorData { 0, "pink" },
                                  PaintableBorderData { 0, ColorData { 100, "black" } } } }

                              } },

          PaintableRectangleData {
            RectanglePositionData { PositionData { 135, 265, 57, 63 }, 0, Qt::AbsoluteSize },
            ColorData { 100, "grey" },
            PaintableBorderData { 0, ColorData { 100, "yellow" } } },

          PaintableTextData { ColorData { 100, "black" },
                              PaintableRectangleData {
                                RectanglePositionData { PositionData { 138, 268, 51, 7 }, 0, Qt::AbsoluteSize },
                                ColorData { 90, "blue" },
                                PaintableBorderData { 0, ColorData { 0, "black" } } },
                              "Total",
                              "",
                              0,
                              0 }

        } }
    };
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_withoutVariables(
  const DocumentTemplateData& documentTemplateData) const
{
    GeneratedDocument expectedGeneratedDocument;

    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;
    expectedGeneratedDocument.documentData.pageItemsList.push_back(PaintableItemListModelData {
      { std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)),
        std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)),
        std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)),
        std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)),
        std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)),
        std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)) } });

    expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData = { PageItemsAssociationData {
      { ItemAssociationData { -1, 0 },
        ItemAssociationData { -1, 1 },
        ItemAssociationData { -1, 2 },
        ItemAssociationData { -1, 3 },
        ItemAssociationData { -1, 5 },
        ItemAssociationData { -1, 6 } } } };

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_1page_1item_bottomRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList.at(4));

    const PaintableTextData& paintableTextItem =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList.at(0));

    const PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    PaintableItemListModelData expectedPage;
    PageItemsAssociationData expectedPageAssociation;

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

    PaintableTextData expectedPaintableTextItem = paintableTextItem;

    PositionData position = expectedPaintableTextItem.background.position.position;
    position.x            = expectedPaintableTextItem.background.position.position.x + paintableList.position.x;
    position.y            = (expectedPaintableTextItem.background.position.position.y + paintableList.position.y
                  - childrenRectList.height - childrenRectList.y);
    position.width        = expectedPaintableTextItem.background.position.position.width;
    position.height       = expectedPaintableTextItem.background.position.position.height;
    expectedPaintableTextItem.background.position.position = position;

    int documentItemIndex = 4;
    for (int rowIndex = 0;
         rowIndex
         < dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.size();
         ++rowIndex)
    {
        const DataTableData& dataTableData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.at(rowIndex);

        expectedPaintableTextItem.text = paintableTextItem.text;
        expectedPaintableTextItem.background.position.position.y =
          (expectedPaintableTextItem.background.position.position.y + childrenRectList.height
           + childrenRectList.y);
        expectedPaintableTextItem.background.position.position.width =
          (paintableTextItem.background.position.position.width);
        expectedPaintableTextItem.background.position.position.height =
          (paintableTextItem.background.position.position.height);

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 1, rowIndex });

        expectedPaintableTextItem.text = dataTableData.value(0);

        expectedPage.paintableItemList.append(expectedPaintableTextItem);

        ++documentItemIndex;
    }

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

    expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
    expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(expectedPageAssociation);

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_1page_1item_rightRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList.at(4));

    const PaintableTextData& paintableTextItem =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList.at(0));

    const PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    PaintableItemListModelData expectedPage;
    PageItemsAssociationData expectedPageAssociation;

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

    PaintableTextData expectedPaintableTextItem = paintableTextItem;

    expectedPaintableTextItem.background.position.position = PositionData(23 - 210, 150, 200, 100);

    for (int rowIndex = 0;
         rowIndex
         < dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.size();
         ++rowIndex)
    {
        const DataTableData& dataTableData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.at(rowIndex);

        expectedPaintableTextItem.text = paintableTextItem.text;

        expectedPaintableTextItem.background.position.position.x =
          (expectedPaintableTextItem.background.position.position.x + childrenRectList.width + childrenRectList.x);
        expectedPaintableTextItem.background.position.position.width =
          (paintableTextItem.background.position.position.width);
        expectedPaintableTextItem.background.position.position.height =
          (paintableTextItem.background.position.position.height);

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 1, rowIndex });

        expectedPaintableTextItem.text = dataTableData.value(0);

        expectedPage.paintableItemList.append(expectedPaintableTextItem);
    }

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

    expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
    expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(expectedPageAssociation);

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_1page_1item_topRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList.at(4));

    const PaintableTextData& paintableTextItem =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList.at(0));

    const PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    PaintableItemListModelData expectedPage;
    PageItemsAssociationData expectedPageAssociation;

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

    PaintableTextData expectedPaintableTextItem = paintableTextItem;

    expectedPaintableTextItem.background.position.position =
      PositionData(paintableList.position.width - 200 + paintableList.position.x,
                   paintableList.position.height - 36 - 10 + paintableList.position.y + 46,
                   100,
                   36);

    for (int rowIndex = 0;
         rowIndex
         < dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.size();
         ++rowIndex)
    {
        const DataTableData& dataTableData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.at(rowIndex);

        expectedPaintableTextItem.text = paintableTextItem.text;

        expectedPaintableTextItem.background.position.position.y =
          (expectedPaintableTextItem.background.position.position.y - paintableList.position.height
           + childrenRectList.y);
        expectedPaintableTextItem.background.position.position.width =
          (paintableTextItem.background.position.position.width);
        expectedPaintableTextItem.background.position.position.height =
          (paintableTextItem.background.position.position.height);

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 1, rowIndex });

        expectedPaintableTextItem.text = dataTableData.value(0);

        expectedPage.paintableItemList.append(expectedPaintableTextItem);
    }

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

    expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
    expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(expectedPageAssociation);

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_1page_1item_leftRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList.at(4));

    const PaintableTextData& paintableTextItem =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList.at(0));

    const PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    PaintableItemListModelData expectedPage;
    PageItemsAssociationData expectedPageAssociation;

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

    PaintableTextData expectedPaintableTextItem = paintableTextItem;

    expectedPaintableTextItem.background.position.position =
      PositionData(paintableList.position.width - 100 - 5 + paintableList.position.x + 100 + 5,
                   paintableList.position.height - 36 - 10 + paintableList.position.y,
                   70,
                   36);

    for (int rowIndex = 0;
         rowIndex
         < dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.size();
         ++rowIndex)
    {
        const DataTableData& dataTableData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1).dataTables.at(rowIndex);

        expectedPaintableTextItem.text = paintableTextItem.text;

        expectedPaintableTextItem.background.position.position.x =
          (expectedPaintableTextItem.background.position.position.x - paintableList.position.width
           + childrenRectList.x);
        expectedPaintableTextItem.background.position.position.width =
          (paintableTextItem.background.position.position.width);
        expectedPaintableTextItem.background.position.position.height =
          (paintableTextItem.background.position.position.height);

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 1, rowIndex });

        expectedPaintableTextItem.text = dataTableData.value(0);

        expectedPage.paintableItemList.append(expectedPaintableTextItem);
    }

    expectedPage.paintableItemList.append(
      std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
    expectedPage.paintableItemList.append(
      std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
    expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

    expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
    expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(expectedPageAssociation);

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_4pages_6items_bottomRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableRectangleData& paintableRectangleItem2 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[2]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[3]);
    const PaintableRectangleData& paintableRectangleItem3 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[4]);
    const PaintableTextData& paintableTextItem3 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[5]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    for (int pageNumber = 0; pageNumber < 4; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData expectedPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData expectedPaintableTextItem1           = paintableTextItem1;
        PaintableRectangleData expectedPaintableRectangleItem2 = paintableRectangleItem2;
        PaintableTextData expectedPaintableTextItem2           = paintableTextItem2;
        PaintableRectangleData expectedPaintableRectangleItem3 = paintableRectangleItem3;
        PaintableTextData expectedPaintableTextItem3           = paintableTextItem3;

        PositionData position = expectedPaintableRectangleItem1.position.position;
        position.x            = expectedPaintableRectangleItem1.position.position.x + paintableList.position.x;
        position.y            = (expectedPaintableRectangleItem1.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width        = expectedPaintableRectangleItem1.position.position.width;
        position.height       = expectedPaintableRectangleItem1.position.position.height;
        expectedPaintableRectangleItem1.position.position = position;

        position        = expectedPaintableTextItem1.background.position.position;
        position.x      = expectedPaintableTextItem1.background.position.position.x + paintableList.position.x;
        position.y      = (expectedPaintableTextItem1.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = expectedPaintableTextItem1.background.position.position.width;
        position.height = expectedPaintableTextItem1.background.position.position.height;
        expectedPaintableTextItem1.background.position.position = position;

        position        = expectedPaintableRectangleItem2.position.position;
        position.x      = expectedPaintableRectangleItem2.position.position.x + paintableList.position.x;
        position.y      = (expectedPaintableRectangleItem2.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = expectedPaintableRectangleItem2.position.position.width;
        position.height = expectedPaintableRectangleItem2.position.position.height;
        expectedPaintableRectangleItem2.position.position = position;

        position        = expectedPaintableTextItem2.background.position.position;
        position.x      = expectedPaintableTextItem2.background.position.position.x + paintableList.position.x;
        position.y      = (expectedPaintableTextItem2.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = expectedPaintableTextItem2.background.position.position.width;
        position.height = expectedPaintableTextItem2.background.position.position.height;
        expectedPaintableTextItem2.background.position.position = position;

        position        = expectedPaintableRectangleItem3.position.position;
        position.x      = expectedPaintableRectangleItem3.position.position.x + paintableList.position.x;
        position.y      = (expectedPaintableRectangleItem3.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = expectedPaintableRectangleItem3.position.position.width;
        position.height = expectedPaintableRectangleItem3.position.position.height;
        expectedPaintableRectangleItem3.position.position = position;

        position        = expectedPaintableTextItem3.background.position.position;
        position.x      = expectedPaintableTextItem3.background.position.position.x + paintableList.position.x;
        position.y      = (expectedPaintableTextItem3.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = expectedPaintableTextItem3.background.position.position.width;
        position.height = expectedPaintableTextItem3.background.position.position.height;
        expectedPaintableTextItem3.background.position.position = position;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int dataIndex = pageNumber * 9;
             dataIndex < (pageNumber + 1) * 9 && dataIndex < dataTablesData.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

            expectedPaintableRectangleItem1.position.position.y =
              (expectedPaintableRectangleItem1.position.position.y + childrenRectList.height + childrenRectList.y);
            expectedPaintableRectangleItem1.position.position.width =
              (paintableRectangleItem1.position.position.width);
            expectedPaintableRectangleItem1.position.position.height =
              (paintableRectangleItem1.position.position.height);

            expectedPage.paintableItemList.append(expectedPaintableRectangleItem1);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });

            expectedPaintableTextItem1.text = paintableTextItem1.text;
            expectedPaintableTextItem1.background.position.position.y =
              (expectedPaintableTextItem1.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            expectedPaintableTextItem1.background.position.position.width =
              (paintableTextItem1.background.position.position.width);
            expectedPaintableTextItem1.background.position.position.height =
              (paintableTextItem1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

            expectedPaintableTextItem1.text = dataTableData.value(0);

            expectedPage.paintableItemList.append(expectedPaintableTextItem1);

            expectedPaintableRectangleItem2.position.position.y =
              (expectedPaintableRectangleItem2.position.position.y + childrenRectList.height + childrenRectList.y);
            expectedPaintableRectangleItem2.position.position.width =
              (paintableRectangleItem2.position.position.width);
            expectedPaintableRectangleItem2.position.position.height =
              (paintableRectangleItem2.position.position.height);

            expectedPage.paintableItemList.append(expectedPaintableRectangleItem2);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

            expectedPaintableTextItem2.text = paintableTextItem2.text;
            expectedPaintableTextItem2.background.position.position.y =
              (expectedPaintableTextItem2.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            expectedPaintableTextItem2.background.position.position.width =
              (paintableTextItem2.background.position.position.width);
            expectedPaintableTextItem2.background.position.position.height =
              (paintableTextItem2.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 3, 0, dataIndex });

            expectedPaintableTextItem2.text = dataTableData.value(1);

            expectedPage.paintableItemList.append(expectedPaintableTextItem2);

            expectedPaintableRectangleItem3.position.position.y =
              (expectedPaintableRectangleItem3.position.position.y + childrenRectList.height + childrenRectList.y);
            expectedPaintableRectangleItem3.position.position.width =
              (paintableRectangleItem3.position.position.width);
            expectedPaintableRectangleItem3.position.position.height =
              (paintableRectangleItem3.position.position.height);

            expectedPage.paintableItemList.append(expectedPaintableRectangleItem3);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 4, 0, dataIndex });

            expectedPaintableTextItem3.text = paintableTextItem3.text;
            expectedPaintableTextItem3.background.position.position.y =
              (expectedPaintableTextItem3.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            expectedPaintableTextItem3.background.position.position.width =
              (paintableTextItem3.background.position.position.width);
            expectedPaintableTextItem3.background.position.position.height =
              (paintableTextItem3.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 5, 0, dataIndex });

            expectedPaintableTextItem3.text = dataTableData.value(2);
            expectedPage.paintableItemList.append(expectedPaintableTextItem3);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    qDebug() << "GenereatedDocument{" << expectedGeneratedDocument.documentData << ','
             << expectedGeneratedDocument.dataItemsAssociationData << '}';

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_4pages_6items_topRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableRectangleData& paintableRectangleItem2 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[2]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[3]);
    const PaintableRectangleData& paintableRectangleItem3 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[4]);
    const PaintableTextData& paintableTextItem3 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[5]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    for (int pageNumber = 0; pageNumber < 4; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableRectangleData currentPaintableRectangleItem2 = paintableRectangleItem2;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;
        PaintableRectangleData currentPaintableRectangleItem3 = paintableRectangleItem3;
        PaintableTextData currentPaintableTextItem3           = paintableTextItem3;

        currentPaintableRectangleItem1 = paintableRectangleItem1;
        PositionData position          = paintableRectangleItem1.position.position;
        position.x                     = paintableRectangleItem1.position.position.x + paintableList.position.x;
        position.y                     = (paintableRectangleItem1.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width                 = paintableRectangleItem1.position.position.width;
        position.height                = paintableRectangleItem1.position.position.height;
        currentPaintableRectangleItem1.position.position = position;

        currentPaintableTextItem1 = paintableTextItem1;
        position                  = currentPaintableTextItem1.background.position.position;
        position.x                = paintableTextItem1.background.position.position.x + paintableList.position.x;
        position.y                = (paintableTextItem1.background.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width            = paintableTextItem1.background.position.position.width;
        position.height           = paintableTextItem1.background.position.position.height;
        currentPaintableTextItem1.background.position.position = position;

        currentPaintableRectangleItem2 = paintableRectangleItem2;
        position                       = paintableRectangleItem2.position.position;
        position.x                     = paintableRectangleItem2.position.position.x + paintableList.position.x;
        position.y                     = (paintableRectangleItem2.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width                 = paintableRectangleItem2.position.position.width;
        position.height                = paintableRectangleItem2.position.position.height;
        currentPaintableRectangleItem2.position.position = position;

        currentPaintableTextItem2 = paintableTextItem2;
        position                  = paintableTextItem2.background.position.position;
        position.x                = paintableTextItem2.background.position.position.x + paintableList.position.x;
        position.y                = (paintableTextItem2.background.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width            = paintableTextItem2.background.position.position.width;
        position.height           = paintableTextItem2.background.position.position.height;
        currentPaintableTextItem2.background.position.position = position;

        currentPaintableRectangleItem3 = paintableRectangleItem3;
        position                       = paintableRectangleItem3.position.position;
        position.x                     = paintableRectangleItem3.position.position.x + paintableList.position.x;
        position.y                     = (paintableRectangleItem3.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width                 = paintableRectangleItem3.position.position.width;
        position.height                = paintableRectangleItem3.position.position.height;
        currentPaintableRectangleItem3.position.position = position;

        currentPaintableTextItem3 = paintableTextItem3;
        position                  = paintableTextItem3.background.position.position;
        position.x                = paintableTextItem3.background.position.position.x + paintableList.position.x;
        position.y                = (paintableTextItem3.background.position.position.y + paintableList.position.y
                      + paintableList.position.height - childrenRectList.y);
        position.width            = paintableTextItem3.background.position.position.width;
        position.height           = paintableTextItem3.background.position.position.height;
        currentPaintableTextItem3.background.position.position = position;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int dataIndex = pageNumber * 9;
             dataIndex < (pageNumber + 1) * 9 && dataIndex < dataTablesData.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

            currentPaintableRectangleItem1.position.position.y =
              (currentPaintableRectangleItem1.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableRectangleItem1.position.position.width =
              (paintableRectangleItem1.position.position.width);
            currentPaintableRectangleItem1.position.position.height =
              (paintableRectangleItem1.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem1);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });

            currentPaintableTextItem1.text = paintableTextItem1.text;
            currentPaintableTextItem1.background.position.position.y =
              (currentPaintableTextItem1.background.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableTextItem1.background.position.position.width =
              (paintableTextItem1.background.position.position.width);
            currentPaintableTextItem1.background.position.position.height =
              (paintableTextItem1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

            currentPaintableTextItem1.text = dataTableData.value(0);

            expectedPage.paintableItemList.append(currentPaintableTextItem1);

            currentPaintableRectangleItem2.position.position.y =
              (currentPaintableRectangleItem2.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableRectangleItem2.position.position.width =
              (paintableRectangleItem2.position.position.width);
            currentPaintableRectangleItem2.position.position.height =
              (paintableRectangleItem2.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem2);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

            currentPaintableTextItem2.text = paintableTextItem2.text;
            currentPaintableTextItem2.background.position.position.y =
              (currentPaintableTextItem2.background.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableTextItem2.background.position.position.width =
              (paintableTextItem2.background.position.position.width);
            currentPaintableTextItem2.background.position.position.height =
              (paintableTextItem2.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 3, 0, dataIndex });

            currentPaintableTextItem2.text = dataTableData.value(1);

            expectedPage.paintableItemList.append(currentPaintableTextItem2);

            currentPaintableRectangleItem3.position.position.y =
              (currentPaintableRectangleItem3.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableRectangleItem3.position.position.width =
              (paintableRectangleItem3.position.position.width);
            currentPaintableRectangleItem3.position.position.height =
              (paintableRectangleItem3.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem3);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 4, 0, dataIndex });

            currentPaintableTextItem3.text = paintableTextItem3.text;
            currentPaintableTextItem3.background.position.position.y =
              (currentPaintableTextItem3.background.position.position.y - paintableList.position.height
               + childrenRectList.y);
            currentPaintableTextItem3.background.position.position.width =
              (paintableTextItem3.background.position.position.width);
            currentPaintableTextItem3.background.position.position.height =
              (paintableTextItem3.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 5, 0, dataIndex });

            currentPaintableTextItem3.text = dataTableData.value(2);

            expectedPage.paintableItemList.append(currentPaintableTextItem3);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_4pages_6items_rightRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableRectangleData& paintableRectangleItem2 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[2]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[3]);
    const PaintableRectangleData& paintableRectangleItem3 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[4]);
    const PaintableTextData& paintableTextItem3 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[5]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    for (int pageNumber = 0; pageNumber < 4; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableRectangleData currentPaintableRectangleItem2 = paintableRectangleItem2;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;
        PaintableRectangleData currentPaintableRectangleItem3 = paintableRectangleItem3;
        PaintableTextData currentPaintableTextItem3           = paintableTextItem3;

        currentPaintableRectangleItem1 = paintableRectangleItem1;
        PositionData position          = currentPaintableRectangleItem1.position.position;
        position.x      = (currentPaintableRectangleItem1.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem1.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem1.position.position.width;
        position.height = currentPaintableRectangleItem1.position.position.height;
        currentPaintableRectangleItem1.position.position = position;

        currentPaintableTextItem1 = paintableTextItem1;
        position                  = currentPaintableTextItem1.background.position.position;
        position.x      = (currentPaintableTextItem1.background.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableTextItem1.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem1.background.position.position.width;
        position.height = currentPaintableTextItem1.background.position.position.height;
        currentPaintableTextItem1.background.position.position = position;

        currentPaintableRectangleItem2 = paintableRectangleItem2;
        position                       = currentPaintableRectangleItem2.position.position;
        position.x      = (currentPaintableRectangleItem2.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem2.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem2.position.position.width;
        position.height = currentPaintableRectangleItem2.position.position.height;
        currentPaintableRectangleItem2.position.position = position;

        currentPaintableTextItem2 = paintableTextItem2;
        position                  = currentPaintableTextItem2.background.position.position;
        position.x      = (currentPaintableTextItem2.background.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableTextItem2.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem2.background.position.position.width;
        position.height = currentPaintableTextItem2.background.position.position.height;
        currentPaintableTextItem2.background.position.position = position;

        currentPaintableRectangleItem3 = paintableRectangleItem3;
        position                       = currentPaintableRectangleItem3.position.position;
        position.x      = (currentPaintableRectangleItem3.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem3.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem3.position.position.width;
        position.height = currentPaintableRectangleItem3.position.position.height;
        currentPaintableRectangleItem3.position.position = position;

        currentPaintableTextItem3 = paintableTextItem3;
        position                  = currentPaintableTextItem3.background.position.position;
        position.x      = (currentPaintableTextItem3.background.position.position.x + paintableList.position.x
                      - childrenRectList.width - childrenRectList.x);
        position.y      = currentPaintableTextItem3.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem3.background.position.position.width;
        position.height = currentPaintableTextItem3.background.position.position.height;
        currentPaintableTextItem3.background.position.position = position;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int dataIndex = pageNumber * 9;
             dataIndex < (pageNumber + 1) * 9 && dataIndex < dataTablesData.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

            currentPaintableRectangleItem1.position.position.x =
              (currentPaintableRectangleItem1.position.position.x + childrenRectList.width + childrenRectList.x);
            currentPaintableRectangleItem1.position.position.width =
              (paintableRectangleItem1.position.position.width);
            currentPaintableRectangleItem1.position.position.height =
              (paintableRectangleItem1.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem1);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });

            currentPaintableTextItem1.text = paintableTextItem1.text;
            currentPaintableTextItem1.background.position.position.x =
              (currentPaintableTextItem1.background.position.position.x + childrenRectList.width
               + childrenRectList.x);
            currentPaintableTextItem1.background.position.position.width =
              (paintableTextItem1.background.position.position.width);
            currentPaintableTextItem1.background.position.position.height =
              (paintableTextItem1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

            currentPaintableTextItem1.text = dataTableData.value(0);

            expectedPage.paintableItemList.append(currentPaintableTextItem1);

            currentPaintableRectangleItem2.position.position.x =
              (currentPaintableRectangleItem2.position.position.x + childrenRectList.width + childrenRectList.x);
            currentPaintableRectangleItem2.position.position.width =
              (paintableRectangleItem2.position.position.width);
            currentPaintableRectangleItem2.position.position.height =
              (paintableRectangleItem2.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem2);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

            currentPaintableTextItem2.text = paintableTextItem2.text;
            currentPaintableTextItem2.background.position.position.x =
              (currentPaintableTextItem2.background.position.position.x + childrenRectList.width
               + childrenRectList.x);
            currentPaintableTextItem2.background.position.position.width =
              (paintableTextItem2.background.position.position.width);
            currentPaintableTextItem2.background.position.position.height =
              (paintableTextItem2.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 3, 0, dataIndex });

            currentPaintableTextItem2.text = dataTableData.value(1);

            expectedPage.paintableItemList.append(currentPaintableTextItem2);

            currentPaintableRectangleItem3.position.position.x =
              (currentPaintableRectangleItem3.position.position.x + childrenRectList.width + childrenRectList.x);
            currentPaintableRectangleItem3.position.position.width =
              (paintableRectangleItem3.position.position.width);
            currentPaintableRectangleItem3.position.position.height =
              (paintableRectangleItem3.position.position.height);

            expectedPage.paintableItemList.append(currentPaintableRectangleItem3);
            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 4, 0, dataIndex });

            currentPaintableTextItem3.text = paintableTextItem3.text;
            currentPaintableTextItem3.background.position.position.x =
              (currentPaintableTextItem3.background.position.position.x + childrenRectList.width
               + childrenRectList.x);
            currentPaintableTextItem3.background.position.position.width =
              (paintableTextItem3.background.position.position.width);
            currentPaintableTextItem3.background.position.position.height =
              (paintableTextItem3.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 5, 0, dataIndex });

            currentPaintableTextItem3.text = dataTableData.value(2);

            expectedPage.paintableItemList.append(currentPaintableTextItem3);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_4pages_6items_leftRepetition(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableRectangleData& paintableRectangleItem2 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[2]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[3]);
    const PaintableRectangleData& paintableRectangleItem3 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[4]);
    const PaintableTextData& paintableTextItem3 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[5]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    for (int pageNumber = 0; pageNumber < 4; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableRectangleData currentPaintableRectangleItem2 = paintableRectangleItem2;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;
        PaintableRectangleData currentPaintableRectangleItem3 = paintableRectangleItem3;
        PaintableTextData currentPaintableTextItem3           = paintableTextItem3;

        currentPaintableRectangleItem1 = paintableRectangleItem1;
        PositionData position          = paintableRectangleItem1.position.position;
        position.x                     = (paintableRectangleItem1.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem1.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem1.position.position.width;
        position.height = currentPaintableRectangleItem1.position.position.height;
        currentPaintableRectangleItem1.position.position = position;

        currentPaintableTextItem1 = paintableTextItem1;
        position                  = currentPaintableTextItem1.background.position.position;
        position.x      = (currentPaintableTextItem1.background.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableTextItem1.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem1.background.position.position.width;
        position.height = currentPaintableTextItem1.background.position.position.height;
        currentPaintableTextItem1.background.position.position = position;

        currentPaintableRectangleItem2 = paintableRectangleItem2;
        position                       = currentPaintableRectangleItem2.position.position;
        position.x      = (currentPaintableRectangleItem2.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem2.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem2.position.position.width;
        position.height = currentPaintableRectangleItem2.position.position.height;
        currentPaintableRectangleItem2.position.position = position;

        currentPaintableTextItem2 = paintableTextItem2;
        position                  = currentPaintableTextItem2.background.position.position;
        position.x      = (currentPaintableTextItem2.background.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableTextItem2.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem2.background.position.position.width;
        position.height = currentPaintableTextItem2.background.position.position.height;
        currentPaintableTextItem2.background.position.position = position;

        currentPaintableRectangleItem3 = paintableRectangleItem3;
        position                       = currentPaintableRectangleItem3.position.position;
        position.x      = (currentPaintableRectangleItem3.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableRectangleItem3.position.position.y + paintableList.position.y;
        position.width  = currentPaintableRectangleItem3.position.position.width;
        position.height = currentPaintableRectangleItem3.position.position.height;
        currentPaintableRectangleItem3.position.position = position;

        currentPaintableTextItem3 = paintableTextItem3;
        position                  = currentPaintableTextItem3.background.position.position;
        position.x      = (currentPaintableTextItem3.background.position.position.x + paintableList.position.x
                      + paintableList.position.width - childrenRectList.x);
        position.y      = currentPaintableTextItem3.background.position.position.y + paintableList.position.y;
        position.width  = currentPaintableTextItem3.background.position.position.width;
        position.height = currentPaintableTextItem3.background.position.position.height;
        currentPaintableTextItem3.background.position.position = position;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int dataIndex = pageNumber * 9;
             dataIndex < (pageNumber + 1) * 9 && dataIndex < dataTablesData.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

            currentPaintableRectangleItem1.position.position.x =
              (currentPaintableRectangleItem1.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableRectangleItem1.position.position.width =
              (paintableRectangleItem1.position.position.width);
            currentPaintableRectangleItem1.position.position.height =
              (paintableRectangleItem1.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

            currentPaintableTextItem1.text = paintableTextItem1.text;
            currentPaintableTextItem1.background.position.position.x =
              (currentPaintableTextItem1.background.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableTextItem1.background.position.position.width =
              (paintableTextItem1.background.position.position.width);
            currentPaintableTextItem1.background.position.position.height =
              (paintableTextItem1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

            currentPaintableTextItem1.text = dataTableData.value(0);

            expectedPage.paintableItemList.append(currentPaintableTextItem1);

            currentPaintableRectangleItem2.position.position.x =
              (currentPaintableRectangleItem2.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableRectangleItem2.position.position.width =
              (paintableRectangleItem2.position.position.width);
            currentPaintableRectangleItem2.position.position.height =
              (paintableRectangleItem2.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem2);

            currentPaintableTextItem2.text = paintableTextItem2.text;
            currentPaintableTextItem2.background.position.position.x =
              (currentPaintableTextItem2.background.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableTextItem2.background.position.position.width =
              (paintableTextItem2.background.position.position.width);
            currentPaintableTextItem2.background.position.position.height =
              (paintableTextItem2.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 3, 0, dataIndex });

            currentPaintableTextItem2.text = dataTableData.value(1);

            expectedPage.paintableItemList.append(currentPaintableTextItem2);

            currentPaintableRectangleItem3.position.position.x =
              (currentPaintableRectangleItem3.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableRectangleItem3.position.position.width =
              (paintableRectangleItem3.position.position.width);
            currentPaintableRectangleItem3.position.position.height =
              (paintableRectangleItem3.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 4, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem3);

            currentPaintableTextItem3.text = paintableTextItem3.text;
            currentPaintableTextItem3.background.position.position.x =
              (currentPaintableTextItem3.background.position.position.x - paintableList.position.width
               + childrenRectList.x);
            currentPaintableTextItem3.background.position.position.width =
              (paintableTextItem3.background.position.position.width);
            currentPaintableTextItem3.background.position.position.height =
              (paintableTextItem3.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 5, 0, dataIndex });

            currentPaintableTextItem3.text = dataTableData.value(2);

            expectedPage.paintableItemList.append(currentPaintableTextItem3);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primarybottom_secondaryright(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnRight = 0; indexOnRight < 3; ++indexOnRight)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnBottom = 0; indexOnBottom < 5; ++indexOnBottom)
            {
                int dataIndex = 15 * pageNumber + 5 * indexOnRight + indexOnBottom;
                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

                currentPaintableTextItem1.text = dataTableData.value(1);

                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

                currentPaintableTextItem2.text = dataTableData.value(2);

                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primaryright_secondarybottom(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnBottom = 0; indexOnBottom < 5; ++indexOnBottom)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(

                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnRight = 0; indexOnRight < 3; ++indexOnRight)
            {
                int dataIndex = 15 * pageNumber + 3 * indexOnBottom + indexOnRight;

                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);
                currentPaintableTextItem1.text = paintableTextItem1.text;

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

                currentPaintableTextItem1.text = dataTableData.value(1);

                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);
                currentPaintableTextItem2.text = paintableTextItem2.text;

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

                currentPaintableTextItem2.text = dataTableData.value(2);

                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primarybottom_secondaryleft(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnLeft = 0; indexOnLeft < 3; ++indexOnLeft)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(

                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnBottom = 0; indexOnBottom < 5; ++indexOnBottom)
            {
                int dataIndex = 15 * pageNumber + 5 * indexOnLeft + indexOnBottom;
                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });

                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

                currentPaintableTextItem1.text = dataTableData.value(1);

                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::BottomRepetition,
                    indexOnBottom);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

                currentPaintableTextItem2.text = dataTableData.value(2);

                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primaryleft_secondarybottom(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnBottom = 0; indexOnBottom < 5; ++indexOnBottom)

        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(

                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::BottomRepetition,
                indexOnBottom,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnLeft = 0; indexOnLeft < 3; ++indexOnLeft)
            {
                int dataIndex = 15 * pageNumber + 3 * indexOnBottom + indexOnLeft;

                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

                currentPaintableTextItem1.text = dataTableData.value(1);

                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

                currentPaintableTextItem2.text = dataTableData.value(2);

                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primarytop_secondaryright(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    expectedGeneratedDocument.documentData.pageItemsList.clear();
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnRight = 0; indexOnRight < 3; ++indexOnRight)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::RightRepetition,
                indexOnRight,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnTop = 0; indexOnTop < 5; ++indexOnTop)
            {
                int dataIndex = 15 * pageNumber + 5 * indexOnRight + indexOnTop;
                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });

                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });

                currentPaintableTextItem1.text = dataTableData.value(1);

                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });

                currentPaintableTextItem2.text = dataTableData.value(2);

                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primaryright_secondarytop(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnTop = 0; indexOnTop < 5; ++indexOnTop)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnRight = 0; indexOnRight < 3; ++indexOnRight)
            {
                int dataIndex = 15 * pageNumber + 3 * indexOnTop + indexOnRight;

                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });
                currentPaintableTextItem1.text = dataTableData.value(1);
                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::RightRepetition,
                    indexOnRight);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });
                currentPaintableTextItem2.text = dataTableData.value(2);
                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primarytop_secondaryleft(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnLeft = 0; indexOnLeft < 3; ++indexOnLeft)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::LeftRepetition,
                indexOnLeft,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnTop = 0; indexOnTop < 5; ++indexOnTop)
            {
                int dataIndex = 15 * pageNumber + 5 * indexOnLeft + indexOnTop;
                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });
                currentPaintableTextItem1.text = dataTableData.value(1);
                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::TopRepetition,
                    indexOnTop);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });
                currentPaintableTextItem2.text = dataTableData.value(2);
                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_secondaryRepetition_primaryleft_secondarytop(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[2]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 3; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int indexOnTop = 0; indexOnTop < 5; ++indexOnTop)
        {
            PositionData paintableRectangleItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableRectangleItem1.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem1RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem1.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);
            PositionData paintableTextItem2RowPosition =
              document::service::documentgenerator::updateListItemPosition(
                paintableList.position,
                paintableTextItem2.background.position.position,
                childrenRectList,
                PaintableListData::RepetitionDirection::TopRepetition,
                indexOnTop,
                PaintableListData::RepetitionDirection::NoRepetition,
                0);

            for (int indexOnLeft = 0; indexOnLeft < 3; ++indexOnLeft)
            {
                int dataIndex = 15 * pageNumber + 3 * indexOnTop + indexOnLeft;
                if (dataIndex >= dataTablesData.dataTables.size()) { break; }

                const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

                currentPaintableRectangleItem1.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableRectangleItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
                expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

                currentPaintableTextItem1.text = paintableTextItem1.text;
                currentPaintableTextItem1.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem1RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });
                currentPaintableTextItem1.text = dataTableData.value(1);
                expectedPage.paintableItemList.append(currentPaintableTextItem1);

                currentPaintableTextItem2.text = paintableTextItem2.text;
                currentPaintableTextItem2.background.position.position =
                  document::service::documentgenerator::updateItemPosition(
                    paintableList.position,
                    paintableTextItem2RowPosition,
                    childrenRectList,
                    PaintableListData::RepetitionDirection::LeftRepetition,
                    indexOnLeft);

                expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });
                currentPaintableTextItem2.text = dataTableData.value(2);
                expectedPage.paintableItemList.append(currentPaintableTextItem2);
            }
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(6)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 6 });

        expectedGeneratedDocument.documentData.pageItemsList.push_back(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

GeneratedDocument Test_DocumentGenerator::_generateDocument_2lists(
  const DocumentTemplateData& documentTemplateData,
  const DataHandlerData& dataHandlerData) const
{
    GeneratedDocument expectedGeneratedDocument;
    expectedGeneratedDocument.documentData.pageSize = documentTemplateData.pageSizeData;

    const PaintableListData& paintableList =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[4]);

    const PaintableRectangleData& paintableRectangleItem1 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem1 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[1]);
    const PaintableRectangleData& paintableRectangleItem2 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[2]);
    const PaintableTextData& paintableTextItem2 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[3]);
    const PaintableRectangleData& paintableRectangleItem3 =
      std::get<PaintableRectangleData>(paintableList.paintableItemListModel.paintableItemList[4]);
    const PaintableTextData& paintableTextItem3 =
      std::get<PaintableTextData>(paintableList.paintableItemListModel.paintableItemList[5]);

    const PaintableListData& refOnPaintableList2 =
      std::get<PaintableListData>(documentTemplateData.rootPaintableItemListData.paintableList[6]);

    const PaintableRectangleData& paintableRectangleItem2_1 =
      std::get<PaintableRectangleData>(refOnPaintableList2.paintableItemListModel.paintableItemList[0]);
    const PaintableTextData& paintableTextItem2_1 =
      std::get<PaintableTextData>(refOnPaintableList2.paintableItemListModel.paintableItemList[1]);

    PositionData childrenRectList =
      document::service::documentgenerator::childrenRectList(paintableList.paintableItemListModel);
    PositionData childrenRectList2 =
      document::service::documentgenerator::childrenRectList(refOnPaintableList2.paintableItemListModel);

    for (int pageNumber = 0; pageNumber < 4; ++pageNumber)
    {
        PaintableItemListModelData expectedPage;
        PageItemsAssociationData expectedPageAssociation;

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(0)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(1)));
        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(2)));
        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(3)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 0 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 1 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 2 });
        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 3 });

        PaintableRectangleData currentPaintableRectangleItem1 = paintableRectangleItem1;
        PaintableTextData currentPaintableTextItem1           = paintableTextItem1;
        PaintableRectangleData currentPaintableRectangleItem2 = paintableRectangleItem2;
        PaintableTextData currentPaintableTextItem2           = paintableTextItem2;
        PaintableRectangleData currentPaintableRectangleItem3 = paintableRectangleItem3;
        PaintableTextData currentPaintableTextItem3           = paintableTextItem3;

        PositionData position = currentPaintableRectangleItem1.position.position;
        position.x            = currentPaintableRectangleItem1.position.position.x + paintableList.position.x;
        position.y            = (currentPaintableRectangleItem1.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width        = currentPaintableRectangleItem1.position.position.width;
        position.height       = currentPaintableRectangleItem1.position.position.height;
        currentPaintableRectangleItem1.position.position = position;

        position        = currentPaintableTextItem1.background.position.position;
        position.x      = currentPaintableTextItem1.background.position.position.x + paintableList.position.x;
        position.y      = (currentPaintableTextItem1.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = currentPaintableTextItem1.background.position.position.width;
        position.height = currentPaintableTextItem1.background.position.position.height;
        currentPaintableTextItem1.background.position.position = position;

        position        = currentPaintableRectangleItem2.position.position;
        position.x      = currentPaintableRectangleItem2.position.position.x + paintableList.position.x;
        position.y      = (currentPaintableRectangleItem2.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = currentPaintableRectangleItem2.position.position.width;
        position.height = currentPaintableRectangleItem2.position.position.height;
        currentPaintableRectangleItem2.position.position = position;

        position        = currentPaintableTextItem2.background.position.position;
        position.x      = currentPaintableTextItem2.background.position.position.x + paintableList.position.x;
        position.y      = (currentPaintableTextItem2.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = currentPaintableTextItem2.background.position.position.width;
        position.height = currentPaintableTextItem2.background.position.position.height;
        currentPaintableTextItem2.background.position.position = position;

        position        = currentPaintableRectangleItem3.position.position;
        position.x      = currentPaintableRectangleItem3.position.position.x + paintableList.position.x;
        position.y      = (currentPaintableRectangleItem3.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = currentPaintableRectangleItem3.position.position.width;
        position.height = currentPaintableRectangleItem3.position.position.height;
        currentPaintableRectangleItem3.position.position = position;

        position        = currentPaintableTextItem3.background.position.position;
        position.x      = currentPaintableTextItem3.background.position.position.x + paintableList.position.x;
        position.y      = (currentPaintableTextItem3.background.position.position.y + paintableList.position.y
                      - childrenRectList.height - childrenRectList.y);
        position.width  = currentPaintableTextItem3.background.position.position.width;
        position.height = currentPaintableTextItem3.background.position.position.height;
        currentPaintableTextItem3.background.position.position = position;

        const DataTablesData& dataTablesData =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(0);

        for (int dataIndex = pageNumber * 9;
             dataIndex < (pageNumber + 1) * 9 && dataIndex < dataTablesData.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData.dataTables.at(dataIndex);

            currentPaintableRectangleItem1.position.position.y =
              (currentPaintableRectangleItem1.position.position.y + childrenRectList.height + childrenRectList.y);
            currentPaintableRectangleItem1.position.position.width =
              (paintableRectangleItem1.position.position.width);
            currentPaintableRectangleItem1.position.position.height =
              (paintableRectangleItem1.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 0, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem1);

            currentPaintableTextItem1.text = paintableTextItem1.text;
            currentPaintableTextItem1.background.position.position.y =
              (currentPaintableTextItem1.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            currentPaintableTextItem1.background.position.position.width =
              (paintableTextItem1.background.position.position.width);
            currentPaintableTextItem1.background.position.position.height =
              (paintableTextItem1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 1, 0, dataIndex });
            currentPaintableTextItem1.text = dataTableData.value(0);
            expectedPage.paintableItemList.append(currentPaintableTextItem1);

            currentPaintableRectangleItem2.position.position.y =
              (currentPaintableRectangleItem2.position.position.y + childrenRectList.height + childrenRectList.y);
            currentPaintableRectangleItem2.position.position.width =
              (paintableRectangleItem2.position.position.width);
            currentPaintableRectangleItem2.position.position.height =
              (paintableRectangleItem2.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 2, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem2);

            currentPaintableTextItem2.text = paintableTextItem2.text;
            currentPaintableTextItem2.background.position.position.y =
              (currentPaintableTextItem2.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            currentPaintableTextItem2.background.position.position.width =
              (paintableTextItem2.background.position.position.width);
            currentPaintableTextItem2.background.position.position.height =
              (paintableTextItem2.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 3, 0, dataIndex });
            currentPaintableTextItem2.text = dataTableData.value(1);
            expectedPage.paintableItemList.append(currentPaintableTextItem2);

            currentPaintableRectangleItem3.position.position.y =
              (currentPaintableRectangleItem3.position.position.y + childrenRectList.height + childrenRectList.y);
            currentPaintableRectangleItem3.position.position.width =
              (paintableRectangleItem3.position.position.width);
            currentPaintableRectangleItem3.position.position.height =
              (paintableRectangleItem3.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 4, 0, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem3);

            currentPaintableTextItem3.text = paintableTextItem3.text;
            currentPaintableTextItem3.background.position.position.y =
              (currentPaintableTextItem3.background.position.position.y + childrenRectList.height
               + childrenRectList.y);
            currentPaintableTextItem3.background.position.position.width =
              (paintableTextItem3.background.position.position.width);
            currentPaintableTextItem3.background.position.position.height =
              (paintableTextItem3.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 4, 5, 0, dataIndex });
            currentPaintableTextItem3.text = dataTableData.value(2);
            expectedPage.paintableItemList.append(currentPaintableTextItem3);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableRectangleData>(documentTemplateData.rootPaintableItemListData.paintableList.at(5)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 5 });

        PaintableRectangleData currentPaintableRectangleItem2_1 = paintableRectangleItem2_1;
        PaintableTextData currentPaintableTextItem2_1           = paintableTextItem2_1;

        position        = currentPaintableRectangleItem2_1.position.position;
        position.x      = (currentPaintableRectangleItem2_1.position.position.x + refOnPaintableList2.position.x
                      - childrenRectList2.width - childrenRectList2.x);
        position.y      = currentPaintableRectangleItem2_1.position.position.y + refOnPaintableList2.position.y;
        position.width  = currentPaintableRectangleItem2_1.position.position.width;
        position.height = currentPaintableRectangleItem2_1.position.position.height;
        currentPaintableRectangleItem2_1.position.position = position;

        position   = currentPaintableTextItem2_1.background.position.position;
        position.x = (currentPaintableTextItem2_1.background.position.position.x + refOnPaintableList2.position.x
                      - childrenRectList2.width - childrenRectList2.x);
        position.y = currentPaintableTextItem2_1.background.position.position.y + refOnPaintableList2.position.y;
        position.width  = currentPaintableTextItem2_1.background.position.position.width;
        position.height = currentPaintableTextItem2_1.background.position.position.height;
        currentPaintableTextItem2_1.background.position.position = position;

        const DataTablesData& dataTablesData2 =
          dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(1);

        for (int dataIndex = pageNumber * 5;
             dataIndex < (pageNumber + 1) * 5 && dataIndex < dataTablesData2.dataTables.size();
             ++dataIndex)
        {
            const DataTableData& dataTableData = dataTablesData2.dataTables.at(dataIndex);

            currentPaintableRectangleItem2_1.position.position.x =
              (currentPaintableRectangleItem2_1.position.position.x + childrenRectList2.width
               + childrenRectList2.x);
            currentPaintableRectangleItem2_1.position.position.width =
              (paintableRectangleItem2_1.position.position.width);
            currentPaintableRectangleItem2_1.position.position.height =
              (paintableRectangleItem2_1.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 6, 0, 1, dataIndex });
            expectedPage.paintableItemList.append(currentPaintableRectangleItem2_1);

            currentPaintableTextItem2_1.text = paintableTextItem2_1.text;
            currentPaintableTextItem2_1.background.position.position.x =
              (currentPaintableTextItem2_1.background.position.position.x + childrenRectList2.width
               + childrenRectList2.x);
            currentPaintableTextItem2_1.background.position.position.width =
              (paintableTextItem2_1.background.position.position.width);
            currentPaintableTextItem2_1.background.position.position.height =
              (paintableTextItem2_1.background.position.position.height);

            expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { 6, 1, 1, dataIndex });
            currentPaintableTextItem2_1.text = dataTableData.value(0);
            expectedPage.paintableItemList.append(currentPaintableTextItem2_1);
        }

        expectedPage.paintableItemList.append(
          std::get<PaintableTextData>(documentTemplateData.rootPaintableItemListData.paintableList.at(7)));

        expectedPageAssociation.itemsAssociationData.push_back(ItemAssociationData { -1, 7 });

        expectedGeneratedDocument.documentData.pageItemsList.append(expectedPage);
        expectedGeneratedDocument.dataItemsAssociationData.pageItemsAssociationData.push_back(
          expectedPageAssociation);
    }

    return expectedGeneratedDocument;
}

QTEST_APPLESS_MAIN(Test_DocumentGenerator)

#include "Test_DocumentGenerator.moc"
