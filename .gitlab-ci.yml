stages:         
  - build
  - package
  - upload
  - release

# For qml tests
variables:
    QT_QPA_PLATFORM: "offscreen"
    PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/release${CI_COMMIT_TAG}/${CI_COMMIT_TAG}

build-linux-debug-sonar:
  stage: build
  variables:
    CXXFLAGS: "--coverage"
    CMAKE_EXPORT_COMPILE_COMMANDS: "ON"
  script:
    - conan install . -o build_testing=True --profile:host gcc10_Debug --profile:build gcc10_Debug
    - conan build .
    - conan install cmake/3.28.1@ --profile:host gcc10_Debug --profile:build gcc10_Debug --generator VirtualRunEnv -if conan_out
    - source conan_out/conanrun.sh
    - cd build/Debug
    - ctest --output-junit ../../test_output.xml
    - cd ../..
    - gcovr build/Debug --exclude build --cobertura cobertura.xml --txt --sonarqube sonarqubecoverage.xml
    - sonar-scanner -Dsonar.host.url="${SONAR_HOST_URL}" -Dsonar.token="${SONAR_TOKEN}" -Dsonar.projectVersion=$(conan inspect . --raw version) -Dsonar.cfamily.compile-commands=build/Debug/compile_commands.json -Dsonar.coverageReportPaths=sonarqubecoverage.xml
  only:
    - merge_requests
    - develop
  coverage: '/^TOTAL.*\s+(\d+\%)$/'
  artifacts:
    when: always
    paths:
      - test_output.xml
      - cobertura.xml
    reports:
      junit: test_output.xml
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml
  image: registry.gitlab.com/conanqt/conan-docker-images/gcc_10_qt_sonar:conan_1.62.0
  tags:
    - linux-docker

build-linux: 
  stage: build
  parallel:
    matrix:
        - BUILD_MODE: [Debug, Release]
  script:
    - conan install . -o build_testing=True --profile:host gcc10_${BUILD_MODE} --profile:build gcc10_${BUILD_MODE}
    - conan build .
    - conan install cmake/3.28.1@ --profile:host gcc10_${BUILD_MODE} --profile:build gcc10_${BUILD_MODE} --generator VirtualRunEnv -if conan_out
    - source conan_out/conanrun.sh
    - cd build/${BUILD_MODE}
    - ctest --output-junit ../../test_output.xml
  artifacts:
    when: always
    paths:
      - test_output.xml
    reports:
      junit: test_output.xml
  image: registry.gitlab.com/conanqt/conan-docker-images/gcc_10_qt:conan_1.62.0
  tags:
    - linux-docker


build-windows:       
  stage: build
  parallel:
    matrix:
        - BUILD_MODE: [Debug, Release]
  script:
    - conan install . -o build_testing=True --profile:host msvc2022_$BUILD_MODE --profile:build msvc2022_$BUILD_MODE
    - conan build .
    - conan install cmake/3.28.1@ --profile:host msvc2022_$BUILD_MODE --profile:build msvc2022_$BUILD_MODE --generator VirtualRunEnv -if conan_cmake_out
    - .\conan_cmake_out\conanrun.ps1
    - conan install . -o build_testing=True --profile:host msvc2022_$BUILD_MODE --profile:build msvc2022_$BUILD_MODE --generator VirtualRunEnv
    - cd build
    - .\generators\conanrun.ps1
    - ctest --output-junit ../test_output.xml
  artifacts:
    when: always
    paths:
      - test_output.xml
    reports:
      junit: test_output.xml
  image: registry.gitlab.com/conanqt/conan-docker-images/msvc_2022_qt:conan_1.62.0
  tags:
    - windows-docker

package:
  stage: package
  parallel:
    matrix:
        - PROFILE: [gcc10_Debug, gcc10_Release]
          CONTAINER_IMAGE: registry.gitlab.com/conanqt/conan-docker-images/gcc_10_qt:conan_1.62.0
          RUNNER_TAG: linux-docker
        - PROFILE: [msvc2022_Debug, msvc2022_Release]
          CONTAINER_IMAGE: registry.gitlab.com/conanqt/conan-docker-images/msvc_2022_qt:conan_1.62.0
          RUNNER_TAG: windows-docker
  script:
    - conan create . --profile:host $PROFILE --profile:build $PROFILE
  except:
    - tags
  image: $CONTAINER_IMAGE
  tags:
    - $RUNNER_TAG
    

package-linux-tag:
  stage: package
  parallel:
    matrix:
        - BUILD_MODE: [Debug, Release]
  script:
    - conan create . conanqt+sosforsmallbuissness/stable --profile:host gcc10_${BUILD_MODE} --profile:build gcc10_${BUILD_MODE}
    - conan user Vincent -r gitlab -p $CONAN_API_PASSWORD
    # Force to avoid error recipe is newer
    - conan upload *@conanqt+sosforsmallbuissness/stable -r gitlab --all --confirm --force
    - PACKAGE_REFERENCE=sosforsmallbuissness/$(conan inspect . --raw version)@conanqt+sosforsmallbuissness/stable
    - conan info $PACKAGE_REFERENCE --profile:host gcc10_${BUILD_MODE} --profile:build gcc10_${BUILD_MODE} --json conan_info.json --paths
    - cp $(python scripts/conan_find_package_folder.py conan_info.json sosforsmallbuissness)/SosForSmallBuissness*.run SosForSmallBuissness-${CI_COMMIT_TAG}_linux_${BUILD_MODE}.run
  artifacts:
    paths:
      - SosForSmallBuissness-${CI_COMMIT_TAG}_linux_${BUILD_MODE}.run
  only:
    - tags
  image: registry.gitlab.com/conanqt/conan-docker-images/gcc_10_qt:conan_1.62.0
  tags:
    - linux-docker

package-windows-tag:
  stage: package
  parallel:
    matrix:
        - BUILD_MODE: [Debug, Release]
  script:
    - conan create . conanqt+sosforsmallbuissness/stable --profile:host msvc2022_${BUILD_MODE} --profile:build msvc2022_${BUILD_MODE}
    - conan user Vincent -r gitlab -p $CONAN_API_PASSWORD
    # Force to avoid error recipe is newer
    - conan upload *@conanqt+sosforsmallbuissness/stable -r gitlab --all --confirm --force
    - $PACKAGE_REFERENCE="sosforsmallbuissness/$(conan inspect . --raw version)@conanqt+sosforsmallbuissness/stable"
    - conan info $PACKAGE_REFERENCE --profile:host msvc2022_${BUILD_MODE} --profile:build msvc2022_${BUILD_MODE} --json conan_info.json --paths
    - cp "$(python scripts/conan_find_package_folder.py conan_info.json sosforsmallbuissness)/SosForSmallBuissness*.exe" "SosForSmallBuissness-${CI_COMMIT_TAG}_windows_${BUILD_MODE}.exe"
  artifacts:
    paths:
      - SosForSmallBuissness-${CI_COMMIT_TAG}_windows_${BUILD_MODE}.exe
  only:
    - tags
  image: registry.gitlab.com/conanqt/conan-docker-images/msvc_2022_qt:conan_1.62.0
  tags:
    - windows-docker
    
upload:
  stage: upload
  parallel:
    matrix:
        - BUILD_MODE: [Debug, Release]
          OS_NAME: windows
          INSTALLER_EXTENSION: .exe
        - BUILD_MODE: [Debug, Release]
          OS_NAME: linux
          INSTALLER_EXTENSION: .run          
  script:
    - |
     curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file SosForSmallBuissness-${CI_COMMIT_TAG}_${OS_NAME}_${BUILD_MODE}${INSTALLER_EXTENSION} "${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_${OS_NAME}_${BUILD_MODE}${INSTALLER_EXTENSION}"
  only:
    - tags
  dependencies:
    - package-linux-tag
    - package-windows-tag
  image: curlimages/curl:latest
  tags:
    - linux-docker

    
release:
  stage: release
  script:
    - echo "${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_windows_Release.exe"
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"SosForSmallBuissness-${CI_COMMIT_TAG}_windows_Release.exe\",\"url\":\"${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_windows_Release.exe\",\"link_type\":\"package\"}" \
        --assets-link "{\"name\":\"SosForSmallBuissness-${CI_COMMIT_TAG}_windows_Debug.exe\",\"url\":\"${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_windows_Debug.exe\",\"link_type\":\"package\"}" \
        --assets-link "{\"name\":\"SosForSmallBuissness-${CI_COMMIT_TAG}_linux_Release.run\",\"url\":\"${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_linux_Release.run\",\"link_type\":\"package\"}" \
        --assets-link "{\"name\":\"SosForSmallBuissness-${CI_COMMIT_TAG}_linux_Debug.run\",\"url\":\"${PACKAGE_REGISTRY_URL}/SosForSmallBuissness-${CI_COMMIT_TAG}_linux_Debug.run\",\"link_type\":\"package\"}" \

  only:
    - tags
  dependencies:
    - package-linux-tag
    - package-windows-tag
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  tags:
    - linux-docker

                  