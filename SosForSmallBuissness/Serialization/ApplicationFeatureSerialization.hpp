#pragma once

#include <QString>

#include "Serialization_global.hpp"

namespace documenttemplate::data
{
struct TemplatesHandlerData;
}

namespace documentdatadefinitions::data
{
struct DataHandlerData;
}

namespace datatemplateassociation::data
{
struct DataTemplateAssociationMapData;
}

namespace document::data
{
struct DocumentHandlerData;
}

namespace serialization
{

class SERIALIZATION_EXPORT ApplicationFeatureSerialization
{
  public:
    ApplicationFeatureSerialization()  = default;
    ~ApplicationFeatureSerialization() = default;

    void saveTemplatesHandlerData(const documenttemplate::data::TemplatesHandlerData& templatesHandlerData);
    documenttemplate::data::TemplatesHandlerData loadTemplatesHandlerData();

    void saveDataHandlerData(const documentdatadefinitions::data::DataHandlerData& dataHandlerData);
    documentdatadefinitions::data::DataHandlerData loadDataHandlerData();

    void saveDataTemplateAssociationMapData(
      const datatemplateassociation::data::DataTemplateAssociationMapData& dataDocumentAssociationMapData);
    datatemplateassociation::data::DataTemplateAssociationMapData loadDataTemplateAssociationMapData();

    void saveDocumentHandlerData(const document::data::DocumentHandlerData& documentHandlerData);
    document::data::DocumentHandlerData loadDocumentHandlerData();

    QString dataFolder() const;
    void setDataFolder(const QString& newDataFolder);

  private:
    Q_DISABLE_COPY_MOVE(ApplicationFeatureSerialization)

    QString _dataFolder;
};
}    // namespace serialization
