#pragma once

#include <QStringView>

namespace serialization
{
constexpr const QStringView templateHandlerFileName            = u"Templates";
constexpr const QStringView dataHandlerFileName                = u"DataHandler";
constexpr const QStringView dataTemplateAssociationMapFileName = u"DataTemplateAssociationMap";
constexpr const QStringView documentHandlerFileName            = u"DocumentHandler";
}    // namespace serialization
