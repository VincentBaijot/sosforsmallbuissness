#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <optional>

#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DataTemplateAssociation/services/DataTemplateAssociationXmlSerialization.hpp"
#include "Document/data/DocumentHandlerData.hpp"
#include "Document/services/DocumentXmlSerialization.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentDataDefinitions/services/DocumentDataDefinitionsXmlSerialization.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "DocumentTemplate/services/DocumentTemplateXmlSerialization.hpp"

namespace serialization
{
template <class ClassToSerialize>
std::optional<ClassToSerialize> read(QXmlStreamReader& reader);

template <class ClassToSerialize>
void write(QXmlStreamWriter& writer, const ClassToSerialize& data);

template <>
std::optional<documenttemplate::data::TemplatesHandlerData> read<documenttemplate::data::TemplatesHandlerData>(
  QXmlStreamReader& reader)
{
    return documenttemplate::service::readTemplatesHandlerData(reader);
}

template <>
void write(QXmlStreamWriter& writer, const documenttemplate::data::TemplatesHandlerData& data)
{
    documenttemplate::service::writeTemplatesHandlerData(writer, data);
}

template <>
std::optional<documentdatadefinitions::data::DataHandlerData> read<documentdatadefinitions::data::DataHandlerData>(
  QXmlStreamReader& reader)
{
    return documentdatadefinitions::service::readDataHandlerData(reader);
}

template <>
void write(QXmlStreamWriter& writer, const documentdatadefinitions::data::DataHandlerData& data)
{
    documentdatadefinitions::service::writeDataHandlerData(writer, data);
}

template <>
std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
read<datatemplateassociation::data::DataTemplateAssociationMapData>(QXmlStreamReader& reader)
{
    return datatemplateassociation::service::readDataTemplateAssociationMapData(reader);
}

template <>
void write(QXmlStreamWriter& writer, const datatemplateassociation::data::DataTemplateAssociationMapData& data)
{
    datatemplateassociation::service::writeDataTemplateAssociationMapData(writer, data);
}

template <>
std::optional<document::data::DocumentHandlerData> read<document::data::DocumentHandlerData>(
  QXmlStreamReader& reader)
{
    return document::service::readDocumentHandlerData(reader);
}

template <>
void write(QXmlStreamWriter& writer, const document::data::DocumentHandlerData& data)
{
    document::service::writeDocumentHandlerData(writer, data);
}

}    // namespace serialization
