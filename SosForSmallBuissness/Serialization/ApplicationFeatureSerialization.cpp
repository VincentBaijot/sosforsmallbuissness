#include "ApplicationFeatureSerialization.hpp"

#include "ApplicationFeatureSerializationConstant.hpp"
#include "FeatureSerialization.hpp"

namespace serialization
{
void ApplicationFeatureSerialization::saveTemplatesHandlerData(
  const documenttemplate::data::TemplatesHandlerData& templatesHandlerData)
{
    saveData(templatesHandlerData, _dataFolder, templateHandlerFileName);
}

documenttemplate::data::TemplatesHandlerData ApplicationFeatureSerialization::loadTemplatesHandlerData()
{
    return loadData<documenttemplate::data::TemplatesHandlerData>(_dataFolder, templateHandlerFileName);
}

void ApplicationFeatureSerialization::saveDataHandlerData(
  const documentdatadefinitions::data::DataHandlerData& dataHandlerData)
{
    saveData(dataHandlerData, _dataFolder, dataHandlerFileName);
}

documentdatadefinitions::data::DataHandlerData ApplicationFeatureSerialization::loadDataHandlerData()
{
    return loadData<documentdatadefinitions::data::DataHandlerData>(_dataFolder, dataHandlerFileName);
}

void ApplicationFeatureSerialization::saveDataTemplateAssociationMapData(
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataDocumentAssociationMapData)
{
    saveData(dataDocumentAssociationMapData, _dataFolder, dataTemplateAssociationMapFileName);
}

datatemplateassociation::data::DataTemplateAssociationMapData
ApplicationFeatureSerialization::loadDataTemplateAssociationMapData()
{
    return loadData<datatemplateassociation::data::DataTemplateAssociationMapData>(
      _dataFolder, dataTemplateAssociationMapFileName);
}

void ApplicationFeatureSerialization::saveDocumentHandlerData(
  const document::data::DocumentHandlerData& documentHandlerData)
{
    saveData(documentHandlerData, _dataFolder, documentHandlerFileName);
}

document::data::DocumentHandlerData ApplicationFeatureSerialization::loadDocumentHandlerData()
{
    return loadData<document::data::DocumentHandlerData>(_dataFolder, documentHandlerFileName);
}

QString ApplicationFeatureSerialization::dataFolder() const
{
    return _dataFolder;
}

void ApplicationFeatureSerialization::setDataFolder(const QString& newDataFolder)
{
    _dataFolder = newDataFolder;
}
}    // namespace application
