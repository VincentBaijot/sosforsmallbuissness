#pragma once

#include <QFile>
#include <QString>
#include <QXmlStreamWriter>

#include "Constants.hpp"
#include "XmlSerialization.hpp"

namespace serialization
{
template <class ClassToSerialize>
void saveData(const ClassToSerialize& dataToSerialize, const QString& dataFolder, QStringView fileName)
{
    Q_ASSERT(!dataFolder.isEmpty());

    QFile xmlSerializationFile(dataFolder + fileName.toString() + ".xml");
    if (!xmlSerializationFile.open(QIODevice::WriteOnly))
    {
        qCWarning(serializationCategory) << "Cannot open file " << xmlSerializationFile.fileName() << " to save "
                                         << typeid(dataToSerialize).name();
        return;
    }

    qCInfo(serializationCategory) << "Save " << typeid(dataToSerialize).name() << " to file "
                                  << xmlSerializationFile.fileName();

    QXmlStreamWriter streamWriter(&xmlSerializationFile);
    streamWriter.setAutoFormatting(true);
    streamWriter.writeStartDocument();
    write(streamWriter, dataToSerialize);
    streamWriter.writeEndDocument();

    qCDebug(serializationCategory) << "Saved data : " << dataToSerialize;
}

template <class ClassToSerialize>
ClassToSerialize loadData(const QString& dataFolder, QStringView fileName)
{
    Q_ASSERT(!dataFolder.isEmpty());

    QFile xmlSerializationFile(dataFolder + fileName.toString() + ".xml");
    if (!xmlSerializationFile.open(QIODevice::ReadOnly))
    {
        qCWarning(serializationCategory) << "Cannot open file " << xmlSerializationFile.fileName() << " to load "
                                         << typeid(ClassToSerialize).name();
        return ClassToSerialize {};
    }

    qCInfo(serializationCategory) << "Load " << typeid(ClassToSerialize).name() << " from file "
                                  << xmlSerializationFile.fileName();

    QXmlStreamReader streamReader(&xmlSerializationFile);
    streamReader.readNextStartElement();
    std::optional<ClassToSerialize> optData = read<ClassToSerialize>(streamReader);
    if (!optData.has_value())
    {
        qCWarning(serializationCategory) << "Cannot load data from file " << xmlSerializationFile.fileName();
        return ClassToSerialize {};
    }
    else
    {
        qCDebug(serializationCategory) << "Loaded data " << optData.value();
        return optData.value();
    }
}
}    // namespace serialization
