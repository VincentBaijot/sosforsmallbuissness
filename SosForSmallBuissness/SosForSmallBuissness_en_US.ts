<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Application</name>
    <message>
        <location filename="Application/Application.qml" line="12"/>
        <source>Sos for small buissness</source>
        <translation>Sos for small buissness</translation>
    </message>
</context>
<context>
    <name>ApplicationBurgerMenu</name>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="19"/>
        <source>Create billing</source>
        <translation>Create document</translation>
    </message>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="37"/>
        <source>Edit templates</source>
        <translation>Edit templates</translation>
    </message>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="55"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationMenuBar</name>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="8"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="11"/>
        <source>&amp;New...</source>
        <translation>&amp;New...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="14"/>
        <source>&amp;Open...</source>
        <translation>&amp;Open...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="17"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="20"/>
        <source>Save &amp;As...</source>
        <translation>Save &amp;As...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="24"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="31"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="33"/>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;t</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="36"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copy</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="39"/>
        <source>&amp;Paste</source>
        <translation>&amp;Paste</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="43"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="45"/>
        <source>&amp;About</source>
        <translation>&amp;About</translation>
    </message>
</context>
<context>
    <name>BorderController</name>
    <message>
        <location filename="SosQml/ItemsControls/BorderController.qml" line="22"/>
        <source>Border color : </source>
        <translation>Border color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/BorderController.qml" line="33"/>
        <source>Border width : </source>
        <translation>Border width : </translation>
    </message>
</context>
<context>
    <name>ColorController</name>
    <message>
        <location filename="SosQml/ItemsControls/ColorController.qml" line="47"/>
        <source>Remove the color</source>
        <translation>Remove the color</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="SosQml/Controls/ColorPicker.qml" line="39"/>
        <source>Please choose a color</source>
        <translation>Please choose a color</translation>
    </message>
</context>
<context>
    <name>CreateBilling</name>
    <message>
        <location filename="SosQml/CreateBilling.qml" line="25"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="SosQml/CreateBilling.qml" line="34"/>
        <source>Document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataDefinitionEditionTable</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionEditionTable.qml" line="166"/>
        <source>Key</source>
        <translation>Key</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionEditionTable.qml" line="199"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
</context>
<context>
    <name>DataDefinitionListEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="39"/>
        <source>Data lists</source>
        <translation>Data lists</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="82"/>
        <source>List name</source>
        <translation>List name</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="111"/>
        <source>Demonstration data for %1</source>
        <translation>Demonstration data for %1</translation>
    </message>
</context>
<context>
    <name>DataEditionTable</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataEditionTable.qml" line="161"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
</context>
<context>
    <name>DataListAssociationController</name>
    <message>
        <location filename="SosQml/ItemsControls/DataListAssociationController.qml" line="26"/>
        <source>Data association : </source>
        <translation>Data association : </translation>
    </message>
</context>
<context>
    <name>DataListEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataListEditionView.qml" line="39"/>
        <source>Data lists</source>
        <translation>Data lists</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataListEditionView.qml" line="87"/>
        <source>Data for %1</source>
        <translation>Data for %1</translation>
    </message>
</context>
<context>
    <name>EditTemplate</name>
    <message>
        <location filename="SosQml/EditTemplate.qml" line="27"/>
        <source>Templates data</source>
        <translation>Templates data</translation>
    </message>
    <message>
        <location filename="SosQml/EditTemplate.qml" line="36"/>
        <source>Template</source>
        <translation>Document template</translation>
    </message>
</context>
<context>
    <name>FontController</name>
    <message>
        <location filename="SosQml/ItemsControls/FontController.qml" line="14"/>
        <source>Text : </source>
        <translation>Text : </translation>
    </message>
</context>
<context>
    <name>FontSizeController</name>
    <message>
        <location filename="SosQml/ItemsControls/FontSizeController.qml" line="13"/>
        <source>Text Size : </source>
        <translation>Text size : </translation>
    </message>
</context>
<context>
    <name>GeneralDataDefinitionsEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/GeneralDataDefinitionsEditionView.qml" line="39"/>
        <source>General variables</source>
        <translation>General variables</translation>
    </message>
</context>
<context>
    <name>GeneralDataEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/GeneralDataEditionView.qml" line="39"/>
        <source>General variables</source>
        <translation>General variables</translation>
    </message>
</context>
<context>
    <name>ItemNameController</name>
    <message>
        <location filename="SosQml/ItemsControls/ItemNameController.qml" line="10"/>
        <source>Item name : </source>
        <translation>Item name : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemNameController.qml" line="11"/>
        <source>Item name</source>
        <translation>Item name</translation>
    </message>
</context>
<context>
    <name>ItemPositionController</name>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="36"/>
        <source>Unit : </source>
        <translation>Unit : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="54"/>
        <source>Position : </source>
        <translation>Position : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="69"/>
        <source>x : </source>
        <translation>x : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="95"/>
        <source>y : </source>
        <translation>y : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="127"/>
        <source>width : </source>
        <translation>width : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="153"/>
        <source>height : </source>
        <translation>height : </translation>
    </message>
</context>
<context>
    <name>ListAssociationController</name>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationController.qml" line="27"/>
        <source>List association : </source>
        <translation>List association : </translation>
    </message>
</context>
<context>
    <name>ListController</name>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="25"/>
        <source>List name : </source>
        <translation>List Name : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="26"/>
        <source>List name</source>
        <translation>List name</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="97"/>
        <source>Primary repetition direction</source>
        <translation>Primary repetition direction</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="109"/>
        <source>Secondary repetition direction</source>
        <translation>Secondary repetition direction</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="139"/>
        <source>Remove the item</source>
        <translation>Remove the list</translation>
    </message>
</context>
<context>
    <name>OpacityController</name>
    <message>
        <location filename="SosQml/ItemsControls/OpacityController.qml" line="13"/>
        <source>Opacity : </source>
        <translation>Opacity : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/OpacityController.qml" line="51"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>PageSizeController</name>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="22"/>
        <source>Page format : </source>
        <translation>Page format : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="70"/>
        <source>Width : </source>
        <translation>Width : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="96"/>
        <source>Height : </source>
        <translation>Height : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="128"/>
        <source>Unit : </source>
        <translation>Unit : </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="104"/>
        <source>Millimeter</source>
        <translation>Millimeter</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="107"/>
        <source>Point</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="110"/>
        <source>Inch</source>
        <translation>Inch</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="113"/>
        <source>Pica</source>
        <translation>Pica</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="116"/>
        <source>Didot</source>
        <translation>Didot</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="120"/>
        <source>Cicero</source>
        <translation>Cicero</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinition.cpp" line="90"/>
        <source>String</source>
        <translation>String</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinition.cpp" line="93"/>
        <source>Number</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="126"/>
        <source>Key</source>
        <translation>Key</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="128"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="130"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="132"/>
        <source>Data type</source>
        <translation>Data type</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/ListDataDefinitionListModel.cpp" line="118"/>
        <source>ListDataDefinition</source>
        <translation>List data definition</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="28"/>
        <source>No List</source>
        <translation>No list</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="51"/>
        <source>List item</source>
        <translation>List item</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/ListDataDefinitionListModel.cpp" line="122"/>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="53"/>
        <source>List name</source>
        <translation>List name</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="149"/>
        <source>No repetition</source>
        <translation>No repetition</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="150"/>
        <source>Bottom</source>
        <translation>Bottom</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="151"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="152"/>
        <source>Top</source>
        <translation>Top</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="153"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
</context>
<context>
    <name>RadiusController</name>
    <message>
        <location filename="SosQml/ItemsControls/RadiusController.qml" line="14"/>
        <source>Radius : </source>
        <translation>Radius : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RadiusController.qml" line="52"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>RectangleController</name>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="87"/>
        <source>Color : </source>
        <translation>Color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="121"/>
        <source>Border color : </source>
        <translation>Border color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="168"/>
        <source>Remove the item</source>
        <translation>Remove the rectangle</translation>
    </message>
</context>
<context>
    <name>TextContentController</name>
    <message>
        <location filename="SosQml/ItemsControls/TextContentController.qml" line="15"/>
        <source>Text : </source>
        <translation>Text : </translation>
    </message>
</context>
<context>
    <name>TextController</name>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="84"/>
        <source>Text color : </source>
        <translation>Text color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="120"/>
        <source>Background color : </source>
        <translation>Background color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="137"/>
        <source>Border color : </source>
        <translation>Border color : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="235"/>
        <source>Remove the item</source>
        <translation>Remove the text</translation>
    </message>
</context>
</TS>
