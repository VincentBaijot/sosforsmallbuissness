#pragma once

#include <QtCore/qglobal.h>

#if defined(DOCUMENT_LIBRARY)
#define DOCUMENT_EXPORT Q_DECL_EXPORT
#else
#define DOCUMENT_EXPORT Q_DECL_IMPORT
#endif

#if defined(ENABLE_TESTING)
#define DOCUMENT_TESTING_EXPORT DOCUMENT_EXPORT
#else
#define DOCUMENT_TESTING_EXPORT
#endif
