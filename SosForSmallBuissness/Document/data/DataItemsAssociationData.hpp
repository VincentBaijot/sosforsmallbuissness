#pragma once

#include "Utils/DataStructureDebugSupport.hpp"

namespace document::data
{
struct ItemAssociationData
{
    int templateListIndex { -1 };
    int templateItemIndex { -1 };
    int modelIndex { -1 };
    int rowIndex { 0 };
};

struct PageItemsAssociationData
{
    QList<ItemAssociationData> itemsAssociationData;
};

struct DataItemsAssociationData
{
    QList<PageItemsAssociationData> pageItemsAssociationData;
};

inline QDebug operator<<(QDebug debugOutput, const ItemAssociationData& itemAssociationData)
{
    const QDebugStateSaver save(debugOutput);

    return debugOutput.nospace() << "ItemAssociationData{" << itemAssociationData.templateListIndex << ','
                                 << itemAssociationData.templateItemIndex << ',' << itemAssociationData.modelIndex
                                 << ',' << itemAssociationData.rowIndex << '}';
}

inline QDebug operator<<(QDebug debugOutput, const PageItemsAssociationData& pageItemsAssociationData)
{
    const QDebugStateSaver save(debugOutput);

    debugOutput.nospace() << "PageItemsAssociationData{QList<ItemAssociationData>";
    utils::qDebugSequentialContainer(debugOutput, pageItemsAssociationData.itemsAssociationData);
    debugOutput << '}';
    return debugOutput;
}

inline QDebug operator<<(QDebug debugOutput, const DataItemsAssociationData& dataItemsAssociationData)
{
    const QDebugStateSaver save(debugOutput);

    debugOutput.nospace() << "DataItemsAssociationData{QList<PageItemsAssociationData>";
    utils::qDebugSequentialContainer(debugOutput, dataItemsAssociationData.pageItemsAssociationData);
    debugOutput << '}';
    return debugOutput;
}

}    // namespace document::data
