#pragma once

#include "DocumentData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace document::data
{
struct DocumentHandlerData
{
    QList<DocumentData> documents;
    bool operator==(const DocumentHandlerData& documentHandlerData) const = default;
};
}    // namespace document::data

inline QDebug operator<<(QDebug debugOutput, const document::data::DocumentHandlerData& documentHandlerData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DocumentHandlerData{QList<DocumentData>";
    utils::qDebugSequentialContainer(debugOutput, documentHandlerData.documents);
    debugOutput << '}';
    return debugOutput;
}
