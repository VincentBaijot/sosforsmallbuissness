#pragma once

#include "DocumentTemplate/data/PageSizeData.hpp"
#include "Painter/data/PaintableItemListModelData.hpp"

namespace document::data
{
struct DocumentData
{
    QList<painter::data::PaintableItemListModelData> pageItemsList;
    documenttemplate::data::PageSizeData pageSize;
    bool operator==(const DocumentData& documentData) const = default;
};
}    // namespace document::data

inline QDebug operator<<(QDebug debugOutput, const document::data::DocumentData& documentData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DocumentData{QList<painter::data::PaintableItemListModelData>";
    utils::qDebugSequentialContainer(debugOutput, documentData.pageItemsList);
    debugOutput << ',' << documentData.pageSize << '}';

    return debugOutput;
}
