#pragma once

#include "Document/data/DataItemsAssociationData.hpp"
#include "Document/data/DocumentData.hpp"

namespace document::service::documentgenerator
{

struct GeneratedDocument
{
    document::data::DocumentData documentData;
    document::data::DataItemsAssociationData dataItemsAssociationData;
};

}    // namespace document::service::documentgenerator
