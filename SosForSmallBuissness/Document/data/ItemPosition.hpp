#pragma once

namespace document::service::documentgenerator
{
struct ItemPosition
{
    int page { -1 };
    int primaryRepetitionIndex { -1 };
    int secondaryRepetitionIndex { -1 };
};

}    // namespace document::service::documentgenerator
