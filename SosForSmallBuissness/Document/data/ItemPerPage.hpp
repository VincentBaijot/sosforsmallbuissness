#pragma once

namespace document::service::documentgenerator
{
struct ItemsPerPage
{
    int primaryRepetition { 0 };
    int secondaryRepetition { 0 };
};

}    // namespace document::service::documentgenerator
