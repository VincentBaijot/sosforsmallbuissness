#include "DocumentXmlSerializationDetails.hpp"

#include "DocumentTemplate/services/DocumentTemplateXmlConstants.hpp"
#include "DocumentTemplate/services/DocumentTemplateXmlSerializationDetails.hpp"
#include "DocumentXmlConstants.hpp"
#include "Painter/services/PainterXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"
#include "data/DocumentData.hpp"
#include "data/DocumentHandlerData.hpp"

namespace document::service::details
{
inline constexpr const QStringView pagesItemsListElement = u"PageItems";

QList<painter::data::PaintableItemListModelData> readPagesItemListElement(QXmlStreamReader& reader)
{
    QList<painter::data::PaintableItemListModelData> pagesItemList;

    while (!utils::xml::checkEndElement(reader, pagesItemsListElement))
    {
        if (reader.isStartElement() && reader.name() == painter::service::details::paintableItemListElement)
        {
            pagesItemList.append(painter::service::details::readPaintableItemListModelData(reader));
        }
        else { reader.readNext(); }
    }

    return pagesItemList;
}

document::data::DocumentData readDocumentData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == documentElement);

    reader.readNextStartElement();

    document::data::DocumentData documentData;

    while (!utils::xml::checkEndElement(reader, documentElement))
    {
        if (reader.isStartElement() && reader.name() == pagesItemsListElement)
        {
            documentData.pageItemsList.append(readPagesItemListElement(reader));
        }
        else if (reader.isStartElement() && reader.name() == documenttemplate::service::pageSizeElement)
        {
            documentData.pageSize = documenttemplate::service::details::readPageSizeData(reader);
        }
        else { reader.readNext(); }
    }

    return documentData;
}

void writeDocumentData(QXmlStreamWriter& writer, const document::data::DocumentData& documentData)
{
    writer.writeStartElement(documentElement.toString());

    writer.writeStartElement(pagesItemsListElement.toString());

    for (const painter::data::PaintableItemListModelData& paintableItemList : documentData.pageItemsList)
    {
        painter::service::details::writePaintableItemListModelData(writer, paintableItemList);
    }

    writer.writeEndElement();

    documenttemplate::service::details::writePageSizeData(writer, documentData.pageSize);

    writer.writeEndElement();
}

document::data::DocumentHandlerData readDocumentHandlerData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == documentHandlerElement);

    reader.readNextStartElement();

    document::data::DocumentHandlerData documentHandlerData;

    while (!utils::xml::checkEndElement(reader, documentHandlerElement))
    {
        if (reader.isStartElement() && reader.name() == documentElement)
        {
            documentHandlerData.documents.append(readDocumentData(reader));
        }
        else { reader.readNext(); }
    }

    return documentHandlerData;
}

void writeDocumentHandlerData(QXmlStreamWriter& writer,
                              const document::data::DocumentHandlerData& documentHandlerData)
{
    writer.writeStartElement(documentHandlerElement.toString());

    for (const document::data::DocumentData& documentData : documentHandlerData.documents)
    {
        writeDocumentData(writer, documentData);
    }

    writer.writeEndElement();
}

}    // namespace document::service::details
