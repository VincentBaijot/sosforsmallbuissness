#include "DocumentGeneratorDetails.hpp"

#include "DocumentGeneratorPosition.hpp"

namespace document::service::documentgenerator::details
{
void updateTextItem(painter::data::PaintableItemListModelData& paintableItemRow,
                    const QList<documentdatadefinitions::data::DataDefinitionData>& dataDefinitions,
                    const documentdatadefinitions::data::DataTableData& dataTableData)
{
    for (painter::data::VariantListableItemData& item : paintableItemRow.paintableItemList)
    {
        if (painter::data::PaintableTextData* textData =
              std::get_if<painter::data::PaintableTextData>(&item))
        {
            painter::data::PaintableTextData sourceTextData = *textData;

            for (int dataDefinitionIndex = 0; dataDefinitionIndex < dataDefinitions.size(); ++dataDefinitionIndex)
            {
                const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData =
                  dataDefinitions.at(dataDefinitionIndex);

                textData->text = generateTemplatedText(
                  textData->text, dataDefinitionData.key, dataTableData.value(dataDefinitionIndex));
            }
        }
    }
}

GeneratedItems generateListItems(const painter::data::PaintableListData& listData,
                                 const documentdatadefinitions::data::DataTablesData& associatedData,
                                 const QList<documentdatadefinitions::data::DataDefinitionData>& dataDefinitions)
{
    if (associatedData.dataTables.empty() || listData.paintableItemListModel.paintableItemList.empty()
        || listData.primaryRepetitionDirection
             == painter::data::PaintableListData::RepetitionDirection::NoRepetition)
    {
        return GeneratedItems {};
    }

    ItemsPerPage itemsPerPage                       = calculateItemsPerPage(listData);
    painter::data::PositionData childrenRect = childrenRectList(listData.paintableItemListModel);

    int rowIndex = 0;
    GeneratedItems generatedItems;
    painter::data::PaintableItemListModelData paintableItemList;
    document::data::PageItemsAssociationData pageItemsAssociation;

    for (const documentdatadefinitions::data::DataTableData& dataTableData : associatedData.dataTables)
    {
        ItemPosition itemPosition = calculateItemPositionFromIndex(itemsPerPage, rowIndex);

        // Use the paintable list to generate the "row"
        painter::data::PaintableItemListModelData paintableItemRow = listData.paintableItemListModel;

        for (painter::data::VariantListableItemData& variantItem : paintableItemRow.paintableItemList)
        {
            std::visit(
              [listData, childrenRect, itemPosition](auto& item)
              {
                  painter::data::PositionData sourcePosition = position(item);

                  painter::data::PositionData targetPosition =
                    updateListItemPosition(listData.position,
                                           sourcePosition,
                                           childrenRect,
                                           listData.primaryRepetitionDirection,
                                           itemPosition.primaryRepetitionIndex,
                                           listData.secondaryRepetitionDirection,
                                           itemPosition.secondaryRepetitionIndex);

                  setPosition(item, targetPosition);
              },
              variantItem);

            if (generatedItems.pageItemsList.size() < itemPosition.page)
            {
                generatedItems.pageItemsList.append(paintableItemList);
                paintableItemList.paintableItemList.clear();

                generatedItems.dataItemsAssociationData.pageItemsAssociationData.push_back(pageItemsAssociation);
                pageItemsAssociation.itemsAssociationData.clear();
            }
        }

        for (int itemTemplateIndex = 0; itemTemplateIndex < paintableItemRow.paintableItemList.size();
             ++itemTemplateIndex)
        {
            pageItemsAssociation.itemsAssociationData.push_back(
              document::data::ItemAssociationData { -1, itemTemplateIndex, -1, rowIndex });
        }

        updateTextItem(paintableItemRow, dataDefinitions, dataTableData);

        paintableItemList.paintableItemList.append(paintableItemRow.paintableItemList);

        rowIndex++;
    }

    generatedItems.pageItemsList.append(paintableItemList);
    generatedItems.dataItemsAssociationData.pageItemsAssociationData.push_back(pageItemsAssociation);
    return generatedItems;
}

QString generateTemplatedText(const QString& sourceTemplateText, const QString& dataKey, const QString& dataText)
{
    QString target = sourceTemplateText;
    return target.replace("${" + dataKey + "}", dataText);
}

}    // namespace document::service::documentgenerator::details
