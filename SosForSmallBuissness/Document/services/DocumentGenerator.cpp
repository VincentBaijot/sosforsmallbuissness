#include "DocumentGenerator.hpp"

#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DocumentGeneratorDetails.hpp"
#include "DocumentGeneratorHelper.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

namespace document::service::documentgenerator
{

GeneratedDocument generateDocument(
  const documenttemplate::data::DocumentTemplateData& documentTemplateData,
  const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData)
{
    DocumentGeneratorHelper documentGeneratorHelper;
    documentGeneratorHelper.initialize(documentTemplateData);

    for (int index = 0; index < documentTemplateData.rootPaintableItemListData.paintableList.size(); ++index)
    {
        const documenttemplate::data::VariantInterfaceQmlPaintableItem& item =
          documentTemplateData.rootPaintableItemListData.paintableList.at(index);

        std::visit(overloaded { [&documentGeneratorHelper,
                                 index](const painter::data::PaintableRectangleData& rectangleData)
                                { documentGeneratorHelper.addPaintableRectangleData(rectangleData, index); },
                                [&documentGeneratorHelper, &dataHandlerData, index](
                                  const painter::data::PaintableTextData& textData) {
                                    documentGeneratorHelper.addPaintableTextData(textData, dataHandlerData, index);
                                },
                                [&documentGeneratorHelper, index, &dataTemplateAssociationData, &dataHandlerData](
                                  const painter::data::PaintableListData& listData) {
                                    documentGeneratorHelper.addPaintableListData(
                                      listData, dataTemplateAssociationData, dataHandlerData, index);
                                } },
                   item);
    }

    return documentGeneratorHelper.generatedDocument();
}

QString generateTemplatedText(const QString& sourceTemplateText, const QString& dataKey, const QString& dataText)
{
    return details::generateTemplatedText(sourceTemplateText, dataKey, dataText);
}

}    // namespace document::service::documentgenerator
