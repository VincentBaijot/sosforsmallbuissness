#include "DocumentGeneratorPosition.hpp"

namespace document::service::documentgenerator
{

painter::data::PositionData childrenRectList(
  const painter::data::PaintableItemListModelData& listData)
{
    if (listData.paintableItemList.empty()) { return {}; }

    painter::data::PositionData childrenRect = position(listData.paintableItemList.at(0));

    for (const painter::data::VariantListableItemData& itemData : listData.paintableItemList)
    {
        painter::data::PositionData itemPosition = position(itemData);

        if (childrenRect.x > itemPosition.x)
        {
            childrenRect.width = childrenRect.width + childrenRect.x - itemPosition.x;
            childrenRect.x     = itemPosition.x;
        }

        if (childrenRect.y > itemPosition.y)
        {
            childrenRect.height = childrenRect.height + childrenRect.y - itemPosition.y;
            childrenRect.y      = itemPosition.y;
        }

        if (childrenRect.x + childrenRect.width < itemPosition.x + itemPosition.width)
        {
            childrenRect.width = itemPosition.x + itemPosition.width - childrenRect.x;
        }

        if (childrenRect.y + childrenRect.height < itemPosition.y + itemPosition.height)
        {
            childrenRect.height = itemPosition.y + itemPosition.height - childrenRect.y;
        }
    }

    return childrenRect;
}

int repetitionNumber(const painter::data::PaintableListData& paintableListData,
                     const painter::data::PositionData& childrenRect,
                     painter::data::PaintableListData::RepetitionDirection repetitionDirection)
{
    switch (repetitionDirection)
    {
        using RepetitionDirection = painter::data::PaintableListData::RepetitionDirection;
        case RepetitionDirection::BottomRepetition:
            return qFloor(paintableListData.position.height / (childrenRect.y + childrenRect.height));
        case RepetitionDirection::TopRepetition:
            return qFloor(paintableListData.position.height
                          / ((paintableListData.position.height - childrenRect.y - childrenRect.height)
                             + childrenRect.height));
        case RepetitionDirection::RightRepetition:
            return qFloor(paintableListData.position.width / (childrenRect.x + childrenRect.width));
        case RepetitionDirection::LeftRepetition:
            return qFloor(
              paintableListData.position.width
              / ((paintableListData.position.width - childrenRect.x - childrenRect.width) + childrenRect.width));
        case RepetitionDirection::NoRepetition:
            return 0;
    }

    return 0;
}

painter::data::PositionData updateListItemPosition(
  const painter::data::PositionData& listPosition,
  const painter::data::PositionData& itemSourcePosition,
  const painter::data::PositionData& listRowRect,
  painter::data::PaintableListData::RepetitionDirection primaryRepetitionDirection,
  int primaryRepetitionListIndex,
  painter::data::PaintableListData::RepetitionDirection secondaryRepetitionDirection,
  int secondaryRepetitionListIndex)
{
    painter::data::PositionData finalPosition { listPosition.x + itemSourcePosition.x,
                                                       listPosition.y + itemSourcePosition.y,
                                                       itemSourcePosition.width,
                                                       itemSourcePosition.height };

    finalPosition = updateItemPosition(
      listPosition, finalPosition, listRowRect, secondaryRepetitionDirection, secondaryRepetitionListIndex);

    return updateItemPosition(
      listPosition, finalPosition, listRowRect, primaryRepetitionDirection, primaryRepetitionListIndex);
}

painter::data::PositionData updateItemPosition(
  const painter::data::PositionData& listPosition,
  const painter::data::PositionData& itemSourcePosition,
  const painter::data::PositionData& listRowRect,
  painter::data::PaintableListData::RepetitionDirection repetitionDirection,
  int listIndex)
{
    qreal finalX      = itemSourcePosition.x;
    qreal finalY      = itemSourcePosition.y;
    qreal finalWidth  = itemSourcePosition.width;
    qreal finalHeight = itemSourcePosition.height;

    switch (repetitionDirection)
    {
        case painter::data::PaintableListData::RepetitionDirection::BottomRepetition:
        {
            finalY += listIndex * (listRowRect.height + listRowRect.y);
            return painter::data::PositionData { finalX, finalY, finalWidth, finalHeight };
        }
        case painter::data::PaintableListData::RepetitionDirection::TopRepetition:
        {
            finalY -= listIndex * (listPosition.height - listRowRect.y);
            return painter::data::PositionData { finalX, finalY, finalWidth, finalHeight };
        }
        case painter::data::PaintableListData::RepetitionDirection::LeftRepetition:
        {
            finalX -= listIndex * (listPosition.width - listRowRect.x);
            return painter::data::PositionData { finalX, finalY, finalWidth, finalHeight };
        }
        case painter::data::PaintableListData::RepetitionDirection::RightRepetition:
        {
            finalX += listIndex * (listRowRect.width + listRowRect.x);
            return painter::data::PositionData { finalX, finalY, finalWidth, finalHeight };
        }
        case painter::data::PaintableListData::RepetitionDirection::NoRepetition:
        {
            break;
        }
    }

    return itemSourcePosition;
}

ItemsPerPage calculateItemsPerPage(const painter::data::PaintableListData& paintableListData)
{
    ItemsPerPage itemsPerPage;

    painter::data::PositionData childrenRect = childrenRectList(paintableListData.paintableItemListModel);

    itemsPerPage.primaryRepetition =
      repetitionNumber(paintableListData, childrenRect, paintableListData.primaryRepetitionDirection);
    itemsPerPage.secondaryRepetition =
      repetitionNumber(paintableListData, childrenRect, paintableListData.secondaryRepetitionDirection);

    return itemsPerPage;
}

ItemPosition calculateItemPositionFromIndex(ItemsPerPage itemsPerPage, int rowIndex)
{
    if (itemsPerPage.primaryRepetition == 0) { return ItemPosition {}; }

    if (itemsPerPage.secondaryRepetition == 0)
    {
        return ItemPosition { rowIndex / itemsPerPage.primaryRepetition,
                              rowIndex % itemsPerPage.primaryRepetition,
                              0 };
    }

    return ItemPosition { rowIndex / (itemsPerPage.primaryRepetition * itemsPerPage.secondaryRepetition),
                          rowIndex % itemsPerPage.primaryRepetition,
                          (rowIndex / itemsPerPage.primaryRepetition) % itemsPerPage.secondaryRepetition };
}

}    // namespace document::service::documentgenerator
