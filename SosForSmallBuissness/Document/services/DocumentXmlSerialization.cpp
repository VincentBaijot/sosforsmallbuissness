#include "DocumentXmlSerialization.hpp"

#include "DocumentXmlConstants.hpp"
#include "DocumentXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"
#include "data/DocumentData.hpp"
#include "data/DocumentHandlerData.hpp"

namespace document::service
{

std::optional<document::data::DocumentData> readDocumentData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, documentElement, u"DocumentData")) { return std::nullopt; }

    return details::readDocumentData(reader);
}

void writeDocumentData(QXmlStreamWriter& writer, const document::data::DocumentData& documentData)
{
    details::writeDocumentData(writer, documentData);
}

std::optional<document::data::DocumentHandlerData> readDocumentHandlerData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, documentHandlerElement, u"DocumentHandlerData"))
    {
        return std::nullopt;
    }

    return details::readDocumentHandlerData(reader);
}

void writeDocumentHandlerData(QXmlStreamWriter& writer,
                              const document::data::DocumentHandlerData& documentHandlerData)
{
    details::writeDocumentHandlerData(writer, documentHandlerData);
}

}    // namespace document::service
