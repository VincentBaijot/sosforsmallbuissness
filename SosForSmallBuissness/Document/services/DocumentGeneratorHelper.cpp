#include "DocumentGeneratorHelper.hpp"

#include "Constants.hpp"
#include "DocumentGeneratorDetails.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "data/DataHandlerData.hpp"
#include "data/DataTemplateAssociationData.hpp"

namespace document::service::documentgenerator
{

void DocumentGeneratorHelper::initialize(
  const documenttemplate::data::DocumentTemplateData& documentTemplateData)
{
    _generatedDocuments.documentData.pageSize = documentTemplateData.pageSizeData;
    _generatedDocuments.documentData.pageItemsList.push_back(painter::data::PaintableItemListModelData {});
    _generatedDocuments.dataItemsAssociationData.pageItemsAssociationData.push_back(
      document::data::PageItemsAssociationData {});
}

void DocumentGeneratorHelper::addPaintableRectangleData(
  const painter::data::PaintableRectangleData& rectangleData,
  int index)
{
    document::data::ItemAssociationData itemAssociationData { -1, index };

    _generalItemsList.paintableItemList.push_back(rectangleData);
    _generalItemsAssociation.itemsAssociationData.push_back(itemAssociationData);

    for (int pageIndex = 0; pageIndex < _generatedDocuments.documentData.pageItemsList.size(); ++pageIndex)
    {
        _generatedDocuments.documentData.pageItemsList[pageIndex].paintableItemList.push_back(rectangleData);
        _generatedDocuments.dataItemsAssociationData.pageItemsAssociationData[pageIndex]
          .itemsAssociationData.push_back(itemAssociationData);
    }
}

void DocumentGeneratorHelper::addPaintableTextData(
  const painter::data::PaintableTextData& textData,
  const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
  int index)
{
    painter::data::PaintableTextData targetText = textData;

    // Quick fix for now because i take first just after !
    // TODO to correct
    if (!dataHandlerData.userDataHandler.generalVariableDataList.dataTables.isEmpty())
    {
        const documentdatadefinitions::data::DataTableData& dataTableData =
          dataHandlerData.userDataHandler.generalVariableDataList.dataTables.first();

        const QList<documentdatadefinitions::data::DataDefinitionData>& dataDefinitions =
          dataHandlerData.dataDefinitionsHandler.generalVariableList.dataDefinitions;

        for (int dataDefinitionIndex = 0; dataDefinitionIndex < dataDefinitions.size(); ++dataDefinitionIndex)
        {
            const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData =
              dataDefinitions.at(dataDefinitionIndex);

            targetText.text = details::generateTemplatedText(
              targetText.text, dataDefinitionData.key, dataTableData.value(dataDefinitionIndex));
        }
    }

    document::data::ItemAssociationData itemAssociationData { -1, index };

    _generalItemsList.paintableItemList.push_back(targetText);
    _generalItemsAssociation.itemsAssociationData.push_back(itemAssociationData);
    for (int pageIndex = 0; pageIndex < _generatedDocuments.documentData.pageItemsList.size(); ++pageIndex)
    {
        _generatedDocuments.documentData.pageItemsList[pageIndex].paintableItemList.push_back(targetText);
        _generatedDocuments.dataItemsAssociationData.pageItemsAssociationData[pageIndex]
          .itemsAssociationData.push_back(itemAssociationData);
    }
}

void DocumentGeneratorHelper::addPaintableListData(
  const painter::data::PaintableListData& paintableListData,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData,
  const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
  int index)
{
    int dataDefinitionIndex =
      dataTemplateAssociationData.paintableListDataDefinitionListAssociationData.association.value(index, -1);

    if (dataDefinitionIndex == -1)
    {
        qCWarning(documentServiceCategory) << "Invalid data definition index";
        return;
    }

    const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinition =
      dataHandlerData.dataDefinitionsHandler.listDataDefinitionList.listOfDataDefinition.at(dataDefinitionIndex);

    int associatedDataIndex =
      dataTemplateAssociationData.paintableListDataTableModelAssociationData.association.value(index, -1);

    if (associatedDataIndex == -1)
    {
        qCWarning(documentServiceCategory) << "Invalid associated data index";
        return;
    }

    const documentdatadefinitions::data::DataTablesData& associatedData =
      dataHandlerData.userDataHandler.dataTableModelListModel.dataTableModelList.at(associatedDataIndex);

    details::GeneratedItems generatedListItems = details::generateListItems(
      paintableListData, associatedData, listDataDefinition.dataDefinitionListData.dataDefinitions);

    while (generatedListItems.pageItemsList.size() > _generatedDocuments.documentData.pageItemsList.size())
    {
        _generatedDocuments.documentData.pageItemsList.push_back(_generalItemsList);
        _generatedDocuments.dataItemsAssociationData.pageItemsAssociationData.push_back(_generalItemsAssociation);
    }

    for (int pageNumber = 0; pageNumber < generatedListItems.pageItemsList.size(); ++pageNumber)
    {
        _generatedDocuments.documentData.pageItemsList[pageNumber].paintableItemList.append(
          generatedListItems.pageItemsList.at(pageNumber).paintableItemList);

        std::ranges::for_each(
          generatedListItems.dataItemsAssociationData.pageItemsAssociationData,
          [associatedDataIndex, index](document::data::PageItemsAssociationData& pageItemsAssociation)
          {
              std::ranges::for_each(
                pageItemsAssociation.itemsAssociationData,
                [associatedDataIndex, index](document::data::ItemAssociationData& itemAssociation)
                {
                    itemAssociation.templateListIndex = index;
                    itemAssociation.modelIndex        = associatedDataIndex;
                });
          });

        _generatedDocuments.dataItemsAssociationData.pageItemsAssociationData[pageNumber]
          .itemsAssociationData.append(
            generatedListItems.dataItemsAssociationData.pageItemsAssociationData.at(pageNumber)
              .itemsAssociationData);
    }
}

const GeneratedDocument& DocumentGeneratorHelper::generatedDocument() const
{
    return _generatedDocuments;
}

}    // namespace document::service::documentgenerator
