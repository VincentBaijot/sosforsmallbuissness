#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "DocumentService_global.hpp"

namespace document::data
{
struct DocumentData;
struct DocumentHandlerData;
}    // namespace document::data

namespace document::service
{
DOCUMENT_SERVICE_EXPORT std::optional<document::data::DocumentData> readDocumentData(QXmlStreamReader& reader);
DOCUMENT_SERVICE_EXPORT void writeDocumentData(QXmlStreamWriter& writer,
                                               const document::data::DocumentData& documentData);

DOCUMENT_SERVICE_EXPORT std::optional<document::data::DocumentHandlerData> readDocumentHandlerData(
  QXmlStreamReader& reader);
DOCUMENT_SERVICE_EXPORT void writeDocumentHandlerData(
  QXmlStreamWriter& writer,
  const document::data::DocumentHandlerData& documentHandlerData);
}    // namespace document::service
