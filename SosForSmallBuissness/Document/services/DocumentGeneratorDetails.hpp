#pragma once

#include "Document/data/DataItemsAssociationData.hpp"
#include "Painter/data/PaintableItemListModelData.hpp"
#include "Painter/data/PaintableListData.hpp"
#include "data/DataDefinitionData.hpp"
#include "data/DataTablesData.hpp"

namespace document::service::documentgenerator::details
{
struct GeneratedItems
{
    QList<painter::data::PaintableItemListModelData> pageItemsList;
    document::data::DataItemsAssociationData dataItemsAssociationData;
};

void updateTextItem(painter::data::PaintableItemListModelData& paintableItemRow,
                    const QList<documentdatadefinitions::data::DataDefinitionData>& dataDefinitions,
                    const documentdatadefinitions::data::DataTableData& dataTableData);

GeneratedItems generateListItems(const painter::data::PaintableListData& listData,
                                 const documentdatadefinitions::data::DataTablesData& associatedData,
                                 const QList<documentdatadefinitions::data::DataDefinitionData>& dataDefinitions);

QString generateTemplatedText(const QString& sourceTemplateText, const QString& dataKey, const QString& dataText);

}    // namespace document::service::documentgenerator::details
