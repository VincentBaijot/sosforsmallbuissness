#pragma once

#include "Document/data/DataItemsAssociationData.hpp"
#include "Document/data/GeneratedDocument.hpp"
#include "Painter/data/PaintableItemListModelData.hpp"

namespace documenttemplate::data
{
struct DocumentTemplateData;
}

namespace documentdatadefinitions::data
{
struct DataHandlerData;
}

namespace datatemplateassociation::data
{
struct DataTemplateAssociationData;
}

namespace painter::data
{
struct PaintableListData;
}

namespace document::service::documentgenerator
{

class DocumentGeneratorHelper
{
  public:
    void initialize(const documenttemplate::data::DocumentTemplateData& documentTemplateData);

    void addPaintableRectangleData(const painter::data::PaintableRectangleData& rectangleData, int index);
    void addPaintableTextData(const painter::data::PaintableTextData& textData,
                              const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
                              int index);
    void addPaintableListData(
      const painter::data::PaintableListData& paintableListData,
      const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData,
      const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
      int index);

    [[nodiscard]] const GeneratedDocument& generatedDocument() const;

  private:
    painter::data::PaintableItemListModelData _generalItemsList;
    document::data::PageItemsAssociationData _generalItemsAssociation;
    GeneratedDocument _generatedDocuments;
};

}    // namespace document::service::documentgenerator
