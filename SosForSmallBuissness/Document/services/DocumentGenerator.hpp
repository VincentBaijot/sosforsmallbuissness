#pragma once

#include "Document/data/GeneratedDocument.hpp"
#include "DocumentService_global.hpp"

namespace documenttemplate::data
{
struct DocumentTemplateData;
}

namespace documentdatadefinitions::data
{
struct DataHandlerData;
}

namespace datatemplateassociation::data
{
struct DataTemplateAssociationData;
}

namespace document::service::documentgenerator
{

[[nodiscard]] DOCUMENT_SERVICE_EXPORT GeneratedDocument
generateDocument(const documenttemplate::data::DocumentTemplateData& documentTemplateData,
                 const documentdatadefinitions::data::DataHandlerData& dataHandlerData,
                 const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData);

[[nodiscard]] DOCUMENT_SERVICE_EXPORT QString generateTemplatedText(const QString& sourceTemplateText,
                                                                    const QString& dataKey,
                                                                    const QString& dataText);

}    // namespace document::service::documentgenerator
