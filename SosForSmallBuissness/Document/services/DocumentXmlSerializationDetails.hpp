#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

namespace document::data
{
struct DocumentData;
struct DocumentHandlerData;
}    // namespace document::data

namespace document::service::details
{
document::data::DocumentData readDocumentData(QXmlStreamReader& reader);
void writeDocumentData(QXmlStreamWriter& writer, const document::data::DocumentData& documentData);

document::data::DocumentHandlerData readDocumentHandlerData(QXmlStreamReader& reader);
void writeDocumentHandlerData(QXmlStreamWriter& writer,
                              const document::data::DocumentHandlerData& documentHandlerData);
}    // namespace document::service::details
