#pragma once

#include "Document/data/ItemPerPage.hpp"
#include "Document/data/ItemPosition.hpp"
#include "DocumentService_global.hpp"
#include "Painter/data/PaintableListData.hpp"

namespace document::service::documentgenerator
{
[[nodiscard]] DOCUMENT_SERVICE_EXPORT painter::data::PositionData updateListItemPosition(
  const painter::data::PositionData& listPosition,
  const painter::data::PositionData& itemSourcePosition,
  const painter::data::PositionData& listRowRect,
  painter::data::PaintableListData::RepetitionDirection primaryRepetitionDirection,
  int primaryRepetitionListIndex,
  painter::data::PaintableListData::RepetitionDirection secondaryRepetitionDirection,
  int secondaryRepetitionListIndex);

[[nodiscard]] DOCUMENT_SERVICE_EXPORT ItemsPerPage
calculateItemsPerPage(const painter::data::PaintableListData& paintableListData);

[[nodiscard]] DOCUMENT_SERVICE_EXPORT ItemPosition calculateItemPositionFromIndex(ItemsPerPage itemsPerPage,
                                                                                  int rowIndex);

[[nodiscard]] DOCUMENT_SERVICE_EXPORT painter::data::PositionData childrenRectList(
  const painter::data::PaintableItemListModelData& listData);

int repetitionNumber(const painter::data::PaintableListData& paintableListData,
                     const painter::data::PositionData& childrenRect,
                     painter::data::PaintableListData::RepetitionDirection repetitionDirection);

[[nodiscard]] constexpr painter::data::PositionData position(
  const painter::data::PaintableRectangleData& paintableRectangle)
{
    return paintableRectangle.position.position;
}
[[nodiscard]] constexpr painter::data::PositionData position(
  const painter::data::PaintableTextData& paintableText)
{
    return paintableText.background.position.position;
}
[[nodiscard]] constexpr painter::data::PositionData position(
  const painter::data::VariantListableItemData& itemData)
{
    return std::visit([](const auto& item) -> painter::data::PositionData { return position(item); },
                      itemData);
}

constexpr void setPosition(painter::data::PaintableRectangleData& paintableRectangle,
                           const painter::data::PositionData& position)
{
    paintableRectangle.position.position = position;
}
constexpr void setPosition(painter::data::PaintableTextData& paintableText,
                           const painter::data::PositionData& position)
{
    paintableText.background.position.position = position;
}
constexpr void setPosition(painter::data::VariantListableItemData& itemData,
                           const painter::data::PositionData& position)
{
    std::visit([position](auto& item) { setPosition(item, position); }, itemData);
}

[[nodiscard]] DOCUMENT_SERVICE_TESTING_EXPORT painter::data::PositionData updateItemPosition(
  const painter::data::PositionData& listPosition,
  const painter::data::PositionData& itemSourcePosition,
  const painter::data::PositionData& listRowRect,
  painter::data::PaintableListData::RepetitionDirection repetitionDirection,
  int listIndex);

}    // namespace document::service::documentgenerator
