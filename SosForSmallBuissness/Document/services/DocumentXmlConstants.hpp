#pragma once

#include <QStringView>

namespace document::service
{
inline constexpr const QStringView documentElement        = u"Document";
inline constexpr const QStringView documentHandlerElement = u"DocumentHandler";
}    // namespace document::service
