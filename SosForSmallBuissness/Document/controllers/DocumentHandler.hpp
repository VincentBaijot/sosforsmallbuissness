#pragma once

#include <QtQml/QQmlEngine>

#include "Document.hpp"
#include "DocumentController_global.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"
#include "data/DocumentData.hpp"

namespace document::data
{
struct DocumentHandlerData;
}

namespace document::controller
{
inline constexpr char documentHandlerHeaderData[] = "Document";

class DOCUMENT_CONTROLLER_EXPORT DocumentHandler
    : public utils::SimpleDataObjectListModel<Document, document::data::DocumentData, documentHandlerHeaderData>
{
    Q_OBJECT

    QML_ELEMENT
  public:
    explicit DocumentHandler(QObject* parent = nullptr);
    explicit DocumentHandler(const document::data::DocumentHandlerData& newData, QObject* parent = nullptr);
    ~DocumentHandler() override = default;

    document::data::DocumentHandlerData objectData() const;
    void setObjectData(const document::data::DocumentHandlerData& newData);

    // Create an alias on display to avoid issues with the display property in TabButton
    enum class DocumentHandlerRole
    {
        DocumentRole = Qt::UserRole + 1
    };
    Q_ENUM(DocumentHandlerRole)
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

  private:
    Q_DISABLE_COPY_MOVE(DocumentHandler)
};
}    // namespace document::controller
