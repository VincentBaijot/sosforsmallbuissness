#include "DocumentHandler.hpp"

#include "Document.hpp"
#include "data/DocumentHandlerData.hpp"

namespace document::controller
{
DocumentHandler::DocumentHandler(QObject* parent)
    : SimpleDataObjectListModel<Document, document::data::DocumentData, documentHandlerHeaderData> { parent }
{
}

DocumentHandler::DocumentHandler(const document::data::DocumentHandlerData& newData, QObject* parent)
    : SimpleDataObjectListModel<Document, document::data::DocumentData, documentHandlerHeaderData> {
          newData.documents,
          parent
      }
{
}

document::data::DocumentHandlerData DocumentHandler::objectData() const
{
    document::data::DocumentHandlerData data;
    data.documents = SimpleDataObjectListModel::listData();
    return data;
}

void DocumentHandler::setObjectData(const document::data::DocumentHandlerData& newData)
{
    if (objectData() == newData) { return; }

    setListData(newData.documents);
}

QVariant DocumentHandler::data(const QModelIndex& index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return QVariant(); }

    switch (role)
    {
        case Qt::DisplayRole:
            Q_FALLTHROUGH();
        case static_cast<int>(DocumentHandlerRole::DocumentRole):
            return QVariant::fromValue(objects().at(index.row()).get());
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> DocumentHandler::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles.insert(static_cast<int>(DocumentHandlerRole::DocumentRole), "document");
    return roles;
}
}    // namespace document::controller
