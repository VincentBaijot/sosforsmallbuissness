#pragma once

#include <QtQml/QQmlEngine>

#include "DocumentController_global.hpp"
#include "Painter/controllers/ListableItemListModel.hpp"
#include "Painter/data/PaintableItemListModelData.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

Q_MOC_INCLUDE("DocumentTemplate/controllers//PageSize.hpp")

namespace painter::data
{
struct PaintableItemListModelData;
}

namespace document::data
{
struct DocumentData;
}

namespace documenttemplate::controller
{
class PageSize;
}

namespace document::controller
{
inline constexpr char documentHeaderData[] = "Page";

class DOCUMENT_CONTROLLER_EXPORT Document
    : public utils::SimpleDataObjectListModel<painter::controller::ListableItemListModel,
                                              painter::data::PaintableItemListModelData,
                                              documentHeaderData>
{
    Q_OBJECT

    Q_PROPERTY(documenttemplate::controller::PageSize* pageSize READ pageSize CONSTANT)

    QML_ELEMENT
  public:
    explicit Document(QObject* parent = nullptr);
    explicit Document(const document::data::DocumentData& newData, QObject* parent = nullptr);
    ~Document() override;

    document::data::DocumentData objectData() const;
    void setObjectData(const document::data::DocumentData& newData);

    documenttemplate::controller::PageSize* pageSize() const;

    void addPage();

  private:
    Q_DISABLE_COPY_MOVE(Document)

    utils::own_qobject<documenttemplate::controller::PageSize> _pageSize;
};

}    // namespace document::controller
