#include "Document.hpp"

#include "DocumentTemplate/controllers/PageSize.hpp"
#include "data/DocumentData.hpp"

namespace document::controller
{
Document::Document(QObject* parent)
    : SimpleDataObjectListModel<painter::controller::ListableItemListModel,
                                painter::data::PaintableItemListModelData,
                                documentHeaderData> { parent },
      _pageSize { utils::make_qobject<documenttemplate::controller::PageSize>(*this) }
{
}

Document::Document(const document::data::DocumentData& newData, QObject* parent)
    : SimpleDataObjectListModel<painter::controller::ListableItemListModel,
                                painter::data::PaintableItemListModelData,
                                documentHeaderData> { newData.pageItemsList, parent },
      _pageSize { utils::make_qobject<documenttemplate::controller::PageSize>(*this, newData.pageSize) }
{
}

Document::~Document() = default;

document::data::DocumentData Document::objectData() const
{
    document::data::DocumentData documentData;

    documentData.pageSize      = _pageSize->objectData();
    documentData.pageItemsList = listData();

    return documentData;
}

void Document::setObjectData(const document::data::DocumentData& newData)
{
    if (objectData() == newData) { return; }

    _pageSize->setObjectData(newData.pageSize);

    setListData(newData.pageItemsList);
}

documenttemplate::controller::PageSize* Document::pageSize() const
{
    return _pageSize.get();
}

void Document::addPage()
{
    objects().emplace_back(utils::make_qobject<painter::controller::ListableItemListModel>(*this));
}

}    // namespace document::controller
