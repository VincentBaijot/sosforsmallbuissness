#include <QtQml/qqmlextensionplugin.h>

#include "Application/Application.hpp"

int main(int argc, char* argv[])
{
    application::Application application;

    return application.executeApplication(std::span { argv, static_cast<std::span<char*>::size_type>(argc) });
}
