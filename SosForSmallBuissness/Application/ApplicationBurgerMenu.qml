import SosQml.Controls
import SosForSmallBuissness.Application

AnimatedVerticalMenu {
    id: root

    required property ApplicationFeatureHandler applicationFeatureHandler

    MenuButton {
        id: createBilling

        anchors {
            top: root.top
            left: root.left
            right: root.right
            margins: 10
        }

        text: root.isFullyOpen ? qsTr("Create billing") : ""
        selected: root.applicationFeatureHandler.selectedFeature
                  === ApplicationFeatureHandler.Billing
        onClicked: {
            root.applicationFeatureHandler.selectedFeature = ApplicationFeatureHandler.Billing
        }
    }

    MenuButton {
        id: editTemplates

        anchors {
            top: createBilling.bottom
            left: root.left
            right: root.right
            margins: 10
        }

        text: root.isFullyOpen ? qsTr("Edit templates") : ""
        selected: root.applicationFeatureHandler.selectedFeature
                  === ApplicationFeatureHandler.EditTemplates
        onClicked: {
            root.applicationFeatureHandler.selectedFeature = ApplicationFeatureHandler.EditTemplates
        }
    }

    MenuButton {
        id: loading

        anchors {
            top: editTemplates.bottom
            left: root.left
            right: root.right
            margins: 10
        }

        text: root.isFullyOpen ? qsTr("Loading") : ""
        selected: root.applicationFeatureHandler.selectedFeature
                  === ApplicationFeatureHandler.Loading
        onClicked: {
            root.applicationFeatureHandler.selectedFeature = ApplicationFeatureHandler.Loading
        }
    }
}
