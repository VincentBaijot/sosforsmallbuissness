#pragma once

#include <QObject>
#include <span>

#include "Application_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

class QGuiApplication;
class QTranslator;
class QQmlApplicationEngine;

namespace serialization
{
class ApplicationFeatureSerialization;
}

namespace application
{
class ApplicationFeatureHandler;

class APPLICATION_EXPORT Application : public QObject
{
    Q_OBJECT
  public:
    explicit Application(QObject* parent = nullptr);
    ~Application() override;

    int executeApplication(std::span<char*> args);

    static const QUrl MAIN_QML_URL;

  private slots:
    static void _onQmlLoaded(const QObject* obj, const QUrl& objUrl);

  private:
    Q_DISABLE_COPY_MOVE(Application)

    static void _setupAppEnvironement();
    void _createQApplication(std::span<char*> args);
    static void _createFolders();
    static void _initializeLogger();
    void _applyTranslations();
    void _createApplicationFeatureHandler();
    void _createApplicationFeatureSerialization();
    void _initializeQmlEngine();
    void _cleanInstances();

    std::unique_ptr<QGuiApplication> _qApplication;
    utils::own_qobject<QTranslator> _translator;
    utils::own_qobject<QQmlApplicationEngine> _qmlEngine;
    utils::own_qobject<ApplicationFeatureHandler> _applicationFeatureHandler;
    std::unique_ptr<serialization::ApplicationFeatureSerialization> _applicationFeatureSerialization;
    std::span<char*> _args;
    int _argv;
};
}    // namespace application
