#include "Application.hpp"

#include <QApplication>
#include <QQuickStyle>
#include <QTranslator>
#include <QtQml/QQmlApplicationEngine>

#include "ApplicationFeatureHandler.hpp"
#include "Constants.hpp"
#include "Logger/Logger.hpp"
#include "Serialization/ApplicationFeatureSerialization.hpp"
#include "Utils/AppFileSystem.hpp"

namespace application
{
using namespace Qt::Literals::StringLiterals;

constexpr QStringView Q_QUICK_STYLE  = u"Fusion";
const QUrl Application::MAIN_QML_URL = u"qrc:/qt/qml/SosForSmallBuissness/Application/Application.qml"_s;

Application::Application(QObject* parent) : QObject { parent }
{
}

Application::~Application()
{
    qCInfo(applicationCategory) << "Close application";
    _cleanInstances();
    qCInfo(applicationCategory) << "Application closed";
}

int Application::executeApplication(std::span<char*> args)
{
    _setupAppEnvironement();

    _cleanInstances();

    qCInfo(applicationCategory) << "Create application";

    _createQApplication(args);
    _applyTranslations();

    qCInfo(applicationCategory) << "Initialize folders and logger";

    _createFolders();
    _initializeLogger();

    qCInfo(applicationCategory) << "Create application data models";

    _createApplicationFeatureSerialization();
    _createApplicationFeatureHandler();

    qCInfo(applicationCategory) << "Startup of the application";

    _initializeQmlEngine();

    return QGuiApplication::exec();
}

void Application::_onQmlLoaded(const QObject* obj, const QUrl& objUrl)
{
    if (!obj && MAIN_QML_URL == objUrl) QCoreApplication::exit(-1);
}

void Application::_setupAppEnvironement()
{
    QCoreApplication::setApplicationName(u"SosForSmallBuissness"_s);

    if (qEnvironmentVariable("QT_LOGGING_CONF").isEmpty())
    {
        qputenv("QT_LOGGING_CONF",
                qUtf8Printable(utils::AppFileSystem::sourceApplicationFolder() + "/soslogging.ini"));
    }
}

void Application::_createQApplication(std::span<char*> args)
{
    _args         = args;
    _argv         = static_cast<int>(_args.size());
    _qApplication = std::make_unique<QApplication>(_argv, args.data());
}

void Application::_createFolders()
{
    utils::AppFileSystem::createFolders();
}

void Application::_initializeLogger()
{
    logger::cleanLogger(QDir(utils::AppFileSystem::logFolder()));

    logger::setLogFolder(QDir(utils::AppFileSystem::logFolder()));
    qInstallMessageHandler(logger::messageHandler);
}

void Application::_applyTranslations()
{
    Q_ASSERT(_qApplication);

    _translator                   = utils::make_qobject<QTranslator>(*this);
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString& locale : uiLanguages)
    {
        const QString baseName = QCoreApplication::applicationName() + "_" + QLocale(locale).name();
        if (_translator->load(":/i18n/" + baseName))
        {
            if (QGuiApplication::installTranslator(_translator.get()))
            {
                qCInfo(applicationCategory) << "Successfully installed translation " << baseName;
            }
            else { qCWarning(applicationCategory) << "Failed to install translation " << baseName; }
            break;
        }
        else { qCWarning(applicationCategory) << "Failed to load translation file " << baseName; }
    }
}

void Application::_createApplicationFeatureHandler()
{
    Q_ASSERT(_applicationFeatureSerialization);

    qCDebug(applicationCategory) << "Creation of application feature handler";

    _applicationFeatureHandler = utils::make_qobject<ApplicationFeatureHandler>(*this);

    _applicationFeatureHandler->setApplicationFeatureSerialization(_applicationFeatureSerialization.get());
    _applicationFeatureHandler->setSelectedFeature(ApplicationFeatureHandler::ApplicationFeatures::Billing);
}

void Application::_createApplicationFeatureSerialization()
{
    _applicationFeatureSerialization = std::make_unique<serialization::ApplicationFeatureSerialization>();
    _applicationFeatureSerialization->setDataFolder(utils::AppFileSystem::dataFolder());
}

void Application::_initializeQmlEngine()
{
    QQuickStyle::setStyle(Q_QUICK_STYLE.toString());

    _qmlEngine = utils::make_qobject<QQmlApplicationEngine>(*this);

    QString importSubPath;
    importSubPath.append(QCoreApplication::applicationDirPath());
    importSubPath.append(QDir::separator());
    importSubPath.append(u"SosForSmallBuissness"_s);
    _qmlEngine->addImportPath(importSubPath);

    _qmlEngine->setInitialProperties(
      { { u"applicationFeatureHandler"_s, QVariant::fromValue(_applicationFeatureHandler.get()) } });

    QObject::connect(_qmlEngine.get(),
                     &QQmlApplicationEngine::objectCreated,
                     this,
                     &Application::_onQmlLoaded,
                     Qt::QueuedConnection);

    _qmlEngine->load(MAIN_QML_URL);
}

void Application::_cleanInstances()
{
    if (_qmlEngine) { _qmlEngine.reset(); }

    if (_applicationFeatureHandler) { _applicationFeatureHandler.reset(); }

    if (_translator) { _translator.reset(); }
}
}    // namespace application
