#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("DataTemplateAssociation/controllers/DataTemplateAssociationMap.hpp")
Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/DataHandler.hpp")
Q_MOC_INCLUDE("DocumentTemplate/controllers//TemplatesHandler.hpp")
Q_MOC_INCLUDE("Document/controllers/DocumentHandler.hpp")

namespace documenttemplate::controller
{
class TemplatesHandler;
}

namespace document::controller
{
class DocumentHandler;
}

namespace documentdatadefinitions::controller
{
class DataHandler;
}

namespace datatemplateassociation::controller
{
class DataTemplateAssociationMap;
}

namespace datadocumentassociation::controller
{
class DataDocumentsHandlerAssociation;
}

namespace serialization
{
class ApplicationFeatureSerialization;
}

namespace application
{
class ApplicationFeatureHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(ApplicationFeatures selectedFeature READ selectedFeature WRITE setSelectedFeature NOTIFY
                 selectedFeatureChanged)
    Q_PROPERTY(
      documenttemplate::controller::TemplatesHandler* templatesHandler READ templatesHandler NOTIFY templatesHandlerChanged)
    Q_PROPERTY(
      documentdatadefinitions::controller::DataHandler* dataHandler READ dataHandler NOTIFY dataHandlerChanged)
    Q_PROPERTY(datatemplateassociation::controller::DataTemplateAssociationMap* dataTemplateAssociationMap READ
                 dataTemplateAssociationMap NOTIFY dataTemplateAssociationMapChanged)
    Q_PROPERTY(
      document::controller::DocumentHandler* documentHandler READ documentHandler NOTIFY documentHandlerChanged)

    QML_ELEMENT
  public:
    enum class ApplicationFeatures
    {
        NoFeature,
        Loading,
        Billing,
        EditTemplates
    };
    Q_ENUM(ApplicationFeatures)

    explicit ApplicationFeatureHandler(QObject* parent = nullptr);
    ~ApplicationFeatureHandler() override;

    ApplicationFeatureHandler::ApplicationFeatures selectedFeature() const;
    void setSelectedFeature(ApplicationFeatureHandler::ApplicationFeatures newSelectedFeature);

    serialization::ApplicationFeatureSerialization* applicationFeatureSerialization() const;
    void setApplicationFeatureSerialization(
      serialization::ApplicationFeatureSerialization* newApplicationFeatureSerialization);

    documenttemplate::controller::TemplatesHandler* templatesHandler() const;
    document::controller::DocumentHandler* documentHandler() const;
    documentdatadefinitions::controller::DataHandler* dataHandler() const;
    datatemplateassociation::controller::DataTemplateAssociationMap* dataTemplateAssociationMap() const;

  signals:
    void selectedFeatureChanged();
    void templatesHandlerChanged();
    void dataHandlerChanged();
    void dataTemplateAssociationMapChanged();
    void documentHandlerChanged();
    void dataDocumentHandlerAssociationChanged();

  private:
    Q_DISABLE_COPY_MOVE(ApplicationFeatureHandler)

    void _persistAndCleanFeatures();
    void _loadSelectedFeature(ApplicationFeatureHandler::ApplicationFeatures newSelectedFeature);
    void _loadBillingFeature();
    void _loadEditTemplatesFeatures();

    ApplicationFeatures _selectedFeature { ApplicationFeatures::NoFeature };

    utils::own_qobject<documenttemplate::controller::TemplatesHandler> _templatesHandler;
    utils::own_qobject<document::controller::DocumentHandler> _documentHandler;
    utils::own_qobject<documentdatadefinitions::controller::DataHandler> _dataHandler;
    utils::own_qobject<datatemplateassociation::controller::DataTemplateAssociationMap>
      _dataDocumentAssociationMap;
    utils::own_qobject<datadocumentassociation::controller::DataDocumentsHandlerAssociation>
      _dataDocumentHandlerAssociation;

    serialization::ApplicationFeatureSerialization* _applicationFeatureSerialization { nullptr };
};
}    // namespace application
