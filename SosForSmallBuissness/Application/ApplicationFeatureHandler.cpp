#include "ApplicationFeatureHandler.hpp"

#include "Constants.hpp"
#include "DataDocumentAssociation/controllers/DataDocumentsHandlerAssociation.hpp"
#include "DataDocumentAssociation/services/DataDocumentAssociationService.hpp"
#include "DataTemplateAssociation/controllers/DataTemplateAssociationMap.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "Document/controllers/DocumentHandler.hpp"
#include "Document/data/DocumentHandlerData.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/data/DataHandlerData.hpp"
#include "DocumentTemplate/controllers/TemplatesHandler.hpp"
#include "Serialization/ApplicationFeatureSerialization.hpp"

namespace application
{
ApplicationFeatureHandler::ApplicationFeatureHandler(QObject* parent) : QObject { parent }
{
}

ApplicationFeatureHandler::~ApplicationFeatureHandler()
{
    _persistAndCleanFeatures();
}

ApplicationFeatureHandler::ApplicationFeatures ApplicationFeatureHandler::selectedFeature() const
{
    return _selectedFeature;
}

void ApplicationFeatureHandler::setSelectedFeature(
  ApplicationFeatureHandler::ApplicationFeatures newSelectedFeature)
{
    Q_ASSERT_X(_applicationFeatureSerialization,
               "ApplicationFeatureHandler::setSelectedFeature",
               "ApplicationFeatureSerialization is null");
    if (_selectedFeature == newSelectedFeature) return;

    qCInfo(applicationCategory) << "Selection of the feature " << newSelectedFeature;

    _selectedFeature = ApplicationFeatures::Loading;
    emit selectedFeatureChanged();

    qCDebug(applicationCategory) << "Persist and clear all features";
    _persistAndCleanFeatures();

    qCDebug(applicationCategory) << "Load the selected feature";

    _loadSelectedFeature(newSelectedFeature);

    qCDebug(applicationCategory) << "Selected feature loaded";

    _selectedFeature = newSelectedFeature;
    emit selectedFeatureChanged();
}

serialization::ApplicationFeatureSerialization* ApplicationFeatureHandler::applicationFeatureSerialization() const
{
    return _applicationFeatureSerialization;
}

void ApplicationFeatureHandler::setApplicationFeatureSerialization(
  serialization::ApplicationFeatureSerialization* newApplicationFeatureSerialization)
{
    _applicationFeatureSerialization = newApplicationFeatureSerialization;
}

void ApplicationFeatureHandler::_persistAndCleanFeatures()
{
    if (_dataDocumentAssociationMap)
    {
        _applicationFeatureSerialization->saveDataTemplateAssociationMapData(
          _dataDocumentAssociationMap->objectData());
        _dataDocumentAssociationMap.reset();
        emit dataTemplateAssociationMapChanged();
    }

    if (_templatesHandler)
    {
        _applicationFeatureSerialization->saveTemplatesHandlerData(_templatesHandler->objectData());
        _templatesHandler.reset();
        emit templatesHandlerChanged();
    }

    if (_documentHandler)
    {
        _applicationFeatureSerialization->saveDocumentHandlerData(_documentHandler->objectData());
        _documentHandler.reset();
        emit documentHandlerChanged();
    }

    if (_dataHandler)
    {
        _applicationFeatureSerialization->saveDataHandlerData(_dataHandler->objectData());
        _dataHandler.reset();
        emit dataHandlerChanged();
    }

    if (_dataDocumentHandlerAssociation)
    {
        _dataDocumentHandlerAssociation.reset();
        emit dataDocumentHandlerAssociationChanged();
    }
}

void ApplicationFeatureHandler::_loadSelectedFeature(ApplicationFeatures newSelectedFeature)
{
    if (newSelectedFeature == ApplicationFeatures::Billing) { _loadBillingFeature(); }
    else { _loadEditTemplatesFeatures(); }
}

void ApplicationFeatureHandler::_loadBillingFeature()
{
    document::data::DocumentHandlerData documentHandler;

    documenttemplate::data::TemplatesHandlerData templatesHandlerData =
      _applicationFeatureSerialization->loadTemplatesHandlerData();
    documentdatadefinitions::data::DataHandlerData dataHandler =
      _applicationFeatureSerialization->loadDataHandlerData();
    datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData =
      _applicationFeatureSerialization->loadDataTemplateAssociationMapData();

    datadocumentassociation::data::DataDocumentsHandlerAssociationData dataDocumentHandlerAssociationData;

    for (int documentTemplateIndex = 0; documentTemplateIndex < templatesHandlerData.documentTemplates.size();
         ++documentTemplateIndex)
    {
        document::service::documentgenerator::GeneratedDocument generatedDocument =
          document::service::documentgenerator::generateDocument(
            templatesHandlerData.documentTemplates.at(documentTemplateIndex),
            dataHandler,
            dataTemplateAssociationMapData.dataDocumentAssociationMap.value(documentTemplateIndex));
        documentHandler.documents.append(generatedDocument.documentData);

        datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData =
          datadocumentassociation::service::generateDataDocumentAssociationData(generatedDocument);

        qDebug() << "GeneratedDocument : {" << generatedDocument.documentData << ","
                 << generatedDocument.dataItemsAssociationData << "}";
        qDebug() << "DataDocumentAssociationData : " << dataDocumentAssociationData;

        dataDocumentHandlerAssociationData.dataDocumentsAssociationData.insert(documentTemplateIndex,
                                                                               dataDocumentAssociationData);
    }

    _documentHandler = utils::make_qobject<document::controller::DocumentHandler>(*this, documentHandler);
    emit documentHandlerChanged();
    _dataHandler = utils::make_qobject<documentdatadefinitions::controller::DataHandler>(*this, dataHandler);
    emit dataHandlerChanged();
    _dataDocumentHandlerAssociation =
      utils::make_qobject<datadocumentassociation::controller::DataDocumentsHandlerAssociation>(
        *this,
        _documentHandler.get(),
        _dataHandler->userDataHandler(),
        templatesHandlerData,
        dataTemplateAssociationMapData,
        dataDocumentHandlerAssociationData);
    emit dataDocumentHandlerAssociationChanged();
}

void ApplicationFeatureHandler::_loadEditTemplatesFeatures()
{
    _templatesHandler = utils::make_qobject<documenttemplate::controller::TemplatesHandler>(
      *this, _applicationFeatureSerialization->loadTemplatesHandlerData());
    emit templatesHandlerChanged();
    _dataHandler = utils::make_qobject<documentdatadefinitions::controller::DataHandler>(
      *this, _applicationFeatureSerialization->loadDataHandlerData());
    emit dataHandlerChanged();
    _dataDocumentAssociationMap =
      utils::make_qobject<datatemplateassociation::controller::DataTemplateAssociationMap>(
        *this,
        _dataHandler.get(),
        _templatesHandler.get(),
        _applicationFeatureSerialization->loadDataTemplateAssociationMapData());
    emit dataTemplateAssociationMapChanged();
}

datatemplateassociation::controller::DataTemplateAssociationMap*
ApplicationFeatureHandler::dataTemplateAssociationMap() const
{
    return _dataDocumentAssociationMap.get();
}

document::controller::DocumentHandler* ApplicationFeatureHandler::documentHandler() const
{
    return _documentHandler.get();
}

documentdatadefinitions::controller::DataHandler* ApplicationFeatureHandler::dataHandler() const
{
    return _dataHandler.get();
}

documenttemplate::controller::TemplatesHandler* ApplicationFeatureHandler::templatesHandler() const
{
    return _templatesHandler.get();
}

}    // namespace application
