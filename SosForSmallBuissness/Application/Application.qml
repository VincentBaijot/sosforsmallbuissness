import QtQuick.Controls
import QtQuick

import SosQml
import SosForSmallBuissness.Application

ApplicationWindow {
    id: root
    width: 1024
    height: 768
    visible: true
    title: qsTr("Sos for small buissness")

    required property ApplicationFeatureHandler applicationFeatureHandler

    menuBar: ApplicationMenuBar {
        onQuitClicked: {
            root.close()
        }
    }

    header: ApplicationHeaderToolBar {
        id: applicationHeaderToolBar

        height: 40

        separatorPosition: menuContainer.openWidth

        burgerMenuChecked: menuContainer.state === menuContainer.openState

        onBurgerMenuCheckedChanged: {
            if (applicationHeaderToolBar.burgerMenuChecked) {
                menuContainer.state = menuContainer.openState
            } else {
                menuContainer.state = menuContainer.closeState
            }
        }
    }

    Item {
        id: rootContainer

        width: root.contentItem.width
        height: root.contentItem.height

        ApplicationBurgerMenu {
            id: menuContainer
            anchors.top: rootContainer.top
            anchors.bottom: rootContainer.bottom
            anchors.left: rootContainer.left

            openWidth: 200
            applicationFeatureHandler: root.applicationFeatureHandler
        }

        ApplicationFeaturesContainer {
            id: appContainer
            anchors.top: rootContainer.top
            anchors.bottom: rootContainer.bottom
            anchors.right: rootContainer.right
            anchors.left: menuContainer.right

            selectedFeature: root.applicationFeatureHandler.selectedFeature
            selectedTool: applicationHeaderToolBar.selectedTool

            applicationFeatureHandler: root.applicationFeatureHandler
        }
    }
}
