import QtQuick
import QtQml

import SosForSmallBuissness.Application
import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosQml
import SosQml.Data

Item {
    id: root

    required property ApplicationFeatureHandler applicationFeatureHandler

    required property int selectedFeature
    required property int selectedTool

    Loader {
        id: featureLoader
        anchors.fill: root
        sourceComponent: {
            if (root.selectedFeature === ApplicationFeatureHandler.Loading) {
                return loading
            } else if (root.selectedFeature === ApplicationFeatureHandler.Billing) {
                return createBilling
            } else if (root.selectedFeature === ApplicationFeatureHandler.EditTemplates) {
                return editTemplate
            }
        }
    }

    Component {
        id: loading
        Rectangle {
            color: "green"
        }
    }

    Component {
        id: createBilling
        CreateBilling {
            documentHandler: root.applicationFeatureHandler.documentHandler
            selectedTool: root.selectedTool
            userDataHandler: root.applicationFeatureHandler.dataHandler.userDataHandler
        }
    }

    Component {
        id: editTemplate

        EditTemplate {
            templatesHandler: root.applicationFeatureHandler.templatesHandler
            selectedTool: root.selectedTool
            dataDefinitionHandler: root.applicationFeatureHandler.dataHandler.dataDefinitionsHandler
            dataTemplateAssociationMap: root.applicationFeatureHandler.dataTemplateAssociationMap
        }
    }
}
