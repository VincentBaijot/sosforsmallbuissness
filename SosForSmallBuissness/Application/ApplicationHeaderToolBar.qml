import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import SosQml.Data
import SosQml.Controls

ToolBar {
    id: root

    property alias burgerMenuChecked: burgerMenuButton.checked
    required property int separatorPosition
    property int selectedTool: Data.SelectionTool

    Item {
        id: toolBarContainer

        anchors.fill: parent

        CustomToolButton {
            id: burgerMenuButton

            anchors {
                top: toolBarContainer.top
                bottom: toolBarContainer.bottom
            }

            width: height

            checkable: true

            icon.source: Qt.resolvedUrl("resources/BurgerMenuIcon.svg")
            icon.width: toolBarContainer.height
            icon.height: toolBarContainer.height
        }

        ToolSeparator {
            id: toolSeparator
            x: root.separatorPosition - root.leftPadding

            leftPadding: 0
            anchors {
                top: toolBarContainer.top
                bottom: toolBarContainer.bottom
            }
        }

        RowLayout {

            id: toolBarRowLayout

            anchors {
                left: toolSeparator.right
                right: toolBarContainer.right
                top: toolBarContainer.top
                bottom: toolBarContainer.bottom
            }

            CustomToolButton {
                id: cursorMenuButton

                Layout.fillHeight: true
                Layout.preferredWidth: toolBarRowLayout.height

                checkable: true
                checked: root.selectedTool === Data.SelectionTool

                icon.source: Qt.resolvedUrl("resources/CursorIcon.svg")
                icon.width: toolBarRowLayout.height
                icon.height: toolBarRowLayout.height

                onClicked: {
                    root.selectedTool = Data.SelectionTool
                }
            }

            CustomToolButton {
                id: moveMenuButton

                Layout.fillHeight: true
                Layout.preferredWidth: toolBarRowLayout.height

                checkable: true
                checked: root.selectedTool === Data.MoveTool

                icon.source: Qt.resolvedUrl("resources/MoveCursorIcon.svg")
                icon.width: toolBarRowLayout.height
                icon.height: toolBarRowLayout.height

                onClicked: {
                    root.selectedTool = Data.MoveTool
                }
            }

            CustomToolButton {
                id: rectangleMenuButton

                Layout.fillHeight: true
                Layout.preferredWidth: toolBarRowLayout.height

                checkable: true
                checked: root.selectedTool === Data.RectangleTool

                icon.source: Qt.resolvedUrl("resources/RectangleIcon.svg")
                icon.width: toolBarRowLayout.height
                icon.height: toolBarRowLayout.height

                onClicked: {
                    root.selectedTool = Data.RectangleTool
                }
            }

            CustomToolButton {
                id: textMenuButton

                Layout.fillHeight: true
                Layout.preferredWidth: toolBarRowLayout.height

                checkable: true
                checked: root.selectedTool === Data.TextTool

                icon.source: Qt.resolvedUrl("resources/TextIcon.svg")
                icon.width: toolBarRowLayout.height
                icon.height: toolBarRowLayout.height

                onClicked: {
                    root.selectedTool = Data.TextTool
                }
            }

            CustomToolButton {
                id: listMenuButton

                Layout.fillHeight: true
                Layout.preferredWidth: toolBarRowLayout.height

                checkable: true
                checked: root.selectedTool === Data.ListTool

                icon.source: Qt.resolvedUrl("resources/List.svg")
                icon.width: toolBarRowLayout.height
                icon.height: toolBarRowLayout.height

                onClicked: {
                    root.selectedTool = Data.ListTool
                }
            }

            Item {
                id: filler
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }
}
