#pragma once

#include <QPageSize>
#include <QSizeF>
#include <QtQml/QQmlEngine>

#include "DocumentTemplate/data/DocumentTemplateDataStructure_global.hpp"

namespace documenttemplate::data
{
class DOCUMENT_TEMPLATE_DATA_EXPORT PageSizeData
{
  public:
    enum class Unit
    {
        Millimeter = QPageSize::Millimeter,
        First      = Millimeter,
        Point      = QPageSize::Point,
        Inch       = QPageSize::Inch,
        Pica       = QPageSize::Pica,
        Didot      = QPageSize::Didot,
        Cicero     = QPageSize::Cicero,
        Last       = Cicero
    };

    PageSizeData()                                          = default;
    ~PageSizeData()                                         = default;
    PageSizeData(const PageSizeData& pageSizeData) noexcept = default;
    PageSizeData(PageSizeData&& pageSizeData) noexcept      = default;
    PageSizeData& operator=(const PageSizeData&) noexcept   = default;
    PageSizeData& operator=(PageSizeData&&) noexcept        = default;

    PageSizeData(const QPageSize& pageSize, int resolution, Unit unit);

    QPageSize pageSize() const;
    void setPageSize(const QPageSize& newPageSizeId);

    int resolution() const;
    void setResolution(int newResolution);

    QSizeF size() const;
    void setSize(QSizeF newSize);

    QSize sizePixel() const;

    Unit unit() const;
    void setUnit(Unit newUnit);

    qreal unitPerPixel() const;

    bool operator==(const PageSizeData& other) const = default;

  private:
    QPageSize _pageSize { QPageSize::A4 };
    int _resolution { 1200 };
    Unit _unit { Unit::Millimeter };
};

}    // namespace documenttemplate::data

DOCUMENT_TEMPLATE_DATA_EXPORT QDebug
operator<<(QDebug debugOutput, const documenttemplate::data::PageSizeData& pageSize);
