#pragma once

#include <variant>

#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"

namespace documenttemplate::data
{
using VariantInterfaceQmlPaintableItem = std::variant<painter::data::PaintableRectangleData,
                                                      painter::data::PaintableTextData,
                                                      painter::data::PaintableListData>;
}    // namespace documenttemplate::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documenttemplate::data::VariantInterfaceQmlPaintableItem& variantInterfaceQmlPaintableItem)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace();

    return std::visit([debugOutput](const auto& itemData) { return debugOutput << itemData; },
                      variantInterfaceQmlPaintableItem);
}
