#pragma once

#include <QList>

#include "Utils/DataStructureDebugSupport.hpp"
#include "VariantInterfaceQmlPaintableItem.hpp"

namespace documenttemplate::data
{
struct RootPaintableItemListData
{
    QList<VariantInterfaceQmlPaintableItem> paintableList;
    bool operator==(const RootPaintableItemListData& list) const = default;
};
}    // namespace documenttemplate::data

inline QDebug operator<<(QDebug debugOutput,
                         const documenttemplate::data::RootPaintableItemListData& rootPaintableItemListData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "RootPaintableItemListData{QList<VariantInterfaceQmlPaintableItem>";
    utils::qDebugSequentialContainer(debugOutput, rootPaintableItemListData.paintableList);
    debugOutput << '}';
    return debugOutput;
}
