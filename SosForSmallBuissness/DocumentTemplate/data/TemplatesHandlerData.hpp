#pragma once

#include "DocumentTemplateData.hpp"
#include "VariantInterfaceQmlPaintableItem.hpp"

namespace documenttemplate::data
{
struct TemplatesHandlerData
{
    QList<DocumentTemplateData> documentTemplates;
    bool operator==(const TemplatesHandlerData& templatesHandlerData) const = default;
};
}    // namespace documenttemplate::data

inline QDebug operator<<(QDebug debugOutput,
                         const documenttemplate::data::TemplatesHandlerData& templatesHandlerData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "TemplatesHandlerData{QList<DocumentTemplateData>";
    utils::qDebugSequentialContainer(debugOutput, templatesHandlerData.documentTemplates);
    debugOutput << '}';
    return debugOutput;
}
