#pragma once

#include "PageSizeData.hpp"
#include "RootPaintableItemListData.hpp"

namespace documenttemplate::data
{
struct DocumentTemplateData
{
    PageSizeData pageSizeData;
    RootPaintableItemListData rootPaintableItemListData;
    bool operator==(const DocumentTemplateData& documentTemplateData) const = default;
};
}    // namespace documenttemplate::data

inline QDebug operator<<(QDebug debugOutput,
                         const documenttemplate::data::DocumentTemplateData& documentTemplateData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "DocumentTemplateData{" << documentTemplateData.pageSizeData << ','
                                 << documentTemplateData.rootPaintableItemListData << '}';
}
