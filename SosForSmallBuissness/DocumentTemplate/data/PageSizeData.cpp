#include "PageSizeData.hpp"

namespace documenttemplate::data
{
PageSizeData::PageSizeData(const QPageSize& pageSize, int resolution, Unit unit)
    : _pageSize { pageSize }, _resolution { resolution }, _unit { unit }
{
}

QPageSize PageSizeData::pageSize() const
{
    return _pageSize;
}

void PageSizeData::setPageSize(const QPageSize& newPageSize)
{
    if (pageSize() == newPageSize) { return; }
    _pageSize = newPageSize;
}

int PageSizeData::resolution() const
{
    return _resolution;
}

void PageSizeData::setResolution(int newResolution)
{
    if (_resolution == newResolution) { return; }
    _resolution = newResolution;
}

QSizeF PageSizeData::size() const
{
    return _pageSize.size(QPageSize::Unit(static_cast<int>(_unit)));
}

void PageSizeData::setSize(QSizeF newSize)
{
    if (size() == newSize) { return; }

    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_unit));
    _pageSize          = QPageSize(newSize, qPageSizeUnit, QString(), QPageSize::ExactMatch);
}

QSize PageSizeData::sizePixel() const
{
    return _pageSize.sizePixels(_resolution);
}

PageSizeData::Unit PageSizeData::unit() const
{
    return _unit;
}

void PageSizeData::setUnit(PageSizeData::Unit newUnit)
{
    if (_unit == newUnit) { return; }
    _unit = newUnit;

    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_unit));
    _pageSize          = QPageSize(_pageSize.size(qPageSizeUnit), qPageSizeUnit, QString(), QPageSize::ExactMatch);
}

qreal PageSizeData::unitPerPixel() const
{
    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_unit));
    auto pageSize      = QPageSize(QSize(1, 1), qPageSizeUnit, QString(), QPageSize::ExactMatch);
    // Give the same result than height : its only linked to the resolution and unit
    return 1.0 / pageSize.sizePixels(_resolution).width();
}

}    // namespace documenttemplate::data

QDebug operator<<(QDebug debugOutput, const documenttemplate::data::PageSizeData& pageSize)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace();
    debugOutput << "PageSizeData{";

    QSizeF pageQSize = pageSize.pageSize().size(pageSize.pageSize().definitionUnits());

    debugOutput << "QPageSize{";

    debugOutput << "QSizeF{" << pageQSize.width() << ',' << pageQSize.height() << '}';

    debugOutput << ",QPageSize::Unit(" << pageSize.pageSize().definitionUnits() << "),";
    debugOutput << pageSize.pageSize().name();
    debugOutput << ",QPageSize::ExactMatch}";

    debugOutput << ',' << pageSize.resolution() << ',' << "PageSizeData::Unit("
                << static_cast<int>(pageSize.unit()) << ")}";

    return debugOutput;
}
