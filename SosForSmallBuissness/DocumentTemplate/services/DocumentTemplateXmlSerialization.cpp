#include "DocumentTemplateXmlSerialization.hpp"

#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "DocumentTemplate/data/PageSizeData.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "DocumentTemplateXmlConstants.hpp"
#include "DocumentTemplateXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace documenttemplate::service
{

std::optional<documenttemplate::data::PageSizeData> readPageSizeData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, pageSizeElement, u"PageSizeData")) { return std::nullopt; }

    return details::readPageSizeData(reader);
}

void writePageSizeData(QXmlStreamWriter& writer, const documenttemplate::data::PageSizeData& pageSizeData)
{
    details::writePageSizeData(writer, pageSizeData);
}

std::optional<documenttemplate::data::DocumentTemplateData> readDocumentTemplateData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, documentTemplateElement, u"DocumentTemplateData"))
    {
        return std::nullopt;
    }

    return details::readDocumentTemplateData(reader);
}

void writeDocumentTemplateData(QXmlStreamWriter& writer,
                               const documenttemplate::data::DocumentTemplateData& documentTemplateData)
{
    details::writeDocumentTemplateData(writer, documentTemplateData);
}

std::optional<documenttemplate::data::TemplatesHandlerData> readTemplatesHandlerData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, templatesHandlerElement, u"TemplatesHandlerData"))
    {
        return std::nullopt;
    }

    return details::readTemplatesHandlerData(reader);
}

void writeTemplatesHandlerData(QXmlStreamWriter& writer,
                               const documenttemplate::data::TemplatesHandlerData& templatesHandlerData)
{
    details::writeTemplatesHandlerData(writer, templatesHandlerData);
}

}    // namespace documenttemplate::service
