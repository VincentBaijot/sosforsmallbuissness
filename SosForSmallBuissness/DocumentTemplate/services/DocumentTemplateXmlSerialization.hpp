#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "DocumentTemplateService_global.hpp"

namespace documenttemplate::data
{
class PageSizeData;
struct DocumentTemplateData;
struct TemplatesHandlerData;
}    // namespace documenttemplate::data

namespace documenttemplate::service
{
DOCUMENT_TEMPLATE_SERVICES_EXPORT std::optional<documenttemplate::data::PageSizeData> readPageSizeData(
  QXmlStreamReader& reader);
DOCUMENT_TEMPLATE_SERVICES_EXPORT void writePageSizeData(QXmlStreamWriter& writer,
                                                         const documenttemplate::data::PageSizeData& pageSizeData);

DOCUMENT_TEMPLATE_SERVICES_EXPORT std::optional<documenttemplate::data::DocumentTemplateData>
readDocumentTemplateData(QXmlStreamReader& reader);
DOCUMENT_TEMPLATE_SERVICES_EXPORT void writeDocumentTemplateData(
  QXmlStreamWriter& writer,
  const documenttemplate::data::DocumentTemplateData& documentTemplateData);

DOCUMENT_TEMPLATE_SERVICES_EXPORT std::optional<documenttemplate::data::TemplatesHandlerData>
readTemplatesHandlerData(QXmlStreamReader& reader);
DOCUMENT_TEMPLATE_SERVICES_EXPORT void writeTemplatesHandlerData(
  QXmlStreamWriter& writer,
  const documenttemplate::data::TemplatesHandlerData& templatesHandlerData);
}    // namespace documenttemplate::service
