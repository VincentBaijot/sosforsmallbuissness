#include "DocumentTemplateXmlSerializationDetails.hpp"

#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "DocumentTemplate/data/PageSizeData.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "DocumentTemplateXmlConstants.hpp"
#include "Painter/services/PainterXmlConstants.hpp"
#include "Painter/services/PainterXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace documenttemplate::service::details
{
inline constexpr const QStringView qPageSizeElement            = u"QPageSize";
inline constexpr const QStringView qSizeFElement               = u"QSizeF";
inline constexpr const QStringView qSizeFWidthElement          = u"width";
inline constexpr const QStringView qSizeFHeightElement         = u"height";
inline constexpr const QStringView qPageSizeUnitElement        = u"definitionUnit";
inline constexpr const QStringView resolutionElement           = u"resolution";
inline constexpr const QStringView unitElement                 = u"unit";
inline constexpr const QStringView rootPaintableItemLisElement = u"RootPaintableItemList";

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

QSizeF readQSizeF(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == qSizeFElement);

    reader.readNextStartElement();

    QSizeF qSizeF;

    while (!utils::xml::checkEndElement(reader, qSizeFElement))
    {
        if (reader.isStartElement() && reader.name() == qSizeFWidthElement)
        {
            qSizeF.setWidth(reader.readElementText().toDouble());
        }
        else if (reader.isStartElement() && reader.name() == qSizeFHeightElement)
        {
            qSizeF.setHeight(reader.readElementText().toDouble());
        }
        else { reader.readNext(); }
    }

    return qSizeF;
}

void writeQSizeF(QXmlStreamWriter& writer, const QSizeF& pageSizeF)
{
    writer.writeStartElement(qSizeFElement.toString());

    writer.writeStartElement(qSizeFWidthElement.toString());
    writer.writeCharacters(QString::number(pageSizeF.width()));
    // end qSizeFWidthElement
    writer.writeEndElement();

    writer.writeStartElement(qSizeFHeightElement.toString());
    writer.writeCharacters(QString::number(pageSizeF.height()));
    // end qSizeFHeightElement
    writer.writeEndElement();

    // end qSizeFElement
    writer.writeEndElement();
}

QPageSize readQPageSize(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == qPageSizeElement);

    reader.readNextStartElement();

    QSizeF pageSizeF;
    QPageSize::Unit pageSizeUnit = QPageSize::Unit::Millimeter;

    while (!utils::xml::checkEndElement(reader, qPageSizeElement))
    {
        if (reader.isStartElement() && reader.name() == qSizeFElement) { pageSizeF = readQSizeF(reader); }
        else if (reader.isStartElement() && reader.name() == qPageSizeUnitElement)
        {
            pageSizeUnit = QPageSize::Unit(reader.readElementText().toInt());
        }
        else { reader.readNext(); }
    }

    if (pageSizeF.isValid())
    {
        return QPageSize(pageSizeF, pageSizeUnit, QString(), QPageSize::SizeMatchPolicy::ExactMatch);
    }
    else { return QPageSize(); }
}

void writeQPageSize(QXmlStreamWriter& writer, const QPageSize& qPageSize)
{
    writer.writeStartElement(qPageSizeElement.toString());

    writeQSizeF(writer, qPageSize.definitionSize());

    writer.writeStartElement(qPageSizeUnitElement.toString());
    writer.writeCharacters(QString::number(static_cast<int>(qPageSize.definitionUnits())));
    writer.writeEndElement();

    // end qPageSizeElement
    writer.writeEndElement();
}

documenttemplate::data::PageSizeData readPageSizeData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == pageSizeElement);

    reader.readNextStartElement();

    documenttemplate::data::PageSizeData pageSizeData;

    while (!utils::xml::checkEndElement(reader, pageSizeElement))
    {
        if (reader.isStartElement() && reader.name() == qPageSizeElement)
        {
            QPageSize pageSize = details::readQPageSize(reader);
            if (pageSize.isValid()) { pageSizeData.setPageSize(pageSize); }
        }
        else if (reader.isStartElement() && reader.name() == resolutionElement)
        {
            bool convertionSucceed = false;
            int resolution         = reader.readElementText().toInt(&convertionSucceed);
            if (convertionSucceed) { pageSizeData.setResolution(resolution); }
        }
        else if (reader.isStartElement() && reader.name() == unitElement)
        {
            pageSizeData.setUnit(documenttemplate::data::PageSizeData::Unit(reader.readElementText().toInt()));
        }
        else { reader.readNext(); }
    }

    return pageSizeData;
}

void writePageSizeData(QXmlStreamWriter& writer, const documenttemplate::data::PageSizeData& pageSizeData)
{
    writer.writeStartElement(pageSizeElement.toString());

    details::writeQPageSize(writer, pageSizeData.pageSize());

    writer.writeStartElement(resolutionElement.toString());
    writer.writeCharacters(QString::number(pageSizeData.resolution()));
    writer.writeEndElement();

    writer.writeStartElement(unitElement.toString());
    writer.writeCharacters(QString::number(static_cast<int>(pageSizeData.unit())));
    writer.writeEndElement();

    // end pageSizeElement
    writer.writeEndElement();
}

documenttemplate::data::RootPaintableItemListData readRootPaintableItemListData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == rootPaintableItemLisElement);

    reader.readNextStartElement();

    documenttemplate::data::RootPaintableItemListData rootPaintableItemListData;

    while (!utils::xml::checkEndElement(reader, rootPaintableItemLisElement))
    {
        if (reader.isStartElement() && reader.name() == painter::service::paintableRectangleElement)
        {
            rootPaintableItemListData.paintableList.append(
              painter::service::details::readPaintableRectangleData(reader));
        }
        else if (reader.isStartElement() && reader.name() == painter::service::paintableTextElement)
        {
            rootPaintableItemListData.paintableList.append(
              painter::service::details::readPaintableTextData(reader));
        }
        else if (reader.isStartElement() && reader.name() == painter::service::paintableListElement)
        {
            rootPaintableItemListData.paintableList.append(
              painter::service::details::readPaintableListData(reader));
        }
        else { reader.readNext(); }
    }

    return rootPaintableItemListData;
}

void writeRootPaintableItemListData(
  QXmlStreamWriter& writer,
  const documenttemplate::data::RootPaintableItemListData& rootPaintableItemListData)
{
    writer.writeStartElement(rootPaintableItemLisElement.toString());

    for (const documenttemplate::data::VariantInterfaceQmlPaintableItem& variantInterfaceQmlPaintableItem :
         rootPaintableItemListData.paintableList)
    {
        std::visit(
          overloaded { [&writer](const painter::data::PaintableRectangleData& paintableRectangleData)
                       { painter::service::details::writePaintableRectangleData(writer, paintableRectangleData); },
                       [&writer](const painter::data::PaintableTextData& paintableTextData)
                       { painter::service::details::writePaintableTextData(writer, paintableTextData); },
                       [&writer](const painter::data::PaintableListData& paintableListData)
                       { painter::service::details::writePaintableListData(writer, paintableListData); } },
          variantInterfaceQmlPaintableItem);
    }

    // end rootPaintableItemLisElement
    writer.writeEndElement();
}

documenttemplate::data::DocumentTemplateData readDocumentTemplateData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == documentTemplateElement);

    reader.readNextStartElement();

    documenttemplate::data::DocumentTemplateData documentTemplateData;

    while (!utils::xml::checkEndElement(reader, documentTemplateElement))
    {
        if (reader.isStartElement() && reader.name() == pageSizeElement)
        {
            documentTemplateData.pageSizeData = details::readPageSizeData(reader);
        }
        else if (reader.isStartElement() && reader.name() == rootPaintableItemLisElement)
        {
            documentTemplateData.rootPaintableItemListData = details::readRootPaintableItemListData(reader);
        }
        else { reader.readNext(); }
    }

    return documentTemplateData;
}

void writeDocumentTemplateData(QXmlStreamWriter& writer,
                               const documenttemplate::data::DocumentTemplateData& documentTemplateData)
{
    writer.writeStartElement(documentTemplateElement.toString());

    writePageSizeData(writer, documentTemplateData.pageSizeData);
    writeRootPaintableItemListData(writer, documentTemplateData.rootPaintableItemListData);

    // end documentTemplateElement
    writer.writeEndElement();
}

documenttemplate::data::TemplatesHandlerData readTemplatesHandlerData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == templatesHandlerElement);

    reader.readNextStartElement();

    documenttemplate::data::TemplatesHandlerData templatesHandlerData;

    while (!utils::xml::checkEndElement(reader, templatesHandlerElement))
    {
        if (reader.isStartElement() && reader.name() == documentTemplateElement)
        {
            templatesHandlerData.documentTemplates.append(details::readDocumentTemplateData(reader));
        }
        else { reader.readNext(); }
    }

    return templatesHandlerData;
}

void writeTemplatesHandlerData(QXmlStreamWriter& writer,
                               const documenttemplate::data::TemplatesHandlerData& templatesHandlerData)
{
    writer.writeStartElement(templatesHandlerElement.toString());

    for (const documenttemplate::data::DocumentTemplateData& documentTemplate :
         templatesHandlerData.documentTemplates)
    {
        details::writeDocumentTemplateData(writer, documentTemplate);
    }

    // end templatesHandlerElement
    writer.writeEndElement();
}
}    // namespace documenttemplate::service::details
