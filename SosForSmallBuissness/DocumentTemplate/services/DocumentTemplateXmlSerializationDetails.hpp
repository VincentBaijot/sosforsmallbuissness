#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "DocumentTemplateService_global.hpp"

namespace documenttemplate::data
{
class PageSizeData;
struct DocumentTemplateData;
struct TemplatesHandlerData;
}    // namespace documenttemplate::data

namespace documenttemplate::service::details
{
DOCUMENT_TEMPLATE_SERVICES_EXPORT documenttemplate::data::PageSizeData readPageSizeData(QXmlStreamReader& reader);
DOCUMENT_TEMPLATE_SERVICES_EXPORT void writePageSizeData(QXmlStreamWriter& writer,
                                                         const documenttemplate::data::PageSizeData& pageSizeData);

documenttemplate::data::DocumentTemplateData readDocumentTemplateData(QXmlStreamReader& reader);
void writeDocumentTemplateData(QXmlStreamWriter& writer,
                               const documenttemplate::data::DocumentTemplateData& documentTemplateData);

documenttemplate::data::TemplatesHandlerData readTemplatesHandlerData(QXmlStreamReader& reader);
void writeTemplatesHandlerData(QXmlStreamWriter& writer,
                               const documenttemplate::data::TemplatesHandlerData& templatesHandlerData);
}    // namespace documenttemplate::service::details
