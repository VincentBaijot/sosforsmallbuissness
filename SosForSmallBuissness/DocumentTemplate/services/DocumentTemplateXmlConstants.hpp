#pragma once

#include <QStringView>

namespace documenttemplate::service
{
inline constexpr const QStringView pageSizeElement         = u"PageSize";
inline constexpr const QStringView documentTemplateElement = u"DocumentTemplate";
inline constexpr const QStringView templatesHandlerElement = u"TemplatesHandler";
}    // namespace documenttemplate::service
