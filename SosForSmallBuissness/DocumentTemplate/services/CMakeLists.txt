cmake_minimum_required(VERSION 3.22)

print_location()

set(HEADERS
    DocumentTemplateService_global.hpp
    Constants.hpp
    DocumentTemplateXmlSerialization.hpp
    DocumentTemplateXmlConstants.hpp
    DocumentTemplateXmlSerializationDetails.hpp
)

set(SOURCES
    Constants.cpp
    DocumentTemplateXmlSerialization.cpp
    DocumentTemplateXmlSerializationDetails.cpp
)

# Short compilation library name to avoid windows long path issues
set(LIBRARY_NAME DTS)

qt_add_library(${LIBRARY_NAME} SHARED ${SOURCES} ${HEADERS})
# Library explicit name
add_library(DocumentTemplate::services ALIAS ${LIBRARY_NAME})

set_target_properties(${LIBRARY_NAME} PROPERTIES OUTPUT_NAME "SosDocumentTemplateService")

target_include_directories(${LIBRARY_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(${LIBRARY_NAME}
                           PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)

target_link_libraries(${LIBRARY_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Qml)
target_link_libraries(${LIBRARY_NAME} PUBLIC DocumentTemplate::data)
target_link_libraries(${LIBRARY_NAME} PRIVATE Painter::services)

target_compile_definitions(${LIBRARY_NAME} PRIVATE DOCUMENT_TEMPLATE_SERVICES_LIBRARY)

install(TARGETS ${LIBRARY_NAME} BUNDLE DESTINATION .)
