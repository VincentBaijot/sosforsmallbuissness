#include "PaintableItemDataHandler.hpp"

#include <QDebug>

#include "Constants.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "DocumentTemplate/data/VariantInterfaceQmlPaintableItem.hpp"
#include "Painter/controllers/PaintableList.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

namespace documenttemplate::controller
{
documenttemplate::data::VariantInterfaceQmlPaintableItem extractData(
  const painter::controller::InterfaceQmlPaintableItem* const item)
{
    if (item)
    {
        if (const painter::controller::PaintableRectangle* const rectangle =
              qobject_cast<const painter::controller::PaintableRectangle* const>(item))
        {
            return rectangle->objectData();
        }
        else if (const painter::controller::PaintableText* const text =
                   qobject_cast<const painter::controller::PaintableText* const>(item))
        {
            return text->objectData();
        }
        else if (const painter::controller::PaintableList* const list =
                   qobject_cast<const painter::controller::PaintableList* const>(item))
        {
            return list->objectData();
        }
        else
        {
            qCWarning(documentTemplateControllerCategory)
              << "Try to extract data of unknown InterfaceQmlPaintableItem type " << item->objectName();
            return documenttemplate::data::VariantInterfaceQmlPaintableItem();
        }
    }

    qCWarning(documentTemplateControllerCategory) << "Try to extract data of null item";

    return documenttemplate::data::VariantInterfaceQmlPaintableItem();
}

utils::own_qobject<painter::controller::InterfaceQmlPaintableItem> generateItem(
  documenttemplate::data::VariantInterfaceQmlPaintableItem itemDataVariant,
  QObject& parent)
{
    return std::visit(
      overloaded { [&parent](const painter::data::PaintableRectangleData& paintableRectangleData)
                   {
                       return utils::own_qobject<painter::controller::InterfaceQmlPaintableItem>(
                         utils::make_qobject<painter::controller::PaintableRectangle>(parent, paintableRectangleData));
                   },
                   [&parent](const painter::data::PaintableTextData& paintableTextData)
                   {
                       return utils::own_qobject<painter::controller::InterfaceQmlPaintableItem>(
                         utils::make_qobject<painter::controller::PaintableText>(parent, paintableTextData));
                   },
                   [&parent](const painter::data::PaintableListData& paintableListData)
                   {
                       return utils::own_qobject<painter::controller::InterfaceQmlPaintableItem>(
                         utils::make_qobject<painter::controller::PaintableList>(parent, paintableListData));
                   } },
      itemDataVariant);
}

utils::OwnQObjectList<painter::controller::InterfaceQmlPaintableItem> listFromData(
  const documenttemplate::data::RootPaintableItemListData& data,
  QObject& parent)
{
    utils::OwnQObjectList<painter::controller::InterfaceQmlPaintableItem> paintableItemList;
    for (const documenttemplate::data::VariantInterfaceQmlPaintableItem& variantData : data.paintableList)
    {
        utils::own_qobject<painter::controller::InterfaceQmlPaintableItem> abstractListableItem =
          generateItem(variantData, parent);
        if (abstractListableItem) { paintableItemList.emplace_back(std::move(abstractListableItem)); }
    }

    return paintableItemList;
}

}    // namespace documenttemplate::controller
