#pragma once

#include <QRectF>
#include <QtQml/QQmlEngine>

#include "DocumentTemplateController_global.hpp"
#include "Painter/controllers/PaintableItemListModel.hpp"

namespace painter::controller
{
class InterfaceQmlPaintableItem;
}

namespace documenttemplate::data
{
struct RootPaintableItemListData;
}

namespace documenttemplate::controller
{

class DOCUMENT_TEMPLATE_CONTROLLER_TESTING_EXPORT RootPaintableItemList
    : public painter::controller::PaintableItemListModel
{
    Q_OBJECT

    QML_ELEMENT
  public:
    /*!
     * @brief Definition of an enum to give access in the qml to the role
     */
    enum class PaintableItemRoles
    {
        PaintableItemRole = Qt::DisplayRole
    };
    Q_ENUM(PaintableItemRoles)

    explicit RootPaintableItemList(QObject* parent = nullptr);
    explicit RootPaintableItemList(const documenttemplate::data::RootPaintableItemListData& data,
                                   QObject* parent = nullptr);
    ~RootPaintableItemList() override = default;

    documenttemplate::data::RootPaintableItemListData objectData() const;
    void setObjectData(const documenttemplate::data::RootPaintableItemListData& newData);

    Q_INVOKABLE painter::controller::InterfaceQmlPaintableItem* addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType paintableItemType,
      const QRectF& rect) override;
    Q_INVOKABLE void addPaintableItem(painter::controller::InterfaceQmlPaintableItem* paintableItem);

    Q_INVOKABLE void testPdf();

  private:
    Q_DISABLE_COPY_MOVE(RootPaintableItemList)
};
}    // namespace documenttemplate::controller
