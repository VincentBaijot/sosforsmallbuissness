#include "DocumentTemplate.hpp"

#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "PageSize.hpp"
#include "RootPaintableItemList.hpp"

namespace documenttemplate::controller
{
DocumentTemplate::DocumentTemplate(QObject* parent)
    : QObject { parent },
      _pageSize { utils::make_qobject<PageSize>(*this) },
      _paintableItemList { utils::make_qobject<RootPaintableItemList>(*this) }
{
}

DocumentTemplate::DocumentTemplate(const documenttemplate::data::DocumentTemplateData& data, QObject* parent)
    : QObject { parent },
      _pageSize { utils::make_qobject<PageSize>(*this, data.pageSizeData) },
      _paintableItemList { utils::make_qobject<RootPaintableItemList>(*this, data.rootPaintableItemListData) }
{
}

DocumentTemplate::~DocumentTemplate() = default;

RootPaintableItemList* DocumentTemplate::paintableItemList()
{
    return _paintableItemList.get();
}

PageSize* DocumentTemplate::pageSize() const
{
    return _pageSize.get();
}

documenttemplate::data::DocumentTemplateData DocumentTemplate::objectData() const
{
    documenttemplate::data::DocumentTemplateData documentTemplateData;
    documentTemplateData.pageSizeData              = _pageSize->objectData();
    documentTemplateData.rootPaintableItemListData = _paintableItemList->objectData();
    return documentTemplateData;
}

void DocumentTemplate::setObjectData(const documenttemplate::data::DocumentTemplateData& newData)
{
    _pageSize->setObjectData(newData.pageSizeData);
    _paintableItemList->setObjectData(newData.rootPaintableItemListData);
}

}    // namespace documenttemplate::controller
