#pragma once

#include <QtQml/QQmlEngine>

#include "DocumentTemplate.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "DocumentTemplateController_global.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

namespace documenttemplate::controller
{
inline constexpr char templatesHandlerHeaderData[] = "Document template";

class DOCUMENT_TEMPLATE_CONTROLLER_EXPORT TemplatesHandler
    : public utils::SimpleDataObjectListModel<DocumentTemplate,
                                              documenttemplate::data::DocumentTemplateData,
                                              templatesHandlerHeaderData>
{
    Q_OBJECT

    QML_ELEMENT
  public:
    explicit TemplatesHandler(QObject* parent = nullptr);
    explicit TemplatesHandler(const documenttemplate::data::TemplatesHandlerData& data, QObject* parent = nullptr);
    ~TemplatesHandler() override = default;

    void addDocumentTemplate();
    void removeDocumentTemplate(int rowIndex);

    documenttemplate::data::TemplatesHandlerData objectData() const;
    void setObjectData(const documenttemplate::data::TemplatesHandlerData& newData);

    // Create an alias on display to avoid issues with the display property in TabButton
    enum class TemplatesHandlerRole
    {
        DocumentTemplateRole = Qt::UserRole + 1
    };
    Q_ENUM(TemplatesHandlerRole)
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

  private:
    Q_DISABLE_COPY_MOVE(TemplatesHandler)
};
}    // namespace documenttemplate::controller
