#include "RootPaintableItemList.hpp"

#include <QPainter>
#include <QPdfWriter>

#include "Constants.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "PaintableItemDataHandler.hpp"
#include "Painter/controllers/DefaultPaintableItemsFactory.hpp"
#include "Painter/controllers/InterfaceQmlPaintableItem.hpp"

namespace documenttemplate::controller
{
RootPaintableItemList::RootPaintableItemList(QObject* parent)
    : painter::controller::PaintableItemListModel { parent }
{
}

RootPaintableItemList::RootPaintableItemList(const documenttemplate::data::RootPaintableItemListData& data,
                                             QObject* parent)
    : painter::controller::PaintableItemListModel { parent }
{
    _setPaintableItemList(listFromData(data, *this));
}

documenttemplate::data::RootPaintableItemListData RootPaintableItemList::objectData() const
{
    documenttemplate::data::RootPaintableItemListData rootPaintableItemListData;

    for (int rowIndex = 0; rowIndex < rowCount(); ++rowIndex)
    {
        const painter::controller::InterfaceQmlPaintableItem* const item         = at(rowIndex);
        documenttemplate::data::VariantInterfaceQmlPaintableItem variantItemData = extractData(item);
        rootPaintableItemListData.paintableList.append(variantItemData);
    }

    return rootPaintableItemListData;
}

void RootPaintableItemList::setObjectData(const documenttemplate::data::RootPaintableItemListData& newData)
{
    if (objectData() != newData) { _setPaintableItemList(listFromData(newData, *this)); }
}

painter::controller::InterfaceQmlPaintableItem* RootPaintableItemList::addPaintableItem(
  painter::controller::InterfaceQmlPaintableItem::ItemType paintableItemType,
  const QRectF& rect)
{
    if (rect.width() == 0 && rect.height() == 0) { return nullptr; }

    painter::controller::InterfaceQmlPaintableItem* positionableItem =
      painter::controller::DefaultPaintableItemsFactory::paintableQmlItem(
        paintableItemType, painter::data::PositionData { rect.x(), rect.y(), rect.width(), rect.height() }, *this)
        .get();

    addPaintableItem(positionableItem);

    qCDebug(documentTemplateControllerCategory)
      << "Add item " << paintableItemType << " : QRectF(" << rect.x() << "," << rect.y() << "," << rect.width()
      << "," << rect.height() << ")";

    return positionableItem;
}

void RootPaintableItemList::addPaintableItem(painter::controller::InterfaceQmlPaintableItem* paintableItem)
{
    _addPaintableItem(utils::own_qobject<painter::controller::InterfaceQmlPaintableItem>(paintableItem));
}

void RootPaintableItemList::testPdf()
{
    QPdfWriter pdfWriter(QStringLiteral("test.pdf"));
    pdfWriter.setPageSize(QPageSize::A4);

    pdfWriter.setPageMargins(QMargins(0, 0, 0, 0));

    QPainter painter(&pdfWriter);

    for (int rowIndex = 0; rowIndex < rowCount(); ++rowIndex)
    {
        const painter::controller::InterfaceQmlPaintableItem* const item = at(rowIndex);
        qCDebug(documentTemplateControllerCategory) << "Paint item : " << item->itemType();
        item->paint(&painter);
    }
}

}    // namespace documenttemplate::controller
