#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DocumentTemplateController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("RootPaintableItemList.hpp")
Q_MOC_INCLUDE("PageSize.hpp")

namespace documenttemplate::data
{
struct DocumentTemplateData;
}

namespace documenttemplate::controller
{
class PageSize;
class RootPaintableItemList;

class DOCUMENT_TEMPLATE_CONTROLLER_EXPORT DocumentTemplate : public QObject
{
    Q_OBJECT

    Q_PROPERTY(PageSize* pageSize READ pageSize CONSTANT)
    Q_PROPERTY(
      documenttemplate::controller::RootPaintableItemList* paintableItemList READ paintableItemList CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit DocumentTemplate(QObject* parent = nullptr);
    explicit DocumentTemplate(const documenttemplate::data::DocumentTemplateData& data, QObject* parent = nullptr);
    ~DocumentTemplate() override;

    [[nodiscard]] RootPaintableItemList* paintableItemList();
    [[nodiscard]] PageSize* pageSize() const;

    [[nodiscard]] documenttemplate::data::DocumentTemplateData objectData() const;
    void setObjectData(const documenttemplate::data::DocumentTemplateData& newData);

  private:
    Q_DISABLE_COPY_MOVE(DocumentTemplate)

    utils::own_qobject<PageSize> _pageSize;
    utils::own_qobject<RootPaintableItemList> _paintableItemList;
};
}    // namespace documenttemplate::controller
