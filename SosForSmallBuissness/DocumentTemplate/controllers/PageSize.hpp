#pragma once

#include <QObject>
#include <QPageSize>
#include <QSizeF>
#include <QtQml/QQmlEngine>

#include "DocumentTemplate/data/PageSizeData.hpp"
#include "DocumentTemplateController_global.hpp"

namespace documenttemplate::controller
{

class DOCUMENT_TEMPLATE_CONTROLLER_EXPORT PageSize : public QObject
{
    Q_OBJECT

    Q_PROPERTY(PageSizeId pageSizeId READ pageSizeId WRITE setPageSizeId NOTIFY pageSizeIdChanged FINAL)
    Q_PROPERTY(int resolution READ resolution WRITE setResolution NOTIFY resolutionChanged FINAL)
    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged FINAL)
    Q_PROPERTY(QSize sizePixel READ sizePixel NOTIFY sizePixelChanged FINAL)
    Q_PROPERTY(documenttemplate::data::PageSizeData::Unit unit READ unit WRITE setUnit NOTIFY unitChanged FINAL)
    Q_PROPERTY(qreal unitPerPixel READ unitPerPixel NOTIFY unitPerPixelChanged FINAL)

    QML_ELEMENT
  public:
    enum class PageSizeId
    {
        Letter    = QPageSize::Letter,
        First     = Letter,
        Legal     = QPageSize::Legal,
        Executive = QPageSize::Executive,
        A0        = QPageSize::A0,
        A1        = QPageSize::A1,
        A2        = QPageSize::A2,
        A3        = QPageSize::A3,
        A4        = QPageSize::A4,
        A5        = QPageSize::A5,
        A6        = QPageSize::A6,
        A7        = QPageSize::A7,
        A8        = QPageSize::A8,
        A9        = QPageSize::A9,
        A10       = QPageSize::A10,
        B0        = QPageSize::B0,
        B1        = QPageSize::B1,
        B2        = QPageSize::B2,
        B3        = QPageSize::B3,
        B4        = QPageSize::B4,
        B5        = QPageSize::B5,
        B6        = QPageSize::B6,
        B7        = QPageSize::B7,
        B8        = QPageSize::B8,
        B9        = QPageSize::B9,
        B10       = QPageSize::B10,
        C5E       = QPageSize::C5E,
        Comm10E   = QPageSize::Comm10E,
        DLE       = QPageSize::DLE,
        Folio     = QPageSize::Folio,
        Ledger    = QPageSize::Ledger,
        Tabloid   = QPageSize::Tabloid,
        Custom    = QPageSize::Custom,
        Last      = Custom
    };
    Q_ENUM(PageSizeId)

    explicit PageSize(QObject* parent = nullptr);
    explicit PageSize(documenttemplate::data::PageSizeData  pageSize, QObject* parent = nullptr);

    documenttemplate::data::PageSizeData objectData() const;
    void setObjectData(const documenttemplate::data::PageSizeData& newPageSize);

    PageSizeId pageSizeId() const;
    void setPageSizeId(PageSizeId newPageSizeId);

    int resolution() const;
    void setResolution(int newResolution);

    QSizeF size() const;
    void setSize(QSizeF newSize);

    QSize sizePixel() const;

    documenttemplate::data::PageSizeData::Unit unit() const;
    void setUnit(documenttemplate::data::PageSizeData::Unit newUnit);

    qreal unitPerPixel() const;

  signals:
    void pageSizeIdChanged();
    void pageSizeNameChanged();
    void resolutionChanged();
    void sizeChanged();
    void sizePixelChanged();
    void unitChanged();
    void unitPerPixelChanged();

  private:
    Q_DISABLE_COPY_MOVE(PageSize)

    documenttemplate::data::PageSizeData _pageSize;
};

}    // namespace documenttemplate::controller
