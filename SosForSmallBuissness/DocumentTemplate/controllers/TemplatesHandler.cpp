#include "TemplatesHandler.hpp"

#include "Constants.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"

namespace documenttemplate::controller
{
TemplatesHandler::TemplatesHandler(QObject* parent)
    : utils::SimpleDataObjectListModel<DocumentTemplate,
                                       documenttemplate::data::DocumentTemplateData,
                                       templatesHandlerHeaderData> { parent }
{
}

TemplatesHandler::TemplatesHandler(const documenttemplate::data::TemplatesHandlerData& data, QObject* parent)
    : utils::SimpleDataObjectListModel<DocumentTemplate,
                                       documenttemplate::data::DocumentTemplateData,
                                       templatesHandlerHeaderData> { data.documentTemplates, parent }
{
}

void TemplatesHandler::addDocumentTemplate()
{
    qCInfo(documentTemplateControllerCategory) << "Add document template";
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    objects().emplace_back(utils::make_qobject<DocumentTemplate>(*this));
    endInsertRows();
}

void TemplatesHandler::removeDocumentTemplate(int rowIndex)
{
    if (rowIndex >= 0 && rowIndex < rowCount())
    {
        qCInfo(documentTemplateControllerCategory) << "Remove document template " << rowIndex;
        beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
        const utils::own_qobject<DocumentTemplate>& documentTemplate = objects().at(rowIndex);
        documentTemplate->deleteLater();
        objects().erase(objects().begin() + rowIndex);
        endRemoveRows();
    }
}

documenttemplate::data::TemplatesHandlerData TemplatesHandler::objectData() const
{
    documenttemplate::data::TemplatesHandlerData templatesHandlerData;
    templatesHandlerData.documentTemplates = listData();

    return templatesHandlerData;
}

void TemplatesHandler::setObjectData(const documenttemplate::data::TemplatesHandlerData& newData)
{
    if (objectData() == newData) { return; }

    setListData(newData.documentTemplates);
}

QVariant TemplatesHandler::data(const QModelIndex& index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return {}; }

    switch (role)
    {
        case Qt::DisplayRole:
            Q_FALLTHROUGH();
        case static_cast<int>(TemplatesHandlerRole::DocumentTemplateRole):
            return QVariant::fromValue(objects().at(index.row()).get());
        default:
            return {};
    }
}

QHash<int, QByteArray> TemplatesHandler::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles.insert(static_cast<int>(TemplatesHandlerRole::DocumentTemplateRole), "documentTemplate");
    return roles;
}

}    // namespace documenttemplate::controller
