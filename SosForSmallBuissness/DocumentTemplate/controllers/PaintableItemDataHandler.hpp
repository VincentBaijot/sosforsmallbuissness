#pragma once

#include <QList>
#include <QObject>
#include <variant>

#include "Utils/OwnQObjectList.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace painter::data
{
struct PaintableRectangleData;
struct PaintableTextData;
struct PaintableListData;
}    // namespace painter::data

namespace documenttemplate::data
{
using VariantInterfaceQmlPaintableItem = std::variant<painter::data::PaintableRectangleData,
                                                      painter::data::PaintableTextData,
                                                      painter::data::PaintableListData>;
struct RootPaintableItemListData;
}    // namespace documenttemplate::data

namespace painter::controller
{
class InterfaceQmlPaintableItem;
}

namespace documenttemplate::controller
{
documenttemplate::data::VariantInterfaceQmlPaintableItem extractData(
  const painter::controller::InterfaceQmlPaintableItem* const item);
utils::own_qobject<painter::controller::InterfaceQmlPaintableItem> generateItem(
  documenttemplate::data::VariantInterfaceQmlPaintableItem itemDataVariant,
  QObject& parent);
utils::OwnQObjectList<painter::controller::InterfaceQmlPaintableItem> listFromData(
  const documenttemplate::data::RootPaintableItemListData& data,
  QObject& parent);
}    // namespace documenttemplate::controller
