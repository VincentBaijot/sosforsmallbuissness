#include "PageSize.hpp"

#include <utility>

#include "Constants.hpp"

namespace documenttemplate::controller
{
PageSize::PageSize(QObject* parent) : QObject { parent }
{
}

PageSize::PageSize(documenttemplate::data::PageSizeData pageSize, QObject* parent)
    : QObject { parent }, _pageSize { std::move(pageSize) }
{
}

PageSize::PageSizeId PageSize::pageSizeId() const
{
    return PageSize::PageSizeId(static_cast<int>(_pageSize.pageSize().id()));
}

void PageSize::setPageSizeId(PageSize::PageSizeId newPageSizeId)
{
    if (pageSizeId() == newPageSizeId) { return; }

    qCDebug(documentTemplateControllerCategory) << "Set page size id " << newPageSizeId;

    _pageSize.setPageSize(QPageSize(QPageSize::PageSizeId(static_cast<int>(newPageSizeId))));

    emit pageSizeIdChanged();
    emit pageSizeNameChanged();
    emit sizeChanged();
    emit sizePixelChanged();
}

int PageSize::resolution() const
{
    return _pageSize.resolution();
}

void PageSize::setResolution(int newResolution)
{
    if (resolution() == newResolution) { return; }

    qCDebug(documentTemplateControllerCategory) << "Set resolution " << newResolution;

    _pageSize.setResolution(newResolution);
    emit resolutionChanged();
    emit sizePixelChanged();
    emit unitPerPixelChanged();
}

QSizeF PageSize::size() const
{
    return _pageSize.pageSize().size(QPageSize::Unit(static_cast<int>(_pageSize.unit())));
}

void PageSize::setSize(QSizeF newSize)
{
    if (size() == newSize) { return; }

    qCDebug(documentTemplateControllerCategory) << "Set size " << newSize;

    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_pageSize.unit()));
    _pageSize.setPageSize(QPageSize(newSize, qPageSizeUnit, QString(), QPageSize::ExactMatch));

    emit sizeChanged();
    emit sizePixelChanged();
    emit pageSizeIdChanged();
    emit pageSizeNameChanged();
}

QSize PageSize::sizePixel() const
{
    return _pageSize.pageSize().sizePixels(_pageSize.resolution());
}

documenttemplate::data::PageSizeData::Unit PageSize::unit() const
{
    return _pageSize.unit();
}

void PageSize::setUnit(documenttemplate::data::PageSizeData::Unit newUnit)
{
    if (_pageSize.unit() == newUnit) { return; }

    _pageSize.setUnit(newUnit);
    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_pageSize.unit()));

    qCDebug(documentTemplateControllerCategory) << "Set unit " << qPageSizeUnit;

    _pageSize.setPageSize(
      QPageSize(_pageSize.pageSize().size(qPageSizeUnit), qPageSizeUnit, QString(), QPageSize::ExactMatch));

    emit unitChanged();
    emit sizeChanged();
    emit pageSizeNameChanged();
    emit unitPerPixelChanged();
}

qreal PageSize::unitPerPixel() const
{
    // Use 1000 to have a precision on the 4th digit
    auto qPageSizeUnit = QPageSize::Unit(static_cast<int>(_pageSize.unit()));
    auto pageSize      = QPageSize(QSize(1000, 1000), qPageSizeUnit, QString(), QPageSize::ExactMatch);
    qCDebug(documentTemplateControllerCategory)
      << "Width of 1" << pageSize.sizePixels(_pageSize.resolution()).width();
    // Give the same result than height : its only linked to the resolution and unit
    return 1000.0 / qreal(pageSize.sizePixels(_pageSize.resolution()).width());
}

documenttemplate::data::PageSizeData PageSize::objectData() const
{
    return _pageSize;
}

void PageSize::setObjectData(const documenttemplate::data::PageSizeData& newPageSize)
{
    if (_pageSize == newPageSize) { return; }
    _pageSize = newPageSize;

    emit pageSizeIdChanged();
    emit pageSizeNameChanged();
    emit resolutionChanged();
    emit sizeChanged();
    emit sizePixelChanged();
    emit unitChanged();
    emit unitPerPixelChanged();
}
}    // namespace documenttemplate::controller
