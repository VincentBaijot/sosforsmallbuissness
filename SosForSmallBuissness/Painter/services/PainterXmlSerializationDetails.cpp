#include "PainterXmlSerializationDetails.hpp"

#include <QDebug>

#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"
#include "PainterXmlConstants.hpp"
#include "Utils/XmlSerializationUtils.hpp"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

namespace painter::service::details
{
inline constexpr const QStringView rectanglePositionElement            = u"RectanglePosition";
inline constexpr const QStringView positionDataElement                 = u"PositionData";
inline constexpr const QStringView positionDataXElement                = u"x";
inline constexpr const QStringView positionDataYElement                = u"y";
inline constexpr const QStringView positionDataWidthElement            = u"width";
inline constexpr const QStringView positionDataHeightElement           = u"height";
inline constexpr const QStringView radiusElement                       = u"Radius";
inline constexpr const QStringView sizeModeElement                     = u"SizeMode";
inline constexpr const QStringView colorDataElement                    = u"ColorData";
inline constexpr const QStringView opacityElement                      = u"Opacity";
inline constexpr const QStringView colorElement                        = u"Color";
inline constexpr const QStringView colorRElement                       = u"r";
inline constexpr const QStringView colorGElement                       = u"g";
inline constexpr const QStringView colorBElement                       = u"b";
inline constexpr const QStringView colorAElement                       = u"a";
inline constexpr const QStringView paintableBorderElement              = u"PaintableBorder";
inline constexpr const QStringView borderWidthElement                  = u"BorderWidth";
inline constexpr const QStringView textElement                         = u"Text";
inline constexpr const QStringView fontFamilyElement                   = u"FontFamily";
inline constexpr const QStringView flagsElement                        = u"Flags";
inline constexpr const QStringView pointSizeElement                    = u"PointSize";
inline constexpr const QStringView primaryRepetitionDirectionElement   = u"PrimaryRepetitionDirection";
inline constexpr const QStringView secondaryRepetitionDirectionElement = u"SecondaryRepetitionDirection";

painter::data::PositionData readPositionData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == positionDataElement);

    reader.readNextStartElement();

    painter::data::PositionData positionData;

    while (!utils::xml::checkEndElement(reader, positionDataElement))
    {
        if (reader.isStartElement() && reader.name() == positionDataXElement)
        {
            positionData.x = reader.readElementText().toDouble();
        }
        else if (reader.isStartElement() && reader.name() == positionDataYElement)
        {
            positionData.y = reader.readElementText().toDouble();
        }
        else if (reader.isStartElement() && reader.name() == positionDataWidthElement)
        {
            positionData.width = reader.readElementText().toDouble();
        }
        else if (reader.isStartElement() && reader.name() == positionDataHeightElement)
        {
            positionData.height = reader.readElementText().toDouble();
        }
        else { reader.readNext(); }
    }

    return positionData;
}

void writePositionData(QXmlStreamWriter& writer, const painter::data::PositionData& position)
{
    writer.writeStartElement(positionDataElement.toString());

    writer.writeStartElement(positionDataXElement.toString());
    writer.writeCharacters(QString::number(position.x));
    writer.writeEndElement();

    writer.writeStartElement(positionDataYElement.toString());
    writer.writeCharacters(QString::number(position.y));
    writer.writeEndElement();

    writer.writeStartElement(positionDataWidthElement.toString());
    writer.writeCharacters(QString::number(position.width));
    writer.writeEndElement();

    writer.writeStartElement(positionDataHeightElement.toString());
    writer.writeCharacters(QString::number(position.height));
    writer.writeEndElement();

    writer.writeEndElement();
}

painter::data::RectanglePositionData readRectanglePositionData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == rectanglePositionElement);

    reader.readNextStartElement();

    painter::data::RectanglePositionData rectanglePositionData;

    while (!utils::xml::checkEndElement(reader, rectanglePositionElement))
    {
        if (reader.isStartElement() && reader.name() == positionDataElement)
        {
            rectanglePositionData.position = readPositionData(reader);
        }
        else if (reader.isStartElement() && reader.name() == radiusElement)
        {
            rectanglePositionData.radius = reader.readElementText().toDouble();
        }
        else if (reader.isStartElement() && reader.name() == sizeModeElement)
        {
            rectanglePositionData.mode = Qt::SizeMode(reader.readElementText().toInt());
        }
        else { reader.readNext(); }
    }

    return rectanglePositionData;
}

void writeRectanglePositionData(QXmlStreamWriter& writer,
                                const painter::data::RectanglePositionData& rectanglePositionData)
{
    writer.writeStartElement(rectanglePositionElement.toString());

    writePositionData(writer, rectanglePositionData.position);

    writer.writeStartElement(radiusElement.toString());
    writer.writeCharacters(QString::number(rectanglePositionData.radius));
    writer.writeEndElement();

    writer.writeStartElement(sizeModeElement.toString());
    writer.writeCharacters(QString::number(rectanglePositionData.mode));
    writer.writeEndElement();

    // end rectanglePositionElement
    writer.writeEndElement();
}

QColor readColorElement(QXmlStreamReader& reader)
{
    QColor color;

    while (!utils::xml::checkEndElement(reader, colorElement))
    {
        if (reader.isStartElement() && reader.name() == colorRElement)
        {
            color.setRed(reader.readElementText().toInt());
        }
        else if (reader.isStartElement() && reader.name() == colorGElement)
        {
            color.setGreen(reader.readElementText().toInt());
        }
        else if (reader.isStartElement() && reader.name() == colorBElement)
        {
            color.setBlue(reader.readElementText().toInt());
        }
        else if (reader.isStartElement() && reader.name() == colorAElement)
        {
            color.setAlpha(reader.readElementText().toInt());
        }
        else { reader.readNext(); }
    }

    return color;
}

painter::data::ColorData readColorData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == colorDataElement);

    reader.readNextStartElement();

    painter::data::ColorData colorData;

    while (!utils::xml::checkEndElement(reader, colorDataElement))
    {
        if (reader.isStartElement() && reader.name() == opacityElement)
        {
            colorData.opacity = reader.readElementText().toInt();
        }
        else if (reader.isStartElement() && reader.name() == colorElement)
        {
            colorData.color = readColorElement(reader);
        }
        else { reader.readNext(); }
    }

    return colorData;
}

void writeColorData(QXmlStreamWriter& writer, const painter::data::ColorData& colorData)
{
    writer.writeStartElement(colorDataElement.toString());

    if (colorData.color.isValid())
    {
        writer.writeStartElement(opacityElement.toString());
        writer.writeCharacters(QString::number(colorData.opacity));
        writer.writeEndElement();

        writer.writeStartElement(colorElement.toString());

        writer.writeStartElement(colorRElement.toString());
        writer.writeCharacters(QString::number(colorData.color.red()));
        writer.writeEndElement();

        writer.writeStartElement(colorGElement.toString());
        writer.writeCharacters(QString::number(colorData.color.green()));
        writer.writeEndElement();

        writer.writeStartElement(colorBElement.toString());
        writer.writeCharacters(QString::number(colorData.color.blue()));
        writer.writeEndElement();

        writer.writeStartElement(colorAElement.toString());
        writer.writeCharacters(QString::number(colorData.color.alpha()));
        writer.writeEndElement();

        // End colorElement
        writer.writeEndElement();
    }

    // End colorDataElement
    writer.writeEndElement();
}

painter::data::PaintableBorderData readPaintableBorderData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableBorderElement);

    reader.readNextStartElement();

    painter::data::PaintableBorderData paintableBorderData;

    while (!utils::xml::checkEndElement(reader, paintableBorderElement))
    {
        if (reader.isStartElement() && reader.name() == borderWidthElement)
        {
            paintableBorderData.borderWidth = reader.readElementText().toDouble();
        }
        else if (reader.isStartElement() && reader.name() == colorDataElement)
        {
            paintableBorderData.color = readColorData(reader);
        }
        else { reader.readNext(); }
    }

    return paintableBorderData;
}

void writePaintableBorderData(QXmlStreamWriter& writer,
                              const painter::data::PaintableBorderData& paintableBorderData)
{
    writer.writeStartElement(paintableBorderElement.toString());

    writer.writeStartElement(borderWidthElement.toString());
    writer.writeCharacters(QString::number(paintableBorderData.borderWidth));
    writer.writeEndElement();

    writeColorData(writer, paintableBorderData.color);

    writer.writeEndElement();
}

painter::data::PaintableRectangleData readPaintableRectangleData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableRectangleElement);

    reader.readNextStartElement();

    painter::data::PaintableRectangleData paintableRectangleData;

    while (!utils::xml::checkEndElement(reader, paintableRectangleElement))
    {
        if (reader.isStartElement() && reader.name() == details::rectanglePositionElement)
        {
            paintableRectangleData.position = details::readRectanglePositionData(reader);
        }
        else if (reader.isStartElement() && reader.name() == details::colorDataElement)
        {
            paintableRectangleData.color = details::readColorData(reader);
        }
        else if (reader.isStartElement() && reader.name() == details::paintableBorderElement)
        {
            paintableRectangleData.border = details::readPaintableBorderData(reader);
        }
        else { reader.readNext(); }
    }

    return paintableRectangleData;
}

void writePaintableRectangleData(QXmlStreamWriter& writer,
                                 const painter::data::PaintableRectangleData& paintableRectangleData)
{
    writer.writeStartElement(paintableRectangleElement.toString());

    details::writeRectanglePositionData(writer, paintableRectangleData.position);
    details::writeColorData(writer, paintableRectangleData.color);
    details::writePaintableBorderData(writer, paintableRectangleData.border);

    // end paintableRectangleElement
    writer.writeEndElement();
}

painter::data::PaintableTextData readPaintableTextData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableTextElement);

    reader.readNextStartElement();

    painter::data::PaintableTextData paintableTextData;

    while (!utils::xml::checkEndElement(reader, paintableTextElement))
    {
        if (reader.isStartElement() && reader.name() == details::colorDataElement)
        {
            paintableTextData.color = details::readColorData(reader);
        }
        else if (reader.isStartElement() && reader.name() == paintableRectangleElement)
        {
            paintableTextData.background = details::readPaintableRectangleData(reader);
        }
        else if (reader.isStartElement() && reader.name() == details::textElement)
        {
            paintableTextData.text = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == details::fontFamilyElement)
        {
            paintableTextData.fontFamily = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == details::flagsElement)
        {
            paintableTextData.flags = reader.readElementText().toInt();
        }
        else if (reader.isStartElement() && reader.name() == details::pointSizeElement)
        {
            paintableTextData.pointSize = reader.readElementText().toInt();
        }
        else { reader.readNext(); }
    }

    return paintableTextData;
}

void writePaintableTextData(QXmlStreamWriter& writer, const painter::data::PaintableTextData& paintableTextData)
{
    writer.writeStartElement(paintableTextElement.toString());

    details::writeColorData(writer, paintableTextData.color);
    details::writePaintableRectangleData(writer, paintableTextData.background);

    writer.writeStartElement(details::textElement.toString());
    writer.writeCharacters(paintableTextData.text);
    writer.writeEndElement();

    writer.writeStartElement(details::fontFamilyElement.toString());
    writer.writeCharacters(paintableTextData.fontFamily);
    writer.writeEndElement();

    writer.writeStartElement(details::flagsElement.toString());
    writer.writeCharacters(QString::number(paintableTextData.flags));
    writer.writeEndElement();

    writer.writeStartElement(details::pointSizeElement.toString());
    writer.writeCharacters(QString::number(paintableTextData.pointSize));
    writer.writeEndElement();

    // end paintableTextElement
    writer.writeEndElement();
}

painter::data::PaintableItemListModelData readPaintableItemListModelData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableItemListElement);

    reader.readNextStartElement();

    painter::data::PaintableItemListModelData paintableItemListModelData;

    while (!utils::xml::checkEndElement(reader, paintableItemListElement))
    {
        if (reader.isStartElement() && reader.name() == paintableRectangleElement)
        {
            paintableItemListModelData.paintableItemList.append(details::readPaintableRectangleData(reader));
        }
        else if (reader.isStartElement() && reader.name() == paintableTextElement)
        {
            paintableItemListModelData.paintableItemList.append(details::readPaintableTextData(reader));
        }
        else { reader.readNext(); }
    }

    return paintableItemListModelData;
}

void writePaintableItemListModelData(QXmlStreamWriter& writer,
                                     const painter::data::PaintableItemListModelData& paintableItemListModelData)
{
    writer.writeStartElement(details::paintableItemListElement.toString());

    for (const painter::data::VariantListableItemData& variantListableItem :
         paintableItemListModelData.paintableItemList)
    {
        std::visit(
          overloaded {
            [&writer](const painter::data::PaintableRectangleData& paintableRectangleData)
            { details::writePaintableRectangleData(writer, paintableRectangleData); },
            [&writer](const painter::data::PaintableTextData& paintableTextData)
            { details::writePaintableTextData(writer, paintableTextData); },
          },
          variantListableItem);
    }

    writer.writeEndElement();
}

painter::data::PaintableListData readPaintableListData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableListElement);

    reader.readNextStartElement();

    painter::data::PaintableListData paintableListData;

    while (!utils::xml::checkEndElement(reader, paintableListElement))
    {
        if (reader.isStartElement() && reader.name() == positionDataElement)
        {
            if (std::optional<painter::data::PositionData> optPositionData = readPositionData(reader))
            {
                paintableListData.position = optPositionData.value();
            }
            else { reader.readNext(); }
        }
        else if (reader.isStartElement() && reader.name() == details::primaryRepetitionDirectionElement)
        {
            paintableListData.primaryRepetitionDirection =
              painter::data::PaintableListData::RepetitionDirection(reader.readElementText().toInt());
        }
        else if (reader.isStartElement() && reader.name() == details::secondaryRepetitionDirectionElement)
        {
            paintableListData.secondaryRepetitionDirection =
              painter::data::PaintableListData::RepetitionDirection(reader.readElementText().toInt());
        }
        else if (reader.isStartElement() && reader.name() == details::paintableItemListElement)
        {
            paintableListData.paintableItemListModel = readPaintableItemListModelData(reader);
        }
        else { reader.readNext(); }
    }

    return paintableListData;
}

void writePaintableListData(QXmlStreamWriter& writer, const painter::data::PaintableListData& paintableListData)
{
    writer.writeStartElement(paintableListElement.toString());

    details::writePositionData(writer, paintableListData.position);

    writer.writeStartElement(details::primaryRepetitionDirectionElement.toString());
    writer.writeCharacters(QString::number(static_cast<int>(paintableListData.primaryRepetitionDirection)));
    writer.writeEndElement();

    writer.writeStartElement(details::secondaryRepetitionDirectionElement.toString());
    writer.writeCharacters(QString::number(static_cast<int>(paintableListData.secondaryRepetitionDirection)));
    writer.writeEndElement();

    details::writePaintableItemListModelData(writer, paintableListData.paintableItemListModel);

    writer.writeEndElement();
}

}    // namespace painter::service::details
