#pragma once

#include <QStringView>

namespace painter::service
{
inline constexpr const QStringView paintableRectangleElement = u"PaintableRectangle";
inline constexpr const QStringView paintableTextElement      = u"PaintableText";
inline constexpr const QStringView paintableListElement      = u"PaintableList";
}    // namespace painter::service
