#pragma once

#include <QStringView>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "PainterService_global.hpp"

namespace painter::data
{
struct PaintableRectangleData;
struct PaintableTextData;
struct PaintableListData;
struct PaintableItemListModelData;
}    // namespace painter::data

namespace painter::service::details
{
constexpr const QStringView paintableItemListElement = u"PaintableItemList";

PAINTER_SERVICES_EXPORT painter::data::PaintableRectangleData readPaintableRectangleData(QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableRectangleData(
  QXmlStreamWriter& writer,
  const painter::data::PaintableRectangleData& paintableRectangleData);

PAINTER_SERVICES_EXPORT painter::data::PaintableTextData readPaintableTextData(QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableTextData(QXmlStreamWriter& writer,
                                                    const painter::data::PaintableTextData& paintableTextData);

PAINTER_SERVICES_EXPORT painter::data::PaintableItemListModelData readPaintableItemListModelData(
  QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableItemListModelData(
  QXmlStreamWriter& writer,
  const painter::data::PaintableItemListModelData& paintableItemListModelData);

PAINTER_SERVICES_EXPORT painter::data::PaintableListData readPaintableListData(QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableListData(QXmlStreamWriter& writer,
                                                    const painter::data::PaintableListData& paintableListData);

}    // namespace painter::service::details
