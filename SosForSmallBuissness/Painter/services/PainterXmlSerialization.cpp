#include "PainterXmlSerialization.hpp"

#include <QDebug>

#include "Painter/data/PaintableListData.hpp"
#include "Painter/data/PaintableRectangleData.hpp"
#include "Painter/data/PaintableTextData.hpp"
#include "PainterXmlConstants.hpp"
#include "PainterXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace painter::service
{
std::optional<painter::data::PaintableRectangleData> readPaintableRectangleData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, paintableRectangleElement, u"PaintableRectangleData"))
    {
        return std::nullopt;
    }

    return details::readPaintableRectangleData(reader);
}

void writePaintableRectangleData(QXmlStreamWriter& writer,
                                 const painter::data::PaintableRectangleData& paintableRectangleData)
{
    details::writePaintableRectangleData(writer, paintableRectangleData);
}

std::optional<painter::data::PaintableTextData> readPaintableTextData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, paintableTextElement, u"PaintableTextData"))
    {
        return std::nullopt;
    }

    return details::readPaintableTextData(reader);
}

void writePaintableTextData(QXmlStreamWriter& writer, const painter::data::PaintableTextData& paintableTextData)
{
    details::writePaintableTextData(writer, paintableTextData);
}

std::optional<painter::data::PaintableListData> readPaintableListData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, paintableListElement, u"PaintableListData"))
    {
        return std::nullopt;
    }

    return details::readPaintableListData(reader);
}

void writePaintableListData(QXmlStreamWriter& writer, const painter::data::PaintableListData& paintableListData)
{
    details::writePaintableListData(writer, paintableListData);
}

}    // namespace painter::service
