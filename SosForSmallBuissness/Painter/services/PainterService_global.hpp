#pragma once

#include <QtCore/qglobal.h>

#if defined(PAINTER_SERVICES_LIBRARY)
#define PAINTER_SERVICES_EXPORT Q_DECL_EXPORT
#else
#define PAINTER_SERVICES_EXPORT Q_DECL_IMPORT
#endif

#if defined(ENABLE_TESTING)
#define PAINTER_SERVICES_TESTING_EXPORT PAINTER_SERVICES_EXPORT
#else
#define PAINTER_SERVICES_TESTING_EXPORT
#endif
