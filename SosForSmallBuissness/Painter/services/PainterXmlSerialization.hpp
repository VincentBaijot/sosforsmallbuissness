#pragma once

#include <QStringView>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "PainterService_global.hpp"

namespace painter::data
{
struct PaintableRectangleData;
struct PaintableTextData;
struct PaintableListData;
}    // namespace painter::data

namespace painter::service
{
PAINTER_SERVICES_EXPORT std::optional<painter::data::PaintableRectangleData> readPaintableRectangleData(
  QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableRectangleData(
  QXmlStreamWriter& writer,
  const painter::data::PaintableRectangleData& paintableRectangleData);

PAINTER_SERVICES_EXPORT std::optional<painter::data::PaintableTextData> readPaintableTextData(
  QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableTextData(QXmlStreamWriter& writer,
                                                    const painter::data::PaintableTextData& paintableTextData);

PAINTER_SERVICES_EXPORT std::optional<painter::data::PaintableListData> readPaintableListData(
  QXmlStreamReader& reader);
PAINTER_SERVICES_EXPORT void writePaintableListData(QXmlStreamWriter& writer,
                                                    const painter::data::PaintableListData& paintableListData);

}    // namespace painter::service
