#include "Position.hpp"

#include "data/PositionData.hpp"

namespace painter::controller
{
Position::Position(QObject* parent) : QObject { parent }
{
}

Position::Position(const painter::data::PositionData& positionData, QObject* parent)
    : QObject { parent },
      _x { positionData.x },
      _y { positionData.y },
      _width { positionData.width },
      _height { positionData.height }
{
}

qreal Position::x() const
{
    return _x;
}

void Position::setX(qreal newX)
{
    if (qFuzzyCompare(_x, newX)) return;
    _x = newX;
    emit xChanged();
}

qreal Position::y() const
{
    return _y;
}

void Position::setY(qreal newY)
{
    if (qFuzzyCompare(_y, newY)) return;
    _y = newY;
    emit yChanged();
}

qreal Position::width() const
{
    return _width;
}

void Position::setWidth(qreal newWidth)
{
    if (qFuzzyCompare(_width, newWidth)) return;
    _width = newWidth;
    emit widthChanged();
}

qreal Position::height() const
{
    return _height;
}

void Position::setHeight(qreal newHeight)
{
    if (qFuzzyCompare(_height, newHeight)) return;
    _height = newHeight;
    emit heightChanged();
}

painter::data::PositionData Position::positionData() const
{
    return painter::data::PositionData { x(), y(), width(), height() };
}

void Position::setPositionData(const painter::data::PositionData& positionData)
{
    setX(positionData.x);
    setY(positionData.y);
    setWidth(positionData.width);
    setHeight(positionData.height);
}

painter::data::PositionData Position::objectData() const
{
    return positionData();
}

void Position::setObjectData(const painter::data::PositionData& newData)
{
    setPositionData(newData);
}

}    // namespace painter::controller
