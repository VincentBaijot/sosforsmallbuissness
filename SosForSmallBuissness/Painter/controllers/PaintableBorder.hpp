#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "PainterController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace painter::data
{
struct PaintableBorderData;
}

namespace painter::controller
{
class Color;

class PAINTER_CONTROLLER_EXPORT PaintableBorder : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal borderWidth READ borderWidth WRITE setBorderWidth NOTIFY borderWidthChanged FINAL)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged FINAL)
    Q_PROPERTY(bool colorIsValid READ colorIsValid NOTIFY colorChanged FINAL)

    QML_ELEMENT
  public:
    explicit PaintableBorder(QObject* parent = nullptr);
    explicit PaintableBorder(const painter::data::PaintableBorderData& data, QObject* parent = nullptr);
    ~PaintableBorder() override;

    qreal borderWidth() const;
    void setBorderWidth(qreal newBorderWidth);

    QColor color() const;
    void setColor(const QColor& newColor);
    bool colorIsValid() const;

    /*!
     * @brief opacity between 0 and 100
     */
    int opacity() const;
    void setOpacity(int newOpacity);
    int alpha() const;

    painter::data::PaintableBorderData objectData() const;
    void setObjectData(const painter::data::PaintableBorderData& data);

  signals:
    void borderWidthChanged();
    void colorChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableBorder)
    qreal _borderWidth { 0 };
    utils::own_qobject<painter::controller::Color> _color;
};
}    // namespace painter::controller
