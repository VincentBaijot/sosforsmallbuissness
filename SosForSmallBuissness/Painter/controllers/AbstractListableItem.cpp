#include "AbstractListableItem.hpp"

#include "PaintableItemListModel.hpp"

namespace painter::controller
{
AbstractListableItem::AbstractListableItem(QObject* parent) : InterfaceQmlPaintableItem { parent }
{
}

PaintableItemListModel* AbstractListableItem::parentList() const
{
    return _parentList;
}

void AbstractListableItem::setParentList(PaintableItemListModel* newParentList)
{
    if (_parentList == newParentList) return;
    _parentList = newParentList;
    setParent(newParentList);
    emit parentListChanged();
}
}    // namespace painter::controller
