#pragma once

#include <QPainterPath>

#include "Position.hpp"

namespace painter::data
{
struct RectanglePositionData;
}

namespace painter::controller
{
class RectanglePosition : public Position
{
    Q_OBJECT

    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(qreal calculatedRadius READ calculatedRadius NOTIFY calculatedRadiusChanged)
    Q_PROPERTY(Qt::SizeMode mode READ mode WRITE setMode NOTIFY modeChanged)

  public:
    enum class PaintableRectangleSizeMode
    {
        PaintableRectangleAbsoluteSize = Qt::SizeMode::AbsoluteSize,
        PaintableRectangleRelativeSize = Qt::SizeMode::RelativeSize
    };
    Q_ENUM(PaintableRectangleSizeMode)

    explicit RectanglePosition(QObject* parent = nullptr);
    explicit RectanglePosition(const painter::data::RectanglePositionData& data, QObject* parent = nullptr);
    ~RectanglePosition() override = default;

    qreal radius() const;
    void setRadius(qreal newRadius);

    Qt::SizeMode mode() const;
    void setMode(Qt::SizeMode newMode);

    qreal calculatedRadius() const;

    QPainterPath roundedRectanglePath(const QRectF& roundedRectangleContainer, qreal radiusRectangleSize) const;

    painter::data::RectanglePositionData objectData() const;
    void setObjectData(const painter::data::RectanglePositionData& data);

  signals:
    void radiusChanged();
    void modeChanged();
    void calculatedRadiusChanged();

  private:
    Q_DISABLE_COPY_MOVE(RectanglePosition)

    void _createInternalConnections() const;

    qreal _radius { 0 };
    Qt::SizeMode _mode { Qt::AbsoluteSize };
};
}    // namespace painter::controller
