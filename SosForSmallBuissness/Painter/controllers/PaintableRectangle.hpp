#pragma once

#include <QtQml/QQmlEngine>
#include <Utils/OwnQObjectPointer.hpp>

#include "AbstractListableItem.hpp"
#include "PainterController_global.hpp"

namespace painter::data
{
struct PaintableRectangleData;
struct PositionData;
}    // namespace painter::data

namespace painter::controller
{
class RectanglePosition;
class Color;
class PaintableBorder;

class PAINTER_CONTROLLER_EXPORT PaintableRectangle : public AbstractListableItem
{
    Q_OBJECT

    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged FINAL)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged FINAL)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged FINAL)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged FINAL)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged FINAL)
    Q_PROPERTY(int opacity READ opacity WRITE setOpacity NOTIFY opacityChanged FINAL)
    Q_PROPERTY(bool colorIsValid READ colorIsValid NOTIFY colorChanged FINAL)

    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged FINAL)
    Q_PROPERTY(qreal calculatedRadius READ calculatedRadius NOTIFY calculatedRadiusChanged FINAL)
    Q_PROPERTY(Qt::SizeMode mode READ mode WRITE setMode NOTIFY modeChanged FINAL)

    Q_PROPERTY(painter::controller::PaintableBorder* border READ border CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit PaintableRectangle(QObject* parent = nullptr);
    explicit PaintableRectangle(const painter::data::PaintableRectangleData& data, QObject* parent = nullptr);
    ~PaintableRectangle() override;

    enum class PaintableRectangleSizeMode
    {
        PaintableRectangleAbsoluteSize = Qt::SizeMode::AbsoluteSize,
        PaintableRectangleRelativeSize = Qt::SizeMode::RelativeSize
    };
    Q_ENUM(PaintableRectangleSizeMode)

    void paint(QPainter* painter) const override;

    ItemType itemType() const override;

    PaintableBorder* border();
    const PaintableBorder* border() const;

    qreal x() const;
    void setX(qreal newX);

    qreal y() const;
    void setY(qreal newY);

    qreal width() const;
    void setWidth(qreal newWidth);

    qreal height() const;
    void setHeight(qreal newHeight);

    painter::data::PositionData positionData() const;
    void setPositionData(const painter::data::PositionData& positionData);

    QColor color() const;
    void setColor(const QColor& newColor);
    bool colorIsValid() const;

    /*!
     * @brief opacity between 0 and 100
     */
    int opacity() const;
    void setOpacity(int newOpacity);
    int alpha() const;

    qreal radius() const;
    void setRadius(qreal newRadius);

    Qt::SizeMode mode() const;
    void setMode(Qt::SizeMode newMode);

    qreal calculatedRadius() const;

    painter::data::PaintableRectangleData objectData() const;
    void setObjectData(const painter::data::PaintableRectangleData& newData);

  signals:
    void xChanged();
    void yChanged();
    void widthChanged();
    void heightChanged();
    void colorChanged();
    void opacityChanged();
    void radiusChanged();
    void modeChanged();
    void calculatedRadiusChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableRectangle)

    void _createInternalConnections() const;

    utils::own_qobject<RectanglePosition> _rectanglePosition;
    utils::own_qobject<Color> _color;
    utils::own_qobject<PaintableBorder> _border;
};
}    // namespace painter::controller
