#include "PaintableBorder.hpp"

#include <QPainter>
#include <QPainterPath>

#include "Color.hpp"
#include "data/PaintableBorderData.hpp"

namespace painter::controller
{

PaintableBorder::PaintableBorder(QObject* parent) : QObject(parent), _color { utils::make_qobject<Color>(*this) }
{
    QObject::connect(_color.get(), &Color::colorChanged, this, &PaintableBorder::colorChanged);
}

PaintableBorder::PaintableBorder(const painter::data::PaintableBorderData& data, QObject* parent)
    : QObject(parent), _borderWidth { data.borderWidth }, _color { utils::make_qobject<Color>(*this, data.color) }
{
    QObject::connect(_color.get(), &Color::colorChanged, this, &PaintableBorder::colorChanged);
}

PaintableBorder::~PaintableBorder() = default;

qreal PaintableBorder::borderWidth() const
{
    return _borderWidth;
}

void PaintableBorder::setBorderWidth(qreal newBorderWidth)
{
    if (qFuzzyCompare(_borderWidth, newBorderWidth)) { return; }
    _borderWidth = newBorderWidth;
    emit borderWidthChanged();
}

QColor PaintableBorder::color() const
{
    return _color->color();
}

void PaintableBorder::setColor(const QColor& newColor)
{
    _color->setColor(newColor);
}

bool PaintableBorder::colorIsValid() const
{
    return _color->colorIsValid();
}

int PaintableBorder::opacity() const
{
    return _color->opacity();
}

void PaintableBorder::setOpacity(int newOpacity)
{
    _color->setOpacity(newOpacity);
}

int PaintableBorder::alpha() const
{
    return _color->alpha();
}

painter::data::PaintableBorderData PaintableBorder::objectData() const
{
    painter::data::PaintableBorderData data;
    data.borderWidth = _borderWidth;
    data.color       = _color->objectData();
    return data;
}

void PaintableBorder::setObjectData(const painter::data::PaintableBorderData& newData)
{
    setBorderWidth(newData.borderWidth);
    _color->setObjectData(newData.color);
}

}    // namespace painter::controller
