#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "InterfaceQmlPaintableItem.hpp"
#include "PainterController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("Painter/controllers/ListableItemListModel.hpp")

namespace painter::data
{
struct PaintableListData;
struct PositionData;
}    // namespace painter::data

namespace painter::controller
{
class Position;
class ListableItemListModel;

class PAINTER_CONTROLLER_EXPORT PaintableList : public InterfaceQmlPaintableItem
{
    Q_OBJECT

    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged FINAL)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged FINAL)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged FINAL)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged FINAL)
    Q_PROPERTY(painter::controller::PaintableList::RepetitionDirection primaryRepetitionDirection READ
                 primaryRepetitionDirection WRITE setPrimaryRepetitionDirection NOTIFY
                   primaryRepetitionDirectionChanged FINAL)
    Q_PROPERTY(painter::controller::PaintableList::RepetitionDirection secondaryRepetitionDirection READ
                 secondaryRepetitionDirection WRITE setSecondaryRepetitionDirection NOTIFY
                   secondaryRepetitionDirectionChanged FINAL)

    Q_PROPERTY(
      painter::controller::ListableItemListModel* listableItemListModel READ listableItemListModel CONSTANT FINAL)
    Q_PROPERTY(QStringList repetitionDirectionNames READ repetitionDirectionNames CONSTANT FINAL)

    QML_ELEMENT
  public:
    // Must be in sync with painter::data::PaintableListData::RepetitionDirection
    enum class RepetitionDirection
    {
        NoRepetition,
        BottomRepetition,
        RightRepetition,
        TopRepetition,
        LeftRepetition
    };
    Q_ENUM(RepetitionDirection)

    explicit PaintableList(QObject* parent = nullptr);
    explicit PaintableList(const painter::data::PaintableListData& data, QObject* parent = nullptr);
    ~PaintableList() override;

    void paint(QPainter* painter) const override;

    ItemType itemType() const override;

    qreal x() const;
    void setX(qreal newX);

    qreal y() const;
    void setY(qreal newY);

    qreal width() const;
    void setWidth(qreal newWidth);

    qreal height() const;
    void setHeight(qreal newHeight);

    painter::data::PositionData positionData() const;
    void setPositionData(const painter::data::PositionData& position);

    RepetitionDirection primaryRepetitionDirection() const;
    void setPrimaryRepetitionDirection(RepetitionDirection newPrimaryRepetitionDirection);

    RepetitionDirection secondaryRepetitionDirection() const;
    void setSecondaryRepetitionDirection(RepetitionDirection newSecondaryRepetitionDirection);

    QStringList repetitionDirectionNames() const;

    ListableItemListModel* listableItemListModel() const;

    painter::data::PaintableListData objectData() const;
    void setObjectData(const painter::data::PaintableListData& newData);

  signals:
    void xChanged();
    void yChanged();
    void widthChanged();
    void heightChanged();

    void primaryRepetitionDirectionChanged();

    void secondaryRepetitionDirectionChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableList)

    void _createInternalConnections() const;

    utils::own_qobject<Position> _abstractPosition;
    utils::own_qobject<ListableItemListModel> _listableItemListModel;
    RepetitionDirection _primaryRepetitionDirection { RepetitionDirection::NoRepetition };
    RepetitionDirection _secondaryRepetitionDirection { RepetitionDirection::NoRepetition };
};

}    // namespace painter::controller
