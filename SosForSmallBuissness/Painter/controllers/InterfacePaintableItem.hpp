#pragma once

#include <QObject>

class QPainter;

namespace painter::controller
{

class InterfacePaintableItem
{
  public:
    InterfacePaintableItem()          = default;
    virtual ~InterfacePaintableItem() = default;

    virtual void paint(QPainter* device) const = 0;

  private:
    Q_DISABLE_COPY_MOVE(InterfacePaintableItem)
};

}    // namespace painter::controller
