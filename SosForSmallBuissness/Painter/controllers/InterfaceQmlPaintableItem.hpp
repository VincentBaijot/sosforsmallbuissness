#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "InterfacePaintableItem.hpp"
#include "PainterController_global.hpp"

namespace painter::controller
{

class PAINTER_CONTROLLER_EXPORT InterfaceQmlPaintableItem : public QObject, public InterfacePaintableItem
{
    Q_OBJECT
    Q_PROPERTY(InterfaceQmlPaintableItem::ItemType itemType READ itemType CONSTANT FINAL)
    Q_PROPERTY(QString itemName READ itemName WRITE setItemName NOTIFY itemNameChanged FINAL)

    QML_ELEMENT
    QML_UNCREATABLE("Abstract type")
  public:
    enum class ItemType
    {
        RectangleItemType,
        TextItemType,
        ImageItemType,
        ListItemType
    };
    Q_ENUM(ItemType)

    explicit InterfaceQmlPaintableItem(QObject* parent = nullptr) : QObject(parent), InterfacePaintableItem() {}
    ~InterfaceQmlPaintableItem() override = default;

    virtual ItemType itemType() const = 0;

    QString itemName() const;
    void setItemName(const QString& newItemName);

  signals:
    void itemNameChanged();

  private:
    Q_DISABLE_COPY_MOVE(InterfaceQmlPaintableItem)

    QString _itemName;
};

}    // namespace painter::controller
