#include "PaintableRectangle.hpp"

#include <QPainter>

#include "Color.hpp"
#include "Constants.hpp"
#include "PaintableBorder.hpp"
#include "RectanglePosition.hpp"
#include "data/PaintableRectangleData.hpp"

namespace painter::controller
{
PaintableRectangle::PaintableRectangle(QObject* parent)
    : AbstractListableItem(parent),
      _rectanglePosition { utils::make_qobject<RectanglePosition>(*this) },
      _color { utils::make_qobject<Color>(*this) },
      _border { utils::make_qobject<PaintableBorder>(*this) }
{
    _createInternalConnections();
}

PaintableRectangle::PaintableRectangle(const painter::data::PaintableRectangleData& data, QObject* parent)
    : AbstractListableItem(parent),
      _rectanglePosition { utils::make_qobject<RectanglePosition>(*this, data.position) },
      _color { utils::make_qobject<Color>(*this, data.color) },
      _border { utils::make_qobject<PaintableBorder>(*this, data.border) }
{
    _createInternalConnections();
}

PaintableRectangle::~PaintableRectangle() = default;

void PaintableRectangle::paint(QPainter* painter) const
{
    if (color().isValid())
    {
        painter->setPen(Qt::PenStyle::NoPen);

        QBrush brush;
        brush.setColor(color());
        brush.setStyle(Qt::BrushStyle::SolidPattern);
        painter->setBrush(brush);

        QRectF rectangleSize { _rectanglePosition->positionData().x,
                               _rectanglePosition->positionData().y,
                               _rectanglePosition->positionData().width,
                               _rectanglePosition->positionData().height };
        if (_border->borderWidth() > 0)
        {
            qreal marginSize = qMin(border()->borderWidth(), qMin(width(), height()) / 2.0);
            rectangleSize.adjust(marginSize, marginSize, -marginSize, -marginSize);
        }

        if (_rectanglePosition->radius() != 0 && _rectanglePosition->calculatedRadius() > _border->borderWidth())
        {
            painter->setRenderHint(QPainter::RenderHint::Antialiasing, true);

            qreal marginSize = qMin(border()->borderWidth(), qMin(width(), height()) / 2.0);
            QPainterPath painterPath(_rectanglePosition->roundedRectanglePath(
              rectangleSize, 2 * (_rectanglePosition->calculatedRadius() - marginSize)));
            painter->drawPath(painterPath);
        }
        else { painter->drawRect(rectangleSize); }
    }

    if (_border->borderWidth() > 0)
    {
        qreal borderWidthValue = qMin(_border->borderWidth(), qMin(width(), height()) / 2.0);

        QPainter::RenderHints previousRenderHints = painter->renderHints();
        QPen pen;
        pen.setWidthF(borderWidthValue);
        pen.setColor(_border->color());
        painter->setPen(pen);

        painter->setBrush(Qt::BrushStyle::NoBrush);

        QRectF positionRectF { positionData().qRectF() };

        if (_rectanglePosition->radius() != 0)
        {
            painter->setRenderHint(QPainter::RenderHint::Antialiasing, true);

            int marginSize = qFloor(borderWidthValue / 2.0);

            QRectF roundedRectangleContainer(
              positionRectF.marginsRemoved(QMarginsF(marginSize, marginSize, marginSize, marginSize)));
            QPainterPath painterPath(_rectanglePosition->roundedRectanglePath(
              roundedRectangleContainer, 2 * _rectanglePosition->calculatedRadius() - borderWidthValue));

            painter->drawPath(painterPath);
        }
        else
        {
            pen.setJoinStyle(Qt::PenJoinStyle::MiterJoin);
            painter->setPen(pen);

            qreal marginSize = borderWidthValue / 2.0;
            QRectF rectangleSize(
              positionRectF.marginsRemoved(QMarginsF(marginSize, marginSize, marginSize, marginSize)));
            painter->drawRect(rectangleSize);
        }

        painter->setRenderHints(previousRenderHints);
    }
}

InterfaceQmlPaintableItem::ItemType PaintableRectangle::itemType() const
{
    return ItemType::RectangleItemType;
}

PaintableBorder* PaintableRectangle::border()
{
    return _border.get();
}

const PaintableBorder* PaintableRectangle::border() const
{
    return _border.get();
}

qreal PaintableRectangle::x() const
{
    return _rectanglePosition->x();
}

void PaintableRectangle::setX(qreal newX)
{
    qCDebug(painterCategory) << "Rectangle set x " << newX;
    _rectanglePosition->setX(newX);
}

qreal PaintableRectangle::y() const
{
    return _rectanglePosition->y();
}

void PaintableRectangle::setY(qreal newY)
{
    qCDebug(painterCategory) << "Rectangle set y " << newY;
    _rectanglePosition->setY(newY);
}

qreal PaintableRectangle::width() const
{
    return _rectanglePosition->width();
}

void PaintableRectangle::setWidth(qreal newWidth)
{
    qCDebug(painterCategory) << "Rectangle set width " << newWidth;
    _rectanglePosition->setWidth(newWidth);
}

qreal PaintableRectangle::height() const
{
    return _rectanglePosition->height();
}

void PaintableRectangle::setHeight(qreal newHeight)
{
    qCDebug(painterCategory) << "Rectangle set width " << newHeight;
    _rectanglePosition->setHeight(newHeight);
}

painter::data::PositionData PaintableRectangle::positionData() const
{
    return _rectanglePosition->positionData();
}

void PaintableRectangle::setPositionData(const painter::data::PositionData& positionData)
{
    qCDebug(painterCategory) << "Rectangle set position " << positionData;
    _rectanglePosition->setPositionData(positionData);
}

QColor PaintableRectangle::color() const
{
    return _color->color();
}

void PaintableRectangle::setColor(const QColor& newColor)
{
    qCDebug(painterCategory) << "Rectangle set color " << newColor;
    _color->setColor(newColor);
}

bool PaintableRectangle::colorIsValid() const
{
    return _color->colorIsValid();
}

int PaintableRectangle::opacity() const
{
    return _color->opacity();
}

void PaintableRectangle::setOpacity(int newOpacity)
{
    qCDebug(painterCategory) << "Rectangle set opacity " << newOpacity;
    _color->setOpacity(newOpacity);
    _border->setOpacity(newOpacity);
}

int PaintableRectangle::alpha() const
{
    return _color->alpha();
}

qreal PaintableRectangle::radius() const
{
    return _rectanglePosition->radius();
}

void PaintableRectangle::setRadius(qreal newRadius)
{
    qCDebug(painterCategory) << "Rectangle set radius " << newRadius;
    _rectanglePosition->setRadius(newRadius);
}

Qt::SizeMode PaintableRectangle::mode() const
{
    return _rectanglePosition->mode();
}

void PaintableRectangle::setMode(Qt::SizeMode newMode)
{
    qCDebug(painterCategory) << "Rectangle set mode " << newMode;
    _rectanglePosition->setMode(newMode);
}

qreal PaintableRectangle::calculatedRadius() const
{
    return _rectanglePosition->calculatedRadius();
}

painter::data::PaintableRectangleData PaintableRectangle::objectData() const
{
    painter::data::PaintableRectangleData data;
    data.position = _rectanglePosition->objectData();
    data.border   = _border->objectData();
    data.color    = _color->objectData();

    return data;
}

void PaintableRectangle::setObjectData(const painter::data::PaintableRectangleData& newData)
{
    _rectanglePosition->setObjectData(newData.position);
    _border->setObjectData(newData.border);
    _color->setObjectData(newData.color);
}

void PaintableRectangle::_createInternalConnections() const
{
    QObject::connect(_rectanglePosition.get(), &Position::xChanged, this, &PaintableRectangle::xChanged);
    QObject::connect(_rectanglePosition.get(), &Position::yChanged, this, &PaintableRectangle::yChanged);
    QObject::connect(_rectanglePosition.get(), &Position::widthChanged, this, &PaintableRectangle::widthChanged);
    QObject::connect(_rectanglePosition.get(), &Position::heightChanged, this, &PaintableRectangle::heightChanged);
    QObject::connect(_color.get(), &Color::colorChanged, this, &PaintableRectangle::colorChanged);
    QObject::connect(_color.get(), &Color::opacityChanged, this, &PaintableRectangle::opacityChanged);
    QObject::connect(
      _rectanglePosition.get(), &RectanglePosition::radiusChanged, this, &PaintableRectangle::radiusChanged);
    QObject::connect(
      _rectanglePosition.get(), &RectanglePosition::modeChanged, this, &PaintableRectangle::modeChanged);
    QObject::connect(_rectanglePosition.get(),
                     &RectanglePosition::calculatedRadiusChanged,
                     this,
                     &PaintableRectangle::calculatedRadiusChanged);
}

}    // namespace painter::controller
