#include "DefaultPaintableItemsFactory.hpp"

#include <QColor>

#include "PaintableBorder.hpp"
#include "PaintableItemListModel.hpp"
#include "PaintableList.hpp"
#include "PaintableRectangle.hpp"
#include "PaintableText.hpp"
#include "data/PositionData.hpp"


namespace painter::controller::DefaultPaintableItemsFactory
{
utils::own_qobject<InterfaceQmlPaintableItem> paintableQmlItem(InterfaceQmlPaintableItem::ItemType itemType,
                                                               const data::PositionData& position,
                                                               PaintableItemListModel& parent)
{
    utils::own_qobject<InterfaceQmlPaintableItem> positionableItem;
    switch (itemType)
    {
        case InterfaceQmlPaintableItem::ItemType::RectangleItemType:
        {
            positionableItem = paintableRectangle(position, parent);
            break;
        }
        case InterfaceQmlPaintableItem::ItemType::TextItemType:
        {
            positionableItem = paintableText(position, parent);
            break;
        }
        case InterfaceQmlPaintableItem::ItemType::ListItemType:
        {
            positionableItem = paintableList(position, parent);
            break;
        }
    }

    return positionableItem;
}

utils::own_qobject<PaintableRectangle> paintableRectangle(const data::PositionData& position,
                                                          PaintableItemListModel& parent)
{
    auto paintableRectangleItem = utils::make_qobject<PaintableRectangle>(parent);
    paintableRectangleItem->setPositionData(position);
    paintableRectangleItem->setColor("black");
    paintableRectangleItem->border()->setColor("black");
    paintableRectangleItem->setMode(Qt::SizeMode::RelativeSize);
    paintableRectangleItem->setParentList(&parent);
    return paintableRectangleItem;
}

utils::own_qobject<PaintableText> paintableText(const data::PositionData& position, PaintableItemListModel& parent)
{
    auto paintableTextItem = utils::make_qobject<PaintableText>(parent);
    paintableTextItem->setPositionData(position);
    paintableTextItem->setPointSize(12);
    paintableTextItem->setFontFamily("Arial");
    paintableTextItem->setColor("black");
    paintableTextItem->background()->setColor(QColor());
    paintableTextItem->background()->setMode(Qt::SizeMode::RelativeSize);
    paintableTextItem->border()->setColor("black");
    paintableTextItem->setParentList(&parent);
    return paintableTextItem;
}

utils::own_qobject<PaintableList> paintableList(const data::PositionData& position, PaintableItemListModel& parent)
{
    auto paintableListItem = utils::make_qobject<PaintableList>(parent);
    paintableListItem->setPositionData(position);
    return paintableListItem;
}

} // namespace painter::controller::DefaultPaintableItemsFactory

