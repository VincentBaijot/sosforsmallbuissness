#pragma once

#include <QRectF>

#include "Painter/controllers/PaintableItemListModel.hpp"
#include "PainterController_global.hpp"

namespace painter::data
{
struct PaintableItemListModelData;
}

namespace painter::controller
{
class AbstractListableItem;

/*!
 * @brief A list of paintable items but specialize to manipulate only listable item,
 * the paintable list type is not an AbstractListableItem to avoid list recursion
 */
class PAINTER_CONTROLLER_EXPORT ListableItemListModel : public PaintableItemListModel
{
    Q_OBJECT
  public:
    explicit ListableItemListModel(QObject* parent = nullptr);
    explicit ListableItemListModel(const painter::data::PaintableItemListModelData& newData,
                                   QObject* parent = nullptr);
    ~ListableItemListModel() override = default;

    painter::data::PaintableItemListModelData objectData() const;
    void setObjectData(const painter::data::PaintableItemListModelData& newData);

    Q_INVOKABLE InterfaceQmlPaintableItem* addPaintableItem(
      InterfaceQmlPaintableItem::ItemType paintableItemType,
      const QRectF& rect) override;
    Q_INVOKABLE void addPaintableItem(AbstractListableItem* paintableItem);
    void addPaintableItem(utils::own_qobject<AbstractListableItem>&& paintableItem);

    void insertPaintableItem(int index, utils::own_qobject<AbstractListableItem>&& paintableItem);
    void insertPaintableItem(int index, AbstractListableItem* paintableItem);

  private:
    Q_DISABLE_COPY_MOVE(ListableItemListModel)
};
}    // namespace painter::controller
