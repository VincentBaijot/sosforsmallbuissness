#include "InterfaceQmlPaintableItem.hpp"

namespace painter::controller
{
QString InterfaceQmlPaintableItem::itemName() const
{
    return _itemName;
}

void InterfaceQmlPaintableItem::setItemName(const QString& newItemName)
{
    if (_itemName == newItemName) return;
    _itemName = newItemName;
    emit itemNameChanged();
}
}    // namespace painter::controller
