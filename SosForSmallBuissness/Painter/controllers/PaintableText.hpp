#pragma once

#include <QTextOption>

#include "AbstractListableItem.hpp"
#include "PainterController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace painter::data
{
struct PositionData;
struct PaintableTextData;
}    // namespace painter::data

namespace painter::controller
{
class Color;
class PaintableRectangle;
class PaintableBorder;

class PAINTER_CONTROLLER_EXPORT PaintableText : public AbstractListableItem
{
    Q_OBJECT

    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged FINAL)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged FINAL)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged FINAL)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged FINAL)

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged FINAL)
    Q_PROPERTY(int opacity READ opacity WRITE setOpacity NOTIFY opacityChanged FINAL)
    Q_PROPERTY(bool colorIsValid READ colorIsValid NOTIFY colorChanged FINAL)

    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged FINAL)
    Q_PROPERTY(int flags READ flags WRITE setFlags NOTIFY flagsChanged FINAL)
    Q_PROPERTY(QTextOption::WrapMode wrapMode READ wrapMode NOTIFY flagsChanged FINAL)
    Q_PROPERTY(Qt::AlignmentFlag horizontalAlignment READ horizontalAlignment NOTIFY flagsChanged FINAL)
    Q_PROPERTY(Qt::AlignmentFlag verticalAlignment READ verticalAlignment NOTIFY flagsChanged FINAL)
    Q_PROPERTY(int pointSize READ pointSize WRITE setPointSize NOTIFY pointSizeChanged FINAL)
    Q_PROPERTY(QString fontFamily READ fontFamily WRITE setFontFamily NOTIFY fontFamilyChanged FINAL)
    Q_PROPERTY(painter::controller::PaintableRectangle* background READ background CONSTANT FINAL)
    Q_PROPERTY(painter::controller::PaintableBorder* border READ border CONSTANT FINAL)

    QML_ELEMENT
  public:
    enum class PaintableTextFlags
    {
        PaintableTextAlignLeft             = Qt::AlignLeft,
        PaintableTextAlignRight            = Qt::AlignRight,
        PaintableTextAlignHCenter          = Qt::AlignHCenter,
        PaintableTextAlignJustify          = Qt::AlignJustify,
        PaintableTextAlignTop              = Qt::AlignTop,
        PaintableTextAlignBottom           = Qt::AlignBottom,
        PaintableTextAlignVCenter          = Qt::AlignVCenter,
        PaintableTextAlignCenter           = Qt::AlignCenter,
        PaintableTextDontClip              = Qt::TextDontClip,
        PaintableTextSingleLine            = Qt::TextSingleLine,
        PaintableTextExpandTabs            = Qt::TextExpandTabs,
        PaintableTextShowMnemonic          = Qt::TextShowMnemonic,
        PaintableTextWordWrap              = Qt::TextWordWrap,
        PaintableTextIncludeTrailingSpaces = Qt::TextIncludeTrailingSpaces

    };
    Q_ENUM(PaintableTextFlags)

    explicit PaintableText(QObject* parent = nullptr);
    explicit PaintableText(const painter::data::PaintableTextData& data, QObject* parent = nullptr);
    ~PaintableText() override;

    void paint(QPainter* painter) const override;

    ItemType itemType() const override;

    qreal x() const;
    void setX(qreal newX);

    qreal y() const;
    void setY(qreal newY);

    qreal width() const;
    void setWidth(qreal newWidth);

    qreal height() const;
    void setHeight(qreal newHeight);

    painter::data::PositionData positionData() const;
    void setPositionData(const painter::data::PositionData& position);

    QColor color() const;
    void setColor(const QColor& newColor);
    bool colorIsValid() const;

    /*!
     * @brief opacity between 0 and 100
     */
    int opacity() const;
    void setOpacity(int newOpacity);
    int alpha() const;

    QString text() const;
    void setText(const QString& newText);

    int flags() const;
    void setFlags(int newFlags);
    QTextOption::WrapMode wrapMode() const;
    Qt::AlignmentFlag horizontalAlignment() const;
    Qt::AlignmentFlag verticalAlignment() const;

    int pointSize() const;
    void setPointSize(int newPointSize);

    QString fontFamily() const;
    void setFontFamily(const QString& newFontFamily);

    PaintableRectangle* background();
    PaintableBorder* border();

    painter::data::PaintableTextData objectData() const;
    void setObjectData(const painter::data::PaintableTextData& data);

  signals:
    void xChanged();
    void yChanged();
    void widthChanged();
    void heightChanged();
    void colorChanged();
    void opacityChanged();
    void textChanged();
    void flagsChanged();
    void pointSizeChanged();
    void fontFamilyChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableText)

    void _createInternalConnections() const;

    utils::own_qobject<Color> _color;
    QString _text;
    int _flags { 0 };
    int _pointSize { 0 };
    QString _fontFamily;

    utils::own_qobject<PaintableRectangle> _background;
};
}    // namespace painter::controller
