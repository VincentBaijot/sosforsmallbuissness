#include "Color.hpp"

namespace painter::controller
{
Color::Color(QObject* parent) : QObject { parent }
{
}

Color::Color(const painter::data::ColorData& newData, QObject* parent) : QObject { parent }, _data { newData }
{
}

QColor Color::color() const
{
    return _data.color;
}

void Color::setColor(const QColor& newColor)
{
    if (_data.color == newColor) { return; }
    _data.color = newColor;
    _data.color.setAlpha(alpha());
    emit colorChanged();
}

bool Color::colorIsValid() const
{
    return _data.color.isValid();
}

int Color::opacity() const
{
    return _data.opacity;
}

void Color::setOpacity(int newOpacity)
{
    if (_data.opacity == newOpacity) { return; }
    _data.opacity = newOpacity;
    emit opacityChanged();

    QColor currentColor = _data.color;
    currentColor.setAlpha(alpha());
    setColor(currentColor);
}

int Color::alpha() const
{
    return static_cast<int>(std::round(_data.opacity * 2.55));
}

painter::data::ColorData Color::objectData() const
{
    return _data;
}

void Color::setObjectData(const painter::data::ColorData& newData)
{
    setColor(newData.color);
    setOpacity(newData.opacity);
}
}    // namespace painter::controller
