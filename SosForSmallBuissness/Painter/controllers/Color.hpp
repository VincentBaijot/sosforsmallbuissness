#pragma once

#include <QColor>
#include <QObject>

#include "Painter/data/ColorData.hpp"

namespace painter::controller
{

class Color : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(int opacity READ opacity WRITE setOpacity NOTIFY opacityChanged)
    Q_PROPERTY(bool colorIsValid READ colorIsValid NOTIFY colorChanged)
  public:
    explicit Color(QObject* parent = nullptr);
    explicit Color(const painter::data::ColorData& newData, QObject* parent = nullptr);
    ~Color() override = default;

    QColor color() const;
    void setColor(const QColor& newColor);
    bool colorIsValid() const;

    /*!
     * @brief opacity between 0 and 100
     */
    int opacity() const;
    void setOpacity(int newOpacity);
    int alpha() const;

    painter::data::ColorData objectData() const;
    void setObjectData(const painter::data::ColorData& newData);

  signals:
    void colorChanged();
    void opacityChanged();

  private:
    Q_DISABLE_COPY_MOVE(Color)
    painter::data::ColorData _data;
};

}    // namespace painter::controller
