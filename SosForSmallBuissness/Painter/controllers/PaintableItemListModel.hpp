#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "InterfaceQmlPaintableItem.hpp"
#include "PainterController_global.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

namespace painter::controller
{
inline constexpr char paintableItemListModelHeaderData[] = "Paintable items";

class PAINTER_CONTROLLER_EXPORT PaintableItemListModel
    : public utils::SimpleObjectListModel<InterfaceQmlPaintableItem, paintableItemListModelHeaderData>
{
    Q_OBJECT

    QML_ELEMENT
    QML_UNCREATABLE("Abstract class")
  public:
    explicit PaintableItemListModel(QObject* parent = nullptr);
    explicit PaintableItemListModel(utils::OwnQObjectList<InterfaceQmlPaintableItem>&& paintableItemList,
                                    QObject* parent = nullptr);
    ~PaintableItemListModel() override = default;

    Q_INVOKABLE virtual painter::controller::InterfaceQmlPaintableItem* addPaintableItem(
      painter::controller::InterfaceQmlPaintableItem::ItemType paintableItemType,
      const QRectF& rect) = 0;
    Q_INVOKABLE void deletePaintableItem(const InterfaceQmlPaintableItem* paintableItem);
    Q_INVOKABLE void deletePaintableItemAt(int index);
    Q_INVOKABLE void removePaintableItem(const InterfaceQmlPaintableItem* paintableItem);
    Q_INVOKABLE void removePaintableItemAt(int index);
    Q_INVOKABLE void clearPaintableItemList();

  protected:
    void _addPaintableItem(utils::own_qobject<InterfaceQmlPaintableItem>&& paintableItem);
    void _insertPaintableItem(int index, utils::own_qobject<InterfaceQmlPaintableItem>&& paintableItem);
    void _setPaintableItemList(utils::OwnQObjectList<InterfaceQmlPaintableItem>&& paintableItemList);

  private:
    Q_DISABLE_COPY_MOVE(PaintableItemListModel)
};
}    // namespace painter::controller
