#pragma once

#include "InterfaceQmlPaintableItem.hpp"
#include "PainterController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace painter::data
{
struct PositionData;
}

namespace painter::controller
{
class PaintableRectangle;
class PaintableText;
class PaintableList;
class PaintableItemListModel;

namespace DefaultPaintableItemsFactory
{
PAINTER_CONTROLLER_EXPORT utils::own_qobject<InterfaceQmlPaintableItem> paintableQmlItem(
  InterfaceQmlPaintableItem::ItemType itemType,
  const data::PositionData& position,
  PaintableItemListModel& parent);

utils::own_qobject<PaintableRectangle> paintableRectangle(const data::PositionData& position,
                                                          PaintableItemListModel& parent);
utils::own_qobject<PaintableText> paintableText(const data::PositionData& position,
                                                PaintableItemListModel& parent);
utils::own_qobject<PaintableList> paintableList(const data::PositionData& position,
                                                PaintableItemListModel& parent);

}    // namespace DefaultPaintableItemsFactory
}    // namespace painter::controller
