#pragma once

#include <QObject>

Q_MOC_INCLUDE("PaintableItemListModel.hpp")
#include "InterfaceQmlPaintableItem.hpp"
#include "PainterController_global.hpp"

namespace painter::controller
{
class PaintableItemListModel;

class PAINTER_CONTROLLER_EXPORT AbstractListableItem : public InterfaceQmlPaintableItem
{
    Q_OBJECT

    Q_PROPERTY(painter::controller::PaintableItemListModel* parentList READ parentList WRITE setParentList NOTIFY
                 parentListChanged)

    QML_ELEMENT
    QML_UNCREATABLE("AbstractListableItem is an abstract base class of paintable items which can be listed.")
  public:
    explicit AbstractListableItem(QObject* parent = nullptr);
    ~AbstractListableItem() override = default;

    PaintableItemListModel* parentList() const;
    void setParentList(PaintableItemListModel* newParentList);

  signals:
    void parentListChanged();

  private:
    Q_DISABLE_COPY_MOVE(AbstractListableItem)

    PaintableItemListModel* _parentList { nullptr };
};
}    // namespace painter::controller
