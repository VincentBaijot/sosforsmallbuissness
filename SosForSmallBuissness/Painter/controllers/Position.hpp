#pragma once

#include <QObject>

namespace painter::data
{
struct PositionData;
}

namespace painter::controller
{
class Position : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
  public:
    explicit Position(QObject* parent = nullptr);
    explicit Position(const painter::data::PositionData& positionData, QObject* parent = nullptr);
    ~Position() override = default;

    qreal x() const;
    void setX(qreal newX);

    qreal y() const;
    void setY(qreal newY);

    qreal width() const;
    void setWidth(qreal newWidth);

    qreal height() const;
    void setHeight(qreal newHeight);

    painter::data::PositionData positionData() const;
    void setPositionData(const painter::data::PositionData& positionData);

    /*!
     * @brief objectData alias on positionData for uniformity with other classes
     * @return the position data
     */
    painter::data::PositionData objectData() const;

    /*!
     * @brief setObjectData alias on setPositionData for uniformity with other classes
     * @param newData the new position data
     */
    void setObjectData(const painter::data::PositionData& newData);

  signals:
    void xChanged();
    void yChanged();
    void widthChanged();
    void heightChanged();

  private:
    Q_DISABLE_COPY_MOVE(Position)

    qreal _x { 0 };
    qreal _y { 0 };
    qreal _width { 0 };
    qreal _height { 0 };
};
}    // namespace painter::controller
