#include "RectanglePosition.hpp"

#include "data/RectanglePositionData.hpp"

namespace painter::controller
{
RectanglePosition::RectanglePosition(QObject* parent) : Position(parent)
{
    _createInternalConnections();
}

RectanglePosition::RectanglePosition(const painter::data::RectanglePositionData& data, QObject* parent)
    : Position(data.position, parent), _radius { data.radius }, _mode { data.mode }
{
    _createInternalConnections();
}

qreal RectanglePosition::radius() const
{
    return _radius;
}

void RectanglePosition::setRadius(qreal newRadius)
{
    if (qFuzzyCompare(_radius, newRadius)) { return; }
    _radius = newRadius;
    emit radiusChanged();
}

Qt::SizeMode RectanglePosition::mode() const
{
    return _mode;
}

void RectanglePosition::setMode(Qt::SizeMode newMode)
{
    if (_mode == newMode) { return; }
    _mode = newMode;
    emit modeChanged();
}

qreal RectanglePosition::calculatedRadius() const
{
    // Radius should never exceeds half of the width or half of the height
    qreal calcRadius = (mode() == Qt::SizeMode::AbsoluteSize ? 1 : qMax(width(), height()) / 200) * radius();
    calcRadius       = qFloor(qMin(qMin(width(), height()) * 0.5, calcRadius));
    return calcRadius;
}

QPainterPath RectanglePosition::roundedRectanglePath(const QRectF& roundedRectangleContainer,
                                                     qreal radiusRectangleSize) const
{
    QPainterPath painterPath;

    QRectF topLeftArc {
        roundedRectangleContainer.left(), roundedRectangleContainer.top(), radiusRectangleSize, radiusRectangleSize
    };

    painterPath.moveTo(QPointF(topLeftArc.left(), topLeftArc.center().y()));
    painterPath.arcTo(topLeftArc, 180, -90);

    QRectF topRigthArc { roundedRectangleContainer.right() - radiusRectangleSize,
                         roundedRectangleContainer.top(),
                         radiusRectangleSize,
                         radiusRectangleSize };

    painterPath.arcTo(topRigthArc, 90, -90);

    QRectF bottomRigthArc { roundedRectangleContainer.right() - radiusRectangleSize,
                            roundedRectangleContainer.bottom() - radiusRectangleSize,
                            radiusRectangleSize,
                            radiusRectangleSize };

    painterPath.arcTo(bottomRigthArc, 0, -90);

    QRectF bottomLeftArc { roundedRectangleContainer.left(),
                           roundedRectangleContainer.bottom() - radiusRectangleSize,
                           radiusRectangleSize,
                           radiusRectangleSize };

    painterPath.arcTo(bottomLeftArc, 270, -90);

    painterPath.lineTo(QPointF(topLeftArc.left(), topLeftArc.center().y()));

    return painterPath;
}

painter::data::RectanglePositionData RectanglePosition::objectData() const
{
    painter::data::RectanglePositionData data;
    data.position = Position::objectData();
    data.mode     = _mode;
    data.radius   = _radius;

    return data;
}

void RectanglePosition::setObjectData(const painter::data::RectanglePositionData& data)
{
    Position::setObjectData(data.position);
    setMode(data.mode);
    setRadius(data.radius);
}

void RectanglePosition::_createInternalConnections() const
{
    QObject::connect(this, &Position::widthChanged, this, &RectanglePosition::calculatedRadiusChanged);
    QObject::connect(this, &Position::heightChanged, this, &RectanglePosition::calculatedRadiusChanged);
    QObject::connect(this, &RectanglePosition::modeChanged, this, &RectanglePosition::calculatedRadiusChanged);
    QObject::connect(this, &RectanglePosition::radiusChanged, this, &RectanglePosition::calculatedRadiusChanged);
}
}    // namespace painter::controller
