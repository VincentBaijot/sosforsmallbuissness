#include "PaintableItemDataHandler.hpp"

#include <QDebug>

#include "Constants.hpp"
#include "PaintableRectangle.hpp"
#include "PaintableText.hpp"
#include "data/PaintableItemListModelData.hpp"
#include "data/VariantListableItemData.hpp"

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

namespace painter::controller
{
painter::data::VariantListableItemData extractData(const AbstractListableItem* const item)
{
    if (item)
    {
        if (const PaintableRectangle* const rectangle = qobject_cast<const PaintableRectangle* const>(item))
        {
            return rectangle->objectData();
        }
        else if (const PaintableText* const text = qobject_cast<const PaintableText* const>(item))
        {
            return text->objectData();
        }
        else
        {
            qCWarning(painterCategory) << "Try to extract data of unknown InterfaceQmlPaintableItem type "
                                       << item->objectName();
            return painter::data::VariantListableItemData();
        }
    }

    qCWarning(painterCategory) << "Try to extract data of null item";

    return painter::data::VariantListableItemData();
}

utils::own_qobject<AbstractListableItem> generateItem(painter::data::VariantListableItemData itemDataVariant,
                                                      QObject& parent)
{
    return std::visit(
      overloaded {
        [&parent](const painter::data::PaintableRectangleData& paintableRectangleData)
        {
            return utils::own_qobject<AbstractListableItem>(
              utils::make_qobject<PaintableRectangle>(parent, paintableRectangleData));
        },
        [&parent](const painter::data::PaintableTextData& paintableTextData)
        {
            return utils::own_qobject<AbstractListableItem>(
              utils::make_qobject<PaintableText>(parent, paintableTextData));
        },
      },
      itemDataVariant);
}

utils::OwnQObjectList<InterfaceQmlPaintableItem> listFromData(
  const painter::data::PaintableItemListModelData& data,
  QObject& parent)
{
    utils::OwnQObjectList<InterfaceQmlPaintableItem> paintableItemList;
    for (const painter::data::VariantListableItemData& variantData : data.paintableItemList)
    {
        auto abstractListableItem =
          utils::own_qobject<InterfaceQmlPaintableItem>(generateItem(variantData, parent));
        if (abstractListableItem) { paintableItemList.emplace_back(std::move(abstractListableItem)); }
    }

    return paintableItemList;
}

}    // namespace painter::controller
