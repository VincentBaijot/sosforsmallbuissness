#include "PaintableText.hpp"

#include <QPainter>

#include "Color.hpp"
#include "Constants.hpp"
#include "PaintableBorder.hpp"
#include "PaintableRectangle.hpp"
#include "data/PaintableTextData.hpp"

namespace painter::controller
{
PaintableText::PaintableText(QObject* parent)
    : AbstractListableItem(parent),
      _color { utils::make_qobject<Color>(*this) },
      _background { utils::make_qobject<PaintableRectangle>(*this) }
{
    _createInternalConnections();
}

PaintableText::PaintableText(const painter::data::PaintableTextData& data, QObject* parent)
    : AbstractListableItem(parent),
      _color { utils::make_qobject<Color>(*this, data.color) },
      _text { data.text },
      _flags { data.flags },
      _pointSize { data.pointSize },
      _fontFamily { data.fontFamily },
      _background { utils::make_qobject<PaintableRectangle>(*this, data.background) }
{
    _createInternalConnections();
}

PaintableText::~PaintableText() = default;

void PaintableText::paint(QPainter* painter) const
{
    _background->paint(painter);

    if (!_text.isEmpty())
    {
        QPen pen;
        pen.setColor(color());
        painter->setPen(pen);

        QFont font = painter->font();
        font.setPointSize(_pointSize);
        font.setFamily(_fontFamily);
        painter->setFont(font);

        QRectF position { positionData().x, positionData().y, positionData().width, positionData().height };
        qreal borderWidth(_background->border()->borderWidth());
        position = position.marginsRemoved(QMarginsF(borderWidth, borderWidth, borderWidth, borderWidth));
        painter->drawText(position, _flags, _text);
    }
}

InterfaceQmlPaintableItem::ItemType PaintableText::itemType() const
{
    return ItemType::TextItemType;
}

qreal PaintableText::x() const
{
    return _background->x();
}

void PaintableText::setX(qreal newX)
{
    qCDebug(painterCategory) << "Text set x " << newX;
    _background->setX(newX);
}

qreal PaintableText::y() const
{
    return _background->y();
}

void PaintableText::setY(qreal newY)
{
    qCDebug(painterCategory) << "Text set y " << newY;
    _background->setY(newY);
}

qreal PaintableText::width() const
{
    return _background->width();
}

void PaintableText::setWidth(qreal newWidth)
{
    qCDebug(painterCategory) << "Text set width " << newWidth;
    _background->setWidth(newWidth);
}

qreal PaintableText::height() const
{
    return _background->height();
}

void PaintableText::setHeight(qreal newHeight)
{
    qCDebug(painterCategory) << "Text set height " << newHeight;
    _background->setHeight(newHeight);
}

painter::data::PositionData PaintableText::positionData() const
{
    return _background->positionData();
}

void PaintableText::setPositionData(const painter::data::PositionData& position)
{
    qCDebug(painterCategory) << "Text set position " << position;
    _background->setPositionData(position);
}

QColor PaintableText::color() const
{
    return _color->color();
}

void PaintableText::setColor(const QColor& newColor)
{
    qCDebug(painterCategory) << "Text set color " << newColor;
    _color->setColor(newColor);
}

bool PaintableText::colorIsValid() const
{
    return _color->colorIsValid();
}

int PaintableText::opacity() const
{
    return _background->opacity();
}

void PaintableText::setOpacity(int newOpacity)
{
    qCDebug(painterCategory) << "Text set opacity " << newOpacity;
    _background->setOpacity(newOpacity);
    _color->setOpacity(newOpacity);
}

int PaintableText::alpha() const
{
    return _color->alpha();
}

QString PaintableText::text() const
{
    return _text;
}

void PaintableText::setText(const QString& newText)
{
    if (_text == newText) return;
    qCDebug(painterCategory) << "Text set text " << newText;
    _text = newText;
    emit textChanged();
}

int PaintableText::flags() const
{
    return _flags;
}

void PaintableText::setFlags(int newFlags)
{
    if (_flags == newFlags) return;
    qCDebug(painterCategory) << "Text set flags " << newFlags;
    _flags = newFlags;
    emit flagsChanged();
}

QTextOption::WrapMode PaintableText::wrapMode() const
{
    QFlags<PaintableTextFlags> textFlags(_flags);

    return textFlags.testFlag(PaintableTextFlags::PaintableTextWordWrap) ? QTextOption::WordWrap
                                                                         : QTextOption::NoWrap;
}

Qt::AlignmentFlag PaintableText::horizontalAlignment() const
{
    QFlags<PaintableTextFlags> textFlags(_flags);

    if (textFlags.testFlag(PaintableTextFlags::PaintableTextAlignJustify))
    {
        return Qt::AlignmentFlag::AlignJustify;
    }

    if (textFlags.testFlag(PaintableTextFlags::PaintableTextAlignHCenter))
    {
        return Qt::AlignmentFlag::AlignHCenter;
    }

    if (textFlags.testFlag(PaintableTextFlags::PaintableTextAlignRight)) { return Qt::AlignmentFlag::AlignRight; }
    return Qt::AlignmentFlag::AlignLeft;
}

Qt::AlignmentFlag PaintableText::verticalAlignment() const
{
    QFlags<PaintableTextFlags> textFlags(_flags);

    if (textFlags.testFlag(PaintableTextFlags::PaintableTextAlignVCenter))
    {
        return Qt::AlignmentFlag::AlignVCenter;
    }

    if (textFlags.testFlag(PaintableTextFlags::PaintableTextAlignBottom))
    {
        return Qt::AlignmentFlag::AlignBottom;
    }
    return Qt::AlignmentFlag::AlignTop;
}

int PaintableText::pointSize() const
{
    return _pointSize;
}

void PaintableText::setPointSize(int newPointSize)
{
    if (_pointSize == newPointSize) { return; }
    qCDebug(painterCategory) << "Text set point size " << newPointSize;
    _pointSize = newPointSize;
    emit pointSizeChanged();
}

QString PaintableText::fontFamily() const
{
    return _fontFamily;
}

void PaintableText::setFontFamily(const QString& newFontFamily)
{
    if (_fontFamily == newFontFamily) { return; }
    qCDebug(painterCategory) << "Text set font family " << newFontFamily;
    _fontFamily = newFontFamily;
    emit fontFamilyChanged();
}

PaintableRectangle* PaintableText::background()
{
    return _background.get();
}

PaintableBorder* PaintableText::border()
{
    return _background->border();
}

painter::data::PaintableTextData PaintableText::objectData() const
{
    painter::data::PaintableTextData data;
    data.color      = _color->objectData();
    data.background = _background->objectData();
    data.text       = _text;
    data.pointSize  = _pointSize;
    data.flags      = _flags;
    data.fontFamily = _fontFamily;

    return data;
}

void PaintableText::setObjectData(const painter::data::PaintableTextData& data)
{
    _color->setObjectData(data.color);
    _background->setObjectData(data.background);
    setText(data.text);
    setFlags(data.flags);
    setPointSize(data.pointSize);
    setFontFamily(data.fontFamily);
}

void PaintableText::_createInternalConnections() const
{
    QObject::connect(_background.get(), &PaintableRectangle::xChanged, this, &PaintableText::xChanged);
    QObject::connect(_background.get(), &PaintableRectangle::yChanged, this, &PaintableText::yChanged);
    QObject::connect(_background.get(), &PaintableRectangle::widthChanged, this, &PaintableText::widthChanged);
    QObject::connect(_background.get(), &PaintableRectangle::heightChanged, this, &PaintableText::heightChanged);
    QObject::connect(_color.get(), &Color::colorChanged, this, &PaintableText::colorChanged);
    QObject::connect(_color.get(), &Color::opacityChanged, this, &PaintableText::opacityChanged);
}

}    // namespace painter::controller
