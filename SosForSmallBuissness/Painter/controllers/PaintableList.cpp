#include "PaintableList.hpp"

#include "Constants.hpp"
#include "ListableItemListModel.hpp"
#include "Position.hpp"
#include "data/PaintableListData.hpp"

static_assert(static_cast<int>(painter::controller::PaintableList::RepetitionDirection::NoRepetition)
                == static_cast<int>(painter::data::PaintableListData::RepetitionDirection::NoRepetition),
              "Enums RepetitionDirection in PaintableList and PaintableListData must be in sync for value : "
              "NoRepetitionDirection");
static_assert(static_cast<int>(painter::controller::PaintableList::RepetitionDirection::BottomRepetition)
                == static_cast<int>(painter::data::PaintableListData::RepetitionDirection::BottomRepetition),
              "Enums RepetitionDirection in PaintableList and PaintableListData must be in sync for value : "
              "BottomRepetition");
static_assert(static_cast<int>(painter::controller::PaintableList::RepetitionDirection::TopRepetition)
                == static_cast<int>(painter::data::PaintableListData::RepetitionDirection::TopRepetition),
              "Enums RepetitionDirection in PaintableList and PaintableListData must be in sync for value : "
              "TopRepetition");
static_assert(static_cast<int>(painter::controller::PaintableList::RepetitionDirection::LeftRepetition)
                == static_cast<int>(painter::data::PaintableListData::RepetitionDirection::LeftRepetition),
              "Enums RepetitionDirection in PaintableList and PaintableListData must be in sync for value : "
              "LeftRepetition");
static_assert(static_cast<int>(painter::controller::PaintableList::RepetitionDirection::RightRepetition)
                == static_cast<int>(painter::data::PaintableListData::RepetitionDirection::RightRepetition),
              "Enums RepetitionDirection in PaintableList and PaintableListData must be in sync for value : "
              "RightRepetition");

namespace painter::controller
{
PaintableList::PaintableList(QObject* parent)
    : InterfaceQmlPaintableItem { parent },
      _abstractPosition { utils::make_qobject<Position>(*this) },
      _listableItemListModel { utils::make_qobject<ListableItemListModel>(*this) }
{
    _createInternalConnections();
}

PaintableList::PaintableList(const painter::data::PaintableListData& data, QObject* parent)
    : InterfaceQmlPaintableItem { parent },
      _abstractPosition { utils::make_qobject<Position>(*this, data.position) },
      _listableItemListModel { utils::make_qobject<ListableItemListModel>(*this, data.paintableItemListModel) },
      _primaryRepetitionDirection { RepetitionDirection(static_cast<int>(data.primaryRepetitionDirection)) },
      _secondaryRepetitionDirection { RepetitionDirection(static_cast<int>(data.secondaryRepetitionDirection)) }
{
    _createInternalConnections();
}

PaintableList::~PaintableList() = default;

void PaintableList::paint(QPainter* painter) const
{
    // Empty method of interface the painting is handle by the elements of the list
}

InterfaceQmlPaintableItem::ItemType PaintableList::itemType() const
{
    return InterfaceQmlPaintableItem::ItemType::ListItemType;
}

qreal PaintableList::x() const
{
    return _abstractPosition->x();
}

void PaintableList::setX(qreal newX)
{
    qCDebug(painterCategory) << "Rectangle set x " << newX;
    _abstractPosition->setX(newX);
}

qreal PaintableList::y() const
{
    return _abstractPosition->y();
}

void PaintableList::setY(qreal newY)
{
    qCDebug(painterCategory) << "Rectangle set y " << newY;
    _abstractPosition->setY(newY);
}

qreal PaintableList::width() const
{
    return _abstractPosition->width();
}

void PaintableList::setWidth(qreal newWidth)
{
    qCDebug(painterCategory) << "Rectangle set width " << newWidth;
    _abstractPosition->setWidth(newWidth);
}

qreal PaintableList::height() const
{
    return _abstractPosition->height();
}

void PaintableList::setHeight(qreal newHeight)
{
    qCDebug(painterCategory) << "Rectangle set height " << newHeight;
    _abstractPosition->setHeight(newHeight);
}

painter::data::PositionData PaintableList::positionData() const
{
    return _abstractPosition->positionData();
}

void PaintableList::setPositionData(const painter::data::PositionData& position)
{
    qCDebug(painterCategory) << "Rectangle set position " << position;
    _abstractPosition->setPositionData(position);
}

PaintableList::RepetitionDirection PaintableList::primaryRepetitionDirection() const
{
    return _primaryRepetitionDirection;
}

void PaintableList::setPrimaryRepetitionDirection(RepetitionDirection newPrimaryRepetitionDirection)
{
    if (_primaryRepetitionDirection == newPrimaryRepetitionDirection) { return; }
    qCDebug(painterCategory) << "Rectangle set primary repetition direction " << newPrimaryRepetitionDirection;
    _primaryRepetitionDirection = newPrimaryRepetitionDirection;
    emit primaryRepetitionDirectionChanged();
}

PaintableList::RepetitionDirection PaintableList::secondaryRepetitionDirection() const
{
    return _secondaryRepetitionDirection;
}

void PaintableList::setSecondaryRepetitionDirection(RepetitionDirection newSecondaryRepetitionDirection)
{
    if (_secondaryRepetitionDirection == newSecondaryRepetitionDirection) { return; }
    qCDebug(painterCategory) << "Rectangle set secondary repetition direction " << newSecondaryRepetitionDirection;
    _secondaryRepetitionDirection = newSecondaryRepetitionDirection;
    emit secondaryRepetitionDirectionChanged();
}

QStringList PaintableList::repetitionDirectionNames() const
{
    static_assert(static_cast<int>(RepetitionDirection::NoRepetition) == 0
                    && static_cast<int>(RepetitionDirection::BottomRepetition) == 1
                    && static_cast<int>(RepetitionDirection::RightRepetition) == 2
                    && static_cast<int>(RepetitionDirection::TopRepetition) == 3
                    && static_cast<int>(RepetitionDirection::LeftRepetition) == 4,
                  "Repetition direction enum order is not correct");
    return QStringList { QObject::tr("No repetition"),
                         QObject::tr("Bottom"),
                         QObject::tr("Right"),
                         QObject::tr("Top"),
                         QObject::tr("Left") };
}

ListableItemListModel* PaintableList::listableItemListModel() const
{
    return _listableItemListModel.get();
}

painter::data::PaintableListData PaintableList::objectData() const
{
    painter::data::PaintableListData data;
    data.position               = positionData();
    data.paintableItemListModel = _listableItemListModel->objectData();
    data.primaryRepetitionDirection =
      painter::data::PaintableListData::RepetitionDirection(static_cast<int>(_primaryRepetitionDirection));
    data.secondaryRepetitionDirection =
      painter::data::PaintableListData::RepetitionDirection(static_cast<int>(_secondaryRepetitionDirection));
    return data;
}

void PaintableList::setObjectData(const painter::data::PaintableListData& newData)
{
    setPositionData(newData.position);
    _listableItemListModel->setObjectData(newData.paintableItemListModel);
    setPrimaryRepetitionDirection(RepetitionDirection(static_cast<int>(newData.primaryRepetitionDirection)));
    setSecondaryRepetitionDirection(RepetitionDirection(static_cast<int>(newData.secondaryRepetitionDirection)));
}

void PaintableList::_createInternalConnections() const
{
    QObject::connect(_abstractPosition.get(), &Position::xChanged, this, &PaintableList::xChanged);
    QObject::connect(_abstractPosition.get(), &Position::yChanged, this, &PaintableList::yChanged);
    QObject::connect(_abstractPosition.get(), &Position::widthChanged, this, &PaintableList::widthChanged);
    QObject::connect(_abstractPosition.get(), &Position::heightChanged, this, &PaintableList::heightChanged);
}

}    // namespace painter::controller
