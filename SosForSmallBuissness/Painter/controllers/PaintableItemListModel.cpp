#include "PaintableItemListModel.hpp"

#include "Constants.hpp"

namespace painter::controller
{
PaintableItemListModel::PaintableItemListModel(QObject* parent)
    : utils::SimpleObjectListModel<InterfaceQmlPaintableItem, paintableItemListModelHeaderData> { parent }
{
}

PaintableItemListModel::PaintableItemListModel(
  utils::OwnQObjectList<InterfaceQmlPaintableItem>&& paintableItemList,
  QObject* parent)
    : SimpleObjectListModel<InterfaceQmlPaintableItem, paintableItemListModelHeaderData> { parent }
{
    setObjects(std::move(paintableItemList));
    for (const utils::own_qobject<InterfaceQmlPaintableItem>& item : objects())
    {
        if (item) { item->setParent(this); }
    }
}

void PaintableItemListModel::deletePaintableItem(const InterfaceQmlPaintableItem* paintableItem)
{
    deletePaintableItemAt(indexOf(paintableItem));
}

void PaintableItemListModel::deletePaintableItemAt(int index)
{
    if (index >= 0 && index < objects().size())
    {
        beginRemoveRows(QModelIndex(), index, index);
        if (const utils::own_qobject<InterfaceQmlPaintableItem>& item = objects().at(index))
        {
            item->deleteLater();
        }
        objects().erase(objects().begin() + index);
        endRemoveRows();
    }
}

void PaintableItemListModel::_addPaintableItem(utils::own_qobject<InterfaceQmlPaintableItem>&& paintableItem)
{
    if (!paintableItem) { return; }

    if (objects().contains(paintableItem.get()))
    {
        qCWarning(painterCategory)
          << "Try to add a paintable item which is already in the current list, this is not expected and abord";
        return;
    }

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    paintableItem->setParent(this);
    objects().emplace_back(std::move(paintableItem));
    endInsertRows();
}

void PaintableItemListModel::_insertPaintableItem(int index,
                                                  utils::own_qobject<InterfaceQmlPaintableItem>&& paintableItem)
{
    if (!paintableItem) { return; }

    if (objects().contains(paintableItem.get()))
    {
        qCWarning(painterCategory)
          << "Try to add a paintable item which is already in the current list, this is not expected and abord";
        return;
    }

    beginInsertRows(QModelIndex(), index, index);
    paintableItem->setParent(this);
    objects().insert(std::cbegin(objects()) + index, std::move(paintableItem));
    endInsertRows();
}

void PaintableItemListModel::_setPaintableItemList(
  utils::OwnQObjectList<InterfaceQmlPaintableItem>&& paintableItemList)
{
    if (objects() == paintableItemList) { return; }

    beginResetModel();

    deleteAllObjects();
    objects().clear();

    setObjects(std::move(paintableItemList));
    for (const utils::own_qobject<InterfaceQmlPaintableItem>& item : objects())
    {
        if (item) { item->setParent(this); }
    }

    endResetModel();
}

void PaintableItemListModel::removePaintableItemAt(int index)
{
    if (index >= 0 && index < objects().size())
    {
        beginRemoveRows(QModelIndex(), index, index);
        objects().erase(std::cbegin(objects()) + index);
        endRemoveRows();
    }
}

void PaintableItemListModel::removePaintableItem(const InterfaceQmlPaintableItem* paintableItem)
{
    int indexOfItem = indexOf(paintableItem);
    if (indexOfItem == -1)
    {
        qCWarning(painterCategory) << "Cannot remove item, it is not found";
        return;
    }
    qCDebug(painterCategory) << "Remove paintable item at " << indexOfItem;
    removePaintableItemAt(indexOfItem);
}

void PaintableItemListModel::clearPaintableItemList()
{
    beginResetModel();
    deleteAllObjects();
    objects().clear();
    endResetModel();
}

}    // namespace painter::controller
