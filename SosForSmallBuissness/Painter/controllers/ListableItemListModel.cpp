#include "ListableItemListModel.hpp"

#include <QRectF>

#include "AbstractListableItem.hpp"
#include "Constants.hpp"
#include "DefaultPaintableItemsFactory.hpp"
#include "PaintableItemDataHandler.hpp"
#include "data/PaintableItemListModelData.hpp"

namespace painter::controller
{
ListableItemListModel::ListableItemListModel(QObject* parent) : PaintableItemListModel { parent }
{
}

ListableItemListModel::ListableItemListModel(const painter::data::PaintableItemListModelData& newData,
                                             QObject* parent)
    : PaintableItemListModel { parent }
{
    _setPaintableItemList(listFromData(newData, *this));

    for (const utils::own_qobject<InterfaceQmlPaintableItem>& item : objects())
    {
        auto* listableItem = qobject_cast<AbstractListableItem*>(item.get());
        Q_ASSERT_X(listableItem,
                   "ListableItemListModel::ListableItemListModel",
                   "All the items in ListableItemListModel must be AbstractListableItem");
        listableItem->setParentList(this);
    }
}

painter::data::PaintableItemListModelData ListableItemListModel::objectData() const
{
    painter::data::PaintableItemListModelData data;

    for (int rowIndex = 0; rowIndex < rowCount(); ++rowIndex)
    {
        const InterfaceQmlPaintableItem* const item = at(rowIndex);
        if (const auto* const listableItem = qobject_cast<const AbstractListableItem*>(item))
        {
            painter::data::VariantListableItemData variantItemData = extractData(listableItem);
            data.paintableItemList.append(variantItemData);
        }
    }

    return data;
}

void ListableItemListModel::setObjectData(const painter::data::PaintableItemListModelData& newData)
{
    if (objectData() != newData) { _setPaintableItemList(listFromData(newData, *this)); }

    for (const utils::own_qobject<InterfaceQmlPaintableItem>& item : objects())
    {
        if (auto* listableItem = qobject_cast<AbstractListableItem*>(item.get()))
        {
            listableItem->setParentList(this);
        }
    }
}

InterfaceQmlPaintableItem* ListableItemListModel::addPaintableItem(
  InterfaceQmlPaintableItem::ItemType paintableItemType,
  const QRectF& rect)
{
    if (rect.width() == 0 && rect.height() == 0) { return nullptr; }

    auto listableItem = DefaultPaintableItemsFactory::paintableQmlItem(
      paintableItemType,
      painter::data::PositionData { rect.x(), rect.y(), rect.width(), rect.height() },
      *this);

    if (listableItem)
    {
        qobject_cast<AbstractListableItem*>(listableItem.get())->setParentList(this);
        _addPaintableItem(std::move(listableItem));

        qCDebug(painterCategory) << "Add item " << paintableItemType << " : QRectF(" << rect.x() << "," << rect.y()
                                 << "," << rect.width() << "," << rect.height() << ")";
    }
    else { qCWarning(painterCategory) << "Cannot add item of type " << paintableItemType; }

    return listableItem.get();
}

void ListableItemListModel::addPaintableItem(AbstractListableItem* paintableItem)
{
    if (paintableItem)
    {
        _addPaintableItem(utils::own_qobject<InterfaceQmlPaintableItem>(paintableItem));
        paintableItem->setParentList(this);
    }
}

void ListableItemListModel::addPaintableItem(utils::own_qobject<AbstractListableItem>&& paintableItem)
{
    if (paintableItem)
    {
        paintableItem->setParentList(this);
        _addPaintableItem(utils::own_qobject<InterfaceQmlPaintableItem>(std::move(paintableItem)));
    }
}

void ListableItemListModel::insertPaintableItem(int index,
                                                utils::own_qobject<AbstractListableItem>&& paintableItem)
{
    _insertPaintableItem(index, utils::own_qobject<InterfaceQmlPaintableItem>(std::move(paintableItem)));
}

void ListableItemListModel::insertPaintableItem(int index, AbstractListableItem* paintableItem)
{
    _insertPaintableItem(index, utils::own_qobject<InterfaceQmlPaintableItem>(paintableItem));
}

}    // namespace painter::controller
