#pragma once

#include <QList>

#include "Utils/OwnQObjectList.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace painter::data
{
struct PaintableRectangleData;
struct PaintableTextData;
using VariantListableItemData = std::variant<PaintableRectangleData, PaintableTextData>;
struct PaintableItemListModelData;
}    // namespace painter::data

class QObject;

namespace painter::controller
{
class AbstractListableItem;
class InterfaceQmlPaintableItem;

painter::data::VariantListableItemData extractData(const AbstractListableItem* const item);
utils::own_qobject<AbstractListableItem> generateItem(
  painter::data::VariantListableItemData itemDataVariant,
  QObject* parent);
utils::OwnQObjectList<InterfaceQmlPaintableItem> listFromData(
  const painter::data::PaintableItemListModelData& data,
  QObject& parent);
}    // namespace painter::controller
