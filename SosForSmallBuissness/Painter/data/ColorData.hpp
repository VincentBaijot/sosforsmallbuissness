#pragma once

#include <QColor>
#include <QDebug>

namespace painter::data
{
struct ColorData
{
    int opacity { 100 };
    QColor color;
    bool operator==(const ColorData& colorA) const = default;
};
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput, const painter::data::ColorData& color)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "ColorData{" << color.opacity << ','
                                 << (color.color.isValid() ? color.color.name(QColor::HexArgb) : "") << '}';
}
