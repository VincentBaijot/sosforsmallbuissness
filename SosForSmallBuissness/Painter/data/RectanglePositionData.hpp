#pragma once

#include <QDebug>
#include <Qt>

#include "PositionData.hpp"

namespace painter::data
{
struct RectanglePositionData
{
    PositionData position;
    qreal radius { 0 };
    Qt::SizeMode mode { Qt::AbsoluteSize };
};

inline bool operator==(const RectanglePositionData& positionA, const RectanglePositionData& positionB)
{
    return positionA.position == positionB.position && qFuzzyCompare(positionA.radius, positionB.radius)
           && positionA.mode == positionB.mode;
}
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput, const painter::data::RectanglePositionData& rectanglePosition)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "RectanglePositionData{" << rectanglePosition.position << ','
                                 << rectanglePosition.radius << ',' << rectanglePosition.mode << '}';
}
