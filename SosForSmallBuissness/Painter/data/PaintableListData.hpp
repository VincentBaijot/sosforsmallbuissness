#pragma once

#include <QDebug>

#include "PaintableItemListModelData.hpp"
#include "PositionData.hpp"

namespace painter::data
{
struct PaintableListData
{
    enum class RepetitionDirection
    {
        NoRepetition,
        BottomRepetition,
        RightRepetition,
        TopRepetition,
        LeftRepetition
    };

    PositionData position;
    RepetitionDirection primaryRepetitionDirection { RepetitionDirection::NoRepetition };
    RepetitionDirection secondaryRepetitionDirection { RepetitionDirection::NoRepetition };
    PaintableItemListModelData paintableItemListModel;
    bool operator==(const PaintableListData& list) const = default;
};
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput,
                         const painter::data::PaintableListData::RepetitionDirection& repetitionDirection)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PaintableListData::RepetitionDirection("
                                 << static_cast<int>(repetitionDirection) << ')';
}

inline QDebug operator<<(QDebug debugOutput, const painter::data::PaintableListData& paintableListData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PaintableListData{" << paintableListData.position << ','
                                 << paintableListData.primaryRepetitionDirection << ','
                                 << paintableListData.secondaryRepetitionDirection << ','
                                 << paintableListData.paintableItemListModel << '}';
}
