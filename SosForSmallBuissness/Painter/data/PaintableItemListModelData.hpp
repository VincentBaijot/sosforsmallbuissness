#pragma once

#include <QDebug>
#include <QList>

#include "Utils/DataStructureDebugSupport.hpp"
#include "VariantListableItemData.hpp"

namespace painter::data
{
struct PaintableItemListModelData
{
    QList<VariantListableItemData> paintableItemList;
    bool operator==(const PaintableItemListModelData& list) const = default;
};
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput,
                         const painter::data::PaintableItemListModelData& paintableItemListModelData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "PaintableItemListModelData{QList<VariantListableItemData>";
    utils::qDebugSequentialContainer(debugOutput, paintableItemListModelData.paintableItemList);
    debugOutput << '}';
    return debugOutput;
}
