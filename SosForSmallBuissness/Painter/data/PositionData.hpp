#pragma once

#include <QDebug>
#include <QRectF>

namespace painter::data
{
struct PositionData
{
    qreal x { 0 };
    qreal y { 0 };
    qreal width { 0 };
    qreal height { 0 };
    constexpr QRectF qRectF() const noexcept { return QRectF { x, y, width, height }; }
};

inline bool operator==(const PositionData& positionA, const PositionData& positionB)
{
    return qFuzzyCompare(positionA.x, positionB.x) && qFuzzyCompare(positionA.y, positionB.y)
           && qFuzzyCompare(positionA.width, positionB.width) && qFuzzyCompare(positionA.height, positionB.height);
}
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput, const painter::data::PositionData& position)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PositionData{" << position.x << ',' << position.y << ',' << position.width
                                 << ',' << position.height << '}';
}
