#pragma once

#include <QDebug>

#include "ColorData.hpp"

namespace painter::data
{
struct PaintableBorderData
{
    qreal borderWidth { 0 };
    ColorData color;
};

inline bool operator==(const PaintableBorderData& borderA, const PaintableBorderData& borderB)
{
    return qFuzzyCompare(borderA.borderWidth, borderB.borderWidth) && borderA.color == borderB.color;
}
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput, const painter::data::PaintableBorderData& border)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PaintableBorderData{" << border.borderWidth << ',' << border.color << '}';
}
