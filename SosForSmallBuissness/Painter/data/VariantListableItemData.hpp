#pragma once

#include <QDebug>
#include <variant>

#include "PaintableRectangleData.hpp"
#include "PaintableTextData.hpp"

namespace painter::data
{
using VariantListableItemData = std::variant<PaintableRectangleData, PaintableTextData>;

}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput,
                         const painter::data::VariantListableItemData& variantListableItemData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace();

    return std::visit([debugOutput](const auto& item) { return debugOutput << item; }, variantListableItemData);
}
