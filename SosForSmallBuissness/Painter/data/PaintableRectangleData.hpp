#pragma once

#include <QDebug>

#include "ColorData.hpp"
#include "PaintableBorderData.hpp"
#include "RectanglePositionData.hpp"

namespace painter::data
{
struct PaintableRectangleData
{
    RectanglePositionData position;
    ColorData color;
    PaintableBorderData border;
    bool operator==(const PaintableRectangleData& rectangle) const = default;
};
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput,
                         const painter::data::PaintableRectangleData& paintableRectangle)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PaintableRectangleData{" << paintableRectangle.position << ','
                                 << paintableRectangle.color << ',' << paintableRectangle.border << '}';
}
