#pragma once

#include <QDebug>

#include "ColorData.hpp"
#include "PaintableRectangleData.hpp"

namespace painter::data
{
struct PaintableTextData
{
    ColorData color;
    PaintableRectangleData background;
    QString text;
    QString fontFamily;
    int flags { 0 };
    int pointSize { 0 };
    bool operator==(const PaintableTextData& text) const = default;
};
}    // namespace painter::data

inline QDebug operator<<(QDebug debugOutput, const painter::data::PaintableTextData& paintableText)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "PaintableTextData{" << paintableText.color << ',' << paintableText.background
                                 << ',' << paintableText.text << ',' << paintableText.fontFamily << ','
                                 << paintableText.flags << ',' << paintableText.pointSize << '}';
}
