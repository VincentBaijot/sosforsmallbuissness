<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Application</name>
    <message>
        <location filename="Application/Application.qml" line="12"/>
        <source>Sos for small buissness</source>
        <translation>Sos pour PME</translation>
    </message>
</context>
<context>
    <name>ApplicationBurgerMenu</name>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="19"/>
        <source>Create billing</source>
        <translation>Créer un document</translation>
    </message>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="37"/>
        <source>Edit templates</source>
        <translation>Editer les modèles</translation>
    </message>
    <message>
        <location filename="Application/ApplicationBurgerMenu.qml" line="55"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationMenuBar</name>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="8"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="11"/>
        <source>&amp;New...</source>
        <translation>&amp;Nouveau...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="14"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="17"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="20"/>
        <source>Save &amp;As...</source>
        <translation>&amp;Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="24"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="31"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="33"/>
        <source>Cu&amp;t</source>
        <translation>Cou&amp;per</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="36"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="39"/>
        <source>&amp;Paste</source>
        <translation>Co&amp;ller</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="43"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="Application/ApplicationMenuBar.qml" line="45"/>
        <source>&amp;About</source>
        <translation>A &amp;propos</translation>
    </message>
</context>
<context>
    <name>BorderController</name>
    <message>
        <location filename="SosQml/ItemsControls/BorderController.qml" line="22"/>
        <source>Border color : </source>
        <translation>Couleur de bordure : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/BorderController.qml" line="33"/>
        <source>Border width : </source>
        <translation>Largeur de bordure : </translation>
    </message>
</context>
<context>
    <name>ColorController</name>
    <message>
        <location filename="SosQml/ItemsControls/ColorController.qml" line="47"/>
        <source>Remove the color</source>
        <translation>Supprimer la couleur</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="SosQml/Controls/ColorPicker.qml" line="39"/>
        <source>Please choose a color</source>
        <translation>Choissisez une couleur</translation>
    </message>
</context>
<context>
    <name>CreateBilling</name>
    <message>
        <location filename="SosQml/CreateBilling.qml" line="25"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location filename="SosQml/CreateBilling.qml" line="34"/>
        <source>Document</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataDefinitionEditionTable</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionEditionTable.qml" line="166"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionEditionTable.qml" line="199"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
</context>
<context>
    <name>DataDefinitionListEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="39"/>
        <source>Data lists</source>
        <translation>Listes de données</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="82"/>
        <source>List name</source>
        <translation>Nom de la liste</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataDefinitionListEditionView.qml" line="111"/>
        <source>Demonstration data for %1</source>
        <translation>Données de démonstration pour %1</translation>
    </message>
</context>
<context>
    <name>DataEditionTable</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataEditionTable.qml" line="161"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
</context>
<context>
    <name>DataListAssociationController</name>
    <message>
        <location filename="SosQml/ItemsControls/DataListAssociationController.qml" line="26"/>
        <source>Data association : </source>
        <translation>Données associées : </translation>
    </message>
</context>
<context>
    <name>DataListEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataListEditionView.qml" line="39"/>
        <source>Data lists</source>
        <translation>Listes de données</translation>
    </message>
    <message>
        <location filename="SosQml/Features/EditTemplateData/DataListEditionView.qml" line="87"/>
        <source>Data for %1</source>
        <translation>Données pour %1</translation>
    </message>
</context>
<context>
    <name>EditTemplate</name>
    <message>
        <location filename="SosQml/EditTemplate.qml" line="27"/>
        <source>Templates data</source>
        <translation>Modèle de données</translation>
    </message>
    <message>
        <location filename="SosQml/EditTemplate.qml" line="36"/>
        <source>Template</source>
        <translation>Modèle de document</translation>
    </message>
</context>
<context>
    <name>FontController</name>
    <message>
        <location filename="SosQml/ItemsControls/FontController.qml" line="14"/>
        <source>Text : </source>
        <translation>Texte : </translation>
    </message>
</context>
<context>
    <name>FontSizeController</name>
    <message>
        <location filename="SosQml/ItemsControls/FontSizeController.qml" line="13"/>
        <source>Text Size : </source>
        <translation>Taille de text : </translation>
    </message>
</context>
<context>
    <name>GeneralDataDefinitionsEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/GeneralDataDefinitionsEditionView.qml" line="39"/>
        <source>General variables</source>
        <translation>Variables générales</translation>
    </message>
</context>
<context>
    <name>GeneralDataEditionView</name>
    <message>
        <location filename="SosQml/Features/EditTemplateData/GeneralDataEditionView.qml" line="39"/>
        <source>General variables</source>
        <translation>Variables générales</translation>
    </message>
</context>
<context>
    <name>ItemNameController</name>
    <message>
        <location filename="SosQml/ItemsControls/ItemNameController.qml" line="10"/>
        <source>Item name : </source>
        <translation>Nom de l&apos;item : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemNameController.qml" line="11"/>
        <source>Item name</source>
        <translation>Nom de l&apos;item</translation>
    </message>
</context>
<context>
    <name>ItemPositionController</name>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="36"/>
        <source>Unit : </source>
        <translation>Unité : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="54"/>
        <source>Position : </source>
        <translation>Position : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="69"/>
        <source>x : </source>
        <translation>x : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="95"/>
        <source>y : </source>
        <translation>y : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="127"/>
        <source>width : </source>
        <translation>largeur : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ItemPositionController.qml" line="153"/>
        <source>height : </source>
        <translation>hauteur : </translation>
    </message>
</context>
<context>
    <name>ListAssociationController</name>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationController.qml" line="27"/>
        <source>List association : </source>
        <translation>List associé : </translation>
    </message>
</context>
<context>
    <name>ListController</name>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="25"/>
        <source>List name : </source>
        <translation>Nom de la liste : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="26"/>
        <source>List name</source>
        <translation>Nom de la liste</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="97"/>
        <source>Primary repetition direction</source>
        <translation>Direction principale de répétition</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="109"/>
        <source>Secondary repetition direction</source>
        <translation>Direction secondaire de répétition</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListController.qml" line="139"/>
        <source>Remove the item</source>
        <translation>Supprimer la liste</translation>
    </message>
</context>
<context>
    <name>OpacityController</name>
    <message>
        <location filename="SosQml/ItemsControls/OpacityController.qml" line="13"/>
        <source>Opacity : </source>
        <translation>Opacité : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/OpacityController.qml" line="51"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>PageSizeController</name>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="22"/>
        <source>Page format : </source>
        <translation>Format de page : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="70"/>
        <source>Width : </source>
        <translation>Largeur : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="96"/>
        <source>Height : </source>
        <translation>Hauteur : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeController.qml" line="128"/>
        <source>Unit : </source>
        <translation>Unité : </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="104"/>
        <source>Millimeter</source>
        <translation>Millimètre</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="107"/>
        <source>Point</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="110"/>
        <source>Inch</source>
        <translation>Pouce</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="113"/>
        <source>Pica</source>
        <translation>Pica</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="116"/>
        <source>Didot</source>
        <translation>Didot</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/PageSizeControlProxy.cpp" line="120"/>
        <source>Cicero</source>
        <translation>Cicero</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinition.cpp" line="90"/>
        <source>String</source>
        <translation>Phrase</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinition.cpp" line="93"/>
        <source>Number</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="126"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="128"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="130"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/DataDefinitionListModel.cpp" line="132"/>
        <source>Data type</source>
        <translation>Type de données</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/ListDataDefinitionListModel.cpp" line="118"/>
        <source>ListDataDefinition</source>
        <translation>Définition de liste de données</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="28"/>
        <source>No List</source>
        <translation>Pas de list</translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="51"/>
        <source>List item</source>
        <translation>Element liste</translation>
    </message>
    <message>
        <location filename="DocumentDataDefinitions/controllers/ListDataDefinitionListModel.cpp" line="122"/>
        <location filename="SosQml/ItemsControls/ListAssociationControllerProxyModel.cpp" line="53"/>
        <source>List name</source>
        <translation>Nom de la liste</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="149"/>
        <source>No repetition</source>
        <translation>Pas de répétitions</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="150"/>
        <source>Bottom</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="151"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="152"/>
        <source>Top</source>
        <translation>Haut</translation>
    </message>
    <message>
        <location filename="Painter/controllers/PaintableList.cpp" line="153"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
</context>
<context>
    <name>RadiusController</name>
    <message>
        <location filename="SosQml/ItemsControls/RadiusController.qml" line="14"/>
        <source>Radius : </source>
        <translation>Rayon : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RadiusController.qml" line="52"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>RectangleController</name>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="87"/>
        <source>Color : </source>
        <translation>Couleur : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="121"/>
        <source>Border color : </source>
        <translation>Couleur de bordure : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/RectangleController.qml" line="168"/>
        <source>Remove the item</source>
        <translation>Supprimer le rectangle</translation>
    </message>
</context>
<context>
    <name>TextContentController</name>
    <message>
        <location filename="SosQml/ItemsControls/TextContentController.qml" line="15"/>
        <source>Text : </source>
        <translation>Texte : </translation>
    </message>
</context>
<context>
    <name>TextController</name>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="84"/>
        <source>Text color : </source>
        <translation>Couleur de texte : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="120"/>
        <source>Background color : </source>
        <translation>Couleur de fond : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="137"/>
        <source>Border color : </source>
        <translation>Couleur de bordure : </translation>
    </message>
    <message>
        <location filename="SosQml/ItemsControls/TextController.qml" line="235"/>
        <source>Remove the item</source>
        <translation>Supprimer le texte</translation>
    </message>
</context>
</TS>
