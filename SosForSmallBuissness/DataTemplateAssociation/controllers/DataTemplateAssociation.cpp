#include "DataTemplateAssociation.hpp"

#include "Constants.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataHandler.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinition.hpp"
#include "DocumentDataDefinitions/controllers/ListDataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/RootPaintableItemList.hpp"
#include "PaintableListDataDefinitionListAssociation.hpp"
#include "PaintableListDataDefinitionListBinding.hpp"
#include "PaintableListDataTableModelAssociation.hpp"
#include "PaintableListDataTableModelBinding.hpp"
#include "Painter/controllers/PaintableList.hpp"

namespace datatemplateassociation::controller
{
DataTemplateAssociation::DataTemplateAssociation(QObject* parent)
    : QObject { parent },
      _paintableListDataDefinitionListAssociation {
          utils::make_qobject<PaintableListDataDefinitionListAssociation>(*this)
      },
      _paintableListDataTableModelAssociation { utils::make_qobject<PaintableListDataTableModelAssociation>(
        *this) }
{
}

DataTemplateAssociation::DataTemplateAssociation(
  documenttemplate::controller::DocumentTemplate* documentTemplate,
  documentdatadefinitions::controller::DataHandler* dataHandler,
  const datatemplateassociation::data::DataTemplateAssociationData& data,
  QObject* parent)
    : QObject { parent },
      _paintableListDataDefinitionListAssociation {
          utils::make_qobject<PaintableListDataDefinitionListAssociation>(*this)
      },
      _paintableListDataTableModelAssociation { utils::make_qobject<PaintableListDataTableModelAssociation>(
        *this) },
      _documentTemplate { documentTemplate },
      _dataHandler { dataHandler }
{
    setObjectData(data);
}

DataTemplateAssociation::~DataTemplateAssociation() = default;

datatemplateassociation::data::DataTemplateAssociationData DataTemplateAssociation::objectData() const
{
    Q_ASSERT_X(_documentTemplate, "DataTemplateAssociation::objectData()", "DocumentTemplate is null");
    Q_ASSERT_X(_dataHandler, "DataTemplateAssociation::objectData()", "Data Handler is null");

    datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData;

    for (const auto& [paintableList, dataDefinition] :
         _paintableListDataDefinitionListAssociation->associationMap())
    {
        datatemplateassociation::data::PaintableListIndex paintableListIndex =
          _documentTemplate->paintableItemList()->indexOf(paintableList);
        datatemplateassociation::data::DataDefinitionIndex dataDefinitionIndex =
          _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->indexOf(
            dataDefinition->dataDefinitionList());

        dataTemplateAssociationData.paintableListDataDefinitionListAssociationData.association.insert(
          paintableListIndex, dataDefinitionIndex);
    }

    for (const auto& [key, value] : _paintableListDataTableModelAssociation->associationMap())
    {
        datatemplateassociation::data::PaintableListIndex paintableListIndex =
          _documentTemplate->paintableItemList()->indexOf(key);
        datatemplateassociation::data::DataTablesModelIndex dataTablesModelIndex =
          _dataHandler->userDataHandler()->dataTableModelListModel()->indexOf(value->dataTableModel());

        dataTemplateAssociationData.paintableListDataTableModelAssociationData.association.insert(
          paintableListIndex, dataTablesModelIndex);
    }

    return dataTemplateAssociationData;
}

void DataTemplateAssociation::setObjectData(
  const datatemplateassociation::data::DataTemplateAssociationData& newData)
{
    Q_ASSERT_X(_documentTemplate, "DataTemplateAssociation::setObjectData()", "DocumentTemplate is null");
    Q_ASSERT_X(_dataHandler, "DataTemplateAssociation::setObjectData()", "Data Handler is null");

    _insertPaintableListDataDefinitionListAssociationData(newData.paintableListDataDefinitionListAssociationData);

    _insertPaintableListDataTableModelAssociationData(newData.paintableListDataTableModelAssociationData);
}

const PaintableListDataDefinitionListAssociation*
DataTemplateAssociation::paintableListDataDefinitionListAssociation() const
{
    return _paintableListDataDefinitionListAssociation.get();
}

PaintableListDataDefinitionListAssociation* DataTemplateAssociation::paintableListDataDefinitionListAssociation()
{
    return _paintableListDataDefinitionListAssociation.get();
}

const PaintableListDataTableModelAssociation* DataTemplateAssociation::paintableListDataTableModelAssociation()
  const
{
    return _paintableListDataTableModelAssociation.get();
}

PaintableListDataTableModelAssociation* DataTemplateAssociation::paintableListDataTableModelAssociation()
{
    return _paintableListDataTableModelAssociation.get();
}

documenttemplate::controller::DocumentTemplate* DataTemplateAssociation::documentTemplate() const
{
    return _documentTemplate;
}

void DataTemplateAssociation::setDocumentTemplate(documenttemplate::controller::DocumentTemplate* newDocumentTemplate)
{
    _documentTemplate = newDocumentTemplate;
}

documentdatadefinitions::controller::DataHandler* DataTemplateAssociation::dataHandler() const
{
    return _dataHandler;
}

void DataTemplateAssociation::setDataHandler(documentdatadefinitions::controller::DataHandler* newDataHandler)
{
    _dataHandler = newDataHandler;
}

void DataTemplateAssociation::insertAssociation(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel)
{
    Q_ASSERT_X(_documentTemplate, "DataTemplateAssociation::insertAssociation()", "DocumentTemplate is null");
    Q_ASSERT_X(_dataHandler, "DataTemplateAssociation::insertAssociation()", "Data Handler is null");

    _paintableListDataDefinitionListAssociation->insertAssociation(paintableList, dataDefinitionListModel);

    qCDebug(dataTemplateAssociationControllerCategory)
      << "Insert association of the list item " << _documentTemplate->paintableItemList()->indexOf(paintableList)
      << " with the data definition "
      << _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->indexOf(dataDefinitionListModel);

    if (!_dataHandler)
    {
        qCWarning(dataTemplateAssociationControllerCategory)
          << "No data handler set for document association, unable to find the data associated to the "
             "given definition";
        return;
    }

    for (int rowIndex = 0; rowIndex < _dataHandler->userDataHandler()->dataTableModelListModel()->rowCount();
         ++rowIndex)
    {
        documentdatadefinitions::controller::DataTablesModel* dataTableModel =
          _dataHandler->userDataHandler()->dataTableModelListModel()->at(rowIndex);
        if (dataTableModel->dataDefinitionModel() == dataDefinitionListModel)
        {
            _paintableListDataTableModelAssociation->insertAssociation(paintableList, dataTableModel);
            qCDebug(dataTemplateAssociationControllerCategory)
              << "Insert association of the list item "
              << _documentTemplate->paintableItemList()->indexOf(paintableList) << " with the user data "
              << rowIndex;
        }
    }
}

PaintableListDataDefinitionListBinding* DataTemplateAssociation::associatedDataDefinition(
  painter::controller::PaintableList* paintableList) const
{
    return _paintableListDataDefinitionListAssociation->association(paintableList);
}

PaintableListDataTableModelBinding* DataTemplateAssociation::associatedDataTable(
  painter::controller::PaintableList* paintableList) const
{
    return _paintableListDataTableModelAssociation->association(paintableList);
}

void DataTemplateAssociation::_insertPaintableListDataDefinitionListAssociationData(
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionAssociation)
{
    for (auto iterator = paintableListDataDefinitionAssociation.association.cbegin();
         iterator != paintableListDataDefinitionAssociation.association.cend();
         ++iterator)
    {
        if (iterator.key() < 0 || iterator.key() >= _documentTemplate->paintableItemList()->rowCount())
        {
            qCWarning(dataTemplateAssociationControllerCategory)
              << "Get an invalid paintable item index " << iterator.key();
            continue;
        }

        if (auto* paintableList =
              qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(iterator.key())))
        {
            if (iterator.value() >= 0
                && iterator.value()
                     < _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->rowCount())
            {
                documentdatadefinitions::controller::ListDataDefinition* const listDataDefinition =
                  _dataHandler->dataDefinitionsHandler()->listDataDefinitionListModel()->at(iterator.value());

                _paintableListDataDefinitionListAssociation->insertAssociation(
                  paintableList, listDataDefinition->dataDefinitionListModel());
            }
            else
            {
                qCWarning(dataTemplateAssociationControllerCategory)
                  << "Get an invalid definition index " << iterator.value();
            }
        }
        else
        {
            qCWarning(dataTemplateAssociationControllerCategory)
              << "Get an item which is not a paintable list " << iterator.key();
        }
    }
}

void DataTemplateAssociation::_insertPaintableListDataTableModelAssociationData(
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData)
{
    for (auto iterator = paintableListDataTableModelAssociationData.association.cbegin();
         iterator != paintableListDataTableModelAssociationData.association.cend();
         ++iterator)
    {
        if (iterator.key() < 0 || iterator.key() >= _documentTemplate->paintableItemList()->rowCount())
        {
            qCWarning(dataTemplateAssociationControllerCategory)
              << "Get an invalid paintable item index " << iterator.key();
            continue;
        }

        painter::controller::PaintableList* paintableList =
          qobject_cast<painter::controller::PaintableList*>(_documentTemplate->paintableItemList()->at(iterator.key()));

        if (paintableList)
        {
            if (iterator.value() >= 0
                && iterator.value() < _dataHandler->userDataHandler()->dataTableModelListModel()->rowCount())
            {
                _paintableListDataTableModelAssociation->insertAssociation(
                  paintableList, _dataHandler->userDataHandler()->dataTableModelListModel()->at(iterator.value()));
            }
            else
            {
                qCWarning(dataTemplateAssociationControllerCategory)
                  << "Get an invalid data table index " << iterator.value();
            }
        }
    }
}
}    // namespace datatemplateassociation::controller
