#include "DataTemplateAssociationMap.hpp"

#include "Constants.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DocumentTemplate/controllers/DocumentTemplate.hpp"
#include "DocumentTemplate/controllers/TemplatesHandler.hpp"

namespace datatemplateassociation::controller
{
DataTemplateAssociationMap::DataTemplateAssociationMap(QObject* parent) : QObject { parent }
{
}

DataTemplateAssociationMap::DataTemplateAssociationMap(
  documentdatadefinitions::controller::DataHandler* dataHandler,
  documenttemplate::controller::TemplatesHandler* templatesHandler,
  const datatemplateassociation::data::DataTemplateAssociationMapData& data,
  QObject* parent)
    : QObject { parent }, _dataHandler { dataHandler }, _templatesHandler { templatesHandler }

{
    setObjectData(data);
}

DataTemplateAssociationMap::~DataTemplateAssociationMap() = default;

documentdatadefinitions::controller::DataHandler* DataTemplateAssociationMap::dataHandler() const
{
    return _dataHandler;
}

void DataTemplateAssociationMap::setDataHandler(documentdatadefinitions::controller::DataHandler* newDataHandler)
{
    if (_dataHandler == newDataHandler) { return; }
    _dataHandler = newDataHandler;

    for (const auto& [key, value] : _dataTemplateAssociationList) { value->setDataHandler(_dataHandler); }

    emit dataHandlerChanged();
}

DataTemplateAssociation* DataTemplateAssociationMap::association(
  documenttemplate::controller::DocumentTemplate* documentTemplate)
{
    if (!documentTemplate) { return nullptr; }

    const auto& [insertedIterator, inserted] = _dataTemplateAssociationList.try_emplace(
      documentTemplate, utils::make_qobject<DataTemplateAssociation>(*this));

    if (inserted)
    {
        const auto& newDataTemplateAssociation = insertedIterator->second;
        newDataTemplateAssociation->setDocumentTemplate(documentTemplate);
        newDataTemplateAssociation->setDataHandler(_dataHandler);

        QObject::connect(
          documentTemplate, &QObject::destroyed, this, &DataTemplateAssociationMap::_onDocumentTemplateDestroyed);
    }

    return insertedIterator->second.get();
}

documenttemplate::controller::TemplatesHandler* DataTemplateAssociationMap::templatesHandler() const
{
    return _templatesHandler;
}

void DataTemplateAssociationMap::setTemplatesHandler(documenttemplate::controller::TemplatesHandler* newTemplatesHandler)
{
    if (_templatesHandler == newTemplatesHandler) { return; }
    _templatesHandler = newTemplatesHandler;
    emit templatesHandlerChanged();
}

datatemplateassociation::data::DataTemplateAssociationMapData DataTemplateAssociationMap::objectData() const
{
    Q_ASSERT_X(_templatesHandler, "DataTemplateAssociationMap::objectData()", "TemplatesHandler is null");
    Q_ASSERT_X(_dataHandler, "DataTemplateAssociationMap::objectData()", "DataHandler is null");

    datatemplateassociation::data::DataTemplateAssociationMapData data;

    for (int templateIndex = 0; templateIndex < _templatesHandler->rowCount(); templateIndex++)
    {
        auto found = _dataTemplateAssociationList.find(_templatesHandler->at(templateIndex));
        if (found != _dataTemplateAssociationList.cend())
        {
            data.dataDocumentAssociationMap.insert(templateIndex, found->second->objectData());
        }
    }

    return data;
}

void DataTemplateAssociationMap::setObjectData(
  const datatemplateassociation::data::DataTemplateAssociationMapData& newData)
{
    Q_ASSERT_X(_templatesHandler, "DataTemplateAssociationMap::setObjectData()", "TemplatesHandler is null");
    Q_ASSERT_X(_dataHandler, "DataTemplateAssociationMap::setObjectData()", "DataHandler is null");

    for (auto iterator = newData.dataDocumentAssociationMap.cbegin();
         iterator != newData.dataDocumentAssociationMap.cend();
         ++iterator)
    {
        if (iterator.key() >= 0 && iterator.key() < _templatesHandler->rowCount())
        {
            DataTemplateAssociation* dataDocumentAssociation = association(_templatesHandler->at(iterator.key()));
            dataDocumentAssociation->setObjectData(iterator.value());
        }
        else
        {
            qCWarning(dataTemplateAssociationControllerCategory)
              << "Get an invalid document template index " << iterator.key();
        }
    }
}

void DataTemplateAssociationMap::_onDocumentTemplateDestroyed(QObject* destroyed)
{
    if (auto* documentTemplate = dynamic_cast<documenttemplate::controller::DocumentTemplate*>(destroyed))
    {
        const utils::own_qobject<DataTemplateAssociation>& binding =
          _dataTemplateAssociationList.at(documentTemplate);
        _dataTemplateAssociationList.erase(documentTemplate);
        if (binding) { binding->deleteLater(); }
    }
}

}    // namespace datatemplateassociation::controller
