#include "PaintableListDataTableModelAssociation.hpp"

#include "Painter/controllers/PaintableList.hpp"

namespace datatemplateassociation::controller
{
PaintableListDataTableModelAssociation::PaintableListDataTableModelAssociation(QObject* parent)
    : QObject { parent }
{
}

void PaintableListDataTableModelAssociation::insertAssociation(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataTablesModel* dataTableModel)
{
    auto iterator = _associationMap.find(paintableList);
    if (iterator == _associationMap.end()) { _createBinding(paintableList, dataTableModel); }
    else
    {
        const utils::own_qobject<PaintableListDataTableModelBinding>& value = iterator->second;
        value->setDataTableModel(dataTableModel);
    }
}

PaintableListDataTableModelBinding* PaintableListDataTableModelAssociation::association(
  painter::controller::PaintableList* paintableList) const
{
    if (paintableList)
    {
        auto iterator = _associationMap.find(paintableList);

        if (iterator == _associationMap.cend()) { return _createBinding(paintableList, nullptr); }

        return iterator->second.get();
    }
    else { return nullptr; }
}

const std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataTableModelBinding>>&
PaintableListDataTableModelAssociation::associationMap() const
{
    return _associationMap;
}

void PaintableListDataTableModelAssociation::_onPaintableListDestroyed(QObject* destroyed)
{
    if (auto* paintableList = dynamic_cast<painter::controller::PaintableList*>(destroyed))
    {
        _associationMap.erase(paintableList);
    }
}

PaintableListDataTableModelBinding* PaintableListDataTableModelAssociation::_createBinding(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataTablesModel* dataTableModel) const
{
    Q_ASSERT_X(paintableList, "PaintableListDataTableModelAssociation::_createBinding", "Paintable list is null");

    const auto& [iterator, isInserted] = _associationMap.try_emplace(
      paintableList,
      utils::make_qobject<PaintableListDataTableModelBinding>(*paintableList, paintableList, dataTableModel));

    if (isInserted)
    {
        QObject::connect(paintableList,
                         &QObject::destroyed,
                         this,
                         &PaintableListDataTableModelAssociation::_onPaintableListDestroyed);
    }

    return iterator->second.get();
}
}    // namespace datatemplateassociation::controller
