#include "PaintableListDataTableModelBinding.hpp"

#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "Painter/controllers/PaintableList.hpp"

namespace datatemplateassociation::controller
{
PaintableListDataTableModelBinding::PaintableListDataTableModelBinding(QObject* parent) : QObject { parent }
{
}

PaintableListDataTableModelBinding::PaintableListDataTableModelBinding(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataTablesModel* dataTableModel,
  QObject* parent)
    : QObject { parent }, _paintableList { paintableList }, _dataTableModel { dataTableModel }
{
}

painter::controller::PaintableList* PaintableListDataTableModelBinding::paintableList() const
{
    return _paintableList;
}

void PaintableListDataTableModelBinding::setPaintableList(painter::controller::PaintableList* newPaintableList)
{
    if (_paintableList == newPaintableList) { return; }
    _paintableList = newPaintableList;
    emit paintableListChanged();
}

documentdatadefinitions::controller::DataTablesModel* PaintableListDataTableModelBinding::dataTableModel() const
{
    return _dataTableModel;
}

void PaintableListDataTableModelBinding::setDataTableModel(
  documentdatadefinitions::controller::DataTablesModel* newDataTableModel)
{
    if (_dataTableModel == newDataTableModel) { return; }
    _dataTableModel = newDataTableModel;
    emit dataTableModelChanged();
}
}    // namespace datatemplateassociation::controller
