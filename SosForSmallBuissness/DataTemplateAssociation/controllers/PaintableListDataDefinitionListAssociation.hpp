#pragma once

#include <QHash>
#include <QObject>
#include <unordered_map>

#include "PaintableListDataDefinitionListBinding.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("Painter/controllers/PaintableList.hpp")
Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp")

namespace painter::controller
{
class PaintableList;
}

namespace documentdatadefinitions::controller
{
class DataDefinitionListModel;
}

namespace datatemplateassociation::controller
{

class PaintableListDataDefinitionListAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit PaintableListDataDefinitionListAssociation(QObject* parent = nullptr);

    const std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataDefinitionListBinding>>&
    associationMap() const;

    Q_INVOKABLE void insertAssociation(
      painter::controller::PaintableList* paintableList,
      documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel);
    Q_INVOKABLE datatemplateassociation::controller::PaintableListDataDefinitionListBinding* association(
      painter::controller::PaintableList* paintableList) const;

  private slots:
    void _onPaintableListDestroyed(QObject* destroyed);

  private:
    Q_DISABLE_COPY_MOVE(PaintableListDataDefinitionListAssociation)

    PaintableListDataDefinitionListBinding* _createBinding(
      painter::controller::PaintableList* paintableList,
      documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel) const;

    // Make the variable mutable to return always one binding object for any paintable list and save the binding
    // object created when the association of an unknown object is required
    mutable std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataDefinitionListBinding>>
      _associationMap;
};
}    // namespace datatemplateassociation::controller
