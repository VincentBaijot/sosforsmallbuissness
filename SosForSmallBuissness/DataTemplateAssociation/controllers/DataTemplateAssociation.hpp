#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DataTemplateAssociationController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("controllers/PaintableListDataDefinitionListBinding.hpp")
Q_MOC_INCLUDE("controllers/PaintableListDataTableModelBinding.hpp")
Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp")
Q_MOC_INCLUDE("Painter/controllers/PaintableList.hpp")

namespace datatemplateassociation::data
{
struct DataTemplateAssociationData;
struct PaintableListDataDefinitionListAssociationData;
struct PaintableListDataTableModelAssociationData;
}    // namespace datatemplateassociation::data

namespace painter::controller
{
class PaintableList;
}

namespace documenttemplate::controller
{
class DocumentTemplate;
}

namespace documentdatadefinitions::controller
{
class DataHandler;
class DataDefinitionListModel;
}    // namespace documentdatadefinitions::controller

namespace datatemplateassociation::controller
{
class PaintableListDataDefinitionListAssociation;
class PaintableListDataDefinitionListBinding;
class PaintableListDataTableModelAssociation;
class PaintableListDataTableModelBinding;

class DATA_TEMPLATE_ASSOCIATION_CONTROLLER_TESTING_EXPORT DataTemplateAssociation : public QObject
{
    Q_OBJECT

    QML_ELEMENT
  public:
    explicit DataTemplateAssociation(QObject* parent = nullptr);
    DataTemplateAssociation(documenttemplate::controller::DocumentTemplate* documentTemplate,
                            documentdatadefinitions::controller::DataHandler* dataHandler,
                            const datatemplateassociation::data::DataTemplateAssociationData& data,
                            QObject* parent = nullptr);
    ~DataTemplateAssociation() override;

    datatemplateassociation::data::DataTemplateAssociationData objectData() const;
    void setObjectData(const datatemplateassociation::data::DataTemplateAssociationData& newData);

    const PaintableListDataDefinitionListAssociation* paintableListDataDefinitionListAssociation() const;
    PaintableListDataDefinitionListAssociation* paintableListDataDefinitionListAssociation();
    const PaintableListDataTableModelAssociation* paintableListDataTableModelAssociation() const;
    PaintableListDataTableModelAssociation* paintableListDataTableModelAssociation();

    documenttemplate::controller::DocumentTemplate* documentTemplate() const;
    void setDocumentTemplate(documenttemplate::controller::DocumentTemplate* newDocumentTemplate);

    documentdatadefinitions::controller::DataHandler* dataHandler() const;
    void setDataHandler(documentdatadefinitions::controller::DataHandler* newDataHandler);

    Q_INVOKABLE void insertAssociation(
      painter::controller::PaintableList* paintableList,
      documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel);
    Q_INVOKABLE datatemplateassociation::controller::PaintableListDataDefinitionListBinding*
    associatedDataDefinition(painter::controller::PaintableList* paintableList) const;
    Q_INVOKABLE datatemplateassociation::controller::PaintableListDataTableModelBinding* associatedDataTable(
      painter::controller::PaintableList* paintableList) const;

  private:
    Q_DISABLE_COPY_MOVE(DataTemplateAssociation)

    void _insertPaintableListDataDefinitionListAssociationData(
      const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
        paintableListDataDefinitionAssociation);
    void _insertPaintableListDataTableModelAssociationData(
      const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
        paintableListDataTableModelAssociationData);

    utils::own_qobject<PaintableListDataDefinitionListAssociation> _paintableListDataDefinitionListAssociation;
    utils::own_qobject<PaintableListDataTableModelAssociation> _paintableListDataTableModelAssociation;

    documenttemplate::controller::DocumentTemplate* _documentTemplate { nullptr };
    documentdatadefinitions::controller::DataHandler* _dataHandler { nullptr };
};
}    // namespace datatemplateassociation::controller
