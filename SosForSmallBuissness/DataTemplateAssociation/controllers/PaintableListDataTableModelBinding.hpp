#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DataTemplateAssociationController_global.hpp"

namespace documentdatadefinitions::controller
{
class DataTablesModel;
}

namespace painter::controller
{
class PaintableList;
}

namespace datatemplateassociation::controller
{
class DATA_TEMPLATE_ASSOCIATION_CONTROLLER_TESTING_EXPORT PaintableListDataTableModelBinding : public QObject
{
    Q_OBJECT

    Q_PROPERTY(
      painter::controller::PaintableList* paintableList READ paintableList WRITE setPaintableList NOTIFY paintableListChanged)
    Q_PROPERTY(documentdatadefinitions::controller::DataTablesModel* dataTableModel READ dataTableModel WRITE
                 setDataTableModel NOTIFY dataTableModelChanged)

    QML_ELEMENT
  public:
    explicit PaintableListDataTableModelBinding(QObject* parent = nullptr);
    PaintableListDataTableModelBinding(painter::controller::PaintableList* paintableList,
                                       documentdatadefinitions::controller::DataTablesModel* dataTableModel,
                                       QObject* parent = nullptr);

    painter::controller::PaintableList* paintableList() const;
    void setPaintableList(painter::controller::PaintableList* newPaintableList);

    documentdatadefinitions::controller::DataTablesModel* dataTableModel() const;
    void setDataTableModel(documentdatadefinitions::controller::DataTablesModel* newDataTableModel);

  signals:
    void paintableListChanged();
    void dataTableModelChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableListDataTableModelBinding)

    QPointer<painter::controller::PaintableList> _paintableList;
    QPointer<documentdatadefinitions::controller::DataTablesModel> _dataTableModel;
};
}    // namespace datatemplateassociation::controller
