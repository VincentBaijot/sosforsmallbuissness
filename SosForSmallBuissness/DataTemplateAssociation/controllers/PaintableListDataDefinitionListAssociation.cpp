#include "PaintableListDataDefinitionListAssociation.hpp"

#include "Painter/controllers/PaintableList.hpp"

namespace datatemplateassociation::controller
{
PaintableListDataDefinitionListAssociation::PaintableListDataDefinitionListAssociation(QObject* parent)
    : QObject { parent }
{
}

const std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataDefinitionListBinding>>&
PaintableListDataDefinitionListAssociation::associationMap() const
{
    return _associationMap;
}

void PaintableListDataDefinitionListAssociation::insertAssociation(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel)
{
    Q_ASSERT_X(paintableList != nullptr, "insertAssociation", "The given paintableList is null");

    auto iterator = _associationMap.find(paintableList);
    if (iterator == _associationMap.cend()) { _createBinding(paintableList, dataDefinitionListModel); }
    else
    {
        const utils::own_qobject<PaintableListDataDefinitionListBinding>& value = iterator->second;
        value->setDataDefinitionList(dataDefinitionListModel);
    }
}

PaintableListDataDefinitionListBinding* PaintableListDataDefinitionListAssociation::association(
  painter::controller::PaintableList* paintableList) const
{
    if (paintableList)
    {
        auto iterator = _associationMap.find(paintableList);

        if (iterator == _associationMap.cend()) { return _createBinding(paintableList, nullptr); }

        return iterator->second.get();
    }
    else { return nullptr; }
}

void PaintableListDataDefinitionListAssociation::_onPaintableListDestroyed(QObject* destroyed)
{
    if (auto* paintableList = dynamic_cast<painter::controller::PaintableList*>(destroyed))
    {
        const utils::own_qobject<PaintableListDataDefinitionListBinding>& binding =
          _associationMap.at(paintableList);
        _associationMap.erase(paintableList);
        if (binding) { binding->deleteLater(); }
    }
}

PaintableListDataDefinitionListBinding* PaintableListDataDefinitionListAssociation::_createBinding(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel) const
{
    Q_ASSERT_X(
      paintableList, "PaintableListDataDefinitionListAssociation::_createBinding", "Paintable list is null");

    const auto& [iterator, isInserted] =
      _associationMap.try_emplace(paintableList,
                                  utils::make_qobject<PaintableListDataDefinitionListBinding>(
                                    *paintableList, paintableList, dataDefinitionListModel));

    if (isInserted)
    {
        QObject::connect(paintableList,
                         &QObject::destroyed,
                         this,
                         &PaintableListDataDefinitionListAssociation::_onPaintableListDestroyed);
    }

    return iterator->second.get();
}
}    // namespace datatemplateassociation::controller
