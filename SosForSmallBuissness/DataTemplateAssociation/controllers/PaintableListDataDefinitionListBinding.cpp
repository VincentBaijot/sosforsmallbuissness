#include "PaintableListDataDefinitionListBinding.hpp"

#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "Painter/controllers/PaintableList.hpp"

namespace datatemplateassociation::controller
{
PaintableListDataDefinitionListBinding::PaintableListDataDefinitionListBinding(QObject* parent)
    : QObject { parent }
{
}

PaintableListDataDefinitionListBinding::PaintableListDataDefinitionListBinding(
  painter::controller::PaintableList* paintableList,
  documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel,
  QObject* parent)
    : QObject { parent }, _paintableList { paintableList }, _dataDefinitionList { dataDefinitionListModel }
{
}

painter::controller::PaintableList* PaintableListDataDefinitionListBinding::paintableList() const
{
    return _paintableList;
}

void PaintableListDataDefinitionListBinding::setPaintableList(painter::controller::PaintableList* newPaintableList)
{
    if (_paintableList == newPaintableList) return;
    _paintableList = newPaintableList;
    emit paintableListChanged();
}

documentdatadefinitions::controller::DataDefinitionListModel* PaintableListDataDefinitionListBinding::dataDefinitionList()
  const
{
    return _dataDefinitionList;
}

void PaintableListDataDefinitionListBinding::setDataDefinitionList(
  documentdatadefinitions::controller::DataDefinitionListModel* newDataDefinitionList)
{
    if (_dataDefinitionList == newDataDefinitionList) return;
    _dataDefinitionList = newDataDefinitionList;
    emit dataDefinitionListChanged();
}
}    // namespace datatemplateassociation::controller
