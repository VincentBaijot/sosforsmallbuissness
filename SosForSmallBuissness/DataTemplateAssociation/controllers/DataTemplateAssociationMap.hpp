#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>
#include <unordered_map>

#include "DataTemplateAssociation.hpp"
#include "DataTemplateAssociationController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("DocumentTemplate/controllers//DocumentTemplate.hpp")
Q_MOC_INCLUDE("DataTemplateAssociation/controllers/DataTemplateAssociation.hpp")

namespace datatemplateassociation::data
{
struct DataTemplateAssociationMapData;
}

namespace documenttemplate::controller
{
class DocumentTemplate;
class TemplatesHandler;
}    // namespace documenttemplate::controller

namespace documentdatadefinitions::controller
{
class DataHandler;
}    // namespace documentdatadefinitions::controller

namespace datatemplateassociation::controller
{

class DATA_TEMPLATE_ASSOCIATION_CONTROLLER_EXPORT DataTemplateAssociationMap : public QObject
{
    Q_OBJECT

    QML_ELEMENT
  public:
    explicit DataTemplateAssociationMap(QObject* parent = nullptr);
    DataTemplateAssociationMap(documentdatadefinitions::controller::DataHandler* dataHandler,
                               documenttemplate::controller::TemplatesHandler* templatesHandler,
                               const datatemplateassociation::data::DataTemplateAssociationMapData& data,
                               QObject* parent = nullptr);
    ~DataTemplateAssociationMap() override;

    Q_INVOKABLE datatemplateassociation::controller::DataTemplateAssociation* association(
      documenttemplate::controller::DocumentTemplate* documentTemplate);

    documentdatadefinitions::controller::DataHandler* dataHandler() const;
    void setDataHandler(documentdatadefinitions::controller::DataHandler* newDataHandler);

    documenttemplate::controller::TemplatesHandler* templatesHandler() const;
    void setTemplatesHandler(documenttemplate::controller::TemplatesHandler* newTemplatesHandler);

    datatemplateassociation::data::DataTemplateAssociationMapData objectData() const;
    void setObjectData(const datatemplateassociation::data::DataTemplateAssociationMapData& newData);

  signals:
    void dataHandlerChanged();
    void templatesHandlerChanged();

  private slots:
    void _onDocumentTemplateDestroyed(QObject* destroyed);

  private:
    Q_DISABLE_COPY_MOVE(DataTemplateAssociationMap)

    documentdatadefinitions::controller::DataHandler* _dataHandler { nullptr };
    documenttemplate::controller::TemplatesHandler* _templatesHandler { nullptr };

    std::unordered_map<documenttemplate::controller::DocumentTemplate*, utils::own_qobject<DataTemplateAssociation>>
      _dataTemplateAssociationList;
};
}    // namespace datatemplateassociation::controller
