#pragma once

#include <QHash>
#include <QObject>
#include <unordered_map>

#include "PaintableListDataTableModelBinding.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("Painter/controllers/PaintableList.hpp")
Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/DataTablesModel.hpp")
Q_MOC_INCLUDE("DataTemplateAssociation/controllers/PaintableListDataTableModelBinding.hpp")

namespace documentdatadefinitions::controller
{
class DataTablesModel;
}

namespace painter::controller
{
class PaintableList;
}

namespace datatemplateassociation::controller
{

class PaintableListDataTableModelAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit PaintableListDataTableModelAssociation(QObject* parent = nullptr);

    Q_INVOKABLE void insertAssociation(painter::controller::PaintableList* paintableList,
                                       documentdatadefinitions::controller::DataTablesModel* dataTableModel);
    Q_INVOKABLE datatemplateassociation::controller::PaintableListDataTableModelBinding* association(
      painter::controller::PaintableList* paintableList) const;

    const std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataTableModelBinding>>&
    associationMap() const;

  private slots:
    void _onPaintableListDestroyed(QObject* destroyed);

  private:
    Q_DISABLE_COPY_MOVE(PaintableListDataTableModelAssociation)

    PaintableListDataTableModelBinding* _createBinding(
      painter::controller::PaintableList* paintableList,
      documentdatadefinitions::controller::DataTablesModel* dataTableModel) const;

    // Make the variable mutable to return always one binding object for any paintable list and save the binding
    // object created when the association of an unknown object is required
    mutable std::unordered_map<painter::controller::PaintableList*, utils::own_qobject<PaintableListDataTableModelBinding>>
      _associationMap;
};
}    // namespace datatemplateassociation::controller
