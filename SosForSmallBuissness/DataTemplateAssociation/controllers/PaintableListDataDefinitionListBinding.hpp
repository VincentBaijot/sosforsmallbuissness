#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DataTemplateAssociationController_global.hpp"

namespace documentdatadefinitions::controller
{
class DataDefinitionListModel;
}

namespace painter::controller
{
class PaintableList;
}

namespace datatemplateassociation::controller
{
class DATA_TEMPLATE_ASSOCIATION_CONTROLLER_TESTING_EXPORT PaintableListDataDefinitionListBinding : public QObject
{
    Q_OBJECT

    Q_PROPERTY(
      painter::controller::PaintableList* paintableList READ paintableList WRITE setPaintableList NOTIFY paintableListChanged)
    Q_PROPERTY(documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionList READ
                 dataDefinitionList WRITE setDataDefinitionList NOTIFY dataDefinitionListChanged)

    QML_ELEMENT
  public:
    explicit PaintableListDataDefinitionListBinding(QObject* parent = nullptr);
    PaintableListDataDefinitionListBinding(
      painter::controller::PaintableList* paintableList,
      documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel,
      QObject* parent = nullptr);

    painter::controller::PaintableList* paintableList() const;
    void setPaintableList(painter::controller::PaintableList* newPaintableList);

    documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionList() const;
    void setDataDefinitionList(
      documentdatadefinitions::controller::DataDefinitionListModel* newDataDefinitionList);

  signals:
    void paintableListChanged();
    void dataDefinitionListChanged();

  private:
    Q_DISABLE_COPY_MOVE(PaintableListDataDefinitionListBinding)

    QPointer<painter::controller::PaintableList> _paintableList;
    QPointer<documentdatadefinitions::controller::DataDefinitionListModel> _dataDefinitionList;
};
}    // namespace datatemplateassociation::controller
