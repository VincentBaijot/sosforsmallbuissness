#pragma once

#include <QStringView>

namespace datatemplateassociation::service
{
inline constexpr const QStringView paintableListDataDefinitionListAssociationElement =
  u"PaintableListDataDefinitionListAssociation";
inline constexpr const QStringView paintableListDataTableModelAssociationElement =
  u"PaintableListDataTableModelAssociation";
inline constexpr const QStringView dataTemplateAssociationElement    = u"DataTemplateAssociation";
inline constexpr const QStringView dataTemplateAssociationMapElement = u"DataTemplateAssociationMap";
}    // namespace datatemplateassociation::service
