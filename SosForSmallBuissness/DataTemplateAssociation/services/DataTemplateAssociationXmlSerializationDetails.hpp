#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

namespace datatemplateassociation::data
{
struct PaintableListDataDefinitionListAssociationData;
struct PaintableListDataTableModelAssociationData;
struct DataTemplateAssociationData;
struct DataTemplateAssociationMapData;
}    // namespace datatemplateassociation::data

namespace datatemplateassociation::service::details
{
datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
readPaintableListDataDefinitionListAssociationData(QXmlStreamReader& reader);
void writePaintableListDataDefinitionListAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData);

datatemplateassociation::data::PaintableListDataTableModelAssociationData
readPaintableListDataTableModelAssociationData(QXmlStreamReader& reader);
void writePaintableListDataTableModelAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData);

datatemplateassociation::data::DataTemplateAssociationData readDataTemplateAssociationData(
  QXmlStreamReader& reader);
void writeDataTemplateAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData);

datatemplateassociation::data::DataTemplateAssociationMapData readDataTemplateAssociationMapData(
  QXmlStreamReader& reader);
void writeDataTemplateAssociationMapData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataTemplateAssociationMapData);
}    // namespace datatemplateassociation::service::details
