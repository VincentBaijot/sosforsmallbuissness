#include "DataTemplateAssociationXmlSerialization.hpp"

#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataDefinitionListAssociationData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataTableModelAssociationData.hpp"
#include "DataTemplateAssociationXmlConstants.hpp"
#include "DataTemplateAssociationXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace datatemplateassociation::service
{

std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
readPaintableListDataDefinitionListAssociationData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader,
                                        paintableListDataDefinitionListAssociationElement,
                                        u"PaintableListDataDefinitionListAssociationData"))
    {
        return std::nullopt;
    }

    return details::readPaintableListDataDefinitionListAssociationData(reader);
}

void writePaintableListDataDefinitionListAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData)
{
    details::writePaintableListDataDefinitionListAssociationData(writer,
                                                                 paintableListDataDefinitionListAssociationData);
}

std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
readPaintableListDataTableModelAssociationData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(
          reader, paintableListDataTableModelAssociationElement, u"PaintableListDataTableModelAssociationData"))
    {
        return std::nullopt;
    }

    return details::readPaintableListDataTableModelAssociationData(reader);
}

void writePaintableListDataTableModelAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData)
{
    details::writePaintableListDataTableModelAssociationData(writer, paintableListDataTableModelAssociationData);
}

std::optional<datatemplateassociation::data::DataTemplateAssociationData> readDataTemplateAssociationData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataTemplateAssociationElement, u"DataTemplateAssociationData"))
    {
        return std::nullopt;
    }

    return details::readDataTemplateAssociationData(reader);
}

void writeDataTemplateAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData)
{
    details::writeDataTemplateAssociationData(writer, dataTemplateAssociationData);
}

std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
readDataTemplateAssociationMapData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(
          reader, dataTemplateAssociationMapElement, u"DataTemplateAssociationMapData"))
    {
        return std::nullopt;
    }

    return details::readDataTemplateAssociationMapData(reader);
}

void writeDataTemplateAssociationMapData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataTemplateAssociationMapData)
{
    details::writeDataTemplateAssociationMapData(writer, dataTemplateAssociationMapData);
}

}    // namespace datatemplateassociation::service
