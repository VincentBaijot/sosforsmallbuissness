#include "DataTemplateAssociationXmlSerializationDetails.hpp"

#include "Constants.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataDefinitionListAssociationData.hpp"
#include "DataTemplateAssociation/data/PaintableListDataTableModelAssociationData.hpp"
#include "DataTemplateAssociationXmlConstants.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace datatemplateassociation::service::details
{
inline constexpr const QStringView paintableListDataDefinitionListAssociationEntryElement = u"AssociationEntry";
inline constexpr const QStringView paintableListIndexElement                              = u"PaintableListIndex";
inline constexpr const QStringView dataDefinitionIndexElement                             = u"DataDefinitionIndex";
inline constexpr const QStringView paintableListDataTableModelAssociationEntryElement     = u"AssociationEntry";
inline constexpr const QStringView dataTablesModelIndexElement         = u"DataTablesModelIndex";
inline constexpr const QStringView dataTemplateAssociationEntryElement = u"AssociationEntry";
inline constexpr const QStringView documentTemplateIndexElement        = u"DocumentTemplateIndex";

datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
readPaintableListDataDefinitionListAssociationEntryElement(QXmlStreamReader& reader)
{
    datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
      paintableListDataDefinitionListAssociationData;

    bool keyFound                                            = false;
    bool valueFound                                          = false;
    datatemplateassociation::data::PaintableListIndex key    = -1;
    datatemplateassociation::data::DataDefinitionIndex value = -1;

    while (!utils::xml::checkEndElement(reader, paintableListDataDefinitionListAssociationEntryElement))
    {
        if (reader.isStartElement() && reader.name() == paintableListIndexElement)
        {
            key = reader.readElementText().toInt(&keyFound);
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionIndexElement)
        {
            value = reader.readElementText().toInt(&valueFound);
        }
        else { reader.readNext(); }
    }

    if (keyFound && valueFound) { paintableListDataDefinitionListAssociationData.association.insert(key, value); }
    else if (!valueFound)
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid PaintableListDataDefinitionListAssociation, missing value "
          << dataDefinitionIndexElement.toString() << " for key " << paintableListIndexElement.toString() << " "
          << key;
    }
    else
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid PaintableListDataDefinitionListAssociation, missing key "
          << paintableListIndexElement.toString() << " for value " << dataDefinitionIndexElement.toString() << " "
          << value;
    }

    return paintableListDataDefinitionListAssociationData;
}

datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
readPaintableListDataDefinitionListAssociationData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableListDataDefinitionListAssociationElement);

    reader.readNextStartElement();

    datatemplateassociation::data::PaintableListDataDefinitionListAssociationData
      paintableListDataDefinitionListAssociationData;

    while (!utils::xml::checkEndElement(reader, paintableListDataDefinitionListAssociationElement))
    {
        if (reader.isStartElement() && reader.name() == paintableListDataDefinitionListAssociationEntryElement)
        {
            paintableListDataDefinitionListAssociationData.association.insert(
              readPaintableListDataDefinitionListAssociationEntryElement(reader).association);
        }
        else { reader.readNext(); }
    }

    return paintableListDataDefinitionListAssociationData;
}

void writePaintableListDataDefinitionListAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData)
{
    writer.writeStartElement(paintableListDataDefinitionListAssociationElement.toString());

    for (auto it = paintableListDataDefinitionListAssociationData.association.cbegin();
         it != paintableListDataDefinitionListAssociationData.association.cend();
         ++it)
    {
        writer.writeStartElement(paintableListDataDefinitionListAssociationEntryElement.toString());

        writer.writeStartElement(paintableListIndexElement.toString());
        writer.writeCharacters(QString::number(it.key()));
        writer.writeEndElement();

        writer.writeStartElement(dataDefinitionIndexElement.toString());
        writer.writeCharacters(QString::number(it.value()));
        writer.writeEndElement();

        writer.writeEndElement();
    }

    writer.writeEndElement();
}

datatemplateassociation::data::PaintableListDataTableModelAssociationData
readPaintableListDataTableModelAssociationEntryElement(QXmlStreamReader& reader)
{
    datatemplateassociation::data::PaintableListDataTableModelAssociationData
      paintableListDataTableModelAssociationData;

    bool keyFound                                             = false;
    bool valueFound                                           = false;
    datatemplateassociation::data::PaintableListIndex key     = -1;
    datatemplateassociation::data::DataTablesModelIndex value = -1;

    while (!utils::xml::checkEndElement(reader, paintableListDataTableModelAssociationEntryElement))
    {
        if (reader.isStartElement() && reader.name() == paintableListIndexElement)
        {
            key = reader.readElementText().toInt(&keyFound);
        }
        else if (reader.isStartElement() && reader.name() == dataTablesModelIndexElement)
        {
            value = reader.readElementText().toInt(&valueFound);
        }
        else { reader.readNext(); }
    }

    if (keyFound && valueFound) { paintableListDataTableModelAssociationData.association.insert(key, value); }
    else if (!valueFound)
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid PaintableListDataTableModelAssociationData, missing value "
          << dataDefinitionIndexElement.toString() << " for key " << dataTablesModelIndexElement.toString() << " "
          << key;
    }
    else
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid PaintableListDataTableModelAssociationData, missing key "
          << paintableListIndexElement.toString() << " for value " << dataTablesModelIndexElement.toString() << " "
          << value;
    }

    return paintableListDataTableModelAssociationData;
}

datatemplateassociation::data::PaintableListDataTableModelAssociationData
readPaintableListDataTableModelAssociationData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == paintableListDataTableModelAssociationElement);

    reader.readNextStartElement();

    datatemplateassociation::data::PaintableListDataTableModelAssociationData
      paintableListDataTableModelAssociationData;

    while (!utils::xml::checkEndElement(reader, paintableListDataTableModelAssociationElement))
    {
        if (reader.isStartElement() && reader.name() == paintableListDataTableModelAssociationEntryElement)
        {
            paintableListDataTableModelAssociationData.association.insert(
              readPaintableListDataTableModelAssociationEntryElement(reader).association);
        }
        else { reader.readNext(); }
    }

    return paintableListDataTableModelAssociationData;
}

void writePaintableListDataTableModelAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData)
{
    writer.writeStartElement(paintableListDataTableModelAssociationElement.toString());

    for (auto it = paintableListDataTableModelAssociationData.association.cbegin();
         it != paintableListDataTableModelAssociationData.association.cend();
         ++it)
    {
        writer.writeStartElement(paintableListDataTableModelAssociationEntryElement.toString());

        writer.writeStartElement(paintableListIndexElement.toString());
        writer.writeCharacters(QString::number(it.key()));
        writer.writeEndElement();

        writer.writeStartElement(dataTablesModelIndexElement.toString());
        writer.writeCharacters(QString::number(it.value()));
        writer.writeEndElement();

        writer.writeEndElement();
    }

    writer.writeEndElement();
}

datatemplateassociation::data::DataTemplateAssociationData readDataTemplateAssociationData(
  QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataTemplateAssociationElement);

    reader.readNextStartElement();

    datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData;

    while (!utils::xml::checkEndElement(reader, dataTemplateAssociationElement))
    {
        if (reader.isStartElement() && reader.name() == paintableListDataDefinitionListAssociationElement)
        {
            dataTemplateAssociationData.paintableListDataDefinitionListAssociationData =
              readPaintableListDataDefinitionListAssociationData(reader);
        }
        else if (reader.isStartElement() && reader.name() == paintableListDataTableModelAssociationElement)
        {
            dataTemplateAssociationData.paintableListDataTableModelAssociationData =
              readPaintableListDataTableModelAssociationData(reader);
        }
        else { reader.readNext(); }
    }

    return dataTemplateAssociationData;
}

void writeDataTemplateAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData)
{
    writer.writeStartElement(dataTemplateAssociationElement.toString());

    writePaintableListDataDefinitionListAssociationData(
      writer, dataTemplateAssociationData.paintableListDataDefinitionListAssociationData);
    writePaintableListDataTableModelAssociationData(
      writer, dataTemplateAssociationData.paintableListDataTableModelAssociationData);

    writer.writeEndElement();
}

datatemplateassociation::data::DataTemplateAssociationMapData readDataTemplateAssociationEntryElement(
  QXmlStreamReader& reader)
{
    datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData;

    bool keyFound                                            = false;
    bool valueFound                                          = false;
    datatemplateassociation::data::DocumentTemplateIndex key = -1;
    datatemplateassociation::data::DataTemplateAssociationData value;

    while (!utils::xml::checkEndElement(reader, dataTemplateAssociationEntryElement))
    {
        if (reader.isStartElement() && reader.name() == documentTemplateIndexElement)
        {
            key = reader.readElementText().toInt(&keyFound);
        }
        else if (reader.isStartElement() && reader.name() == dataTemplateAssociationElement)
        {
            valueFound = true;
            value      = readDataTemplateAssociationData(reader);
        }
        else { reader.readNext(); }
    }

    if (keyFound && valueFound) { dataTemplateAssociationMapData.dataDocumentAssociationMap.insert(key, value); }
    else if (!valueFound)
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid DataTemplateAssociation, missing value " << dataTemplateAssociationElement.toString()
          << " for key " << documentTemplateIndexElement.toString() << " " << key;
    }
    else
    {
        qCWarning(dataTemplateAssociationServiceCategory)
          << "Read invalid DataTemplateAssociation, missing key " << documentTemplateIndexElement.toString()
          << " for value " << dataTemplateAssociationElement.toString() << " " << value;
    }

    return dataTemplateAssociationMapData;
}

datatemplateassociation::data::DataTemplateAssociationMapData readDataTemplateAssociationMapData(
  QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataTemplateAssociationMapElement);

    reader.readNextStartElement();

    datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData;

    while (!utils::xml::checkEndElement(reader, dataTemplateAssociationMapElement))
    {
        if (reader.isStartElement() && reader.name() == dataTemplateAssociationEntryElement)
        {
            dataTemplateAssociationMapData.dataDocumentAssociationMap.insert(
              readDataTemplateAssociationEntryElement(reader).dataDocumentAssociationMap);
        }
        else { reader.readNext(); }
    }

    return dataTemplateAssociationMapData;
}

void writeDataTemplateAssociationMapData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataTemplateAssociationMapData)
{
    writer.writeStartElement(dataTemplateAssociationMapElement.toString());

    for (auto it = dataTemplateAssociationMapData.dataDocumentAssociationMap.cbegin();
         it != dataTemplateAssociationMapData.dataDocumentAssociationMap.cend();
         ++it)
    {
        writer.writeStartElement(dataTemplateAssociationEntryElement.toString());

        writer.writeStartElement(documentTemplateIndexElement.toString());
        writer.writeCharacters(QString::number(it.key()));
        writer.writeEndElement();

        writeDataTemplateAssociationData(writer, it.value());

        writer.writeEndElement();
    }

    writer.writeEndElement();
}

}    // namespace datatemplateassociation::service::details
