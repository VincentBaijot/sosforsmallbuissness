#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "DataTemplateAssociationService_global.hpp"

namespace datatemplateassociation::data
{
struct PaintableListDataDefinitionListAssociationData;
struct PaintableListDataTableModelAssociationData;
struct DataTemplateAssociationData;
struct DataTemplateAssociationMapData;
}    // namespace datatemplateassociation::data

namespace datatemplateassociation::service
{
DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT
std::optional<datatemplateassociation::data::PaintableListDataDefinitionListAssociationData>
readPaintableListDataDefinitionListAssociationData(QXmlStreamReader& reader);
DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT void writePaintableListDataDefinitionListAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData);

DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT
std::optional<datatemplateassociation::data::PaintableListDataTableModelAssociationData>
readPaintableListDataTableModelAssociationData(QXmlStreamReader& reader);
DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT void writePaintableListDataTableModelAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
    paintableListDataTableModelAssociationData);

DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT
std::optional<datatemplateassociation::data::DataTemplateAssociationData> readDataTemplateAssociationData(
  QXmlStreamReader& reader);
DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT void writeDataTemplateAssociationData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData);

DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT
std::optional<datatemplateassociation::data::DataTemplateAssociationMapData>
readDataTemplateAssociationMapData(QXmlStreamReader& reader);
DATA_TEMPLATE_ASSOCIATION_SERVICE_EXPORT void writeDataTemplateAssociationMapData(
  QXmlStreamWriter& writer,
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataTemplateAssociationMapData);

}    // namespace datatemplateassociation::service
