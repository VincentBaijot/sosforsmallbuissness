#pragma once

#include <QDebug>
#include <QHash>

#include "Utils/DataStructureDebugSupport.hpp"

namespace datatemplateassociation::data
{
using PaintableListIndex  = int;
using DataDefinitionIndex = int;

struct PaintableListDataDefinitionListAssociationData
{
    QHash<PaintableListIndex, DataDefinitionIndex> association;
    bool operator==(const PaintableListDataDefinitionListAssociationData&
                      paintableListDataDefinitionListAssociation) const = default;
};
}    // namespace datatemplateassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datatemplateassociation::data::PaintableListDataDefinitionListAssociationData&
    paintableListDataDefinitionListAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "PaintableListDataDefinitionListAssociationData{";
    debugOutput << "QHash<PaintableListIndex, DataDefinitionIndex>{";
    utils::qDebugAssociativeContainer(debugOutput, paintableListDataDefinitionListAssociationData.association);
    debugOutput << "}";
    debugOutput << "}";
    return debugOutput;
}
