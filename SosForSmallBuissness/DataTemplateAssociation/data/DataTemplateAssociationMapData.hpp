#pragma once

#include "DataTemplateAssociationData.hpp"

namespace datatemplateassociation::data
{
using DocumentTemplateIndex = int;

struct DataTemplateAssociationMapData
{
    QHash<DocumentTemplateIndex, DataTemplateAssociationData> dataDocumentAssociationMap;
    bool operator==(const DataTemplateAssociationMapData& dataDocumentAssociationMapData) const = default;
};
}    // namespace datatemplateassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datatemplateassociation::data::DataTemplateAssociationMapData& dataTemplateAssociationMapData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataTemplateAssociationMapData{";
    debugOutput << "QHash<DocumentTemplateIndex, DataTemplateAssociationData>{";
    utils::qDebugAssociativeContainer(debugOutput, dataTemplateAssociationMapData.dataDocumentAssociationMap);
    debugOutput << "}";
    debugOutput << "}";
    return debugOutput;
}
