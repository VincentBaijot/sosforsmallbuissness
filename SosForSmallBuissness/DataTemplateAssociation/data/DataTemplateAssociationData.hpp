#pragma once

#include "PaintableListDataDefinitionListAssociationData.hpp"
#include "PaintableListDataTableModelAssociationData.hpp"

namespace datatemplateassociation::data
{
struct DataTemplateAssociationData
{
    PaintableListDataDefinitionListAssociationData paintableListDataDefinitionListAssociationData;
    PaintableListDataTableModelAssociationData paintableListDataTableModelAssociationData;
    bool operator==(const DataTemplateAssociationData& dataDocumentAssociationData) const = default;
};

}    // namespace datatemplateassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "DataTemplateAssociationData{"
                                 << dataTemplateAssociationData.paintableListDataDefinitionListAssociationData
                                 << ',' << dataTemplateAssociationData.paintableListDataTableModelAssociationData
                                 << '}';
}
