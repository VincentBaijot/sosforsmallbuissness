#pragma once

#include <QDebug>
#include <QHash>

#include "Utils/DataStructureDebugSupport.hpp"

namespace datatemplateassociation::data
{
using PaintableListIndex   = int;
using DataTablesModelIndex = int;
struct PaintableListDataTableModelAssociationData
{
    QHash<PaintableListIndex, DataTablesModelIndex> association;
    bool operator==(const PaintableListDataTableModelAssociationData& paintableListDataTableModelAssociationData)
      const = default;
};

}    // namespace datatemplateassociation::data

inline QDebug operator<<(QDebug debugOutput,
                         const datatemplateassociation::data::PaintableListDataTableModelAssociationData&
                           paintableListDataTableModelAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "PaintableListDataTableModelAssociationData{";
    debugOutput << "QHash<PaintableListIndex, DataTablesModelIndex>{";
    utils::qDebugAssociativeContainer(debugOutput, paintableListDataTableModelAssociationData.association);
    debugOutput << "}";
    debugOutput << "}";
    return debugOutput;
}
