cmake_minimum_required(VERSION 3.22)

print_location()

set(HEADERS
   DataDefinitionData.hpp
   DataDefinitionListData.hpp
   DataTablesData.hpp
   ListDataDefinitionData.hpp
   ListDataDefinitionListData.hpp
   DataDefinitionsHandlerData.hpp
   DataTableModelListData.hpp
   UserDataHandlerData.hpp
   DataHandlerData.hpp
)

# Short compilation library name to avoid windows long path issues
set(LIBRARY_NAME DDDD)

qt_add_library(${LIBRARY_NAME} INTERFACE ${HEADERS})
# Library explicit name
add_library(DocumentDataDefinitions::data ALIAS ${LIBRARY_NAME})

target_link_libraries(${LIBRARY_NAME} INTERFACE Qt${QT_VERSION_MAJOR}::Core)
target_link_libraries(${LIBRARY_NAME} INTERFACE Utils)
