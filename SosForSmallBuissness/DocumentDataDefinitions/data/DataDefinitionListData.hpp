#pragma once

#include <QList>

#include "DataDefinitionData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace documentdatadefinitions::data
{
struct DataDefinitionListData
{
    QString listName;
    QList<DataDefinitionData> dataDefinitions;
    bool operator==(const DataDefinitionListData& dataDefinitionDataList) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataDefinitionListData{" << dataDefinitionListData.listName << ',';
    debugOutput << "QList<DataDefinitionData>";
    utils::qDebugSequentialContainer(debugOutput, dataDefinitionListData.dataDefinitions);
    debugOutput << '}';

    return debugOutput;
}
