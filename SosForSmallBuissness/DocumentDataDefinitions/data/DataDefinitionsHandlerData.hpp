#pragma once

#include <QList>

#include "DataDefinitionListData.hpp"
#include "ListDataDefinitionListData.hpp"

namespace documentdatadefinitions::data
{
struct DataDefinitionsHandlerData
{
    DataDefinitionListData generalVariableList;
    ListDataDefinitionListData listDataDefinitionList;
    bool operator==(const DataDefinitionsHandlerData& dataDefinitionsHandlerData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "DataDefinitionsHandlerData{" << dataDefinitionsHandlerData.generalVariableList
                                 << ',' << dataDefinitionsHandlerData.listDataDefinitionList << '}';
}
