#pragma once

#include "DataTablesData.hpp"

namespace documentdatadefinitions::data
{
struct DataTableModelListData
{
    QList<DataTablesData> dataTableModelList;
    bool operator==(const DataTableModelListData& dataTableModelListData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::DataTableModelListData& dataTableModelListData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataTableModelListData{";
    debugOutput << "QList<DataTablesData>{";
    utils::qDebugSequentialContainer(debugOutput, dataTableModelListData.dataTableModelList);
    debugOutput << "}";
    debugOutput << "}";
    return debugOutput;
}
