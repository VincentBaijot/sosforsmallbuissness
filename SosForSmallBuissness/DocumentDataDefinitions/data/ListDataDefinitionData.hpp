#pragma once

#include "DataDefinitionListData.hpp"
#include "DataTablesData.hpp"

namespace documentdatadefinitions::data
{
struct ListDataDefinitionData
{
    DataDefinitionListData dataDefinitionListData;
    DataTablesData demonstrationTable;
    bool operator==(const ListDataDefinitionData& listDataDefinitionData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "ListDataDefinitionData{" << listDataDefinitionData.dataDefinitionListData
                                 << ',' << listDataDefinitionData.demonstrationTable << "}";
}
