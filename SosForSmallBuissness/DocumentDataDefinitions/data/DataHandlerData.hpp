#pragma once

#include "DataDefinitionsHandlerData.hpp"
#include "UserDataHandlerData.hpp"

namespace documentdatadefinitions::data
{
struct DataHandlerData
{
    DataDefinitionsHandlerData dataDefinitionsHandler;
    UserDataHandlerData userDataHandler;
    bool operator==(const DataHandlerData& dataHandlerData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(QDebug debugOutput,
                         const documentdatadefinitions::data::DataHandlerData& dataHandlerData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "DataHandlerData{" << dataHandlerData.dataDefinitionsHandler << ','
                                 << dataHandlerData.userDataHandler << '}';
}
