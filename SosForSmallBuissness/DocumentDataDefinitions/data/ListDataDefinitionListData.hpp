#pragma once

#include "ListDataDefinitionData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace documentdatadefinitions::data
{
struct ListDataDefinitionListData
{
    QList<ListDataDefinitionData> listOfDataDefinition;
    bool operator==(const ListDataDefinitionListData& listDataDefinitionListData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace();
    debugOutput << "ListDataDefinitionListData{" << "QList<ListDataDefinitionData>";
    utils::qDebugSequentialContainer(debugOutput, listDataDefinitionListData.listOfDataDefinition);
    debugOutput << "}";

    return debugOutput;
}
