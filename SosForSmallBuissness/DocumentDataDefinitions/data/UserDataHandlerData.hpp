#pragma once

#include "DataTableModelListData.hpp"

namespace documentdatadefinitions::data
{
struct UserDataHandlerData
{
    DataTablesData generalVariableDataList;
    DataTableModelListData dataTableModelListModel;
    bool operator==(const UserDataHandlerData& userDataHandlerData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(
  QDebug debugOutput,
  const documentdatadefinitions::data::UserDataHandlerData& listDataDefinitionListData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "UserDataHandlerData{" << listDataDefinitionListData.generalVariableDataList
                                 << ',' << listDataDefinitionListData.dataTableModelListModel << '}';
}
