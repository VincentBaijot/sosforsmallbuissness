#pragma once

#include <QDebug>
#include <QString>

namespace documentdatadefinitions::data
{
struct DataDefinitionData
{
    enum class DataType
    {
        StringType = 0,
        First      = StringType,
        NumberType = 1,
        Last       = NumberType
    };

    QString key;
    QString name;
    QString description;
    DataType dataType { DataType::StringType };
    bool operator==(const DataDefinitionData& dataDefinitionData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(QDebug debugOutput,
                         const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataDefinitionData{" << dataDefinitionData.key << ',' << dataDefinitionData.name
                          << ',' << dataDefinitionData.description << ",DataDefinitionData::DataType("
                          << static_cast<int>(dataDefinitionData.dataType) << ")}";
    return debugOutput;
}
