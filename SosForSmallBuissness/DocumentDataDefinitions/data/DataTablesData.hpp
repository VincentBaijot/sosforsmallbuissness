#pragma once

#include <QDebug>
#include <QHash>
#include <QList>
#include <QString>

#include "Utils/DataStructureDebugSupport.hpp"

namespace documentdatadefinitions::data
{
using DataDefinitionIndex = int;
using DataTableData       = QHash<DataDefinitionIndex, QString>;

struct DataTablesData
{
    QList<DataTableData> dataTables;
    bool operator==(const DataTablesData& dataTablesData) const = default;
};
}    // namespace documentdatadefinitions::data

inline QDebug operator<<(QDebug debugOutput,
                         const documentdatadefinitions::data::DataTableData& dataTableData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataTableData";
    utils::qDebugAssociativeContainer(debugOutput, dataTableData);
    debugOutput << "";
    return debugOutput;
}

inline QDebug operator<<(QDebug debugOutput,
                         const documentdatadefinitions::data::DataTablesData& dataTablesData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataTablesData{";
    debugOutput << "QList<DataTableData>{";
    utils::qDebugSequentialContainer(debugOutput, dataTablesData.dataTables);
    debugOutput << "}";
    debugOutput << "}";
    return debugOutput;
}
