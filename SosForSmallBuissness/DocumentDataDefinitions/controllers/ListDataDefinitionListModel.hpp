#pragma once

#include <QtQml/QQmlEngine>

#include "DocumentDataDefinitions/data/ListDataDefinitionListData.hpp"
#include "DocumentDataDefinitionsController_global.hpp"
#include "ListDataDefinition.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

namespace documentdatadefinitions::controller
{
class DataDefinitionListModel;

/*!
 * @brief A list of the definitions of the list of data defined and available for the templates
 */
class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT ListDataDefinitionListModel
    : public utils::SimpleDataObjectListModel<documentdatadefinitions::controller::ListDataDefinition,
                                              documentdatadefinitions::data::ListDataDefinitionData>
{
    Q_OBJECT

    QML_ELEMENT
  public:
    enum class ListDataDefinitionListModelRoles
    {
        ListName = Qt::UserRole + 1
    };

    explicit ListDataDefinitionListModel(QObject* parent = nullptr);
    explicit ListDataDefinitionListModel(
      const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData,
      QObject* parent = nullptr);
    ~ListDataDefinitionListModel() override = default;

    documentdatadefinitions::data::ListDataDefinitionListData objectData() const;
    void setObjectData(
      const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData);

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void addListDataDefinition();
    Q_INVOKABLE void removeListDataDefinition(int rowIndex);

    Q_INVOKABLE int indexOf(
      const documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel) const;

  private:
    Q_DISABLE_COPY_MOVE(ListDataDefinitionListModel)
};

}    // namespace documentdatadefinitions::controller
