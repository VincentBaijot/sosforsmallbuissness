#pragma once

#include <QtQml/QQmlEngine>

#include "DataDefinition.hpp"
#include "DocumentDataDefinitions/data/DataDefinitionListData.hpp"
#include "DocumentDataDefinitionsController_global.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

namespace documentdatadefinitions::controller
{
class DataDefinition;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataDefinitionListModel
    : public utils::SimpleDataObjectListModel<documentdatadefinitions::controller::DataDefinition,
                                              documentdatadefinitions::data::DataDefinitionData>
{
    Q_OBJECT

    Q_PROPERTY(QString listName READ listName WRITE setListName NOTIFY listNameChanged FINAL)
    Q_PROPERTY(QStringList dataTypeNames READ dataTypeNames CONSTANT FINAL)

    QML_ELEMENT
  public:
    enum class DataDefinitionRole
    {
        KeyDefinition = Qt::UserRole + 1,
        NameDefinition,
        DescriptionDefinition,
        DataTypeDefinition
    };
    Q_ENUM(DataDefinitionRole)

    explicit DataDefinitionListModel(QObject* parent = nullptr);
    explicit DataDefinitionListModel(const documentdatadefinitions::data::DataDefinitionListData& data,
                                     QObject* parent = nullptr);
    ~DataDefinitionListModel() override = default;

    documentdatadefinitions::data::DataDefinitionListData objectData() const;
    void setObjectData(const documentdatadefinitions::data::DataDefinitionListData& newData);

    QString listName() const;
    void setListName(const QString& newListName);

    Q_INVOKABLE void addDataDefinition();
    Q_INVOKABLE void removeDataDefinition(int rowIndex);

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    static QStringList dataTypeNames();

  signals:
    void listNameChanged();

  private:
    Q_DISABLE_COPY_MOVE(DataDefinitionListModel)

    QString _listName;
};

}    // namespace documentdatadefinitions::controller
