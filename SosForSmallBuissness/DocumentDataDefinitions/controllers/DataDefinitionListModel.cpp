#include "DataDefinitionListModel.hpp"

#include "Constants.hpp"

namespace documentdatadefinitions::controller
{
DataDefinitionListModel::DataDefinitionListModel(QObject* parent)
    : SimpleDataObjectListModel<documentdatadefinitions::controller::DataDefinition,
                                documentdatadefinitions::data::DataDefinitionData> { parent }
{
    // Add one first data definition
    addDataDefinition();
}

DataDefinitionListModel::DataDefinitionListModel(const documentdatadefinitions::data::DataDefinitionListData& data,
                                                 QObject* parent)
    : SimpleDataObjectListModel<documentdatadefinitions::controller::DataDefinition,
                                documentdatadefinitions::data::DataDefinitionData> { data.dataDefinitions,
                                                                                     parent },
      _listName { data.listName }
{
}

documentdatadefinitions::data::DataDefinitionListData DataDefinitionListModel::objectData() const
{
    documentdatadefinitions::data::DataDefinitionListData dataDefinitionList;
    dataDefinitionList.listName        = _listName;
    dataDefinitionList.dataDefinitions = listData();

    return dataDefinitionList;
}

void DataDefinitionListModel::setObjectData(const documentdatadefinitions::data::DataDefinitionListData& newData)
{
    if (newData == objectData()) { return; }

    setListName(newData.listName);
    setListData(newData.dataDefinitions);
}

void DataDefinitionListModel::addDataDefinition()
{
    qCInfo(documentDataDefinitionControllerCategory) << "Add data definition ";

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    objects().push_back(utils::make_qobject<DataDefinition>(*this));
    endInsertRows();
}

void DataDefinitionListModel::removeDataDefinition(int rowIndex)
{
    if (rowIndex < 0 || rowIndex >= objects().size()) { return; }

    qCInfo(documentDataDefinitionControllerCategory) << "Remove data definition " << rowIndex;

    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    const utils::own_qobject<DataDefinition>& dataDefinition = objects().at(rowIndex);
    dataDefinition->deleteLater();
    objects().erase(objects().begin() + rowIndex);
    endRemoveRows();
}

QVariant DataDefinitionListModel::data(const QModelIndex& index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return QVariant(); }

    switch (DataDefinitionRole(role))
    {
        case DataDefinitionRole::KeyDefinition:
            return objects().at(index.row())->key();
        case DataDefinitionRole::NameDefinition:
            return objects().at(index.row())->name();
        case DataDefinitionRole::DescriptionDefinition:
            return objects().at(index.row())->description();
        case DataDefinitionRole::DataTypeDefinition:
            return static_cast<int>(objects().at(index.row())->dataType());
        default:
            return {};
    }
}

bool DataDefinitionListModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (index.row() < 0 || index.row() >= rowCount()) { return false; }

    switch (DataDefinitionRole(role))
    {
        case DataDefinitionRole::KeyDefinition:
        {
            qCDebug(documentDataDefinitionControllerCategory)
              << "For data definition " << index.row() << " set the key " << value.toString();
            objects().at(index.row())->setKey(value.toString());
            emit dataChanged(index, index, { static_cast<int>(DataDefinitionRole::KeyDefinition) });
            return true;
        }
        case DataDefinitionRole::NameDefinition:
            qCDebug(documentDataDefinitionControllerCategory)
              << "For data definition " << index.row() << " set the name " << value.toString();
            objects().at(index.row())->setName(value.toString());
            emit dataChanged(index, index, { static_cast<int>(DataDefinitionRole::NameDefinition) });
            return true;
        case DataDefinitionRole::DescriptionDefinition:
            qCDebug(documentDataDefinitionControllerCategory)
              << "For data definition " << index.row() << " set the description " << value.toString();
            objects().at(index.row())->setDescription(value.toString());
            emit dataChanged(index, index, { static_cast<int>(DataDefinitionRole::DescriptionDefinition) });
            return true;
        case DataDefinitionRole::DataTypeDefinition:
            qCDebug(documentDataDefinitionControllerCategory)
              << "For data definition " << index.row() << " set the data type " << value.toString();
            objects().at(index.row())->setDataType(DataDefinition::DataType(value.toInt()));
            emit dataChanged(index, index, { static_cast<int>(DataDefinitionRole::DataTypeDefinition) });
            return true;
        default:
            return false;
    }
}

QVariant DataDefinitionListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)

    switch (DataDefinitionRole(role))
    {
        case DataDefinitionRole::KeyDefinition:
            return QObject::tr("Key");
        case DataDefinitionRole::NameDefinition:
            return QObject::tr("Name");
        case DataDefinitionRole::DescriptionDefinition:
            return QObject::tr("Description");
        case DataDefinitionRole::DataTypeDefinition:
            return QObject::tr("Data type");
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> DataDefinitionListModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles.insert(static_cast<int>(DataDefinitionRole::KeyDefinition), "keyDefinition");
    roles.insert(static_cast<int>(DataDefinitionRole::NameDefinition), "nameDefinition");
    roles.insert(static_cast<int>(DataDefinitionRole::DescriptionDefinition), "descriptionDefinition");
    roles.insert(static_cast<int>(DataDefinitionRole::DataTypeDefinition), "dataTypeDefinition");
    return roles;
}

Qt::ItemFlags DataDefinitionListModel::flags(const QModelIndex& index) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return Qt::NoItemFlags; }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

QStringList DataDefinitionListModel::dataTypeNames()
{
    return DataDefinition::dataTypeNames();
}

QString DataDefinitionListModel::listName() const
{
    return _listName;
}

void DataDefinitionListModel::setListName(const QString& newListName)
{
    if (_listName == newListName) return;
    _listName = newListName;
    emit listNameChanged();
}

}    // namespace documentdatadefinitions::controller
