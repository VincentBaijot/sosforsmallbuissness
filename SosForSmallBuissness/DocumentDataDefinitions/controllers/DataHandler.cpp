#include "DataHandler.hpp"

#include "DataDefinitionsHandler.hpp"
#include "data/DataHandlerData.hpp"
#include "UserDataHandler.hpp"

namespace documentdatadefinitions::controller
{

DataHandler::DataHandler(QObject* parent)
    : QObject { parent },
      _dataDefinitionsHandler { utils::make_qobject<DataDefinitionsHandler>(*this) },
      _userDataHandler { utils::make_qobject<UserDataHandler>(*this, _dataDefinitionsHandler.get()) }
{
}

DataHandler::DataHandler(const documentdatadefinitions::data::DataHandlerData& data, QObject* parent)
    : QObject { parent },
      _dataDefinitionsHandler { utils::make_qobject<DataDefinitionsHandler>(*this, data.dataDefinitionsHandler) },
      _userDataHandler {
          utils::make_qobject<UserDataHandler>(*this, _dataDefinitionsHandler.get(), data.userDataHandler)
      }
{
}

DataHandler::~DataHandler() = default;

DataDefinitionsHandler* DataHandler::dataDefinitionsHandler()
{
    return _dataDefinitionsHandler.get();
}

UserDataHandler* DataHandler::userDataHandler()
{
    return _userDataHandler.get();
}

documentdatadefinitions::data::DataHandlerData DataHandler::objectData() const
{
    return documentdatadefinitions::data::DataHandlerData { _dataDefinitionsHandler->objectData(),
                                                                   _userDataHandler->objectData() };
}

void DataHandler::setObjectData(const documentdatadefinitions::data::DataHandlerData& newData)
{
    _dataDefinitionsHandler->setObjetData(newData.dataDefinitionsHandler);
    _userDataHandler->setObjectData(newData.userDataHandler);
}

}    // namespace documentdatadefinitions::controller
