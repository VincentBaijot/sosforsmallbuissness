#include "DataTableModelListModel.hpp"

#include "Constants.hpp"
#include "DataTablesModel.hpp"
#include "ListDataDefinition.hpp"
#include "data/DataTableModelListData.hpp"

namespace documentdatadefinitions::controller
{
DataTableModelListModel::DataTableModelListModel(QObject* parent)
    : SimpleObjectListModel<documentdatadefinitions::controller::DataTablesModel, dataTableModelListModelHeaderData> { parent }
{
}

DataTableModelListModel::DataTableModelListModel(
  const ListDataDefinitionListModel* newListDataDefinitionListModel,
  const documentdatadefinitions::data::DataTableModelListData& data,
  QObject* parent)
    : SimpleObjectListModel<documentdatadefinitions::controller::DataTablesModel,
                            dataTableModelListModelHeaderData> { parent },
      _listDataDefinitionListModel { newListDataDefinitionListModel }
{
    _connectAndCreateTables();
    _setData(data);
}

DataTableModelListModel::DataTableModelListModel(const ListDataDefinitionListModel* newListDataDefinitionListModel,
                                                 QObject* parent)
    : SimpleObjectListModel<DataTablesModel, dataTableModelListModelHeaderData> { parent },
      _listDataDefinitionListModel { newListDataDefinitionListModel }
{
    _connectAndCreateTables();
}

documentdatadefinitions::data::DataTableModelListData DataTableModelListModel::objectData() const
{
    documentdatadefinitions::data::DataTableModelListData dataTableModelListData;

    for (const utils::own_qobject<DataTablesModel>& dataTables : objects())
    {
        dataTableModelListData.dataTableModelList.append(dataTables->objectData());
    }

    return dataTableModelListData;
}

void DataTableModelListModel::setObjectData(
  const documentdatadefinitions::data::DataTableModelListData& newData)
{
    if (objectData() == newData) { return; }
    _setData(newData);

    emit dataChanged(index(0), index(static_cast<int>(objects().size())), { Qt::DisplayRole });
}

const ListDataDefinitionListModel* DataTableModelListModel::listDataDefinitionListModel() const
{
    return _listDataDefinitionListModel;
}

void DataTableModelListModel::setListDataDefinitionListModel(
  const ListDataDefinitionListModel* newListDataDefinitionListModel)
{
    if (_listDataDefinitionListModel)
    {
        QObject::disconnect(_listDataDefinitionListModel,
                            &QAbstractItemModel::rowsInserted,
                            this,
                            &DataTableModelListModel::_dataDefinitionListModelAdded);
        QObject::disconnect(_listDataDefinitionListModel,
                            &QAbstractItemModel::rowsRemoved,
                            this,
                            &DataTableModelListModel::_dataDefinitionListModelRemoved);
        QObject::disconnect(_listDataDefinitionListModel,
                            &QAbstractItemModel::modelAboutToBeReset,
                            this,
                            &DataTableModelListModel::_onListDataDefinitionListModelAboutToBeReset);
        QObject::disconnect(_listDataDefinitionListModel,
                            &QAbstractItemModel::modelReset,
                            this,
                            &DataTableModelListModel::_onListDataDefinitionListModelReset);
        QObject::disconnect(_listDataDefinitionListModel,
                            &QObject::destroyed,
                            this,
                            &DataTableModelListModel::_onListDataDefinitionListModelDestroyed);
    }

    _listDataDefinitionListModel = newListDataDefinitionListModel;

    beginResetModel();
    objects().clear();

    _connectAndCreateTables();

    endResetModel();
}

void DataTableModelListModel::_dataDefinitionListModelAdded(const QModelIndex& parent, int first, int last)
{
    if (first >= 0 && last >= 0 && first < _listDataDefinitionListModel->rowCount()
        && last < _listDataDefinitionListModel->rowCount())
    {
        beginInsertRows(parent, first, last);
        for (int i = first; i <= last; ++i)
        {
            if (const ListDataDefinition* listDataDefinition = _listDataDefinitionListModel->at(i))
            {
                objects().emplace_back(utils::make_qobject<DataTablesModel>(*this));
                objects().back()->setDataDefinitionModel(listDataDefinition->dataDefinitionListModel());
            }
        }
        endInsertRows();
    }
}

void DataTableModelListModel::_dataDefinitionListModelRemoved(const QModelIndex& parent, int first, int last)
{
    if (first >= 0 && last >= 0 && first < static_cast<int>(objects().size())
        && last < static_cast<int>(objects().size()))
    {
        beginRemoveRows(parent, first, last);
        for (int i = first; i <= last; ++i)
        {
            const utils::own_qobject<DataTablesModel>& dataTableModel = objects().at(i);
            dataTableModel->deleteLater();
        }
        objects().erase(objects().begin() + first, objects().begin() + last + 1);
        endRemoveRows();
    }
}

void DataTableModelListModel::_onListDataDefinitionListModelAboutToBeReset()
{
    beginResetModel();
    deleteAllObjects();
    objects().clear();
}

void DataTableModelListModel::_onListDataDefinitionListModelReset()
{
    _createTables();
    endResetModel();
}

void DataTableModelListModel::_onListDataDefinitionListModelDestroyed()
{
    setListDataDefinitionListModel(nullptr);
}

void DataTableModelListModel::_connectAndCreateTables()
{
    if (_listDataDefinitionListModel)
    {
        QObject::connect(_listDataDefinitionListModel,
                         &QAbstractItemModel::rowsInserted,
                         this,
                         &DataTableModelListModel::_dataDefinitionListModelAdded);
        QObject::connect(_listDataDefinitionListModel,
                         &QAbstractItemModel::rowsRemoved,
                         this,
                         &DataTableModelListModel::_dataDefinitionListModelRemoved);
        QObject::connect(_listDataDefinitionListModel,
                         &QAbstractItemModel::modelAboutToBeReset,
                         this,
                         &DataTableModelListModel::_onListDataDefinitionListModelAboutToBeReset);
        QObject::connect(_listDataDefinitionListModel,
                         &QAbstractItemModel::modelReset,
                         this,
                         &DataTableModelListModel::_onListDataDefinitionListModelReset);
        QObject::connect(_listDataDefinitionListModel,
                         &QObject::destroyed,
                         this,
                         &DataTableModelListModel::_onListDataDefinitionListModelDestroyed);

        _createTables();
    }
}

void DataTableModelListModel::_createTables()
{
    for (int index = 0; index < _listDataDefinitionListModel->rowCount(); ++index)
    {
        const ListDataDefinition* const listDataDefinition = _listDataDefinitionListModel->at(index);
        if (listDataDefinition)
        {
            objects().emplace_back(utils::make_qobject<DataTablesModel>(*this));
            objects().back()->setDataDefinitionModel(listDataDefinition->dataDefinitionListModel());
        }
    }
}

void DataTableModelListModel::_setData(const documentdatadefinitions::data::DataTableModelListData& newData)
{
    size_t actualListSize = newData.dataTableModelList.size();

    if (actualListSize > objects().size())
    {
        qCWarning(documentDataDefinitionControllerCategory)
          << "The number of defined list of data " << objects().size()
          << " is less than the list of data set as input data " << newData.dataTableModelList.size();
        actualListSize = objects().size();
    }

    for (qsizetype index = 0; index < static_cast<qsizetype>(actualListSize); ++index)
    {
        objects().at(index)->setObjectData(newData.dataTableModelList.at(index));
    }
}

}    // namespace documentdatadefinitions::controller
