#include "ListDataDefinitionListModel.hpp"

#include "Constants.hpp"
#include "DataDefinitionListModel.hpp"
#include "ListDataDefinition.hpp"

namespace documentdatadefinitions::controller
{
ListDataDefinitionListModel::ListDataDefinitionListModel(QObject* parent)
    : SimpleDataObjectListModel<documentdatadefinitions::controller::ListDataDefinition,
                                documentdatadefinitions::data::ListDataDefinitionData> { parent }
{
    // Add a first list of data definitions
    addListDataDefinition();
}

ListDataDefinitionListModel::ListDataDefinitionListModel(
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData,
  QObject* parent)
    : SimpleDataObjectListModel<documentdatadefinitions::controller::ListDataDefinition,
                                documentdatadefinitions::data::ListDataDefinitionData> { parent }
{
    objects().reserve(listDataDefinitionListData.listOfDataDefinition.size());
    for (const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData :
         listDataDefinitionListData.listOfDataDefinition)
    {
        objects().emplace_back(utils::make_qobject<ListDataDefinition>(*this, listDataDefinitionData));
    }
}

documentdatadefinitions::data::ListDataDefinitionListData ListDataDefinitionListModel::objectData() const
{
    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData;
    listDataDefinitionListData.listOfDataDefinition = listData();

    return listDataDefinitionListData;
}

void ListDataDefinitionListModel::setObjectData(
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData)
{
    if (objectData() == listDataDefinitionListData) { return; }

    setListData(listDataDefinitionListData.listOfDataDefinition);
}

void ListDataDefinitionListModel::addListDataDefinition()
{
    beginInsertRows(QModelIndex(), static_cast<int>(objects().size()), static_cast<int>(objects().size()));
    objects().emplace_back(utils::make_qobject<ListDataDefinition>(*this));

    int index        = 0;
    QString listName = "List " + QString::number(index);

    while (std::ranges::find_if(objects(),
                                [listName](const utils::own_qobject<ListDataDefinition>& listDataDefinition)
                                { return listDataDefinition->dataDefinitionListModel()->listName() == listName; })
           != std::cend(objects()))
    {
        ++index;
        listName = "List " + QString::number(index);
    }

    qCInfo(documentDataDefinitionControllerCategory) << "Add list data definition " << listName;

    objects().back()->dataDefinitionListModel()->setListName(listName);

    endInsertRows();
}

void ListDataDefinitionListModel::removeListDataDefinition(int rowIndex)
{
    if (rowIndex < 0 || rowIndex >= objects().size()) { return; }
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    const utils::own_qobject<ListDataDefinition>& listDataDefinition = objects().at(rowIndex);

    qCInfo(documentDataDefinitionControllerCategory)
      << "Remove list data definition " << listDataDefinition->dataDefinitionListModel()->listName() << "("
      << rowIndex << ")";

    listDataDefinition->deleteLater();
    objects().erase(objects().begin() + rowIndex);
    endRemoveRows();
}

QVariant ListDataDefinitionListModel::data(const QModelIndex& index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return QVariant(); }

    switch (role)
    {
        case Qt::DisplayRole:
            return QVariant::fromValue(objects().at(index.row()).get());
        case static_cast<int>(ListDataDefinitionListModelRoles::ListName):
            return objects().at(index.row())->dataDefinitionListModel()->listName();
        default:
            return {};
    }
}

QHash<int, QByteArray> ListDataDefinitionListModel::roleNames() const
{
    QHash<int, QByteArray> roleNamesHash = QAbstractListModel::roleNames();

    roleNamesHash.insert(static_cast<int>(ListDataDefinitionListModelRoles::ListName), "listName");

    return roleNamesHash;
}

QVariant ListDataDefinitionListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Orientation::Vertical) { return QVariant(); }

    switch (role)
    {
        case Qt::DisplayRole:
        {
            return QObject::tr("ListDataDefinition");
        }
        case static_cast<int>(ListDataDefinitionListModelRoles::ListName):
        {
            return QObject::tr("List name");
        }

        default:
            return QVariant();
    }
}

int ListDataDefinitionListModel::indexOf(const DataDefinitionListModel* dataDefinitionListModel) const
{
    for (qsizetype index = 0; index < objects().size(); ++index)
    {
        if (objects().at(index)->dataDefinitionListModel() == dataDefinitionListModel)
        {
            return static_cast<int>(index);
        }
    }

    return -1;
}

}    // namespace documentdatadefinitions::controller
