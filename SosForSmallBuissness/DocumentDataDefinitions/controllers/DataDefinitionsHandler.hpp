#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DocumentDataDefinitionsController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("DataDefinitionListModel.hpp")
Q_MOC_INCLUDE("ListDataDefinitionListModel.hpp")

namespace documentdatadefinitions::data
{
struct DataDefinitionsHandlerData;
}

namespace documentdatadefinitions::controller
{

class DataDefinitionListModel;
class ListDataDefinitionListModel;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataDefinitionsHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(documentdatadefinitions::controller::DataDefinitionListModel* generalVariableList READ generalVariableList
                 CONSTANT FINAL)
    Q_PROPERTY(documentdatadefinitions::controller::ListDataDefinitionListModel* listDataDefinitionListModel READ
                 listDataDefinitionListModel CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit DataDefinitionsHandler(QObject* parent = nullptr);
    explicit DataDefinitionsHandler(const documentdatadefinitions::data::DataDefinitionsHandlerData& data,
                                    QObject* parent = nullptr);
    ~DataDefinitionsHandler() override;

    DataDefinitionListModel* generalVariableList();
    const DataDefinitionListModel* generalVariableList() const;
    ListDataDefinitionListModel* listDataDefinitionListModel();
    const ListDataDefinitionListModel* listDataDefinitionListModel() const;

    documentdatadefinitions::data::DataDefinitionsHandlerData objectData() const;
    void setObjetData(const documentdatadefinitions::data::DataDefinitionsHandlerData& newData);

  private:
    Q_DISABLE_COPY_MOVE(DataDefinitionsHandler)

    utils::own_qobject<DataDefinitionListModel> _generalVariableList;
    utils::own_qobject<ListDataDefinitionListModel> _listDataDefinitionListModel;
};

}    // namespace documentdatadefinitions::controller
