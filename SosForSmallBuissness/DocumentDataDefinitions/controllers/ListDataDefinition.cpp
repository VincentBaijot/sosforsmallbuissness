#include "ListDataDefinition.hpp"

#include "DataDefinitionListModel.hpp"
#include "DataTablesModel.hpp"
#include "data/ListDataDefinitionData.hpp"

namespace documentdatadefinitions::controller
{
ListDataDefinition::ListDataDefinition(QObject* parent)
    : QObject { parent },
      _dataDefinitionListModel { utils::make_qobject<DataDefinitionListModel>(*this) },
      _demonstrationDataTable { utils::make_qobject<DataTablesModel>(*this, _dataDefinitionListModel.get()) }
{
}

ListDataDefinition::ListDataDefinition(const documentdatadefinitions::data::ListDataDefinitionData& newData,
                                       QObject* parent)
    : QObject { parent },
      _dataDefinitionListModel { utils::make_qobject<DataDefinitionListModel>(*this,
                                                                              newData.dataDefinitionListData) },
      _demonstrationDataTable {
          utils::make_qobject<DataTablesModel>(*this, _dataDefinitionListModel.get(), newData.demonstrationTable)
      }
{
}

ListDataDefinition::~ListDataDefinition() = default;

documentdatadefinitions::data::ListDataDefinitionData ListDataDefinition::objectData() const
{
    documentdatadefinitions::data::ListDataDefinitionData data;
    data.dataDefinitionListData = _dataDefinitionListModel->objectData();
    data.demonstrationTable     = _demonstrationDataTable->objectData();
    return data;
}

void ListDataDefinition::setObjectData(const documentdatadefinitions::data::ListDataDefinitionData& newData)
{
    _dataDefinitionListModel->setObjectData(newData.dataDefinitionListData);
    _demonstrationDataTable->setObjectData(newData.demonstrationTable);
}

DataDefinitionListModel* ListDataDefinition::dataDefinitionListModel()
{
    return _dataDefinitionListModel.get();
}

const DataDefinitionListModel* ListDataDefinition::dataDefinitionListModel() const
{
    return _dataDefinitionListModel.get();
}

DataTablesModel* ListDataDefinition::demonstrationDataTable()
{
    return _demonstrationDataTable.get();
}

const DataTablesModel* ListDataDefinition::demonstrationDataTable() const
{
    return _demonstrationDataTable.get();
}

}    // namespace documentdatadefinitions::controller
