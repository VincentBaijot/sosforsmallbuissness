#pragma once

#include <QtQml/QQmlEngine>

#include "DataTablesModel.hpp"
#include "DocumentDataDefinitionsController_global.hpp"
#include "ListDataDefinitionListModel.hpp"
#include "Utils/SimpleDataObjectListModel.hpp"

namespace documentdatadefinitions::data
{
struct DataTableModelListData;
}

namespace documentdatadefinitions::controller
{

inline constexpr char dataTableModelListModelHeaderData[] = "User data list";

/*!
 * @brief The List of data tables correspondong to the lists of data definitions created
 */
class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataTableModelListModel
    : public utils::SimpleObjectListModel<documentdatadefinitions::controller::DataTablesModel,
                                          dataTableModelListModelHeaderData>
{
    Q_OBJECT

    QML_ELEMENT
  public:
    explicit DataTableModelListModel(QObject* parent = nullptr);
    DataTableModelListModel(const ListDataDefinitionListModel* newListDataDefinitionListModel,
                            const documentdatadefinitions::data::DataTableModelListData& data,
                            QObject* parent = nullptr);
    explicit DataTableModelListModel(const ListDataDefinitionListModel* newListDataDefinitionListModel,
                                     QObject* parent = nullptr);
    ~DataTableModelListModel() override = default;

    [[nodiscard]] documentdatadefinitions::data::DataTableModelListData objectData() const;
    void setObjectData(const documentdatadefinitions::data::DataTableModelListData& newData);

    [[nodiscard]] const ListDataDefinitionListModel* listDataDefinitionListModel() const;
    void setListDataDefinitionListModel(const ListDataDefinitionListModel* newListDataDefinitionListModel);

  private slots:
    void _dataDefinitionListModelAdded(const QModelIndex& parent, int first, int last);
    void _dataDefinitionListModelRemoved(const QModelIndex& parent, int first, int last);
    void _onListDataDefinitionListModelAboutToBeReset();
    void _onListDataDefinitionListModelReset();
    void _onListDataDefinitionListModelDestroyed();

  private:
    Q_DISABLE_COPY_MOVE(DataTableModelListModel)

    void _connectAndCreateTables();
    void _createTables();
    void _setData(const documentdatadefinitions::data::DataTableModelListData& newData);

    QPointer<const ListDataDefinitionListModel> _listDataDefinitionListModel { nullptr };
};

}    // namespace documentdatadefinitions::controller
