#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DataDefinitionsHandler.hpp"
#include "DocumentDataDefinitionsController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("DataTablesModel.hpp")
Q_MOC_INCLUDE("DataTableModelListModel.hpp")

namespace documentdatadefinitions::data
{
struct UserDataHandlerData;
}

namespace documentdatadefinitions::controller
{

class DataTablesModel;
class DataTableModelListModel;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT UserDataHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(documentdatadefinitions::controller::DataTablesModel* generalVariableDataList READ generalVariableDataList
                 CONSTANT FINAL)
    Q_PROPERTY(documentdatadefinitions::controller::DataTableModelListModel* dataTableModelListModel READ
                 dataTableModelListModel CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit UserDataHandler(QObject* parent = nullptr);
    explicit UserDataHandler(const DataDefinitionsHandler* dataDefinitionsHandler, QObject* parent = nullptr);
    UserDataHandler(const DataDefinitionsHandler* dataDefinitionsHandler,
                    const documentdatadefinitions::data::UserDataHandlerData& data,
                    QObject* parent = nullptr);
    ~UserDataHandler() override;

    [[nodiscard]] documentdatadefinitions::controller::DataTablesModel* generalVariableDataList();
    [[nodiscard]] documentdatadefinitions::controller::DataTableModelListModel* dataTableModelListModel();

    [[nodiscard]] const DataDefinitionsHandler* dataDefinitionsHandler() const;
    void setDataDefinitionsHandler(const DataDefinitionsHandler* newDataDefinitionsHandler);

    [[nodiscard]] documentdatadefinitions::data::UserDataHandlerData objectData() const;
    void setObjectData(const documentdatadefinitions::data::UserDataHandlerData& newData);

  private:
    Q_DISABLE_COPY_MOVE(UserDataHandler)

    QPointer<const DataDefinitionsHandler> _dataDefinitionsHandler { nullptr };

    utils::own_qobject<DataTablesModel> _generalVariableDataList;
    utils::own_qobject<DataTableModelListModel> _dataTableModelListModel;
};

}    // namespace documentdatadefinitions::controller
