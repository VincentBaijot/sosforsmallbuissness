#include "UserDataHandler.hpp"

#include "DataTableModelListModel.hpp"
#include "DataTablesModel.hpp"
#include "data/UserDataHandlerData.hpp"

namespace documentdatadefinitions::controller
{
UserDataHandler::UserDataHandler(QObject* parent)
    : QObject { parent },
      _generalVariableDataList { utils::make_qobject<DataTablesModel>(*this) },
      _dataTableModelListModel { utils::make_qobject<DataTableModelListModel>(*this) }
{
}

UserDataHandler::UserDataHandler(const DataDefinitionsHandler* dataDefinitionsHandler, QObject* parent)
    : QObject { parent }, _dataDefinitionsHandler { dataDefinitionsHandler }
{
    if (dataDefinitionsHandler)
    {
        _generalVariableDataList =
          utils::make_qobject<DataTablesModel>(*this, dataDefinitionsHandler->generalVariableList());
        _dataTableModelListModel = utils::make_qobject<DataTableModelListModel>(
          *this, dataDefinitionsHandler->listDataDefinitionListModel());
    }
    else
    {
        _generalVariableDataList = utils::make_qobject<DataTablesModel>(*this);
        _dataTableModelListModel = utils::make_qobject<DataTableModelListModel>(*this);
    }
}

UserDataHandler::UserDataHandler(const DataDefinitionsHandler* dataDefinitionsHandler,
                                 const documentdatadefinitions::data::UserDataHandlerData& data,
                                 QObject* parent)
    : QObject { parent }, _dataDefinitionsHandler { dataDefinitionsHandler }
{
    if (dataDefinitionsHandler)
    {
        // Security to prevent usage of a destroyed data definition
        _generalVariableDataList = utils::make_qobject<DataTablesModel>(
          *this, dataDefinitionsHandler->generalVariableList(), data.generalVariableDataList);
        _dataTableModelListModel = utils::make_qobject<DataTableModelListModel>(
          *this, dataDefinitionsHandler->listDataDefinitionListModel(), data.dataTableModelListModel);
    }
    else
    {
        _generalVariableDataList = utils::make_qobject<DataTablesModel>(*this);
        _dataTableModelListModel = utils::make_qobject<DataTableModelListModel>(*this);
    }
}

UserDataHandler::~UserDataHandler() = default;

DataTablesModel* UserDataHandler::generalVariableDataList()
{
    return _generalVariableDataList.get();
}

DataTableModelListModel* UserDataHandler::dataTableModelListModel()
{
    return _dataTableModelListModel.get();
}

documentdatadefinitions::data::UserDataHandlerData UserDataHandler::objectData() const
{
    return documentdatadefinitions::data::UserDataHandlerData { _generalVariableDataList->objectData(),
                                                                       _dataTableModelListModel->objectData() };
}

void UserDataHandler::setObjectData(const documentdatadefinitions::data::UserDataHandlerData& newData)
{
    _generalVariableDataList->setObjectData(newData.generalVariableDataList);
    _dataTableModelListModel->setObjectData(newData.dataTableModelListModel);
}

const DataDefinitionsHandler* UserDataHandler::dataDefinitionsHandler() const
{
    return _dataDefinitionsHandler;
}

void UserDataHandler::setDataDefinitionsHandler(const DataDefinitionsHandler* newDataDefinitionsHandler)
{
    _dataDefinitionsHandler = newDataDefinitionsHandler;
    if (_dataDefinitionsHandler)
    {
        _generalVariableDataList->setDataDefinitionModel(_dataDefinitionsHandler->generalVariableList());
        _dataTableModelListModel->setListDataDefinitionListModel(
          _dataDefinitionsHandler->listDataDefinitionListModel());
    }
    else
    {
        _generalVariableDataList->setDataDefinitionModel(nullptr);
        _dataTableModelListModel->setListDataDefinitionListModel(nullptr);
    }
}

}    // namespace documentdatadefinitions::controller
