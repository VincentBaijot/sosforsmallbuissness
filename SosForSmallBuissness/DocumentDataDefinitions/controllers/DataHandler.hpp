#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DocumentDataDefinitionsController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/DataDefinitionsHandler.hpp")
Q_MOC_INCLUDE("DocumentDataDefinitions/controllers/UserDataHandler.hpp")

namespace documentdatadefinitions::data
{
struct DataHandlerData;
}

namespace documentdatadefinitions::controller
{

class DataDefinitionsHandler;
class UserDataHandler;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(DataDefinitionsHandler* dataDefinitionsHandler READ dataDefinitionsHandler CONSTANT)
    Q_PROPERTY(UserDataHandler* userDataHandler READ userDataHandler CONSTANT)

    QML_ELEMENT
  public:
    explicit DataHandler(QObject* parent = nullptr);
    explicit DataHandler(const documentdatadefinitions::data::DataHandlerData& data,
                         QObject* parent = nullptr);
    ~DataHandler() override;

    DataDefinitionsHandler* dataDefinitionsHandler();
    UserDataHandler* userDataHandler();

    documentdatadefinitions::data::DataHandlerData objectData() const;
    void setObjectData(const documentdatadefinitions::data::DataHandlerData& newData);

  private:
    Q_DISABLE_COPY_MOVE(DataHandler)

    utils::own_qobject<DataDefinitionsHandler> _dataDefinitionsHandler;
    utils::own_qobject<UserDataHandler> _userDataHandler;
};

}    // namespace documentdatadefinitions::controller
