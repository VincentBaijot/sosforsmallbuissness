#pragma once

#include <QAbstractTableModel>
#include <QtQml/QQmlEngine>

#include "DocumentDataDefinitions/data/DataTablesData.hpp"
#include "DocumentDataDefinitionsController_global.hpp"

namespace documentdatadefinitions::controller
{
class DataDefinitionListModel;
class DataDefinition;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataTablesModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(QString listName READ listName NOTIFY listNameChanged FINAL)

    QML_ELEMENT
  public:
    explicit DataTablesModel(QObject* parent = nullptr);
    explicit DataTablesModel(const DataDefinitionListModel* dataDefinitionModel, QObject* parent = nullptr);
    DataTablesModel(const DataDefinitionListModel* dataDefinitionModel,
                    const documentdatadefinitions::data::DataTablesData& dataTablesData,
                    QObject* parent = nullptr);
    ~DataTablesModel() override = default;

    [[nodiscard]] const DataDefinitionListModel* dataDefinitionModel() const;
    void setDataDefinitionModel(const DataDefinitionListModel* newDataDefinitionModel);

    [[nodiscard]] documentdatadefinitions::data::DataTablesData objectData() const;
    void setObjectData(const documentdatadefinitions::data::DataTablesData& newData);
    [[nodiscard]] documentdatadefinitions::data::DataTableData dataRow(int rowIndex) const;

    [[nodiscard]] const QList<QHash<const DataDefinition* const, QString>>& dataTables() const;

    [[nodiscard]] QString listName() const;

    Q_INVOKABLE void addDataRow();
    Q_INVOKABLE void removeDataRow(int rowIndex);

    [[nodiscard]] int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    [[nodiscard]] QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    [[nodiscard]] QVariant headerData(int section,
                                      Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override;

  signals:
    void listNameChanged();

  private slots:
    void _onDataDefinitionListModelChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight);
    void _onDataDefinitionListModelRowsAboutTeBeRemoved(const QModelIndex& parent, int first, int last);
    void _onDataDefinitionListModelRowsRemoved();

    void _onDataDefinitionListModelDestroyed();
    void _onDataDefinitionListModelAboutToBeReset();

  private:
    Q_DISABLE_COPY_MOVE(DataTablesModel)

    void _connectDatatDefinitionListModel() const;
    void _setData(const documentdatadefinitions::data::DataTablesData& newData);
    QHash<const DataDefinition* const, QString> _getDataDefinitionAssociation(
      const documentdatadefinitions::data::DataTableData& dataTableData) const;

    QPointer<const DataDefinitionListModel> _dataDefinitionModel;

    QList<QHash<const DataDefinition* const, QString>> _dataTables;
    QList<const DataDefinition*> _dataDefinitionsAboutToBeRemoved;
};

}    // namespace documentdatadefinitions::controller
