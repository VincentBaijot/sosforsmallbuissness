#pragma once

#include <QObject>

#include "DocumentDataDefinitions/data/DataDefinitionData.hpp"
#include "DocumentDataDefinitionsController_global.hpp"

namespace documentdatadefinitions::controller
{

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT DataDefinition : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString key READ key WRITE setKey NOTIFY keyChanged FINAL)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged FINAL)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged FINAL)
    Q_PROPERTY(DataDefinition::DataType dataType READ dataType WRITE setDataType NOTIFY dataTypeChanged FINAL)
    Q_PROPERTY(QStringList dataTypeNames READ dataTypeNames CONSTANT FINAL)

  public:
    using DataType = documentdatadefinitions::data::DataDefinitionData::DataType;
    Q_ENUM(DataDefinition::DataType)

    explicit DataDefinition(QObject* parent = nullptr);
    explicit DataDefinition(const documentdatadefinitions::data::DataDefinitionData& data,
                            QObject* parent = nullptr);
    ~DataDefinition() override = default;

    documentdatadefinitions::data::DataDefinitionData objectData() const;
    void setObjectData(const documentdatadefinitions::data::DataDefinitionData& newData);

    QString key() const;
    void setKey(const QString& newKey);

    QString name() const;
    void setName(const QString& newName);

    QString description() const;
    void setDescription(const QString& newDescription);

    DataDefinition::DataType dataType() const;
    void setDataType(DataDefinition::DataType newDataType);

    static QStringList dataTypeNames();

  signals:

    void keyChanged();
    void nameChanged();
    void descriptionChanged();
    void dataTypeChanged();

  private:
    Q_DISABLE_COPY_MOVE(DataDefinition)

    documentdatadefinitions::data::DataDefinitionData _dataDefinitionData;
};

}    // namespace documentdatadefinitions::controller
