#include "DataDefinition.hpp"

namespace documentdatadefinitions::controller
{

DataDefinition::DataDefinition(QObject* parent) : QObject { parent }
{
}

DataDefinition::DataDefinition(const documentdatadefinitions::data::DataDefinitionData& data, QObject* parent)
    : QObject { parent }, _dataDefinitionData { data }
{
}

documentdatadefinitions::data::DataDefinitionData DataDefinition::objectData() const
{
    return _dataDefinitionData;
}

void DataDefinition::setObjectData(const documentdatadefinitions::data::DataDefinitionData& newData)
{
    if (objectData() == newData) { return; }
    _dataDefinitionData = newData;

    emit dataTypeChanged();
    emit keyChanged();
    emit nameChanged();
    emit descriptionChanged();
}

QString DataDefinition::key() const
{
    return _dataDefinitionData.key;
}

void DataDefinition::setKey(const QString& newKey)
{
    if (_dataDefinitionData.key == newKey) return;
    _dataDefinitionData.key = newKey;
    emit keyChanged();
}

QString DataDefinition::name() const
{
    return _dataDefinitionData.name;
}

void DataDefinition::setName(const QString& newName)
{
    if (_dataDefinitionData.name == newName) return;
    _dataDefinitionData.name = newName;
    emit nameChanged();
}

QString DataDefinition::description() const
{
    return _dataDefinitionData.description;
}

void DataDefinition::setDescription(const QString& newDescription)
{
    if (_dataDefinitionData.description == newDescription) return;
    _dataDefinitionData.description = newDescription;
    emit descriptionChanged();
}

DataDefinition::DataType DataDefinition::dataType() const
{
    return _dataDefinitionData.dataType;
}

void DataDefinition::setDataType(DataDefinition::DataType newDataType)
{
    if (_dataDefinitionData.dataType == newDataType) return;
    _dataDefinitionData.dataType = newDataType;
    emit dataTypeChanged();
}

QStringList DataDefinition::dataTypeNames()
{
    QStringList dataTypeNames;

    for (auto dataType = static_cast<int>(DataType::First); dataType <= static_cast<int>(DataType::Last);
         ++dataType)
    {
        // Use a switch so we have a warning if a value is missing
        switch (DataType(dataType))
        {
            case DataType::StringType:
                dataTypeNames.append(QObject::tr("String"));
                break;
            case DataType::NumberType:
                dataTypeNames.append(QObject::tr("Number"));
                break;
        }
    }

    return dataTypeNames;
}

}    // namespace documentdatadefinitions::controller
