#include "DataDefinitionsHandler.hpp"

#include "DataDefinitionListModel.hpp"
#include "data/DataDefinitionsHandlerData.hpp"
#include "ListDataDefinitionListModel.hpp"

namespace documentdatadefinitions::controller
{
DataDefinitionsHandler::DataDefinitionsHandler(QObject* parent)
    : QObject(parent),
      _generalVariableList { utils::make_qobject<DataDefinitionListModel>(*this) },
      _listDataDefinitionListModel { utils::make_qobject<ListDataDefinitionListModel>(*this) }
{
}

DataDefinitionsHandler::DataDefinitionsHandler(
  const documentdatadefinitions::data::DataDefinitionsHandlerData& data,
  QObject* parent)
    : QObject(parent),
      _generalVariableList { utils::make_qobject<DataDefinitionListModel>(*this, data.generalVariableList) },
      _listDataDefinitionListModel {
          utils::make_qobject<ListDataDefinitionListModel>(*this, data.listDataDefinitionList)
      }
{
}

DataDefinitionsHandler::~DataDefinitionsHandler() = default;

DataDefinitionListModel* DataDefinitionsHandler::generalVariableList()
{
    return _generalVariableList.get();
}

const DataDefinitionListModel* DataDefinitionsHandler::generalVariableList() const
{
    return _generalVariableList.get();
}

ListDataDefinitionListModel* DataDefinitionsHandler::listDataDefinitionListModel()
{
    return _listDataDefinitionListModel.get();
}

const ListDataDefinitionListModel* DataDefinitionsHandler::listDataDefinitionListModel() const
{
    return _listDataDefinitionListModel.get();
}

documentdatadefinitions::data::DataDefinitionsHandlerData DataDefinitionsHandler::objectData() const
{
    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData;
    dataDefinitionsHandlerData.generalVariableList    = _generalVariableList->objectData();
    dataDefinitionsHandlerData.listDataDefinitionList = _listDataDefinitionListModel->objectData();
    return dataDefinitionsHandlerData;
}

void DataDefinitionsHandler::setObjetData(
  const documentdatadefinitions::data::DataDefinitionsHandlerData& newData)
{
    _generalVariableList->setObjectData(newData.generalVariableList);
    _listDataDefinitionListModel->setObjectData(newData.listDataDefinitionList);
}

}    // namespace documentdatadefinitions::controller
