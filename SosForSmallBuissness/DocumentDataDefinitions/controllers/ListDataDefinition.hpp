#pragma once

#include <QObject>
#include <QtQml/QQmlEngine>

#include "DocumentDataDefinitionsController_global.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace documentdatadefinitions::data
{
struct ListDataDefinitionData;
}

namespace documentdatadefinitions::controller
{

class DataDefinitionListModel;
class DataTablesModel;

class DOCUMENT_DATA_DEFINITIONS_CONTROLLER_EXPORT ListDataDefinition : public QObject
{
    Q_OBJECT

    Q_PROPERTY(documentdatadefinitions::controller::DataDefinitionListModel* dataDefinitionListModel READ
                 dataDefinitionListModel CONSTANT)
    Q_PROPERTY(
      documentdatadefinitions::controller::DataTablesModel* demonstrationDataTable READ demonstrationDataTable CONSTANT)

    QML_ELEMENT
  public:
    explicit ListDataDefinition(QObject* parent = nullptr);
    explicit ListDataDefinition(const documentdatadefinitions::data::ListDataDefinitionData& newData,
                                QObject* parent = nullptr);
    ~ListDataDefinition() override;

    [[nodiscard]] documentdatadefinitions::data::ListDataDefinitionData objectData() const;
    void setObjectData(const documentdatadefinitions::data::ListDataDefinitionData& newData);

    DataDefinitionListModel* dataDefinitionListModel();
    [[nodiscard]] const DataDefinitionListModel* dataDefinitionListModel() const;
    DataTablesModel* demonstrationDataTable();
    [[nodiscard]] const DataTablesModel* demonstrationDataTable() const;

  private:
    Q_DISABLE_COPY_MOVE(ListDataDefinition)

    utils::own_qobject<DataDefinitionListModel> _dataDefinitionListModel;
    utils::own_qobject<DataTablesModel> _demonstrationDataTable;
};

}    // namespace documentdatadefinitions::controller
