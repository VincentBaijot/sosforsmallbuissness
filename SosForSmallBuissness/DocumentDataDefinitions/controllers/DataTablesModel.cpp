#include "DataTablesModel.hpp"

#include "Constants.hpp"
#include "DataDefinition.hpp"
#include "DataDefinitionListModel.hpp"

namespace documentdatadefinitions::controller
{
DataTablesModel::DataTablesModel(QObject* parent) : QAbstractTableModel { parent }
{
    // Add a first row
    addDataRow();
}

DataTablesModel::DataTablesModel(const DataDefinitionListModel* dataDefinitionModel, QObject* parent)
    : QAbstractTableModel { parent }, _dataDefinitionModel { dataDefinitionModel }
{
    _connectDatatDefinitionListModel();
    // Add a first row
    addDataRow();
}

DataTablesModel::DataTablesModel(const DataDefinitionListModel* dataDefinitionModel,
                                 const documentdatadefinitions::data::DataTablesData& dataTablesData,
                                 QObject* parent)
    : QAbstractTableModel { parent }, _dataDefinitionModel { dataDefinitionModel }
{
    _connectDatatDefinitionListModel();
    _setData(dataTablesData);
}

const DataDefinitionListModel* DataTablesModel::dataDefinitionModel() const
{
    return _dataDefinitionModel;
}

void DataTablesModel::setDataDefinitionModel(const DataDefinitionListModel* newDataDefinitionModel)
{
    if (_dataDefinitionModel)
    {
        QObject::disconnect(
          _dataDefinitionModel, &QObject::destroyed, this, &DataTablesModel::_onDataDefinitionListModelDestroyed);

        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::rowsAboutToBeInserted,
                            this,
                            &QAbstractItemModel::columnsAboutToBeInserted);
        QObject::disconnect(
          _dataDefinitionModel, &QAbstractItemModel::rowsInserted, this, &QAbstractItemModel::columnsInserted);

        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::rowsAboutToBeRemoved,
                            this,
                            &QAbstractItemModel::columnsAboutToBeRemoved);
        QObject::disconnect(
          _dataDefinitionModel, &QAbstractItemModel::rowsRemoved, this, &QAbstractItemModel::columnsRemoved);

        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::rowsAboutToBeRemoved,
                            this,
                            &DataTablesModel::_onDataDefinitionListModelRowsAboutTeBeRemoved);
        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::rowsRemoved,
                            this,
                            &DataTablesModel::_onDataDefinitionListModelRowsRemoved);

        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::dataChanged,
                            this,
                            &DataTablesModel::_onDataDefinitionListModelChanged);

        QObject::disconnect(_dataDefinitionModel,
                            &QAbstractItemModel::modelAboutToBeReset,
                            this,
                            &DataTablesModel::_onDataDefinitionListModelAboutToBeReset);

        QObject::disconnect(_dataDefinitionModel,
                            &DataDefinitionListModel::listNameChanged,
                            this,
                            &DataTablesModel::listNameChanged);
    }

    _dataDefinitionModel = newDataDefinitionModel;

    beginResetModel();
    _dataTables.clear();
    endResetModel();

    _connectDatatDefinitionListModel();
}

documentdatadefinitions::data::DataTablesData DataTablesModel::objectData() const
{
    documentdatadefinitions::data::DataTablesData dataTablesData;

    if (_dataDefinitionModel)
    {
        for (const QHash<const DataDefinition* const, QString>& dataTable : _dataTables)
        {
            documentdatadefinitions::data::DataTableData dataTableData;

            for (auto iterator = dataTable.cbegin(); iterator != dataTable.cend(); ++iterator)
            {
                dataTableData.insert(_dataDefinitionModel->indexOf(iterator.key()), iterator.value());
            }

            dataTablesData.dataTables.append(dataTableData);
        }
    }

    return dataTablesData;
}

void DataTablesModel::setObjectData(const documentdatadefinitions::data::DataTablesData& newData)
{
    if (objectData() == newData) { return; }

    beginResetModel();
    _dataTables.clear();

    _setData(newData);

    endResetModel();
}

documentdatadefinitions::data::DataTableData DataTablesModel::dataRow(int rowIndex) const
{
    const QHash<const DataDefinition* const, QString>& dataTable = _dataTables.at(rowIndex);

    documentdatadefinitions::data::DataTableData dataTableData;

    for (auto iterator = dataTable.cbegin(); iterator != dataTable.cend(); ++iterator)
    {
        dataTableData.insert(_dataDefinitionModel->indexOf(iterator.key()), iterator.value());
    }

    return dataTableData;
}

const QList<QHash<const DataDefinition* const, QString>>& DataTablesModel::dataTables() const
{
    return _dataTables;
}

QString DataTablesModel::listName() const
{
    return _dataDefinitionModel->listName();
}

void DataTablesModel::addDataRow()
{
    qCInfo(documentDataDefinitionControllerCategory) << "Add row in data table";

    beginInsertRows(QModelIndex(), static_cast<int>(_dataTables.size()), static_cast<int>(_dataTables.size()));
    _dataTables.append(QHash<const DataDefinition* const, QString>());
    endInsertRows();
}

void DataTablesModel::removeDataRow(int rowIndex)
{
    qCInfo(documentDataDefinitionControllerCategory) << "Remove row " << rowIndex << " in data table";
    if (rowIndex < 0 || rowIndex >= _dataTables.size()) { return; }
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    _dataTables.removeAt(rowIndex);
    endRemoveRows();
}

int DataTablesModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    return static_cast<int>(_dataTables.size());
}

int DataTablesModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    if (!_dataDefinitionModel) { return 0; }

    return _dataDefinitionModel->rowCount();
}

QVariant DataTablesModel::data(const QModelIndex& index, int role) const
{
    if (index.column() < 0 || index.column() > columnCount()) { return {}; }

    if (index.row() < 0 || index.row() > rowCount()) { return {}; }

    if (role == Qt::DisplayRole)
    {
        if (const DataDefinition* const dataDefinition = _dataDefinitionModel->at(index.column()))
        {
            return _dataTables[index.row()].value(dataDefinition);
        }
        return {};
    }

    return {};
}

bool DataTablesModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (index.column() < 0 || index.column() >= columnCount())
    {
        qCWarning(documentDataDefinitionControllerCategory)
          << "Try to set data for an invalid column " << index.column() << ", while column count is "
          << columnCount();
        return false;
    }

    if (index.row() < 0 || index.row() >= rowCount())
    {
        qCWarning(documentDataDefinitionControllerCategory)
          << "Try to set data for an invalid row " << index.row() << ", while row count is " << rowCount();
        return false;
    }

    if (role == Qt::EditRole)
    {
        if (const DataDefinition* const dataDefinition = _dataDefinitionModel->at(index.column()))
        {
            qCDebug(documentDataDefinitionControllerCategory)
              << "Insert data " << value.toString() << " for data definition " << index.row() << " for the key "
              << dataDefinition->key();
            _dataTables[index.row()].insert(dataDefinition, value.toString());
            emit dataChanged(index, index, { Qt::DisplayRole });
            return true;
        }

        return false;
    }

    return false;
}

QVariant DataTablesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Orientation::Vertical) { return {}; }

    if (section < 0 || section > columnCount()) { return {}; }

    if (role == Qt::DisplayRole)
    {
        if (const DataDefinition* const dataDefinition = _dataDefinitionModel->at(section))
        {
            return dataDefinition->key();
        }
        return {};
    }

    return {};
}

void DataTablesModel::_onDataDefinitionListModelChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
{
    emit headerDataChanged(Qt::Orientation::Horizontal, topLeft.row(), bottomRight.row());
}

void DataTablesModel::_onDataDefinitionListModelRowsAboutTeBeRemoved(const QModelIndex& parent,
                                                                     int first,
                                                                     int last)
{
    Q_UNUSED(parent)

    if (_dataDefinitionModel)
    {
        _dataDefinitionsAboutToBeRemoved.clear();
        for (int index = first; index <= last; ++index)
        {
            _dataDefinitionsAboutToBeRemoved.append(_dataDefinitionModel->at(index));
        }
    }
}

void DataTablesModel::_onDataDefinitionListModelRowsRemoved()
{
    for (int index = 0; index < _dataDefinitionModel->rowCount(); ++index)
    {
        _dataDefinitionsAboutToBeRemoved.removeAll(_dataDefinitionModel->at(index));
    }

    for (QHash<const DataDefinition* const, QString>& dataRow : _dataTables)
    {
        for (const DataDefinition* const dataDefinition : _dataDefinitionsAboutToBeRemoved)
        {
            // No need to signal a column or row removed because the data will just return an empty QString,
            // We just "empty the cell"
            dataRow.remove(dataDefinition);
        }
    }
}

void DataTablesModel::_onDataDefinitionListModelDestroyed()
{
    setDataDefinitionModel(nullptr);
}

void DataTablesModel::_onDataDefinitionListModelAboutToBeReset()
{
    beginResetModel();
    _dataTables.clear();
    endResetModel();
}

void DataTablesModel::_connectDatatDefinitionListModel() const
{
    if (_dataDefinitionModel)
    {
        QObject::connect(
          _dataDefinitionModel, &QObject::destroyed, this, &DataTablesModel::_onDataDefinitionListModelDestroyed);

        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::rowsAboutToBeInserted,
                         this,
                         &QAbstractItemModel::columnsAboutToBeInserted);
        QObject::connect(
          _dataDefinitionModel, &QAbstractItemModel::rowsInserted, this, &QAbstractItemModel::columnsInserted);

        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::rowsAboutToBeRemoved,
                         this,
                         &QAbstractItemModel::columnsAboutToBeRemoved);
        QObject::connect(
          _dataDefinitionModel, &QAbstractItemModel::rowsRemoved, this, &QAbstractItemModel::columnsRemoved);

        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::rowsAboutToBeRemoved,
                         this,
                         &DataTablesModel::_onDataDefinitionListModelRowsAboutTeBeRemoved);
        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::rowsRemoved,
                         this,
                         &DataTablesModel::_onDataDefinitionListModelRowsRemoved);

        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::dataChanged,
                         this,
                         &DataTablesModel::_onDataDefinitionListModelChanged);

        QObject::connect(_dataDefinitionModel,
                         &QAbstractItemModel::modelAboutToBeReset,
                         this,
                         &DataTablesModel::_onDataDefinitionListModelAboutToBeReset);

        QObject::connect(_dataDefinitionModel,
                         &DataDefinitionListModel::listNameChanged,
                         this,
                         &DataTablesModel::listNameChanged);
    }
}

void DataTablesModel::_setData(const documentdatadefinitions::data::DataTablesData& newData)
{
    if (_dataDefinitionModel)
    {
        _dataTables.reserve(newData.dataTables.size());
        for (const documentdatadefinitions::data::DataTableData& dataTableData : newData.dataTables)
        {
            _dataTables.append(_getDataDefinitionAssociation(dataTableData));
        }
    }
    else { _dataTables.resize(newData.dataTables.size()); }
}

QHash<const DataDefinition* const, QString> DataTablesModel::_getDataDefinitionAssociation(
  const data::DataTableData& dataTableData) const
{
    QHash<const DataDefinition* const, QString> dataTable;
    for (auto iterator = dataTableData.cbegin(); iterator != dataTableData.cend(); ++iterator)
    {
        if (iterator.key() >= 0 && iterator.key() <= _dataDefinitionModel->rowCount())
        {
            const DataDefinition* const dataDefinition = _dataDefinitionModel->at(iterator.key());
            dataTable.insert(dataDefinition, iterator.value());
        }
        else
        {
            qCWarning(documentDataDefinitionControllerCategory)
              << "Cannot find the data definition with the index " << iterator.key();
        }
    }
    return dataTable;
}

}    // namespace documentdatadefinitions::controller
