#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

namespace documentdatadefinitions::data
{
using DataDefinitionIndex = int;
using DataTableData       = QHash<DataDefinitionIndex, QString>;

struct DataDefinitionData;
struct DataDefinitionListData;
struct DataTablesData;
struct ListDataDefinitionData;
struct ListDataDefinitionListData;
struct DataDefinitionsHandlerData;
struct UserDataHandlerData;
struct DataTableModelListData;
struct DataHandlerData;
}    // namespace documentdatadefinitions::data

namespace documentdatadefinitions::service::details
{
documentdatadefinitions::data::DataDefinitionData readDataDefinitionData(QXmlStreamReader& reader);
void writeDataDefinitionData(QXmlStreamWriter& writer,
                             const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData);

documentdatadefinitions::data::DataDefinitionListData readDataDefinitionListData(QXmlStreamReader& reader);
void writeDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData);

documentdatadefinitions::data::DataTableData readDataTableData(QXmlStreamReader& reader);
void writeDataTableData(QXmlStreamWriter& writer,
                        const documentdatadefinitions::data::DataTableData& dataTableData);

documentdatadefinitions::data::DataTablesData readDataTablesData(QXmlStreamReader& reader);
void writeDataTablesData(QXmlStreamWriter& writer,
                         const documentdatadefinitions::data::DataTablesData& dataTablesData);

documentdatadefinitions::data::ListDataDefinitionData readListDataDefinitionData(QXmlStreamReader& reader);
void writeListDataDefinitionData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData);

documentdatadefinitions::data::ListDataDefinitionListData readListDataDefinitionListData(QXmlStreamReader& reader);
void writeListDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData);

documentdatadefinitions::data::DataDefinitionsHandlerData readDataDefinitionsHandlerData(QXmlStreamReader& reader);
void writeDataDefinitionsHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData);

documentdatadefinitions::data::DataTableModelListData readDataTableModelListData(QXmlStreamReader& reader);
void writeDataTableModelListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataTableModelListData& dataTableModelListData);

documentdatadefinitions::data::UserDataHandlerData readUserDataHandlerData(QXmlStreamReader& reader);
void writeUserDataHandlerData(QXmlStreamWriter& writer,
                              const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData);

documentdatadefinitions::data::DataHandlerData readDataHandlerData(QXmlStreamReader& reader);
void writeDataHandlerData(QXmlStreamWriter& writer,
                          const documentdatadefinitions::data::DataHandlerData& dataHandlerData);
}    // namespace documentdatadefinitions::service::details
