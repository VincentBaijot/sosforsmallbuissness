#include "DocumentDataDefinitionsXmlSerialization.hpp"

#include "data/DataDefinitionData.hpp"
#include "data/DataDefinitionListData.hpp"
#include "data/DataDefinitionsHandlerData.hpp"
#include "data/DataHandlerData.hpp"
#include "data/DataTablesData.hpp"
#include "data/ListDataDefinitionData.hpp"
#include "data/ListDataDefinitionListData.hpp"
#include "data/UserDataHandlerData.hpp"
#include "DocumentDataDefinitionsXmlConstants.hpp"
#include "DocumentDataDefinitionsXmlSerializationDetails.hpp"
#include "Utils/XmlSerializationUtils.hpp"

namespace documentdatadefinitions::service
{

std::optional<documentdatadefinitions::data::DataDefinitionData> readDataDefinitionData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataDefinitionElement, u"DataDefinitionData"))
    {
        return std::nullopt;
    }

    return details::readDataDefinitionData(reader);
}

void writeDataDefinitionData(QXmlStreamWriter& writer,
                             const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData)
{
    details::writeDataDefinitionData(writer, dataDefinitionData);
}

std::optional<documentdatadefinitions::data::DataDefinitionListData> readDataDefinitionListData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataDefinitionListElement, u"DataDefinitionListData"))
    {
        return std::nullopt;
    }

    return details::readDataDefinitionListData(reader);
}

void writeDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionData)
{
    details::writeDataDefinitionListData(writer, dataDefinitionData);
}

std::optional<documentdatadefinitions::data::DataTablesData> readDataTablesData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataTablesElement, u"DataTablesData")) { return std::nullopt; }

    return details::readDataTablesData(reader);
}

void writeDataTablesData(QXmlStreamWriter& writer,
                         const documentdatadefinitions::data::DataTablesData& dataTablesData)
{
    details::writeDataTablesData(writer, dataTablesData);
}

std::optional<documentdatadefinitions::data::ListDataDefinitionData> readListDataDefinitionData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, listDataDefinitionElement, u"ListDataDefinitionData"))
    {
        return std::nullopt;
    }

    return details::readListDataDefinitionData(reader);
}

void writeListDataDefinitionData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData)
{
    details::writeListDataDefinitionData(writer, listDataDefinitionData);
}

std::optional<documentdatadefinitions::data::ListDataDefinitionListData> readListDataDefinitionListData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, listDataDefinitionListElement, u"ListDataDefinitionListData"))
    {
        return std::nullopt;
    }

    return details::readListDataDefinitionListData(reader);
}

void writeListDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData)
{
    details::writeListDataDefinitionListData(writer, listDataDefinitionListData);
}

std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData> readDataDefinitionsHandlerData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataDefinitionsHandlerElement, u"DataDefinitionsHandlerData"))
    {
        return std::nullopt;
    }

    return details::readDataDefinitionsHandlerData(reader);
}

void writeDataDefinitionsHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData)
{
    details::writeDataDefinitionsHandlerData(writer, dataDefinitionsHandlerData);
}

std::optional<documentdatadefinitions::data::UserDataHandlerData> readUserDataHandlerData(
  QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, userDataHandlerElement, u"UserDataHandlerData"))
    {
        return std::nullopt;
    }

    return details::readUserDataHandlerData(reader);
}

void writeUserDataHandlerData(QXmlStreamWriter& writer,
                              const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData)
{
    details::writeUserDataHandlerData(writer, userDataHandlerData);
}

std::optional<documentdatadefinitions::data::DataHandlerData> readDataHandlerData(QXmlStreamReader& reader)
{
    if (!utils::xml::isDataStartElement(reader, dataHandlerElement, u"DataHandlerData")) { return std::nullopt; }

    return details::readDataHandlerData(reader);
}

void writeDataHandlerData(QXmlStreamWriter& writer,
                          const documentdatadefinitions::data::DataHandlerData& dataHandlerData)
{
    details::writeDataHandlerData(writer, dataHandlerData);
}

}    // namespace documentdatadefinitions::service
