#pragma once

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "DocumentDataDefinitionsService_global.hpp"

namespace documentdatadefinitions::data
{
struct DataDefinitionData;
struct DataDefinitionListData;
struct DataTablesData;
struct ListDataDefinitionData;
struct ListDataDefinitionListData;
struct DataDefinitionsHandlerData;
struct UserDataHandlerData;
struct DataHandlerData;
}    // namespace documentdatadefinitions::data

namespace documentdatadefinitions::service
{
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT std::optional<documentdatadefinitions::data::DataDefinitionData>
readDataDefinitionData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeDataDefinitionData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT
  std::optional<documentdatadefinitions::data::DataDefinitionListData>
  readDataDefinitionListData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT std::optional<documentdatadefinitions::data::DataTablesData>
readDataTablesData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeDataTablesData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataTablesData& dataTablesData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT
  std::optional<documentdatadefinitions::data::ListDataDefinitionData>
  readListDataDefinitionData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeListDataDefinitionData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT
  std::optional<documentdatadefinitions::data::ListDataDefinitionListData>
  readListDataDefinitionListData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeListDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT
  std::optional<documentdatadefinitions::data::DataDefinitionsHandlerData>
  readDataDefinitionsHandlerData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeDataDefinitionsHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT std::optional<documentdatadefinitions::data::UserDataHandlerData>
readUserDataHandlerData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeUserDataHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData);

DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT std::optional<documentdatadefinitions::data::DataHandlerData>
readDataHandlerData(QXmlStreamReader& reader);
DOCUMENT_DATA_DEFINITIONS_SERVICES_EXPORT void writeDataHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataHandlerData& dataHandlerData);

}    // namespace documentdatadefinitions::service
