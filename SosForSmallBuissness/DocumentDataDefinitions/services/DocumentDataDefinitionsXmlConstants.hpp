#pragma once

#include <QStringView>

namespace documentdatadefinitions::service
{
inline constexpr const QStringView dataDefinitionElement         = u"DataDefinition";
inline constexpr const QStringView dataDefinitionListElement     = u"DataDefinitionList";
inline constexpr const QStringView dataTablesElement             = u"DataTablesData";
inline constexpr const QStringView listDataDefinitionElement     = u"ListDataDefinition";
inline constexpr const QStringView listDataDefinitionListElement = u"ListDataDefinitionList";
inline constexpr const QStringView dataDefinitionsHandlerElement = u"DataDefinitionsHandler";
inline constexpr const QStringView userDataHandlerElement        = u"UserDataHandler";
inline constexpr const QStringView dataHandlerElement            = u"DataHandler";
}
