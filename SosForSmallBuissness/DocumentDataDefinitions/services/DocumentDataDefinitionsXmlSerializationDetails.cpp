#include "DocumentDataDefinitionsXmlSerializationDetails.hpp"

#include "Constants.hpp"
#include "DocumentDataDefinitionsXmlConstants.hpp"
#include "Utils/XmlSerializationUtils.hpp"
#include "data/DataDefinitionData.hpp"
#include "data/DataDefinitionListData.hpp"
#include "data/DataDefinitionsHandlerData.hpp"
#include "data/DataHandlerData.hpp"
#include "data/DataTablesData.hpp"
#include "data/ListDataDefinitionData.hpp"
#include "data/ListDataDefinitionListData.hpp"
#include "data/UserDataHandlerData.hpp"

namespace documentdatadefinitions::service::details
{

inline constexpr const QStringView dataDefinitionKeyElement         = u"Key";
inline constexpr const QStringView dataDefinitionNameElement        = u"Name";
inline constexpr const QStringView dataDefinitionDescriptionElement = u"Description";
inline constexpr const QStringView dataDefinitionDataTypeElement    = u"DataType";
inline constexpr const QStringView dataDefinitionListNameElement    = u"Name";

inline constexpr const QStringView dataTableDataElement       = u"DataTableData";
inline constexpr const QStringView dataTableEntryElement      = u"DataTableEntry";
inline constexpr const QStringView dataDefinitionIndexElement = u"DataDefinitionIndex";
inline constexpr const QStringView dataDefinitionValueElement = u"DataDefinitionValue";

inline constexpr const QStringView dataTableModelListElement = u"DataTableModelList";

documentdatadefinitions::data::DataDefinitionData readDataDefinitionData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataDefinitionElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataDefinitionData dataDefinitionData;

    while (!utils::xml::checkEndElement(reader, dataDefinitionElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionKeyElement)
        {
            dataDefinitionData.key = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionNameElement)
        {
            dataDefinitionData.name = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionDescriptionElement)
        {
            dataDefinitionData.description = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionDataTypeElement)
        {
            dataDefinitionData.dataType =
              documentdatadefinitions::data::DataDefinitionData::DataType(reader.readElementText().toInt());
        }
        else { reader.readNext(); }
    }

    return dataDefinitionData;
}

void writeDataDefinitionData(QXmlStreamWriter& writer,
                             const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData)
{
    writer.writeStartElement(dataDefinitionElement.toString());

    writer.writeStartElement(dataDefinitionKeyElement.toString());
    writer.writeCharacters(dataDefinitionData.key);
    // end keyElement
    writer.writeEndElement();

    writer.writeStartElement(dataDefinitionNameElement.toString());
    writer.writeCharacters(dataDefinitionData.name);
    // end nameElement
    writer.writeEndElement();

    writer.writeStartElement(dataDefinitionDescriptionElement.toString());
    writer.writeCharacters(dataDefinitionData.description);
    // end descriptionElement
    writer.writeEndElement();

    writer.writeStartElement(dataDefinitionDataTypeElement.toString());
    writer.writeCharacters(QString::number(static_cast<int>(dataDefinitionData.dataType)));
    // end dataTypeElement
    writer.writeEndElement();

    // end qSizeFElement
    writer.writeEndElement();
}

documentdatadefinitions::data::DataDefinitionListData readDataDefinitionListData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataDefinitionListElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataDefinitionListData dataDefinitionListData;

    while (!utils::xml::checkEndElement(reader, dataDefinitionListElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionListNameElement)
        {
            dataDefinitionListData.listName = reader.readElementText();
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionElement)
        {
            dataDefinitionListData.dataDefinitions.append(readDataDefinitionData(reader));
        }
        else { reader.readNext(); }
    }

    return dataDefinitionListData;
}

void writeDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionListData& dataDefinitionListData)
{
    writer.writeStartElement(dataDefinitionListElement.toString());

    writer.writeStartElement(dataDefinitionListNameElement.toString());
    writer.writeCharacters(dataDefinitionListData.listName);
    // end dataTypeElement
    writer.writeEndElement();

    for (const documentdatadefinitions::data::DataDefinitionData& dataDefinitionData :
         dataDefinitionListData.dataDefinitions)
    {
        writeDataDefinitionData(writer, dataDefinitionData);
    }

    writer.writeEndElement();
}

documentdatadefinitions::data::DataTablesData readDataTablesData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataTablesElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataTablesData dataTablesData;

    while (!utils::xml::checkEndElement(reader, dataTablesElement))
    {
        if (reader.isStartElement() && reader.name() == dataTableDataElement)
        {
            dataTablesData.dataTables.append(readDataTableData(reader));
        }
        else { reader.readNext(); }
    }

    return dataTablesData;
}

void writeDataTablesData(QXmlStreamWriter& writer,
                         const documentdatadefinitions::data::DataTablesData& dataTablesData)
{
    writer.writeStartElement(dataTablesElement.toString());

    for (const documentdatadefinitions::data::DataTableData& dataTableData : dataTablesData.dataTables)
    {
        writeDataTableData(writer, dataTableData);
    }

    writer.writeEndElement();
}

documentdatadefinitions::data::DataTableData readDataTableEntryElement(QXmlStreamReader& reader)
{
    documentdatadefinitions::data::DataTableData dataTableData;

    bool keyFound   = false;
    bool valueFound = false;
    int key         = -1;
    QString value;

    while (!utils::xml::checkEndElement(reader, dataTableEntryElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionIndexElement)
        {
            key = reader.readElementText().toInt(&keyFound);
        }
        else if (reader.isStartElement() && reader.name() == dataDefinitionValueElement)
        {
            valueFound = true;
            value      = reader.readElementText();
        }
        else { reader.readNext(); }
    }

    if (keyFound && valueFound) { dataTableData.insert(key, value); }
    else if (!valueFound)
    {
        qCWarning(documentDataDefinitionServiceCategory)
          << "Read invalid data table entry, missing value for key " << key;
    }
    else
    {
        qCWarning(documentDataDefinitionServiceCategory)
          << "Read invalid data table entry, missing key for value " << value;
    }
    return dataTableData;
}

documentdatadefinitions::data::DataTableData readDataTableData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataTableDataElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataTableData dataTableData;

    while (!utils::xml::checkEndElement(reader, dataTableDataElement))
    {
        if (reader.isStartElement() && reader.name() == dataTableEntryElement)
        {
            dataTableData.insert(readDataTableEntryElement(reader));
        }
        else { reader.readNext(); }
    }

    return dataTableData;
}

void writeDataTableData(QXmlStreamWriter& writer,
                        const documentdatadefinitions::data::DataTableData& dataTableData)
{
    writer.writeStartElement(dataTableDataElement.toString());
    for (auto it = dataTableData.cbegin(); it != dataTableData.cend(); ++it)
    {
        writer.writeStartElement(dataTableEntryElement.toString());

        writer.writeStartElement(dataDefinitionIndexElement.toString());
        writer.writeCharacters(QString::number(it.key()));
        writer.writeEndElement();

        writer.writeStartElement(dataDefinitionValueElement.toString());
        writer.writeCharacters(it.value());
        writer.writeEndElement();

        // end dataTableEntryElement
        writer.writeEndElement();
    }
    writer.writeEndElement();
}

documentdatadefinitions::data::ListDataDefinitionData readListDataDefinitionData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == listDataDefinitionElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::ListDataDefinitionData listDataDefinitionData;

    while (!utils::xml::checkEndElement(reader, listDataDefinitionElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionListElement)
        {
            listDataDefinitionData.dataDefinitionListData = readDataDefinitionListData(reader);
        }
        else if (reader.isStartElement() && reader.name() == dataTablesElement)
        {
            listDataDefinitionData.demonstrationTable = readDataTablesData(reader);
        }
        else { reader.readNext(); }
    }

    return listDataDefinitionData;
}

void writeListDataDefinitionData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData)
{
    writer.writeStartElement(listDataDefinitionElement.toString());

    writeDataDefinitionListData(writer, listDataDefinitionData.dataDefinitionListData);
    writeDataTablesData(writer, listDataDefinitionData.demonstrationTable);

    writer.writeEndElement();
}

documentdatadefinitions::data::ListDataDefinitionListData readListDataDefinitionListData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == listDataDefinitionListElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::ListDataDefinitionListData listDataDefinitionListData;

    while (!utils::xml::checkEndElement(reader, listDataDefinitionListElement))
    {
        if (reader.isStartElement() && reader.name() == listDataDefinitionElement)
        {
            listDataDefinitionListData.listOfDataDefinition.append(readListDataDefinitionData(reader));
        }
        else { reader.readNext(); }
    }

    return listDataDefinitionListData;
}

void writeListDataDefinitionListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::ListDataDefinitionListData& listDataDefinitionListData)
{
    writer.writeStartElement(listDataDefinitionListElement.toString());

    for (const documentdatadefinitions::data::ListDataDefinitionData& listDataDefinitionData :
         listDataDefinitionListData.listOfDataDefinition)
    {
        writeListDataDefinitionData(writer, listDataDefinitionData);
    }

    writer.writeEndElement();
}

documentdatadefinitions::data::DataDefinitionsHandlerData readDataDefinitionsHandlerData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataDefinitionsHandlerElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataDefinitionsHandlerData dataDefinitionsHandlerData;

    while (!utils::xml::checkEndElement(reader, dataDefinitionsHandlerElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionListElement)
        {
            dataDefinitionsHandlerData.generalVariableList = readDataDefinitionListData(reader);
        }
        else if (reader.isStartElement() && reader.name() == listDataDefinitionListElement)
        {
            dataDefinitionsHandlerData.listDataDefinitionList = readListDataDefinitionListData(reader);
        }
        else { reader.readNext(); }
    }

    return dataDefinitionsHandlerData;
}

void writeDataDefinitionsHandlerData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataDefinitionsHandlerData& dataDefinitionsHandlerData)
{
    writer.writeStartElement(dataDefinitionsHandlerElement.toString());

    writeDataDefinitionListData(writer, dataDefinitionsHandlerData.generalVariableList);
    writeListDataDefinitionListData(writer, dataDefinitionsHandlerData.listDataDefinitionList);

    writer.writeEndElement();
}

documentdatadefinitions::data::DataTableModelListData readDataTableModelListData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataTableModelListElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataTableModelListData dataTableModelListData;

    while (!utils::xml::checkEndElement(reader, dataTableModelListElement))
    {
        if (reader.isStartElement() && reader.name() == dataTablesElement)
        {
            dataTableModelListData.dataTableModelList.append(readDataTablesData(reader));
        }
        else { reader.readNext(); }
    }

    return dataTableModelListData;
}

void writeDataTableModelListData(
  QXmlStreamWriter& writer,
  const documentdatadefinitions::data::DataTableModelListData& dataTableModelListData)
{
    writer.writeStartElement(dataTableModelListElement.toString());

    for (const documentdatadefinitions::data::DataTablesData& dataTablesData :
         dataTableModelListData.dataTableModelList)
    {
        writeDataTablesData(writer, dataTablesData);
    }

    writer.writeEndElement();
}

documentdatadefinitions::data::UserDataHandlerData readUserDataHandlerData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == userDataHandlerElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::UserDataHandlerData userDataHandlerData;

    while (!utils::xml::checkEndElement(reader, userDataHandlerElement))
    {
        if (reader.isStartElement() && reader.name() == dataTablesElement)
        {
            userDataHandlerData.generalVariableDataList = readDataTablesData(reader);
        }
        else if (reader.isStartElement() && reader.name() == dataTableModelListElement)
        {
            userDataHandlerData.dataTableModelListModel = readDataTableModelListData(reader);
        }
        else { reader.readNext(); }
    }

    return userDataHandlerData;
}

void writeUserDataHandlerData(QXmlStreamWriter& writer,
                              const documentdatadefinitions::data::UserDataHandlerData& userDataHandlerData)
{
    writer.writeStartElement(userDataHandlerElement.toString());

    writeDataTablesData(writer, userDataHandlerData.generalVariableDataList);
    writeDataTableModelListData(writer, userDataHandlerData.dataTableModelListModel);

    writer.writeEndElement();
}

documentdatadefinitions::data::DataHandlerData readDataHandlerData(QXmlStreamReader& reader)
{
    Q_ASSERT(reader.isStartElement() && reader.name() == dataHandlerElement);

    reader.readNextStartElement();

    documentdatadefinitions::data::DataHandlerData dataHandlerData;

    while (!utils::xml::checkEndElement(reader, dataHandlerElement))
    {
        if (reader.isStartElement() && reader.name() == dataDefinitionsHandlerElement)
        {
            dataHandlerData.dataDefinitionsHandler = readDataDefinitionsHandlerData(reader);
        }
        else if (reader.isStartElement() && reader.name() == userDataHandlerElement)
        {
            dataHandlerData.userDataHandler = readUserDataHandlerData(reader);
        }
        else { reader.readNext(); }
    }

    return dataHandlerData;
}

void writeDataHandlerData(QXmlStreamWriter& writer,
                          const documentdatadefinitions::data::DataHandlerData& dataHandlerData)
{
    writer.writeStartElement(dataHandlerElement.toString());

    writeDataDefinitionsHandlerData(writer, dataHandlerData.dataDefinitionsHandler);
    writeUserDataHandlerData(writer, dataHandlerData.userDataHandler);

    writer.writeEndElement();
}

}    // namespace documentdatadefinitions::service::details
