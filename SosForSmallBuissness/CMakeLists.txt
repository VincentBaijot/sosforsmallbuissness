cmake_minimum_required(VERSION 3.22)

print_location()

#Set the qml import path for qt creator
set(QML_IMPORT_PATH ${CMAKE_CURRENT_BINARY_DIR} CACHE PATH "Path to import qml modules")

list(APPEND translationsFiles SosForSmallBuissness_en_US.ts SosForSmallBuissness_fr_FR.ts)
list(TRANSFORM translationsFiles PREPEND ${CMAKE_CURRENT_SOURCE_DIR}/)

# Force lupdate to update translation file
execute_process(COMMAND ${CMAKE_PREFIX_PATH}/bin/lupdate.exe ${CMAKE_CURRENT_SOURCE_DIR} -ts ${translationsFiles} -no-obsolete)

option(LOGGER_CONSOLE_COLORED_OUTPUT "Add colors to the console output" OFF)

if (LOGGER_CONSOLE_COLORED_OUTPUT)
  add_compile_definitions(LOGGER_CONSOLE_COLORED_OUTPUT)
endif()

add_subdirectory(Application)
add_subdirectory(Logger)
add_subdirectory(Utils)
add_subdirectory(Painter)
add_subdirectory(DocumentTemplate)
add_subdirectory(SosQml)
add_subdirectory(DocumentDataDefinitions)
add_subdirectory(DataTemplateAssociation)
add_subdirectory(Document)
add_subdirectory(DataDocumentAssociation)
add_subdirectory(Serialization)

set(CMAKE_EXECUTABLE_SUFFIX ".exe")

qt_add_executable(${PROJECT_NAME} main.cpp)

qt_add_qml_module(${PROJECT_NAME}
    URI SosForSmallBuissness
    VERSION 1.0

    IMPORTS
        SosForSmallBuissness.Application
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

qt_add_translations(${PROJECT_NAME} TS_FILES ${translationsFiles})

target_link_libraries(${PROJECT_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Quick)
target_link_libraries(${PROJECT_NAME} PUBLIC Application)

install(TARGETS ${PROJECT_NAME}
     EXPORT Application
     EXPORT DataTemplateAssociation
     EXPORT Document
     EXPORT DocumentDataDefinitions
     EXPORT DocumentTemplate
     EXPORT Logger
     EXPORT Painter
     EXPORT SosQml
     EXPORT Utils
     BUNDLE  DESTINATION .
 )

qt_generate_deploy_qml_app_script(
TARGET ${PROJECT_NAME}
OUTPUT_SCRIPT deploy_script
NO_UNSUPPORTED_PLATFORM_ERROR
DEPLOY_USER_QML_MODULES_ON_UNSUPPORTED_PLATFORM
MACOS_BUNDLE_POST_BUILD
)

install(SCRIPT ${deploy_script})

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    set(CMAKE_EXE_LINKER_FLAGS "-no-pie")
    set(PROCESS_CMD [[COMMAND linuxdeployqt ${CMAKE_INSTALL_PREFIX}]])
    string(APPEND PROCESS_CMD "/bin/${PROJECT_NAME}.exe --appimage-extract-and-run -bundle-non-qt-libs -qmldir=${CMAKE_SOURCE_DIR} -qmake=${QT_QMAKE_EXECUTABLE} RESULT_VARIABLE result")
   install(CODE "\
    execute_process(${PROCESS_CMD})
        if(NOT result EQUAL 0)
            message(FATAL_ERROR \"linuxdeployqt failed\")
        endif()
    ")

    install(SCRIPT CorrectQtQmlRpath.cmake)
elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
	
else()
	message(FATAL_ERROR "Untested platform")
endif()


install(FILES ${CMAKE_SOURCE_DIR}/LICENSE DESTINATION .)

set(CPACK_GENERATOR IFW)
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_SOURCE_DIR}/LICENSE)
set(CPACK_PACKAGE_EXECUTABLES ${PROJECT_NAME} ${PROJECT_NAME})
set(CPACK_CREATE_DESKTOP_LINKS ${PROJECT_NAME})

include(CPack)
include(CPackIFW)

