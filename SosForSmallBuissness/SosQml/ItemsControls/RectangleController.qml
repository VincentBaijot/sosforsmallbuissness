import QtQuick

import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosQml.Controls
import SosQml.ItemsControls

Column {
    id: root

    spacing: 5

    required property PaintableRectangle paintableRectangle
    required property PageSize pageSize
    required property ListAssociationControllerProxyModel listAssociationControllerProxyModel

    signal deletePaintableItem(PaintableItemListModel paintableItemListModel, InterfaceQmlPaintableItem paintableItem)
    signal associatedListUpdated(PaintableItemListModel oldPaintableList, PaintableItemListModel newPaintableList)

    ListAssociationController {
        id: listAssociationController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        listAssociationControllerProxyModel: root.listAssociationControllerProxyModel

        currentAssociatedList: root.paintableRectangle.parentList

        onAssociatedListUpdated: newPaintableList => {
                                     root.associatedListUpdated(
                                         root.paintableRectangle.parentList,
                                         newPaintableList)
                                 }
    }

    ItemPositionController {
        id: itemPositionController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        pageSize: root.pageSize

        itemX: root.paintableRectangle.x
        itemY: root.paintableRectangle.y
        itemWidth: root.paintableRectangle.width
        itemHeight: root.paintableRectangle.height

        onItemXUpdated: newItemX => {
                            root.paintableRectangle.x = newItemX
                        }

        onItemYUpdated: newItemY => {
                            root.paintableRectangle.y = newItemY
                        }

        onItemWidthUpdated: newItemWidth => {
                                root.paintableRectangle.width = newItemWidth
                            }

        onItemHeightUpdated: newItemHeight => {
                                 root.paintableRectangle.height = newItemHeight
                             }
    }

    ColorController {
        id: colorController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemColor: root.paintableRectangle.color
        itemColorIsValid: root.paintableRectangle.colorIsValid
        colorSelectorTitle: qsTr("Color : ")

        onItemColorUpdated: newItemColor => {
                                root.paintableRectangle.color = newItemColor
                            }
    }

    OpacityController {
        id: opacityController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemOpacity: root.paintableRectangle.opacity

        onItemOpacityUpdated: newItemOpacity => {
                                  root.paintableRectangle.opacity = newItemOpacity
                              }
    }

    BorderController {
        id: borderColor

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        colorSelectorTitle: qsTr("Border color : ")

        itemHeight: root.paintableRectangle.height
        itemWidth: root.paintableRectangle.width
        paintableBorder: root.paintableRectangle.border
        pageSize: root.pageSize
        canBeRemoved: false
    }

    RadiusController {
        id: radiusController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemRadius: root.paintableRectangle.radius

        onItemRadiusUpdated: newItemRadius => {
                                 root.paintableRectangle.radius = newItemRadius
                             }
    }

    Item {
        id: separator

        anchors {
            left: root.left
            right: root.right
        }

        height: 20
    }

    RemoveMenuButton {
        id: removeButton
        objectName: "removeButtonRectangleMenu"

        anchors {
            left: root.left
            right: root.right
        }

        buttonMargin: 5
        text: qsTr("Remove the item")

        onClicked: {
            root.deletePaintableItem(root.paintableRectangle.parentList,
                                     root.paintableRectangle)
        }
    }
}
