import QtQuick
import QtQuick.Controls

Column {
    id: root
    spacing: 5

    required property real itemRadius

    signal itemRadiusUpdated(real newItemRadiusUpdated)

    Text{
        id: opacitySelectorTitle
        text: qsTr("Radius : ")
    }

    Item {
        id: radiusRow

        anchors{
            left:root.left
            right: root.right
        }

        height: Math.max(radiusSelector.implicitHeight, radiusValue.height)

        Slider {
            id: radiusSelector
            from: 0
            to: 100
            value: root.itemRadius

            anchors {
                left: radiusRow.left
                verticalCenter: radiusRow.verticalCenter
            }

            width: 3.0 * radiusRow.width / 4.0

            onMoved: {
                root.itemRadiusUpdated(radiusSelector.value)
            }
        }

        Text{
            id: radiusValue

            anchors {
                right: radiusRow.right
            }

            text: Math.round(root.itemRadius) + qsTr("%")
        }
    }
}
