import QtQuick
import QtQuick.Controls

import SosQml.Controls

Column {
    id: root

    required property string itemText

    signal itemTextUpdated(string newItemText)

    Text{
        id: textContentSelectorTitle
        text: qsTr("Text : ")
    }

    ScrollView {
        id: scrollView

        height: 100
        width: root.width

        CustomTextArea {
            id: textArea
            text: root.itemText

            onTextChanged: {
                root.itemTextUpdated(textArea.text)
            }
        }
    }
}
