import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosQml.ItemsControls

Column {
    id: columnController

    spacing: 5

    required property PageSize pageSize

    PageSizeControlProxy {
        id: pageSizeControlProxy

        pageSize: columnController.pageSize
    }

    Text{
        id:pageSelectorTitle
        text: qsTr("Page format : ")
    }

    ComboBox{
        id: pageSelector

        anchors{
            left: columnController.left
            right: columnController.right
        }

        model: pageSizeControlProxy.pageSizeNames
        displayText: pageSizeControlProxy.pageSizeName

        onActivated: (activatedIndex) => {
          pageSizeControlProxy.pageSizeName =  pageSizeControlProxy.pageSizeNames[activatedIndex]
        }
    }

    Item {
        id: pageSizeSpacer
        height: 5

        anchors{
            left: columnController.left
            right: columnController.right
        }
    }

    Row{

        id: pageSizeSpinBoxesContainer

        anchors{
            left: columnController.left
            right: columnController.right
        }

        spacing: 5

        readonly property IntValidator validator : IntValidator{}

        Column {
            id: widthColumn
            width: pageSizeSpinBoxesContainer.width/2 - pageSizeSpinBoxesContainer.spacing/2

            Text{
                id:widthTitle
                text: qsTr("Width : ")
            }

            SpinBox{
                id: withSpinBox
                width: widthColumn.width
                value: pageSizeControlProxy.size.width
                editable: true
                from: 0
                to: pageSizeSpinBoxesContainer.validator.top

                onValueModified: {
                   var originalSize = pageSizeControlProxy.size
                   originalSize.width = withSpinBox.value
                   pageSizeControlProxy.size = originalSize
                }
            }
        }

        Column {

            id: heightColumn
            width: pageSizeSpinBoxesContainer.width/2 - pageSizeSpinBoxesContainer.spacing/2

            Text{
                id:heightTitle
                text: qsTr("Height : ")
            }

            SpinBox{
                id: heightSpinBox
                width: heightColumn.width
                value: pageSizeControlProxy.size.height
                editable: true
                from: 0
                to: pageSizeSpinBoxesContainer.validator.top

                onValueModified: {
                   var originalSize = pageSizeControlProxy.size
                   originalSize.height = heightSpinBox.value
                   pageSizeControlProxy.size = originalSize
                }
            }
        }
    }

    Item {
        id: pageSizeSpinBoxesSpacer
        height: 5

        anchors{
            left: columnController.left
            right: columnController.right
        }
    }

    Text{
        id: unitSelectorTitle
        text: qsTr("Unit : ")
    }

    ComboBox{
        id: unitSelector

        anchors{
            left: columnController.left
            right: columnController.right
        }

        model: pageSizeControlProxy.unitNames
        displayText: pageSizeControlProxy.unitName
        currentIndex: pageSizeControlProxy.unitNames.indexOf(pageSizeControlProxy.unitName)

        onActivated: (activatedIndex) => {
          pageSizeControlProxy.unitName =  pageSizeControlProxy.unitNames[activatedIndex]
        }
    }
}
