import QtQuick

import SosQml.Controls

Column {
    id: root

    required property string itemName

    property string title : qsTr("Item name : ")
    property string placeholder : qsTr("Item name")

    signal itemNameUpdated(string newItemName)

    Text {
        id: fontSelectorTitle
        text: root.title
    }

    CustomTextField {
        id: dataField

        anchors {
            left: root.left
            right: root.right
        }

        placeholderText: root.placeholder

        text: root.itemName

        onTextEdited: {
            root.itemNameUpdated(dataField.text)
        }
    }
}
