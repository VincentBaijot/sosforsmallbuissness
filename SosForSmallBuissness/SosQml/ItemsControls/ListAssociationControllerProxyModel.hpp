#pragma once

#include <QAbstractListModel>
#include <QtQml/QQmlEngine>

#include "ItemsControls_global.hpp"
Q_MOC_INCLUDE("Painter/controllers/PaintableItemListModel.hpp")

namespace painter::controller
{
class PaintableList;
class PaintableItemListModel;
}    // namespace painter::controller

namespace itemscontrols
{
class ITEMS_CONTROLS_TESTING_EXPORT ListAssociationControllerProxyModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(painter::controller::PaintableItemListModel* sourceListModel READ sourceListModel WRITE setSourceListModel
                 NOTIFY sourceListModelChanged)

    QML_ELEMENT
  public:
    enum ListItemFilteredRoles
    {
        ListName = Qt::UserRole + 1
    };

    explicit ListAssociationControllerProxyModel(QObject* parent = nullptr);
    ~ListAssociationControllerProxyModel() override = default;

    /// Override funtion of QAbstractListModel
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    /// Override funtion of QAbstractListModel
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE int roleValue(const QByteArray& roleName) const;

    painter::controller::PaintableItemListModel* sourceListModel() const;
    void setSourceListModel(painter::controller::PaintableItemListModel* newSourceListModel);

    Q_INVOKABLE qsizetype indexOf(const painter::controller::PaintableItemListModel* listableItemListModel) const;

  signals:
    void sourceListModelChanged();

  private slots:
    void _onSourceModelRowsInserted(const QModelIndex& parent, int first, int last);
    void _onSourceModelRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last);
    void _onSourceModelRowsRemoved(const QModelIndex& parent, int first, int last);
    void _onSourceModelReset();

  private:
    Q_DISABLE_COPY_MOVE(ListAssociationControllerProxyModel)

    QList<painter::controller::PaintableList*> _extractPaintableListFromSourceModel();

    painter::controller::PaintableItemListModel* _sourceListModel { nullptr };

    QList<painter::controller::PaintableList*> _paintableItemList;

    QList<painter::controller::PaintableList*> _paintableItemToRemove;
};

}    // namespace documenttemplate::controller
