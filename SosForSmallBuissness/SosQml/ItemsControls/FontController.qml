import QtQml
import QtQuick
import QtQuick.Controls

Column {
    id: root

    required property string itemFont

    signal itemFontUpdated(string newItemFont)

    Text {
        id: fontSelectorTitle
        text: qsTr("Text : ")
    }

    ComboBox {
        id: fontSelector

        readonly property list<string> availableFonts : Qt.fontFamilies()

        anchors {
            left: root.left
            right: root.right
        }

        model: fontSelector.availableFonts
        displayText: root.itemFont

        onActivated: activatedIndex => {
            root.itemFontUpdated(fontSelector.availableFonts[activatedIndex])
        }
    }
}
