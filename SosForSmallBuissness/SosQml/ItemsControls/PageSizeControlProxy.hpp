#pragma once

#include <QObject>

#pragma once

#include <QHash>
#include <QObject>
#include <QPageSize>
#include <QSizeF>
#include <QtQml/QQmlEngine>

#include "DocumentTemplate/controllers/PageSize.hpp"
#include "ItemsControls_global.hpp"

namespace itemscontrols
{

class ITEMS_CONTROLS_TESTING_EXPORT PageSizeControlProxy : public QObject
{
    Q_OBJECT

    Q_PROPERTY(documenttemplate::controller::PageSize* pageSize READ pageSize WRITE setPageSize NOTIFY pageSizeChanged)
    Q_PROPERTY(QStringList pageSizeNames READ pageSizeNames CONSTANT FINAL)
    Q_PROPERTY(documenttemplate::controller::PageSize::PageSizeId pageSizeId READ pageSizeId WRITE setPageSizeId NOTIFY
                 pageSizeIdChanged FINAL)
    Q_PROPERTY(QString pageSizeName READ pageSizeName WRITE setPageSizeName NOTIFY pageSizeNameChanged FINAL)
    Q_PROPERTY(int resolution READ resolution WRITE setResolution NOTIFY resolutionChanged FINAL)
    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged FINAL)
    Q_PROPERTY(QSize sizePixel READ sizePixel NOTIFY sizePixelChanged FINAL)
    Q_PROPERTY(
      documenttemplate::data::PageSizeData::Unit unit READ unit WRITE setUnit NOTIFY unitChanged FINAL)
    Q_PROPERTY(QString unitName READ unitName WRITE setUnitName NOTIFY unitChanged FINAL)
    Q_PROPERTY(QStringList unitNames READ unitNames CONSTANT FINAL)
    Q_PROPERTY(qreal unitPerPixel READ unitPerPixel NOTIFY unitPerPixelChanged FINAL)

    QML_ELEMENT
  public:
    explicit PageSizeControlProxy(QObject* parent = nullptr);

    documenttemplate::controller::PageSize* pageSize() const;
    void setPageSize(documenttemplate::controller::PageSize* newPageSize);

    documenttemplate::controller::PageSize::PageSizeId pageSizeId() const;
    void setPageSizeId(documenttemplate::controller::PageSize::PageSizeId newPageSizeId);

    QString pageSizeName() const;
    void setPageSizeName(const QString& pageSizeName);

    QStringList pageSizeNames() const;

    int resolution() const;
    void setResolution(int newResolution);

    QSizeF size() const;
    void setSize(QSizeF newSize);

    QSize sizePixel() const;

    documenttemplate::data::PageSizeData::Unit unit() const;
    void setUnit(documenttemplate::data::PageSizeData::Unit newUnit);

    QString unitName() const;
    void setUnitName(const QString& newUnitName);

    QStringList unitNames() const;

    const QHash<documenttemplate::data::PageSizeData::Unit, QString>& unitNameMap() const;

    qreal unitPerPixel() const;

  signals:
    void pageSizeChanged();
    void pageSizeIdChanged();
    void pageSizeNameChanged();
    void resolutionChanged();
    void sizeChanged();
    void sizePixelChanged();
    void unitChanged();
    void unitPerPixelChanged();

  private:
    Q_DISABLE_COPY_MOVE(PageSizeControlProxy)

    documenttemplate::controller::PageSize* _pageSize { nullptr };

    const QHash<documenttemplate::controller::PageSize::PageSizeId, QString> _commonPageSizeNameMap;
    const QHash<documenttemplate::data::PageSizeData::Unit, QString> _unitNameMap;
};

}    // namespace documenttemplate::controller
