import QtQuick

import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosQml.Controls
import SosQml.ItemsControls

Column {
    id: root

    spacing: 5

    required property PaintableText paintableText
    required property PageSize pageSize
    required property ListAssociationControllerProxyModel listAssociationControllerProxyModel

    signal deletePaintableItem(PaintableItemListModel paintableItemListModel, InterfaceQmlPaintableItem paintableItem)
    signal associatedListUpdated(PaintableItemListModel oldPaintableList, PaintableItemListModel newPaintableList)

    ListAssociationController {
        id: listAssociationController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        listAssociationControllerProxyModel: root.listAssociationControllerProxyModel
        currentAssociatedList: root.paintableText.parentList

        onAssociatedListUpdated: newPaintableList => {
                                     root.associatedListUpdated(
                                         root.paintableText.parentList,
                                         newPaintableList)
                                 }
    }

    ItemPositionController {
        id: itemPositionController

        anchors {
            left: root.left
            right: root.right
        }

        pageSize: root.pageSize

        itemX: root.paintableText.x
        itemY: root.paintableText.y
        itemWidth: root.paintableText.width
        itemHeight: root.paintableText.height

        onItemXUpdated: newItemX => {
                            root.paintableText.x = newItemX
                        }

        onItemYUpdated: newItemY => {
                            root.paintableText.y = newItemY
                        }

        onItemWidthUpdated: newItemWidth => {
                                root.paintableText.width = newItemWidth
                            }

        onItemHeightUpdated: newItemHeight => {
                                 root.paintableText.height = newItemHeight
                             }
    }

    ColorController {
        id: colorController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemColor: root.paintableText.color
        itemColorIsValid: root.paintableText.colorIsValid
        colorSelectorTitle: qsTr("Text color : ")

        onItemColorUpdated: newItemColor => {
                                root.paintableText.color = newItemColor
                            }
    }

    OpacityController {
        id: opacityController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemOpacity: root.paintableText.opacity

        onItemOpacityUpdated: newItemOpacity => {
                                  root.paintableText.opacity = newItemOpacity
                              }
    }

    ColorController {
        id: backgroundColorController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemColor: root.paintableText.background.color
        itemColorIsValid: root.paintableText.background.colorIsValid
        colorSelectorTitle: qsTr("Background color : ")

        onItemColorUpdated: newItemColor => {
                                root.paintableText.background.color = newItemColor
                            }
    }

    BorderController {
        id: borderColor

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        colorSelectorTitle: qsTr("Border color : ")

        itemHeight: root.paintableText.height
        itemWidth: root.paintableText.width
        paintableBorder: root.paintableText.border
        pageSize: root.pageSize
        canBeRemoved: false
    }

    RadiusController {
        id: radiusController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemRadius: root.paintableText.background.radius

        onItemRadiusUpdated: newItemRadius => {
                                 root.paintableText.background.radius = newItemRadius
                             }
    }

    TextContentController {
        id: textContentController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemText: root.paintableText.text

        onItemTextUpdated: newText => {
                               root.paintableText.text = newText
                           }
    }

    FontController {
        id: fontController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemFont: root.paintableText.fontFamily

        onItemFontUpdated: newFont => {
                               root.paintableText.fontFamily = newFont
                           }
    }

    FontSizeController {
        id: fontSizeController

        anchors {
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemFontSize: root.paintableText.pointSize

        onItemFontSizeUpdated: newFontSize => {
                                   root.paintableText.pointSize = newFontSize
                               }
    }

    Item {
        id: separator

        anchors {
            left: root.left
            right: root.right
        }

        height: 20
    }

    RemoveMenuButton {
        id: removeButton
        objectName: "removeButtonTextMenu"

        anchors {
            left: root.left
            right: root.right
        }

        buttonMargin: 5
        text: qsTr("Remove the item")

        onClicked: {
            root.deletePaintableItem(root.paintableText.parentList,
                                     root.paintableText)
        }
    }
}
