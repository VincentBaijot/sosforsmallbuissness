import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosForSmallBuissness.Painter
import SosQml.Controls
import SosQml.ItemsControls

Item {
    id: root

    required property InterfaceQmlPaintableItem selectedPaintableItem
    required property PageSize pageSize

    property DataTemplateAssociation dataDocumentAssociation: null
    property PaintableItemListModel rootPaintableItemList: null
    property ListDataDefinitionListModel listDataDefinitionListModel: null

    signal selectedPaintableItemUpdated(InterfaceQmlPaintableItem newSelectedPaintableItem)

    QtObject {
        id: privateProperties

        function deletePaintableItem(paintableItemListModel, paintableItem) {
            if (!paintableItemListModel) {
                paintableItemListModel = root.rootPaintableItemList
            }

            root.selectedPaintableItemUpdated(null)
            paintableItemListModel.deletePaintableItem(paintableItem)
        }

        function associatedListUpdated(oldPaintableList : ListDataDefinitionListModel, newPaintableList : ListDataDefinitionListModel) {

            if (!(root.selectedPaintableItem instanceof AbstractListableItem)) {
                return
            }

            if (oldPaintableList !== null) {
                oldPaintableList.removePaintableItem(root.selectedPaintableItem)
            }

            if (newPaintableList === null) {
                newPaintableList = root.rootPaintableItemList
            }

            newPaintableList.addPaintableItem(root.selectedPaintableItem)
            root.selectedPaintableItem.parentList = newPaintableList
        }
    }

    anchors.fill: root

    ListAssociationControllerProxyModel {
        id: listAssociationControllerProxyModelObject
        sourceListModel: root.rootPaintableItemList
    }

    Flickable {
        id: flickableView

        anchors.fill: root

        interactive: editionMenuControllerContainer.height > root.height
                     || flickableView.width > root.width

        contentHeight: editionMenuControllerContainer.height
        contentWidth: flickableView.width

        ScrollBar.vertical: ScrollBar {}
        ScrollBar.horizontal: ScrollBar {}

        clip: true

        Item {
            id: editionMenuControllerContainer

            width: flickableView.width
            height: editionMenuControllerLoader.height
                    + editionMenuControllerLoader.anchors.leftMargin
                    + editionMenuControllerLoader.anchors.rightMargin

            Loader {
                id: editionMenuControllerLoader
                objectName: "editionMenuControllerLoader"

                anchors {
                    left: editionMenuControllerContainer.left
                    leftMargin: 10
                    right: editionMenuControllerContainer.right
                    rightMargin: 20
                    top: editionMenuControllerContainer.top
                    topMargin: 10
                }

                width: flickableView.width - 2 * editionMenuControllerContainer.margin

                sourceComponent: {
                    if (root.selectedPaintableItem === null) {
                        return pageSizeControllerComponent
                    } else if (root.selectedPaintableItem.itemType
                               === InterfaceQmlPaintableItem.RectangleItemType) {
                        return rectangleControllerComponent
                    } else if (root.selectedPaintableItem.itemType
                               === InterfaceQmlPaintableItem.TextItemType) {
                        return textControllerComponent
                    } else if (root.selectedPaintableItem.itemType
                               === InterfaceQmlPaintableItem.ListItemType) {
                        return listControllerComponent
                    }

                    return undefined
                }
            }
        }
    }

    Component {
        id: pageSizeControllerComponent

        PageSizeController {
            id: pageSizeController

            pageSize: root.pageSize
        }
    }

    Component {
        id: rectangleControllerComponent

        RectangleController {
            id: rectangleController

            pageSize: root.pageSize
            paintableRectangle: root.selectedPaintableItem as PaintableRectangle

            listAssociationControllerProxyModel: listAssociationControllerProxyModelObject
            onAssociatedListUpdated: (oldPaintableList, newPaintableList) => {
                                         privateProperties.associatedListUpdated(
                                             oldPaintableList, newPaintableList)
                                     }

            onDeletePaintableItem: (paintableItemListModel, paintableItem) => {
                                       privateProperties.deletePaintableItem(
                                           paintableItemListModel,
                                           paintableItem)
                                   }
        }
    }

    Component {
        id: textControllerComponent

        TextController {
            id: textController

            pageSize: root.pageSize
            paintableText: root.selectedPaintableItem as PaintableText
            listAssociationControllerProxyModel: listAssociationControllerProxyModelObject

            onAssociatedListUpdated: (oldPaintableList, newPaintableList) => {
                                         privateProperties.associatedListUpdated(
                                             oldPaintableList, newPaintableList)
                                     }

            onDeletePaintableItem: (paintableItemListModel, paintableItem) => {
                                       privateProperties.deletePaintableItem(
                                           paintableItemListModel,
                                           paintableItem)
                                   }
        }
    }

    Component {
        id: listControllerComponent

        ListController {
            id: listController

            pageSize: root.pageSize
            paintableList: root.selectedPaintableItem as PaintableList
            listDataDefinitionListModel: root.listDataDefinitionListModel
            dataDocumentAssociation: root.dataDocumentAssociation

            onDeletePaintableItem: (paintableItemListModel, paintableItem) => {
                                       privateProperties.deletePaintableItem(
                                           paintableItemListModel,
                                           paintableItem)
                                   }
        }
    }
}
