#include "PageSizeControlProxy.hpp"

namespace itemscontrols
{
documenttemplate::controller::PageSize* PageSizeControlProxy::pageSize() const
{
    return _pageSize;
}

void PageSizeControlProxy::setPageSize(documenttemplate::controller::PageSize* newPageSize)
{
    if (_pageSize == newPageSize) return;

    if (_pageSize)
    {
        QObject::disconnect(_pageSize,
                            &documenttemplate::controller::PageSize::pageSizeIdChanged,
                            this,
                            &PageSizeControlProxy::pageSizeIdChanged);
        QObject::disconnect(_pageSize,
                            &documenttemplate::controller::PageSize::pageSizeNameChanged,
                            this,
                            &PageSizeControlProxy::pageSizeNameChanged);
        QObject::disconnect(_pageSize,
                            &documenttemplate::controller::PageSize::resolutionChanged,
                            this,
                            &PageSizeControlProxy::resolutionChanged);
        QObject::disconnect(
          _pageSize, &documenttemplate::controller::PageSize::sizeChanged, this, &PageSizeControlProxy::sizeChanged);
        QObject::disconnect(
          _pageSize, &documenttemplate::controller::PageSize::sizePixelChanged, this, &PageSizeControlProxy::sizePixelChanged);
        QObject::disconnect(
          _pageSize, &documenttemplate::controller::PageSize::unitChanged, this, &PageSizeControlProxy::unitChanged);
        QObject::disconnect(_pageSize,
                            &documenttemplate::controller::PageSize::unitPerPixelChanged,
                            this,
                            &PageSizeControlProxy::unitPerPixelChanged);
    }

    _pageSize = newPageSize;

    if (_pageSize)
    {
        QObject::connect(_pageSize,
                         &documenttemplate::controller::PageSize::pageSizeIdChanged,
                         this,
                         &PageSizeControlProxy::pageSizeIdChanged);
        QObject::connect(_pageSize,
                         &documenttemplate::controller::PageSize::pageSizeNameChanged,
                         this,
                         &PageSizeControlProxy::pageSizeNameChanged);
        QObject::connect(_pageSize,
                         &documenttemplate::controller::PageSize::resolutionChanged,
                         this,
                         &PageSizeControlProxy::resolutionChanged);
        QObject::connect(
          _pageSize, &documenttemplate::controller::PageSize::sizeChanged, this, &PageSizeControlProxy::sizeChanged);
        QObject::connect(
          _pageSize, &documenttemplate::controller::PageSize::sizePixelChanged, this, &PageSizeControlProxy::sizePixelChanged);
        QObject::connect(
          _pageSize, &documenttemplate::controller::PageSize::unitChanged, this, &PageSizeControlProxy::unitChanged);
        QObject::connect(_pageSize,
                         &documenttemplate::controller::PageSize::unitPerPixelChanged,
                         this,
                         &PageSizeControlProxy::unitPerPixelChanged);
    }
    emit pageSizeChanged();
    emit pageSizeIdChanged();
    emit pageSizeNameChanged();
    emit resolutionChanged();
    emit sizeChanged();
    emit sizePixelChanged();
    emit unitChanged();
    emit unitPerPixelChanged();
}

QHash<documenttemplate::controller::PageSize::PageSizeId, QString> initializeCommonPageSizeNameMap()
{
    QHash<documenttemplate::controller::PageSize::PageSizeId, QString> commonPageSizeNameMap;
    for (int commonPageSizeId = static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::First);
         commonPageSizeId < static_cast<int>(documenttemplate::controller::PageSize::PageSizeId::Last);
         ++commonPageSizeId)
    {
        commonPageSizeNameMap.insert(documenttemplate::controller::PageSize::PageSizeId(commonPageSizeId),
                                     QPageSize::name(QPageSize::PageSizeId(commonPageSizeId)));
    }

    return commonPageSizeNameMap;
}

QHash<documenttemplate::data::PageSizeData::Unit, QString> initializeUnitNameMap()
{
    QHash<documenttemplate::data::PageSizeData::Unit, QString> unitNameMap;

    for (int unit = static_cast<int>(documenttemplate::data::PageSizeData::Unit::First);
         unit <= static_cast<int>(documenttemplate::data::PageSizeData::Unit::Last);
         ++unit)
    {
        // Use a switch so we have a warning if a value is missing
        switch (documenttemplate::data::PageSizeData::Unit(unit))
        {
            case documenttemplate::data::PageSizeData::Unit::Millimeter:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Millimeter,
                                   QObject::tr("Millimeter"));
                break;
            case documenttemplate::data::PageSizeData::Unit::Point:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Point, QObject::tr("Point"));
                break;
            case documenttemplate::data::PageSizeData::Unit::Inch:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Inch, QObject::tr("Inch"));
                break;
            case documenttemplate::data::PageSizeData::Unit::Pica:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Pica, QObject::tr("Pica"));
                break;
            case documenttemplate::data::PageSizeData::Unit::Didot:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Didot, QObject::tr("Didot"));
                break;
            case documenttemplate::data::PageSizeData::Unit::Cicero:
                unitNameMap.insert(documenttemplate::data::PageSizeData::Unit::Cicero,
                                   QObject::tr("Cicero"));
                break;
        }
    }

    return unitNameMap;
}

PageSizeControlProxy::PageSizeControlProxy(QObject* parent)
    : QObject { parent },
      _commonPageSizeNameMap(initializeCommonPageSizeNameMap()),
      _unitNameMap(initializeUnitNameMap())
{
}

documenttemplate::controller::PageSize::PageSizeId PageSizeControlProxy::pageSizeId() const
{
    if (_pageSize) { return _pageSize->pageSizeId(); }
    else { return documenttemplate::controller::PageSize::PageSizeId::Custom; }
}

void PageSizeControlProxy::setPageSizeId(documenttemplate::controller::PageSize::PageSizeId newPageSizeId)
{
    if (_pageSize) { _pageSize->setPageSizeId(newPageSizeId); }
}

QString PageSizeControlProxy::pageSizeName() const
{
    if (_pageSize) { return _pageSize->objectData().pageSize().name(); }
    else { return QString(); }
}

void PageSizeControlProxy::setPageSizeName(const QString& pageSizeName)
{
    setPageSizeId(_commonPageSizeNameMap.key(pageSizeName, documenttemplate::controller::PageSize::PageSizeId::Custom));
}

QStringList PageSizeControlProxy::pageSizeNames() const
{
    return _commonPageSizeNameMap.values();
}

int PageSizeControlProxy::resolution() const
{
    if (_pageSize) { return _pageSize->resolution(); }
    else { return 0; }
}

void PageSizeControlProxy::setResolution(int newResolution)
{
    if (_pageSize) { _pageSize->setResolution(newResolution); }
}

QSizeF PageSizeControlProxy::size() const
{
    if (_pageSize) { return _pageSize->size(); }
    else { return QSizeF(); }
}

void PageSizeControlProxy::setSize(QSizeF newSize)
{
    if (_pageSize) { _pageSize->setSize(newSize); }
}

QSize PageSizeControlProxy::sizePixel() const
{
    if (_pageSize) { return _pageSize->sizePixel(); }
    else { return QSize(); }
}

documenttemplate::data::PageSizeData::Unit PageSizeControlProxy::unit() const
{
    if (_pageSize) { return _pageSize->unit(); }
    else { return documenttemplate::data::PageSizeData::Unit::Millimeter; }
}

void PageSizeControlProxy::setUnit(documenttemplate::data::PageSizeData::Unit newUnit)
{
    if (_pageSize) { _pageSize->setUnit(newUnit); }
}

QString PageSizeControlProxy::unitName() const
{
    if (_pageSize) { return _unitNameMap.value(_pageSize->unit()); }
    else { return QString(); }
}

void PageSizeControlProxy::setUnitName(const QString& newUnitName)
{
    setUnit(_unitNameMap.key(newUnitName, documenttemplate::data::PageSizeData::Unit::Millimeter));
}

QStringList PageSizeControlProxy::unitNames() const
{
    return _unitNameMap.values();
}

const QHash<documenttemplate::data::PageSizeData::Unit, QString>& PageSizeControlProxy::unitNameMap() const
{
    return _unitNameMap;
}

qreal PageSizeControlProxy::unitPerPixel() const
{
    if (_pageSize) { return _pageSize->unitPerPixel(); }
    else { return 0; }
}

}    // namespace documenttemplate::controller
