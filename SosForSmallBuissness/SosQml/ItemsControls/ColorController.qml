import QtQuick

import SosQml.Controls

Column {
    id: root

    spacing: 5

    required property color itemColor
    required property bool itemColorIsValid
    required property string colorSelectorTitle
    property bool canBeRemoved : true

    signal itemColorUpdated(color newItemColor)

    Text{
        id: colorSelectorTitle
        text: root.colorSelectorTitle
    }

    Row{

        id: backgroundColorSelectionContainer
        width: root.width
        spacing: 20

        ColorPicker {
            id: colorDisplay

            width: 25
            height: colorDisplay.width

            itemColor: root.itemColor
            itemColorIsValid: root.itemColorIsValid


            onItemColorUpdated: (newColor) => {
                root.itemColorUpdated(newColor)
            }
        }

        MenuButton {
            id: noColorSelector

            buttonMargin: 5
            text: qsTr("Remove the color")
            visible: root.canBeRemoved

            onClicked: {
                root.itemColorUpdated("")
            }
        }
    }
}
