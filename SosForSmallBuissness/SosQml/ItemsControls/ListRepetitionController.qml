import QtQuick
import QtQuick.Controls

Column {
    id: root

    required property string controllerTitle
    required property list<string> repetitionDirectionNames
    required property int repetitionDirection

    signal repetitionDirectionUpdated(int newRepetitionDirection)

    Text {
        id: listRepetitionControllerTitle
        text: root.controllerTitle
    }

    ComboBox {
        id: listRepetitionDirectionSelector

        model: root.repetitionDirectionNames
        currentIndex: root.repetitionDirection

        onActivated: (activatedIndex) => {
                         root.repetitionDirectionUpdated(activatedIndex)
                     }
    }
}
