import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentDataDefinitions
import SosQml.ItemsControls

Column {
    id: root
    objectName: "DataListAssociationController"

    spacing: 5

    required property ListDataDefinitionListModel listDataDefinitionListModel
    required property DataDefinitionListModel dataDefinitionListModel

    signal associatedListDataDefinitionUpdated(ListDataDefinition listDataDefinition)

    Text{
        id:listAssociation

        anchors{
            left: root.left
            right: root.right
        }

        text: qsTr("Data association : ")
    }

    ComboBox {
        id: listSelector
        objectName: "DataListAssociationControllerComboBox"

        anchors{
            left: root.left
            right: root.right
        }

        model: root.listDataDefinitionListModel
        textRole: "listName"
        valueRole: "display"
        currentIndex: root.listDataDefinitionListModel.indexOf(root.dataDefinitionListModel)

        onActivated: () => {
          root.associatedListDataDefinitionUpdated(listSelector.currentValue)
        }
    }
}
