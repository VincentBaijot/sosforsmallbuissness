import QtQuick
import Qt.labs.platform
import QtQuick.Controls

import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosQml.Controls

ColorController {
    id: root

    spacing: 5

    required property PaintableBorder paintableBorder
    required property real itemWidth
    required property real itemHeight
    required property PageSize pageSize

    readonly property int numberOfSignificativeDigits : Math.round(Math.log10(root.pageSize.unitPerPixel))
    readonly property double unitSpinBoxStep : Math.pow(10, root.numberOfSignificativeDigits)

    colorSelectorTitle: qsTr("Border color : ")

    itemColor: root.paintableBorder.color
    itemColorIsValid: root.paintableBorder.colorIsValid

    onItemColorUpdated: (newItemColor) => {
        root.paintableBorder.color = newItemColor
    }

    Text{
        id: borderWidthSelectorTitle
        text: qsTr("Border width : ")
    }

    SpinBoxPixelToUnit {
        id: borderWidthSpinBox
        objectName: "borderWidthSpinBox"

        fromPixel: 0
        toPixel: Math.min(root.itemHeight/2,root.itemWidth/2)
        pixelValue: root.paintableBorder.borderWidth
        unitPerPixel: root.pageSize.unitPerPixel

        onPixelValueModified: newPixelValue => {
            root.paintableBorder.borderWidth = newPixelValue
        }
    }


}
