import QtQuick
import QtQuick.Controls

Column {
    id: root

    required property real itemOpacity

    signal itemOpacityUpdated(real newItemOpacity)

    Text {
        id: opacitySelectorTitle
        text: qsTr("Opacity : ")
    }

    Item {
        id: opacityRow

        anchors {
            left: root.left
            right: root.right
        }

        height: Math.max(opacitySelector.implicitHeight, opacityValue.height)

        Slider {
            id: opacitySelector
            from: 0
            to: 100
            value: root.itemOpacity

            anchors {
                left: opacityRow.left
                verticalCenter: opacityRow.verticalCenter
            }

            width: 3.0 * opacityRow.width / 4.0

            onMoved: {
                root.itemOpacityUpdated(opacitySelector.value)
            }
        }

        Text {
            id: opacityValue

            anchors {
                right: opacityRow.right
            }

            text: Math.round(root.itemOpacity) + qsTr("%")
        }
    }
}
