import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.Painter
import SosQml.ItemsControls

Column {
    id: root
    objectName: "ListAssociationController"

    spacing: 5

    required property ListAssociationControllerProxyModel listAssociationControllerProxyModel
    required property PaintableItemListModel currentAssociatedList

    signal associatedListUpdated(PaintableItemListModel paintableList)

    Text {
        id: listAssociation

        anchors {
            left: root.left
            right: root.right
        }

        text: qsTr("List association : ")
    }

    ComboBox {
        id: listSelector
        objectName: "ListAssociationControllerComboBox"

        anchors {
            left: root.left
            right: root.right
        }

        model: root.listAssociationControllerProxyModel
        textRole: "listName"
        valueRole: "display"
        currentIndex: root.listAssociationControllerProxyModel.indexOf(
                          root.currentAssociatedList)

        onActivated: () => {
                         root.associatedListUpdated(
                             listSelector.currentValue?.listableItemListModel)
                     }
    }
}
