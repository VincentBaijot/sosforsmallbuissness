import QtQuick
import QtQuick.Controls

Column {
    id: root

    required property int itemFontSize

    signal itemFontSizeUpdated(int newItemFontSize)

    Text {
        id: fontSelectorTitle
        text: qsTr("Text Size : ")
    }

    SpinBox {
        id: fontSizeSpinBox

        width: root.width
        value: root.itemFontSize

        onValueModified: {
            root.itemFontSizeUpdated(fontSizeSpinBox.value)
        }
    }
}

