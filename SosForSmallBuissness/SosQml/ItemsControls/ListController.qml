import QtQuick

import SosForSmallBuissness.Painter
import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosQml.Controls

Column {
    id: root

    spacing: 5

    required property PaintableList paintableList
    required property PageSize pageSize
    required property ListDataDefinitionListModel listDataDefinitionListModel
    required property DataTemplateAssociation dataDocumentAssociation
    readonly property PaintableListDataDefinitionListBinding paintableListDataDefinitionListBinding : root.dataDocumentAssociation.associatedDataDefinition(paintableList)

    signal deletePaintableItem(PaintableItemListModel paintableItemListModel, InterfaceQmlPaintableItem paintableItem)

    ItemNameController {
        id: itemNameController

        title: qsTr("List name : ")
        placeholder: qsTr("List name")

        anchors{
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        itemName: root.paintableList.itemName

        onItemNameUpdated: (newItemName) => {
           root.paintableList.itemName = newItemName
        }
    }

    DataListAssociationController{
        id: dataListAssocationController

        anchors{
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        listDataDefinitionListModel: root.listDataDefinitionListModel
        dataDefinitionListModel: root.paintableListDataDefinitionListBinding.dataDefinitionList

        onAssociatedListDataDefinitionUpdated: (listDataDefinition) => {
            root.dataDocumentAssociation.insertAssociation(root.paintableList,listDataDefinition.dataDefinitionListModel)
        }
    }

    ItemPositionController {
        id: itemPositionController

        anchors{
            left: root.left
            right: root.right
        }

        spacing: root.spacing

        pageSize: root.pageSize

        itemX: root.paintableList.x
        itemY: root.paintableList.y
        itemWidth: root.paintableList.width
        itemHeight: root.paintableList.height

        onItemXUpdated: (newItemX) => {
            root.paintableList.x = newItemX
        }

        onItemYUpdated: (newItemY) => {
            root.paintableList.y = newItemY
        }

        onItemWidthUpdated: (newItemWidth) => {
            root.paintableList.width = newItemWidth
        }

        onItemHeightUpdated: (newItemHeight) => {
            root.paintableList.height = newItemHeight
        }
    }

    ListRepetitionController{
        id : primaryrepetitionController

        controllerTitle: qsTr("Primary repetition direction")
        repetitionDirectionNames: root.paintableList.repetitionDirectionNames
        repetitionDirection: root.paintableList.primaryRepetitionDirection

        onRepetitionDirectionUpdated: (newRepetitionDirection) => {
                                          root.paintableList.primaryRepetitionDirection = newRepetitionDirection
                                      }
    }

    ListRepetitionController{
        id : secondaryRepetitionController

        controllerTitle: qsTr("Secondary repetition direction")
        repetitionDirectionNames: root.paintableList.repetitionDirectionNames
        repetitionDirection: root.paintableList.secondaryRepetitionDirection

        onRepetitionDirectionUpdated: (newRepetitionDirection) => {
                                          root.paintableList.secondaryRepetitionDirection = newRepetitionDirection
                                      }
    }

    Item {
        id: separator

        anchors{
            left: root.left
            right: root.right
        }

        height: 20
    }

    RemoveMenuButton {
        id: removeButton
        objectName: "removeButtonListMenu"

        anchors{
            left: root.left
            right: root.right
        }

        buttonMargin: 5
        text: qsTr("Remove the item")

        onClicked: {
            root.deletePaintableItem(undefined, root.paintableList)
        }
    }
}
