import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosQml.Controls
import SosQml.ItemsControls

Column {
    id: root

    spacing: 5

    required property real itemX
    required property real itemY
    required property real itemWidth
    required property real itemHeight

    signal itemXUpdated(real newItemX)
    signal itemYUpdated(real newItemY)
    signal itemWidthUpdated(real newItemWidth)
    signal itemHeightUpdated(real newItemHeight)

    required property PageSize pageSize

    readonly property double unitSpinBoxStep : Math.pow(10, root.numberOfSignificativeDigits)
    readonly property int numberOfSignificativeDigits : Math.round(Math.log10(root.pageSize.unitPerPixel))

    PageSizeControlProxy {
        id: pageSizeControlProxy

        pageSize: root.pageSize
    }

    Text{
        id: unitSelectorTitle
        text: qsTr("Unit : ")
    }

    ComboBox{
        id: unitSelector

        width: root.width
        model: pageSizeControlProxy.unitNames
        displayText: pageSizeControlProxy.unitName
        currentIndex: pageSizeControlProxy.unitNames.indexOf(pageSizeControlProxy.unitName)

        onActivated: (activatedIndex) => {
          pageSizeControlProxy.unitName =  pageSizeControlProxy.unitNames[activatedIndex]
        }
    }

    Text{
        id:pageSelectorTitle
        text: qsTr("Position : ")
    }

    Row{

        id: positionSpinBoxesContainer
        width: root.width
        spacing: 5

        Column {
            id: xColumn
            width: positionSpinBoxesContainer.width/2 - positionSpinBoxesContainer.spacing/2

            Text{
                id:xTitle
                text: qsTr("x : ")
            }

            SpinBoxPixelToUnit {
                id: xSpinBox
                objectName: "xSpinBox"

                width: xColumn.width
                fromPixel: 0
                toPixel: pageSizeControlProxy.sizePixel.width - root.itemWidth
                pixelValue: root.itemX
                unitPerPixel: pageSizeControlProxy.unitPerPixel

                onPixelValueModified: (newPixelValue) => {
                    root.itemXUpdated(newPixelValue)
                }
            }
        }

        Column {

            id: yColumn
            width: positionSpinBoxesContainer.width/2 - positionSpinBoxesContainer.spacing/2

            Text{
                id:yTitle
                text: qsTr("y : ")
            }

            SpinBoxPixelToUnit {
                id: ySpinBox
                objectName: "ySpinBox"

                width: yColumn.width
                fromPixel: 0
                toPixel: pageSizeControlProxy.sizePixel.height - root.itemHeight
                pixelValue: root.itemY
                unitPerPixel: pageSizeControlProxy.unitPerPixel

                onPixelValueModified: (newPixelValue) => {
                    root.itemYUpdated(newPixelValue)
                }
            }
        }
    }

    Row{

        id: sizeSpinBoxesContainer
        width: root.width
        spacing: 5

        Column {
            id: widthColumn
            width: sizeSpinBoxesContainer.width/2 - sizeSpinBoxesContainer.spacing/2

            Text{
                id:widthTitle
                text: qsTr("width : ")
            }

            SpinBoxPixelToUnit {
                id: widthSpinBox
                objectName: "widthSpinBox"

                width: widthColumn.width
                fromPixel: 0
                toPixel: pageSizeControlProxy.sizePixel.width - root.itemX
                pixelValue: root.itemWidth
                unitPerPixel: pageSizeControlProxy.unitPerPixel

                onPixelValueModified: (newPixelValue) => {
                    root.itemWidthUpdated(newPixelValue)
                }
            }
        }

        Column {

            id: heightColumn
            width: sizeSpinBoxesContainer.width/2 - sizeSpinBoxesContainer.spacing/2

            Text{
                id: heightTitle
                text: qsTr("height : ")
            }

            SpinBoxPixelToUnit {
                id: heightSpinBox
                objectName: "heightSpinBox"

                width: heightColumn.width
                fromPixel: 0
                toPixel: pageSizeControlProxy.sizePixel.height - root.itemY
                pixelValue: root.itemHeight
                unitPerPixel: pageSizeControlProxy.unitPerPixel

                onPixelValueModified: (newPixelValue) => {
                    root.itemHeightUpdated(newPixelValue)
                }
            }
        }
    }
}
