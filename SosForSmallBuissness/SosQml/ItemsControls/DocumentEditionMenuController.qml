import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.Document
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.Painter
import SosQml.Controls
import SosQml.ItemsControls

AnimatedVerticalMenu {
    id: root

    required property Document document
    required property InterfaceQmlPaintableItem selectedPaintableItem

    signal selectedPaintableItemUpdated(InterfaceQmlPaintableItem newSelectedPaintableItem)

    EditionMenuController {
        id: editionMenuController

        anchors.fill: root

        selectedPaintableItem: root.selectedPaintableItem
        pageSize: root.document.pageSize

        onSelectedPaintableItemUpdated: function (newSelectedPaintableItem) {
            root.selectedPaintableItem(newSelectedPaintableItem)
        }
    }
}
