import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosForSmallBuissness.Painter
import SosQml.Controls
import SosQml.ItemsControls

AnimatedVerticalMenu {
    id: root

    required property DocumentTemplate documentTemplate
    required property InterfaceQmlPaintableItem selectedPaintableItem
    required property DataTemplateAssociation dataDocumentAssociation
    required property ListDataDefinitionListModel listDataDefinitionListModel

    signal selectedPaintableItemUpdated(InterfaceQmlPaintableItem newSelectedPaintableItem)

    EditionMenuController {
        id: editionMenuController

        anchors.fill: root

        rootPaintableItemList: root.documentTemplate.paintableItemList
        selectedPaintableItem: root.selectedPaintableItem
        dataDocumentAssociation: root.dataDocumentAssociation
        pageSize: root.documentTemplate.pageSize
        listDataDefinitionListModel: root.listDataDefinitionListModel

        onSelectedPaintableItemUpdated: function (newSelectedPaintableItem) {
            root.selectedPaintableItemUpdated(newSelectedPaintableItem)
        }
    }
}
