#include "ListAssociationControllerProxyModel.hpp"

#include "Painter/controllers/InterfaceQmlPaintableItem.hpp"
#include "Painter/controllers/ListableItemListModel.hpp"
#include "Painter/controllers/PaintableItemListModel.hpp"
#include "Painter/controllers/PaintableList.hpp"

namespace itemscontrols
{
ListAssociationControllerProxyModel::ListAssociationControllerProxyModel(QObject* parent)
    : QAbstractListModel { parent }
{
}

QVariant ListAssociationControllerProxyModel::data(const QModelIndex& index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) { return QVariant(); }

    switch (role)
    {
        case Qt::DisplayRole:
            if (index.row() == 0) { return QVariant(); }
            else
            {
                return QVariant::fromValue(_paintableItemList.at(index.row() - 1));
            }
        case ListItemFilteredRoles::ListName:
            if (index.row() == 0) { return QObject::tr("No List"); }
            else
            {
                return _paintableItemList.at(index.row() - 1)->itemName();
            }
        default:
            return QVariant();
    }
}

int ListAssociationControllerProxyModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return _paintableItemList.size() + 1;
}

QVariant ListAssociationControllerProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)

    switch (role)
    {
        case Qt::DisplayRole:
            return QObject::tr("List item");
        case ListItemFilteredRoles::ListName:
            return QObject::tr("List name");
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> ListAssociationControllerProxyModel::roleNames() const
{
    QHash<int, QByteArray> roleNamesHash = QAbstractListModel::roleNames();

    roleNamesHash.insert(ListItemFilteredRoles::ListName, "listName");

    return roleNamesHash;
}

int ListAssociationControllerProxyModel::roleValue(const QByteArray& roleName) const
{
    return roleNames().key(roleName);
}

painter::controller::PaintableItemListModel* ListAssociationControllerProxyModel::sourceListModel() const
{
    return _sourceListModel;
}

void ListAssociationControllerProxyModel::setSourceListModel(painter::controller::PaintableItemListModel* newSourceListModel)
{
    if (_sourceListModel == newSourceListModel) { return; }

    beginResetModel();

    if (_sourceListModel)
    {
        QObject::disconnect(_sourceListModel,
                            &QAbstractListModel::rowsInserted,
                            this,
                            &ListAssociationControllerProxyModel::_onSourceModelRowsInserted);
        QObject::disconnect(_sourceListModel,
                            &QAbstractListModel::rowsAboutToBeRemoved,
                            this,
                            &ListAssociationControllerProxyModel::_onSourceModelRowsAboutToBeRemoved);
        QObject::disconnect(_sourceListModel,
                            &QAbstractListModel::rowsRemoved,
                            this,
                            &ListAssociationControllerProxyModel::_onSourceModelRowsRemoved);

        QObject::disconnect(_sourceListModel,
                            &QAbstractListModel::modelReset,
                            this,
                            &ListAssociationControllerProxyModel::_onSourceModelReset);
    }

    _paintableItemList.clear();
    _sourceListModel = newSourceListModel;
    emit sourceListModelChanged();

    if (_sourceListModel)
    {
        QList<painter::controller::PaintableList*> listOfPaintablelist = _extractPaintableListFromSourceModel();

        if (listOfPaintablelist.size() > 0)
        {
            beginInsertRows(QModelIndex(), 0, listOfPaintablelist.size() - 1);
            _paintableItemList = listOfPaintablelist;
            endInsertRows();
        }
    }

    if (newSourceListModel)
    {
        QObject::connect(newSourceListModel,
                         &QAbstractListModel::rowsInserted,
                         this,
                         &ListAssociationControllerProxyModel::_onSourceModelRowsInserted);
        QObject::connect(newSourceListModel,
                         &QAbstractListModel::rowsAboutToBeRemoved,
                         this,
                         &ListAssociationControllerProxyModel::_onSourceModelRowsAboutToBeRemoved);
        QObject::connect(newSourceListModel,
                         &QAbstractListModel::rowsRemoved,
                         this,
                         &ListAssociationControllerProxyModel::_onSourceModelRowsRemoved);
        QObject::connect(newSourceListModel,
                         &QAbstractListModel::modelReset,
                         this,
                         &ListAssociationControllerProxyModel::_onSourceModelReset);
    }

    endResetModel();
}

qsizetype ListAssociationControllerProxyModel::indexOf(
  const painter::controller::PaintableItemListModel* listableItemListModel) const
{
    if (!_sourceListModel) { return -1; }
    QList<painter::controller::PaintableList*>::const_iterator cbegin = std::cbegin(_paintableItemList);
    for (QList<painter::controller::PaintableList*>::const_iterator iterator = cbegin;
         iterator != std::cend(_paintableItemList);
         ++iterator)
    {
        const painter::controller::PaintableList* paintableList = *iterator;
        if (paintableList->listableItemListModel() == listableItemListModel)
        { return std::distance(cbegin, iterator) + 1; }
    }

    return 0;
}

void ListAssociationControllerProxyModel::_onSourceModelRowsInserted(const QModelIndex& parent,
                                                                     int first,
                                                                     int last)
{
    if (!_sourceListModel) { return; }

    QList<painter::controller::PaintableList*> listOfPaintablelist;

    for (int index = first; index <= last && index < _sourceListModel->rowCount(); ++index)
    {
        if (auto listItem = qobject_cast<painter::controller::PaintableList*>(_sourceListModel->at(index)))
        { listOfPaintablelist.append(listItem); }
    }

    if (listOfPaintablelist.size() > 0)
    {
        beginInsertRows(
          QModelIndex(), _paintableItemList.size() + 1, _paintableItemList.size() + listOfPaintablelist.size());
        _paintableItemList.append(listOfPaintablelist);
        endInsertRows();
    }
}

void ListAssociationControllerProxyModel::_onSourceModelRowsAboutToBeRemoved(const QModelIndex& parent,
                                                                             int first,
                                                                             int last)
{
    if (!_sourceListModel) { return; }

    _paintableItemToRemove.clear();

    for (int index = first; index <= last && index < _sourceListModel->rowCount(); ++index)
    {
        if (auto listItem = qobject_cast<painter::controller::PaintableList*>(_sourceListModel->at(index)))
        { _paintableItemToRemove.append(listItem); }
    }
}

void ListAssociationControllerProxyModel::_onSourceModelRowsRemoved(const QModelIndex& parent, int first, int last)
{
    if (!_sourceListModel) { return; }

    for (int rowIndex = 0; rowIndex < _sourceListModel->rowCount(); ++rowIndex)
    {
        const painter::controller::InterfaceQmlPaintableItem* const interfacePaintableItem = _sourceListModel->at(rowIndex);
        _paintableItemToRemove.removeAll(interfacePaintableItem);
    }

    if (_paintableItemToRemove.size() == 1)
    {
        qsizetype itemIndex = _paintableItemList.indexOf(_paintableItemToRemove.at(0));
        beginRemoveRows(QModelIndex(), itemIndex + 1, itemIndex + 1);
        _paintableItemList.removeAt(itemIndex);
        endRemoveRows();
    }
    else if (_paintableItemToRemove.size() > 1)
    {
        qFatal("onSourceModelRowsRemoved is not implemented for _paintableItemToRemove.size() > 1");
    }
}

void ListAssociationControllerProxyModel::_onSourceModelReset()
{
    if (!_sourceListModel) { return; }

    beginResetModel();

    _paintableItemList = _extractPaintableListFromSourceModel();

    endResetModel();
}

QList<painter::controller::PaintableList*> ListAssociationControllerProxyModel::_extractPaintableListFromSourceModel()
{
    QList<painter::controller::PaintableList*> listOfPaintablelist;

    if (_sourceListModel)
    {
        for (int rowIndex = 0; rowIndex < _sourceListModel->rowCount(); ++rowIndex)
        {
            painter::controller::InterfaceQmlPaintableItem* paintableItem = _sourceListModel->at(rowIndex);
            if (auto listItem = qobject_cast<painter::controller::PaintableList*>(paintableItem))
            { listOfPaintablelist.append(listItem); }
        }
    }

    return listOfPaintablelist;
}

}    // namespace itemscontrols
