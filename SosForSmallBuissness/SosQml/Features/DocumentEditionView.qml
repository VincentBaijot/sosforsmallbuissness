import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.Document
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosForSmallBuissness.Painter
import SosQml.Data
import SosQml.Features.EditTemplate
import SosQml.ItemsControls

Rectangle {
    id: root

    color: ColorsSingleton.editionViewBackgroundColor

    required property Document document
    required property int selectedTool
    property InterfaceQmlPaintableItem selectedPaintableItem: null

    DocumentEditionMenuController {
        id: editionMenuController

        anchors {
            top: root.top
            bottom: root.bottom
            right: root.right
        }

        openWidth: 200
        state: editionMenuController.openState
        document: root.document
        selectedPaintableItem: root.selectedPaintableItem

        onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                            root.selectedPaintableItem = newSelectedPaintableItem
                                        }
    }

    EditionArea {
        id: editionArea

        anchors {
            top: root.top
            bottom: root.bottom
            right: editionMenuController.left
            left: root.left
        }

        selectedTool: root.selectedTool
        selectedPaintableItem: root.selectedPaintableItem
        pageSize: root.document.pageSize
        model: root.document

        onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                            root.selectedPaintableItem = newSelectedPaintableItem
                                        }
    }
}
