import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosForSmallBuissness.Painter
import SosQml.Data
import SosQml.Features.EditTemplate
import SosQml.ItemsControls

Rectangle {
    id: root

    color: ColorsSingleton.editionViewBackgroundColor

    required property DocumentTemplate documentTemplate
    required property int selectedTool
    required property DataTemplateAssociation dataDocumentAssociation
    required property DataDefinitionsHandler dataDefinitionHandler

    property InterfaceQmlPaintableItem selectedPaintableItem: null

    TemplateEditionMenuController {
        id: editionMenuController

        anchors {
            top: root.top
            bottom: root.bottom
            right: root.right
        }

        openWidth: 200
        state: editionMenuController.openState
        documentTemplate: root.documentTemplate
        dataDocumentAssociation: root.dataDocumentAssociation
        selectedPaintableItem: root.selectedPaintableItem
        listDataDefinitionListModel: root.dataDefinitionHandler.listDataDefinitionListModel

        onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                            root.selectedPaintableItem = newSelectedPaintableItem
                                        }
    }

    ListModel {
        id: paintableItemListModel

        Component.onCompleted: {
            paintableItemListModel.append({
                                              "display": root.documentTemplate.paintableItemList
                                          })
        }
    }

    EditionArea {
        id: editionArea

        anchors {
            top: root.top
            bottom: root.bottom
            right: editionMenuController.left
            left: root.left
        }

        selectedTool: root.selectedTool
        selectedPaintableItem: root.selectedPaintableItem
        pageSize: root.documentTemplate.pageSize
        model: paintableItemListModel

        onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                            root.selectedPaintableItem = newSelectedPaintableItem
                                        }
    }

    Button {
        text: "Test"
        onClicked: {
            root.documentTemplate.paintableItemList.testPdf()
        }
    }
}
