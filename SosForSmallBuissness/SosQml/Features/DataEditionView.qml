import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentDataDefinitions
import SosQml.Data
import SosQml.Features.EditTemplateData

Rectangle {
    id: root

    required property UserDataHandler userDataHandler

    readonly property real dataMargin: 20

    color: ColorsSingleton.editionViewBackgroundColor

    Flickable {
        id: flickable

        anchors.fill: root

        clip: true
        interactive: flickable.availableFlickableContentWidth < flickableContentItem.width || flickable.availableFlickableContentHeight < flickableContentItem.height
        flickableDirection: Flickable.VerticalFlick

        contentWidth: flickableContentItem.width
        contentHeight: flickableContentItem.height

        topMargin: root.dataMargin
        bottomMargin: root.dataMargin
        leftMargin: root.dataMargin
        rightMargin: root.dataMargin

        readonly property real availableFlickableContentWidth: flickable.width - flickable.leftMargin - flickable.rightMargin
        readonly property real availableFlickableContentHeight: flickable.height - flickable.topMargin - flickable.bottomMargin

        ScrollBar.vertical: ScrollBar {}
        ScrollBar.horizontal: ScrollBar {}

        Column {
            id: flickableContentItem

            width: flickable.availableFlickableContentWidth

            spacing: root.dataMargin

            GeneralDataEditionView {
                id: generalDataEditionView

                anchors {
                    left: flickableContentItem.left
                    right: flickableContentItem.right
                }

                rootDataMargin: root.dataMargin
                dataTableModel: root.userDataHandler.generalVariableDataList
            }

            DataListEditionView {
                id: dataDefinitionListEditionView

                anchors {
                    left: flickableContentItem.left
                    right: flickableContentItem.right
                }

                dataTableModelListModel: root.userDataHandler.dataTableModelListModel
            }
        }
    }
}
