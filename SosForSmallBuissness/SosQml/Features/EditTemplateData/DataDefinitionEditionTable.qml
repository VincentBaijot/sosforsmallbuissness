import QtQuick
import QtQuick.Controls

import SosQml.Data
import SosQml.Controls
import SosForSmallBuissness.DocumentDataDefinitions

Item {
    id: root

    required property DataDefinitionListModel dataDefinitionListModel

    readonly property real columnWidth: TableSizing.columnWidth
    readonly property real columnSpacing: TableSizing.columnSpacing
    readonly property real rowHeight: TableSizing.rowHeight
    readonly  property real rowSpacing: TableSizing.rowSpacing
    readonly property real borderWidth : TableSizing.borderWidth

    readonly property color backgroundHeaderColor : ColorsSingleton.tableColors.backgroundHeaderColor
    readonly property color backgroundColor : ColorsSingleton.tableColors.backgroundColor
    readonly property color innerBorderColor : ColorsSingleton.tableColors.innerBorderColor
    readonly property color outerBorderColor : ColorsSingleton.tableColors.outerBorderColor

    height: root.childrenRect.height

    Row {
        id: headerDataRow

        Rectangle {
            id: keyTitleBackground

            height: root.rowHeight + root.rowSpacing
            width: root.columnWidth + root.columnSpacing

            color: root.backgroundHeaderColor

            border{
                color: root.outerBorderColor
                width: root.borderWidth
            }

            Text {
                anchors {
                    fill: keyTitleBackground
                    topMargin: root.rowSpacing/2
                    bottomMargin: root.rowSpacing/2
                    leftMargin: root.columnSpacing/2
                    rightMargin: root.columnSpacing/2
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:  Text.AlignVCenter

                text: root.dataDefinitionListModel.headerData(0,0,DataDefinitionListModel.KeyDefinition)
            }
        }

        Rectangle {
            id: descriptionTitleBackground

            height: root.rowHeight + root.rowSpacing
            width: root.columnWidth + root.columnSpacing

            color: root.backgroundHeaderColor

            border{
                color: root.outerBorderColor
                width: root.borderWidth
            }

            Text {
                anchors {
                    fill: descriptionTitleBackground
                    topMargin: root.rowSpacing/2
                    bottomMargin: root.rowSpacing/2
                    leftMargin: root.columnSpacing/2
                    rightMargin: root.columnSpacing/2
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:  Text.AlignVCenter

                text: root.dataDefinitionListModel.headerData(0,0,DataDefinitionListModel.DescriptionDefinition)
            }
        }

        Rectangle {
            id: dataTypeTitleBackground

            height: root.rowHeight + root.rowSpacing
            width: root.columnWidth + root.columnSpacing

            color: root.backgroundHeaderColor

            border{
                color: root.outerBorderColor
                width: root.borderWidth
            }

            Text {
                anchors {
                    fill: dataTypeTitleBackground
                    topMargin: root.rowSpacing/2
                    bottomMargin: root.rowSpacing/2
                    leftMargin: root.columnSpacing/2
                    rightMargin: root.columnSpacing/2
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:  Text.AlignVCenter

                text: root.dataDefinitionListModel.headerData(0,0,DataDefinitionListModel.DataTypeDefinition)
            }
        }
    }

    Column {
        id: templateDataColumn

        anchors {
            left: root.left
            top: headerDataRow.bottom
        }

        width: templateDataColumn.childrenRect.width

        Repeater {
            id: listView

            model: root.dataDefinitionListModel

            height: 200

            delegate: Row {
                id: rootDataRow

                required property var model
                required property int index
                required property string keyDefinition
                required property string descriptionDefinition
                required property int dataTypeDefinition

                Rectangle {
                    id: keyFieldBackground

                    height: root.rowHeight + root.rowSpacing
                    width: root.columnWidth + root.columnSpacing

                    color: root.backgroundColor
                    border{
                        color: root.innerBorderColor
                        width: root.borderWidth
                    }

                    CustomTextField {
                        id: keyField

                        anchors {
                            fill: keyFieldBackground
                            topMargin: root.rowSpacing/2
                            bottomMargin: root.rowSpacing/2
                            leftMargin: root.columnSpacing/2
                            rightMargin: root.columnSpacing/2
                        }

                        placeholderText: qsTr("Key")

                        text: rootDataRow.keyDefinition

                        onTextEdited: {
                            rootDataRow.model.keyDefinition = keyField.text
                        }
                    }
                }

                Rectangle {
                    id: descriptionFieldBackground

                    height: root.rowHeight + root.rowSpacing
                    width: root.columnWidth + root.columnSpacing

                    color: root.backgroundColor
                    border{
                        color: root.innerBorderColor
                        width: root.borderWidth
                    }

                    CustomTextField {
                        id: descriptionField

                        anchors {
                            fill: descriptionFieldBackground
                            topMargin: root.rowSpacing/2
                            bottomMargin: root.rowSpacing/2
                            leftMargin: root.columnSpacing/2
                            rightMargin: root.columnSpacing/2
                        }

                        placeholderText: qsTr("Description")

                        text: rootDataRow.descriptionDefinition

                        onTextEdited: {
                            rootDataRow.model.descriptionDefinition = descriptionField.text
                        }
                    }
                }

                Rectangle {
                    id: dataTypeComboBoxBackground

                    height: root.rowHeight + root.rowSpacing
                    width: root.columnWidth + root.columnSpacing

                    color: root.backgroundColor
                    border{
                        color: root.innerBorderColor
                        width: root.borderWidth
                    }

                    ComboBox{
                        id: dataTypeComboBoxField

                        anchors {
                            fill: dataTypeComboBoxBackground
                            topMargin: root.rowSpacing/2
                            bottomMargin: root.rowSpacing/2
                            leftMargin: root.columnSpacing/2
                            rightMargin: root.columnSpacing/2
                        }

                        model: root.dataDefinitionListModel.dataTypeNames
                        currentIndex: rootDataRow.dataTypeDefinition

                        onActivated: (activatedIndex) => {
                                         rootDataRow.model.dataTypeDefinition =  activatedIndex
                                     }
                    }
                }

                Rectangle {
                    id: minusButtonBackground

                    height: root.rowHeight + root.rowSpacing
                    width: root.rowHeight + root.rowSpacing

                    color: root.backgroundColor
                    border{
                        color: root.innerBorderColor
                        width: root.borderWidth
                    }

                    MinusButton {
                        id: minusButton

                        anchors {
                            fill: minusButtonBackground
                            margins: root.rowSpacing/2
                        }

                        onClicked: {
                            root.dataDefinitionListModel.removeDataDefinition(rootDataRow.index)
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: plusButtonBackground

        anchors{
            top: templateDataColumn.bottom
            left: templateDataColumn.left
        }

        height: root.rowHeight + root.rowSpacing
        width: root.rowHeight + root.rowSpacing

        color: root.backgroundColor

        border{
            color: root.innerBorderColor
            width: root.borderWidth
        }

        PlusButton {
            id: plusButton

            anchors {
                fill: plusButtonBackground
                margins: root.rowSpacing/2
            }

            height: root.rowHeight
            width: root.rowHeight

            onClicked: {
                root.dataDefinitionListModel.addDataDefinition()
            }
        }
    }

    //---------------------------------------------------------
    // Drawing of the borders of the table
    //---------------------------------------------------------

    Rectangle {
        id: topBorder

        anchors{
            left: headerDataRow.left
            top: headerDataRow.top
            right: headerDataRow.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: leftBorder

        anchors{
            top: headerDataRow.top
            left: headerDataRow.left
            bottom: plusButtonBackground.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: bottomPlusButtonBorder

        anchors{
            left: plusButtonBackground.left
            bottom: plusButtonBackground.bottom
            right: plusButtonBackground.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightPlusButtonBorder

        anchors{
            top: plusButtonBackground.top
            right: plusButtonBackground.right
            bottom: plusButtonBackground.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: bottomBorder

        anchors{
            left: plusButtonBackground.right
            bottom: templateDataColumn.bottom
            right: templateDataColumn.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightBorder

        anchors{
            top: templateDataColumn.top
            right: templateDataColumn.right
            bottom: templateDataColumn.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: topRightBorder

        anchors{
            left: headerDataRow.right
            bottom: headerDataRow.bottom
            right: templateDataColumn.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightHeaderBorder

        anchors{
            top: headerDataRow.top
            right: headerDataRow.right
            bottom: headerDataRow.bottom
        }

        color: root.outerBorderColor

        width: 1
    }
}
