import QtQuick
import QtQuick.Controls

import SosQml.Data
import SosQml.Controls
import SosForSmallBuissness.DocumentDataDefinitions

Rectangle {
    id: root

    property real rootDataMargin: 20

    required property ListDataDefinitionListModel listDataDefinitionListModel

    border {
        color: ColorsSingleton.secondaryMenuBorderColor
        width: 2
    }

    color: ColorsSingleton.secondaryMenuBackgroundColor

    height: templateDataContainer.height + 2 * root.rootDataMargin

    Column {
        id: templateDataContainer

        anchors{
            top: root.top
            right: root.right
            left: root.left
            margins: root.rootDataMargin
        }

        spacing: root.rootDataMargin

        Text {
            id: noListDataTitle

            text: qsTr("Data lists")
        }

        Repeater {
            id: listDataDefinitnion

            model: root.listDataDefinitionListModel

            delegate : Rectangle {
                id: secondary

                required property ListDataDefinition display
                readonly property DataDefinitionListModel dataDefinitionListModel : secondary.display.dataDefinitionListModel

                height: listTemplateDataContainer.height + 2 * root.rootDataMargin

                anchors{
                    left: templateDataContainer.left
                    right: templateDataContainer.right
                }

                border {
                    color: ColorsSingleton.ternaryMenuBorderColor
                    width: 2
                }

                color: ColorsSingleton.ternaryMenuBackgroundColor

                Column {
                    id: listTemplateDataContainer

                    anchors{
                        top: secondary.top
                        right: secondary.right
                        left: secondary.left
                        margins: root.rootDataMargin
                    }

                    spacing: root.rootDataMargin

                    CustomTextField {
                        id: listDataName

                        placeholderText: qsTr("List name")
                        text: secondary.dataDefinitionListModel.listName

                        onTextEdited: {
                            secondary.dataDefinitionListModel.listName = listDataName.text
                        }
                    }

                    DataDefinitionEditionTable{
                        id: dataDefinitionEditionTable

                        anchors{
                            left: listTemplateDataContainer.left
                            right: listTemplateDataContainer.right
                            margins: root.rootDataMargin
                        }

                        dataDefinitionListModel: secondary.dataDefinitionListModel
                    }

                    Text {
                        id: demonstrationDataTitle

                        anchors{
                            left: listTemplateDataContainer.left
                            right: listTemplateDataContainer.right
                            margins: root.rootDataMargin
                        }

                        text: qsTr("Demonstration data for %1").arg(secondary.dataDefinitionListModel.listName)
                    }

                    Flickable {
                        id: dataDemonstrationEditionTableFlickable

                        anchors{
                            left: listTemplateDataContainer.left
                            right: listTemplateDataContainer.right
                            margins: root.rootDataMargin
                        }

                        height: dataDemonstrationEditionTable.height + root.rootDataMargin

                        clip: true
                        flickableDirection: Flickable.HorizontalFlick
                        interactive: dataDemonstrationEditionTableFlickable.width < dataDemonstrationEditionTableFlickable.contentWidth

                        contentWidth: dataDemonstrationEditionTable.width

                        ScrollBar.horizontal: ScrollBar {}

                        DataEditionTable {
                            id: dataDemonstrationEditionTable

                            dataTableModel: secondary.display.demonstrationDataTable
                        }
                    }
                }
            }
        }

        PlusButton {
            id: plusButton

            height: TableSizing.rowHeight
            width: TableSizing.rowHeight

            onClicked: {
                root.listDataDefinitionListModel.addListDataDefinition()
            }
        }
    }
}
