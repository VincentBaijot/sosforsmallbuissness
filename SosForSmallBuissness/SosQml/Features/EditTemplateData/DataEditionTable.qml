import QtQuick
import QtQuick.Controls

import SosQml.Data
import SosQml.Controls
import SosForSmallBuissness.DocumentDataDefinitions

Item {
    id: root

    required property DataTablesModel dataTableModel

    readonly property real columnWidth: TableSizing.columnWidth
    readonly property real columnSpacing: TableSizing.columnSpacing
    readonly property real rowHeight: TableSizing.rowHeight
    readonly  property real rowSpacing: TableSizing.rowSpacing
    readonly property real borderWidth : TableSizing.borderWidth

    readonly property color backgroundHeaderColor : ColorsSingleton.tableColors.backgroundHeaderColor
    readonly property color backgroundColor : ColorsSingleton.tableColors.backgroundColor
    readonly property color innerBorderColor : ColorsSingleton.tableColors.innerBorderColor
    readonly property color outerBorderColor : ColorsSingleton.tableColors.outerBorderColor

    height: root.childrenRect.height
    width: root.childrenRect.width

    HorizontalHeaderView {
        id: horizontalHeader

        syncView: tableView
        anchors{
            top: root.top
        }

        interactive: false
        resizableRows: false
        resizableColumns: false

        delegate: Rectangle {
            id: headerBackground

            required property string display

            implicitHeight: root.rowHeight + root.rowSpacing
            implicitWidth: root.columnWidth + root.columnSpacing

            color: root.backgroundHeaderColor

            border{
                color: root.outerBorderColor
                width: root.borderWidth
            }

            Text {
                id: headetBackgroundText

                anchors {
                    fill: headerBackground
                    topMargin: root.rowSpacing/2
                    bottomMargin: root.rowSpacing/2
                    leftMargin: root.columnSpacing/2
                    rightMargin: root.columnSpacing/2
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment:  Text.AlignVCenter

                text: headerBackground.display
            }
        }
    }

    VerticalHeaderView {
        id: verticalHeader

        syncView: tableView
        anchors{
            top: tableView.top
            left: tableView.right
        }

        interactive: false
        resizableRows: false
        resizableColumns: false

        delegate: Rectangle {
            id: minusButtonBackground

            required property string display
            required property int row

            implicitHeight: root.rowHeight + root.rowSpacing
            implicitWidth: root.rowHeight + root.rowSpacing

            color: root.backgroundColor
            border{
                color: root.innerBorderColor
                width: root.borderWidth
            }

            MinusButton {
                id: minusButton

                anchors {
                    fill: minusButtonBackground
                    margins: root.rowSpacing/2
                }

                onClicked: {
                    root.dataTableModel.removeDataRow(minusButtonBackground.row)
                }
            }
        }
    }

    TableView {
        id: tableView

        model: root.dataTableModel

        anchors{
            top: horizontalHeader.bottom
        }

        readonly property real rowDelegateHeight : root.rowHeight + root.rowSpacing
        readonly property real columnDelegateWidth : root.columnWidth + root.columnSpacing

        height: (tableView.rowDelegateHeight)*tableView.rows
        width: (tableView.columnDelegateWidth)*tableView.columns

        interactive: false
        resizableRows: false
        resizableColumns: false

        delegate: Rectangle {
            id: dataFieldBackground

            required property string display
            required property var model

            implicitHeight: tableView.rowDelegateHeight
            implicitWidth: tableView.columnDelegateWidth

            color: root.backgroundColor
            border{
                color: root.innerBorderColor
                width: root.borderWidth
            }

            CustomTextField {
                id: dataField

                anchors {
                    fill: dataFieldBackground
                    topMargin: root.rowSpacing/2
                    bottomMargin: root.rowSpacing/2
                    leftMargin: root.columnSpacing/2
                    rightMargin: root.columnSpacing/2
                }

                placeholderText: qsTr("Data")

                text: dataFieldBackground.display

                onTextEdited: {
                    dataFieldBackground.model.edit = dataField.text
                }
            }
        }
    }

    Rectangle {
        id: plusButtonBackground

        anchors{
            top: tableView.bottom
            left: root.left
        }

        height: root.rowHeight + root.rowSpacing
        width: root.rowHeight + root.rowSpacing

        color: root.backgroundColor

        border{
            color: root.innerBorderColor
            width: root.borderWidth
        }

        PlusButton {
            id: plusButton

            anchors {
                fill: plusButtonBackground
                margins: root.rowSpacing/2
            }

            height: root.rowHeight
            width: root.rowHeight

            onClicked: {
                root.dataTableModel.addDataRow()
            }
        }
    }

    //---------------------------------------------------------
    // Drawing of the borders of the table
    //---------------------------------------------------------

    Rectangle {
        id: topBorder

        anchors{
            left: horizontalHeader.left
            top: horizontalHeader.top
            right: horizontalHeader.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: leftBorder

        anchors{
            top: horizontalHeader.top
            left: tableView.left
            bottom: plusButtonBackground.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: bottomPlusButtonBorder

        anchors{
            left: plusButtonBackground.left
            bottom: plusButtonBackground.bottom
            right: plusButtonBackground.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightPlusButtonBorder

        anchors{
            top: plusButtonBackground.top
            right: plusButtonBackground.right
            bottom: plusButtonBackground.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: bottomBorder

        anchors{
            left: plusButtonBackground.right
            bottom: tableView.bottom
            right: verticalHeader.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightBorder

        anchors{
            top: verticalHeader.top
            right: verticalHeader.right
            bottom: verticalHeader.bottom
        }

        color: root.outerBorderColor

        width: 1
    }

    Rectangle {
        id: topRightBorder

        anchors{
            left: horizontalHeader.right
            bottom: horizontalHeader.bottom
            right: verticalHeader.right
        }

        color: root.outerBorderColor

        height: 1
    }

    Rectangle {
        id: rightHeaderBorder

        anchors{
            top: horizontalHeader.top
            right: horizontalHeader.right
            bottom: horizontalHeader.bottom
        }

        color: root.outerBorderColor

        width: 1
    }
}
