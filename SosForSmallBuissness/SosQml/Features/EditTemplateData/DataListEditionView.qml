import QtQuick
import QtQuick.Controls

import SosQml.Data
import SosQml.Controls
import SosForSmallBuissness.DocumentDataDefinitions

Rectangle {
    id: root

    property real rootDataMargin: 20

    required property DataTableModelListModel dataTableModelListModel

    border {
        color: ColorsSingleton.secondaryMenuBorderColor
        width: 2
    }

    color: ColorsSingleton.secondaryMenuBackgroundColor

    height: templateDataContainer.height + 2 * root.rootDataMargin

    Column {
        id: templateDataContainer

        anchors{
            top: root.top
            right: root.right
            left: root.left
            margins: root.rootDataMargin
        }

        spacing: root.rootDataMargin

        Text {
            id: noListDataTitle

            text: qsTr("Data lists")
        }

        Repeater {
            id: listDataDefinition

            model: root.dataTableModelListModel

            delegate : Rectangle {
                id: secondary

                required property DataTablesModel display

                height: listTemplateDataContainer.height + 2 * root.rootDataMargin

                anchors{
                    left: templateDataContainer.left
                    right: templateDataContainer.right
                }

                border {
                    color: ColorsSingleton.ternaryMenuBorderColor
                    width: 2
                }

                color: ColorsSingleton.ternaryMenuBackgroundColor

                Column {
                    id: listTemplateDataContainer

                    anchors{
                        top: secondary.top
                        right: secondary.right
                        left: secondary.left
                        margins: root.rootDataMargin
                    }

                    spacing: root.rootDataMargin

                    Text {
                        id: dataTitle

                        anchors{
                            left: listTemplateDataContainer.left
                            right: listTemplateDataContainer.right
                            margins: root.rootDataMargin
                        }

                        text: qsTr("Data for %1").arg(secondary.display.listName)
                    }

                    Flickable {
                        id: dataEditionTableFlickable

                        anchors{
                            left: listTemplateDataContainer.left
                            right: listTemplateDataContainer.right
                            margins: root.rootDataMargin
                        }

                        height: dataDemonstrationEditionTable.height + root.rootDataMargin

                        clip: true
                        flickableDirection: Flickable.HorizontalFlick
                        interactive: dataEditionTableFlickable.width < dataEditionTableFlickable.contentWidth

                        contentWidth: dataDemonstrationEditionTable.width

                        ScrollBar.horizontal: ScrollBar {}

                        DataEditionTable {
                            id: dataDemonstrationEditionTable

                            dataTableModel: secondary.display
                        }
                    }
                }
            }
        }
    }
}
