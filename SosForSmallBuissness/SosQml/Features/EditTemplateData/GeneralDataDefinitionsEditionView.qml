import QtQuick
import QtQuick.Controls

import SosQml.Controls
import SosQml.Data
import SosForSmallBuissness.DocumentDataDefinitions

Rectangle {
    id: root

    required property DataDefinitionListModel dataDefinitionListModel

    property real rootDataMargin: 20

    border {
        color: ColorsSingleton.secondaryMenuBorderColor
        width: 2
    }

    color: ColorsSingleton.secondaryMenuBackgroundColor

    height: templateDataContainer.height + 2 * root.rootDataMargin

    Column {
        id: templateDataContainer

        anchors{
            top: root.top
            right: root.right
            left: root.left
            margins: root.rootDataMargin
        }

        spacing: root.rootDataMargin

        Text {
            id: noListDataTitle

            text: qsTr("General variables")
        }

        DataDefinitionEditionTable{
            id: dataDefinitionEditionTable

            anchors{
                left: templateDataContainer.left
                right: templateDataContainer.right
                margins: root.rootDataMargin
            }

            dataDefinitionListModel: root.dataDefinitionListModel
        }
    }
}
