import QtQuick

import SosForSmallBuissness.Painter

Rectangle {
    id: root
    required property InterfaceQmlPaintableItem paintableItem
    readonly property PaintableRectangle paintableRectangle : paintableItem as PaintableRectangle

    x: root.paintableRectangle.x
    y: root.paintableRectangle.y
    width: root.paintableRectangle.width
    height: root.paintableRectangle.height

    color: root.paintableRectangle.colorIsValid ? root.paintableRectangle.color :  "transparent"
    radius: root.paintableRectangle.calculatedRadius

    border{
        width: root.paintableRectangle.border.borderWidth
        color: root.paintableRectangle.border.color
    }
}
