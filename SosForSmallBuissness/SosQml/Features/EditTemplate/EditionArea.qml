import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.Painter
import SosQml.Data

Flickable {
    id: flickable

    property real viewScale: 0.95 * Math.min(
                                 flickable.width / (contentWithMargin.width),
                                 flickable.height / (contentWithMargin.height))
    readonly property real maxViewScale: 2
    readonly property real minViewScale: 0.01
    readonly property real flickableMargin: 200

    required property int selectedTool
    required property PageSize pageSize
    property alias model: pageRepeater.model
    required property InterfaceQmlPaintableItem selectedPaintableItem

    signal selectedPaintableItemUpdated(InterfaceQmlPaintableItem newSelectedPaintableItem)

    contentWidth: (contentWithMargin.width) * flickable.viewScale
    contentHeight: (contentWithMargin.height) * flickable.viewScale
    clip: true
    interactive: flickable.selectedTool === Data.MoveTool

    ScrollBar.vertical: ScrollBar {}
    ScrollBar.horizontal: ScrollBar {}

    Item {
        id: flickableContainer
        objectName: "flickableContainer"

        width: Math.max(flickable.width,
                        (contentWithMargin.width) * flickable.viewScale)
        height: Math.max(flickable.height,
                         (contentWithMargin.height) * flickable.viewScale)

        MouseArea {
            anchors.fill: flickableContainer

            onWheel: wheel => {
                         flickable.viewScale += flickable.viewScale * wheel.angleDelta.y / 640
                         if (flickable.viewScale > flickable.maxViewScale) {
                             flickable.viewScale = flickable.maxViewScale
                         } else if (flickable.viewScale < flickable.minViewScale) {
                             flickable.viewScale = flickable.minViewScale
                         }
                     }
        }

        Item {
            id: contentWithMargin

            width: childrenRect.width + 2 * flickable.flickableMargin
            height: childrenRect.height + 2 * flickable.flickableMargin

            anchors.centerIn: flickableContainer
            transformOrigin: Item.Center
            scale: flickable.viewScale

            Column {

                anchors {
                    top: contentWithMargin.top
                    topMargin: flickable.flickableMargin
                    left: contentWithMargin.left
                    leftMargin: flickable.flickableMargin
                }

                spacing: 100

                Repeater {
                    id: pageRepeater

                    Rectangle {
                        id: contentContainer
                        objectName: "contentContainer"

                        required property PaintableItemListModel display

                        width: flickable.pageSize.sizePixel.width
                        height: flickable.pageSize.sizePixel.height

                        color: ColorsSingleton.pageBackgroundColor

                        PaintableItemDisplayArea {
                            id: paintableItemDisplayArea

                            anchors.fill: contentContainer

                            pageSize: flickable.pageSize
                            paintableItemListModel: contentContainer.display
                            selectedTool: flickable.selectedTool
                            selectedPaintableItem: flickable.selectedPaintableItem

                            onSelectedPaintableItemUpdated: newSelectedPaintableItem => {
                                                                flickable.selectedPaintableItemUpdated(
                                                                    newSelectedPaintableItem)
                                                            }
                        }

                        SelectionDrawingArea {
                            id: selectionDrawingArea

                            anchors.fill: contentContainer

                            selectedTool: flickable.selectedTool
                            paintableItemList: contentContainer.display

                            onPaintableItemAdded: addedPaintableItem => {
                                                      flickable.selectedPaintableItemUpdated(
                                                          addedPaintableItem)
                                                  }
                        }
                    }
                }
            }
        }
    }
}
