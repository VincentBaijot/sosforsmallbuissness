import QtQuick
import QtQuick.Controls

import SosForSmallBuissness.Painter

Rectangle {
    id: root

    required property InterfaceQmlPaintableItem paintableItem
    readonly property PaintableText paintableText : paintableItem as PaintableText
    required property int resolution
    readonly property TextArea textArea : textAreaId

    x: root.paintableText.x
    y: root.paintableText.y
    width: root.paintableText.width
    height: root.paintableText.height

    color: root.paintableText.background.colorIsValid ? root.paintableText.background.color :  "transparent"
    radius: root.paintableText.background.calculatedRadius

    border{
        width: root.paintableText.background.border.borderWidth
        color: root.paintableText.background.border.color
    }

    TextArea {
        id: textAreaId

        anchors{
            fill: root
            margins: root.paintableText.border.borderWidth
        }

        placeholderText: "Text"
        text: root.paintableText.text
        font.pixelSize: root.paintableText.pointSize * (root.resolution/72)
        font.family: root.paintableText.fontFamily
        color: root.paintableText.color
        wrapMode: root.paintableText.wrapMode
        verticalAlignment: root.paintableText.verticalAlignment
        horizontalAlignment: root.paintableText.horizontalAlignment

        onTextChanged: {
            root.paintableText.text = textAreaId.text
        }

        onEditingFinished: {
            root.paintableText.text = textAreaId.text
        }

        background: Item{}

        cursorDelegate:Rectangle {
            id: cursorBackground
            width: 10

            color: "black"
            visible: textAreaId.cursorVisible

            PropertyAnimation { id: animation;
                target: cursorBackground;
                property: "color";
                from: "black"
                to: "transparent"
                duration: 1000
                running: true
                loops: Animation.Infinite
                easing.type: Easing.InOutQuart
            }
        }
    }

}

