import QtQuick

import SosForSmallBuissness.Painter

MouseArea {
    id: root

    required property Item referenceItem
    required property InterfaceQmlPaintableItem positionableItem

    enum VerticalResize {
        NoResize,
        TopResize,
        BottomResize
    }

    enum HorizontalResize {
        NoResize,
        LeftResize,
        RightResize
    }

    property int verticalResize : MouseAreaItemResize.NoResize
    property int horizontalResize : MouseAreaItemResize.NoResize

    cursorShape: {
        switch(root.verticalResize){
        case MouseAreaItemResize.NoResize:
            switch(root.horizontalResize){
            case MouseAreaItemResize.NoResize:
                return Qt.ArrowCursor
            case MouseAreaItemResize.LeftResize:
            case MouseAreaItemResize.RightResize:
                return Qt.SizeHorCursor
            }
            return Qt.ArrowCursor
        case MouseAreaItemResize.TopResize:
            switch(root.horizontalResize){
            case MouseAreaItemResize.NoResize:
                return Qt.SizeVerCursor
            case MouseAreaItemResize.LeftResize:
                return Qt.SizeFDiagCursor
            case MouseAreaItemResize.RightResize:
                return Qt.SizeBDiagCursor
            }
            return Qt.ArrowCursor
        case MouseAreaItemResize.BottomResize:
            switch(root.horizontalResize){
            case MouseAreaItemResize.NoResize:
                return Qt.SizeVerCursor
            case MouseAreaItemResize.LeftResize:
                return Qt.SizeBDiagCursor
            case MouseAreaItemResize.RightResize:
                return Qt.SizeFDiagCursor
            }
            return Qt.ArrowCursor
        }
    }

    visible: root.enabled

    property point initialPosition
    property rect itemInitialPosition

    onPressed: mouse => {
                   let referencePosition = root.mapToItem(
                       root.referenceItem, mouse.x, mouse.y)
                   root.initialPosition = Qt.point(
                       referencePosition.x, referencePosition.y)
                   root.itemInitialPosition = Qt.rect(
                       root.positionableItem.x, root.positionableItem.y,
                       root.positionableItem.width,
                       root.positionableItem.height)
               }

    onPositionChanged:
        mouse => {
            if (root.pressed) {
                let referencePosition = root.mapToItem(
                    root.referenceItem, mouse.x, mouse.y)

                if(root.horizontalResize === MouseAreaItemResize.LeftResize
                   || root.horizontalResize === MouseAreaItemResize.RightResize){
                    let dx = (referencePosition.x
                              - root.initialPosition.x) * (root.horizontalResize === MouseAreaItemResize.LeftResize ? 1 : -1)
                    if(dx < root.itemInitialPosition.width){

                        // If the x position will go bellow 0
                        if(root.horizontalResize === MouseAreaItemResize.LeftResize && root.itemInitialPosition.x + dx < 0){
                            dx = -root.itemInitialPosition.x
                        }
                        root.positionableItem.x = root.itemInitialPosition.x + (root.horizontalResize === MouseAreaItemResize.LeftResize ? dx : 0)

                        // If the position of x + width will go beyond referenceItem.width
                        if(root.positionableItem.x + root.itemInitialPosition.width - dx > root.referenceItem.width){
                            dx = root.positionableItem.x + root.itemInitialPosition.width - root.referenceItem.width
                        }
                        root.positionableItem.width = root.itemInitialPosition.width - dx

                    } else {
                        // If the x position will go bellow 0
                        if(root.horizontalResize !== MouseAreaItemResize.LeftResize && root.itemInitialPosition.x + root.itemInitialPosition.width - dx < 0){
                            dx = root.itemInitialPosition.x + root.itemInitialPosition.width
                        }
                        root.positionableItem.x = root.itemInitialPosition.x + root.itemInitialPosition.width + (root.horizontalResize === MouseAreaItemResize.LeftResize ? 0 : -dx)
                        // If the position of x + width will go beyond referenceItem.width
                        if(root.positionableItem.x + dx - root.itemInitialPosition.width > root.referenceItem.width){
                            dx = root.referenceItem.width + root.itemInitialPosition.width - root.positionableItem.x
                        }
                        root.positionableItem.width = dx - root.itemInitialPosition.width
                    }
                }


                if(root.verticalResize === MouseAreaItemResize.TopResize
                   || root.verticalResize === MouseAreaItemResize.BottomResize){
                    let dy = (referencePosition.y
                              - root.initialPosition.y) * (root.verticalResize === MouseAreaItemResize.TopResize ? 1 : -1)

                    if(dy < root.itemInitialPosition.height){

                        // If the position y will go below 0
                        if(root.verticalResize === MouseAreaItemResize.TopResize && root.itemInitialPosition.y + dy < 0){
                            dy = -root.itemInitialPosition.y
                        }
                        root.positionableItem.y = root.itemInitialPosition.y + (root.verticalResize === MouseAreaItemResize.TopResize ? dy : 0)

                        // If the position y +height will go beyond referenceItem.height
                        if(root.positionableItem.y + root.itemInitialPosition.height - dy > root.referenceItem.height){
                            dy = root.positionableItem.y + root.itemInitialPosition.height - root.referenceItem.height
                        }
                        root.positionableItem.height = root.itemInitialPosition.height - dy


                    } else {
                        // If the position y will go below 0
                        if(root.verticalResize !== MouseAreaItemResize.TopResize && root.itemInitialPosition.y + root.itemInitialPosition.height - dy < 0){
                            dy = root.itemInitialPosition.y + root.itemInitialPosition.height
                        }
                        root.positionableItem.y = root.itemInitialPosition.y + root.itemInitialPosition.height + (root.verticalResize === MouseAreaItemResize.TopResize ? 0: -dy)

                        // If the position y +height will go beyond referenceItem.height
                        if(root.positionableItem.y + dy - root.itemInitialPosition.height > root.referenceItem.height){
                            dy = root.referenceItem.height + root.itemInitialPosition.height - root.positionableItem.y
                        }
                        root.positionableItem.height = dy - root.itemInitialPosition.height

                    }
                }
            }
        }
}
