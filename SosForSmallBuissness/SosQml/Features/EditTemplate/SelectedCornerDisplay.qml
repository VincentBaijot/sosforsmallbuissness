import QtQuick

Rectangle {
    id: cornerOuterDot

    required property color innerDotColor
    required property int dotCornerSize
    required property int dotCornerCenterMargin

    color: cornerOuterDot.color

    width: cornerOuterDot.dotCornerSize
    height: cornerOuterDot.width
    radius: cornerOuterDot.width

    Rectangle {
        id: cornerInnerDot

        color: cornerOuterDot.innerDotColor

        anchors.centerIn: cornerOuterDot

        width: cornerOuterDot.width - cornerOuterDot.dotCornerCenterMargin
        height: cornerInnerDot.width
        radius: cornerInnerDot.width
    }
}
