import QtQuick
import QtQuick.Shapes

import SosForSmallBuissness.Painter

Shape {
    id: root
    required property InterfaceQmlPaintableItem paintableItem
    readonly property PaintableList paintableList: paintableItem as PaintableList

    x: root.paintableList.x
    y: root.paintableList.y
    width: root.paintableList.width
    height: root.paintableList.height

    ShapePath {
        startX: 0
        startY: 0

        strokeWidth: 50
        strokeColor: "blue"
        strokeStyle: ShapePath.DashLine
        dashPattern: [1, 4, 4, 4]
        fillColor: "transparent"

        PathPolyline {
            path: [Qt.point(0, 0), Qt.point(root.paintableList.width,
                                            0), Qt.point(
                    root.paintableList.width,
                    root.paintableList.height), Qt.point(
                    0, root.paintableList.height), Qt.point(0, 0)]
        }
    }
}
