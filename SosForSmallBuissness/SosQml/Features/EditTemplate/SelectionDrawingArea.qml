import QtQuick

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.Painter
import SosQml.Data

Item {
    id: root

    required property int selectedTool
    required property PaintableItemListModel paintableItemList

    visible: mouseAreaDrawing.enabled

    signal paintableItemAdded(InterfaceQmlPaintableItem addedPaintableItem)

    Rectangle {
        id: selectionRectangle
        objectName: "drawingSelectionRectangle"

        color: ColorsSingleton.drawingSelectionColor

        visible: mouseAreaDrawing.pressed
    }

    MouseArea {
        id: mouseAreaDrawing
        objectName: "mouseAreaDrawing"

        anchors.fill: root
        enabled: root.selectedTool === Data.RectangleTool
                 || root.selectedTool === Data.TextTool
                 || root.selectedTool === Data.ListTool

        cursorShape: {
            if (root.selectedTool === Data.SelectionTool) {
                return Qt.ArrowCursor
            } else if (root.selectedTool === Data.MoveTool) {
                return Qt.SizeAllCursor
            } else {
                return Qt.CrossCursor
            }
        }

        property point initialPosition

        onPressed: mouse => {
                       mouseAreaDrawing.initialPosition = Qt.point(mouse.x,
                                                                   mouse.y)
                       selectionRectangle.x = mouseAreaDrawing.initialPosition.x
                       selectionRectangle.y = mouseAreaDrawing.initialPosition.y
                       selectionRectangle.width = 0
                       selectionRectangle.height = 0
                   }

        onPositionChanged: mouse => {
                               if (mouse.x > mouseAreaDrawing.initialPosition.x) {
                                   selectionRectangle.x = mouseAreaDrawing.initialPosition.x
                                   selectionRectangle.width = mouse.x - selectionRectangle.x
                               } else {
                                   selectionRectangle.x = mouse.x
                                   selectionRectangle.width
                                   = mouseAreaDrawing.initialPosition.x - mouse.x
                               }

                               if (mouse.y > mouseAreaDrawing.initialPosition.y) {
                                   selectionRectangle.y = mouseAreaDrawing.initialPosition.y
                                   selectionRectangle.height = mouse.y - selectionRectangle.y
                               } else {
                                   selectionRectangle.y = mouse.y
                                   selectionRectangle.height
                                   = mouseAreaDrawing.initialPosition.y - mouse.y
                               }
                           }

        onReleased: {
            let rectanglePosition = Qt.rect(selectionRectangle.x,
                                            selectionRectangle.y,
                                            selectionRectangle.width,
                                            selectionRectangle.height)
            if (root.selectedTool === Data.RectangleTool) {
                root.paintableItemAdded(
                            root.paintableItemList.addPaintableItem(
                                InterfaceQmlPaintableItem.RectangleItemType,
                                rectanglePosition))
            } else if (root.selectedTool === Data.TextTool) {
                root.paintableItemAdded(
                            root.paintableItemList.addPaintableItem(
                                InterfaceQmlPaintableItem.TextItemType,
                                rectanglePosition))
            } else if (root.selectedTool === Data.ListTool) {
                root.paintableItemAdded(
                            root.paintableItemList.addPaintableItem(
                                InterfaceQmlPaintableItem.ListItemType,
                                rectanglePosition))
            }
        }
    }
}
