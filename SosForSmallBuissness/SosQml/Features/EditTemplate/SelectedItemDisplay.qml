import QtQuick

import SosQml.Data
import SosForSmallBuissness.Painter

Item {
    id: root

    required property int selectedTool
    required property InterfaceQmlPaintableItem paintableItem
    required property Item referenceItem

    readonly property color fromColor: ColorsSingleton.selectionHighlightFromColor
    readonly property color toColor: ColorsSingleton.selectionHighlightToColor
    readonly property color innerDotColor: ColorsSingleton.controlsColors.activeBackgroundColor
    readonly property int animationDuration: 1000

    readonly property int borderWidth: 50
    readonly property int dotCornerSize: 100
    readonly property int dotCornerCenterMargin: dotCornerSize / 3
    readonly property int mouseAreaSize: 200

    readonly property bool resizeEnabled: root.paintableItem && (root.paintableItem.itemType === InterfaceQmlPaintableItem.RectangleItemType
                                           || root.paintableItem.itemType === InterfaceQmlPaintableItem.TextItemType
                                           || root.paintableItem.itemType === InterfaceQmlPaintableItem.ListItemType)
                                          && root.selectedTool === Data.SelectionTool

    property color color

    SequentialAnimation on color {
        loops: Animation.Infinite
        running: root.visible

        ColorAnimation {
            from: root.fromColor
            to: root.toColor
            duration: root.animationDuration
        }

        ColorAnimation {
            from: root.toColor
            to: root.fromColor
            duration: root.animationDuration
        }
    }

    //---------------------------------------------------------------
    // Definitions of the borders
    //---------------------------------------------------------------
    Rectangle {
        id: leftBorder

        color: root.color

        anchors {
            left: root.left
            top: root.top
            bottom: root.bottom
        }

        width: root.borderWidth
    }

    Rectangle {
        id: rightBorder

        color: root.color

        anchors {
            right: root.right
            top: root.top
            bottom: root.bottom
        }

        width: root.borderWidth
    }

    Rectangle {
        id: topBorder

        color: root.color

        anchors {
            top: root.top
            left: root.left
            right: root.right
        }

        height: root.borderWidth
    }

    Rectangle {
        id: bottomBorder

        color: root.color

        anchors {
            bottom: root.bottom
            left: root.left
            right: root.right
        }

        height: root.borderWidth
    }

    //---------------------------------------------------------------
    // Definitions of the corners
    //---------------------------------------------------------------
    SelectedCornerDisplay {
        id: topLeftCorner

        innerDotColor: root.innerDotColor
        dotCornerSize: root.dotCornerSize
        dotCornerCenterMargin: root.dotCornerCenterMargin
        color: root.color

        anchors {
            verticalCenter: topBorder.verticalCenter
            horizontalCenter: leftBorder.horizontalCenter
        }

        visible: root.resizeEnabled
    }

    SelectedCornerDisplay {
        id: topRightCorner

        innerDotColor: root.innerDotColor
        dotCornerSize: root.dotCornerSize
        dotCornerCenterMargin: root.dotCornerCenterMargin
        color: root.color

        anchors {
            verticalCenter: topBorder.verticalCenter
            horizontalCenter: rightBorder.horizontalCenter
        }

        visible: root.resizeEnabled
    }

    SelectedCornerDisplay {
        id: bottomLeftCorner

        innerDotColor: root.innerDotColor
        dotCornerSize: root.dotCornerSize
        dotCornerCenterMargin: root.dotCornerCenterMargin
        color: root.color

        anchors {
            verticalCenter: bottomBorder.verticalCenter
            horizontalCenter: leftBorder.horizontalCenter
        }

        visible: root.resizeEnabled
    }

    SelectedCornerDisplay {
        id: bottomRightCorner

        innerDotColor: root.innerDotColor
        dotCornerSize: root.dotCornerSize
        dotCornerCenterMargin: root.dotCornerCenterMargin
        color: root.color

        anchors {
            verticalCenter: bottomBorder.verticalCenter
            horizontalCenter: rightBorder.horizontalCenter
        }

        visible: root.resizeEnabled
    }

    //---------------------------------------------------------------
    // Definitions of the mouse area corners
    //---------------------------------------------------------------
    MouseAreaItemResize {
        id: topLeftMouseArea

        anchors {centerIn: topLeftCorner }

        width: root.mouseAreaSize
        height: topLeftMouseArea.width

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.TopResize
        horizontalResize: MouseAreaItemResize.LeftResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: topRightMouseArea

        anchors { centerIn: topRightCorner }

        width: root.mouseAreaSize
        height: topRightMouseArea.width

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.TopResize
        horizontalResize: MouseAreaItemResize.RightResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: bottomLeftMouseArea

        anchors {centerIn: bottomLeftCorner }

        width: root.mouseAreaSize
        height: bottomLeftMouseArea.width

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.BottomResize
        horizontalResize: MouseAreaItemResize.LeftResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: bottomRightMouseArea

        anchors { centerIn: bottomRightCorner }

        width: root.mouseAreaSize
        height: bottomRightMouseArea.width

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.BottomResize
        horizontalResize: MouseAreaItemResize.RightResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    //---------------------------------------------------------------
    // Definitions of the mouse area borders
    //---------------------------------------------------------------
    MouseAreaItemResize {
        id: topMouseArea

        anchors {
            top: topLeftMouseArea.top
            left: topLeftMouseArea.right
            right: topRightMouseArea.left
            bottom: topRightMouseArea.bottom
        }

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.TopResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: leftMouseArea

        anchors {
            top: topLeftMouseArea.bottom
            left: topLeftMouseArea.left
            right: bottomLeftMouseArea.right
            bottom: bottomLeftMouseArea.top
        }

        enabled: root.resizeEnabled

        horizontalResize: MouseAreaItemResize.LeftResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: rightMouseArea

        anchors {
            top: topRightMouseArea.bottom
            left: topRightMouseArea.left
            right: bottomRightMouseArea.right
            bottom: bottomRightMouseArea.top
        }

        enabled: root.resizeEnabled

        horizontalResize: MouseAreaItemResize.RightResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }

    MouseAreaItemResize {
        id: bottomMouseArea

        anchors {
            top: bottomLeftMouseArea.top
            left: bottomLeftMouseArea.right
            right: bottomRightMouseArea.left
            bottom: bottomRightMouseArea.bottom
        }

        enabled: root.resizeEnabled

        verticalResize: MouseAreaItemResize.BottomResize

        referenceItem: root.referenceItem
        positionableItem: root.paintableItem
    }
}
