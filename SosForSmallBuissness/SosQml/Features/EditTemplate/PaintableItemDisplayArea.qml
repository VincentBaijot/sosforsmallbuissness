import QtQuick

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.Painter
import SosQml.Data

Item {
    id: root

    required property PageSize pageSize
    required property PaintableItemListModel paintableItemListModel
    required property InterfaceQmlPaintableItem selectedPaintableItem
    required property int selectedTool

    signal selectedPaintableItemUpdated(InterfaceQmlPaintableItem newSelectedPaintableItem)

    MouseArea {
        id: backgroundSelectionMouseArea

        anchors.fill: root

        propagateComposedEvents: true
        enabled: root.selectedTool === Data.SelectionTool

        function isMouseInItem(mouse, item) {
            return item !== null && item
                    !== undefined && (item.itemType === InterfaceQmlPaintableItem.RectangleItemType || item.itemType
                                      === InterfaceQmlPaintableItem.TextItemType || item.itemType
                                      === InterfaceQmlPaintableItem.ListItemType) && mouse.x
                    >= item.x && mouse.x <= item.x + item.width
                    && mouse.y >= item.y && mouse.y <= item.y + item.height
        }

        onPressed: mouse => {
                       if (!isMouseInItem(mouse, root.selectedPaintableItem)) {
                           let paintableItemList = root.paintableItemListModel
                           let selectedItem = null
                           for (let i = paintableItemList.rowCount() - 1; i >= 0; --i) {
                               let paintableItem = paintableItemList.data(
                                   paintableItemList.index(i, 0),
                                   RootPaintableItemList.PaintableItemRole)
                               if (isMouseInItem(mouse, paintableItem)) {
                                   selectedItem = paintableItem
                                   break
                               }
                           }

                           root.selectedPaintableItemUpdated(selectedItem)
                       }

                       mouse.accepted = false
                   }
    }

    Repeater {
        id: paintableItemRepeater
        objectName: "paintableItemRepeater"

        anchors.fill: root

        model: root.paintableItemListModel

        delegate: Item {
            id: delegateContainer
            objectName: "delegateContainer"

            required property InterfaceQmlPaintableItem display
            anchors.fill: paintableItemRepeater

            Loader {
                id: paintableItemLoader
                objectName: "paintableItemLoader"

                sourceComponent: {
                    if (!delegateContainer.display) {
                        return undefined
                    } else if (delegateContainer.display.itemType
                               === InterfaceQmlPaintableItem.RectangleItemType) {
                        return rectangleComponent
                    } else if (delegateContainer.display.itemType
                               === InterfaceQmlPaintableItem.TextItemType) {
                        return textComponent
                    } else if (delegateContainer.display.itemType
                               === InterfaceQmlPaintableItem.ListItemType) {
                        return listComponent
                    }

                    return undefined
                }
            }

            SelectedItemDisplay {
                id: selectedItemDisplay

                readonly property Item loadedItem: paintableItemLoader.item as Item

                x: selectedItemDisplay.loadedItem ? selectedItemDisplay.loadedItem.x
                                                    - selectedItemDisplay.borderWidth : 0
                y: selectedItemDisplay.loadedItem ? selectedItemDisplay.loadedItem.y
                                                    - selectedItemDisplay.borderWidth : 0
                width: selectedItemDisplay.loadedItem?.width + 2 * selectedItemDisplay.borderWidth
                height: selectedItemDisplay.loadedItem?.height + 2 * selectedItemDisplay.borderWidth

                visible: root.selectedPaintableItem === delegateContainer.display

                selectedTool: root.selectedTool
                paintableItem: delegateContainer.display
                referenceItem: root
            }

            Component {
                id: rectangleComponent

                PaintableRectangleDisplay {
                    id: paintableRectangle
                    objectName: "paintableRectangleDisplay"

                    paintableItem: delegateContainer.display
                }
            }

            Component {
                id: textComponent

                PaintableTextDisplay {
                    id: paintableText

                    objectName: "paintableTextDisplay"

                    enabled: root.selectedPaintableItem === delegateContainer.display

                    paintableItem: delegateContainer.display
                    resolution: root.pageSize.resolution
                }
            }

            Component {
                id: listComponent

                PaintableListDisplay {
                    id: paintableList
                    objectName: "paintableListDisplay"

                    paintableItem: delegateContainer.display
                }
            }
        }
    }
}
