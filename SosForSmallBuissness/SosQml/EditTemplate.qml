import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import SosForSmallBuissness.DocumentTemplate
import SosForSmallBuissness.DocumentDataDefinitions
import SosForSmallBuissness.DataTemplateAssociation
import SosQml.Features

Item {
    id: root

    required property TemplatesHandler templatesHandler
    required property DataDefinitionsHandler dataDefinitionHandler
    required property DataTemplateAssociationMap dataTemplateAssociationMap

    property int selectedTool: Data.SelectionTool

    TabBar {
        id: tabBar
        anchors {
            left: root.left
            right: root.right
        }

        TabButton {
            text: qsTr("Templates data")
        }

        Repeater {
            model: root.templatesHandler

            delegate: TabButton {
                required property DocumentTemplate documentTemplate

                text: qsTr("Template")
            }
        }
    }

    StackLayout {
        anchors {
            top: tabBar.bottom
            left: root.left
            right: root.right
            bottom: root.bottom
        }
        currentIndex: tabBar.currentIndex

        TemplateDataEditionView {
            id: dataEditionView

            dataDefinitionHandler: root.dataDefinitionHandler
        }

        Repeater {
            model: root.templatesHandler

            delegate: TemplateEditionView {
                id: templateEditionView

                selectedTool: root.selectedTool
                dataDefinitionHandler: root.dataDefinitionHandler
                dataDocumentAssociation: root.dataTemplateAssociationMap.association(
                                             templateEditionView.documentTemplate)
            }
        }
    }
}
