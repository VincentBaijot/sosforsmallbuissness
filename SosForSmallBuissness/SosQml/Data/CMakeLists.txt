cmake_minimum_required(VERSION 3.22)

print_location()

set(LIBRARY_NAME SosQmlData)

set_source_files_properties(ColorsSingleton.qml TableSizing.qml PROPERTIES
   QT_QML_SINGLETON_TYPE TRUE
)

qt_add_library(${LIBRARY_NAME} SHARED)
qt_add_qml_module(${LIBRARY_NAME}
    URI SosQml.Data
    VERSION 1.0

    DEPENDENCIES
        QtQml
        QtQuick

    SOURCES
        MainColors.hpp
        ControlsColors.hpp
        GreenControlsColors.hpp
        RedControlsColors.hpp
        TableColors.hpp

    QML_FILES
        ColorsSingleton.qml
        Data.qml
        TableSizing.qml
)

correctPluginBuildLocation(${LIBRARY_NAME})

target_compile_definitions(${LIBRARY_NAME} PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${LIBRARY_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Qml Qt${QT_VERSION_MAJOR}::Quick Qt${QT_VERSION_MAJOR}::QuickControls2)

set_target_properties(${LIBRARY_NAME}plugin PROPERTIES BUILD_RPATH "$ORIGIN")
set_target_properties(${LIBRARY_NAME}plugin PROPERTIES INSTALL_RPATH "$ORIGIN/../../../lib")

install(TARGETS ${LIBRARY_NAME}
    BUNDLE DESTINATION .)

install(TARGETS ${LIBRARY_NAME}plugin
	LIBRARY DESTINATION "qml/SosQml/Data"
	RUNTIME DESTINATION "qml/SosQml/Data")
