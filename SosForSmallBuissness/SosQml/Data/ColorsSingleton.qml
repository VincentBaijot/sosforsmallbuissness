pragma Singleton

import QtQuick
import SosQml.Data

MainColors {
    readonly property ControlsColors controlsColors : ControlsColors {}
    readonly property RedControlsColors redControlsColors : RedControlsColors {}
    readonly property GreenControlsColors greenControlsColors : GreenControlsColors {}
    readonly property TableColors tableColors : TableColors{}
}
