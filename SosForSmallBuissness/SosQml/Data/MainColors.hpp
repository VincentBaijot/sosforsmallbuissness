#pragma once

#include <QColor>
#include <QObject>
#include <QtQml/QQmlEngine>

class MainColors : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor selectionHighlightFromColor READ selectionHighlightFromColor CONSTANT FINAL)
    Q_PROPERTY(QColor selectionHighlightToColor READ selectionHighlightToColor CONSTANT FINAL)
    Q_PROPERTY(QColor redColor READ redColor CONSTANT FINAL)
    Q_PROPERTY(QColor secondaryMenuBackgroundColor READ secondaryMenuBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor secondaryMenuBorderColor READ secondaryMenuBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor ternaryMenuBackgroundColor READ ternaryMenuBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor ternaryMenuBorderColor READ ternaryMenuBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor editionViewBackgroundColor READ editionViewBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor pageBackgroundColor READ pageBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor drawingSelectionColor READ drawingSelectionColor CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit MainColors(QObject* parent = nullptr) : QObject(parent) {}
    ~MainColors() override = default;

    constexpr QColor selectionHighlightFromColor() const { return _selectionHighlightFromColor; }
    constexpr QColor selectionHighlightToColor() const { return _selectionHighlightToColor; }
    constexpr QColor redColor() const { return _redColor; }
    constexpr QColor secondaryMenuBackgroundColor() const { return _secondaryMenuBackgroundColor; }
    constexpr QColor secondaryMenuBorderColor() const { return _secondaryMenuBorderColor; }
    constexpr QColor ternaryMenuBackgroundColor() const { return _ternaryMenuBackgroundColor; }
    constexpr QColor ternaryMenuBorderColor() const { return _ternaryMenuBorderColor; }
    constexpr QColor editionViewBackgroundColor() const { return _editionViewBackgroundColor; }
    constexpr QColor pageBackgroundColor() const { return _pageBackgroundColor; }
    constexpr QColor drawingSelectionColor() const { return _drawingSelectionColor; }

  private:
    Q_DISABLE_COPY_MOVE(MainColors)

    static constexpr QColor _selectionHighlightFromColor { QColor(0x40, 0x40, 0x40) };
    static constexpr QColor _selectionHighlightToColor { QColor(0xE7, 0xE7, 0xE7) };
    static constexpr QColor _redColor { QColorConstants::Red };
    static constexpr QColor _secondaryMenuBackgroundColor { QColor(0xB3, 0xB3, 0xB3) };
    static constexpr QColor _secondaryMenuBorderColor { QColor(0xE9, 0xE9, 0xE9) };
    static constexpr QColor _ternaryMenuBackgroundColor { QColor(0x90, 0x90, 0x90) };
    static constexpr QColor _ternaryMenuBorderColor { QColor(0xD9, 0xD9, 0xD9) };
    static constexpr QColor _editionViewBackgroundColor { QColorConstants::DarkGray };
    static constexpr QColor _pageBackgroundColor { QColorConstants::White };
    static constexpr QColor _drawingSelectionColor { QColorConstants::Blue };
};
