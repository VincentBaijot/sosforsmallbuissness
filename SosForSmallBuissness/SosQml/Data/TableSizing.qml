pragma Singleton

import QtQuick

QtObject {
    readonly property real columnWidth: 100
    readonly property real columnSpacing: 20

    readonly property real rowHeight: 20
    readonly property real rowSpacing: 10

    readonly property real borderWidth : 1
}
