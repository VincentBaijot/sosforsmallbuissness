#pragma once

#include <QColor>
#include <QObject>
#include <QtQml/QQmlEngine>


class GreenControlsColors : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor enabledBackgroundColor READ enabledBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor disabledBackgroundColor READ disabledBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor activeBackgroundColor READ activeBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor disabledBorderColor READ disabledBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor enabledBorderColor READ enabledBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor activeBorderColor READ activeBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor hoverBackgroundColor READ hoverBackgroundColor CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit GreenControlsColors(QObject* parent = nullptr) : QObject(parent) {}
    ~GreenControlsColors() override = default;

    constexpr QColor enabledBackgroundColor() const { return _enabledBackgroundColor; }
    constexpr QColor disabledBackgroundColor() const { return _disabledBackgroundColor; }
    constexpr QColor disabledBorderColor() const { return _disabledBorderColor; }
    constexpr QColor enabledBorderColor() const { return _enabledBorderColor; }
    constexpr QColor activeBorderColor() const { return _activeBorderColor; }
    constexpr QColor activeBackgroundColor() const { return _activeBackgroundColor; }
    constexpr QColor hoverBackgroundColor() const { return _hoverBackgroundColor; }

  private:
    Q_DISABLE_COPY_MOVE(GreenControlsColors)

    static constexpr QColor _enabledBackgroundColor { QColor(0x97, 0xFF, 0x9C) };
    static constexpr QColor _disabledBackgroundColor { QColor(0, 0, 0, 0) };
    static constexpr QColor _activeBackgroundColor { QColor(0x5F, 0xDD, 0x5A) };
    static constexpr QColor _hoverBackgroundColor { QColor(0x69, 0xF6, 0x64) };
    static constexpr QColor _disabledBorderColor { QColor(255, 255, 255) };
    static constexpr QColor _enabledBorderColor { QColor(255, 255, 255) };
    static constexpr QColor _activeBorderColor { QColor(0x1B, 0xD9, 0x3C) };
};
