import QtQml

QtObject {
    enum ApplicationFeatures {
        Billing,
        EditTemplates
    }

    enum ApplicationTools {
        SelectionTool,
        MoveTool,
        RectangleTool,
        TextTool,
        ListTool
    }
}
