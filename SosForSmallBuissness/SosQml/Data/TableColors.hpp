#pragma once

#include <QColor>
#include <QObject>
#include <QtQml/QQmlEngine>

class TableColors : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor backgroundHeaderColor READ backgroundHeaderColor CONSTANT FINAL)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor innerBorderColor READ innerBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor outerBorderColor READ outerBorderColor CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit TableColors(QObject* parent = nullptr) : QObject(parent) {}
    ~TableColors() override = default;

    constexpr QColor backgroundHeaderColor() const { return _backgroundHeaderColor; }
    constexpr QColor backgroundColor() const { return _backgroundColor; }
    constexpr QColor innerBorderColor() const { return _innerBorderColor; }
    constexpr QColor outerBorderColor() const { return _outerBorderColor; }

  private:
    Q_DISABLE_COPY_MOVE(TableColors)

    static constexpr QColor _backgroundHeaderColor { QColor(0xD5, 0xD5, 0xD5) };
    static constexpr QColor _backgroundColor { QColor(0xE5, 0xE5, 0xE5) };
    static constexpr QColor _innerBorderColor { QColor(0xC5, 0xC5, 0xC5) };
    static constexpr QColor _outerBorderColor { QColor(0x00, 0x00, 0x00) };
};
