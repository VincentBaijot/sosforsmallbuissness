#pragma once

#include <QColor>
#include <QObject>
#include <QtQml/QQmlEngine>


class ControlsColors : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor enabledBackgroundColor READ enabledBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor disabledBackgroundColor READ disabledBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor activeBackgroundColor READ activeBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor disabledBorderColor READ disabledBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor noBackgroundColor READ noBackgroundColor CONSTANT FINAL)
    Q_PROPERTY(QColor enabledBorderColor READ enabledBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor activeBorderColor READ activeBorderColor CONSTANT FINAL)
    Q_PROPERTY(QColor hoverBackgroundColor READ hoverBackgroundColor CONSTANT FINAL)

    QML_ELEMENT
  public:
    explicit ControlsColors(QObject* parent = nullptr) : QObject(parent) {}
    ~ControlsColors() override = default;

    constexpr QColor enabledBackgroundColor() const { return _enabledBackgroundColor; }
    constexpr QColor disabledBackgroundColor() const { return _disabledBackgroundColor; }
    constexpr QColor disabledBorderColor() const { return _disabledBorderColor; }
    constexpr QColor noBackgroundColor() const { return _noBackgroundColor; }
    constexpr QColor enabledBorderColor() const { return _enabledBorderColor; }
    constexpr QColor activeBorderColor() const { return _activeBorderColor; }
    constexpr QColor activeBackgroundColor() const { return _activeBackgroundColor; }
    constexpr QColor hoverBackgroundColor() const { return _hoverBackgroundColor; }

  private:
    Q_DISABLE_COPY_MOVE(ControlsColors)

    static constexpr QColor _enabledBackgroundColor { QColor(255, 255, 255) };
    static constexpr QColor _disabledBackgroundColor { QColor(0xDE, 0xDE, 0xFF) };
    static constexpr QColor _noBackgroundColor { QColor(0, 0, 0, 0) };
    static constexpr QColor _activeBackgroundColor { QColor(0x7A, 0x7A, 0xFF) };
    static constexpr QColor _hoverBackgroundColor { QColor(0xB5, 0xB5, 0xFF) };
    static constexpr QColor _disabledBorderColor { QColor(255, 255, 255) };
    static constexpr QColor _enabledBorderColor { QColor(0x03, 0x0F, 0x76) };
    static constexpr QColor _activeBorderColor { QColor(0x03, 0x0F, 0x76) };
};
