import QtQuick

import SosQml.Data

Rectangle {
    id: root

    required property double openWidth

    readonly property string openState: "OPEN"
    readonly property string closeState: "CLOSE"
    readonly property bool isFullyOpen : root.width === root.openWidth

    color: ColorsSingleton.secondaryMenuBackgroundColor

    state: root.openState
    visible: root.width !== 0

    states: [
        State {
            name: root.openState
            PropertyChanges {
                target: root
                width: root.openWidth
            }
        },
        State {
            name: root.closeState
            PropertyChanges {
                target: root
                width: 0
            }
        }
    ]

    transitions: [
        Transition {
            to: "*"
            PropertyAnimation {
                property: "width"
                duration: 200
            }
        }
    ]
}
