import QtQuick
import QtQuick.Controls

import SosQml.Data

Button {
    id: plusButton

    implicitWidth: 20
    implicitHeight: 20

    icon {
        source: Qt.resolvedUrl("resources/Plus.svg")
    }

    background: Rectangle {
        implicitWidth: 20
        implicitHeight: 20
        opacity: enabled ? 1 : 0.3
        border.color: ColorsSingleton.greenControlsColors.activeBorderColor
        border.width: 1
        radius: 2
        color: { if (plusButton.down || plusButton.checked) {
                return  ColorsSingleton.greenControlsColors.activeBackgroundColor
            } else if (plusButton.hovered) {
                return ColorsSingleton.greenControlsColors.hoverBackgroundColor
            }
            return ColorsSingleton.greenControlsColors.enabledBackgroundColor
        }
    }
}
