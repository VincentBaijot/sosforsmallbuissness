import QtQuick

import SosQml.Data

Rectangle {
    id: root

    border.width: 1
    border.color: ColorsSingleton.controlsColors.activeBorderColor

    readonly property real widthCross: 0.8 * root.width
    readonly property real heightCross: 0.1 * root.height

    Rectangle {
        id: leftCross

        anchors.centerIn: root
        width: root.widthCross
        height: root.heightCross

        color: ColorsSingleton.redColor

        rotation: 45
    }

    Rectangle {
        id: rightCross

        anchors.centerIn: root
        width: root.widthCross
        height: root.heightCross

        color: ColorsSingleton.redColor

        rotation: -45
    }
}
