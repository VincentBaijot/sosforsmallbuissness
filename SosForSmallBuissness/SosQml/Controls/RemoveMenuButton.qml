import QtQuick

import SosQml.Data

MenuButton {

    activeBackgroundColor: ColorsSingleton.redControlsColors.activeBackgroundColor
    hoverBackgroundColor: ColorsSingleton.redControlsColors.hoverBackgroundColor
    disabledBackgroundColor: ColorsSingleton.redControlsColors.disabledBackgroundColor
    activeBorderColor: ColorsSingleton.redControlsColors.activeBorderColor
}
