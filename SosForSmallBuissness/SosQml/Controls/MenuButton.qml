import QtQuick

import SosQml.Data

Rectangle {
    id: root

    signal clicked
    property bool hover : false
    property bool selected : false
    property real buttonMargin : 10

    property color activeBackgroundColor : ColorsSingleton.controlsColors.activeBackgroundColor
    property color hoverBackgroundColor : ColorsSingleton.controlsColors.hoverBackgroundColor
    property color disabledBackgroundColor : ColorsSingleton.controlsColors.disabledBackgroundColor
    property color activeBorderColor : ColorsSingleton.controlsColors.activeBorderColor

    property alias text : buttonText.text

    color: {
        if(root.selected){
            return root.activeBackgroundColor
        }
        else if(root.hover){
            return root.hoverBackgroundColor
        } else {
            return root.disabledBackgroundColor
        }
    }
    border.color: root.activeBorderColor
    border.width: 2



    implicitWidth: buttonText.implicitWidth + buttonText.anchors.leftMargin + buttonText.anchors.rightMargin
    implicitHeight: buttonText.implicitHeight + buttonText.anchors.topMargin + buttonText.anchors.bottomMargin

    Text {
        id: buttonText

        horizontalAlignment: Text.AlignHCenter

        anchors{
            fill: root
            margins: root.buttonMargin
        }
    }

    MouseArea{
        id: buttonMouseArea

        anchors.fill: root
        hoverEnabled: true

        onEntered: { root.hover = true }
        onExited: { root.hover = false }
        onClicked: { root.clicked() }
    }
}
