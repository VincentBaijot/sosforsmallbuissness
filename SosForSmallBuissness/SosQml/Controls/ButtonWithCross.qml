import QtQuick

CrossIcon {
    id: root

    signal clicked

    MouseArea {
        id: mouseArea

        anchors.fill: root
        onClicked: {
            root.clicked()
        }
    }
}
