import QtQuick
import QtQuick.Controls

import SosQml.Data

Button {
    id: minusButton

    icon {
        source: Qt.resolvedUrl("resources/Minus.svg")
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 1 : 0.3
        border.color: ColorsSingleton.redControlsColors.activeBorderColor
        border.width: 1
        radius: 2
        color: { if (minusButton.down || minusButton.checked) {
                   return ColorsSingleton.redControlsColors.activeBackgroundColor
               } else if (minusButton.hovered) {
                   return ColorsSingleton.redControlsColors.hoverBackgroundColor
               }
               return ColorsSingleton.redControlsColors.enabledBackgroundColor
        }
    }
}
