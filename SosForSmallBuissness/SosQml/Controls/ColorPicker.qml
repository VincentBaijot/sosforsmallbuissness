import QtQuick
import Qt.labs.platform

import SosForSmallBuissness.Painter

Item {
    id: root

    required property color itemColor
    required property bool itemColorIsValid

    signal itemColorUpdated(color newItemColor)

    CrossIcon {
        id: noColorDisplay

        anchors.fill : root
        visible: !root.itemColorIsValid
    }

    Rectangle{
        id: colorDisplay

        anchors.fill : root
        color: root.itemColor
        visible: root.itemColorIsValid
    }

    MouseArea{
        id: colorDialogMouseArea
        anchors.fill: root
        onClicked: {
            colorDialog.open()
        }
    }

    ColorDialog {
        id: colorDialog
        title: qsTr("Please choose a color")
        onAccepted: {
            root.itemColorUpdated(colorDialog.color)
        }
    }
}


