import QtQml
import QtQuick
import QtQuick.Controls

SpinBox {
    id: spinBox

    required property int pixelValue
    required property double unitPerPixel

    readonly property int numberOfSignificativeDigits: Math.ceil(
                                                           Math.log10(
                                                               spinBox.unitPerPixel))
    readonly property double unitSpinBoxStep: Math.pow(
                                                  10,
                                                  spinBox.numberOfSignificativeDigits)

    property int fromPixel: 0
    //default value of to is 99
    property int toPixel: 99 * spinBox.unitSpinBoxStep / spinBox.unitPerPixel

    signal pixelValueModified(int newPixelValue)

    value: Math.round(
               spinBox.pixelValue * spinBox.unitPerPixel / spinBox.unitSpinBoxStep)
    editable: true
    from: Math.round(
              spinBox.fromPixel * spinBox.unitPerPixel / spinBox.unitSpinBoxStep)
    to: Math.round(
            spinBox.toPixel * spinBox.unitPerPixel / spinBox.unitSpinBoxStep)

    validator: DoubleValidator {
//        locale: Qt.locale().name
        bottom: Math.min(spinBox.from, spinBox.to)
        top: Math.max(spinBox.from, spinBox.to)
    }

    onValueModified: {
        spinBox.pixelValueModified(
                    Math.round(
                        spinBox.value * spinBox.unitSpinBoxStep / spinBox.unitPerPixel))
    }

    function spinBoxTextFromValue(value: int, locale: Locale) : string {
        return Number(value * spinBox.unitSpinBoxStep).toLocaleString(
                    locale, 'f', Math.abs(spinBox.numberOfSignificativeDigits))
    }

    function spinBoxValueFromText(text:string, locale:Locale) : int {
            return Math.round(Number.fromLocaleString(
                                  locale, text) / spinBox.unitSpinBoxStep)
    }

    textFromValue: spinBoxTextFromValue

    valueFromText: spinBoxValueFromText
}
