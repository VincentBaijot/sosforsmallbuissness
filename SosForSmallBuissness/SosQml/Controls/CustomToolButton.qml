import QtQuick
import QtQuick.Controls

import SosQml.Data

ToolButton {
    id: control

    background: Rectangle {
        implicitWidth: 20
        implicitHeight: 20

        color: {
            if (control.highlighted) {
                return "yellow"
            } else if (control.down || control.checked) {
                return ColorsSingleton.controlsColors.activeBackgroundColor
            } else if (control.hovered) {
                return ColorsSingleton.controlsColors.hoverBackgroundColor
            }
            return ColorsSingleton.controlsColors.noBackgroundColor
        }

        radius: 2
        border.color: {
            return ColorsSingleton.controlsColors.activeBorderColor
        }

        border.width: 2
    }
}
