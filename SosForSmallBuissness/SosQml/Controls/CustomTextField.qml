import QtQuick
import QtQuick.Controls

import SosQml.Data

TextField {
    id: control

    selectByMouse: true

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 20
        color: control.enabled ? "white" : "white"

        border.color: {
            if (control.activeFocus) {
                return ColorsSingleton.controlsColors.activeBackgroundColor
            } else if (control.enabled) {
                return ColorsSingleton.controlsColors.enabledBackgroundColor
            } else {
                return ColorsSingleton.controlsColors.disabledBackgroundColor
            }
        }
        border.width: 2
    }
}
