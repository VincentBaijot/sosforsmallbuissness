import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import SosForSmallBuissness.Document
import SosForSmallBuissness.DocumentDataDefinitions
import SosQml.Features

Item {
    id: root

    required property DocumentHandler documentHandler
    required property UserDataHandler userDataHandler

    property int selectedTool: Data.SelectionTool

    TabBar {
        id: tabBar
        anchors {
            left: root.left
            right: root.right
        }

        TabButton {
            text: qsTr("Data")
        }

        Repeater {
            model: root.documentHandler

            delegate: TabButton {
                required property Document document

                text: qsTr("Document")
            }
        }
    }

    StackLayout {
        anchors {
            top: tabBar.bottom
            left: root.left
            right: root.right
            bottom: root.bottom
        }
        currentIndex: tabBar.currentIndex

        DataEditionView {
            id: dataEditionView

            userDataHandler: root.userDataHandler
        }

        Repeater {
            model: root.documentHandler

            delegate: DocumentEditionView {
                id: documentEditionView

                selectedTool: root.selectedTool
            }
        }
    }
}
