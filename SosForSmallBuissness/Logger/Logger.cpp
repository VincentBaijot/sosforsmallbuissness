#include "Logger.hpp"

#include "Constants.hpp"
#include "LogFile.hpp"
#include "LogHandler.hpp"

namespace logger
{
/// The maximal age of a log file in days, older log files than this value are cleaned by the cleanLogger method
constexpr uint LOG_FILE_MAX_AGE = 15;

namespace details
{

QtMessageHandler _defaultQtMessageHandler()
{
    static QtMessageHandler messageHandler = qInstallMessageHandler(nullptr);
    return messageHandler;
}

void defaultQtMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    _defaultQtMessageHandler()(type, context, message);
}

LogHandler* _instance()
{
    // Force initialization to occur only once
    if (static bool initialized = false; !initialized)
    {
        _defaultQtMessageHandler();

#ifdef QT_NO_DEBUG
        qSetMessagePattern("[%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}Warning"
                           "%{endif}%{if-critical}Critical%{endif}%{if-fatal}Fatal%{endif}] (%{category}) %{time"
                           " hh:mm:ss.zzz} : \"%{message}\"");
#else
        qSetMessagePattern("[%{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}Warning"
                           "%{endif}%{if-critical}Critical%{endif}%{if-fatal}Fatal%{endif}] (%{category}) %{time"
                           " hh:mm:ss.zzz} : \"%{message}\" %{file}:%{line}");
#endif
        initialized = true;
    }

    static LogHandler instance;
    return &instance;
}

void printColoredMessageToConsole(QtMsgType type, const QString& message)
{
    QTextStream stdOutStream(stdout);
    // Color the console output with one color for each log level
    switch (type)
    {
        case QtDebugMsg:
            stdOutStream << "\033[38;2;176;176;176m";
            break;
        case QtInfoMsg:
            stdOutStream << "\033[38;2;54;189;244m";
            break;
        case QtWarningMsg:
            stdOutStream << "\033[38;2;255;153;0m";
            break;
        case QtCriticalMsg:
            stdOutStream << "\033[38;2;255;51;0m";
            break;
        case QtFatalMsg:
            stdOutStream << "\033[38;2;153;0;0m";
            break;
    }

    // "\033[0m" reset the standart console output
    stdOutStream << message << "\033[0m" << Qt::endl;
}

void printMessageToLogFile(const QString& message)
{
    LogHandler* logHandler = _instance();

    if (logHandler->logFile()->open(QIODevice::Append))
    {
        QTextStream(logHandler->logFile()) << message << Qt::endl;
        logHandler->logFile()->close();
    }
    else { qFatal("Cannot write in the log file %s", qUtf8Printable(logHandler->logFile()->fileName())); }
}

}    // namespace details

void setLogFolder(const QDir& dir)
{
    details::_instance()->setLogDir(dir);
}

void cleanLogger(const QDir& dir)
{
    const QStringList filesList(dir.entryList(QStringList() << QStringLiteral("*.log"), QDir::Files));

    for (const QString& logFileName : filesList)
    {
        LogFile logFile(dir.absolutePath() + "/" + logFileName);

        if (logFile.getLogDate().daysTo(QDateTime::currentDateTime()) > LOG_FILE_MAX_AGE)
        {
            qCInfo(loggerCategory) << "Remove log file : " << logFile.fileName();
            logFile.remove();
        }
    }
}

}    // namespace logger
