#pragma once

#include <QDir>
#include <QMutex>
#include <QtLogging>

#include "Logger_utils.hpp"

namespace logger
{

namespace details
{
LOGGER_EXPORT void defaultQtMessageHandler(QtMsgType type,
                                           const QMessageLogContext& context,
                                           const QString& message);
LOGGER_EXPORT void printColoredMessageToConsole(QtMsgType type, const QString& message);
LOGGER_EXPORT void printMessageToLogFile(const QString& message);
}    // namespace details

LOGGER_EXPORT void setLogFolder(const QDir& dir);

/*!
 * @brief Delete the too old logs message in the given folder
 * @param dir the folder where the too old log message are cleanned
 */
LOGGER_EXPORT void cleanLogger(const QDir& dir);

/*!
 * @brief Format and print output log message
 * @param type the type of the log message (debug, warning, critical or fatal)
 * @param context informations about the log message location
 * @param message the message to log
 */
template <bool enableConsoleColoredOutput =
#ifdef LOGGER_CONSOLE_COLORED_OUTPUT
            true
#else
            false
#endif
          >

void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    static QMutex mutex;
    QMutexLocker lock(&mutex);

    if constexpr (enableConsoleColoredOutput)
    {
        details::printColoredMessageToConsole(type, qFormatLogMessage(type, context, message));
    }
    else { details::defaultQtMessageHandler(type, context, message); }

    details::printMessageToLogFile(qFormatLogMessage(type, context, message));
}

}    // namespace logger
