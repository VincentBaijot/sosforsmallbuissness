#pragma once

#include <QtCore/qglobal.h>

#if defined(LOGGER_LIBRARY)
#define LOGGER_EXPORT Q_DECL_EXPORT
#else
#define LOGGER_EXPORT Q_DECL_IMPORT
#endif

#if defined(ENABLE_TESTING)
#define LOGGER_TESTING_EXPORT LOGGER_EXPORT
#else
#define LOGGER_TESTING_EXPORT
#endif
