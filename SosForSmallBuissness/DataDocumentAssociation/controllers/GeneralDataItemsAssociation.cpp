#include "GeneralDataItemsAssociation.hpp"

#include "Constants.hpp"
#include "Document/controllers/Document.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "Painter/controllers/PaintableText.hpp"

namespace datadocumentassociation::controller
{
GeneralDataItemsAssociation::GeneralDataItemsAssociation(QObject* parent) : QObject { parent }
{
}

GeneralDataItemsAssociation::GeneralDataItemsAssociation(
  document::controller::Document* document,
  const documentdatadefinitions::controller::DataTablesModel* const generalDataTablesModel,
  const documenttemplate::data::DocumentTemplateData& documentTemplateData,
  const datadocumentassociation::data::GeneralListItemsAssociationData& data,
  QObject* parent)
    : QObject { parent },
      _document { document },
      _generalDataTablesModel { generalDataTablesModel },
      _documentTemplateData { documentTemplateData }
{
    setObjectData(data);
}

document::controller::Document* GeneralDataItemsAssociation::document() const
{
    return _document;
}

void GeneralDataItemsAssociation::setDocument(document::controller::Document* newDocument)
{
    _document = newDocument;
}

const documentdatadefinitions::controller::DataTablesModel* GeneralDataItemsAssociation::generalDataTablesModel() const
{
    return _generalDataTablesModel;
}

void GeneralDataItemsAssociation::setGeneralDataTablesModel(
  const documentdatadefinitions::controller::DataTablesModel* newGeneralDataTablesModel)
{
    if (_generalDataTablesModel)
    {
        QObject::disconnect(_generalDataTablesModel,
                            &QAbstractItemModel::dataChanged,
                            this,
                            &GeneralDataItemsAssociation::_onTableDataChanged);
    }

    _generalDataTablesModel = newGeneralDataTablesModel;

    if (_generalDataTablesModel)
    {
        QObject::connect(_generalDataTablesModel,
                         &QAbstractItemModel::dataChanged,
                         this,
                         &GeneralDataItemsAssociation::_onTableDataChanged);
    }
}

documenttemplate::data::DocumentTemplateData GeneralDataItemsAssociation::documentTemplateData() const
{
    return _documentTemplateData;
}

void GeneralDataItemsAssociation::setDocumentTemplateData(
  const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData)
{
    _documentTemplateData = newDocumentTemplateData;
}

datadocumentassociation::data::GeneralListItemsAssociationData GeneralDataItemsAssociation::objectData() const
{
    Q_ASSERT(_document);

    datadocumentassociation::data::GeneralListItemsAssociationData generalListItemsAssociationData;

    for (int pageIndex = 0; pageIndex < _templatedItems.size(); ++pageIndex)
    {
        datadocumentassociation::data::PageItemsTemplateAssociationData pageItemsTemplateAssociationData;
        pageItemsTemplateAssociationData.pageIndex = pageIndex;

        for (const TemplatedItem& templatedItem : _templatedItems.at(pageIndex))
        {
            datadocumentassociation::data::TemplatedItemData templatedItemData;
            templatedItemData.itemIndex         = _document->at(pageIndex)->indexOf(templatedItem.item);
            templatedItemData.itemTemplateIndex = templatedItem.itemTemplateIndex;

            pageItemsTemplateAssociationData.templatedItems.push_back(templatedItemData);
        }

        generalListItemsAssociationData.generalListItemsAssociationData.push_back(
          pageItemsTemplateAssociationData);
    }
    return generalListItemsAssociationData;
}

void GeneralDataItemsAssociation::setObjectData(
  const datadocumentassociation::data::GeneralListItemsAssociationData& newData)
{
    _templatedItems.clear();
    _templatedTexts.clear();

    for (const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData :
         newData.generalListItemsAssociationData)
    {
        insertTemplatedItems(pageItemsTemplateAssociationData);
    }
}

void GeneralDataItemsAssociation::insertTemplatedItems(
  const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData)
{
    Q_ASSERT(_document);

    painter::controller::ListableItemListModel* const page = _document->at(pageItemsTemplateAssociationData.pageIndex);

    for (const datadocumentassociation::data::TemplatedItemData& templatedItemData :
         pageItemsTemplateAssociationData.templatedItems)
    {
        TemplatedItem templatedItem;
        templatedItem.itemTemplateIndex = templatedItemData.itemTemplateIndex;
        templatedItem.item              = page->at(templatedItemData.itemIndex);

        if (_templatedItems.size() > pageItemsTemplateAssociationData.pageIndex)
        {
            _templatedItems[pageItemsTemplateAssociationData.pageIndex].push_back(templatedItem);
        }
        else { _templatedItems.push_back({ templatedItem }); }

        if (const auto* const paintableTextData = std::get_if<painter::data::PaintableTextData>(
              &_documentTemplateData.rootPaintableItemListData.paintableList.at(templatedItem.itemTemplateIndex)))
        {
            _insertTextTemplateItem(templatedItem, pageItemsTemplateAssociationData);
        }
    }
}

void GeneralDataItemsAssociation::_onTableDataChanged(const QModelIndex& topLeft,
                                                      const QModelIndex& bottomRight,
                                                      const QList<int>& roles) const
{
    Q_ASSERT_X(_generalDataTablesModel,
               "GeneralDataItemsAssociation::_onTableDataChanged",
               "DataTablesModel is required to update document on data changed");
    Q_ASSERT_X(topLeft.row() == 0,
               "GeneralDataItemsAssociation::_onTableDataChanged",
               "General data model must contain and update only one row");
    Q_ASSERT_X(bottomRight.row() == 0,
               "GeneralDataItemsAssociation::_onTableDataChanged",
               "General data model must contain and update only one row");

    if (!roles.contains(Qt::DisplayRole)) { return; }

    for (const QList<TemplatedText>& templatedTexts : _templatedTexts)
    {
        for (const TemplatedText& templatedText : templatedTexts)
        {
            QString updatedText =
              std::get<painter::data::PaintableTextData>(
                _documentTemplateData.rootPaintableItemListData.paintableList.at(templatedText.itemTemplateIndex))
                .text;

            for (int dataColumn = 0; dataColumn < _generalDataTablesModel->columnCount(); ++dataColumn)
            {
                const documentdatadefinitions::controller::DataDefinition* const dataDefinition =
                  _generalDataTablesModel->dataDefinitionModel()->at(dataColumn);

                QString dataString =
                  _generalDataTablesModel->data(_generalDataTablesModel->index(0, dataColumn), Qt::DisplayRole)
                    .toString();

                updatedText = document::service::documentgenerator::generateTemplatedText(
                  updatedText, dataDefinition->key(), dataString);
            }

            templatedText.text->setText(updatedText);
        }
    }
}

void GeneralDataItemsAssociation::_insertTextTemplateItem(
  TemplatedItem templatedItem,
  const data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData)
{
    if (auto* const textItem = qobject_cast<painter::controller::PaintableText*>(templatedItem.item))
    {
        TemplatedText templatedText;
        templatedText.itemTemplateIndex = templatedItem.itemTemplateIndex;
        templatedText.text              = textItem;

        if (_templatedTexts.size() > pageItemsTemplateAssociationData.pageIndex)
        {
            _templatedTexts[pageItemsTemplateAssociationData.pageIndex].push_back(templatedText);
        }
        else { _templatedTexts.push_back({ templatedText }); }
    }
    else
    {
        qCWarning(dataDocumentAssociationControllerCategory)
          << "Inconsistant data, text template found but item is not a text !";
    }
}

}    // namespace datadocumentassociation::controller
