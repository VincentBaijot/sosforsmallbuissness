cmake_minimum_required(VERSION 3.22)

print_location()

set(HEADERS
    Constants.hpp
    DataDocumentAssociationController_global.hpp
    DataItemsAssociation.hpp
    DataDocumentAssociation.hpp
    GeneralDataItemsAssociation.hpp
    DataDocumentsHandlerAssociation.hpp
)

set(SOURCES
    Constants.cpp
    DataItemsAssociation.cpp
    DataDocumentAssociation.cpp
    GeneralDataItemsAssociation.cpp
    DataDocumentsHandlerAssociation.cpp
)

# Short compilation library name to avoid windows long path issues
set(LIBRARY_NAME DDAC)

qt_add_library(${LIBRARY_NAME} SHARED ${HEADERS} ${SOURCES})
# Library explicit name
add_library(DataDocumentAssociation::controllers ALIAS ${LIBRARY_NAME})

set_target_properties(${LIBRARY_NAME} PROPERTIES OUTPUT_NAME "SosDataDocumentAssociationController")

target_include_directories(${LIBRARY_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(${LIBRARY_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/..)

target_link_libraries(
  ${LIBRARY_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Core
                          Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Qml)
target_link_libraries(${LIBRARY_NAME} PUBLIC DataDocumentAssociation::data)
target_link_libraries(${LIBRARY_NAME} PRIVATE DataDocumentAssociation::services)
target_link_libraries(
  ${LIBRARY_NAME} PRIVATE Document Painter DocumentDataDefinitions
                          DocumentTemplate DataTemplateAssociation Utils)

target_compile_definitions(${LIBRARY_NAME}
                           PRIVATE DATA_DOCUMENT_ASSOCIATION_CONTROLLER_LIBRARY)

install(TARGETS ${LIBRARY_NAME} BUNDLE DESTINATION .)
