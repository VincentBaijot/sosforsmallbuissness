#include "DataItemsAssociation.hpp"

#include <QModelIndex>
#include <utility>

#include "Constants.hpp"
#include "DataDocumentAssociation/data/DataItemsAssociationData.hpp"
#include "Document/controllers/Document.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "services/DataItemsAssociationService.hpp"

namespace datadocumentassociation::controller
{

DataItemsAssociation::DataItemsAssociation(QObject* parent) : QObject { parent }
{
}

DataItemsAssociation::DataItemsAssociation(
  document::controller::Document* const document,
  const documentdatadefinitions::controller::DataTablesModel* generalDataTablesModel,
  const documentdatadefinitions::controller::DataTablesModel* listDataTablesModel,
  documenttemplate::data::DocumentTemplateData documentTemplateData,
  int paintableListTemplateIndex,
  const datadocumentassociation::data::DataItemsAssociationData& objectData,
  QObject* parent)
    : QObject { parent },
      _document { document },
      _generalDataTablesModel { generalDataTablesModel },
      _listDataTablesModel { listDataTablesModel },
      _documentTemplateData { std::move(documentTemplateData) },
      _paintableListTemplateIndex { paintableListTemplateIndex }
{
    setObjectData(objectData);

    if (_listDataTablesModel)
    {
        QObject::connect(_listDataTablesModel,
                         &QAbstractItemModel::dataChanged,
                         this,
                         &DataItemsAssociation::_onTableDataChanged);
        QObject::connect(_listDataTablesModel,
                         &QAbstractItemModel::rowsInserted,
                         this,
                         &DataItemsAssociation::_onTableRowInserted);
        QObject::connect(
          _listDataTablesModel, &QAbstractItemModel::rowsRemoved, this, &DataItemsAssociation::_onTableRowRemoved);
    }
}

const documentdatadefinitions::controller::DataTablesModel* DataItemsAssociation::listDataTablesModel() const
{
    return _listDataTablesModel;
}

void DataItemsAssociation::setListDataTablesModel(
  const documentdatadefinitions::controller::DataTablesModel* const newListDataTablesModel)
{
    if (_listDataTablesModel)
    {
        QObject::disconnect(newListDataTablesModel,
                            &QAbstractItemModel::dataChanged,
                            this,
                            &DataItemsAssociation::_onTableDataChanged);
        QObject::disconnect(newListDataTablesModel,
                            &QAbstractItemModel::rowsInserted,
                            this,
                            &DataItemsAssociation::_onTableRowInserted);
        QObject::disconnect(newListDataTablesModel,
                            &QAbstractItemModel::rowsRemoved,
                            this,
                            &DataItemsAssociation::_onTableRowRemoved);
    }

    _listDataTablesModel = newListDataTablesModel;

    if (_listDataTablesModel)
    {
        QObject::connect(newListDataTablesModel,
                         &QAbstractItemModel::dataChanged,
                         this,
                         &DataItemsAssociation::_onTableDataChanged);
        QObject::connect(newListDataTablesModel,
                         &QAbstractItemModel::rowsInserted,
                         this,
                         &DataItemsAssociation::_onTableRowInserted);
        QObject::connect(newListDataTablesModel,
                         &QAbstractItemModel::rowsRemoved,
                         this,
                         &DataItemsAssociation::_onTableRowRemoved);
    }
}

datadocumentassociation::data::DataItemsAssociationData DataItemsAssociation::objectData() const
{
    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData;

    for (int rowIndex = 0; rowIndex < _templatedItems.size(); ++rowIndex)
    {
        datadocumentassociation::data::PageItemsTemplateAssociationData dataRowItemsAssociation =
          dataRowItemsAssociationData(rowIndex);
        if (dataRowItemsAssociation != datadocumentassociation::data::PageItemsTemplateAssociationData {})
        {
            dataItemsAssociationData.rows.push_back(dataRowItemsAssociation);
        }
    }

    return dataItemsAssociationData;
}

void DataItemsAssociation::setObjectData(const datadocumentassociation::data::DataItemsAssociationData& newData)
{
    for (int rowIndex = 0; rowIndex < newData.rows.size(); ++rowIndex)
    {
        insertDataRowItemsAssociationData(rowIndex, newData.rows.at(rowIndex));
    }
}

datadocumentassociation::data::PageItemsTemplateAssociationData DataItemsAssociation::dataRowItemsAssociationData(
  int rowIndex) const
{
    Q_ASSERT(rowIndex < _templatedItems.size());
    Q_ASSERT(_document);

    datadocumentassociation::data::PageItemsTemplateAssociationData dataRowItemsAssociationData;

    const QList<TemplatedItem>& templatedItems = _templatedItems.at(rowIndex);

    if (templatedItems.empty())
    {
        qCWarning(dataDocumentAssociationControllerCategory) << "No item found for row " << rowIndex;
        return {};
    }

    const TemplatedItem& firstTemplatedItem = templatedItems.at(0);

    for (int pageIndex = 0; pageIndex < _document->rowCount(); ++pageIndex)
    {
        const auto* const page = _document->at(pageIndex);

        int indexOfItem = page->indexOf(firstTemplatedItem.item);

        if (indexOfItem != -1)
        {
            dataRowItemsAssociationData.pageIndex = pageIndex;
            break;
        }
    }

    const auto* const page = _document->at(dataRowItemsAssociationData.pageIndex);

    for (const TemplatedItem& templatedItem : templatedItems)
    {
        int indexOfItem = page->indexOf(templatedItem.item);

        if (indexOfItem == -1)
        {
            qCWarning(dataDocumentAssociationControllerCategory)
              << "One item cannot be found in the page " << dataRowItemsAssociationData.pageIndex;
            return {};
        }

        datadocumentassociation::data::TemplatedItemData templatedItemData;
        templatedItemData.itemTemplateIndex = templatedItem.itemTemplateIndex;
        templatedItemData.itemIndex         = indexOfItem;

        dataRowItemsAssociationData.templatedItems.push_back(templatedItemData);
    }

    return dataRowItemsAssociationData;
}

void DataItemsAssociation::insertDataRowItemsAssociationData(
  int rowIndex,
  const datadocumentassociation::data::PageItemsTemplateAssociationData& dataRowItemsAssociationData)
{
    Q_ASSERT(_document);
    Q_ASSERT(_templatedItems.size() >= rowIndex);

    painter::controller::ListableItemListModel* const page = _document->at(dataRowItemsAssociationData.pageIndex);

    for (const datadocumentassociation::data::TemplatedItemData& templatedItemData :
         dataRowItemsAssociationData.templatedItems)
    {
        TemplatedItem templatedItem;
        templatedItem.itemTemplateIndex = templatedItemData.itemTemplateIndex;
        templatedItem.item              = page->at(templatedItemData.itemIndex);

        if (_templatedItems.size() > rowIndex) { _templatedItems[rowIndex].push_back(templatedItem); }
        else { _templatedItems.push_back({ templatedItem }); }

        _insertTemplatedTextItem(templatedItem, rowIndex);
    }
}

qsizetype DataItemsAssociation::rowCount() const
{
    return _templatedItems.size();
}

qsizetype DataItemsAssociation::columnCount(uint row) const
{
    return _templatedItems.at(row).size();
}

const painter::data::VariantListableItemData& DataItemsAssociation::itemTemplate(uint row,
                                                                                        uint columnIndex) const
{
    return paintableListTemplate().paintableItemListModel.paintableItemList.at(
      _templatedItems.at(row).at(columnIndex).itemTemplateIndex);
}

painter::controller::InterfaceQmlPaintableItem* DataItemsAssociation::item(uint row, uint columnIndex) const
{
    return _templatedItems.at(row).at(columnIndex).item;
}

void DataItemsAssociation::_onTableDataChanged(const QModelIndex& topLeft,
                                               const QModelIndex& bottomRight,
                                               const QList<int>& roles) const
{
    Q_ASSERT_X(_listDataTablesModel,
               "DataItemsAssociation::_onTableDataChanged",
               "DataTablesModel is required to update document on data changed");

    if (!roles.contains(Qt::DisplayRole)) { return; }

    for (int dataRow = topLeft.row(); dataRow <= bottomRight.row(); ++dataRow)
    {
        const QList<TemplatedText> templatedTexts = _templatedTexts.at(dataRow);

        for (const TemplatedText& templatedText : templatedTexts)
        {
            QString updatedText = std::get<painter::data::PaintableTextData>(
                                    paintableListTemplate().paintableItemListModel.paintableItemList.at(
                                      templatedText.itemTemplateIndex))
                                    .text;

            for (int dataColumn = 0; dataColumn < _listDataTablesModel->columnCount(); ++dataColumn)
            {
                const documentdatadefinitions::controller::DataDefinition* const dataDefinition =
                  _listDataTablesModel->dataDefinitionModel()->at(dataColumn);

                QString dataString =
                  _listDataTablesModel->data(_listDataTablesModel->index(dataRow, dataColumn), Qt::DisplayRole)
                    .toString();

                updatedText = document::service::documentgenerator::generateTemplatedText(
                  updatedText, dataDefinition->key(), dataString);
            }

            templatedText.text->setText(updatedText);
        }
    }
}

void DataItemsAssociation::_onTableRowInserted(const QModelIndex& parent, int first, int last)
{
    Q_UNUSED(parent)
    Q_ASSERT_X(_listDataTablesModel,
               "DataItemsAssociation::_onTableRowInserted",
               "DataTablesModel is required to update document on row inserted");
    Q_ASSERT_X(_document,
               "DataItemsAssociation::_onTableRowInserted",
               "Document is required to update document on row inserted");
    Q_ASSERT_X(_generalDataTablesModel,
               "DataItemsAssociation::_onTableRowInserted",
               "General DataTableModel is required to update document on row inserted");

    const painter::data::PaintableListData& paintableListTemplateData = paintableListTemplate();
    document::service::documentgenerator::ItemsPerPage itemsPerPage =
      document::service::documentgenerator::calculateItemsPerPage(paintableListTemplateData);

    for (int index = first; index <= last; ++index)
    {
        document::service::documentgenerator::ItemPosition itemPosition =
          document::service::documentgenerator::calculateItemPositionFromIndex(itemsPerPage, index);

        if (_document->rowCount() <= itemPosition.page)
        {
            _addPageToDocument(index, itemPosition, paintableListTemplateData);
        }
        else { _addItemsToPage(index, itemPosition, paintableListTemplateData); }
    }
}

void DataItemsAssociation::_onTableRowRemoved(const QModelIndex& parent, int first, int last)
{
    Q_UNUSED(parent)
    Q_ASSERT_X(_document,
               "DataItemsAssociation::_onTableRowRemoved",
               "Document is required to update document on row removed");

    Q_ASSERT_X(
      first == last, "DataItemsAssociation::_onTableRowRemoved", "Not implemented to remove multiple rows");

    painter::data::PaintableListData paintableListTemplateData = paintableListTemplate();
    document::service::documentgenerator::ItemsPerPage itemsPerPage =
      document::service::documentgenerator::calculateItemsPerPage(paintableListTemplateData);

    document::service::documentgenerator::ItemPosition itemPosition =
      document::service::documentgenerator::calculateItemPositionFromIndex(itemsPerPage, first);
    for (int rowIndex = static_cast<int>(_templatedItems.size()) - 2; rowIndex >= first; --rowIndex)
    {
        for (int columnIndex = 0; columnIndex < _templatedItems.at(rowIndex).size(); ++columnIndex)
        {
            _moveItemToPreviousRow(rowIndex, columnIndex, itemsPerPage);
        }

        for (int columnIndex = 0; columnIndex < _templatedTexts.at(rowIndex).size(); ++columnIndex)
        {
            _templatedTexts[rowIndex + 1][columnIndex].itemTemplateIndex =
              _templatedTexts[rowIndex][columnIndex].itemTemplateIndex;
        }
    }

    for (const TemplatedItem& templatedItem : _templatedItems.at(first))
    {
        _document->at(itemPosition.page)->removePaintableItem(templatedItem.item);
        delete templatedItem.item;
    }

    _templatedItems.remove(first);
    _templatedTexts.remove(first);
}

void DataItemsAssociation::_addPageToDocument(
  int index,
  document::service::documentgenerator::ItemPosition itemPosition,
  const painter::data::PaintableListData& paintableListTemplateData)
{
    _document->addPage();

    datadocumentassociation::data::PageItemsTemplateAssociationData dataRowAssociationData;
    dataRowAssociationData.pageIndex = itemPosition.page;

    painter::controller::ListableItemListModel* page = _document->at(itemPosition.page);

    QList<datadocumentassociation::data::TemplatedItemAssociation> templatedItems =
      service::insertNewPageItem(itemPosition,
                                 _documentTemplateData.rootPaintableItemListData,
                                 paintableListTemplateData,
                                 *_generalDataTablesModel);

    QList<datadocumentassociation::data::TemplatedItemData> templatedDataRowItems;

    for (const datadocumentassociation::data::TemplatedItemAssociation& templatedItem : templatedItems)
    {
        if (templatedItem.itemIndex != -1)
        {
            templatedDataRowItems.push_back(datadocumentassociation::data::TemplatedItemData {
              templatedItem.itemTemplateIndex, templatedItem.itemIndex });
        }

        page->addPaintableItem(std::visit([page](const auto& item)
                                          { return service::createPaintableItem(item, page); },
                                          templatedItem.itemTemplate)
                                 .get());
    }

    dataRowAssociationData.templatedItems = templatedDataRowItems;
    insertDataRowItemsAssociationData(index, dataRowAssociationData);
}

void DataItemsAssociation::_addItemsToPage(
  int index,
  document::service::documentgenerator::ItemPosition itemPosition,
  const painter::data::PaintableListData& paintableListTemplateData)
{
    painter::controller::ListableItemListModel* const page = _document->at(itemPosition.page);
    int indexOfNextItem                        = 0;

    // We have no items of this list in the current page, so the last templatedItems cannot give us the
    // index where the item must be inserted
    if (itemPosition.primaryRepetitionIndex == 0 && itemPosition.secondaryRepetitionIndex == 0)
    {
        indexOfNextItem =
          service::indexOfNextItem(page->objectData(), _documentTemplateData, _paintableListTemplateIndex);
    }
    else { indexOfNextItem = page->indexOf(_templatedItems.last().last().item) + 1; }

    QList<datadocumentassociation::data::TemplatedItemAssociation> templatedDataRowItems =
      service::insertNewListRowItem(paintableListTemplateData, itemPosition, indexOfNextItem);

    datadocumentassociation::data::PageItemsTemplateAssociationData dataRowAssociationData;
    dataRowAssociationData.pageIndex = itemPosition.page;

    for (const datadocumentassociation::data::TemplatedItemAssociation& templatedItem : templatedDataRowItems)
    {
        page->insertPaintableItem(
          indexOfNextItem,
          std::visit([page](const auto& item) { return service::createPaintableItem(item, page); },
                     templatedItem.itemTemplate));
        dataRowAssociationData.templatedItems.push_back(datadocumentassociation::data::TemplatedItemData {
          templatedItem.itemTemplateIndex, templatedItem.itemIndex });
        ++indexOfNextItem;
    }

    insertDataRowItemsAssociationData(index, dataRowAssociationData);
}

void DataItemsAssociation::_moveItemToPreviousRow(int rowIndex,
                                                  int columnIndex,
                                                  document::service::documentgenerator::ItemsPerPage itemsPerPage)
{
    _templatedItems[rowIndex + 1][columnIndex].itemTemplateIndex =
      _templatedItems[rowIndex][columnIndex].itemTemplateIndex;

    document::service::documentgenerator::ItemPosition currentItemPosition =
      document::service::documentgenerator::calculateItemPositionFromIndex(itemsPerPage, rowIndex);
    document::service::documentgenerator::ItemPosition nextItemPosition =
      document::service::documentgenerator::calculateItemPositionFromIndex(itemsPerPage, rowIndex + 1);

    if (currentItemPosition.page != nextItemPosition.page)
    {
        int itemIndex = _document->at(currentItemPosition.page)->indexOf(_templatedItems.at(rowIndex).last().item);
        _document->at(nextItemPosition.page)
          ->removePaintableItem(_templatedItems.at(rowIndex + 1).at(columnIndex).item);
        _document->at(currentItemPosition.page)
          ->insertPaintableItem(
            itemIndex + 1 + columnIndex,
            qobject_cast<painter::controller::AbstractListableItem*>(_templatedItems.at(rowIndex + 1).at(columnIndex).item));
    }

    if (const auto* const rectangleItem =
          qobject_cast<painter::controller::PaintableRectangle*>(_templatedItems[rowIndex][columnIndex].item))
    {
        auto* nextRectangle =
          qobject_cast<painter::controller::PaintableRectangle*>(_templatedItems[rowIndex + 1][columnIndex].item);
        if (nextRectangle) { nextRectangle->setPositionData(rectangleItem->positionData()); }
        else
        {
            qCWarning(dataDocumentAssociationControllerCategory)
              << "Inconsistant item type, next item is not a rectangle";
        }
    }
    else if (const auto* const textItem =
               qobject_cast<painter::controller::PaintableText*>(_templatedItems[rowIndex][columnIndex].item))
    {
        auto* nextText = qobject_cast<painter::controller::PaintableText*>(_templatedItems[rowIndex + 1][columnIndex].item);
        if (nextText) { nextText->setPositionData(textItem->positionData()); }
        else
        {
            qCWarning(dataDocumentAssociationControllerCategory)
              << "Inconsistant item type, next item is not a text";
        }
    }
    else { qCWarning(dataDocumentAssociationControllerCategory) << "Found an item of an unexpected type"; }
}

void DataItemsAssociation::_insertTemplatedTextItem(TemplatedItem templatedItem, int rowIndex)
{
    if (const auto* const paintableTextData = std::get_if<painter::data::PaintableTextData>(
          &paintableListTemplate().paintableItemListModel.paintableItemList.at(templatedItem.itemTemplateIndex)))
    {
        if (auto* const textItem = qobject_cast<painter::controller::PaintableText*>(templatedItem.item))
        {
            TemplatedText templatedText;
            templatedText.itemTemplateIndex = templatedItem.itemTemplateIndex;
            templatedText.text              = textItem;

            if (_templatedTexts.size() > rowIndex) { _templatedTexts[rowIndex].push_back(templatedText); }
            else { _templatedTexts.push_back({ templatedText }); }
        }
        else
        {
            qCWarning(dataDocumentAssociationControllerCategory)
              << "Inconsistant data, text template found but item is not a text !";
        }
    }
}

const documentdatadefinitions::controller::DataTablesModel* DataItemsAssociation::generalDataTablesModel() const
{
    return _generalDataTablesModel;
}

void DataItemsAssociation::setGeneralDataTablesModel(
  const documentdatadefinitions::controller::DataTablesModel* const newGeneralDataTablesModel)
{
    _generalDataTablesModel = newGeneralDataTablesModel;
}

const documenttemplate::data::DocumentTemplateData& DataItemsAssociation::documentTemplateData() const
{
    return _documentTemplateData;
}

void DataItemsAssociation::setDocumentTemplateData(
  const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData)
{
    _documentTemplateData = newDocumentTemplateData;
}

const painter::data::PaintableListData& DataItemsAssociation::paintableListTemplate() const
{
    return std::get<painter::data::PaintableListData>(
      _documentTemplateData.rootPaintableItemListData.paintableList.at(_paintableListTemplateIndex));
}

void DataItemsAssociation::setPaintableListTemplateIndex(int paintableListIndex)
{
    Q_ASSERT(std::holds_alternative<painter::data::PaintableListData>(
      _documentTemplateData.rootPaintableItemListData.paintableList.at(paintableListIndex)));
    _paintableListTemplateIndex = paintableListIndex;
}

document::controller::Document* DataItemsAssociation::document() const
{
    return _document;
}

void DataItemsAssociation::setDocument(document::controller::Document* newDocument)
{
    _document = newDocument;
}

}    // namespace datadocumentassociation::controller
