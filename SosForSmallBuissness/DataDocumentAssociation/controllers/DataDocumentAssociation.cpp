#include "DataDocumentAssociation.hpp"

#include "Constants.hpp"
#include "DocumentDataDefinitions/controllers/DataTableModelListModel.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"
#include "GeneralDataItemsAssociation.hpp"

namespace datadocumentassociation::controller
{
DataDocumentAssociation::DataDocumentAssociation(QObject* parent)
    : QObject { parent }, _generalDataItemsAssociation { utils::make_qobject<GeneralDataItemsAssociation>(*this) }
{
}

DataDocumentAssociation::DataDocumentAssociation(
  document::controller::Document* document,
  documentdatadefinitions::controller::UserDataHandler* userDataHandler,
  const documenttemplate::data::DocumentTemplateData& documentTemplateData,
  const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData,
  const datadocumentassociation::data::DataDocumentAssociationData& newData,
  QObject* parent)
    : QObject { parent },
      _document { document },
      _userDataHandler { userDataHandler },
      _documentTemplateData { documentTemplateData },
      _dataTemplateAssociationData { dataTemplateAssociationData },
      _generalDataItemsAssociation { utils::make_qobject<GeneralDataItemsAssociation>(
        *this,
        _document,
        _userDataHandler->generalVariableDataList(),
        documentTemplateData,
        newData.generalListItemsAssociationData) },
      _dataItemsAssociations { _generateDataItemsAssociations(newData.dataItemsAssociationData) }
{
    setObjectData(newData);
}

DataDocumentAssociation::~DataDocumentAssociation() = default;

datadocumentassociation::data::DataDocumentAssociationData DataDocumentAssociation::objectData() const
{
    datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData;

    dataDocumentAssociationData.generalListItemsAssociationData = _generalDataItemsAssociation->objectData();

    for (const auto& [key, value] : _dataItemsAssociations)
    {
        const datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData =
          value->objectData();
        dataDocumentAssociationData.dataItemsAssociationData.insert(key, dataItemsAssociationData);
    }
    return dataDocumentAssociationData;
}

void DataDocumentAssociation::setObjectData(
  const datadocumentassociation::data::DataDocumentAssociationData& newData)
{
    for (const auto& [key, value] : _dataItemsAssociations) { value->deleteLater(); }
    _dataItemsAssociations.clear();

    _generalDataItemsAssociation->setObjectData(newData.generalListItemsAssociationData);
    _dataItemsAssociations = _generateDataItemsAssociations(newData.dataItemsAssociationData);
}

datadocumentassociation::data::DataItemsAssociationData DataDocumentAssociation::dataItemsAssociationData(
  datadocumentassociation::data::PaintableListIndex paintableListIndex) const
{
    if (const DataItemsAssociation* const item = dataItemsAssociation(paintableListIndex))
    {
        return item->objectData();
    }

    return {};
}

document::controller::Document* DataDocumentAssociation::document() const
{
    return _document;
}

void DataDocumentAssociation::setDocument(document::controller::Document* newDocument)
{
    _document = newDocument;

    _generalDataItemsAssociation->setDocument(newDocument);
}

documentdatadefinitions::controller::UserDataHandler* DataDocumentAssociation::userDataHandler() const
{
    return _userDataHandler;
}

void DataDocumentAssociation::setUserDataHandler(documentdatadefinitions::controller::UserDataHandler* newUserDataHandler)
{
    _userDataHandler = newUserDataHandler;

    _generalDataItemsAssociation->setGeneralDataTablesModel(newUserDataHandler->generalVariableDataList());
}

documenttemplate::data::DocumentTemplateData DataDocumentAssociation::documentTemplateData() const
{
    return _documentTemplateData;
}

void DataDocumentAssociation::setDocumentTemplateData(
  const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData)
{
    _documentTemplateData = newDocumentTemplateData;

    _generalDataItemsAssociation->setDocumentTemplateData(newDocumentTemplateData);
}

datatemplateassociation::data::DataTemplateAssociationData
DataDocumentAssociation::dataTemplateAssociationData() const
{
    return _dataTemplateAssociationData;
}

void DataDocumentAssociation::setDataTemplateAssociationData(
  const datatemplateassociation::data::DataTemplateAssociationData& newDataTemplateAssociationData)
{
    _dataTemplateAssociationData = newDataTemplateAssociationData;
}

const DataItemsAssociation* DataDocumentAssociation::dataItemsAssociation(
  datadocumentassociation::data::PaintableListIndex paintableListIndex) const
{
    auto found = _dataItemsAssociations.find(paintableListIndex);
    if (found == std::cend(_dataItemsAssociations) || found->second == nullptr) { return nullptr; }

    return found->second.get();
}

std::unordered_map<datadocumentassociation::data::PaintableListIndex, utils::own_qobject<DataItemsAssociation>>
DataDocumentAssociation::_generateDataItemsAssociations(
  const QHash<datadocumentassociation::data::PaintableListIndex,
              datadocumentassociation::data::DataItemsAssociationData>& dataItemsAssociationDataMap)
{
    std::unordered_map<datadocumentassociation::data::PaintableListIndex, utils::own_qobject<DataItemsAssociation>>
      dataItemsAssociations;

    for (auto iterator = std::cbegin(dataItemsAssociationDataMap);
         iterator != std::cend(dataItemsAssociationDataMap);
         ++iterator)
    {
        const datadocumentassociation::data::PaintableListIndex& paintableListIndex             = iterator.key();
        const datadocumentassociation::data::DataItemsAssociationData& dataItemsAssociationData = iterator.value();

        if (paintableListIndex == -1)
        {
            qCWarning(dataDocumentAssociationControllerCategory) << "Invalid paintable list index found";
        }
        else
        {
            datatemplateassociation::data::DataTablesModelIndex dataTablesModelIndex =
              _dataTemplateAssociationData.paintableListDataTableModelAssociationData.association.value(
                paintableListIndex);
            documentdatadefinitions::controller::DataTablesModel* const listDataModel =
              _userDataHandler->dataTableModelListModel()->at(dataTablesModelIndex);

            dataItemsAssociations.try_emplace(
              paintableListIndex,
              utils::make_qobject<DataItemsAssociation>(*this,
                                                        _document,
                                                        _userDataHandler->generalVariableDataList(),
                                                        listDataModel,
                                                        _documentTemplateData,
                                                        paintableListIndex,
                                                        dataItemsAssociationData));
        }
    }

    return dataItemsAssociations;
}
}    // namespace datadocumentassociation::controller
