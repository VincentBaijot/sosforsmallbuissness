#pragma once

#include <QObject>

#include "DataDocumentAssociation.hpp"
#include "DataDocumentAssociation/data/DataDocumentsHandlerAssociationData.hpp"
#include "DataDocumentAssociationController_global.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationMapData.hpp"
#include "DocumentTemplate/data/TemplatesHandlerData.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace document::controller
{
class DocumentHandler;
}

namespace documentdatadefinitions::controller
{
class UserDataHandler;
}

namespace datadocumentassociation::controller
{
class DATA_DOCUMENT_ASSOCIATION_CONTROLLER_EXPORT DataDocumentsHandlerAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit DataDocumentsHandlerAssociation(QObject* parent = nullptr);
    DataDocumentsHandlerAssociation(
      document::controller::DocumentHandler* documentHandler,
      documentdatadefinitions::controller::UserDataHandler* userDataHandler,
      documenttemplate::data::TemplatesHandlerData templateHandlerData,
      datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData,
      const datadocumentassociation::data::DataDocumentsHandlerAssociationData& newData,
      QObject* parent);
    ~DataDocumentsHandlerAssociation() override;

    [[nodiscard]] document::controller::DocumentHandler* documentHandler() const;
    void setDocumentHandler(document::controller::DocumentHandler* newDocumentHandler);

    [[nodiscard]] documentdatadefinitions::controller::UserDataHandler* userDataHandler() const;
    void setUserDataHandler(documentdatadefinitions::controller::UserDataHandler* newUserDataHandler);

    [[nodiscard]] documenttemplate::data::TemplatesHandlerData templateHandlerData() const;
    void setTemplateHandlerData(const documenttemplate::data::TemplatesHandlerData& newTemplateHandlerData);

    [[nodiscard]] datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapdata()
      const;
    void setDataTemplateAssociationMapdata(
      const datatemplateassociation::data::DataTemplateAssociationMapData& newDataTemplateAssociationMapdata);

    [[nodiscard]] datadocumentassociation::data::DataDocumentsHandlerAssociationData objectData() const;
    void setObjectData(const datadocumentassociation::data::DataDocumentsHandlerAssociationData& newData);

    [[nodiscard]] const std::unordered_map<datadocumentassociation::data::DocumentIndex,
                                           utils::own_qobject<DataDocumentAssociation>>&
    dataDocumentAssociation() const;

  private:
    Q_DISABLE_COPY_MOVE(DataDocumentsHandlerAssociation)

    QPointer<document::controller::DocumentHandler> _documentHandler;
    QPointer<documentdatadefinitions::controller::UserDataHandler> _userDataHandler;
    documenttemplate::data::TemplatesHandlerData _templateHandlerData;
    datatemplateassociation::data::DataTemplateAssociationMapData _dataTemplateAssociationMapdata;

    std::unordered_map<datadocumentassociation::data::DocumentIndex, utils::own_qobject<DataDocumentAssociation>>
      _dataDocumentAssociation;
};
}    // namespace datadocumentassociation::controller
