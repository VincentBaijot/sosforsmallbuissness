#pragma once

#include <QObject>

#include "DataDocumentAssociation/data/DataDocumentAssociationData.hpp"
#include "DataDocumentAssociationController_global.hpp"
#include "DataItemsAssociation.hpp"
#include "DataTemplateAssociation/data/DataTemplateAssociationData.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace document
{
class Document;
}

namespace documentdatadefinitions::controller
{
class UserDataHandler;
}

namespace datadocumentassociation::controller
{
class GeneralDataItemsAssociation;

class DATA_DOCUMENT_ASSOCIATION_CONTROLLER_EXPORT DataDocumentAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit DataDocumentAssociation(QObject* parent = nullptr);
    DataDocumentAssociation(
      document::controller::Document* document,
      documentdatadefinitions::controller::UserDataHandler* userDataHandler,
      const documenttemplate::data::DocumentTemplateData& documentTemplateData,
      const datatemplateassociation::data::DataTemplateAssociationData& dataTemplateAssociationData,
      const datadocumentassociation::data::DataDocumentAssociationData& newData,
      QObject* parent = nullptr);
    ~DataDocumentAssociation() override;

    datadocumentassociation::data::DataDocumentAssociationData objectData() const;
    void setObjectData(const datadocumentassociation::data::DataDocumentAssociationData& newData);

    datadocumentassociation::data::DataItemsAssociationData dataItemsAssociationData(
      datadocumentassociation::data::PaintableListIndex paintableListIndex) const;

    document::controller::Document* document() const;
    void setDocument(document::controller::Document* newDocument);

    documentdatadefinitions::controller::UserDataHandler* userDataHandler() const;
    void setUserDataHandler(documentdatadefinitions::controller::UserDataHandler* newUserDataHandler);

    documenttemplate::data::DocumentTemplateData documentTemplateData() const;
    void setDocumentTemplateData(
      const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData);

    datatemplateassociation::data::DataTemplateAssociationData dataTemplateAssociationData() const;
    void setDataTemplateAssociationData(
      const datatemplateassociation::data::DataTemplateAssociationData& newDataTemplateAssociationData);

    const DataItemsAssociation* dataItemsAssociation(
      datadocumentassociation::data::PaintableListIndex paintableListIndex) const;

  private:
    Q_DISABLE_COPY_MOVE(DataDocumentAssociation)

    std::unordered_map<datadocumentassociation::data::PaintableListIndex, utils::own_qobject<DataItemsAssociation>>
    _generateDataItemsAssociations(
      const QHash<datadocumentassociation::data::PaintableListIndex,
                  datadocumentassociation::data::DataItemsAssociationData>& dataItemsAssociationData);

    document::controller::Document* _document { nullptr };
    documentdatadefinitions::controller::UserDataHandler* _userDataHandler { nullptr };
    documenttemplate::data::DocumentTemplateData _documentTemplateData;
    datatemplateassociation::data::DataTemplateAssociationData _dataTemplateAssociationData;

    utils::own_qobject<GeneralDataItemsAssociation> _generalDataItemsAssociation;
    std::unordered_map<datadocumentassociation::data::PaintableListIndex, utils::own_qobject<DataItemsAssociation>>
      _dataItemsAssociations;
};

}    // namespace datadocumentassociation::controller
