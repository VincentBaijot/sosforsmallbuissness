#pragma once

#include <QObject>

#include "DataDocumentAssociation/data/GeneralListItemsAssociationData.hpp"
#include "DataDocumentAssociationController_global.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"

namespace document::controller
{
class Document;
}

namespace documentdatadefinitions::controller
{
class DataTablesModel;
}

namespace painter::controller
{
class PaintableText;
class InterfaceQmlPaintableItem;
}    // namespace painter::controller

namespace datadocumentassociation::controller
{
class DATA_DOCUMENT_ASSOCIATION_CONTROLLER_TESTING_EXPORT GeneralDataItemsAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit GeneralDataItemsAssociation(QObject* parent = nullptr);
    GeneralDataItemsAssociation(document::controller::Document* document,
                                const documentdatadefinitions::controller::DataTablesModel* generalDataTablesModel,
                                const documenttemplate::data::DocumentTemplateData& documentTemplateData,
                                const datadocumentassociation::data::GeneralListItemsAssociationData& data,
                                QObject* parent = nullptr);
    ~GeneralDataItemsAssociation() override = default;

    document::controller::Document* document() const;
    void setDocument(document::controller::Document* newDocument);

    const documentdatadefinitions::controller::DataTablesModel* generalDataTablesModel() const;
    void setGeneralDataTablesModel(
      const documentdatadefinitions::controller::DataTablesModel* newGeneralDataTablesModel);

    documenttemplate::data::DocumentTemplateData documentTemplateData() const;
    void setDocumentTemplateData(
      const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData);

    datadocumentassociation::data::GeneralListItemsAssociationData objectData() const;
    void setObjectData(const datadocumentassociation::data::GeneralListItemsAssociationData& newData);

    void insertTemplatedItems(
      const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData);

  private slots:
    void _onTableDataChanged(const QModelIndex& topLeft,
                             const QModelIndex& bottomRight,
                             const QList<int>& roles = QList<int>()) const;

  private:
    Q_DISABLE_COPY_MOVE(GeneralDataItemsAssociation)

    struct TemplatedItem
    {
        int itemTemplateIndex { -1 };
        painter::controller::InterfaceQmlPaintableItem* item { nullptr };
    };

    struct TemplatedText
    {
        int itemTemplateIndex { -1 };
        painter::controller::PaintableText* text { nullptr };
    };

    void _insertTextTemplateItem(
      TemplatedItem templatedItem,
      const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData);

    document::controller::Document* _document { nullptr };
    const documentdatadefinitions::controller::DataTablesModel* _generalDataTablesModel { nullptr };
    documenttemplate::data::DocumentTemplateData _documentTemplateData;
    // For each page
    QList<QList<TemplatedItem>> _templatedItems;
    QList<QList<TemplatedText>> _templatedTexts;
};

}    // namespace datadocumentassociation::controller
