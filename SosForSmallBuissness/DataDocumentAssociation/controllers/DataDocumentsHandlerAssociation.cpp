#include "DataDocumentsHandlerAssociation.hpp"

#include <utility>

#include "Document/controllers/DocumentHandler.hpp"
#include "DocumentDataDefinitions/controllers/UserDataHandler.hpp"

namespace datadocumentassociation::controller
{
DataDocumentsHandlerAssociation::DataDocumentsHandlerAssociation(QObject* parent) : QObject { parent }
{
}

DataDocumentsHandlerAssociation::DataDocumentsHandlerAssociation(
  document::controller::DocumentHandler* documentHandler,
  documentdatadefinitions::controller::UserDataHandler* userDataHandler,
  documenttemplate::data::TemplatesHandlerData templateHandlerData,
  datatemplateassociation::data::DataTemplateAssociationMapData dataTemplateAssociationMapData,
  const datadocumentassociation::data::DataDocumentsHandlerAssociationData& newData,
  QObject* parent)
    : QObject { parent },
      _documentHandler { documentHandler },
      _userDataHandler { userDataHandler },
      _templateHandlerData { std::move(templateHandlerData) },
      _dataTemplateAssociationMapdata { std::move(dataTemplateAssociationMapData) }
{
    setObjectData(newData);
}

DataDocumentsHandlerAssociation::~DataDocumentsHandlerAssociation() = default;

document::controller::DocumentHandler* DataDocumentsHandlerAssociation::documentHandler() const
{
    return _documentHandler;
}

void DataDocumentsHandlerAssociation::setDocumentHandler(document::controller::DocumentHandler* newDocumentHandler)
{
    _documentHandler = newDocumentHandler;
}

documentdatadefinitions::controller::UserDataHandler* DataDocumentsHandlerAssociation::userDataHandler() const
{
    return _userDataHandler;
}

void DataDocumentsHandlerAssociation::setUserDataHandler(
  documentdatadefinitions::controller::UserDataHandler* newUserDataHandler)
{
    _userDataHandler = newUserDataHandler;
}

documenttemplate::data::TemplatesHandlerData DataDocumentsHandlerAssociation::templateHandlerData() const
{
    return _templateHandlerData;
}

void DataDocumentsHandlerAssociation::setTemplateHandlerData(
  const documenttemplate::data::TemplatesHandlerData& newTemplateHandlerData)
{
    _templateHandlerData = newTemplateHandlerData;
}

datatemplateassociation::data::DataTemplateAssociationMapData
DataDocumentsHandlerAssociation::dataTemplateAssociationMapdata() const
{
    return _dataTemplateAssociationMapdata;
}

void DataDocumentsHandlerAssociation::setDataTemplateAssociationMapdata(
  const datatemplateassociation::data::DataTemplateAssociationMapData& newDataTemplateAssociationMapdata)
{
    _dataTemplateAssociationMapdata = newDataTemplateAssociationMapdata;
}

datadocumentassociation::data::DataDocumentsHandlerAssociationData DataDocumentsHandlerAssociation::objectData()
  const
{
    datadocumentassociation::data::DataDocumentsHandlerAssociationData dataDocumentHandlerAssociationData;

    for (const auto& [key, value] : _dataDocumentAssociation)
    {
        dataDocumentHandlerAssociationData.dataDocumentsAssociationData.insert(key, value->objectData());
    }

    return dataDocumentHandlerAssociationData;
}

void DataDocumentsHandlerAssociation::setObjectData(
  const datadocumentassociation::data::DataDocumentsHandlerAssociationData& newData)
{
    for (const auto& [key, value] : _dataDocumentAssociation) { value->deleteLater(); }
    _dataDocumentAssociation.clear();

    for (auto iterator = std::cbegin(newData.dataDocumentsAssociationData);
         iterator != std::cend(newData.dataDocumentsAssociationData);
         ++iterator)
    {
        _dataDocumentAssociation.try_emplace(
          iterator.key(),
          utils::make_qobject<DataDocumentAssociation>(
            *this,
            _documentHandler->at(iterator.key()),
            _userDataHandler,
            _templateHandlerData.documentTemplates.at(iterator.key()),
            _dataTemplateAssociationMapdata.dataDocumentAssociationMap.value(iterator.key()),
            iterator.value()));
    }
}

const std::unordered_map<datadocumentassociation::data::DocumentIndex,
                         utils::own_qobject<DataDocumentAssociation>>&
DataDocumentsHandlerAssociation::dataDocumentAssociation() const
{
    return _dataDocumentAssociation;
}

}    // namespace datadocumentassociation::controller
