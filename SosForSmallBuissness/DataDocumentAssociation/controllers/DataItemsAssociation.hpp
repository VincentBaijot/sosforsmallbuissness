#pragma once

#include <QMultiHash>
#include <QObject>

#include "DataDocumentAssociation/data/DataItemsAssociationData.hpp"
#include "DataDocumentAssociationController_global.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"
#include "Painter/data/PaintableListData.hpp"

namespace documentdatadefinitions::controller
{
class DataDefinition;
class DataTablesModel;
}    // namespace documentdatadefinitions::controller

namespace painter::controller
{
class PaintableText;
class InterfaceQmlPaintableItem;
}    // namespace painter::controller

namespace document
{
namespace controller
{
class Document;
}
namespace service::documentgenerator
{
struct ItemPosition;
struct ItemsPerPage;
}    // namespace service::documentgenerator
}    // namespace document

namespace datadocumentassociation::controller
{

class DATA_DOCUMENT_ASSOCIATION_CONTROLLER_TESTING_EXPORT DataItemsAssociation : public QObject
{
    Q_OBJECT
  public:
    explicit DataItemsAssociation(QObject* parent = nullptr);
    DataItemsAssociation(document::controller::Document* document,
                         const documentdatadefinitions::controller::DataTablesModel* generalDataTablesModel,
                         const documentdatadefinitions::controller::DataTablesModel* listDataTablesModel,
                         documenttemplate::data::DocumentTemplateData documentTemplateData,
                         int paintableListTemplateIndex,
                         const datadocumentassociation::data::DataItemsAssociationData& objectData,
                         QObject* parent = nullptr);
    ~DataItemsAssociation() override = default;

    [[nodiscard]] document::controller::Document* document() const;
    void setDocument(document::controller::Document* newDocument);

    [[nodiscard]] const documentdatadefinitions::controller::DataTablesModel* generalDataTablesModel() const;
    void setGeneralDataTablesModel(
      const documentdatadefinitions::controller::DataTablesModel* newGeneralDataTablesModel);

    [[nodiscard]] const documentdatadefinitions::controller::DataTablesModel* listDataTablesModel() const;
    void setListDataTablesModel(
      const documentdatadefinitions::controller::DataTablesModel* newListDataTablesModel);

    [[nodiscard]] const documenttemplate::data::DocumentTemplateData& documentTemplateData() const;
    void setDocumentTemplateData(
      const documenttemplate::data::DocumentTemplateData& newDocumentTemplateData);

    [[nodiscard]] const painter::data::PaintableListData& paintableListTemplate() const;
    void setPaintableListTemplateIndex(int paintableListIndex);

    [[nodiscard]] datadocumentassociation::data::DataItemsAssociationData objectData() const;
    void setObjectData(const datadocumentassociation::data::DataItemsAssociationData& newData);

    [[nodiscard]] datadocumentassociation::data::PageItemsTemplateAssociationData dataRowItemsAssociationData(
      int rowIndex) const;
    void insertDataRowItemsAssociationData(
      int rowIndex,
      const datadocumentassociation::data::PageItemsTemplateAssociationData& dataRowItemsAssociationData);

    [[nodiscard]] qsizetype rowCount() const;
    [[nodiscard]] qsizetype columnCount(uint row) const;

    [[nodiscard]] const painter::data::VariantListableItemData& itemTemplate(uint row,
                                                                                    uint columnIndex) const;
    [[nodiscard]] painter::controller::InterfaceQmlPaintableItem* item(uint row, uint columnIndex) const;

  private slots:
    void _onTableDataChanged(const QModelIndex& topLeft,
                             const QModelIndex& bottomRight,
                             const QList<int>& roles = QList<int>()) const;
    void _onTableRowInserted(const QModelIndex& parent, int first, int last);
    void _onTableRowRemoved(const QModelIndex& parent, int first, int last);

  private:
    Q_DISABLE_COPY_MOVE(DataItemsAssociation)

    void _addPageToDocument(int index,
                            document::service::documentgenerator::ItemPosition itemPosition,
                            const painter::data::PaintableListData& paintableListTemplateData);
    void _addItemsToPage(int index,
                         document::service::documentgenerator::ItemPosition itemPosition,
                         const painter::data::PaintableListData& paintableListTemplateData);
    void _moveItemToPreviousRow(int rowIndex,
                                int columnIndex,
                                document::service::documentgenerator::ItemsPerPage itemsPerPage);

    struct TemplatedItem
    {
        int itemTemplateIndex { -1 };
        painter::controller::InterfaceQmlPaintableItem* item { nullptr };
    };

    struct TemplatedText
    {
        int itemTemplateIndex { -1 };
        painter::controller::PaintableText* text { nullptr };
    };

    void _insertTemplatedTextItem(TemplatedItem templatedItem, int rowIndex);

    QPointer<document::controller::Document> _document;
    QPointer<const documentdatadefinitions::controller::DataTablesModel> _generalDataTablesModel;
    QPointer<const documentdatadefinitions::controller::DataTablesModel> _listDataTablesModel;
    documenttemplate::data::DocumentTemplateData _documentTemplateData;
    int _paintableListTemplateIndex { -1 };
    // For each data row, the list of the items associated with this list and linked to their template
    QList<QList<TemplatedItem>> _templatedItems;
    QList<QList<TemplatedText>> _templatedTexts;
};
}    // namespace datadocumentassociation::controller
