#pragma once

#include <QDebug>

namespace datadocumentassociation::data
{
struct TemplatedItemData
{
    int itemTemplateIndex;
    int itemIndex { -1 };
    bool operator==(const TemplatedItemData& templatedItemData) const = default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(QDebug debugOutput,
                         const datadocumentassociation::data::TemplatedItemData& templatedItemData)
{
    const QDebugStateSaver saver(debugOutput);

    return debugOutput.nospace() << "TemplatedItemData{" << templatedItemData.itemTemplateIndex << ','
                                 << templatedItemData.itemIndex << '}';
}
