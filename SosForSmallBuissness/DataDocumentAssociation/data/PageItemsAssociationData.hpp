#pragma once

#include "TemplatedItemData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace datadocumentassociation::data
{

struct PageItemsTemplateAssociationData
{
    int pageIndex { -1 };
    QList<TemplatedItemData> templatedItems;
    bool operator==(const PageItemsTemplateAssociationData& pageItemsTemplateAssociationData) const = default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datadocumentassociation::data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "PageItemsTemplateAssociationData{" << pageItemsTemplateAssociationData.pageIndex
                          << ",";
    utils::qDebugSequentialContainer(debugOutput, pageItemsTemplateAssociationData.templatedItems);
    debugOutput << '}';

    return debugOutput;
}
