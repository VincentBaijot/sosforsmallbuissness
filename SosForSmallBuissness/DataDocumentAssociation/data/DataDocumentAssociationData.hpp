#pragma once

#include "DataItemsAssociationData.hpp"
#include "GeneralListItemsAssociationData.hpp"

namespace datadocumentassociation::data
{
using PaintableListIndex = int;

struct DataDocumentAssociationData
{
    GeneralListItemsAssociationData generalListItemsAssociationData;
    QHash<PaintableListIndex, DataItemsAssociationData> dataItemsAssociationData;
    bool operator==(const DataDocumentAssociationData& dataDocumentAssociationData) const = default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datadocumentassociation::data::DataDocumentAssociationData& dataDocumentAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataDocumentAssociationData{"
                          << dataDocumentAssociationData.generalListItemsAssociationData << ',';
    utils::qDebugAssociativeContainer(debugOutput, dataDocumentAssociationData.dataItemsAssociationData);
    debugOutput << '}';

    return debugOutput;
}
