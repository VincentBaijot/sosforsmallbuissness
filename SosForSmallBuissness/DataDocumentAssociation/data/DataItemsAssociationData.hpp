#pragma once

#include "PageItemsAssociationData.hpp"

namespace datadocumentassociation::data
{

struct DataItemsAssociationData
{
    QList<PageItemsTemplateAssociationData> rows;
    bool operator==(const DataItemsAssociationData& dataItemsAssociationDatata) const = default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(QDebug debugOutput,
                         const datadocumentassociation::data::DataItemsAssociationData& dataItemsAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataItemsAssociationData{";
    utils::qDebugSequentialContainer(debugOutput, dataItemsAssociationData.rows);
    debugOutput << '}';

    return debugOutput;
}
