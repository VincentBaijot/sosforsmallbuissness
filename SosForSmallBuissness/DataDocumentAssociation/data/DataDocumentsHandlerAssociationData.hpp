#pragma once

#include "DataDocumentAssociationData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace datadocumentassociation::data
{

using DocumentIndex = int;

struct DataDocumentsHandlerAssociationData
{
    QHash<DocumentIndex, DataDocumentAssociationData> dataDocumentsAssociationData;
    bool operator==(const DataDocumentsHandlerAssociationData& dataDocumentsHandlerAssociationData) const =
      default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datadocumentassociation::data::DataDocumentsHandlerAssociationData& dataDocumentsHandlerAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "DataDocumentsHandlerAssociationData{";
    utils::qDebugAssociativeContainer(debugOutput,
                                      dataDocumentsHandlerAssociationData.dataDocumentsAssociationData);
    debugOutput << '}';

    return debugOutput;
}
