
#pragma once

#include "PageItemsAssociationData.hpp"
#include "Utils/DataStructureDebugSupport.hpp"

namespace datadocumentassociation::data
{

struct GeneralListItemsAssociationData
{
    QList<PageItemsTemplateAssociationData> generalListItemsAssociationData;
    bool operator==(const GeneralListItemsAssociationData& generalListItemsAssociationData) const = default;
};

}    // namespace datadocumentassociation::data

inline QDebug operator<<(
  QDebug debugOutput,
  const datadocumentassociation::data::GeneralListItemsAssociationData& generalListItemsAssociationData)
{
    const QDebugStateSaver saver(debugOutput);

    debugOutput.nospace() << "GeneralListItemsAssociationData{";
    utils::qDebugSequentialContainer(debugOutput, generalListItemsAssociationData.generalListItemsAssociationData);
    debugOutput << '}';

    return debugOutput;
}
