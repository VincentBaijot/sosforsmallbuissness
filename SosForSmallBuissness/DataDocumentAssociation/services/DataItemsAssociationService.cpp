#include "DataItemsAssociationService.hpp"

#include "DocumentDataDefinitions/controllers/DataDefinitionListModel.hpp"
#include "DocumentTemplate/data/DocumentTemplateData.hpp"

namespace datadocumentassociation::service
{

template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

template <class ItemData>
bool compareItemData(const ItemData& a, const ItemData& b);

template <class ItemData>
bool compareItemData(const ItemData& a, const painter::data::VariantListableItemData& b)
{
    if (const auto* castedItem = std::get_if<ItemData>(&b)) { return compareItemData(a, *castedItem); }
    else { return false; }
}

template <>
bool compareItemData(const painter::data::PaintableRectangleData& a,
                     const painter::data::PaintableRectangleData& b)
{
    return a == b;
}

template <>
bool compareItemData(const painter::data::PaintableTextData& a,
                     const painter::data::PaintableTextData& b)
{
    return a.background == b.background && a.color == b.color && a.flags == b.flags && a.fontFamily == b.fontFamily
           && a.pointSize == b.pointSize;
}

QList<datadocumentassociation::data::TemplatedItemAssociation> insertNewListRowItem(
  const painter::data::PaintableListData& paintableListData,
  const document::service::documentgenerator::ItemPosition& itemPosition,
  int itemIndex)
{
    QList<datadocumentassociation::data::TemplatedItemAssociation> templatedItems;
    for (int itemTemplateIndex = 0;
         itemTemplateIndex < paintableListData.paintableItemListModel.paintableItemList.size();
         ++itemTemplateIndex)
    {
        const painter::data::VariantListableItemData& variantListableItem =
          paintableListData.paintableItemListModel.paintableItemList.at(itemTemplateIndex);

        std::visit(
          [&templatedItems, paintableListData, itemPosition, itemTemplateIndex, itemIndex](const auto& item)
          {
              auto updatedItem                            = item;
              painter::data::PositionData position = document::service::documentgenerator::position(item);
              position                                    = document::service::documentgenerator::updateListItemPosition(
                paintableListData.position,
                position,
                document::service::documentgenerator::childrenRectList(paintableListData.paintableItemListModel),
                paintableListData.primaryRepetitionDirection,
                itemPosition.primaryRepetitionIndex,
                paintableListData.secondaryRepetitionDirection,
                itemPosition.secondaryRepetitionIndex);
              document::service::documentgenerator::setPosition(updatedItem, position);
              templatedItems.push_back(datadocumentassociation::data::TemplatedItemAssociation {
                updatedItem, itemTemplateIndex, itemIndex });
          },
          variantListableItem);

        itemIndex++;
    }
    return templatedItems;
}

QList<datadocumentassociation::data::TemplatedItemAssociation> insertNewPageItem(
  const document::service::documentgenerator::ItemPosition& itemPosition,
  const documenttemplate::data::RootPaintableItemListData& rootPaintableItemListData,
  const painter::data::PaintableListData& paintableListTemplate,
  const documentdatadefinitions::controller::DataTablesModel& generalDataTablesModel)
{
    QList<datadocumentassociation::data::TemplatedItemAssociation> templatedItems;
    int itemIndex = 0;

    for (const documenttemplate::data::VariantInterfaceQmlPaintableItem& itemTemplate :
         rootPaintableItemListData.paintableList)
    {
        std::visit(
          overloaded {
            [&templatedItems,
             &itemIndex](const painter::data::PaintableRectangleData& paintableRectangleData)
            {
                templatedItems.push_back(datadocumentassociation::data::TemplatedItemAssociation {
                  paintableRectangleData, itemIndex, -1 });
                itemIndex++;
            },
            [&generalDataTablesModel, &templatedItems, &itemIndex](
              const painter::data::PaintableTextData& paintableTextData)
            {
                painter::data::PaintableTextData updatedText = paintableTextData;

                for (int dataColumn = 0; dataColumn < generalDataTablesModel.columnCount(); ++dataColumn)
                {
                    const documentdatadefinitions::controller::DataDefinition* const dataDefinition =
                      generalDataTablesModel.dataDefinitionModel()->at(dataColumn);

                    QString dataString =
                      generalDataTablesModel.data(generalDataTablesModel.index(0, dataColumn), Qt::DisplayRole)
                        .toString();

                    updatedText.text = document::service::documentgenerator::generateTemplatedText(
                      updatedText.text, dataDefinition->key(), dataString);
                }

                templatedItems.push_back(
                  datadocumentassociation::data::TemplatedItemAssociation { updatedText, itemIndex, -1 });
                itemIndex++;
            },
            [paintableListTemplate, itemPosition, &itemIndex, &templatedItems](
              const painter::data::PaintableListData& paintableListData)
            {
                if (paintableListData == paintableListTemplate)
                {
                    QList<datadocumentassociation::data::TemplatedItemAssociation> listRowItems =
                      insertNewListRowItem(paintableListData, itemPosition, itemIndex);

                    itemIndex += listRowItems.size();
                    templatedItems.append(listRowItems);
                }
            } },
          itemTemplate);
    }

    return templatedItems;
}

bool correspondToListItems(const painter::data::PaintableItemListModelData& paintableItemListModel,
                           const painter::data::PaintableListData& paintableListData,
                           int indexOfCurrentItem,
                           const painter::data::PositionData& listRowRect,
                           int primaryRepetitionIndex,
                           int secondaryRepetitionIndex)
{
    for (painter::data::VariantListableItemData listItem :
         paintableListData.paintableItemListModel.paintableItemList)
    {
        painter::data::PositionData updatedPosition =
          document::service::documentgenerator::updateListItemPosition(paintableListData.position,
                                                              document::service::documentgenerator::position(listItem),
                                                              listRowRect,
                                                              paintableListData.primaryRepetitionDirection,
                                                              primaryRepetitionIndex,
                                                              paintableListData.secondaryRepetitionDirection,
                                                              secondaryRepetitionIndex);

        document::service::documentgenerator::setPosition(listItem, updatedPosition);

        if (!std::visit([listItem](const auto& currentItem) { return compareItemData(currentItem, listItem); },
                        paintableItemListModel.paintableItemList.at(indexOfCurrentItem)))
        {
            return false;
        }
        ++indexOfCurrentItem;
    }

    return true;
}

int indexOfLastListItem(const painter::data::PaintableItemListModelData& paintableItemListModel,
                        const painter::data::PaintableListData& paintableListData,
                        int indexOfCurrentItem)
{
    int primaryRepetitionIndex   = 0;
    int secondaryRepetitionIndex = 0;

    painter::data::PositionData listRowRect =
      document::service::documentgenerator::childrenRectList(paintableListData.paintableItemListModel);

    bool isListItem = correspondToListItems(paintableItemListModel,
                                            paintableListData,
                                            indexOfCurrentItem,
                                            listRowRect,
                                            primaryRepetitionIndex,
                                            secondaryRepetitionIndex);

    while (isListItem)
    {
        indexOfCurrentItem += paintableListData.paintableItemListModel.paintableItemList.size();

        ++primaryRepetitionIndex;

        isListItem = correspondToListItems(paintableItemListModel,
                                           paintableListData,
                                           indexOfCurrentItem,
                                           listRowRect,
                                           primaryRepetitionIndex,
                                           secondaryRepetitionIndex);

        if (!isListItem)
        {
            primaryRepetitionIndex = 0;
            ++secondaryRepetitionIndex;

            isListItem = correspondToListItems(paintableItemListModel,
                                               paintableListData,
                                               indexOfCurrentItem,
                                               listRowRect,
                                               primaryRepetitionIndex,
                                               secondaryRepetitionIndex);
        }
    }

    return indexOfCurrentItem;
}

// Search for the index where the first item of the given list must be inserted
int indexOfNextItem(const painter::data::PaintableItemListModelData& paintableItemListModel,
                    const documenttemplate::data::DocumentTemplateData& documentTemplateData,
                    int paintableListTemplateIndex)
{
    int indexOfLastItem     = 0;
    int indexOfTemplateItem = 0;

    while (indexOfLastItem < paintableItemListModel.paintableItemList.size()
           && indexOfTemplateItem < documentTemplateData.rootPaintableItemListData.paintableList.size())
    {
        const documenttemplate::data::VariantInterfaceQmlPaintableItem& listItemTemplate =
          documentTemplateData.rootPaintableItemListData.paintableList.at(indexOfTemplateItem);
        const painter::data::VariantListableItemData& lastItem =
          paintableItemListModel.paintableItemList.at(indexOfLastItem);

        indexOfLastItem = std::visit(
          overloaded { [paintableItemListModel,
                        indexOfLastItem](const painter::data::PaintableListData& paintableListData)
                       { return indexOfLastListItem(paintableItemListModel, paintableListData, indexOfLastItem); },
                       [lastItem, indexOfLastItem](const auto& itemTemplate) {
                           return compareItemData(itemTemplate, lastItem) ? (indexOfLastItem + 1)
                                                                          : indexOfLastItem;
                       } },
          listItemTemplate);
        ++indexOfTemplateItem;

        if (indexOfTemplateItem == paintableListTemplateIndex) { return indexOfLastItem; }
    }

    return -1;
}

}    // namespace datadocumentassociation::service
