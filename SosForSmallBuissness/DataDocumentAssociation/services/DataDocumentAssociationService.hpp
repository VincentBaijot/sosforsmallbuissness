#pragma once

#include "DataDocumentAssociation/data/DataDocumentAssociationData.hpp"
#include "DataDocumentAssociationService_global.hpp"
#include "Document/services/DocumentGenerator.hpp"

namespace datadocumentassociation::service
{

[[nodiscard]] DATA_DOCUMENT_ASSOCIATION_SERVICE_EXPORT datadocumentassociation::data::DataDocumentAssociationData
generateDataDocumentAssociationData(
  const document::service::documentgenerator::GeneratedDocument& generatedDocument);

datadocumentassociation::data::DataDocumentAssociationData generatePageDataDocumentAssociationData(
  const document::data::PageItemsAssociationData& pageItemsAssociation,
  int pageIndex);
}    // namespace datadocumentassociation::service
