#include "DataDocumentAssociationService.hpp"

namespace datadocumentassociation::service
{

QHash<data::PaintableListIndex, data::DataItemsAssociationData>& mergeDataItemsAssociationData(
  QHash<data::PaintableListIndex, data::DataItemsAssociationData>& sourceDataItemsAssociationDataMap,
  const QHash<data::PaintableListIndex, data::DataItemsAssociationData>& newDataItemsAssociationDataMap)
{
    for (auto iterator = std::cbegin(newDataItemsAssociationDataMap);
         iterator != std::cend(newDataItemsAssociationDataMap);
         ++iterator)
    {
        const data::DataItemsAssociationData& newDataItemsAssociationData = iterator.value();
        data::DataItemsAssociationData& sourceDataItemsAssociationData =
          sourceDataItemsAssociationDataMap[iterator.key()];

        std::ranges::copy_if(newDataItemsAssociationData.rows,
                             std::back_inserter(sourceDataItemsAssociationData.rows),
                             [](const data::PageItemsTemplateAssociationData& pageItemsTemplateAssociationData)
                             { return pageItemsTemplateAssociationData.pageIndex != -1; });
    }

    return sourceDataItemsAssociationDataMap;
}

datadocumentassociation::data::DataDocumentAssociationData generateDataDocumentAssociationData(
  const document::service::documentgenerator::GeneratedDocument& generatedDocument)
{
    datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData;

    for (int pageIndex = 0; pageIndex < generatedDocument.dataItemsAssociationData.pageItemsAssociationData.size();
         ++pageIndex)
    {
        const document::data::PageItemsAssociationData& pageItemsAssociation =
          generatedDocument.dataItemsAssociationData.pageItemsAssociationData.at(pageIndex);

        datadocumentassociation::data::DataDocumentAssociationData pageDataDocumentAssociationData =
          generatePageDataDocumentAssociationData(pageItemsAssociation, pageIndex);

        // On page contains only the general data of the page
        dataDocumentAssociationData.generalListItemsAssociationData.generalListItemsAssociationData.push_back(
          pageDataDocumentAssociationData.generalListItemsAssociationData.generalListItemsAssociationData.front());
        mergeDataItemsAssociationData(dataDocumentAssociationData.dataItemsAssociationData,
                                      pageDataDocumentAssociationData.dataItemsAssociationData);
    }

    return dataDocumentAssociationData;
}

data::DataDocumentAssociationData generatePageDataDocumentAssociationData(
  const document::data::PageItemsAssociationData& pageItemsAssociation,
  int pageIndex)
{
    datadocumentassociation::data::DataDocumentAssociationData dataDocumentAssociationData;

    datadocumentassociation::data::PageItemsTemplateAssociationData pageItemsTemplateAssociationData;
    pageItemsTemplateAssociationData.pageIndex = pageIndex;

    for (int itemIndex = 0; itemIndex < pageItemsAssociation.itemsAssociationData.size(); ++itemIndex)
    {
        if (const document::data::ItemAssociationData& itemAssociationData =
              pageItemsAssociation.itemsAssociationData[itemIndex];
            itemAssociationData.templateListIndex == -1)
        {
            datadocumentassociation::data::TemplatedItemData templatedItemdata;

            templatedItemdata.itemIndex         = itemIndex;
            templatedItemdata.itemTemplateIndex = itemAssociationData.templateItemIndex;

            pageItemsTemplateAssociationData.templatedItems.push_back(templatedItemdata);
        }
        else
        {
            datadocumentassociation::data::DataItemsAssociationData& dataItemsAssociationData =
              dataDocumentAssociationData.dataItemsAssociationData[itemAssociationData.templateListIndex];

            while (dataItemsAssociationData.rows.size() <= itemAssociationData.rowIndex)
            {
                dataItemsAssociationData.rows.push_back(
                  datadocumentassociation::data::PageItemsTemplateAssociationData {});
            }

            dataItemsAssociationData.rows[itemAssociationData.rowIndex].pageIndex = pageIndex;
            dataItemsAssociationData.rows[itemAssociationData.rowIndex].templatedItems.push_back(
              datadocumentassociation::data::TemplatedItemData { itemAssociationData.templateItemIndex,
                                                                 itemIndex });
        }
    }
    dataDocumentAssociationData.generalListItemsAssociationData.generalListItemsAssociationData.push_back(
      pageItemsTemplateAssociationData);

    return dataDocumentAssociationData;
}

}    // namespace datadocumentassociation::service
