#pragma once

#include "DataDocumentAssociationService_global.hpp"
#include "Document/services/DocumentGenerator.hpp"
#include "Document/services/DocumentGeneratorPosition.hpp"
#include "DocumentDataDefinitions/controllers/DataTablesModel.hpp"
#include "DocumentTemplate/data/RootPaintableItemListData.hpp"
#include "Painter/controllers/PaintableRectangle.hpp"
#include "Painter/controllers/PaintableText.hpp"
#include "Utils/OwnQObjectPointer.hpp"

namespace datadocumentassociation::data
{
struct TemplatedItemAssociation
{
    painter::data::VariantListableItemData itemTemplate;
    int itemTemplateIndex { -1 };
    int itemIndex { -1 };
};
}    // namespace datadocumentassociation::data

namespace datadocumentassociation::service
{
template <class ItemDataType>
inline utils::own_qobject<painter::controller::AbstractListableItem> createPaintableItem(const ItemDataType& paintableItemData,
                                                                             QObject* parent);

DATA_DOCUMENT_ASSOCIATION_SERVICE_EXPORT QList<datadocumentassociation::data::TemplatedItemAssociation>
insertNewPageItem(const document::service::documentgenerator::ItemPosition& itemPosition,
                  const documenttemplate::data::RootPaintableItemListData& rootPaintableItemListData,
                  const painter::data::PaintableListData& paintableListTemplate,
                  const documentdatadefinitions::controller::DataTablesModel& generalDataTablesModel);

DATA_DOCUMENT_ASSOCIATION_SERVICE_EXPORT QList<datadocumentassociation::data::TemplatedItemAssociation>
insertNewListRowItem(const painter::data::PaintableListData& paintableListData,
                     const document::service::documentgenerator::ItemPosition& itemPosition,
                     int itemIndex);

// Search for the index where the first item of the given list must be inserted
DATA_DOCUMENT_ASSOCIATION_SERVICE_EXPORT int indexOfNextItem(
  const painter::data::PaintableItemListModelData& paintableItemListModel,
  const documenttemplate::data::DocumentTemplateData& documentTemplateData,
  int paintableListTemplateIndex);

template <>
inline utils::own_qobject<painter::controller::AbstractListableItem> createPaintableItem(
  const painter::data::PaintableRectangleData& paintableRectangleData,
  QObject* parent)
{
    return utils::own_qobject<painter::controller::AbstractListableItem>(
      utils::make_qobject<painter::controller::PaintableRectangle>(*parent, paintableRectangleData));
}

template <>
inline utils::own_qobject<painter::controller::AbstractListableItem> createPaintableItem(
  const painter::data::PaintableTextData& paintableTextData,
  QObject* parent)
{
    return utils::own_qobject<painter::controller::AbstractListableItem>(
      utils::make_qobject<painter::controller::PaintableText>(*parent, paintableTextData));
}

}    // namespace datadocumentassociation::service
