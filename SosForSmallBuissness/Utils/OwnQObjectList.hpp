#pragma once

#include "OwnQObjectPointer.hpp"

namespace utils
{

template <class T>
class OwnQObjectList
{
  public:
    using value_type             = own_qobject<T>;
    using container_type         = std::vector<value_type>;
    using size_type              = container_type::size_type;
    using reference              = container_type::reference;
    using const_reference        = container_type::const_reference;
    using iterator               = container_type::iterator;
    using const_iterator         = container_type::const_iterator;
    using reverse_iterator       = container_type::reverse_iterator;
    using const_reverse_iterator = container_type::const_reverse_iterator;

    constexpr OwnQObjectList() noexcept                                       = default;
    constexpr ~OwnQObjectList() noexcept                                      = default;
    constexpr OwnQObjectList(const OwnQObjectList& other)                     = delete;
    constexpr OwnQObjectList(OwnQObjectList&& other) noexcept                 = default;
    constexpr OwnQObjectList& operator=(const OwnQObjectList& other) noexcept = delete;
    constexpr OwnQObjectList& operator=(OwnQObjectList&& other) noexcept      = default;

    constexpr reference at(size_type position) { return _container.at(position); }
    constexpr const_reference at(size_type position) const { return _container.at(position); }

    constexpr iterator begin() noexcept { return _container.begin(); }
    constexpr const_iterator begin() const noexcept { return _container.begin(); }
    constexpr iterator end() noexcept { return _container.end(); }
    constexpr const_iterator end() const noexcept { return _container.end(); }
    constexpr const_iterator cbegin() const noexcept { return _container.cbegin(); }
    constexpr const_iterator cend() const noexcept { return _container.cend(); }
    constexpr reverse_iterator rbegin() noexcept { return _container.rbegin(); }
    constexpr const_reverse_iterator rbegin() const noexcept { return _container.rbegin(); }
    constexpr reverse_iterator rend() noexcept { return _container.rend(); }
    constexpr const_reverse_iterator rend() const noexcept { return _container.rend(); }

    constexpr reference front() { return _container.front(); }
    constexpr const_reference front() const { return _container.front(); }
    constexpr reference back() { return _container.back(); }
    constexpr const_reference back() const { return _container.back(); }

    constexpr bool empty() const noexcept { return _container.empty(); }
    constexpr size_type size() const noexcept { return _container.size(); }
    constexpr void clear() noexcept { _container.clear(); }
    constexpr void reserve(size_type new_cap) { _container.reserve(new_cap); }

    constexpr bool contains(const T* const value) const { return std::find(cbegin(), cend(), value) != cend(); }
    constexpr void push_back(value_type&& value) { _container.push_back(std::move(value)); }
    template <class... Args>
    constexpr reference emplace_back(Args&&... args)
    {
        return _container.emplace_back(std::forward<Args>(args)...);
    }
    constexpr iterator insert(const_iterator position, value_type&& value)
    {
        return _container.insert(position, std::move(value));
    }

    constexpr iterator erase(const_iterator position) { return _container.erase(position); }
    constexpr iterator erase(const_iterator first, const_iterator last) { return _container.erase(first, last); }

    // gcc 10 do not accept constexpr operator== for vector
    /*constexpr*/ bool operator==(const OwnQObjectList& other) const = default;

  private:
    container_type _container;
};

}    // namespace utils
