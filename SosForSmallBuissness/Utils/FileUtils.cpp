#include "FileUtils.hpp"

#include <QDebug>
#include <QDir>
#include <QJsonDocument>
#include <QtGui/QFontDatabase>

#include "Constants.hpp"

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

namespace utils
{
void FileUtils::createFolder(const QString& folderPath)
{
    QDir folder(folderPath);

#ifdef Q_OS_ANDROID
    QtAndroid::PermissionResult r = QtAndroid::checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");
    if (r == QtAndroid::PermissionResult::Denied)
    {
        QtAndroid::requestPermissionsSync(QStringList() << "android.permission.WRITE_EXTERNAL_STORAGE");
    }
#endif

    if (!folder.exists())
    {
        if (!folder.mkpath(QStringLiteral(".")))
            qCCritical(utilsCategory) << "Cannot create folder in : " << folderPath;
        else
            qCInfo(utilsCategory) << "Created folder : " << folderPath;
    }
}

QList<int> FileUtils::loadFonts(const QString& sourceFolder)
{
    QDir sourceDirectory(sourceFolder);
    QList<int> fontIdList;

    QFileInfoList fontsToLoad = sourceDirectory.entryInfoList(QStringList { "*.ttf", "*.ttc" }, QDir::Files);
    fontIdList.reserve(fontsToLoad.size());
    for (const QFileInfo& fontFile : qAsConst(fontsToLoad))
    {
        fontIdList.append(QFontDatabase::addApplicationFont(fontFile.filePath()));
        qDebug() << "Add font to application : " << fontFile.fileName();
    }

    sourceDirectory.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

    const QStringList& subdirs(sourceDirectory.entryList());
    for (const QString& subdir : subdirs)
    {
        QList<int> subFontIdList = loadFonts(sourceDirectory.absolutePath() + QDir::separator() + subdir);
        for (int subIds : qAsConst(subFontIdList)) fontIdList.append(subIds);
    }

    return fontIdList;
}
}    // namespace utils
