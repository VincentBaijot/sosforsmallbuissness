#pragma once

#include <QXmlStreamReader>

#include "Utils_global.hpp"

namespace utils
{
namespace xml
{
UTILS_EXPORT bool isDataStartElement(const QXmlStreamReader& reader,
                                     QStringView startElementName,
                                     QStringView dataName);

UTILS_EXPORT bool checkEndElement(const QXmlStreamReader& reader, QStringView endElementName);
}    // namespace xml
}    // namespace utils
