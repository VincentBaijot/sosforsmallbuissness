#pragma once

#include <QDebug>
#include <QDebugStateSaver>
#include <QString>
#include <concepts>

namespace utils
{

template <class DataStructure>
concept DataStructureWithQDebugOperator = requires(DataStructure dataStructure, QDebug qDebugType) {
    { operator<<(qDebugType, dataStructure) } -> std::convertible_to<QDebug>;
};

template <class DataStructure>
concept DataStructureWithQDebugMember = requires(DataStructure dataStructure, QDebug qDebugType) {
    { qDebugType.operator<<(dataStructure) } -> std::convertible_to<QDebug>;
};

template <class DataStructure>
concept DataStructureWithQDebug =
  DataStructureWithQDebugOperator<DataStructure> || DataStructureWithQDebugMember<DataStructure>;

template <class ContainerType>
concept SequentialContainerWithQDebug = requires(const ContainerType constContainer) {
    requires DataStructureWithQDebug<typename ContainerType::value_type>;
    requires std::forward_iterator<typename ContainerType::const_iterator>;
    { constContainer.begin() } -> std::same_as<typename ContainerType::const_iterator>;
    { constContainer.end() } -> std::same_as<typename ContainerType::const_iterator>;
};

template <class ContainerType>
concept AssociativeContainerWithQDebug = requires(const ContainerType constContainer) {
    requires DataStructureWithQDebug<typename ContainerType::value_type>;
    requires DataStructureWithQDebug<typename ContainerType::key_type>;
    requires std::forward_iterator<typename ContainerType::const_iterator>;
    { constContainer.begin() } -> std::same_as<typename ContainerType::const_iterator>;
    { constContainer.end() } -> std::same_as<typename ContainerType::const_iterator>;
};

template <SequentialContainerWithQDebug ContainerType>
QDebug qDebugSequentialContainer(QDebug debug, const ContainerType& container)
{
    const QDebugStateSaver saver(debug);

    debug.nospace();

    debug << "{";

    typename ContainerType::const_iterator it = container.begin(), end = container.end();
    if (it != end)
    {
        debug << *it;
        ++it;
    }
    while (it != end)
    {
        debug << "," << *it;
        ++it;
    }

    debug << '}';
    return debug;
}

template <AssociativeContainerWithQDebug AssociativeContainer>
inline QDebug qDebugAssociativeContainer(QDebug debug, const AssociativeContainer& container)
{
    const QDebugStateSaver saver(debug);
    debug.nospace() << "{";
    for (typename AssociativeContainer::const_iterator it = container.begin(); it != container.end(); ++it)
    {
        debug << '{' << it.key() << ", " << it.value() << '}';
        if (std::next(it) != container.end()) { debug << ','; }
    }
    debug << '}';
    return debug;
}

// Override the debug operator to have a debug corresponding to my need, this might not work in futur version
// of qt, if the template is constrain with concept
template <DataStructureWithQDebug TypeToDebug>
inline QDebug operator<<(QDebug debug, const QList<TypeToDebug>& list)
{
    return qDebugSequentialContainer(debug, list);
}

// Override the debug operator to have a debug corresponding to my need, this might not work in futur version
// of qt, if the template is constrain with concept
template <DataStructureWithQDebug Key, DataStructureWithQDebug T>
inline QDebug operator<<(QDebug debug, const QHash<Key, T>& hash)
{
    return qDebugAssociativeContainer(debug, hash);
}

}    // namespace utils
