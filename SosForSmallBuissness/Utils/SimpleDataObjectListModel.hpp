#pragma once

#include <QAbstractListModel>
#include <QObject>
#include <concepts>

#include "OwnQObjectList.hpp"
#include "OwnQObjectPointer.hpp"

namespace utils
{

template <class ObjectType, class AssociatedData>
concept ObjectWithDataGetter = requires(ObjectType object, AssociatedData associatedData) {
    { object.objectData() } -> std::convertible_to<AssociatedData>;
};

template <class ObjectType, class AssociatedData>
concept ObjectWithDataSetter = requires(ObjectType object, AssociatedData associatedData) {
    { object.setObjectData(associatedData) };
};

template <class ObjectType, class AssociatedData>
concept ObjectWithAssociatedData =
  std::constructible_from<ObjectType, AssociatedData, QObject*> && ObjectWithDataGetter<ObjectType, AssociatedData>
  && ObjectWithDataSetter<ObjectType, AssociatedData>;

template <class ObjectType, class AssociatedData>
concept DerivedQObjectWithAssociatedData =
  DerivedQObject<ObjectType> && ObjectWithAssociatedData<ObjectType, AssociatedData>;

inline constexpr char defaultHeaderData[] = "";

template <DerivedQObject ObjectType, const char* headerValue = defaultHeaderData>
class SimpleObjectListModel : public QAbstractListModel
{
  public:
    explicit SimpleObjectListModel(QObject* parent = nullptr) : QAbstractListModel { parent } {}
    ~SimpleObjectListModel() override = default;

    [[nodiscard]] int rowCount(const QModelIndex& parent = QModelIndex()) const override
    {
        Q_UNUSED(parent)

        return static_cast<int>(_objects.size());
    }

    [[nodiscard]] QVariant headerData(int section,
                                      Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override
    {
        Q_UNUSED(section)
        if (orientation == Qt::Orientation::Vertical) { return {}; }

        if (role == Qt::DisplayRole) { return SimpleObjectListModel::tr(headerValue); }
        return {};
    }

    [[nodiscard]] QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
    {
        if (index.row() < 0 || index.row() >= rowCount()) { return {}; }

        if (role == Qt::DisplayRole) { return QVariant::fromValue(_objects.at(index.row()).get()); }
        return {};
    }

    const ObjectType* at(int rowIndex) const { return _objects.at(rowIndex).get(); }

    ObjectType* at(int rowIndex) { return _objects.at(rowIndex).get(); }

    Q_INVOKABLE int indexOf(const ObjectType* const object) const
    {
        auto foundElement = std::ranges::find_if(
          _objects, [object](const utils::own_qobject<ObjectType>& listObject) { return listObject == object; });
        if (foundElement == std::cend(_objects)) { return -1; }
        return static_cast<int>(std::distance(std::cbegin(_objects), foundElement));
    }

  protected:
    const OwnQObjectList<ObjectType>& objects() const { return _objects; }
    OwnQObjectList<ObjectType>& objects() { return _objects; }
    void setObjects(OwnQObjectList<ObjectType>&& objects) { _objects = std::move(objects); }
    void deleteAllObjects()
    {
        for (utils::own_qobject<ObjectType>& object : _objects) { object->deleteLater(); }
    }

  private:
    Q_DISABLE_COPY_MOVE(SimpleObjectListModel)

    OwnQObjectList<ObjectType> _objects;
};

template <class ObjectType, class AssociatedData, const char* headerValue = defaultHeaderData>
    requires DerivedQObjectWithAssociatedData<ObjectType, AssociatedData>
class SimpleDataObjectListModel : public SimpleObjectListModel<ObjectType, headerValue>
{
  public:
    explicit SimpleDataObjectListModel(QObject* parent = nullptr)
        : SimpleObjectListModel<ObjectType, headerValue> { parent }
    {
    }
    explicit SimpleDataObjectListModel(const QList<AssociatedData>& newData, QObject* parent = nullptr)
        : SimpleObjectListModel<ObjectType, headerValue> { parent }
    {
        for (const AssociatedData& associatedData : newData)
        {
            this->objects().emplace_back(utils::make_qobject<ObjectType>(*this, associatedData));
        }
    }
    ~SimpleDataObjectListModel() override = default;

  protected:
    QList<AssociatedData> listData() const
    {
        QList<AssociatedData> data;

        data.reserve(this->objects().size());

        for (const utils::own_qobject<ObjectType>& object : this->objects()) { data.append(object->objectData()); }

        return data;
    }

    void setListData(const QList<AssociatedData>& newListData)
    {
        QAbstractListModel::beginResetModel();

        this->deleteAllObjects();
        this->objects().clear();

        for (const AssociatedData& associatedData : newListData)
        {
            this->objects().emplace_back(utils::make_qobject<ObjectType>(*this, associatedData));
        }

        QAbstractListModel::endResetModel();
    }

  private:
    Q_DISABLE_COPY_MOVE(SimpleDataObjectListModel)
};

}    // namespace utils
