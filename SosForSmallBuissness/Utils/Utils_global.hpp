#pragma once

#include <QtCore/qglobal.h>

#if defined(UTILS_LIBRARY)
#define UTILS_EXPORT Q_DECL_EXPORT
#else
#define UTILS_EXPORT Q_DECL_IMPORT
#endif

#if defined(ENABLE_TESTING)
#define UTILS_TESTING_EXPORT UTILS_EXPORT
#else
#define UTILS_TESTING_EXPORT
#endif
