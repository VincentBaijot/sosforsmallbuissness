#include "XmlSerializationUtils.hpp"

#include <QDebug>

#include "Constants.hpp"

namespace utils
{
namespace xml
{
bool isDataStartElement(const QXmlStreamReader& reader, QStringView startElementName, QStringView dataName)
{
    if (!reader.isStartElement())
    {
        qCWarning(utilsCategory) << "Cannot read " << dataName << " from a non start element "
                                 << reader.tokenString();
        return false;
    }

    if (reader.name() != startElementName)
    {
        qCWarning(utilsCategory) << "Start element " << reader.name() << " do not correspond to "
                                 << startElementName << " cannot read " << dataName;
        return false;
    }

    return true;
}

bool checkEndElement(const QXmlStreamReader& reader, QStringView endElementName)
{
    return (reader.isEndElement() && reader.name() == endElementName) || reader.hasError();
}

}    // namespace xml
}    // namespace utils
