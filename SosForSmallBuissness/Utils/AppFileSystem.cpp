#include "AppFileSystem.hpp"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

#include "FileUtils.hpp"

namespace utils
{
QString AppFileSystem::sourceApplicationFolder()
{
#if defined(Q_OS_IOS)
    return QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
#elif defined(Q_OS_ANDROID)
    return QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + u"/"_qs
           + QCoreApplication::applicationName();
#else
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
#endif
}

QString AppFileSystem::logFolder()
{
    return sourceApplicationFolder() + QStringLiteral("/log/");
}

QString AppFileSystem::dataFolder()
{
    return sourceApplicationFolder() + QStringLiteral("/data/");
}

void AppFileSystem::createFolders()
{
    utils::FileUtils::createFolder(sourceApplicationFolder());
    utils::FileUtils::createFolder(logFolder());
    utils::FileUtils::createFolder(dataFolder());
}
}    // namespace utils
