cmake_minimum_required(VERSION 3.22)

function(print_location)
    file(RELATIVE_PATH
        FILE_RELAT_PATH # Output variable
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}
    )

    message("-- Processing ${FILE_RELAT_PATH}/")
endfunction()

# The plugin created by qt_add_qml_module is generated in a folder Debug/Release,
# but windeployqt can only install the plugin if its in the same folder than the qmldir ("QT_QML_OUTPUT_DIRECTORY")
# so use this customization to generate the plugin in the correct folder
function(correctPluginBuildLocation LIBRARY_NAME)
    get_target_property(CURRENT_BINARY_DIR_PROP ${LIBRARY_NAME} BINARY_DIR)

    set_target_properties(${LIBRARY_NAME}plugin
        PROPERTIES
            LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CURRENT_BINARY_DIR_PROP}
            LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CURRENT_BINARY_DIR_PROP}
            ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CURRENT_BINARY_DIR_PROP}
            ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CURRENT_BINARY_DIR_PROP})
endfunction()
